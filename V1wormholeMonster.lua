local Monster = GameData.wormhole.Monster
Monster[5500100] = {
  ID = 5500100,
  vessels = 5400101,
  durability = 5400101,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500101] = {
  ID = 5500101,
  vessels = 1,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500102] = {
  ID = 5500102,
  vessels = 2,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500103] = {
  ID = 5500103,
  vessels = 3,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500104] = {
  ID = 5500104,
  vessels = 4,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500105] = {
  ID = 5500105,
  vessels = 5,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500106] = {
  ID = 5500106,
  vessels = 6,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500201] = {
  ID = 5500201,
  vessels = 1,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500202] = {
  ID = 5500202,
  vessels = 2,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500203] = {
  ID = 5500203,
  vessels = 3,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500204] = {
  ID = 5500204,
  vessels = 4,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500205] = {
  ID = 5500205,
  vessels = 5,
  durability = 121,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5500206] = {
  ID = 5500206,
  vessels = 6,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500301] = {
  ID = 5500301,
  vessels = 1,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500302] = {
  ID = 5500302,
  vessels = 2,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500303] = {
  ID = 5500303,
  vessels = 3,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500304] = {
  ID = 5500304,
  vessels = 4,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500305] = {
  ID = 5500305,
  vessels = 5,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500306] = {
  ID = 5500306,
  vessels = 6,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500401] = {
  ID = 5500401,
  vessels = 1,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500402] = {
  ID = 5500402,
  vessels = 2,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500403] = {
  ID = 5500403,
  vessels = 3,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500404] = {
  ID = 5500404,
  vessels = 4,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500405] = {
  ID = 5500405,
  vessels = 5,
  durability = 170,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5500406] = {
  ID = 5500406,
  vessels = 6,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500501] = {
  ID = 5500501,
  vessels = 1,
  durability = 305,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5500502] = {
  ID = 5500502,
  vessels = 2,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500503] = {
  ID = 5500503,
  vessels = 3,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500504] = {
  ID = 5500504,
  vessels = 4,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500505] = {
  ID = 5500505,
  vessels = 5,
  durability = 305,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5500506] = {
  ID = 5500506,
  vessels = 6,
  durability = 305,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5500601] = {
  ID = 5500601,
  vessels = 1,
  durability = 347,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5500602] = {
  ID = 5500602,
  vessels = 2,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5500603] = {
  ID = 5500603,
  vessels = 3,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5500604] = {
  ID = 5500604,
  vessels = 4,
  durability = 347,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500605] = {
  ID = 5500605,
  vessels = 5,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5500606] = {
  ID = 5500606,
  vessels = 6,
  durability = 347,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500701] = {
  ID = 5500701,
  vessels = 1,
  durability = 396,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5500702] = {
  ID = 5500702,
  vessels = 2,
  durability = 396,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500703] = {
  ID = 5500703,
  vessels = 3,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500704] = {
  ID = 5500704,
  vessels = 4,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500705] = {
  ID = 5500705,
  vessels = 5,
  durability = 396,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500706] = {
  ID = 5500706,
  vessels = 6,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500801] = {
  ID = 5500801,
  vessels = 1,
  durability = 452,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5500802] = {
  ID = 5500802,
  vessels = 2,
  durability = 452,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5500803] = {
  ID = 5500803,
  vessels = 3,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500804] = {
  ID = 5500804,
  vessels = 4,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500805] = {
  ID = 5500805,
  vessels = 5,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500806] = {
  ID = 5500806,
  vessels = 6,
  durability = 452,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500901] = {
  ID = 5500901,
  vessels = 1,
  durability = 515,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5500902] = {
  ID = 5500902,
  vessels = 2,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500903] = {
  ID = 5500903,
  vessels = 3,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5500904] = {
  ID = 5500904,
  vessels = 4,
  durability = 515,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500905] = {
  ID = 5500905,
  vessels = 5,
  durability = 515,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5500906] = {
  ID = 5500906,
  vessels = 6,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501001] = {
  ID = 5501001,
  vessels = 1,
  durability = 735,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501002] = {
  ID = 5501002,
  vessels = 2,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501003] = {
  ID = 5501003,
  vessels = 3,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501004] = {
  ID = 5501004,
  vessels = 4,
  durability = 735,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501005] = {
  ID = 5501005,
  vessels = 5,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501006] = {
  ID = 5501006,
  vessels = 6,
  durability = 735,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5501101] = {
  ID = 5501101,
  vessels = 1,
  durability = 812,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501102] = {
  ID = 5501102,
  vessels = 2,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501103] = {
  ID = 5501103,
  vessels = 3,
  durability = 812,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501104] = {
  ID = 5501104,
  vessels = 4,
  durability = 812,
  Avatar = "head8",
  Ship = "ship5"
}
Monster[5501105] = {
  ID = 5501105,
  vessels = 5,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501106] = {
  ID = 5501106,
  vessels = 6,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501201] = {
  ID = 5501201,
  vessels = 1,
  durability = 896,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501202] = {
  ID = 5501202,
  vessels = 2,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501203] = {
  ID = 5501203,
  vessels = 3,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501204] = {
  ID = 5501204,
  vessels = 4,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501205] = {
  ID = 5501205,
  vessels = 5,
  durability = 896,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501206] = {
  ID = 5501206,
  vessels = 6,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501301] = {
  ID = 5501301,
  vessels = 1,
  durability = 987,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501302] = {
  ID = 5501302,
  vessels = 2,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501303] = {
  ID = 5501303,
  vessels = 3,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501304] = {
  ID = 5501304,
  vessels = 4,
  durability = 987,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501305] = {
  ID = 5501305,
  vessels = 5,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501306] = {
  ID = 5501306,
  vessels = 6,
  durability = 987,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501401] = {
  ID = 5501401,
  vessels = 1,
  durability = 1085,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501402] = {
  ID = 5501402,
  vessels = 2,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501403] = {
  ID = 5501403,
  vessels = 3,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501404] = {
  ID = 5501404,
  vessels = 4,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501405] = {
  ID = 5501405,
  vessels = 5,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501406] = {
  ID = 5501406,
  vessels = 6,
  durability = 1085,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5501501] = {
  ID = 5501501,
  vessels = 1,
  durability = 1390,
  Avatar = "head58",
  Ship = "ship61"
}
Monster[5501502] = {
  ID = 5501502,
  vessels = 2,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501503] = {
  ID = 5501503,
  vessels = 3,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501504] = {
  ID = 5501504,
  vessels = 4,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501505] = {
  ID = 5501505,
  vessels = 5,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5501506] = {
  ID = 5501506,
  vessels = 6,
  durability = 1390,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5501601] = {
  ID = 5501601,
  vessels = 1,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501602] = {
  ID = 5501602,
  vessels = 2,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501603] = {
  ID = 5501603,
  vessels = 3,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501604] = {
  ID = 5501604,
  vessels = 4,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501605] = {
  ID = 5501605,
  vessels = 5,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501606] = {
  ID = 5501606,
  vessels = 6,
  durability = 107,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501701] = {
  ID = 5501701,
  vessels = 1,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501702] = {
  ID = 5501702,
  vessels = 2,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501703] = {
  ID = 5501703,
  vessels = 3,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501704] = {
  ID = 5501704,
  vessels = 4,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501705] = {
  ID = 5501705,
  vessels = 5,
  durability = 121,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5501706] = {
  ID = 5501706,
  vessels = 6,
  durability = 121,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501801] = {
  ID = 5501801,
  vessels = 1,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501802] = {
  ID = 5501802,
  vessels = 2,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501803] = {
  ID = 5501803,
  vessels = 3,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501804] = {
  ID = 5501804,
  vessels = 4,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501805] = {
  ID = 5501805,
  vessels = 5,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501806] = {
  ID = 5501806,
  vessels = 6,
  durability = 142,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501901] = {
  ID = 5501901,
  vessels = 1,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501902] = {
  ID = 5501902,
  vessels = 2,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501903] = {
  ID = 5501903,
  vessels = 3,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501904] = {
  ID = 5501904,
  vessels = 4,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5501905] = {
  ID = 5501905,
  vessels = 5,
  durability = 170,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5501906] = {
  ID = 5501906,
  vessels = 6,
  durability = 170,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502001] = {
  ID = 5502001,
  vessels = 1,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502002] = {
  ID = 5502002,
  vessels = 2,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502003] = {
  ID = 5502003,
  vessels = 3,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502004] = {
  ID = 5502004,
  vessels = 4,
  durability = 305,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502005] = {
  ID = 5502005,
  vessels = 5,
  durability = 305,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5502006] = {
  ID = 5502006,
  vessels = 6,
  durability = 305,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5502101] = {
  ID = 5502101,
  vessels = 1,
  durability = 347,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502102] = {
  ID = 5502102,
  vessels = 2,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5502103] = {
  ID = 5502103,
  vessels = 3,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5502104] = {
  ID = 5502104,
  vessels = 4,
  durability = 347,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502105] = {
  ID = 5502105,
  vessels = 5,
  durability = 347,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5502106] = {
  ID = 5502106,
  vessels = 6,
  durability = 347,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502201] = {
  ID = 5502201,
  vessels = 1,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502202] = {
  ID = 5502202,
  vessels = 2,
  durability = 396,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502203] = {
  ID = 5502203,
  vessels = 3,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502204] = {
  ID = 5502204,
  vessels = 4,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502205] = {
  ID = 5502205,
  vessels = 5,
  durability = 396,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502206] = {
  ID = 5502206,
  vessels = 6,
  durability = 396,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502301] = {
  ID = 5502301,
  vessels = 1,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502302] = {
  ID = 5502302,
  vessels = 2,
  durability = 452,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5502303] = {
  ID = 5502303,
  vessels = 3,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502304] = {
  ID = 5502304,
  vessels = 4,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502305] = {
  ID = 5502305,
  vessels = 5,
  durability = 452,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502306] = {
  ID = 5502306,
  vessels = 6,
  durability = 452,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502401] = {
  ID = 5502401,
  vessels = 1,
  durability = 515,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502402] = {
  ID = 5502402,
  vessels = 2,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502403] = {
  ID = 5502403,
  vessels = 3,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502404] = {
  ID = 5502404,
  vessels = 4,
  durability = 515,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502405] = {
  ID = 5502405,
  vessels = 5,
  durability = 515,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502406] = {
  ID = 5502406,
  vessels = 6,
  durability = 515,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502501] = {
  ID = 5502501,
  vessels = 1,
  durability = 735,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502502] = {
  ID = 5502502,
  vessels = 2,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502503] = {
  ID = 5502503,
  vessels = 3,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502504] = {
  ID = 5502504,
  vessels = 4,
  durability = 735,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502505] = {
  ID = 5502505,
  vessels = 5,
  durability = 735,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502506] = {
  ID = 5502506,
  vessels = 6,
  durability = 735,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5502601] = {
  ID = 5502601,
  vessels = 1,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502602] = {
  ID = 5502602,
  vessels = 2,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502603] = {
  ID = 5502603,
  vessels = 3,
  durability = 812,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502604] = {
  ID = 5502604,
  vessels = 4,
  durability = 812,
  Avatar = "head8",
  Ship = "ship5"
}
Monster[5502605] = {
  ID = 5502605,
  vessels = 5,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502606] = {
  ID = 5502606,
  vessels = 6,
  durability = 812,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502701] = {
  ID = 5502701,
  vessels = 1,
  durability = 896,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502702] = {
  ID = 5502702,
  vessels = 2,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502703] = {
  ID = 5502703,
  vessels = 3,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502704] = {
  ID = 5502704,
  vessels = 4,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502705] = {
  ID = 5502705,
  vessels = 5,
  durability = 896,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502706] = {
  ID = 5502706,
  vessels = 6,
  durability = 896,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502801] = {
  ID = 5502801,
  vessels = 1,
  durability = 987,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502802] = {
  ID = 5502802,
  vessels = 2,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502803] = {
  ID = 5502803,
  vessels = 3,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502804] = {
  ID = 5502804,
  vessels = 4,
  durability = 987,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502805] = {
  ID = 5502805,
  vessels = 5,
  durability = 987,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502806] = {
  ID = 5502806,
  vessels = 6,
  durability = 987,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502901] = {
  ID = 5502901,
  vessels = 1,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502902] = {
  ID = 5502902,
  vessels = 2,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502903] = {
  ID = 5502903,
  vessels = 3,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502904] = {
  ID = 5502904,
  vessels = 4,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5502905] = {
  ID = 5502905,
  vessels = 5,
  durability = 1085,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5502906] = {
  ID = 5502906,
  vessels = 6,
  durability = 1085,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5503001] = {
  ID = 5503001,
  vessels = 1,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503002] = {
  ID = 5503002,
  vessels = 2,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503003] = {
  ID = 5503003,
  vessels = 3,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503004] = {
  ID = 5503004,
  vessels = 4,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503005] = {
  ID = 5503005,
  vessels = 5,
  durability = 1390,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503006] = {
  ID = 5503006,
  vessels = 6,
  durability = 1390,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5503101] = {
  ID = 5503101,
  vessels = 1,
  durability = 357,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503102] = {
  ID = 5503102,
  vessels = 2,
  durability = 357,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503103] = {
  ID = 5503103,
  vessels = 3,
  durability = 507,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503104] = {
  ID = 5503104,
  vessels = 4,
  durability = 357,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503105] = {
  ID = 5503105,
  vessels = 5,
  durability = 357,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503106] = {
  ID = 5503106,
  vessels = 6,
  durability = 357,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503201] = {
  ID = 5503201,
  vessels = 1,
  durability = 367,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503202] = {
  ID = 5503202,
  vessels = 2,
  durability = 367,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503203] = {
  ID = 5503203,
  vessels = 3,
  durability = 517,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503204] = {
  ID = 5503204,
  vessels = 4,
  durability = 367,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503205] = {
  ID = 5503205,
  vessels = 5,
  durability = 367,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5503206] = {
  ID = 5503206,
  vessels = 6,
  durability = 367,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503301] = {
  ID = 5503301,
  vessels = 1,
  durability = 382,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503302] = {
  ID = 5503302,
  vessels = 2,
  durability = 382,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503303] = {
  ID = 5503303,
  vessels = 3,
  durability = 532,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503304] = {
  ID = 5503304,
  vessels = 4,
  durability = 382,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503305] = {
  ID = 5503305,
  vessels = 5,
  durability = 382,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503306] = {
  ID = 5503306,
  vessels = 6,
  durability = 382,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503401] = {
  ID = 5503401,
  vessels = 1,
  durability = 402,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503402] = {
  ID = 5503402,
  vessels = 2,
  durability = 402,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503403] = {
  ID = 5503403,
  vessels = 3,
  durability = 552,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503404] = {
  ID = 5503404,
  vessels = 4,
  durability = 402,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503405] = {
  ID = 5503405,
  vessels = 5,
  durability = 402,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5503406] = {
  ID = 5503406,
  vessels = 6,
  durability = 402,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503501] = {
  ID = 5503501,
  vessels = 1,
  durability = 527,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503502] = {
  ID = 5503502,
  vessels = 2,
  durability = 527,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503503] = {
  ID = 5503503,
  vessels = 3,
  durability = 677,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5503504] = {
  ID = 5503504,
  vessels = 4,
  durability = 527,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503505] = {
  ID = 5503505,
  vessels = 5,
  durability = 527,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5503506] = {
  ID = 5503506,
  vessels = 6,
  durability = 527,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5503601] = {
  ID = 5503601,
  vessels = 1,
  durability = 557,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503602] = {
  ID = 5503602,
  vessels = 2,
  durability = 557,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5503603] = {
  ID = 5503603,
  vessels = 3,
  durability = 707,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5503604] = {
  ID = 5503604,
  vessels = 4,
  durability = 557,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503605] = {
  ID = 5503605,
  vessels = 5,
  durability = 557,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5503606] = {
  ID = 5503606,
  vessels = 6,
  durability = 557,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503701] = {
  ID = 5503701,
  vessels = 1,
  durability = 592,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503702] = {
  ID = 5503702,
  vessels = 2,
  durability = 592,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503703] = {
  ID = 5503703,
  vessels = 3,
  durability = 742,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5503704] = {
  ID = 5503704,
  vessels = 4,
  durability = 592,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503705] = {
  ID = 5503705,
  vessels = 5,
  durability = 592,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503706] = {
  ID = 5503706,
  vessels = 6,
  durability = 592,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503801] = {
  ID = 5503801,
  vessels = 1,
  durability = 632,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503802] = {
  ID = 5503802,
  vessels = 2,
  durability = 632,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5503803] = {
  ID = 5503803,
  vessels = 3,
  durability = 782,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5503804] = {
  ID = 5503804,
  vessels = 4,
  durability = 632,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503805] = {
  ID = 5503805,
  vessels = 5,
  durability = 632,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503806] = {
  ID = 5503806,
  vessels = 6,
  durability = 632,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503901] = {
  ID = 5503901,
  vessels = 1,
  durability = 677,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503902] = {
  ID = 5503902,
  vessels = 2,
  durability = 677,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5503903] = {
  ID = 5503903,
  vessels = 3,
  durability = 827,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5503904] = {
  ID = 5503904,
  vessels = 4,
  durability = 677,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503905] = {
  ID = 5503905,
  vessels = 5,
  durability = 677,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5503906] = {
  ID = 5503906,
  vessels = 6,
  durability = 677,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504001] = {
  ID = 5504001,
  vessels = 1,
  durability = 777,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504002] = {
  ID = 5504002,
  vessels = 2,
  durability = 777,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504003] = {
  ID = 5504003,
  vessels = 3,
  durability = 927,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504004] = {
  ID = 5504004,
  vessels = 4,
  durability = 777,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504005] = {
  ID = 5504005,
  vessels = 5,
  durability = 777,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504006] = {
  ID = 5504006,
  vessels = 6,
  durability = 777,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5504101] = {
  ID = 5504101,
  vessels = 1,
  durability = 832,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504102] = {
  ID = 5504102,
  vessels = 2,
  durability = 832,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504103] = {
  ID = 5504103,
  vessels = 3,
  durability = 982,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504104] = {
  ID = 5504104,
  vessels = 4,
  durability = 832,
  Avatar = "head8",
  Ship = "ship5"
}
Monster[5504105] = {
  ID = 5504105,
  vessels = 5,
  durability = 832,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504106] = {
  ID = 5504106,
  vessels = 6,
  durability = 832,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504201] = {
  ID = 5504201,
  vessels = 1,
  durability = 892,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504202] = {
  ID = 5504202,
  vessels = 2,
  durability = 892,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504203] = {
  ID = 5504203,
  vessels = 3,
  durability = 1042,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504204] = {
  ID = 5504204,
  vessels = 4,
  durability = 892,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504205] = {
  ID = 5504205,
  vessels = 5,
  durability = 892,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504206] = {
  ID = 5504206,
  vessels = 6,
  durability = 892,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504301] = {
  ID = 5504301,
  vessels = 1,
  durability = 957,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504302] = {
  ID = 5504302,
  vessels = 2,
  durability = 957,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504303] = {
  ID = 5504303,
  vessels = 3,
  durability = 1107,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504304] = {
  ID = 5504304,
  vessels = 4,
  durability = 957,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504305] = {
  ID = 5504305,
  vessels = 5,
  durability = 957,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504306] = {
  ID = 5504306,
  vessels = 6,
  durability = 957,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504401] = {
  ID = 5504401,
  vessels = 1,
  durability = 1027,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504402] = {
  ID = 5504402,
  vessels = 2,
  durability = 1027,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504403] = {
  ID = 5504403,
  vessels = 3,
  durability = 1177,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504404] = {
  ID = 5504404,
  vessels = 4,
  durability = 1027,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504405] = {
  ID = 5504405,
  vessels = 5,
  durability = 1027,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504406] = {
  ID = 5504406,
  vessels = 6,
  durability = 1027,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5504501] = {
  ID = 5504501,
  vessels = 1,
  durability = 1132,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504502] = {
  ID = 5504502,
  vessels = 2,
  durability = 1132,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504503] = {
  ID = 5504503,
  vessels = 3,
  durability = 1282,
  Avatar = "head116",
  Ship = "ship119"
}
Monster[5504504] = {
  ID = 5504504,
  vessels = 4,
  durability = 1132,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504505] = {
  ID = 5504505,
  vessels = 5,
  durability = 1132,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5504506] = {
  ID = 5504506,
  vessels = 6,
  durability = 1132,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5504601] = {
  ID = 5504601,
  vessels = 2,
  durability = 2200,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5504602] = {
  ID = 5504602,
  vessels = 2,
  durability = 2200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504603] = {
  ID = 5504603,
  vessels = 3,
  durability = 2200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504604] = {
  ID = 5504604,
  vessels = 4,
  durability = 2200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504605] = {
  ID = 5504605,
  vessels = 5,
  durability = 2200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504606] = {
  ID = 5504606,
  vessels = 6,
  durability = 2200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504701] = {
  ID = 5504701,
  vessels = 1,
  durability = 2400,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5504702] = {
  ID = 5504702,
  vessels = 2,
  durability = 2400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504703] = {
  ID = 5504703,
  vessels = 3,
  durability = 2400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504704] = {
  ID = 5504704,
  vessels = 4,
  durability = 2400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504705] = {
  ID = 5504705,
  vessels = 5,
  durability = 2400,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5504706] = {
  ID = 5504706,
  vessels = 6,
  durability = 2400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504801] = {
  ID = 5504801,
  vessels = 1,
  durability = 2800,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5504802] = {
  ID = 5504802,
  vessels = 2,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504803] = {
  ID = 5504803,
  vessels = 3,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504804] = {
  ID = 5504804,
  vessels = 4,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504805] = {
  ID = 5504805,
  vessels = 5,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504806] = {
  ID = 5504806,
  vessels = 6,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504901] = {
  ID = 5504901,
  vessels = 1,
  durability = 2800,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5504902] = {
  ID = 5504902,
  vessels = 2,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504903] = {
  ID = 5504903,
  vessels = 3,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504904] = {
  ID = 5504904,
  vessels = 4,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5504905] = {
  ID = 5504905,
  vessels = 5,
  durability = 2800,
  Avatar = "head24",
  Ship = "ship5"
}
Monster[5504906] = {
  ID = 5504906,
  vessels = 6,
  durability = 2800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505001] = {
  ID = 5505001,
  vessels = 1,
  durability = 3000,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505002] = {
  ID = 5505002,
  vessels = 2,
  durability = 3000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505003] = {
  ID = 5505003,
  vessels = 3,
  durability = 3000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505004] = {
  ID = 5505004,
  vessels = 4,
  durability = 3000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505005] = {
  ID = 5505005,
  vessels = 5,
  durability = 3000,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5505006] = {
  ID = 5505006,
  vessels = 6,
  durability = 3000,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5505101] = {
  ID = 5505101,
  vessels = 1,
  durability = 3400,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505102] = {
  ID = 5505102,
  vessels = 2,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5505103] = {
  ID = 5505103,
  vessels = 3,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5505104] = {
  ID = 5505104,
  vessels = 4,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505105] = {
  ID = 5505105,
  vessels = 5,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship37"
}
Monster[5505106] = {
  ID = 5505106,
  vessels = 6,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505201] = {
  ID = 5505201,
  vessels = 1,
  durability = 3400,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505202] = {
  ID = 5505202,
  vessels = 2,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505203] = {
  ID = 5505203,
  vessels = 3,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505204] = {
  ID = 5505204,
  vessels = 4,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505205] = {
  ID = 5505205,
  vessels = 5,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505206] = {
  ID = 5505206,
  vessels = 6,
  durability = 3400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505301] = {
  ID = 5505301,
  vessels = 1,
  durability = 3600,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505302] = {
  ID = 5505302,
  vessels = 2,
  durability = 3600,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5505303] = {
  ID = 5505303,
  vessels = 3,
  durability = 3600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505304] = {
  ID = 5505304,
  vessels = 4,
  durability = 3600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505305] = {
  ID = 5505305,
  vessels = 5,
  durability = 3600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505306] = {
  ID = 5505306,
  vessels = 6,
  durability = 3600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505401] = {
  ID = 5505401,
  vessels = 1,
  durability = 4000,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505402] = {
  ID = 5505402,
  vessels = 2,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505403] = {
  ID = 5505403,
  vessels = 3,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505404] = {
  ID = 5505404,
  vessels = 4,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505405] = {
  ID = 5505405,
  vessels = 5,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505406] = {
  ID = 5505406,
  vessels = 6,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505501] = {
  ID = 5505501,
  vessels = 1,
  durability = 4000,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505502] = {
  ID = 5505502,
  vessels = 2,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505503] = {
  ID = 5505503,
  vessels = 3,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505504] = {
  ID = 5505504,
  vessels = 4,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505505] = {
  ID = 5505505,
  vessels = 5,
  durability = 4000,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505506] = {
  ID = 5505506,
  vessels = 6,
  durability = 4000,
  Avatar = "head77",
  Ship = "ship80"
}
Monster[5505601] = {
  ID = 5505601,
  vessels = 1,
  durability = 4200,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505602] = {
  ID = 5505602,
  vessels = 2,
  durability = 4200,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505603] = {
  ID = 5505603,
  vessels = 3,
  durability = 4200,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505604] = {
  ID = 5505604,
  vessels = 4,
  durability = 4200,
  Avatar = "head8",
  Ship = "ship5"
}
Monster[5505605] = {
  ID = 5505605,
  vessels = 5,
  durability = 4200,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505606] = {
  ID = 5505606,
  vessels = 6,
  durability = 4200,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505701] = {
  ID = 5505701,
  vessels = 1,
  durability = 4700,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505702] = {
  ID = 5505702,
  vessels = 2,
  durability = 4700,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505703] = {
  ID = 5505703,
  vessels = 3,
  durability = 4700,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505704] = {
  ID = 5505704,
  vessels = 4,
  durability = 4700,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505705] = {
  ID = 5505705,
  vessels = 5,
  durability = 4700,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505706] = {
  ID = 5505706,
  vessels = 6,
  durability = 4700,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505801] = {
  ID = 5505801,
  vessels = 1,
  durability = 4600,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505802] = {
  ID = 5505802,
  vessels = 2,
  durability = 4600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505803] = {
  ID = 5505803,
  vessels = 3,
  durability = 4600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505804] = {
  ID = 5505804,
  vessels = 4,
  durability = 4600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505805] = {
  ID = 5505805,
  vessels = 5,
  durability = 4600,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505806] = {
  ID = 5505806,
  vessels = 6,
  durability = 4600,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505901] = {
  ID = 5505901,
  vessels = 1,
  durability = 4800,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5505902] = {
  ID = 5505902,
  vessels = 2,
  durability = 4800,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505903] = {
  ID = 5505903,
  vessels = 3,
  durability = 4800,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505904] = {
  ID = 5505904,
  vessels = 4,
  durability = 4800,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5505905] = {
  ID = 5505905,
  vessels = 5,
  durability = 4800,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5505906] = {
  ID = 5505906,
  vessels = 6,
  durability = 4800,
  Avatar = "head12",
  Ship = "ship47"
}
Monster[5506001] = {
  ID = 5506001,
  vessels = 1,
  durability = 5400,
  Avatar = "head130",
  Ship = "ship133"
}
Monster[5506002] = {
  ID = 5506002,
  vessels = 2,
  durability = 5400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5506003] = {
  ID = 5506003,
  vessels = 3,
  durability = 5400,
  Avatar = "head13",
  Ship = "ship16"
}
Monster[5506004] = {
  ID = 5506004,
  vessels = 4,
  durability = 5400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5506005] = {
  ID = 5506005,
  vessels = 5,
  durability = 5400,
  Avatar = "head13",
  Ship = "ship47"
}
Monster[5506006] = {
  ID = 5506006,
  vessels = 6,
  durability = 5400,
  Avatar = "head77",
  Ship = "ship80"
}
