local GameStateAlliance = GameStateManager.GameStateAlliance
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIUnalliance = LuaObjectManager:GetLuaObject("GameUIUnalliance")
local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
GameUIUnalliance.mReqIdx = -1
GameUIUnalliance.mJoinAllanceAwardData = {}
function GameUIUnalliance:InitLocalData()
  local alliances_info = {}
  alliances_info.total = 0
  alliances_info.index = 0
  alliances_info.perpage = 50
  alliances_info.search_keyword = ""
  alliances_info.data_list = {}
  alliances_info.applied_list = {}
  self.alliances_info = alliances_info
end
function GameUIUnalliance:AlliancesRequest()
  local content = {
    keyword = self.alliances_info.search_keyword,
    index = self.alliances_info.index,
    size = self.alliances_info.perpage
  }
  local function netCallProcess()
    NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, content, self.NetCallbackAlliances, true, netCallProcess)
  end
  netCallProcess()
end
function GameUIUnalliance:OnAddToGameState(game_state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:InitLocalData()
  self:AlliancesRequest()
  self:UpdateCreationCondition()
  GameUIUnalliance:isShowJoinAllianceAwardLayer()
  self.is_active = true
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local unlock = false
    local moduleStatus = GameGlobalData:GetData("modules_status").modules
    for k, v in pairs(moduleStatus) do
      if "friends" == v.name then
        unlock = v.status
      end
    end
    flash_obj:InvokeASCallback("_root", "SetContactBtnVisible", unlock)
  end
end
function GameUIUnalliance:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
  self.is_active = nil
  self.isOnShowCreatAlliance = nil
end
function GameUIUnalliance:IsActive()
  return self.is_active
end
function GameUIUnalliance:OnFSCommand(cmd, arg)
  if cmd == "update_alliance_item" then
    self:UpdateAllianceItem(tonumber(arg))
  elseif cmd == "select_alliance" then
    GameUIUnalliance:SelectAlliance(tonumber(arg))
  elseif cmd == "download_next_page" then
  elseif cmd == "btn_contacts" then
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameUIFriend.friendListCallback, true, nil)
  end
  if cmd == "gotab_alliance_list" then
    self:SelectTab("alliance_list")
    return
  end
  if cmd == "gotab_create_alliance" then
    self:SelectTab("alliance_create")
    return
  end
  if cmd == "gopage_alliance_list" then
    return
  end
  if cmd == "show_creat_alliance" then
    self.isOnShowCreatAlliance = true
  end
  if cmd == "close_creat_alliance" then
    self.isOnShowCreatAlliance = nil
  end
  if cmd == "alliance_next_page" then
    local pageIndex = GameStateAlliance._CurrentAlliancesPage + 1
    local pageCount = GameStateAlliance:AlliancePagesCount()
    if pageIndex < pageCount then
      local content = {
        keyword = GameStateAlliance._AllianceSearchKeyword,
        index = GameStateAlliance._CurrentAlliancesPage + 1,
        size = GameStateAlliance.numAllianceItemsPage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, content, self.serverCallback, true, nil)
    end
    return
  end
  if cmd == "alliance_prev_page" then
    local pageIndex = GameStateAlliance._CurrentAlliancesPage + 1
    local pageCount = GameStateAlliance:AlliancePagesCount()
    if pageIndex > 1 then
      local content = {
        keyword = GameStateAlliance._AllianceSearchKeyword,
        index = GameStateAlliance._CurrentAlliancesPage - 1,
        size = GameStateAlliance.numAllianceItemsPage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, content, self.serverCallback, true, nil)
    end
    return
  end
  if cmd == "alliance_first_page" then
    local pageIndex = GameStateAlliance._CurrentAlliancesPage + 1
    local pageCount = GameStateAlliance:AlliancePagesCount()
    if pageIndex > 1 then
      local content = {
        keyword = GameStateAlliance._AllianceSearchKeyword,
        index = 0,
        size = GameStateAlliance.numAllianceItemsPage
      }
      NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, content, self.serverCallback, true, nil)
    end
    return
  end
  if cmd == "alliance_last_page" then
    if pageIndex < pageCount then
    end
    return
  end
  if cmd == "alliance_page_moveto" then
    local pageIndex = GameStateAlliance._CurrentAlliancesPage + 1
    local pageCount = GameStateAlliance:AlliancePagesCount()
    local newPageIndex = 0
    if arg == "next" and pageIndex < pageCount then
      newPageIndex = pageIndex + 1
    elseif arg == "prev" and pageIndex > 1 then
      newPageIndex = pageIndex - 1
    elseif arg == "last" and pageIndex < pageCount then
      newPageIndex = pageCount
    elseif arg == "first" and pageIndex > 1 then
      newPageIndex = 1
    else
      assert(false)
    end
    self:UpdateAlliancesData(newPageIndex)
    return
  end
  if cmd == "apply_alliance" then
    GameUIUnalliance.mReqIdx = tonumber(arg)
    local data_alliance = self:GetAllianceData(tonumber(arg))
    self:ApplyAlliance(data_alliance.id)
    return
  end
  if cmd == "unapply_alliance" then
    local data_alliance = self:GetAllianceData(tonumber(arg))
    self:UnapplyAlliance(data_alliance.id)
    return
  end
  if cmd == "create_alliance" then
    if arg ~= "" then
      local content = {name = arg}
      NetMessageMgr:SendMsg(NetAPIList.alliance_create_req.Code, content, GameUIUnalliance.NetCallbackCreate, true, nil)
    else
      local errorText = GameLoader:GetGameText("LC_MENU_CREAT_ALLIANCE_NAME_CANNOT_BE_EMPTY")
      GameTip:Show(errorText)
    end
    return
  end
  if cmd == "search_alliance" then
    self.alliances_info.data_list = {}
    self.alliances_info.search_keyword = arg
    self.alliances_info.index = 0
    self:AlliancesRequest()
    return
  end
  if cmd == "quit_state" then
    GameStateAlliance:Quit()
    return
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUIUnalliance:GetAllianceData(index)
  if self.alliances_info then
    return self.alliances_info.data_list[index]
  end
  return nil
end
function GameUIUnalliance:UpdateAllianceData(new_data)
  for index, old_data in ipairs(self.alliances_info.data_list) do
  end
end
function GameUIUnalliance:InitFlashObject()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "setLocalText", "BUTTON_CAPTION_APPLY", GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_APPLY"))
  flash_obj:InvokeASCallback("_root", "setLocalText", "BUTTON_CAPTION_CANCEL", GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL"))
end
function GameUIUnalliance:Visible(is_visible, mc_path)
  mc_path = mc_path or -1
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_setVisible", mc_path, is_visible)
end
function GameUIUnalliance:InitListbox()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "initListbox")
  end
end
function GameUIUnalliance:PushListItem(item_count)
  self:GetFlashObject():InvokeASCallback("_root", "pushListItem", item_count)
end
function GameUIUnalliance:UpdateCreationCondition()
  local level_require = GameGlobalData:GetGameConfig("alliance_creation_level_req")
  local money_require = GameGlobalData:GetGameConfig("alliance_creation_money")
  if not level_require or not money_require then
    do
      local content = {}
      content.config_attrs = {}
      table.insert(content.config_attrs, "alliance_creation_money")
      table.insert(content.config_attrs, "alliance_creation_level_req")
      local function netCallProcess()
        NetMessageMgr:SendMsg(NetAPIList.system_configuration_req.Code, content, self.NetCallbackConfig, false, netCallProcess)
      end
      netCallProcess()
    end
  elseif self:GetFlashObject() then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    local resourceInfo = GameGlobalData:GetData("resource")
    local isAllowed = level_require <= levelInfo.level
    self:GetFlashObject():InvokeASCallback("_root", "updateCreationCondition", level_require, GameUtils.numberConversion(money_require), isAllowed)
  end
end
function GameUIUnalliance:UpdateAllianceItem(index)
  local alliance_data = self.alliances_info.data_list[index]
  local alliance_rank = alliance_data.rank
  local alliance_name = alliance_data.name
  local alliance_level = alliance_data.level_info.level
  local alliance_chairman = GameUtils:GetUserDisplayName(alliance_data.creator_name)
  local alliance_members = alliance_data.members_count
  local alliance_maxmembers = alliance_data.members_max
  local alliance_status = self:CheckApplyStatus(alliance_data.id)
  self:GetFlashObject():InvokeASCallback("_root", "updateAllianceItem", index, alliance_rank, alliance_name, alliance_level, alliance_chairman, alliance_members, alliance_maxmembers, alliance_status)
end
function GameUIUnalliance:SelectAlliance(index)
  index = index or -1
  self:GetFlashObject():InvokeASCallback("_root", "selectAllianceItem", index)
  local alliance_data = self.alliances_info.data_list[index]
  if alliance_data then
    local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
    GameUIAllianceInfo:SetAllianceData(alliance_data)
    GameUIAllianceInfo:SetApplyStatus(self:CheckApplyStatus(alliance_data.id))
    GameStateManager:GetCurrentGameState():AddObject(GameUIAllianceInfo)
  end
end
function GameUIUnalliance:CheckApplyStatus(alliance_id)
  for _, applied in ipairs(self.alliances_info.applied_list) do
    if alliance_id == applied then
      return "cancel"
    end
  end
  if #self.alliances_info.applied_list < 3 then
    return "apply"
  else
    return "disable"
  end
end
function GameUIUnalliance:UpdatePageInfo()
  local pageIndex = GameStateAlliance._CurrentAlliancesPage + 1
  local pageCount = GameStateAlliance:AlliancePagesCount()
  local pageItemPath = "AllianceListDialog._menuPage."
  local flash_obj = self:GetFlashObject()
  if pageIndex == 1 then
    flash_obj:SetBtnEnable(pageItemPath .. "btnFirst", false)
    flash_obj:SetBtnEnable(pageItemPath .. "btnPrev", false)
  else
    flash_obj:SetBtnEnable(pageItemPath .. "btnFirst", true)
    flash_obj:SetBtnEnable(pageItemPath .. "btnPrev", true)
  end
  if pageIndex == pageCount then
    flash_obj:SetBtnEnable(pageItemPath .. "btnLast", false)
    flash_obj:SetBtnEnable(pageItemPath .. "btnNext", false)
  else
    flash_obj:SetBtnEnable(pageItemPath .. "btnLast", true)
    flash_obj:SetBtnEnable(pageItemPath .. "btnNext", true)
  end
  local pageText = tostring(pageIndex) .. "/" .. tostring(pageCount)
  flash_obj:SetText(pageItemPath .. "_textInfo", pageText)
end
function GameUIUnalliance:UpdateAlliancesData(index_page)
  local packet = {
    keyword = GameStateAlliance._AllianceSearchKeyword,
    index = index_page - 1,
    size = GameUIUnalliance._NumAlliancesPerPage
  }
  NetMessageMgr:SendMsg(NetAPIList.alliances_req.Code, packet, GameUIUnalliance.serverCallback, true, nil)
end
function GameUIUnalliance:NeedUpdateAllianceItems()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "needUpdateListItem")
end
function GameUIUnalliance:ApplyAlliance(index_alliance)
  local content = {id = index_alliance}
  NetMessageMgr:SendMsg(NetAPIList.alliance_apply_req.Code, content, GameUIUnalliance.NetCallbackApply, true, nil)
end
function GameUIUnalliance:UnapplyAlliance(index_alliance)
  local content = {id = index_alliance}
  NetMessageMgr:SendMsg(NetAPIList.alliance_unapply_req.Code, content, GameUIUnalliance.NetCallbackUnapply, true, nil)
end
function GameUIUnalliance:PrepareCreateAlliance()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_ShowNameAlliance")
end
function GameUIUnalliance:CreateAlliance(name_alliance)
  local content = {name = name_alliance}
  NetMessageMgr:SendMsg(NetAPIList.alliance_create_req.Code, content, GameUIUnalliance.NetCallbackCreate, true, nil)
end
function GameUIUnalliance:ShowWarningTip(text_content, display_time)
  self:GetFlashObject():InvokeASCallback("_root", "showWarningTip", text_content, display_time)
end
function GameUIUnalliance:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "onUpdateFrame", dt)
end
function GameUIUnalliance:RegisterJoinAllianceAward(...)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.alliance_guid_ntf.Code, GameUIUnalliance.JoinAllianceAward)
end
function GameUIUnalliance.JoinAllianceAward(content)
  DebugOut("JoinAllianceAward = ")
  DebugTable(content)
  GameUIUnalliance.mJoinAllanceAwardData = content
  if GameUtils:IsModuleUnlock("alliance") and content.is_guid then
    GameUIBarRight:MoveInRightMenu()
  end
end
function GameUIUnalliance:isShowJoinAllianceAwardLayer(...)
  DebugOut("isShowJoinAllianceAwardLayer:")
  local isShowFlag = self.mJoinAllanceAwardData.is_guid
  local item = self.mJoinAllanceAwardData.award
  local itemName = GameHelper:GetAwardNameText(item.item_type, item.number)
  local itemCount = "x" .. GameHelper:GetAwardCount(item.item_type, item.number, item.no)
  local titleInfo = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_REWARDS_CENTER_INFO")
  local textTitle = GameLoader:GetGameText("LC_MENU_ALLIANCE_DONATE_TITLE")
  local awardText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_REWARDS_INFO")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "isShowJoinAllianceAwardLayer", isShowFlag, itemName, itemCount, titleInfo, textTitle, awardText)
  end
end
function GameUIUnalliance.NetCallbackCreate(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and (content.api == NetAPIList.alliance_create_req.Code or content.api == NetAPIList.alliance_create_req.Code) then
    if content.code == 0 then
      if content.api == NetAPIList.alliance_create_req.Code then
        local success_text = "create alliance success"
        GameTip:Show(success_text)
      end
    elseif content.api == NetAPIList.alliance_create_req.Code then
      local errorTextContent = AlertDataList:GetTextFromErrorCode(content.code)
      GameUIUnalliance:ShowWarningTip(errorTextContent, 3000)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_create_fail_ack.Code then
    local failed_text = AlertDataList:GetTextFromErrorCode(content.code)
    GameUIUnalliance:ShowWarningTip(failed_text, 3000)
    return true
  end
  if msgtype == NetAPIList.alliance_create_ack.Code then
    local user_alliance = GameGlobalData:GetData("alliance")
    user_alliance.id = content.alliance.id
    user_alliance.position = GameGlobalData.MemberPositionType.chairman
    local success_text = GameLoader:GetGameText("LC_MENU_create_alliance_success")
    GameTip:Show(success_text)
    GameStateAlliance:CheckUserAllianceStatus()
    local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
    GameUIUserAlliance:UpdateUserAllianceInfo()
    return true
  end
  return false
end
function GameUIUnalliance.NetCallbackAlliances(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliances_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgtype == NetAPIList.alliances_ack.Code then
    local alliances_info = GameUIUnalliance.alliances_info
    alliances_info.total = content.total
    alliances_info.applied_list = content.applied_alliances
    alliances_info.index = content.index
    local before_update = #alliances_info.data_list
    local start_index = alliances_info.index * alliances_info.perpage
    for index, data in ipairs(content.alliances) do
      alliances_info.data_list[start_index + index] = data
    end
    if before_update == 0 then
      GameUIUnalliance:InitListbox()
    end
    local count_add = #alliances_info.data_list - before_update
    GameUIUnalliance:PushListItem(count_add)
    return true
  end
  return false
end
function GameUIUnalliance.NetCallbackConfig(msgtype, content)
  if msgtype == NetAPIList.system_configuration_ack.Code then
    local level_require = GameGlobalData:GetConfigValueFromContent(content, "alliance_creation_level_req")
    level_require = tonumber(level_require)
    GameGlobalData:UpdateGameConfig("alliance_creation_level_req", level_require)
    local cost_require = GameGlobalData:GetConfigValueFromContent(content, "alliance_creation_money")
    cost_require = tonumber(cost_require)
    GameGlobalData:UpdateGameConfig("alliance_creation_money", cost_require)
    GameUIUnalliance:UpdateCreationCondition()
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.system_configuration_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIUnalliance.NetCallbackApply(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_apply_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_apply_ack.Code then
    local allianceData = GameUIUnalliance:GetAllianceData(GameUIUnalliance.mReqIdx)
    if allianceData then
      if 1 == content.apply_type then
        local tip = GameLoader:GetGameText("LC_MENU_ALLIANCE_WELCOME_INFO")
        tip = string.format(tip, allianceData.name)
        GameTip:Show(tip)
      elseif 0 < allianceData.apply_limit then
        local tip = GameLoader:GetGameText("LC_MENU_ALLIANCE_LOW_LEVEL_INFO")
        tip = string.format(tip, allianceData.apply_limit)
        GameTip:Show(tip)
      else
        local tip = GameLoader:GetGameText("LC_MENU_ALLIANCE_SUCCESS_INFO")
        GameTip:Show(tip)
      end
    end
    GameUIUnalliance.alliances_info.applied_list = content.applied_alliances
    GameUIUnalliance:NeedUpdateAllianceItems()
    if GameUIAllianceInfo:IsActive() then
      local alliance_id = GameUIAllianceInfo:GetAllianceData().id
      GameUIAllianceInfo:SetApplyStatus(GameUIUnalliance:CheckApplyStatus(alliance_id))
    end
    return true
  end
  if msgtype == NetAPIList.alliance_apply_fail_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIUnalliance.NetCallbackUnapply(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_unapply_req.Code then
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_unapply_ack.Code then
    GameUIUnalliance.alliances_info.applied_list = content.applied_alliances
    GameUIUnalliance:NeedUpdateAllianceItems()
    if GameUIAllianceInfo:IsActive() then
      local alliance_id = GameUIAllianceInfo:GetAllianceData().id
      GameUIAllianceInfo:SetApplyStatus(GameUIUnalliance:CheckApplyStatus(alliance_id))
    end
    return true
  end
  if msgtype == NetAPIList.alliance_unapply_fail_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameUIUnalliance.OnAndroidBack()
    if GameUIUnalliance.isOnShowCreatAlliance then
      GameUIUnalliance:GetFlashObject():InvokeASCallback("_root", "animationMoveOutNameAlliance")
    else
      GameStateAlliance:Quit()
    end
  end
end
