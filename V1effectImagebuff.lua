local buff = GameData.effectImage.buff
table.insert(buff, {id = 1, buffname = "exile"})
table.insert(buff, {id = 2, buffname = "confusion"})
table.insert(buff, {id = 3, buffname = "locked"})
table.insert(buff, {id = 4, buffname = "def_dec"})
table.insert(buff, {id = 5, buffname = "def_add"})
table.insert(buff, {id = 6, buffname = "invincible"})
table.insert(buff, {id = 7, buffname = "dot"})
table.insert(buff, {id = 8, buffname = "shield"})
