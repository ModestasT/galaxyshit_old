local Monsters = GameData.ChapterMonsterAct1.Monsters
Monsters[110001] = {
  ID = 110001,
  vessels = 1,
  durability = 3000,
  Avatar = "mystery",
  Ship = "ship24"
}
Monsters[101001] = {
  ID = 101001,
  vessels = 2,
  durability = 136,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101002] = {
  ID = 101002,
  vessels = 1,
  durability = 165,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101003] = {
  ID = 101003,
  vessels = 2,
  durability = 105,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101004] = {
  ID = 101004,
  vessels = 1,
  durability = 180,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101005] = {
  ID = 101005,
  vessels = 2,
  durability = 105,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101006] = {
  ID = 101006,
  vessels = 2,
  durability = 100,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101007] = {
  ID = 101007,
  vessels = 3,
  durability = 24,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101008] = {
  ID = 101008,
  vessels = 4,
  durability = 78,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101009] = {
  ID = 101009,
  vessels = 2,
  durability = 156,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101010] = {
  ID = 101010,
  vessels = 2,
  durability = 156,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101011] = {
  ID = 101011,
  vessels = 2,
  durability = 185,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[101012] = {
  ID = 101012,
  vessels = 2,
  durability = 130,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101013] = {
  ID = 101013,
  vessels = 2,
  durability = 130,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101014] = {
  ID = 101014,
  vessels = 1,
  durability = 225,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101015] = {
  ID = 101015,
  vessels = 2,
  durability = 180,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101016] = {
  ID = 101016,
  vessels = 2,
  durability = 180,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101017] = {
  ID = 101017,
  vessels = 2,
  durability = 212,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101018] = {
  ID = 101018,
  vessels = 4,
  durability = 116,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101019] = {
  ID = 101019,
  vessels = 1,
  durability = 260,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101020] = {
  ID = 101020,
  vessels = 3,
  durability = 52,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101021] = {
  ID = 101021,
  vessels = 2,
  durability = 212,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101022] = {
  ID = 101022,
  vessels = 4,
  durability = 116,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101023] = {
  ID = 101023,
  vessels = 1,
  durability = 420,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[101024] = {
  ID = 101024,
  vessels = 3,
  durability = 48,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101025] = {
  ID = 101025,
  vessels = 2,
  durability = 188,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101026] = {
  ID = 101026,
  vessels = 4,
  durability = 104,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101027] = {
  ID = 101027,
  vessels = 1,
  durability = 230,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101028] = {
  ID = 101028,
  vessels = 3,
  durability = 350,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101029] = {
  ID = 101029,
  vessels = 2,
  durability = 200,
  Avatar = "head13",
  Ship = "ship37"
}
Monsters[101030] = {
  ID = 101030,
  vessels = 4,
  durability = 110,
  Avatar = "head13",
  Ship = "ship37"
}
Monsters[101031] = {
  ID = 101031,
  vessels = 5,
  durability = 110,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101032] = {
  ID = 101032,
  vessels = 3,
  durability = 350,
  Avatar = "head13",
  Ship = "ship37"
}
Monsters[101033] = {
  ID = 101033,
  vessels = 2,
  durability = 110,
  Avatar = "head13",
  Ship = "ship37"
}
Monsters[101034] = {
  ID = 101034,
  vessels = 4,
  durability = 110,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[101035] = {
  ID = 101035,
  vessels = 5,
  durability = 357,
  Avatar = "head13",
  Ship = "ship37"
}
Monsters[102001] = {
  ID = 102001,
  vessels = 2,
  durability = 215,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102002] = {
  ID = 102002,
  vessels = 1,
  durability = 326,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102003] = {
  ID = 102003,
  vessels = 2,
  durability = 272,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102004] = {
  ID = 102004,
  vessels = 3,
  durability = 92,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102005] = {
  ID = 102005,
  vessels = 1,
  durability = 245,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102006] = {
  ID = 102006,
  vessels = 2,
  durability = 205,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102007] = {
  ID = 102007,
  vessels = 1,
  durability = 356,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102008] = {
  ID = 102008,
  vessels = 2,
  durability = 226,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102009] = {
  ID = 102009,
  vessels = 1,
  durability = 506,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[102010] = {
  ID = 102010,
  vessels = 2,
  durability = 350,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102011] = {
  ID = 102011,
  vessels = 2,
  durability = 250,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102012] = {
  ID = 102012,
  vessels = 2,
  durability = 265,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102013] = {
  ID = 102013,
  vessels = 3,
  durability = 100,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102014] = {
  ID = 102014,
  vessels = 4,
  durability = 188,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102015] = {
  ID = 102015,
  vessels = 2,
  durability = 320,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102016] = {
  ID = 102016,
  vessels = 2,
  durability = 320,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102017] = {
  ID = 102017,
  vessels = 1,
  durability = 386,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102018] = {
  ID = 102018,
  vessels = 4,
  durability = 188,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102019] = {
  ID = 102019,
  vessels = 2,
  durability = 320,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102020] = {
  ID = 102020,
  vessels = 1,
  durability = 386,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102021] = {
  ID = 102021,
  vessels = 2,
  durability = 275,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102022] = {
  ID = 102022,
  vessels = 2,
  durability = 266,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102023] = {
  ID = 102023,
  vessels = 4,
  durability = 200,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102024] = {
  ID = 102024,
  vessels = 2,
  durability = 344,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102025] = {
  ID = 102025,
  vessels = 2,
  durability = 368,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102026] = {
  ID = 102026,
  vessels = 2,
  durability = 368,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102027] = {
  ID = 102027,
  vessels = 2,
  durability = 368,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102028] = {
  ID = 102028,
  vessels = 4,
  durability = 212,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102029] = {
  ID = 102029,
  vessels = 1,
  durability = 415,
  Avatar = "head8",
  Ship = "ship5"
}
Monsters[102030] = {
  ID = 102030,
  vessels = 2,
  durability = 386,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102031] = {
  ID = 102031,
  vessels = 2,
  durability = 395,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102032] = {
  ID = 102032,
  vessels = 2,
  durability = 256,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[102033] = {
  ID = 102033,
  vessels = 2,
  durability = 716,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102034] = {
  ID = 102034,
  vessels = 2,
  durability = 416,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[102035] = {
  ID = 102035,
  vessels = 2,
  durability = 236,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103001] = {
  ID = 103001,
  vessels = 2,
  durability = 381,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103002] = {
  ID = 103002,
  vessels = 1,
  durability = 485,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103003] = {
  ID = 103003,
  vessels = 2,
  durability = 382,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103004] = {
  ID = 103004,
  vessels = 3,
  durability = 149,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103005] = {
  ID = 103005,
  vessels = 1,
  durability = 484,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103006] = {
  ID = 103006,
  vessels = 2,
  durability = 381,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103007] = {
  ID = 103007,
  vessels = 1,
  durability = 484,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103008] = {
  ID = 103008,
  vessels = 2,
  durability = 381,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103009] = {
  ID = 103009,
  vessels = 1,
  durability = 516,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103010] = {
  ID = 103010,
  vessels = 2,
  durability = 404,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103011] = {
  ID = 103011,
  vessels = 2,
  durability = 405,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103012] = {
  ID = 103012,
  vessels = 2,
  durability = 405,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103013] = {
  ID = 103013,
  vessels = 3,
  durability = 124,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103014] = {
  ID = 103014,
  vessels = 4,
  durability = 225,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[103015] = {
  ID = 103015,
  vessels = 2,
  durability = 433,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103016] = {
  ID = 103016,
  vessels = 2,
  durability = 433,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103017] = {
  ID = 103017,
  vessels = 1,
  durability = 546,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103018] = {
  ID = 103018,
  vessels = 4,
  durability = 213,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103019] = {
  ID = 103019,
  vessels = 2,
  durability = 428,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103020] = {
  ID = 103020,
  vessels = 1,
  durability = 545,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103021] = {
  ID = 103021,
  vessels = 2,
  durability = 450,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103022] = {
  ID = 103022,
  vessels = 2,
  durability = 450,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103023] = {
  ID = 103023,
  vessels = 4,
  durability = 222,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103024] = {
  ID = 103024,
  vessels = 2,
  durability = 450,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103025] = {
  ID = 103025,
  vessels = 2,
  durability = 450,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103026] = {
  ID = 103026,
  vessels = 2,
  durability = 451,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103027] = {
  ID = 103027,
  vessels = 2,
  durability = 451,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103028] = {
  ID = 103028,
  vessels = 4,
  durability = 250,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103029] = {
  ID = 103029,
  vessels = 1,
  durability = 1922,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103030] = {
  ID = 103030,
  vessels = 2,
  durability = 1153,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103031] = {
  ID = 103031,
  vessels = 2,
  durability = 1154,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103032] = {
  ID = 103032,
  vessels = 2,
  durability = 1154,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103033] = {
  ID = 103033,
  vessels = 3,
  durability = 481,
  Avatar = "head13",
  Ship = "ship39"
}
Monsters[103034] = {
  ID = 103034,
  vessels = 2,
  durability = 1153,
  Avatar = "head13",
  Ship = "ship47"
}
Monsters[103035] = {
  ID = 103035,
  vessels = 4,
  durability = 432,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[103036] = {
  ID = 103036,
  vessels = 5,
  durability = 1153,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[103037] = {
  ID = 103037,
  vessels = 2,
  durability = 481,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[103038] = {
  ID = 103038,
  vessels = 2,
  durability = 360,
  Avatar = "head5",
  Ship = "ship39"
}
Monsters[104001] = {
  ID = 104001,
  vessels = 1,
  durability = 2306,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104002] = {
  ID = 104002,
  vessels = 2,
  durability = 1382,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104003] = {
  ID = 104003,
  vessels = 3,
  durability = 576,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104004] = {
  ID = 104004,
  vessels = 4,
  durability = 345,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104005] = {
  ID = 104005,
  vessels = 5,
  durability = 460,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104006] = {
  ID = 104006,
  vessels = 1,
  durability = 2306,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104007] = {
  ID = 104007,
  vessels = 2,
  durability = 1382,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104008] = {
  ID = 104008,
  vessels = 3,
  durability = 575,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104009] = {
  ID = 104009,
  vessels = 4,
  durability = 345,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104010] = {
  ID = 104010,
  vessels = 5,
  durability = 461,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104011] = {
  ID = 104011,
  vessels = 1,
  durability = 2304,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104012] = {
  ID = 104012,
  vessels = 2,
  durability = 1381,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104013] = {
  ID = 104013,
  vessels = 3,
  durability = 575,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104014] = {
  ID = 104014,
  vessels = 4,
  durability = 518,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[104015] = {
  ID = 104015,
  vessels = 5,
  durability = 460,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104016] = {
  ID = 104016,
  vessels = 1,
  durability = 2432,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104017] = {
  ID = 104017,
  vessels = 2,
  durability = 1459,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104018] = {
  ID = 104018,
  vessels = 3,
  durability = 608,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104019] = {
  ID = 104019,
  vessels = 4,
  durability = 364,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104020] = {
  ID = 104020,
  vessels = 5,
  durability = 486,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104021] = {
  ID = 104021,
  vessels = 1,
  durability = 2432,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104022] = {
  ID = 104022,
  vessels = 2,
  durability = 1458,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104023] = {
  ID = 104023,
  vessels = 3,
  durability = 608,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104024] = {
  ID = 104024,
  vessels = 4,
  durability = 364,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104025] = {
  ID = 104025,
  vessels = 5,
  durability = 486,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104026] = {
  ID = 104026,
  vessels = 1,
  durability = 3632,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104027] = {
  ID = 104027,
  vessels = 2,
  durability = 2178,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104028] = {
  ID = 104028,
  vessels = 3,
  durability = 907,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104029] = {
  ID = 104029,
  vessels = 4,
  durability = 544,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104030] = {
  ID = 104030,
  vessels = 5,
  durability = 725,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104031] = {
  ID = 104031,
  vessels = 1,
  durability = 3632,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[104032] = {
  ID = 104032,
  vessels = 2,
  durability = 2176,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[104033] = {
  ID = 104033,
  vessels = 3,
  durability = 907,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[104034] = {
  ID = 104034,
  vessels = 4,
  durability = 817,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[104035] = {
  ID = 104035,
  vessels = 5,
  durability = 725,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105001] = {
  ID = 105001,
  vessels = 1,
  durability = 3758,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105002] = {
  ID = 105002,
  vessels = 2,
  durability = 2254,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105003] = {
  ID = 105003,
  vessels = 3,
  durability = 939,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105004] = {
  ID = 105004,
  vessels = 4,
  durability = 563,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105005] = {
  ID = 105005,
  vessels = 5,
  durability = 752,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105006] = {
  ID = 105006,
  vessels = 1,
  durability = 3756,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105007] = {
  ID = 105007,
  vessels = 2,
  durability = 2254,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105008] = {
  ID = 105008,
  vessels = 3,
  durability = 939,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105009] = {
  ID = 105009,
  vessels = 4,
  durability = 563,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105010] = {
  ID = 105010,
  vessels = 5,
  durability = 752,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105011] = {
  ID = 105011,
  vessels = 1,
  durability = 3760,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105012] = {
  ID = 105012,
  vessels = 2,
  durability = 2254,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105013] = {
  ID = 105013,
  vessels = 3,
  durability = 940,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105014] = {
  ID = 105014,
  vessels = 4,
  durability = 563,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105015] = {
  ID = 105015,
  vessels = 5,
  durability = 1128,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[105016] = {
  ID = 105016,
  vessels = 1,
  durability = 3884,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105017] = {
  ID = 105017,
  vessels = 2,
  durability = 2332,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105018] = {
  ID = 105018,
  vessels = 3,
  durability = 972,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105019] = {
  ID = 105019,
  vessels = 4,
  durability = 583,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105020] = {
  ID = 105020,
  vessels = 5,
  durability = 777,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105021] = {
  ID = 105021,
  vessels = 1,
  durability = 3886,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105022] = {
  ID = 105022,
  vessels = 2,
  durability = 2330,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105023] = {
  ID = 105023,
  vessels = 3,
  durability = 971,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105024] = {
  ID = 105024,
  vessels = 4,
  durability = 583,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105025] = {
  ID = 105025,
  vessels = 5,
  durability = 777,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105026] = {
  ID = 105026,
  vessels = 1,
  durability = 4652,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105027] = {
  ID = 105027,
  vessels = 2,
  durability = 2793,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105028] = {
  ID = 105028,
  vessels = 3,
  durability = 1163,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105029] = {
  ID = 105029,
  vessels = 4,
  durability = 698,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105030] = {
  ID = 105030,
  vessels = 5,
  durability = 930,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105031] = {
  ID = 105031,
  vessels = 1,
  durability = 4652,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[105032] = {
  ID = 105032,
  vessels = 2,
  durability = 2792,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[105033] = {
  ID = 105033,
  vessels = 3,
  durability = 1163,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[105034] = {
  ID = 105034,
  vessels = 4,
  durability = 697,
  Avatar = "head24",
  Ship = "ship47"
}
Monsters[105035] = {
  ID = 105035,
  vessels = 5,
  durability = 1396,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106001] = {
  ID = 106001,
  vessels = 1,
  durability = 4782,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[106002] = {
  ID = 106002,
  vessels = 2,
  durability = 2869,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106003] = {
  ID = 106003,
  vessels = 3,
  durability = 1195,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106004] = {
  ID = 106004,
  vessels = 4,
  durability = 717,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106005] = {
  ID = 106005,
  vessels = 5,
  durability = 956,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106006] = {
  ID = 106006,
  vessels = 1,
  durability = 4780,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[106007] = {
  ID = 106007,
  vessels = 2,
  durability = 2868,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106008] = {
  ID = 106008,
  vessels = 3,
  durability = 1195,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106009] = {
  ID = 106009,
  vessels = 4,
  durability = 717,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106010] = {
  ID = 106010,
  vessels = 5,
  durability = 956,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106011] = {
  ID = 106011,
  vessels = 1,
  durability = 5148,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[106012] = {
  ID = 106012,
  vessels = 2,
  durability = 3088,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106013] = {
  ID = 106013,
  vessels = 3,
  durability = 1287,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106014] = {
  ID = 106014,
  vessels = 4,
  durability = 772,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106015] = {
  ID = 106015,
  vessels = 5,
  durability = 1030,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106016] = {
  ID = 106016,
  vessels = 1,
  durability = 7728,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[106017] = {
  ID = 106017,
  vessels = 2,
  durability = 3091,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106018] = {
  ID = 106018,
  vessels = 3,
  durability = 1287,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106019] = {
  ID = 106019,
  vessels = 4,
  durability = 772,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106020] = {
  ID = 106020,
  vessels = 5,
  durability = 1030,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106021] = {
  ID = 106021,
  vessels = 1,
  durability = 5278,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[106022] = {
  ID = 106022,
  vessels = 2,
  durability = 3168,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106023] = {
  ID = 106023,
  vessels = 3,
  durability = 1319,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106024] = {
  ID = 106024,
  vessels = 4,
  durability = 792,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106025] = {
  ID = 106025,
  vessels = 5,
  durability = 1055,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106026] = {
  ID = 106026,
  vessels = 1,
  durability = 5406,
  Avatar = "head12",
  Ship = "ship39"
}
Monsters[106027] = {
  ID = 106027,
  vessels = 2,
  durability = 3243,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106028] = {
  ID = 106028,
  vessels = 3,
  durability = 1351,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106029] = {
  ID = 106029,
  vessels = 4,
  durability = 810,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106030] = {
  ID = 106030,
  vessels = 5,
  durability = 1081,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106031] = {
  ID = 106031,
  vessels = 1,
  durability = 8109,
  Avatar = "head16",
  Ship = "ship5"
}
Monsters[106032] = {
  ID = 106032,
  vessels = 2,
  durability = 3244,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106033] = {
  ID = 106033,
  vessels = 3,
  durability = 1351,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[106034] = {
  ID = 106034,
  vessels = 4,
  durability = 810,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[106035] = {
  ID = 106035,
  vessels = 5,
  durability = 1080,
  Avatar = "head12",
  Ship = "ship47"
}
Monsters[107001] = {
  ID = 107001,
  vessels = 1,
  durability = 6062,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107002] = {
  ID = 107002,
  vessels = 2,
  durability = 3638,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107003] = {
  ID = 107003,
  vessels = 3,
  durability = 1516,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107004] = {
  ID = 107004,
  vessels = 4,
  durability = 909,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107005] = {
  ID = 107005,
  vessels = 5,
  durability = 1212,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107006] = {
  ID = 107006,
  vessels = 1,
  durability = 6062,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107007] = {
  ID = 107007,
  vessels = 2,
  durability = 3636,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107008] = {
  ID = 107008,
  vessels = 3,
  durability = 1516,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107009] = {
  ID = 107009,
  vessels = 4,
  durability = 909,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107010] = {
  ID = 107010,
  vessels = 5,
  durability = 1212,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107011] = {
  ID = 107011,
  vessels = 1,
  durability = 6064,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107012] = {
  ID = 107012,
  vessels = 2,
  durability = 3637,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107013] = {
  ID = 107013,
  vessels = 3,
  durability = 1516,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107014] = {
  ID = 107014,
  vessels = 4,
  durability = 909,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107015] = {
  ID = 107015,
  vessels = 5,
  durability = 1212,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107016] = {
  ID = 107016,
  vessels = 1,
  durability = 9093,
  Avatar = "head12",
  Ship = "ship5"
}
Monsters[107017] = {
  ID = 107017,
  vessels = 2,
  durability = 3636,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107018] = {
  ID = 107018,
  vessels = 3,
  durability = 1515,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107019] = {
  ID = 107019,
  vessels = 4,
  durability = 909,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107020] = {
  ID = 107020,
  vessels = 5,
  durability = 1212,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107021] = {
  ID = 107021,
  vessels = 1,
  durability = 6316,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107022] = {
  ID = 107022,
  vessels = 2,
  durability = 3790,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107023] = {
  ID = 107023,
  vessels = 3,
  durability = 1579,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107024] = {
  ID = 107024,
  vessels = 4,
  durability = 947,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107025] = {
  ID = 107025,
  vessels = 5,
  durability = 1263,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107026] = {
  ID = 107026,
  vessels = 1,
  durability = 6318,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107027] = {
  ID = 107027,
  vessels = 2,
  durability = 3790,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107028] = {
  ID = 107028,
  vessels = 3,
  durability = 1579,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107029] = {
  ID = 107029,
  vessels = 4,
  durability = 947,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107030] = {
  ID = 107030,
  vessels = 5,
  durability = 1263,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107031] = {
  ID = 107031,
  vessels = 1,
  durability = 6574,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[107032] = {
  ID = 107032,
  vessels = 2,
  durability = 3944,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107033] = {
  ID = 107033,
  vessels = 3,
  durability = 1643,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107034] = {
  ID = 107034,
  vessels = 4,
  durability = 986,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107035] = {
  ID = 107035,
  vessels = 5,
  durability = 1314,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107036] = {
  ID = 107036,
  vessels = 1,
  durability = 9861,
  Avatar = "head24",
  Ship = "ship42"
}
Monsters[107037] = {
  ID = 107037,
  vessels = 2,
  durability = 3944,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107038] = {
  ID = 107038,
  vessels = 3,
  durability = 1643,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[107039] = {
  ID = 107039,
  vessels = 4,
  durability = 985,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[107040] = {
  ID = 107040,
  vessels = 5,
  durability = 1315,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108001] = {
  ID = 108001,
  vessels = 1,
  durability = 6830,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[108002] = {
  ID = 108002,
  vessels = 2,
  durability = 4098,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108003] = {
  ID = 108003,
  vessels = 3,
  durability = 1707,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108004] = {
  ID = 108004,
  vessels = 4,
  durability = 1024,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108005] = {
  ID = 108005,
  vessels = 5,
  durability = 1366,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108006] = {
  ID = 108006,
  vessels = 1,
  durability = 6828,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[108007] = {
  ID = 108007,
  vessels = 2,
  durability = 4098,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108008] = {
  ID = 108008,
  vessels = 3,
  durability = 1708,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108009] = {
  ID = 108009,
  vessels = 4,
  durability = 1024,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108010] = {
  ID = 108010,
  vessels = 5,
  durability = 1366,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108011] = {
  ID = 108011,
  vessels = 1,
  durability = 10248,
  Avatar = "head24",
  Ship = "ship5"
}
Monsters[108012] = {
  ID = 108012,
  vessels = 2,
  durability = 4096,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108013] = {
  ID = 108013,
  vessels = 3,
  durability = 1708,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108014] = {
  ID = 108014,
  vessels = 4,
  durability = 1024,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108015] = {
  ID = 108015,
  vessels = 5,
  durability = 1366,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108016] = {
  ID = 108016,
  vessels = 1,
  durability = 7084,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[108017] = {
  ID = 108017,
  vessels = 2,
  durability = 4252,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108018] = {
  ID = 108018,
  vessels = 3,
  durability = 1771,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108019] = {
  ID = 108019,
  vessels = 4,
  durability = 1062,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108020] = {
  ID = 108020,
  vessels = 5,
  durability = 1416,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108021] = {
  ID = 108021,
  vessels = 1,
  durability = 7084,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[108022] = {
  ID = 108022,
  vessels = 2,
  durability = 4251,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108023] = {
  ID = 108023,
  vessels = 3,
  durability = 1771,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108024] = {
  ID = 108024,
  vessels = 4,
  durability = 1063,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108025] = {
  ID = 108025,
  vessels = 5,
  durability = 1416,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108026] = {
  ID = 108026,
  vessels = 1,
  durability = 7934,
  Avatar = "head25",
  Ship = "ship39"
}
Monsters[108027] = {
  ID = 108027,
  vessels = 2,
  durability = 4761,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108028] = {
  ID = 108028,
  vessels = 3,
  durability = 1983,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108029] = {
  ID = 108029,
  vessels = 4,
  durability = 1190,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108030] = {
  ID = 108030,
  vessels = 5,
  durability = 1587,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108031] = {
  ID = 108031,
  vessels = 1,
  durability = 11904,
  Avatar = "head7",
  Ship = "ship42"
}
Monsters[108032] = {
  ID = 108032,
  vessels = 2,
  durability = 4761,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108033] = {
  ID = 108033,
  vessels = 3,
  durability = 1983,
  Avatar = "head25",
  Ship = "ship5"
}
Monsters[108034] = {
  ID = 108034,
  vessels = 4,
  durability = 1190,
  Avatar = "head25",
  Ship = "ship47"
}
Monsters[108035] = {
  ID = 108035,
  vessels = 5,
  durability = 1587,
  Avatar = "head25",
  Ship = "ship47"
}
