local Data = GameData.Prestige.Data
Data[1] = {
  Prestige_Level = 1,
  Name = "\230\150\176\229\133\181",
  prestige = 100,
  new_matrix_cell = 4,
  fight_fleet_cnt = 2,
  award = "[{money,1510},{technique,500}]",
  spell = 0
}
Data[2] = {
  Prestige_Level = 2,
  Name = "\228\186\140\231\173\137\229\133\181",
  prestige = 300,
  new_matrix_cell = 2,
  fight_fleet_cnt = 2,
  award = "[{money,3770},{technique,600}]",
  spell = 0
}
Data[3] = {
  Prestige_Level = 3,
  Name = "\228\184\128\231\173\137\229\133\181",
  prestige = 500,
  new_matrix_cell = 0,
  fight_fleet_cnt = 3,
  award = "[{money,6080},{technique,700}]",
  spell = 0
}
Data[4] = {
  Prestige_Level = 4,
  Name = "\228\184\138\231\173\137\229\133\181",
  prestige = 1000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 3,
  award = "[{money,7440},{technique,800}]",
  spell = 3
}
Data[5] = {
  Prestige_Level = 5,
  Name = "\228\184\139\229\163\171",
  prestige = 1500,
  new_matrix_cell = 6,
  fight_fleet_cnt = 3,
  award = "[{money,9000},{technique,900}]",
  spell = 0
}
Data[6] = {
  Prestige_Level = 6,
  Name = "\228\184\173\229\163\171",
  prestige = 2500,
  new_matrix_cell = 0,
  fight_fleet_cnt = 3,
  award = "[{money,10770},{technique,1000}]",
  spell = 4
}
Data[7] = {
  Prestige_Level = 7,
  Name = "\228\184\138\229\163\171",
  prestige = 4000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 4,
  award = "[{money,12820},{technique,1100}]",
  spell = 0
}
Data[8] = {
  Prestige_Level = 8,
  Name = "\229\176\145\229\176\137",
  prestige = 5000,
  new_matrix_cell = 8,
  fight_fleet_cnt = 4,
  award = "[{money,15170},{technique,1300}]",
  spell = 0
}
Data[9] = {
  Prestige_Level = 9,
  Name = "\228\184\173\229\176\137",
  prestige = 10000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 4,
  award = "[{money,17880},{technique,1500}]",
  spell = 5
}
Data[10] = {
  Prestige_Level = 10,
  Name = "\228\184\138\229\176\137",
  prestige = 25000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,24530},{technique,1700}]",
  spell = 0
}
Data[11] = {
  Prestige_Level = 11,
  Name = "\229\176\145\230\160\161",
  prestige = 50000,
  new_matrix_cell = 9,
  fight_fleet_cnt = 5,
  award = "[{money,36520},{technique,2000}]",
  spell = 0
}
Data[12] = {
  Prestige_Level = 12,
  Name = "\228\184\173\230\160\161",
  prestige = 75000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,46210},{technique,2300}]",
  spell = 6
}
Data[13] = {
  Prestige_Level = 13,
  Name = "\228\184\138\230\160\161",
  prestige = 100000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,57880},{technique,2600}]",
  spell = 0
}
Data[14] = {
  Prestige_Level = 14,
  Name = "\229\135\134\229\176\134",
  prestige = 200000,
  new_matrix_cell = 1,
  fight_fleet_cnt = 5,
  award = "[{money,96000},{technique,3000}]",
  spell = 0
}
Data[15] = {
  Prestige_Level = 15,
  Name = "\229\176\145\229\176\134",
  prestige = 300000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,108000},{technique,3400}]",
  spell = 0
}
Data[16] = {
  Prestige_Level = 16,
  Name = "\228\184\173\229\176\134",
  prestige = 500000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,126000},{technique,3800}]",
  spell = 0
}
Data[17] = {
  Prestige_Level = 17,
  Name = "\228\184\138\229\176\134",
  prestige = 700000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,144000},{technique,4200}]",
  spell = 0
}
Data[18] = {
  Prestige_Level = 18,
  Name = "\228\184\128\230\152\159\228\184\138\229\176\134",
  prestige = 1000000,
  new_matrix_cell = 7,
  fight_fleet_cnt = 5,
  award = "[{money,180000},{technique,4700}]",
  spell = 0
}
Data[19] = {
  Prestige_Level = 19,
  Name = "\228\186\140\230\152\159\228\184\138\229\176\134",
  prestige = 2000000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,198000},{technique,5200}]",
  spell = 0
}
Data[20] = {
  Prestige_Level = 20,
  Name = "\228\184\137\230\152\159\228\184\138\229\176\134",
  prestige = 3000000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,216000},{technique,5700}]",
  spell = 0
}
Data[21] = {
  Prestige_Level = 21,
  Name = "\229\155\155\230\152\159\228\184\138\229\176\134",
  prestige = 5000000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,234000},{technique,6200}]",
  spell = 0
}
Data[22] = {
  Prestige_Level = 22,
  Name = "\228\186\148\230\152\159\228\184\138\229\176\134",
  prestige = 7000000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,252000},{technique,6700}]",
  spell = 0
}
Data[23] = {
  Prestige_Level = 23,
  Name = "\229\133\131\229\184\133",
  prestige = 10000000,
  new_matrix_cell = 3,
  fight_fleet_cnt = 5,
  award = "[{money,270000},{technique,7700}]",
  spell = 0
}
Data[24] = {
  Prestige_Level = 24,
  Name = "\229\164\167\229\133\131\229\184\133",
  prestige = 20000000,
  new_matrix_cell = 0,
  fight_fleet_cnt = 5,
  award = "[{money,288000},{technique,8700}]",
  spell = 0
}
