require("data1/WVEConstant.tfl")
require("data1/WVEPlayerManager.tfl")
require("data1/WVEPlayer.tfl")
require("data1/WVEEnemy.tfl")
require("data1/WVEPlayerMySelf.tfl")
require("data1/WVEMapPoint.tfl")
require("data1/WVEMapPath.tfl")
require("data1/WVEMap.tfl")
require("data1/WVEInfoPanelPoint.tfl")
require("data1/WVEInfoPanelEnemyPoint.tfl")
require("data1/WVEDamageInfoPanel.tfl")
require("data1/WVEInfoPanelPowerUp.tfl")
require("data1/WVEEnemyInfoPanel.tfl")
require("data1/WVEInfoPanelDie.tfl")
require("data1/WVEInfoPanelReward.tfl")
require("data1/WVEInfoPanelLeaderboard.tfl")
require("data1/WVEInfoPanelEscape.tfl")
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local QuestTutorialWVE = TutorialQuestManager.QuestTutorialWVE
local WVEGameTutorial = LuaObjectManager:GetLuaObject("WVEGameTutorial")
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
function WVEGameManeger:CreateManeger()
  self.mCurrentState = nil
  self.mWVEMap = WVEMap.new(self)
  self.mWVEPlayerManager = WVEPlayerManager.new(self)
  self.mWVEDamageInfoPanel = WVEDamageInfoPanel.new(self)
  self.mWVEEnemyInfoPanel = WVEEnemyInfoPanel.new(self)
  self.mWVEInfoPanelPowerUp = WVEInfoPanelPowerUp.new(self)
  self.mWVEInfoPanelReward = WVEInfoPanelReward.new(self)
  self.mWVEInfoPanelLeaderboard = WVEInfoPanelLeaderboard.new(self)
  self.mWVEInfoPanelDie = WVEInfoPanelDie.new(self)
  self.mWVEInfoPanelEscapeHelp = WVEInfoPanelEscape.new(self)
  self.mWVECharBar = GameUIGlobalChatBar
  self.mLeftTime = 0
  self.mTimeDesc = ""
  self.mCurrentRound = nil
  self.mNextRound = nil
  self.mIsInEnd = false
  self.mEscapeList = {}
  self.mCurrentEscapeEvent = nil
  self.mLastFrameEscapeTime = -1
end
function WVEGameManeger:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.mWVECharBar:Init(self, self:GetFlashObject(), "_root.main.chat_bar")
  self.mWVECharBar:SetDefaultChanel("battle")
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "InitLanguage", lang)
  self:MoveIn()
  self.mWVEInfoPanelPowerUp:Init()
end
function WVEGameManeger:OnEraseFromGameState()
  self.mWVECharBar:Release()
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function WVEGameManeger:MoveIn()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "MoveIn")
    self.mWVECharBar:MoveIn()
  end
end
function WVEGameManeger:MoveOut()
end
function WVEGameManeger:GetMap()
  return self.mWVEMap
end
function WVEGameManeger:GetPlayerManager()
  return self.mWVEPlayerManager
end
function WVEGameManeger:GotoState(state)
  if self.mCurrentState == state then
    return
  end
  self:OnLeaveState(self.mCurrentState)
  self.mCurrentState = state
  self:OnEnterState(self.mCurrentState)
end
function WVEGameManeger:OnEnterState(state)
  if state == EWVEGameState.STATE_ENTER_GAME then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateWVE)
    self:RegisterAllMsgHandler()
    self:RequestMapInfo()
    self:RequestAllCost()
    self:GotoState(EWVEGameState.STATE_IN_GAME)
  elseif state == EWVEGameState.STATE_BACK_TOGAME then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateWVE)
    self.mWVEMap:PrepareMapForDisplay()
    self:RequestAllNtf()
    self:RequestAllCost()
    self:GotoState(EWVEGameState.STATE_IN_GAME)
  elseif state == EWVEGameState.STATE_ENTER_TUTORIAL then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateWVE)
  end
end
function WVEGameManeger:GetGameState()
  return self.mCurrentState
end
function WVEGameManeger:RequestAllCost()
  local content = {}
  content.type = 0
  content.price_type = "revive_in_prime_invade"
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, content, self.RequestAllCostCallback, false, nil)
  content.price_type = "inc_power_cost_in_prime"
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, content, self.RequestAllCostCallback, false, nil)
  local jumpContent = {
    dest_id = -1,
    line_ids = {},
    distance = -1
  }
  NetMessageMgr:SendMsg(NetAPIList.prime_jump_price_req.Code, jumpContent, self.RequestPlyaerJumpPriceCallback, false, nil)
end
function WVEGameManeger.RequestAllCostCallback(msgType, content)
  DebugOut("RequestAllCostCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.price_ack.Code then
    if content.price_type == "revive_in_prime_invade" then
      WVEGameManeger.revive_credit = tonumber(content.price)
    elseif content.price_type == "inc_power_cost_in_prime" then
      WVEGameManeger.powerup_credit = tonumber(content.price)
      if WVEGameManeger.mWVEInfoPanelPowerUp then
        WVEGameManeger.mWVEInfoPanelPowerUp:SetPowerUpBtnCreditNum(WVEGameManeger.powerup_credit)
      end
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.price_req.Code then
    return true
  end
  return false
end
function WVEGameManeger.RequestPlyaerJumpPriceCallback(msgType, content)
  DebugOut("RequestPlyaerJumpPriceCallback = ")
  DebugTable(content)
  if msgType == NetAPIList.prime_jump_price_ack.Code then
    WVEGameManeger.jump_in_credit = content.price_unit
    WVEGameManeger.jump_in_credit_min = content.price_min
    WVEGameManeger.jump_in_credit_max = content.price_max
    return true
  elseif msgType == NetAPIList.common_ack.Code and content and content.api == NetAPIList.prime_jump_price_req.Code then
    return true
  end
  return false
end
function WVEGameManeger:OnLeaveState(state)
end
function WVEGameManeger:OnFSCommand(cmd, arg)
  DebugOut("WVEGameManeger cmd = ", cmd)
  DebugOut("self.mInfoPanelEnemyPoint = ", self.mInfoPanelEnemyPoint)
  if self.mInfoPanelPoint and self.mInfoPanelPoint:OnFSCommand(cmd, arg) then
    return
  end
  if self.mInfoPanelEnemyPoint and self.mInfoPanelEnemyPoint:OnFSCommand(cmd, arg) then
    DebugOut("mInfoPanelEnemyPoint = ", cmd)
    return
  end
  if self.mWVEDamageInfoPanel and self.mWVEDamageInfoPanel:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEInfoPanelPowerUp and self.mWVEInfoPanelPowerUp:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEEnemyInfoPanel and self.mWVEEnemyInfoPanel:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEInfoPanelReward and self.mWVEInfoPanelReward:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEInfoPanelLeaderboard and self.mWVEInfoPanelLeaderboard:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEInfoPanelDie and self.mWVEInfoPanelDie:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVEInfoPanelEscapeHelp and self.mWVEInfoPanelEscapeHelp:OnFSCommand(cmd, arg) then
    return
  end
  if self.mWVECharBar then
    self.mWVECharBar:OnFSCommand(cmd, arg)
  end
  if cmd == "PointClicked" then
    if self.mWVEMap:GetPointTypeByID(tonumber(arg)) == EWVEPointType.TYPE_ENEMY_BASE then
      return
    end
    if self.mWVEMap:GetPointTypeByID(tonumber(arg)) == EWVEPointType.TYPE_PLAYER_BASE then
      self:ShowInfoPanelPoint(tonumber(arg))
      return
    end
    if self.mWVEMap:IsEnemyPoint(tonumber(arg)) then
      self:ShowInfoPanelEnemyPoint(tonumber(arg))
    else
      self:ShowInfoPanelPoint(tonumber(arg))
    end
  elseif cmd == "showEnemyMarchInfo" then
    self.mWVEPlayerManager:ShowMarchEnemyInfo(arg)
  elseif cmd == "hideEnemyMarchInfo" then
    self.mWVEPlayerManager:HideMarchEnemyInfo(arg)
  elseif cmd == "close_main" then
    self:RemoveMessageHandler()
    self.mIsInEnd = false
    if GameStateManager.GameStateWVE.lastState == GameStateManager.GameStateBattlePlay then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    else
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateWVE.lastState)
    end
  elseif cmd == "show_help_panel" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_PRIMUS_INVADE_HELP"))
  elseif cmd == "playerStatusClicked" then
    DebugOut("playerStatusClicked", self.mWVEPlayerManager:GetPlayerMyself():GetState(), ",", EWVEPlayerState.STATE_PLAYER_DIE)
    if self.mWVEPlayerManager:GetPlayerMyself():GetState() == EWVEPlayerState.STATE_PLAYER_DIE then
      self.mWVEPlayerManager:GetPlayerMyself():PlayerDie()
    else
      self.mWVEPlayerManager:FoucsOnMyself()
    end
  end
end
function WVEGameManeger:StartWVEGame()
  self:CreateManeger()
  if QuestTutorialWVE:IsFinished() or WVEGameTutorial:IsFinishedLocal() then
    self:GotoState(EWVEGameState.STATE_ENTER_GAME)
  else
    self:GotoState(EWVEGameState.STATE_ENTER_TUTORIAL)
  end
end
function WVEGameManeger:BackToGame()
  self:GotoState(EWVEGameState.STATE_BACK_TOGAME)
end
function WVEGameManeger.Reconnected()
  DebugOut("wve recoonected")
  if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWVE and GameStateManager:GetCurrentGameState():IsObjectInState(WVEGameManeger) then
    DebugOut("wve recoonected get all ntf ")
    WVEGameManeger.mIsInEnd = false
    WVEGameManeger:RequestAllNtf()
  end
end
function WVEGameManeger:UpdateWVEGame(dt)
  self:UpdateSubPanel(dt)
  self:UpdateGameTime()
end
function WVEGameManeger:UpdateSubPanel(dt)
  if self.mWVEInfoPanelDie then
    self.mWVEInfoPanelDie:OnUpdate(dt)
  end
  if self.mInfoPanelPoint then
    self.mInfoPanelPoint:OnUpdate()
  end
  if self.mInfoPanelEnemyPoint then
    self.mInfoPanelEnemyPoint:OnUpdate()
  end
end
function WVEGameManeger:RequestMapInfo()
  NetMessageMgr:SendMsg(NetAPIList.prime_map_req.Code, nil, WVEGameManeger.RequestMapInfoCallback, true, nil)
end
function WVEGameManeger.RequestMapInfoCallback(msgType, content)
  DebugOut("msgType = ", msgType)
  DebugTable(content)
  if msgType == NetAPIList.prime_map_ack.Code then
    WVEGameManeger.mWVEMap:GenerateMapData(content)
    WVEGameManeger:RequestAllNtf()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prime_map_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function WVEGameManeger:RequestAllNtf()
  DebugOut("get all ntf when neccesy ")
  NetMessageMgr:SendMsg(NetAPIList.prime_active_req.Code, nil, nil, false, nil)
end
function WVEGameManeger:RegisterAllMsgHandler()
  DebugOut("RegisterAllMsgHandler")
  NetMessageMgr:RegisterReconnectHandler(WVEGameManeger.Reconnected)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_active_ntf.Code, WVEGameManeger.MapActivityInfoHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_my_info_ntf.Code, WVEGameManeger.MySelfInfoNtfHander)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_dot_status_ntf.Code, WVEGameManeger.BattleOnPointHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_dot_occupy_ntf.Code, WVEGameManeger.PointStateChangedHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_dot_occupy_no_ntf.Code, WVEGameManeger.PointOccupyNumberChangedHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_march_ntf.Code, WVEGameManeger.MapPlayerMarchInfoHandler)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_rank_ntf.Code, GameGlobalData.WVEPrimeRankInfoNtf)
  GameGlobalData:RegisterDataChangeCallback("prime_rank_info", WVEGameManeger.PrimeRankNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_round_result_ntf.Code, WVEGameManeger.GameStateNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_round_fight_ntf.Code, WVEGameManeger.PrimeCountChange)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_time_wve_boss_left_ntf.Code, WVEGameManeger.PrimeWillEscape)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.prime_do_wve_boss_left_ntf.Code, WVEGameManeger.CancelAllEscape)
end
function WVEGameManeger.PrimeCountChange(content)
  if content then
    local flash_obj = WVEGameManeger:GetFlashObject()
    if not flash_obj then
      return
    end
    local colorFrame = "p0"
    local temPercent = 100 - content.escape_rate
    if temPercent == 0 then
      colorFrame = "p0"
    elseif temPercent <= 5 then
      colorFrame = "p5"
    elseif temPercent <= 10 then
      colorFrame = "p10"
    elseif temPercent <= 20 then
      colorFrame = "p20"
    elseif temPercent <= 30 then
      colorFrame = "p30"
    elseif temPercent <= 40 then
      colorFrame = "p40"
    else
      colorFrame = "p100"
    end
    flash_obj:InvokeASCallback("_root", "MainMC_SetPrimePercent", colorFrame, temPercent .. "%")
  end
end
function WVEGameManeger.PrimeWillEscape(content)
  DebugOut("PrimeWillEscape:")
  DebugTable(content)
  DebugOut("pre mEscapeList:")
  DebugTable(WVEGameManeger.mEscapeList)
  table.sort(content.list, function(a, b)
    return a.left_time < b.left_time
  end)
  local fetchTime = os.time()
  for i, v in ipairs(content.list) do
    local escapeEvent = {}
    escapeEvent.id = v.id
    escapeEvent.leftTime = v.left_time
    escapeEvent.fetchTime = fetchTime
    table.insert(WVEGameManeger.mEscapeList, escapeEvent)
  end
  DebugOut("cur mEscapeList:")
  DebugTable(WVEGameManeger.mEscapeList)
  DebugOut("cur mCurrentEscapeEvent:")
  DebugTable(WVEGameManeger.mCurrentEscapeEvent)
end
function WVEGameManeger.CancelAllEscape(content)
  DebugOut("CancelAllEscape:")
  DebugTable(content)
  DebugOut("pre mEscapeList:")
  DebugTable(WVEGameManeger.mEscapeList)
  for i, v in ipairs(content.id_list) do
    local tmpIndex = 0
    for i2, v2 in ipairs(WVEGameManeger.mEscapeList) do
      if v2.id == v then
        tmpIndex = i2
      end
    end
    table.remove(WVEGameManeger.mEscapeList, tmpIndex)
    if WVEGameManeger.mCurrentEscapeEvent and WVEGameManeger.mCurrentEscapeEvent.id == v then
      WVEGameManeger.mCurrentEscapeEvent = nil
    end
  end
  if WVEGameManeger.mEscapeList and #WVEGameManeger.mEscapeList <= 0 then
    local flashObj = WVEGameManeger:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "MainMC_SetEscapeTimeVisible", false)
    end
  end
  DebugOut("cur mEscapeList:")
  DebugTable(WVEGameManeger.mEscapeList)
  DebugOut("cur mCurrentEscapeEvent:")
  DebugTable(WVEGameManeger.mCurrentEscapeEvent)
end
function WVEGameManeger.WVERoundResultNtf(content)
  DebugOut("WVERoundResultNtf:")
  DebugTable(content)
  if content then
    local flash_obj = WVEGameManeger:GetFlashObject()
    if not flash_obj then
      return
    end
    if WVEGameManeger.mIsInEnd then
      return
    end
    local isShowdBefore = false
    for i, v in ipairs(WVEGameManeger.resultShowFlags or {}) do
      if content.round_id == v then
        isShowdBefore = true
      end
    end
    if content.result and tonumber(content.result) == 1 and not isShowdBefore then
      WVEGameManeger.mIsInEnd = true
      flash_obj:InvokeASCallback("_root", "PlayVictoryAnim")
      WVEGameManeger:SetResultShowFlag(content.round_id)
    elseif content.result and tonumber(content.result) == 2 and not isShowdBefore then
      WVEGameManeger.mIsInEnd = true
      flash_obj:InvokeASCallback("_root", "PlayFailedAnim")
      WVEGameManeger:SetResultShowFlag(content.round_id)
    else
      WVEGameManeger.mIsInEnd = false
    end
  end
end
function WVEGameManeger:SetResultShowFlag(roundID)
  local content = {}
  content.round_id = roundID
  NetMessageMgr:SendMsg(NetAPIList.prime_push_show_round_req.Code, content, nil, false, nil)
end
function WVEGameManeger:RemoveMessageHandler()
  DebugOut("RemoveMessageHandler")
  NetMessageMgr:SendMsg(NetAPIList.prime_quit_req.Code, nil, nil, false, nil)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_active_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_my_info_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_dot_status_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_dot_occupy_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_dot_occupy_no_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_march_ntf.Code)
  NetMessageMgr:RemoveMsgHandler(NetAPIList.prime_rank_ntf.Code)
  NetMessageMgr:RegisterReconnectHandler(nil)
end
function WVEGameManeger.MapActivityInfoHandler(content)
  if content and content.my then
    WVEGameManeger.leftFreeJumpTime = content.my.left_free_jump_time
    WVEGameManeger.totalFreeJumpTime = content.my.total_free_jump_time
    WVEGameManeger.resultShowFlags = content.my.result_show_list
  end
  WVEGameManeger.mWVEPlayerManager:UpdateMyselfStatus(content.my)
  WVEGameManeger.mWVEMap:UpdateMapPointsStatus(content.dots)
  WVEGameManeger.mWVEPlayerManager:UpdatePlayerMarch(content.lines, true)
  WVEGameManeger.mWVEMap:UpdateEnemyDotStayTime(content.prime_dot_stay_time)
  WVEGameManeger.mWVEInfoPanelPowerUp:UpdatePowerUpTime(content.my)
end
function WVEGameManeger.PrimeRankNtf(content)
  WVEGameManeger.mWVEDamageInfoPanel.PrimeRankNtf(content)
end
function WVEGameManeger.MapPlayerMarchInfoHandler(content)
  WVEGameManeger.mWVEPlayerManager:UpdatePlayerMarch(content.marchs)
end
function WVEGameManeger.MySelfInfoNtfHander(content)
  DebugOut("MySelfInfoNtfHander")
  if content then
    WVEGameManeger.leftFreeJumpTime = content.left_free_jump_time
    WVEGameManeger.totalFreeJumpTime = content.total_free_jump_time
    WVEGameManeger.resultShowFlags = content.result_show_list
  end
  WVEGameManeger.mWVEPlayerManager:UpdateMyselfStatus(content)
  WVEGameManeger.mWVEInfoPanelPowerUp:UpdatePowerUpTime(content)
end
function WVEGameManeger.BattleOnPointHandler(content)
  if content ~= nil then
    WVEGameManeger.mWVEMap:UpdateMapSinglePointBattleState(content)
  end
end
function WVEGameManeger.PointStateChangedHandler(content)
  if content ~= nil then
    WVEGameManeger.mWVEMap:UpdateMapSinglePointOccupyState(content)
  end
end
function WVEGameManeger.PointOccupyNumberChangedHandler(content)
  if content ~= nil then
    WVEGameManeger.mWVEMap:UpdateMapSinglePointOccupyNumber(content)
  end
end
function WVEGameManeger:RequestMarch(targetPointID)
  local mySelf = self.mWVEPlayerManager:GetPlayerMyself()
  local myPointID = mySelf:GetPointID()
  local dotList = self.mWVEMap:CalculaeMarchPath(myPointID, targetPointID)
  if not dotList then
    return
  end
  local content = {dots = dotList}
  NetMessageMgr:SendMsg(NetAPIList.prime_march_req.Code, content, WVEGameManeger.RequestMarchCallback, true, nil)
end
function WVEGameManeger.RequestMarchCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prime_march_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    else
      if WVEGameManeger.mInfoPanelPoint then
        WVEGameManeger.mInfoPanelPoint:Hide()
      end
      if WVEGameManeger.mInfoPanelEnemyPoint then
        WVEGameManeger.mInfoPanelEnemyPoint:Hide()
      end
      local tips = GameLoader:GetGameText("LC_MENU_PRIMUS_INVADE_ATTACK_INFO")
      GameTip:Show(tips)
      return true
    end
  end
  return false
end
function WVEGameManeger:RequestJump(PointID)
  local jumpInfo = {}
  if self.mInfoPanelPoint then
    local curPrice = self.mInfoPanelPoint:GetJumpPrice(PointID)
    jumpInfo.targetPoint = PointID
    jumpInfo.pointList = {}
    jumpInfo.totalDistance = self.mInfoPanelPoint:GetCurDistance()
  elseif self.mInfoPanelEnemyPoint then
    local curPrice = self.mInfoPanelEnemyPoint:GetJumpPrice(PointID)
    jumpInfo.targetPoint = PointID
    jumpInfo.pointList = {}
    jumpInfo.totalDistance = self.mInfoPanelEnemyPoint:GetCurDistance()
  else
    return
  end
  local content = {
    dest_id = jumpInfo.targetPoint,
    line_ids = jumpInfo.pointList,
    distance = jumpInfo.totalDistance
  }
  DebugOut("RequestJump")
  DebugTable(content)
  local playerMyself = self.mWVEPlayerManager:GetPlayerMyself()
  playerMyself:PrepareJumpTo(targetPointID)
  NetMessageMgr:SendMsg(NetAPIList.prime_jump_req.Code, content, WVEGameManeger.RequestJumpCallback, true, nil)
end
function WVEGameManeger.RequestJumpCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prime_jump_req.Code then
    if content.code == 0 then
      if WVEGameManeger.mInfoPanelPoint then
        WVEGameManeger.mInfoPanelPoint:Hide()
      end
      if WVEGameManeger.mInfoPanelEnemyPoint then
        WVEGameManeger.mInfoPanelEnemyPoint:Hide()
      end
      WVEGameManeger.mWVEPlayerManager:PlayerJumpOutAnim()
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function WVEGameManeger:RequestRevive()
  NetMessageMgr:SendMsg(NetAPIList.revive_self_req.Code, nil, WVEGameManeger.RequestReviveCallback, true, nil)
end
function WVEGameManeger.RequestReviveCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.revive_self_req.Code then
    if content.code == 0 then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function WVEGameManeger:RequestRevivePrice()
  local price_content = {
    price_type = "tc_revivial_cost",
    type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_content, GameUIStarCraftMap.RequestRevivePriceCallback, false, nil)
end
function WVEGameManeger.RequestRevivePriceCallback(msgType, content)
  if msgType == NetAPIList.price_ack.Code and content.price_type == "tc_revivial_cost" then
    GameStateStarCraft.mRevivePrice = content.price
    return true
  end
  return false
end
function WVEGameManeger:RequestPointPanelInfo(pointID)
  local content = {site_id = pointID}
  NetMessageMgr:SendMsg(NetAPIList.prime_site_info_req.Code, content, WVEGameManeger.RequestPointPanelInfoCallback, false, nil)
end
function WVEGameManeger.RequestPointPanelInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prime_site_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.prime_site_info_ack.Code then
    if WVEGameManeger.mInfoPanelPoint then
      DebugOut("WVEGameManeger.mInfoPanelPoint.mPointID = ", WVEGameManeger.mInfoPanelPoint.mPointID)
      local playerMySelf = WVEGameManeger.mWVEPlayerManager:GetPlayerMyself()
      local pointInfo = {}
      pointInfo.isMyselfPoint = playerMySelf:IsStayOnPoint(WVEGameManeger.mInfoPanelPoint.mPointID)
      pointInfo.isMyselfOnMarch = playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_MARCH or playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_WAITMARCH
      pointInfo.isMyselfDie = playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_DIE
      pointInfo.playersCount = content.team_amount
      pointInfo.totalForce = content.total_force
      pointInfo.reviveCount = content.reparing_amount
      pointInfo.leftTime = content.left_time
      pointInfo.leftTimeStr = GameUtils:formatTimeString(content.left_time)
      pointInfo.pointName = WVEGameManeger.mWVEMap:GetPointNameByID(WVEGameManeger.mInfoPanelPoint.mPointID)
      pointInfo.pointType = WVEGameManeger.mWVEMap:GetPointTypeByID(WVEGameManeger.mInfoPanelPoint.mPointID)
      WVEGameManeger.mInfoPanelPoint:Show(pointInfo)
    end
    return true
  end
  return false
end
function WVEGameManeger:RequestEnemyPointPanelInfo(pointID)
  local content = {site_id = pointID}
  NetMessageMgr:SendMsg(NetAPIList.prime_site_info_req.Code, content, WVEGameManeger.RequestEnemyPointPanelInfoCallback, false, nil)
end
function WVEGameManeger.RequestEnemyPointPanelInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.prime_site_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.prime_site_info_ack.Code then
    if WVEGameManeger.mInfoPanelEnemyPoint then
      DebugOut("WVEGameManeger.mInfoPanelEnemyPoint.mPointID = ", WVEGameManeger.mInfoPanelEnemyPoint.mPointID)
      local playerMySelf = WVEGameManeger.mWVEPlayerManager:GetPlayerMyself()
      local pointInfo = {}
      pointInfo.isMyselfOnMarch = playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_MARCH or playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_WAITMARCH
      pointInfo.isMyselfDie = playerMySelf:GetState() == EWVEPlayerState.STATE_PLAYER_DIE
      pointInfo.pointName = WVEGameManeger.mWVEMap:GetPointNameByID(WVEGameManeger.mInfoPanelEnemyPoint.mPointID)
      pointInfo.pointType = WVEGameManeger.mWVEMap:GetPointTypeByID(WVEGameManeger.mInfoPanelEnemyPoint.mPointID)
      pointInfo.bigBossCount = content.prime_amount
      pointInfo.smallBossCount = content.monster_amount
      pointInfo.leftTime = content.left_time
      pointInfo.leftTimeStr = GameUtils:formatTimeString(content.left_time)
      DebugOut("pointInfo = ")
      DebugTable(pointInfo)
      WVEGameManeger.mInfoPanelEnemyPoint:Show(pointInfo)
    end
    return true
  end
  return false
end
function WVEGameManeger:ShowInfoPanelPoint(pointID)
  self.mInfoPanelPoint = nil
  self.mInfoPanelEnemyPoint = nil
  self.mInfoPanelPoint = WVEInfoPanelPoint.new(self, pointID)
  self:RequestPointPanelInfo(pointID)
end
function WVEGameManeger:ShowInfoPanelEnemyPoint(pointID)
  self.mInfoPanelEnemyPoint = nil
  self.mInfoPanelPoint = nil
  self.mInfoPanelEnemyPoint = WVEInfoPanelEnemyPoint.new(self, pointID)
  self:RequestEnemyPointPanelInfo(pointID)
end
function WVEGameManeger:CreateInfoPanel(panelType)
  if panelType == EWVEINFOPANEL_TYPE.PANEL_DIE then
    self.mWVEInfoPanelDie = self.mWVEInfoPanelDie or WVEInfoPanelDie.new(self)
    return self.mWVEInfoPanelDie
  end
  return nil
end
function WVEGameManeger:DestroyInfoPanel(panelType)
  if panelType == EWVEINFOPANEL_TYPE.PANEL_DIE and self.mWVEInfoPanelDie then
    self.mWVEInfoPanelDie:Hide()
  end
end
function WVEGameManeger:Update(dt)
  self.mWVEMap:Update(dt)
  self.mWVEPlayerManager:OnUpdate(dt)
  self.mWVEInfoPanelPowerUp:Update(dt)
  self:UpdateWVEGame(dt)
  self:UpdateEscapeEvent()
end
function WVEGameManeger:UpdateEscapeEvent()
  if self.mCurrentEscapeEvent then
    if self.mCurrentEscapeEvent.leftTime > os.time() - self.mCurrentEscapeEvent.fetchTime then
      local tmpLeftTime = self.mCurrentEscapeEvent.leftTime - (os.time() - self.mCurrentEscapeEvent.fetchTime)
      if tmpLeftTime < 0 then
        tmpLeftTime = 0
      end
      if self.mLastFrameEscapeTime == tmpLeftTime then
        return
      end
      self.mLastFrameEscapeTime = tmpLeftTime
      local timeStr = GameUtils:formatTimeString(tmpLeftTime)
      local timeDesc = GameLoader:GetGameText("LC_MENU_WVE_PRIMUS_GET_AWAY_TIME") .. " " .. timeStr
      local flashObj = self:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "MainMC_SetEscapeTime", timeDesc)
      end
      return
    else
      self.mCurrentEscapeEvent = nil
      local flashObj = self:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "MainMC_SetEscapeTimeVisible", false)
      end
    end
  end
  self.mCurrentEscapeEvent = nil
  if self.mEscapeList and 0 < #self.mEscapeList then
    local findingEvent = true
    while findingEvent do
      local event = self.mEscapeList[1]
      if event then
        if event.leftTime > os.time() - event.fetchTime then
          self.mCurrentEscapeEvent = event
          findingEvent = false
        end
        table.remove(self.mEscapeList, 1)
      else
        findingEvent = false
      end
    end
  end
end
function WVEGameManeger:UpdateChatBarText(htmlText)
  self.mWVECharBar:UpdateChatBarText(htmlText)
end
function WVEGameManeger.GameStateNtf(content)
  if content then
    WVEGameManeger.mGameLeftTime = content.activity_left_time
    WVEGameManeger.mGameStateFetchTime = os.time()
    DebugOut("content = ")
    DebugTable(content)
    if content.step_id ~= -1 then
      WVEGameManeger.mTimeDesc = string.format(GameLoader:GetGameText("LC_MENU_NEXT_PRIMUS_SQUAD"), content.step_count)
      WVEGameManeger.mLeftTime = content.step_left_time
    elseif content.current_round_id == -1 then
      if content.next_round_id == -1 then
        WVEGameManeger.mTimeDesc = GameLoader:GetGameText("LC_MENU_WVE_COOUNTDOWN_4")
        WVEGameManeger.mLeftTime = WVEGameManeger.mGameLeftTime
        WVEGameManeger.WVERoundResultNtf(content)
        WVEGameManeger:GotoState(EWVEGameState.STATE_GAME_END)
        WVEGameManeger:GetFlashObject():InvokeASCallback("_root", "WVEMap_EndBattle")
      elseif content.next_round_id == 1 then
        WVEGameManeger.mTimeDesc = GameLoader:GetGameText("LC_MENU_WVE_COOUNTDOWN_1")
        WVEGameManeger.mLeftTime = content.next_round_begin_time
      elseif content.next_round_id > 1 then
        WVEGameManeger.WVERoundResultNtf(content)
        WVEGameManeger.mTimeDesc = GameLoader:GetGameText("LC_MENU_WVE_COOUNTDOWN_3")
        WVEGameManeger.mLeftTime = content.next_round_begin_time
      end
    else
      WVEGameManeger.mTimeDesc = GameLoader:GetGameText("LC_MENU_WVE_COOUNTDOWN_2")
      WVEGameManeger.mLeftTime = content.current_round_left_time
    end
    WVEGameManeger.mLastFrameTime = -1
    WVEGameManeger:UpdateGameTime()
    if WVEGameManeger.mNextRound and WVEGameManeger.mNextRound ~= -1 and WVEGameManeger.mNextRound == content.current_round_id then
      local flash_obj = WVEGameManeger:GetFlashObject()
      if not flash_obj then
        return
      end
      flash_obj:InvokeASCallback("_root", "HideVictoryAnim")
      flash_obj:InvokeASCallback("_root", "HideFailedAnim")
    end
    WVEGameManeger.mNextRound = content.next_round_id
    WVEGameManeger.mCurrentRound = content.current_round_id
  end
end
function WVEGameManeger:UpdateGameTime()
  local tmpLeftTime = 0
  if 0 < self.mLeftTime then
    tmpLeftTime = self.mLeftTime - (os.time() - self.mGameStateFetchTime)
    if tmpLeftTime < 0 then
      tmpLeftTime = 0
      if self.mCurrentState == EWVEGameState.STATE_GAME_END then
        self:RemoveMessageHandler()
        GameUILab:SetItemStatus("prime_wve", 5)
        self.mIsInEnd = false
        GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
      end
    end
  else
    if self.mCurrentState == EWVEGameState.STATE_GAME_END then
      self:RemoveMessageHandler()
      GameUILab:SetItemStatus("prime_wve", 5)
      self.mIsInEnd = false
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
    end
    tmpLeftTime = 0
  end
  if self.mLastFrameTime == tmpLeftTime then
    return
  end
  self.mLastFrameTime = tmpLeftTime
  local timeStr = GameUtils:formatTimeString(tmpLeftTime)
  local timeDesc = self.mTimeDesc
  if tmpLeftTime <= 60 then
    timeStr = "<font color='#FF3E3E'>" .. timeStr .. "</font>"
  else
    timeStr = "<font color='#DCFFFC'>" .. timeStr .. "</font>"
  end
  DebugOut("timeStr = ", timeStr)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "MainMC_SetUpGameTime", tmpLeftTime, timeStr, timeDesc)
  end
end
if AutoUpdate.isAndroidDevice then
  function WVEGameManeger.OnAndroidBack()
    if WVEGameManeger.mWVEInfoPanelLeaderboard and WVEGameManeger.mWVEInfoPanelLeaderboard.IsShowLeaderboardWindow then
      WVEGameManeger:OnFSCommand("CloseLeaderboardPanel")
    elseif WVEGameManeger.mWVEInfoPanelReward and WVEGameManeger.mWVEInfoPanelReward.IsShowRewardWindow then
      WVEGameManeger:OnFSCommand("HideRewardWindow")
    elseif WVEGameManeger.mWVEEnemyInfoPanel and WVEGameManeger.mWVEEnemyInfoPanel.IsShowEnemyInfoWindow then
      WVEGameManeger:OnFSCommand("HideEnemyInfoWindow")
    elseif WVEGameManeger.mInfoPanelEnemyPoint and WVEGameManeger.mInfoPanelEnemyPoint.mIsShow then
      WVEGameManeger:OnFSCommand("EnemyPointMC_closePressed")
    elseif WVEGameManeger.mInfoPanelPoint and WVEGameManeger.mInfoPanelPoint.mIsShow then
      WVEGameManeger:OnFSCommand("PointMC_closePressed")
    else
      WVEGameManeger:GetFlashObject():InvokeASCallback("_root", "CloseWVEWindow")
    end
  end
end
