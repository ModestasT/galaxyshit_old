BattleEffectManager = {}
BattleEffectManager.m_defaultBuffID = 0
BattleEffectManager.m_defaultAttack = "normal"
BattleEffectManager.m_defaultSPAttack = {
  [0] = 16,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 50,
  [5] = 401
}
function BattleEffectManager:Init()
  self:GenerteEffectImagesMap()
  self:GenerteEffectsMap()
end
function BattleEffectManager:IsAdjutantAttack(player_adjutant, adjutant_id)
  local adjutantFleets = player_adjutant
  if not adjutantFleets then
    DebugOutBattlePlay("adjutantFleets is nil")
    return false
  end
  for k, v in pairs(adjutantFleets) do
    DebugOutBattlePlay(v.adjutant .. "," .. adjutant_id)
    if v.adjutant and v.adjutant == adjutant_id then
      DebugOutBattlePlay("adjutant's key:" .. k)
      return k
    end
  end
  return false
end
function BattleEffectManager:PreLoadBattlePlayImage(battleResult)
  local rounds = battleResult.rounds
  local buffEffect = {}
  local attackEffect = {}
  local attackNames = {}
  for _, round in ipairs(rounds) do
    local dots = round.dot
    local buffs = round.buff
    local attacks = round.attacks
    local sp_attacks = round.sp_attacks
    local isAdjutantAttack = false
    for _, buff in ipairs(buffs) do
      if buff.buff_effect ~= 0 then
        buffEffect[buff.buff_effect] = true
      end
    end
    for _, sp_attack in ipairs(sp_attacks) do
      if round.player1_action then
        isAdjutantAttack = sp_attack.adjutant ~= 0 and true
      else
        isAdjutantAttack = sp_attack.adjutant ~= 0 and self:IsAdjutantAttack(battleResult.player2_adjutant, sp_attack.adjutant)
      end
      attackEffect[sp_attack.sp_id] = not isAdjutantAttack
      if sp_attack.buff_effect ~= 0 then
        buffEffect[sp_attack.buff_effect] = true
      end
    end
    for _, attack in ipairs(attacks) do
      if USE_DEFAULT_EFFECT then
        attack.atk_effect = self.m_defaultAttack
      end
      attackNames[attack.atk_effect] = true
    end
  end
  DebugOutBattlePlay("buffEffect")
  DebugOutBattlePlayTable(buffEffect)
  DebugOutBattlePlay("attackEffect")
  DebugOutBattlePlayTable(attackEffect)
  DebugOutBattlePlay("attackNames")
  DebugOutBattlePlayTable(attackNames)
  self.m_preLoadImageList = {}
  DebugOut("Check buffEffect")
  for k, v in pairs(buffEffect) do
    if v then
      DebugOutBattlePlay("CheckComponent: ", k)
      self:LoadBuffEffectImageList(self.m_preLoadImageList, k)
    end
  end
  DebugOut("Check attackEffect")
  for k, v in pairs(attackEffect) do
    if v then
      DebugOutBattlePlay("CheckComponent: ", k)
      self:LoadAttackEffectImageList(self.m_preLoadImageList, k)
    end
  end
  DebugOut("Check attackNames")
  for k, v in pairs(attackNames) do
    if v then
      DebugOutBattlePlay("CheckComponent: ", k)
      self:LoadAttackEffectImageList(self.m_preLoadImageList, k)
    end
  end
  if SKIP_BATTLE_PLAY_PRELOAD then
    return
  end
  if ENABLE_BATTLE_LOADING_STEP then
    local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
    GameWaiting:ShowLoadingScreen()
    self:SplitLoadingRes()
    return
  end
  local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
  local flashObj = GameObjectBattleReplay:GetFlashObject()
  DebugOutBattlePlay("self.m_preLoadImageList")
  DebugOutBattlePlayTable(self.m_preLoadImageList)
  for img, _ in pairs(self.m_preLoadImageList) do
    DebugOutBattlePlay("PreLoadImage: ", img)
    flashObj:ReplaceTexture(img, img)
  end
end
function BattleEffectManager:SplitLoadingRes()
  if not ENABLE_BATTLE_LOADING_STEP then
    return
  end
  self.m_loadStepImageList = {}
  for img, _ in pairs(self.m_preLoadImageList) do
    table.insert(self.m_loadStepImageList, img)
  end
  DebugOutBattlePlay("self.m_loadStepImageList")
  DebugOutBattlePlayTable(self.m_loadStepImageList)
end
function BattleEffectManager:UpdateLoadingRes()
  if not ENABLE_BATTLE_LOADING_STEP then
    return true
  end
  if SKIP_BATTLE_PLAY_PRELOAD then
    return true
  end
  if not self.m_loadStepImageList then
    return true
  end
  local k_LoadResPerFrame = 2
  local currentLoadRes = 0
  while true do
    local img = self.m_loadStepImageList[1]
    if img then
      local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
      local flashObj = GameObjectBattleReplay:GetFlashObject()
      flashObj:ReplaceTexture(img, img)
      DebugOutBattlePlay("load " .. img)
      table.remove(self.m_loadStepImageList, 1)
    else
      break
    end
    currentLoadRes = currentLoadRes + 1
    if k_LoadResPerFrame <= currentLoadRes then
      return false
    end
  end
  self.m_loadStepImageList = nil
  local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
  GameWaiting:HideLoadingScreen()
  return true
end
function BattleEffectManager:CheckAttackEffectCanPlay(effectID)
  local attEffectName = GameDataAccessHelper:GetSpillEffect(effectID)
  DebugOutBattlePlay("BattleEffectManager:CheckAttackEffectCanPlay: ", effectID, attEffectName)
  if GameDataAccessHelper:IsSpellResNeedDownloadByName(attEffectName) then
    local effectImageList = GameDataAccessHelper:GetBattleEffectImages(attEffectName)
    if not effectImageList or #effectImageList <= 0 then
      return false
    end
    for k, v in pairs(effectImageList) do
      if (ext.crc32.crc32(v) == "" or ext.crc32.crc32(v) == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer(v) then
        return false
      end
    end
    return true
  end
  DebugOut("can play attack")
  return true
end
function BattleEffectManager:CheckBuffEffectCanPlay(effectID)
  DebugOut("check buff effect can play = ", effectID)
  if effectID == 0 then
    return true
  end
  local buffEffectName = GameDataAccessHelper:GetBuffEffectName(effectID)
  DebugOut("check buff effect can play name = ", buffEffectName)
  if GameDataAccessHelper:IsBuffResNeedDownload(effectID) then
    DebugOut("check buff effect need download = ", buffEffectName)
    local buffImageList = GameDataAccessHelper:GetBattleBuffImages(buffEffectName)
    if not buffImageList or #buffImageList <= 0 then
      DebugOut("check buff effect download not finish 111111 = ", buffEffectName)
      return false
    end
    local retValue = true
    for k, v in pairs(buffImageList) do
      if (ext.crc32.crc32(v) == "" or ext.crc32.crc32(v) == nil) and not AutoUpdateInBackground:IsBuffResUpdatedToServer(v) then
        DebugOut("check buff effect download not finish 2222222 = ", buffEffectName)
        retValue = false
      end
    end
    return retValue
  end
  return true
end
function BattleEffectManager:LoadBuffEffectImageList(appendList, effectID)
  local effectName = GameDataAccessHelper:GetBuffEffectName(effectID)
  local images
  if not effectName then
    DebugWarning(effectName, "LoadBuffEffectImageList1 " .. effectID)
    return
  end
  if LuaUtils:string_startswith(effectName, "buff_") then
    local resultTable = GameDataAccessHelper:GetEffectImages(effectName)
    if not resultTable then
      DebugWarning(resultTable, "LoadBuffEffectImageList2 " .. effectID)
      return
    end
    images = resultTable.images
  else
    local resultTable = GameDataAccessHelper:GetEffectImages("buff_" .. effectName)
    if not resultTable then
      DebugWarning(resultTable, "LoadBuffEffectImageList3 " .. effectID)
      return
    end
    images = resultTable.images
  end
  if not images then
    DebugWarning(images, "LoadBuffEffectImageList4" .. effectID)
    return
  end
  for _, img in ipairs(images) do
    appendList[img] = true
  end
end
function BattleEffectManager:LoadAttackEffectImageList(appendList, effect)
  local attackName
  if type(effect) == "number" then
    attackName = GameDataAccessHelper:GetSpillEffect(effect)
  elseif type(effect) == "string" then
    attackName = effect
  end
  if not attackName then
    DebugWarning(attackName, "LoadAttackEffectImageList1 " .. effect)
    return
  end
  local resultTable = GameDataAccessHelper:GetEffectImages("att_" .. attackName)
  if not resultTable then
    DebugWarning(resultTable, "LoadAttackEffectImageList2 " .. effect)
    return
  end
  local images = resultTable.images
  if not images then
    DebugWarning(images, "LoadAttackEffectImageList3 " .. effect)
    return
  end
  for _, img in ipairs(images) do
    appendList[img] = true
  end
end
function BattleEffectManager:CheckEffectImageLocal()
end
function BattleEffectManager:OnImageDownload(imageName)
end
function BattleEffectManager:CheckAllEffectCanPlay()
end
function BattleEffectManager:GenerteEffectImagesMap()
end
function BattleEffectManager:GenerteEffectsMap()
end
BattleEffectManager.m_battleBGEffect = {
  "battle_bg_blue.png",
  "battle_effct.png",
  "battle_play.png",
  "laser_hurt_new00.png",
  "laser_hurt_new02.png",
  "laser_hurt_new03.png",
  "laser_hurt_new05.png",
  "laser_hurt_new07.png",
  "laser_hurt_new09.png",
  "laser_hurt_new11.png",
  "laser_hurt_new13.png",
  "laser_hurt_new15.png",
  "LAZY_LOAD_gamma_ray.png",
  "LAZY_LOAD_locked_attack00000.png",
  "LAZY_LOAD_locked_attack00001.png",
  "LAZY_LOAD_locked_attack00002.png",
  "LAZY_LOAD_locked_attack00003.png",
  "LAZY_LOAD_locked_attack00004.png",
  "LAZY_LOAD_locked_attack00005.png",
  "LAZY_LOAD_locked_attack00006.png",
  "LAZY_LOAD_missile_attack00.png",
  "LAZY_LOAD_missile_attack01.png",
  "LAZY_LOAD_missile_attack02.png",
  "LAZY_LOAD_missile_attack03.png",
  "LAZY_LOAD_missile_attack04.png",
  "LAZY_LOAD_missile_attack05.png",
  "LAZY_LOAD_missile_attack06.png",
  "LAZY_LOAD_missile_attack07.png",
  "LAZY_LOAD_missile_attack08.png",
  "LAZY_LOAD_missile_attack09.png",
  "LAZY_LOAD_missile_attack10.png",
  "LAZY_LOAD_missile_attack11.png",
  "LAZY_LOAD_missile_attack12.png",
  "LAZY_LOAD_missile_attack13.png",
  "LAZY_LOAD_missile_attack14.png",
  "LAZY_LOAD_missile_attack15.png",
  "LAZY_LOAD_missile_attack16.png",
  "LAZY_LOAD_missile_attack17.png",
  "LAZY_LOAD_missile_attack18.png",
  "LAZY_LOAD_missile_attack19.png",
  "LAZY_LOAD_missile_attack20.png",
  "LAZY_LOAD_missile_attack21.png",
  "LAZY_LOAD_missile_attack22.png",
  "LAZY_LOAD_missile_attack23.png",
  "LAZY_LOAD_missile_attack24.png",
  "LAZY_LOAD_missile_flame.png",
  "LAZY_LOAD_missile_fly_line.png",
  "LAZY_LOAD_missile_head.png",
  "LAZY_LOAD_missile_hurt01.png",
  "LAZY_LOAD_missile_hurt02.png",
  "LAZY_LOAD_missile_hurt03.png",
  "LAZY_LOAD_missile_hurt04.png",
  "LAZY_LOAD_missile_hurt05.png",
  "LAZY_LOAD_missile_hurt06.png",
  "LAZY_LOAD_missile_hurt07.png",
  "LAZY_LOAD_missile_hurt08.png",
  "LAZY_LOAD_missile_light.png",
  "LAZY_LOAD_normal_attack_new00.png",
  "LAZY_LOAD_normal_attack_new02.png",
  "LAZY_LOAD_normal_attack_new04.png",
  "LAZY_LOAD_normal_attack_new06.png",
  "LAZY_LOAD_normal_attack_new08.png",
  "LAZY_LOAD_normal_attack_new10.png",
  "LAZY_LOAD_normal_attack_new12.png",
  "LAZY_LOAD_normal_attack_new14.png",
  "LAZY_LOAD_normal_attack_new16.png",
  "LAZY_LOAD_normal_attack_new18.png",
  "LAZY_LOAD_normal_attack_new20.png",
  "LAZY_LOAD_normal_attack_new22.png",
  "LAZY_LOAD_normal_attack_new24.png",
  "LAZY_LOAD_normal_new_attack00.png",
  "LAZY_LOAD_normal_new_attack02.png",
  "LAZY_LOAD_normal_new_attack04.png",
  "LAZY_LOAD_normal_new_attack06.png",
  "LAZY_LOAD_normal_new_attack08.png",
  "LAZY_LOAD_normal_new_attack10.png",
  "LAZY_LOAD_normal_new_attack12.png",
  "LAZY_LOAD_normal_new_attack14.png",
  "LAZY_LOAD_normal_new_attack16.png",
  "LAZY_LOAD_normal_new_attack18.png",
  "LAZY_LOAD_normal_new_attack20.png",
  "LAZY_LOAD_normal_new_attack22.png",
  "LAZY_LOAD_normal_new_attack24.png",
  "LAZY_LOAD_normal_new_hurt00.png",
  "LAZY_LOAD_normal_new_hurt01.png",
  "LAZY_LOAD_normal_new_hurt02.png",
  "LAZY_LOAD_normal_new_hurt03.png",
  "LAZY_LOAD_normal_new_hurt04.png",
  "LAZY_LOAD_normal_new_hurt05.png",
  "LAZY_LOAD_normal_new_hurt06.png",
  "LAZY_LOAD_normal_new_hurt07.png",
  "LAZY_LOAD_normal_new_hurt08.png",
  "LAZY_LOAD_grim_reap_attack1.png",
  "LAZY_LOAD_grim_reap_attack2.png",
  "LAZY_LOAD_grim_reap_attack3.png",
  "LAZY_LOAD_grim_reap_attack5.png",
  "LAZY_LOAD_grim_reap_attack6.png",
  "LAZY_LOAD_grim_reap_fly_00000.png",
  "LAZY_LOAD_grim_reap_fly_00002.png",
  "LAZY_LOAD_grim_reap_fly_00004.png",
  "LAZY_LOAD_grim_reap_fly_00006.png",
  "LAZY_LOAD_grim_reap_fly_00008.png",
  "LAZY_LOAD_grim_reap_fly_00010.png",
  "LAZY_LOAD_grim_reap_fly_00012.png",
  "LAZY_LOAD_grim_reap_fly_00014.png",
  "LAZY_LOAD_grim_reap_fly_00016.png",
  "LAZY_LOAD_grim_reap_fly_00018.png",
  "LAZY_LOAD_grim_reap_hurk2_00009.png",
  "LAZY_LOAD_grim_reap_hurt1.png",
  "LAZY_LOAD_grim_reap_hurt_00000.png",
  "LAZY_LOAD_grim_reap_hurt_00002.png",
  "LAZY_LOAD_grim_reap_hurt_00004.png",
  "LAZY_LOAD_grim_reap_hurt_00006.png",
  "LAZY_LOAD_grim_reap_hurt_00008.png",
  "LAZY_LOAD_grim_reap_hurt_00010.png",
  "LAZY_LOAD_grim_reap_hurt_00012.png",
  "LAZY_LOAD_grim_reap_hurt_00014.png"
}
