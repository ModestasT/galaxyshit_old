Webview = {}
function Webview:Show(html, x, y, w, h)
  assert(not self.m_visible)
  self.m_visible = true
  if ext.webview and ext.webview.CreateWebview then
    ext.webview.CreateWebview(html, x, y, w, h)
  end
end
function Webview:Hide()
  assert(self.m_visible)
  self.m_visible = false
  if ext.webview and ext.webview.DestroyWebview then
    ext.webview.DestroyWebview()
  end
end
function Webview:Open(url, x, y, w, h)
  assert(not self.m_visible)
  self.m_visible = true
  if ext.webview and ext.webview.OpenWebview then
    ext.webview.OpenWebview(url, x, y, w, h)
  end
end
function Webview:Destroy()
  assert(self.m_visible)
  self.m_visible = false
  if ext.webview and ext.webview.DestroyWebview then
    ext.webview.DestroyWebview()
  end
end
function Webview:OnGlobalScreenShow(b)
  if ext.webview and ext.webview.DestroyWebview then
    ext.webview.DestroyWebview(b)
  end
end
function Webview:OnRestartGame()
  if self.m_visible then
    self:Hide()
  end
end
function Webview:IsSupport()
  if ext.webview and ext.webview.OpenWebview and ext.webview.DestroyWebview then
    if AutoUpdate.isAndroidDevice then
      if ext.webview.IsSupportWebview then
        return true
      else
        return false
      end
    else
      return true
    end
  else
    return false
  end
end
function Webview:OpenViewTest()
end
