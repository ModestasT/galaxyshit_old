local GameWDAttkBox = LuaObjectManager:GetLuaObject("GameWDAttkBox")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameStateWD = GameStateManager.GameStateWD
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIWDInBattle = LuaObjectManager:GetLuaObject("GameUIWDInBattle")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
GameWDAttkBox.attkID = nil
GameWDAttkBox.isDenfence = false
GameWDAttkBox.isSelect = 0
function GameWDAttkBox:OnInitGame()
end
GameWDAttkBox.supplyItemCount = nil
GameWDAttkBox.supplyCount = nil
GameWDAttkBox.credit = nil
function GameWDAttkBox:RefreshSource()
  local resource = GameGlobalData:GetData("item_count")
  GameWDAttkBox.supplyItemCount = 0
  for i, v in ipairs(resource.items) do
    if v.item_id == 1031 then
      GameWDAttkBox.supplyItemCount = GameWDAttkBox.supplyItemCount + v.item_no
      break
    end
  end
  resource = GameGlobalData:GetData("resource")
  GameWDAttkBox.supplyCount = tonumber(GameUtils.numberConversion(resource.wd_supply))
  DebugOut("refresh wd source", GameWDAttkBox.supplyItemCount, GameWDAttkBox.supplyCount)
  DebugTable(resource)
end
GameWDAttkBox.tag = nil
GameWDAttkBox.tag1 = nil
GameWDAttkBox.num = nil
GameWDAttkBox.num1 = nil
function GameWDAttkBox:SetButtonText()
  local obj = self:GetFlashObject()
  if obj then
    local tag = 1
    local tag1 = 1
    local num = 0
    local num1 = 0
    local num2 = 0
    local credit = GameWDAttkBox.credit or 400
    if 1 <= GameWDAttkBox.supplyCount then
      tag = 1
      num = 1
      num2 = 4
    elseif 1 <= GameWDAttkBox.supplyItemCount then
      tag = 2
      num = 1
      num2 = 4
    else
      tag = 3
      num = credit / 4
      num2 = credit
    end
    if GameWDAttkBox.supplyCount >= 4 then
      tag1 = 1
      num1 = 4
    elseif 4 <= GameWDAttkBox.supplyItemCount then
      tag1 = 2
      num1 = 4
    else
      tag1 = 3
      num1 = credit
    end
    GameWDAttkBox.tag = tag
    GameWDAttkBox.tag1 = tag1
    GameWDAttkBox.num = num
    GameWDAttkBox.num1 = num1
    obj:InvokeASCallback("_root", "setButtonText", tag, tag, num, num2)
  end
end
function GameWDAttkBox:Init()
  local obj = self:GetFlashObject()
  DebugOut("defend level")
  if obj then
    GameWDAttkBox:RefreshSource()
    if not GameWDAttkBox.credit then
      local price_requestParam = {
        price_type = "wd_supply_resume_cost",
        type = 0
      }
      NetMessageMgr:SendMsg(NetAPIList.price_req.Code, price_requestParam, GameWDAttkBox.SetSkipPrice, true, nil)
    else
      GameWDAttkBox:SetButtonText()
    end
    local text_content = GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_SKIP_BATTLE_LABEL")
    obj:InvokeASCallback("_root", "setSkipPrice", text_content)
    local curLevel = GameVipDetailInfoPanel:GetVipLevel()
    local viplevellimit = GameDataAccessHelper:GetVIPLimit("wd_skip_battle")
    if curLevel < viplevellimit then
      GameWDAttkBox.isSelect = 0
    end
    obj:InvokeASCallback("_root", "setSkip", GameWDAttkBox.isSelect)
    if GameWDAttkBox.attkID == 1 then
      local percent = GameStateWD.OpponentAllianceDefenceInfo.hp / GameStateWD.OpponentAllianceDefenceInfo.hp_limit
      if GameStateWD.OpponentAllianceDefenceInfo.hp_limit == 0 then
        percent = 1
      end
      percent = math.floor(percent * 100)
      if percent <= 0 then
        percent = 1
      elseif percent > 100 then
        percent = 100
      end
      local desc = GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_DESC")
      local point = 100 - GameDataAccessHelper:GetAllianceDefendInfo(GameStateWD.OpponentAllianceDefenceInfo.level).point / 100 .. "%"
      desc = string.format(desc, point)
      obj:InvokeASCallback("_root", "setTitleAndDesc", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_TITLE"), GameStateWD.BothAllianceInfo.infoes[2].name, desc)
      obj:InvokeASCallback("_root", "setDefenceWall", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_TITLE"), GameStateWD.OpponentAllianceDefenceInfo.level, percent, "" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp) .. "/" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp_limit))
    elseif GameWDAttkBox.attkID == 2 then
      obj:InvokeASCallback("_root", "setTitleAndDesc", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_CORE_TITLE"), GameStateWD.BothAllianceInfo.infoes[2].name, GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_CORE_DESC"))
      obj:InvokeASCallback("_root", "setDefenceCore", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_CORE_TITLE"))
    elseif GameWDAttkBox.isDenfence then
      obj:InvokeASCallback("_root", "setTitleAndDesc", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_DEFENCER_TITLE"), GameStateWD.BothAllianceInfo.infoes[2].name, GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_DEFENCER_DESC"))
      local percent = GameStateWD.OpponentAllianceDefenceInfo.leader_hp / GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit
      percent = math.floor(percent * 100)
      if percent <= 0 then
        percent = 1
      elseif percent > 100 then
        percent = 100
      end
      local userinfo = GameStateWD:GetUserInfoByID(GameWDAttkBox.attkID)
      local battleInfo = GameStateWD:GetUserBattleInfoByID(GameWDAttkBox.attkID)
      local userMainFleet = {fleet_id = 1, levelup = 0}
      for k, v in pairs(userinfo.fleet_ids) do
        if v.fleet_id == 1 then
          userMainFleet = v
        end
      end
      DebugOut("WD Atk")
      DebugTable(userinfo)
      local userAvatar = GameDataAccessHelper:GetFleetAvatar(userinfo.main_fleet_id, userMainFleet.levelup, userinfo.sex)
      DebugOut("userAvatar = ", userAvatar)
      if 0 < battleInfo.attk_count then
        obj:InvokeASCallback("_root", "setDefenceLeader", GameUtils:GetUserDisplayName(userinfo.name), userinfo.level, userAvatar, userinfo.force, percent, "" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp .. "/" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit)
      else
        obj:InvokeASCallback("_root", "setDefenceLeader", GameUtils:GetUserDisplayName(userinfo.name), "?", userAvatar, "?", percent, "" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp .. "/" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit)
      end
    else
      local userinfo = GameStateWD:GetUserInfoByID(GameWDAttkBox.attkID)
      local battleInfo = GameStateWD:GetUserBattleInfoByID(GameWDAttkBox.attkID)
      obj:InvokeASCallback("_root", "setTitleAndDesc", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_MEMBER_TITLE"), GameStateWD.BothAllianceInfo.infoes[2].name, GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_MEMBER_DESC"))
      local userMainFleet = {fleet_id = 1, levelup = 0}
      for k, v in pairs(userinfo.fleet_ids) do
        if v.fleet_id == 1 then
          userMainFleet = v
        end
      end
      local userAvatar = GameDataAccessHelper:GetFleetAvatar(userinfo.main_fleet_id, userMainFleet.levelup, userinfo.sex)
      if 0 < battleInfo.attk_count then
        obj:InvokeASCallback("_root", "setMember", GameUtils:GetUserDisplayName(userinfo.name), userinfo.level, userAvatar, userinfo.force)
      else
        obj:InvokeASCallback("_root", "setMember", GameUtils:GetUserDisplayName(userinfo.name), "?", userAvatar, "?")
      end
    end
    if GameWDAttkBox.attkID == 2 then
      self:SetDefenceCoreAttackedNum(GameUIWDInBattle.BattleCoreLoseCount)
    end
  end
end
function GameWDAttkBox:RefreshDefenceHP()
  local obj = self:GetFlashObject()
  if obj then
    if GameWDAttkBox.isDenfence then
      local percent = GameStateWD.OpponentAllianceDefenceInfo.leader_hp / GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit
      percent = math.floor(percent * 100)
      if percent <= 0 then
        percent = 1
      elseif percent > 100 then
        percent = 100
      end
      obj:InvokeASCallback("_root", "setDefenceLeaderHP", percent, "" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp .. "/" .. GameStateWD.OpponentAllianceDefenceInfo.leader_hp_limit)
    elseif GameWDAttkBox.attkID == 1 then
      local percent = GameStateWD.OpponentAllianceDefenceInfo.hp / GameStateWD.OpponentAllianceDefenceInfo.hp_limit
      if GameStateWD.OpponentAllianceDefenceInfo.hp_limit == 0 then
        percent = 1
      end
      percent = math.floor(percent * 100)
      if percent <= 0 then
        percent = 1
      elseif percent > 100 then
        percent = 100
      end
      local desc = GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_DESC")
      local point = 100 - GameDataAccessHelper:GetAllianceDefendInfo(GameStateWD.OpponentAllianceDefenceInfo.level).point / 100 .. "%"
      desc = string.format(desc, point)
      obj:InvokeASCallback("_root", "setTitleAndDesc", GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_WALL_TITLE"), GameStateWD.BothAllianceInfo.infoes[2].name, desc)
      obj:InvokeASCallback("_root", "setDefenceWallHP", GameStateWD.OpponentAllianceDefenceInfo.level, percent, "" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp) .. "/" .. GameUtils.numberConversion(GameStateWD.OpponentAllianceDefenceInfo.hp_limit))
    end
  end
end
function GameWDAttkBox:RefreshTarget()
  local obj = self:GetFlashObject()
  if obj then
    DebugOut("GameWDAttkBox.isDenfence = ", GameWDAttkBox.isDenfence)
    if GameWDAttkBox.isDenfence then
      local userinfo = GameStateWD:GetUserInfoByID(GameWDAttkBox.attkID)
      local battleInfo = GameStateWD:GetUserBattleInfoByID(GameWDAttkBox.attkID)
      local userMainFleet = {fleet_id = 1, levelup = 0}
      for k, v in pairs(userinfo.fleet_ids) do
        if v.fleet_id == 1 then
          userMainFleet = v
        end
      end
      local userAvatar = GameDataAccessHelper:GetFleetAvatar(userinfo.main_fleet_id, userMainFleet.levelup, userinfo.sex)
      if 0 < battleInfo.attk_count then
        obj:InvokeASCallback("_root", "setDefenceLeaderTarget", GameUtils:GetUserDisplayName(userinfo.name), userinfo.level, userAvatar, userinfo.force)
      else
        obj:InvokeASCallback("_root", "setDefenceLeaderTarget", GameUtils:GetUserDisplayName(userinfo.name), "?", userAvatar, "?")
      end
    elseif GameWDAttkBox.attkID ~= 1 and GameWDAttkBox.attkID ~= 2 then
      assert(GameWDAttkBox.attkID ~= nil)
      local userinfo = GameStateWD:GetUserInfoByID(GameWDAttkBox.attkID)
      local battleInfo = GameStateWD:GetUserBattleInfoByID(GameWDAttkBox.attkID)
      local userMainFleet = {fleet_id = 1, levelup = 0}
      for k, v in pairs(userinfo.fleet_ids) do
        if v.fleet_id == 1 then
          userMainFleet = v
        end
      end
      local userAvatar = GameDataAccessHelper:GetFleetAvatar(userinfo.main_fleet_id, userMainFleet.levelup, userinfo.sex)
      if 0 < battleInfo.attk_count then
        obj:InvokeASCallback("_root", "setMember", GameUtils:GetUserDisplayName(userinfo.name), userinfo.level, userAvatar, userinfo.force)
      else
        obj:InvokeASCallback("_root", "setMember", GameUtils:GetUserDisplayName(userinfo.name), "?", userAvatar, "?")
      end
    end
  end
end
function GameWDAttkBox.SetSkipPrice(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.price_req.Code then
      GameWDAttkBox.credit = 1200
      GameWDAttkBox:SetButtonText()
      return true
    end
  elseif msgType == NetAPIList.price_ack.Code then
    local price = content.price
    GameWDAttkBox.credit = tonumber(price)
    GameWDAttkBox:SetButtonText()
    return true
  end
  return false
end
function GameWDAttkBox:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "initText", GameLoader:GetGameText("LC_MENU_Level"))
  self:Init()
end
function GameWDAttkBox:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameWDAttkBox:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:Update(dt)
  end
end
function GameWDAttkBox:OnFSCommand(cmd, arg)
  if cmd == "sattack" then
    local isSkip = false
    if tonumber(arg) == 1 then
      isSkip = true
    end
    self:RequestAttack(GameWDAttkBox.attkID, isSkip, true)
  elseif cmd == "skipPress" then
    local curLevel = GameVipDetailInfoPanel:GetVipLevel()
    local viplevellimit = GameDataAccessHelper:GetVIPLimit("wd_skip_battle")
    if curLevel < viplevellimit then
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_VIP_REWARDS_CANT_RECEIVE_DES_ALERT"))
    else
      self:GetFlashObject():InvokeASCallback("_root", "setSkipState")
      if GameWDAttkBox.isSelect == 0 then
        GameWDAttkBox.isSelect = 1
      else
        GameWDAttkBox.isSelect = 0
      end
    end
  elseif cmd == "attack" then
    local isSkip = false
    if tonumber(arg) == 1 then
      isSkip = true
    end
    self:RequestAttack(GameWDAttkBox.attkID, isSkip, false)
  elseif cmd == "moveout" then
    GameWDAttkBox.attkID = nil
    GameWDAttkBox.isDenfence = false
    GameStateWD:EraseObject(GameWDAttkBox)
  end
end
GameWDAttkBox.isSkip = nil
function GameWDAttkBox:RequestAttack(attkId, isSkip, isOnce)
  DebugOut("attkId", attkId)
  if tonumber(attkID) == 1 then
    isOnce = false
  elseif tonumber(attkID) == 2 then
    isOnce = false
  else
    if GameWDAttkBox.isDenfence then
    else
    end
  end
  local function callback()
    GameWDAttkBox.isSkip = isSkip
    local content = {
      id = tonumber(attkId),
      once = isOnce,
      leap = isSkip
    }
    NetMessageMgr:SendMsg(NetAPIList.domination_challenge_req.Code, content, GameWDAttkBox.AttackCallback, true, nil)
  end
  if not isOnce then
    if GameWDAttkBox.tag == 3 then
      local text_content = GameLoader:GetGameText("LC_MENU_WD_ATTACK_NO_SUPPLY_CONFIRM_ALERT")
      text_content = string.format(text_content, GameWDAttkBox.num, GameLoader:GetGameText("LC_MENU_LOOT_CREDIT"))
      GameUtils:CreditCostConfirm(text_content, callback)
    else
      callback()
    end
  elseif GameWDAttkBox.tag1 == 3 then
    DebugOut("attk tag 3")
    local text_content = GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_HAZARD_ATTACK_NO_SUPPLY_CONFIRM_ALERT")
    text_content = string.format(text_content, GameWDAttkBox.num1, GameLoader:GetGameText("LC_MENU_LOOT_CREDIT"))
    GameUtils:CreditCostConfirm(text_content, callback)
  else
    DebugOut("attk tag 1")
    callback()
  end
end
function GameWDAttkBox.AttackCallback(msgType, content)
  DebugOut(msgType, "battle call back11111")
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.domination_challenge_req.Code then
    if content.Code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.domination_challenge_ack.Code then
    GameWDAttkBox:RefreshSource()
    GameWDAttkBox:SetButtonText()
    do
      local window_type
      local displayText = ""
      if content.result == 1 then
        displayText = GameLoader:GetGameText("LC_MENU_BATTLE_FIELD_BATTLE_RESULT_SUC_ALERT")
        displayText = string.format(displayText, content.rewards[1].number)
        window_type = "challenge_win"
        GameUIBattleResult:UpdateAwardItem("challenge_win", 1, "wd_point", content.rewards[1].number)
        GameUIBattleResult:UpdateAwardItem("challenge_win", 2, -1, -1)
      else
        displayText = GameLoader:GetGameText("LC_MENU_BATTLE_FIELD_BATTLE_RESULT_FAIL_ALERT")
        displayText = string.format(displayText, content.rewards[1].number)
        window_type = "challenge_lose"
      end
      if not GameWDAttkBox.isSkip then
        if content.reports and content.reports[1] then
          GameUIBattleResult:SetFightReport(content.reports[1], nil, nil)
          do
            local lastGameState = GameStateManager:GetCurrentGameState()
            GameStateBattlePlay.curBattleType = "WD"
            GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.reports[1])
            GameStateBattlePlay:RegisterOverCallback(function()
              GameStateManager:SetCurrentGameState(lastGameState)
              GameUIBattleResult:LoadFlashObject()
              GameUIBattleResult:AnimationMoveIn(window_type, false)
              if content.id == 1 or content.id == 2 then
                GameUIBattleResult:SetReplayBtnState(false)
              else
                GameUIBattleResult:SetReplayBtnState(true)
              end
              lastGameState:AddObject(GameUIBattleResult)
            end)
            GameStateManager:SetCurrentGameState(GameStateBattlePlay)
          end
        else
          GameUIBattleResult:LoadFlashObject()
          GameUIBattleResult:AnimationMoveIn(window_type, false)
          GameStateWD:AddObject(GameUIBattleResult)
          GameUIBattleResult:SetReplayBtnState(false)
        end
      else
        GameUtils:ShowTips(displayText)
      end
      return true
    end
  end
  return false
end
function GameWDAttkBox:SetDefenceCoreAttackedNum(attackedNum)
  local obj = self:GetFlashObject()
  if obj then
    local displayText = GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_CORE_ATTACKED_TIMES_TEXT")
    displayText = string.format(displayText, attackedNum)
    obj:InvokeASCallback("_root", "setDefenceCoreAttackedNum", displayText)
  end
end
