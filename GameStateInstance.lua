local GameStateInstance = GameStateManager.GameStateInstance
local GameObjectClimbTower = LuaObjectManager:GetLuaObject("GameObjectClimbTower")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local currentBGMap = 0
function GameStateInstance:InitGameState()
  DebugOut("GameStateInstance:InitGameState")
  GameStateInstance.category = GameUIActivityNew.category.TYPE_DUNGEON
  GameStateInstance.currentInstanceId = 1
end
function GameStateInstance:OnFocusGain(previousState)
  DebugOut("GameStateInstance:OnFocusGain", GameStateInstance.category, GameStateInstance.currentInstanceId)
  GameStateInstance:AddObject(GameObjectBattleMapBG)
  GameStateInstance:LoadBackGround()
  if previousState ~= GameStateManager.GameStateFormation and previousState ~= GameStateManager.GameStateBattlePlay and previousState ~= GameStateManager.GameStateStore then
    self.m_preState = previousState
  end
  GameObjectClimbTower.currentInstanceId = GameStateInstance.currentInstanceId
  GameObjectClimbTower.category = GameStateInstance.category
  self:AddObject(GameObjectClimbTower)
end
function GameStateInstance:OnFocusLost(newState)
  DebugOut("GameStateInstance:OnFocusLost")
  self:EraseObject(GameObjectBattleMapBG)
  self:EraseObject(GameObjectClimbTower)
  GameStateInstance.m_IsActive = false
  currentBGMap = 0
end
function GameStateInstance:EnterInstance(ladderInfo)
  DebugOut("GameStateInstance:EnterInstance")
  GameObjectClimbTower:setRecoverData(ladderInfo)
  GameStateManager:SetCurrentGameState(self)
  self:ForceCompleteCammandList()
end
function GameStateInstance:EnterInstance2(ladderInfo)
  DebugOut("GameStateInstance:EnterInstance")
  GameObjectClimbTower:setRecoverData(ladderInfo)
  GameStateManager:SetCurrentGameState(self)
end
function GameStateInstance:LoadBackGround()
  DebugOut("LoadBackGround", GameStateInstance.category)
  if GameStateInstance.category == GameUIActivityNew.category.TYPE_DUNGEON then
    if currentBGMap ~= GameStateInstance.category then
      currentBGMap = GameStateInstance.category
      GameObjectBattleMapBG:LoadBGMap(1, 1)
    end
  elseif GameStateInstance.category == GameUIActivityNew.category.TYPE_WORM_HOLE then
    if currentBGMap ~= GameStateInstance.category then
      currentBGMap = GameStateInstance.category
      GameObjectBattleMapBG:LoadBGMap(4, 7)
    end
  elseif GameStateInstance.category == GameUIActivityNew.category.TYPE_EXPEDITION then
    if currentBGMap ~= GameStateInstance.category then
      currentBGMap = GameStateInstance.category
      GameObjectBattleMapBG:LoadBGMap(8, 5)
    end
  elseif GameStateInstance.category == GameUIActivityNew.category.TYPE_SPEEDUP and currentBGMap ~= GameStateInstance.category then
    currentBGMap = GameStateInstance.category
    GameObjectBattleMapBG:LoadBGMap(1, 1)
  end
end
function GameStateInstance:TryToRecoverLadderData()
  if GameObjectClimbTower.currentInstanceId ~= 0 and GameDataAccessHelper.LadderCurrentStepInfo ~= nil then
    GameObjectClimbTower:setRecoverData(GameDataAccessHelper.LadderCurrentStepInfo)
  end
end
