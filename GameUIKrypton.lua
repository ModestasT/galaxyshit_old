local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameStateKrypton = GameStateManager.GameStateKrypton
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameFleetInfoBackground = LuaObjectManager:GetLuaObject("GameFleetInfoBackground")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialEquipKrypton = TutorialQuestManager.QuestTutorialEquipKrypton
local QuestTutorialEnhanceKrypton = TutorialQuestManager.QuestTutorialEnhanceKrypton
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
require("FleetMatrix.tfl")
GameUIKrypton.KryptonAutoDecomposeLevel = 1
GameUIKrypton.KryptonAutoDecomposeLevelMax = 5
GameUIKrypton.KryptonAutoDecomposeLevelMin = 1
GameUIKrypton.force = {}
GameUIKrypton.inmultiselectedmode = false
GameUIKrypton.multiselectedSlot = {}
function GameUIKrypton:OnInitGame()
  self.initFleetIndex = 1
  self.equipOffDestSlot = -1
  self.equipOffSrcSlot = -1
  self.equipDestSlot = -1
  self.equipSrcSlot = -1
  self.tab = 1
  self.initDecomposeBar = false
end
function GameUIKrypton.registerDatacallback()
  GameGlobalData:RegisterDataChangeCallback("fleetkryptons", GameUIKrypton.RefreshKrypton)
  GameGlobalData:RegisterDataChangeCallback("resource", GameUIKrypton.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameUIKrypton.UpdateForce)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameUIKrypton.OnGlobalLevelChange)
end
function GameUIKrypton.removeDatacallback()
  GameGlobalData:RemoveDataChangeCallback("resource", GameUIKrypton.RefreshResource)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", GameUIKrypton.UpdateForce)
  GameGlobalData:RemoveDataChangeCallback("levelinfo", GameUIKrypton.OnGlobalLevelChange)
  GameGlobalData:RemoveDataChangeCallback("fleetkryptons", GameUIKrypton.RefreshKrypton)
  FleetMatrix:GetMatrixsReq()
end
function GameUIKrypton.RefreshKrypton()
  if GameUIKrypton:GetFlashObject() then
    DebugOut("Refresh update server krypton")
    GameUIKrypton:GetFleetKrypton()
    GameUIKrypton:RequestKryptonInBag()
  end
end
function GameUIKrypton:CheckInitGameData()
  if not self.kryptonSlotLevel then
    self.kryptonSlotLevel = GameDataAccessHelper:GetKryptonUnLockSlot()
  end
end
function GameUIKrypton.RefreshResource()
  DebugOut("GameUIKrypton.RefreshResource")
  if not GameUIKrypton.requestComposeing then
    DebugOut("GameUIKrypton.RefreshResource 2222")
    if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIKrypton) then
      local resource = GameGlobalData:GetData("resource")
      GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "RefreshResource", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit), GameUtils.numberConversion(resource.kenergy))
    end
  end
end
function GameUIKrypton.RefreshFleets()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIKrypton) then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    GameUIKrypton.currentSlectedIndex = GameUIKrypton.currentSlectedIndex or 1
    GameUIKrypton:UpdateSelectedFleetInfo(GameUIKrypton.currentSlectedIndex)
  end
end
function GameUIKrypton:SetInitFleet(index)
  self.initFleetIndex = index
end
function GameUIKrypton:Init()
  self.autoDecompose = false
  self.batchGacha = false
  self.requestComposeing = false
  self.RefreshResource()
  self.initDecomposeBar = false
  self.inmultiselectedmode = false
  GameUIKrypton.mCurSortType = 1
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local isVip = curLevel > 4
  local force = GameLoader:GetGameText("LC_MENU_FRIEND_BATTLE")
  self:GetFlashObject():InvokeASCallback("_root", "setTab", GameStateKrypton.currentTab, isVip, force)
  local AutoComposeVisible = self:CheckIsShowAutoCompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoComposeVisible", AutoComposeVisible)
  self:UpdateAutoCompose()
  local AutoDecomposeVisible = curLevel >= GameDataAccessHelper:GetVIPLimit("krypton_auto_decompose")
  self:GetFlashObject():InvokeASCallback("_root", "setAutoDecomposeVisible", AutoDecomposeVisible)
  self:UpdateAutoDecompose()
  self:setKryptonGameText()
  self:RequestMartixs()
  local energyLevelUp = GameLoader:GetGameText("LC_MENU_KRYPTON_ENERGY_LEVEL_UP")
  local freeNext = GameLoader:GetGameText("LC_MENU_FREE_KRYPTON_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "setEnergLevelUp", energyLevelUp)
  self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
  local info = GameLoader:GetGameText("LC_HELP_KRYPTON_INFO_CHAR")
  local txtBatchDecompose = GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE")
  DebugOut("krypton help", info)
  self:GetFlashObject():InvokeASCallback("_root", "setKryptonHelp", info, txtBatchDecompose)
  self:UpdateAutoComposeLvBar()
end
function GameUIKrypton:RequestMartixs()
  DebugOut("FleetMatrix.Matrix_Type GameUIKrypton:" .. FleetMatrix.Matrix_Type)
  FleetMatrix:GetMatrixsReq(GameUIKrypton.requestMultiMatrixCallBack, FleetMatrix.Matrix_Type)
end
function GameUIKrypton.requestMultiMatrixCallBack()
  local flash_obj = GameUIKrypton:GetFlashObject()
  if flash_obj then
    DebugOutPutTable(FleetMatrix.matrixs_in_as, "matrixs_in_as=")
    flash_obj:InvokeASCallback("_root", "initMartix", FleetMatrix.matrixs_in_as, FleetMatrix.system_index)
  end
end
function GameUIKrypton.OnUpdateMatrixFail()
end
function GameUIKrypton.OnUpdateMatrix()
  GameUIKrypton.force = {}
  GameUIKrypton.currentSlectedIndex = 1
  GameUIKrypton:RequestKryptonInBag()
  GameUIKrypton:UpdateSelectedFleetInfo(GameUIKrypton.currentSlectedIndex)
end
function GameUIKrypton:CheckIsShowAutoCompose(...)
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local limitLevel = GameDataAccessHelper:GetLevelLimit("krypton_auto_compose")
  local limitVip = GameDataAccessHelper:GetVIPLimit("krypton_auto_compose")
  if curLevel >= limitVip or curPlayerLevel >= limitLevel then
    return true
  else
    return false
  end
end
function GameUIKrypton:UpdateAutoCompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoComposeEnabled", self.batchGacha)
end
function GameUIKrypton:UpdateAutoDecompose()
  self:GetFlashObject():InvokeASCallback("_root", "setAutoDecomposeEnabled", self.autoDecompose)
end
function GameUIKrypton:Clear()
  self.initFleetIndex = 1
  self.equipmentShown = false
end
function GameUIKrypton:ClearHook()
  self.initFleetIndex = 1
  self.equipmentShown = false
end
function GameUIKrypton:setKryptonGameText()
  local kryptonPointStr = GameLoader:GetGameText("LC_MENU_KRYPTON_POINT")
  local kryptonLbLockedStr = GameLoader:GetGameText("LC_MENU_LOCKED_CHAR")
  self:GetFlashObject():InvokeASCallback("_root", "setKryptonGameText", kryptonPointStr, kryptonLbLockedStr)
end
function GameUIKrypton:TryQueryKryptonDetail(kryptonID, kryptonLevel, callback)
  DebugOut("query krypton detail req ", kryptonID)
  local detail = GameGlobalData:GetKryptonDetail(kryptonID, kryptonLevel)
  if detail == nil then
    local req_content = {krypton_id = kryptonID, level = kryptonLevel}
    if callback == nil then
      callback = GameUIKrypton.QueryKryptonDetailCallBack
    end
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_info_req.Code, req_content, callback, true, nil)
  else
    DebugOut("got local krypton detail")
  end
  return detail
end
function GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
  DebugOut("krypton info call back")
  DebugOut(msgType)
  DebugTable(conteng)
  if msgType == NetAPIList.krypton_info_ack.Code then
    GameWaiting:HideLoadingScreen()
    GameGlobalData:SetKryptonDetail(tonumber(content.krypton.id), tonumber(content.krypton.level), content.krypton)
    return true
  end
  return false
end
function GameUIKrypton.OnGlobalLevelChange()
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if levelInfo and tonumber(levelInfo.level) > 29 and not QuestTutorialBuildKrypton:IsActive() and not QuestTutorialBuildKrypton:IsFinished() then
    local krypton = GameGlobalData:GetBuildingInfo("krypton_center")
    if krypton.level > 0 then
      QuestTutorialBuildKrypton:SetFinish(true)
      local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
      local ishavekrypton = false
      for k, v in pairs(fleetkryptons) do
        if v.kryptons and 0 < #v.kryptons then
          ishavekrypton = true
        end
      end
      if ishavekrypton then
        QuestTutorialUseKrypton:SetFinish(true)
      end
    else
      QuestTutorialBuildKrypton:SetActive(true)
    end
  end
end
function GameUIKrypton:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:Init()
  GameUIKrypton.currentSlectedIndex = 1
  self:InitCurrentFleet()
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialKryptonTab")
  self:GetFlashObject():InvokeASCallback("_root", "HideTutorialLabTab")
  if QuestTutorialUseKrypton:IsActive() then
    if GameStateKrypton.currentTab == GameStateKrypton.KRYPTON then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialLabTab")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialKryptonTab")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      if not GameUtils:GetTutorialHelp() then
        GameUICommonDialog:PlayStory({100033}, callback)
      end
    elseif GameStateKrypton.currentTab == GameStateKrypton.LAB then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialBtnAction")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialLabTab")
      if not GameUtils:GetTutorialHelp() then
        GameUICommonDialog:PlayStory({100035}, callback)
      end
    elseif GameStateKrypton.currentTab == GameStateKrypton.ADV_LAB then
      local function callback()
        self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAdvBtnAction")
      end
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialAdvLabTab")
    end
  elseif QuestTutorialEquipKrypton:IsActive() then
    local function callback()
      self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEquipKrypton")
    end
    GameUICommonDialog:PlayStory({100037}, callback)
  end
  GameUIKrypton:CheckKryptonRefineEnterBtnVisible()
  if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
    GameUIKrypton:GetHelpTutorialPos()
  end
end
function GameUIKrypton:OnEraseFromGameState()
  self:ClearHook()
  self:UnloadFlashObject()
  FleetMatrix.Matrix_Type = 0
  for i = 1, 9 do
    local temp = "tempfleetforce" .. i
    if self[temp] ~= nil then
      DebugOut("clear ", temp)
      self[temp] = nil
    end
  end
end
function GameUIKrypton:RequestKryptonInBag()
  local function netFailedCallback()
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, GameUIKrypton.DownloadKryptonBag, true, netFailedCallback)
  end
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, GameUIKrypton.DownloadKryptonBag, true, netFailedCallback)
end
function GameUIKrypton:UpdateSelectedFleetInfo(fleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOut("!!!fleetIndex = ", fleetIndex)
  DebugOut("fleet Table")
  DebugTable(fleets)
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if fleetIndex > 0 and fleetIndex <= maxFleetIndex then
      if self:GetFlashObject() ~= nil then
        self:GetFlashObject():InvokeASCallback("_root", "setPage", fleetIndex, maxFleetIndex)
      end
      local content = fleets[fleetIndex]
      GameFleetInfoBackground.currentIndex = fleetIndex
      GameFleetInfoBackground:RefreshCurrentFleet()
      GameFleetInfoBackground:FleetAppear()
      self.currentSlectedIndex = fleetIndex
      GameUIKrypton:ShowEquipMent()
      GameUIKrypton:GetFleetKrypton()
    end
  end
end
function GameUIKrypton:GetKryptonType(item)
  if item.item_type and item.rank then
    local _type = 50000 + item.item_type * 100 + item.rank
    return _type
  end
  return -1
end
function GameUIKrypton:Update(dt)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateGrid", dt)
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    if self.dragBagKrypton then
      self:UpdataFleetSlotState(self.onSelectBagSlot, false)
    end
  end
end
function GameUIKrypton:RefreshBagKrypton()
  local cnt = math.ceil(GameUIKrypton.grid_capacity / 4)
  if cnt > 0 then
    for k = 1, cnt do
      GameUIKrypton:UpdateBagListItem(k)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonCount", self:GetKryptonCount(), GameUIKrypton.grid_capacity, string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_8"), "<number1>", tostring(GameUIKrypton.grid_capacity)))
  end
end
function GameUIKrypton:GetKryptonCount()
  local count = 0
  for k, v in pairs(GameUIKrypton.kryptonBag) do
    if v then
      count = count + 1
    end
  end
  return count
end
function GameUIKrypton:GetBagSlot()
  local slot = 0
  for i = 1, GameUIKrypton.grid_capacity do
    local item = self.kryptonBag[i]
    if not item then
      slot = i
      return slot
    end
  end
  return slot
end
function GameUIKrypton:ShowEquipMent()
  if not self.equipmentShown then
    self.equipmentShown = true
    self:GetFlashObject():InvokeASCallback("_root", "showEquipment")
  end
end
function GameUIKrypton:HideEquipMent()
  if self.equipmentShown then
    self.equipmentShown = false
    self:GetFlashObject():InvokeASCallback("_root", "hideEquipment")
  end
end
function GameUIKrypton:GetFleetKrypton()
  DebugOut("GetFleetKrypton = " .. self.currentSlectedIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(fleets, "fleets = ")
  local fleet = fleets[self.currentSlectedIndex]
  if not fleet then
    return
  end
  local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
  DebugOutPutTable(fleetkryptons, "fleetkryptons ")
  local fleetkrypton = LuaUtils:table_values(LuaUtils:table_filter(fleetkryptons, function(k, v)
    return v.fleet_identity == fleet.identity
  end))[1]
  if fleetkrypton then
    GameUIKrypton.currentFleetKrypton = fleetkrypton.kryptons
  else
    GameUIKrypton.currentFleetKrypton = {}
    DebugOut("select fleet's kryptons is nil", self.currentSlectedIndex)
  end
  DebugOutPutTable(GameUIKrypton.currentFleetKrypton, "GetFleetKrypton")
  GameUIKrypton.currentFleetKrypton = GameUIKrypton.currentFleetKrypton or {}
  GameUIKrypton.currentFleetKrypton = GameUIKrypton:ProcessMatrixInfo(GameUIKrypton.currentFleetKrypton)
  GameUIKrypton:RefreshCurFleetKrypton()
  GameUIKrypton:RefreshChargeKrypton()
end
function GameUIKrypton:InitCurrentFleet()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local fleet = fleets[self.currentSlectedIndex]
  local fleet_name = GameDataAccessHelper:GetFleetLevelDisplayName(fleet.identity, fleet.level)
  DebugOut("!!!!!!!!!!!!!fleet_name = ", fleet_name)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleet.identity, fleet.level)
  local vesselsType = GameDataAccessHelper:GetCommanderVesselsType(fleet.identity, fleet.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleet.identity, fleet.level)
  if leaderlist and curMatrixIndex and fleet.identity == 1 then
    local tempid = leaderlist.leader_ids[curMatrixIndex]
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, fleet.level)
    vesselsType = GameDataAccessHelper:GetCommanderVesselsType(tempid, fleet.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, fleet.level)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFleetKrypton", GameUtils.numberConversion(fleet.krypton), GameUtils.numberConversion(fleet.force), fleet_name, iconFrame, tonumber(fleet.identity), vesselsType, fleetColor)
end
function GameUIKrypton:RefreshCurFleetKrypton()
  self:CheckInitGameData()
  local frame, lockLevel, kryptonLV, canEvalote = "", "", "", ""
  if not self.currentFleetKrypton then
    return
  end
  local building = GameGlobalData:GetBuildingInfo(GameStateKrypton.buildingName)
  local totalKrypton = 0
  DebugOutPutTable(self.currentFleetKrypton, "self.currentFleetKrypton")
  local resource = GameGlobalData:GetData("resource")
  for i = 1, 8 do
    local item = self.currentFleetKrypton[i]
    if item then
      local id = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = frame .. "item_" .. id .. "\001"
        else
          frame = frame .. "temp" .. "\001"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = frame .. "item_" .. id .. "\001"
      end
      totalKrypton = item.decompose_kenergy
      kryptonLV = kryptonLV .. GameLoader:GetGameText("LC_MENU_Level") .. item.level .. "\001"
      if resource and resource.kenergy >= item.kenergy_upgrade and not item.max_level then
        canEvalote = canEvalote .. "true" .. "\001"
      else
        canEvalote = canEvalote .. "false" .. "\001"
      end
    else
      frame = frame .. "empty" .. "\001"
      kryptonLV = kryptonLV .. "no" .. "\001"
      canEvalote = canEvalote .. "false" .. "\001"
    end
  end
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= building.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshEquipment", frame, lockLevel, totalKrypton, kryptonLV, GameLoader:GetGameText("LC_MENU_Level"))
  self:GetFlashObject():InvokeASCallback("_root", "SetEquipArrows", canEvalote)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.currentSlectedIndex]
  local fleet_name = GameDataAccessHelper:GetFleetLevelDisplayName(fleet.identity, fleet.level)
  DebugOut("!!!!!!!!!!!!!fleet_name = ", fleet_name)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleet.identity, fleet.level)
  local vesselsType = GameDataAccessHelper:GetCommanderVesselsType(fleet.identity, fleet.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleet.identity, fleet.level)
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if leaderlist and curMatrixIndex and fleet.identity == 1 then
    local tempid = leaderlist.leader_ids[curMatrixIndex]
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, fleet.level)
    vesselsType = GameDataAccessHelper:GetCommanderVesselsType(tempid, fleet.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, fleet.level)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setFleetKrypton", GameUtils.numberConversion(fleet.krypton), GameUtils.numberConversion(fleet.force), fleet_name, iconFrame, tonumber(fleet.identity), vesselsType, fleetColor)
  local temp = "tempfleetforce" .. fleet.identity
  if not self.force[temp] then
    self.force[temp] = fleet.force
  end
  local forceChangeNum = tonumber(fleet.force - self.force[temp])
  self.force[temp] = fleet.force
  local forceChangeNumtemp = GameUtils.numberConversion(math.abs(forceChangeNum))
  self:GetFlashObject():InvokeASCallback("_root", "showForceChangeAnimation", forceChangeNum, forceChangeNumtemp)
end
function GameUIKrypton.UpdateForce()
  if GameUIKrypton:GetFlashObject() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[GameUIKrypton.currentSlectedIndex]
    DebugOut("fleet.force:" .. fleet.force .. "," .. GameUIKrypton.currentSlectedIndex)
    DebugTable(GameUIKrypton.force)
    local temp = "tempfleetforce" .. fleet.identity
    if not GameUIKrypton.force[temp] then
      GameUIKrypton.force[temp] = fleet.force
    end
    local forceChangeNum = tonumber(fleet.force - GameUIKrypton.force[temp])
    GameUIKrypton.force[temp] = fleet.force
    local forceChangeNumtemp = GameUtils.numberConversion(math.abs(forceChangeNum))
    GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "showForceChangeAnimation", forceChangeNum, forceChangeNumtemp)
    GameUIKrypton.RefreshKrypton()
  end
end
function GameUIKrypton:SetCurrentChargeKrypton(kryptonId)
  self.currentChargeItemId = kryptonId
  self:RefreshChargeKrypton()
end
function GameUIKrypton:EquipOffKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if not fleets or not fleets[self.currentSlectedIndex] then
    return
  end
  local identity = fleets[self.currentSlectedIndex].identity
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for tk, tv in pairs(v.formation) do
        if tv.formation_id == FleetMatrix.system_index then
          self.equipOffSrcSlot = tv.slot
        end
      end
    end
  end
  self.equipOffDestSlot = destSlot
  DebugOut("EquipOffKrypton: ", identity, kryptonId, self.equipOffDestSlot, self.equipOffSrcSlot)
  local krypton_equip_off_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipOffDestSlot
  }
  local function netFailedCallback()
    GameStateKrypton:onEndDragItem()
    GameUIKrypton:RequestKryptonInBag()
  end
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_equip_off_req.Code, krypton_equip_off_req_param, GameUIKrypton.kryptonEquipCallback, true, netFailedCallback)
end
function GameUIKrypton.UseKrypton()
  GameUIKrypton:CheckInitGameData()
  local destSlot = 0
  local building = GameGlobalData:GetBuildingInfo(GameStateKrypton.buildingName)
  local item = GameUIKrypton.kryptonBag[GameUIKrypton.onSelectBagSlot]
  local state = GameUIKrypton:GetCurrentKryptonInFleetKryptonState(item)
  if state == 0 then
    for i = 1, 8 do
      if GameUIKrypton.kryptonSlotLevel[i].labLevel <= building.level and not GameUIKrypton.currentFleetKrypton[i] then
        destSlot = i
        break
      end
    end
  end
  if destSlot == 0 then
    DebugOut("state == " .. state)
    for i = 8, 1, -1 do
      if state == 1 then
        if GameUIKrypton.currentFleetKrypton[i] and GameUIKrypton:GetTwoKrytponAttribute(item, GameUIKrypton.currentFleetKrypton[i]) then
          destSlot = i
          DebugOut("destSlot = " .. destSlot)
          break
        end
      elseif GameUIKrypton.kryptonSlotLevel[i].labLevel <= building.level then
        destSlot = i
      end
    end
  end
  if destSlot > 0 then
    GameUIKrypton:EquipKrypton(GameUIKrypton.decomposeId, destSlot)
  end
end
function GameUIKrypton:EquipKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.currentSlectedIndex].identity
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.equipSrcSlot = v.slot
    end
  end
  self.equipDestSlot = destSlot
  DebugOut("EquipKrypton: ", self.equipSrcSlot, " ", self.equipDestSlot)
  local krypton_equip_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipDestSlot
  }
  local function netFailedCallback()
    GameStateKrypton:onEndDragItem()
    GameUIKrypton:RequestKryptonInBag()
  end
  DebugTable(krypton_equip_req_param)
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_equip_req.Code, krypton_equip_req_param, GameUIKrypton.kryptonEquipCallback, true, netFailedCallback)
end
function GameUIKrypton:SendMsgWithMatrix(msgType, param, callback, block, failCallback)
  DebugOut("SendMsgWithMatrix:" .. msgType)
  param = param or {}
  local isIndex = false
  for k, v in pairs(param) do
    if k == "formation_id" then
      isIndex = true
      break
    end
  end
  if not isIndex then
    param.formation_id = FleetMatrix.system_index or 1
  end
  NetMessageMgr:SendMsg(msgType, param, callback, block, failCallback)
  if msgType == NetAPIList.krypton_gacha_one_req.Code then
    GameUtils:RecordForTongdui(msgType, param, callback, block, failCallback)
    local function callback()
      if GameUIKrypton:GetFlashObject() then
        GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      end
    end
    GameUtils:RecordForTongdui_onuser_cancel(msgType, callback)
    self.requestComposeing = false
  end
end
function GameUIKrypton:SwapFleetKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for vk, vv in pairs(v.formation) do
        if vv.formation_id == FleetMatrix.system_index then
          self.swapSrcSlot = vv.slot
        end
      end
    end
  end
  self.swapDestSlot = destSlot
  if self.swapSrcSlot == self.swapDestSlot then
    if GameUIKrypton.dragItemInstance then
      GameStateKrypton:onEndDragItem()
    end
    return
  end
  local destItem = self.currentFleetKrypton[destSlot]
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.currentSlectedIndex].identity
  local krypton_fleet_move_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    dest_slot = self.swapDestSlot
  }
  local function netFailedCallback()
    GameStateKrypton:onEndDragItem()
    GameUIKrypton:RequestKryptonInBag()
  end
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_fleet_move_req.Code, krypton_fleet_move_req_param, GameUIKrypton.swapFleetCallback, true, netFailedCallback)
end
function GameUIKrypton:SwapBagKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.swapSrcSlot = v.slot
    end
  end
  self.swapDestSlot = destSlot
  if self.swapSrcSlot == self.swapDestSlot then
    if GameUIKrypton.dragItemInstance then
      GameStateKrypton:onEndDragItem()
    end
    return
  end
  local destItem = self.kryptonBag[destSlot]
  DebugOut("destItem: ", destItem)
  local krypton_store_swap_req_param = {
    id_from = kryptonId,
    id_to = destItem and destItem.id or "",
    slot_to = destSlot
  }
  DebugOut("SwapBagKryptonSlot: ", self.swapSrcSlot, self.swapDestSlot)
  DebugOut("SwapBagKryptonPos: ", krypton_store_swap_req_param.id_from, krypton_store_swap_req_param.id_to, krypton_store_swap_req_param.slot_to)
  local function netFailedCallback()
    GameStateKrypton:onEndDragItem()
    GameUIKrypton:RequestKryptonInBag()
  end
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_store_swap_req.Code, krypton_store_swap_req_param, GameUIKrypton.swapBagCallback, true, netFailedCallback)
end
function GameUIKrypton:RefreshChargeKrypton()
  if self.currentChargeItemId then
    local item
    for k, v in pairs(self.kryptonBag) do
      if v.id == self.currentChargeItemId then
        item = v
      end
    end
    if item == nil then
      for k, v in pairs(self.currentFleetKrypton) do
        if v.id == self.currentChargeItemId then
          item = v
        end
      end
    end
    if item then
      if not item.kenergy_upgrade then
        item.kenergy_upgrade = 0
      end
      local frame = ""
      local id = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", frame, item.kenergy_upgrade)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", "empty", 0)
  end
end
function GameUIKrypton:BagItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local operateLeftText, operateLeftFunc
  local item = self.kryptonBag[slot]
  local operationTable = {}
  local number = 0
  if self.tab == 1 then
    operateLeftText = GameLoader:GetGameText("LC_MENU_USE_CHAR")
    operateLeftFunc = self.UseKrypton
    operationTable.btnUseVisible = true
    operationTable.btnUpgradeVisible = true
    number = #item.formation
  elseif self.tab == 2 or self.tab == 3 then
    operateLeftText = GameLoader:GetGameText("LC_MENU_DECOMPOSE_CHAR")
    operateLeftFunc = self.requestDecompose
    operationTable.btnDecomposeVisible = true
  end
  if item then
    GameUIKrypton.decomposeId = item.id
    ItemBox:SetKryptonBox(item, GameUIKrypton:GetKryptonType(item), number)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function GameUIKrypton:UpdataFleetSlotState(kryptonId, isEnd)
  local currentSelectKrypton = self.kryptonBag[kryptonId]
  if self.tab == 1 and currentSelectKrypton then
    self:CheckKryptonWithFleetKrypton(currentSelectKrypton, isEnd)
  end
end
function GameUIKrypton:CheckKryptonWithFleetKrypton(krypton, isEnd)
  self:CheckInitGameData()
  local kryptonLabInfo = GameGlobalData:GetBuildingInfo(GameStateKrypton.buildingName)
  local state = 0
  if not isEnd then
    state = self:GetCurrentKryptonInFleetKryptonState(krypton)
  end
  local allSlotState = self:GetFleetKryptonState(state, krypton)
  local lockLevel = ""
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= kryptonLabInfo.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  DebugOut("state = " .. state)
  DebugOut("allSlotState = " .. allSlotState)
  DebugOut("lockLevelText = " .. lockLevel)
  DebugOutPutTable(kryptonLabInfo, "kryptonLabInfo ")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipmentState", allSlotState, lockLevel, GameLoader:GetGameText("LC_MENU_Level"))
  end
end
function GameUIKrypton:GetFleetKryptonState(state, currentKrypton)
  local slotState = {}
  local slotCount = 8
  if state == 0 then
    for i = 1, slotCount do
      local _state = "no"
      table.insert(slotState, _state)
    end
  elseif state == 1 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "no"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  elseif state == 2 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "not"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  end
  return ...
end
function GameUIKrypton:GetCurrentKryptonInFleetKryptonState(currentKrypton)
  local currentkryptonAttributeCount = LuaUtils:table_size(currentKrypton.addon)
  local hasTheSameAttributeSlotCount = 0
  for _, v in pairs(self.currentFleetKrypton) do
    if self:GetTwoKrytponAttribute(currentKrypton, v) then
      hasTheSameAttributeSlotCount = hasTheSameAttributeSlotCount + 1
    end
  end
  return hasTheSameAttributeSlotCount
end
function GameUIKrypton:GetTwoKrytponAttribute(krypton1, krypton2)
  local krypton1AttributeCount = LuaUtils:table_size(krypton1.addon)
  local count = 0
  for _, v1 in pairs(krypton1.addon) do
    for _, v2 in pairs(krypton2.addon) do
      if v1.type == v2.type then
        count = count + 1
        break
      end
    end
  end
  return count > 0
end
function GameUIKrypton:FleetItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local item = self.currentFleetKrypton[slot]
  local operateText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
  local function operateFunc()
    DebugTable(item)
    if GameUIKrypton:GetBagSlot() > 0 then
      self:EquipOffKrypton(item.id, GameUIKrypton:GetBagSlot())
    else
      local tip = GameLoader:GetGameText("LC_ALERT_krypton_store_full")
      GameTip:Show(tip)
    end
  end
  GameUIKrypton.UnloadKrypton = operateFunc
  local operationTable = {}
  operationTable.btnUnloadVisible = true
  operationTable.btnUpgradeVisible = true
  if item then
    GameUIKrypton.decomposeId = item.id
    ItemBox:SetKryptonBox(item, GameUIKrypton:GetKryptonType(item), #item.formation)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function GameUIKrypton.requestDecompose()
  local item
  for k, v in pairs(GameUIKrypton.kryptonBag) do
    if v.id == GameUIKrypton.decomposeId then
      item = v
    end
  end
  if not item then
    for k, v in pairs(GameUIKrypton.currentFleetKrypton) do
      if v.id == GameUIKrypton.decomposeId then
        item = v
      end
    end
  end
  GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "decomposeSingleKrypton", item.slot)
  if #item.formation > 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIKrypton.DoRequestDecompose, {})
    local formations = ""
    for i, v in ipairs(item.formation) do
      formations = formations .. FleetMatrix.GetRomaNumber(v.formation_id) .. "/"
    end
    formations = string.sub(formations, 1, -2)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_ALERT_TITLE_DECOMPOSE"), GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_4") .. formations)
  elseif 1 < item.level or item.rank >= 6 then
    DebugOut("item.level: ", item.level)
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIKrypton.DoRequestDecompose, {})
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_ALERT_TITLE_DECOMPOSE"), GameLoader:GetGameText("LC_MENU_ALERT_DECOMPOSE"))
  else
    GameUIKrypton.DoRequestDecompose()
  end
end
function GameUIKrypton.DoRequestDecompose()
  local krypton_decompose_req_param = {
    krypton_type = 1,
    id = GameUIKrypton.decomposeId
  }
  if GameUIKrypton.currentChargeItemId == GameUIKrypton.decomposeId then
    GameUIKrypton.currentChargeItemId = nil
  end
  local function netFailedCallback()
    GameUIKrypton:RequestKryptonInBag()
  end
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_decompose_req.Code, krypton_decompose_req_param, GameUIKrypton.DecomposeCallback, true, netFailedCallback)
end
function GameUIKrypton:dragItem(cmd, arg)
  local param = LuaUtils:string_split(arg, "\001")
  local initPosX, initPosY, posX, posY = unpack(param)
  self.dragX = tonumber(posX)
  self.dragY = tonumber(posY)
  self.initPosX = tonumber(initPosX)
  self.initPosY = tonumber(initPosX)
  local pressIndex = -1
  if cmd == "dragFleetItem" then
    pressIndex = self.pressFleetSlot
  elseif cmd == "dragBagItem" then
    pressIndex = self.pressBagSlot
  end
  DebugOut("pressIndex: ", pressIndex)
  if pressIndex ~= -1 then
    local item
    if cmd == "dragFleetItem" then
      item = self.currentFleetKrypton[pressIndex]
    else
      item = self.kryptonBag[pressIndex]
    end
    DebugOut("item: ", item)
    if item then
      if cmd == "dragFleetItem" then
        self.dragBagItem = false
        self.dragFleetItem = true
      elseif cmd == "dragBagItem" then
        self.dragFleetItem = false
        self.dragBagItem = true
        self.dragBagKrypton = true
      end
      GameStateManager:GetCurrentGameState():BeginDragItem(item.id, GameUIKrypton:GetKryptonType(item), initPosX, initPosY, posX, posY)
    else
      GameUIKrypton:onEndDragItem()
    end
  end
end
function GameUIKrypton:onBeginDragItem(dragitem)
  self.dragItemInstance = dragitem
  DebugOut("onBeginDragItem: ", self.dragBagItem, self.dragFleetItem, self.pressBagSlot, self.pressFleetSlot)
  self:GetFlashObject():InvokeASCallback("_root", "onBeginDragItem", self.dragBagItem, self.dragFleetItem, self.pressBagSlot, self.pressFleetSlot)
end
function GameUIKrypton:onEndDragItem()
  self.dragBagKrypton = false
  self.dragBagItem = false
  self.dragFleetItem = false
  self.dragItemInstance = nil
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
  GameUIKrypton:RefreshBagKrypton()
  GameUIKrypton:RefreshCurFleetKrypton()
end
function GameUIKrypton.DoDecomposeAllAnimation()
  local frame = ""
  for i = 1, 12 do
    local item = GameUIKrypton.kryptonBag[i]
    if item and #item.formation == 0 and item.rank < 3 then
      frame = frame .. "play" .. "\001"
    else
      frame = frame .. "no" .. "\001"
    end
  end
  GameUIKrypton:DoDecomposeAll()
  GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "PlayDecomposeAnimation", frame)
  GameUIKrypton.DecomposeTag = false
end
function GameUIKrypton:DoDecomposeAll()
  local krypton_decompose_req_param = {
    krypton_type = 3,
    id = GameUIKrypton.decomposeId
  }
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_decompose_req.Code, krypton_decompose_req_param, GameUIKrypton.DecomposeCallback, true)
end
function GameUIKrypton:OnFSCommand(cmd, arg)
  if cmd == "closePress" then
    if GameStateKrypton.previousState == GameStateManager.GameStateInstance then
      GameStateManager.GameStateInstance:TryToRecoverLadderData()
    end
    GameStateManager:SetCurrentGameState(GameStateKrypton.previousState)
    if GameStateKrypton.battle_failed then
      GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
      local param = {cli_data = "krypton"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameStateKrypton.battle_failed = nil
    end
  end
  if cmd == "UpdateMarix" then
    local index = tonumber(arg)
    FleetMatrix:SwitchMatrixSystem(NetAPIList.switch_formation_req.Code, {formation_id = index}, index, self.OnUpdateMatrix, self.OnUpdateMatrixFail)
  end
  if cmd == "showFleetPopView" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    DebugOutPutTable(fleets, "fleets 2 = ")
    DebugOut("sdsdf = " .. GameFleetInfoBackground.currentIndex)
    local fleet = fleets[GameFleetInfoBackground.currentIndex]
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curmatrixIndex_m = GameGlobalData.GlobalData.curMatrixIndex
    if fleet.identity == 1 and leaderlist and curmatrixIndex_m then
      ItemBox:ShowCommanderDetail2(leaderlist.leader_ids[curmatrixIndex_m], fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
  end
  if cmd == "tabChanged" then
    self.tab = tonumber(arg)
    GameStateKrypton:EnterKrypton(self.tab)
    if QuestTutorialUseKrypton:IsActive() and self.tab == GameStateKrypton.LAB and not GameUtils:GetTutorialHelp() then
      GameUICommonDialog:PlayStory({100035}, nil)
    end
    if self.tab == 2 and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
      self:GetHelpTutorialCompositePos()
    end
  end
  if cmd == "beginBagPress" then
    self.pressBagSlot = tonumber(arg)
    self.onSelectBagSlot = tonumber(arg)
  elseif cmd == "endBagPress" then
    self.pressBagSlot = -1
  end
  if cmd == "beginFleetPress" then
    self.pressFleetSlot = tonumber(arg)
  elseif cmd == "endFleetPress" then
    self.pressFleetSlot = -1
  end
  if cmd == "BagKryptonItemReleased" then
    self:BagItemReleased(arg)
  elseif cmd == "FleetKryptonItemReleased" then
    self:FleetItemReleased(arg)
  end
  if cmd == "equipOffChargeKrypton" then
    self:SetCurrentChargeKrypton()
  end
  if cmd == "dragFleetItem" or cmd == "dragBagItem" then
    self:dragItem(cmd, arg)
  end
  if cmd == "drag_bag_item_start" then
    local param = LuaUtils:string_split(arg, "\001")
    local newArg = "" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
    self:dragItem("dragBagItem", newArg)
  end
  if cmd == "chargePress" then
    if not self.currentChargeItemId then
      return
    end
    local isInFleet, identity = false, -1
    for k, v in pairs(self.currentFleetKrypton) do
      if v.id == self.currentChargeItemId then
        isInFleet = true
      end
    end
    if isInFleet then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      identity = fleets[self.currentSlectedIndex].identity
    end
    local krypton_enhance_req_param = {
      id = self.currentChargeItemId,
      fleet_identity = identity
    }
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_enhance_req.Code, krypton_enhance_req_param, self.enhanceKryptonCallback, true, nil)
  end
  if cmd == "decomposeAll" then
    self.decomposeId = ""
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIKrypton.DoDecomposeAllAnimation, {})
    local tipText = GameLoader:GetGameText("LC_MENU_ALERT_DECOMPOSE_ALL")
    tipText = string.gsub(tipText, "<kryptonLevel_num>", GameUIKrypton.KryptonAutoDecomposeLevel)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_ALERT_TITLE_DECOMPOSE"), tipText)
  end
  if cmd == "batchdecomposestart" then
    GameUIKrypton.inmultiselectedmode = true
    local kryptonCount = #GameUIKrypton.kryptonBag
    self:GetFlashObject():InvokeASCallback("_root", "SetBagListItemShowChooseBox", kryptonCount, true, GameStateKrypton.currentTab)
    GameUIKrypton.multiselectedSlot = {}
  end
  if cmd == "batchdecomposerun" then
    if false == GameUIKrypton.inmultiselectedmode then
      return
    end
    local selectedCount = #GameUIKrypton.multiselectedSlot
    if selectedCount <= 0 then
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE_TIPS_1"))
      return
    end
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE_TIPS_2"), GameUIKrypton.CallBatchDecomposeKryptons, nil)
  end
  if cmd == "batchdecomposecancel" then
    GameUIKrypton.inmultiselectedmode = false
    local kryptonCount = #GameUIKrypton.kryptonBag
    self:GetFlashObject():InvokeASCallback("_root", "SetBagListItemShowChooseBox", kryptonCount, false, GameStateKrypton.currentTab)
    GameUIKrypton.multiselectedSlot = {}
  end
  if cmd == "choosBoxTouchRelease" then
    local selectedSlot = tonumber(arg)
    local findIndex = -1
    for i = 1, #GameUIKrypton.multiselectedSlot do
      if GameUIKrypton.multiselectedSlot[i] == selectedSlot then
        findIndex = i
        break
      end
    end
    if findIndex ~= -1 then
      table.remove(GameUIKrypton.multiselectedSlot, findIndex)
      self:GetFlashObject():InvokeASCallback("_root", "SetBagChooseItemStatusChange", selectedSlot, false)
    else
      table.insert(GameUIKrypton.multiselectedSlot, selectedSlot)
      self:GetFlashObject():InvokeASCallback("_root", "SetBagChooseItemStatusChange", selectedSlot, true)
    end
  end
  if cmd == "choosBoxItemClicked" then
    local param = LuaUtils:string_split(arg, "\001")
    local idx = (tonumber(param[1]) - 1) * 4 + tonumber(param[2])
    local newArg = tostring(idx) .. "\001" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
    GameUIKrypton:BagItemReleased(newArg)
  end
  if cmd == "actionKrypton" then
    self.animationAction = true
    GameUIKrypton:AfterAnimationRefreshBag()
  end
  if cmd == "requestCompose" and not self.requestComposeing then
    self.requestComposeing = true
    self:GetFlashObject():InvokeASCallback("_root", "showActionKryptonAnim")
    local decompose = self.autoDecompose and 1 or 0
    local krypton_gacha_req_param = {auto_decompose = decompose, use_credit = false}
    if self.batchGacha then
      GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_gacha_some_req.Code, krypton_gacha_req_param, GameUIKrypton.gachaCallback, true, nil)
    else
      GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_gacha_one_req.Code, krypton_gacha_req_param, GameUIKrypton.gachaCallback, true, nil)
    end
  end
  if cmd == "autoDecomposePress" then
    self.autoDecompose = not self.autoDecompose
    self:UpdateAutoDecompose()
    local info = GameLoader:GetGameText("LC_MENU_AUTO_DECOMPOSE_TIP")
    info = string.gsub(info, "<Level_num>", GameUIKrypton.KryptonAutoDecomposeLevel)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(info)
  end
  if cmd == "SetAutoDecomposeLevel" then
    DebugOut("SetAutoDecomposeLevel" .. arg)
    self:SetKryptonDecomposeLevel(tonumber(arg))
  end
  if cmd == "batchGachaPress" then
    if self:CheckIsShowAutoCompose() then
      self.batchGacha = not self.batchGacha
      self:UpdateAutoCompose()
      local info = GameLoader:GetGameText("LC_MENU_AUTO_FUSION_TIP")
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(info)
    else
      local limitVipLevel = GameDataAccessHelper:GetVIPLimit("krypton_auto_compose")
      local limitPlayerLevel = GameDataAccessHelper:GetLevelLimit("krypton_auto_compose")
      local showTips = GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_OR_VIP_INFO")
      showTips = string.gsub(showTips, "<playerLevel_num>", limitPlayerLevel)
      showTips = string.gsub(showTips, "<vipLevel_num>", limitVipLevel)
      GameUtils:ShowTips(showTips)
    end
  end
  if cmd == "leavlup_end" then
    GameUIKrypton:RefreshChargeKrypton()
    GameUIKrypton:RefreshBagKrypton()
    GameUIKrypton:RefreshCurFleetKrypton()
  end
  if cmd == "compose_credit" and not self.requestComposeing then
    local info = GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR")
    if GameSettingData.Save_Lang == "cn" or GameSettingData.Save_Lang == "zh" then
      info = GameLoader:GetGameText("LC_MENU_KRYPTON_EXCHANGE_CN_ALERT")
    end
    info = string.format(info, GameUIKrypton.adv_cost)
    if self.batchGacha and GameUIKrypton.adv_cost == 0 then
      if GameSettingData.Save_Lang == "cn" or GameSettingData.Save_Lang == "zh" then
        info = GameLoader:GetGameText("LC_MENU_KRYPTON_HIGH_AUTO_COMPOSITE_TIP_CN")
      else
        info = GameLoader:GetGameText("LC_MENU_KRYPTON_HIGH_AUTO_COMPOSITE_TIP")
      end
    end
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(function()
      self.requestComposeing = true
      self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", false)
      self:GetFlashObject():InvokeASCallback("_root", "showActionKryptonAnim")
      local decompose = self.autoDecompose and 1 or 0
      local content = {auto_decompose = decompose, use_credit = true}
      if self.batchGacha then
        GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_gacha_some_req.Code, content, GameUIKrypton.gachaCallback, true, nil)
      else
        GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_gacha_one_req.Code, content, GameUIKrypton.gachaCallback, true, nil)
      end
    end)
    GameUIMessageDialog:Display(text_title, info)
  end
  if cmd == "receiveReward" and not self.requestComposeing then
    self.requestComposeing = true
    self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", false)
    self:GetFlashObject():InvokeASCallback("_root", "showActionKryptonAnim")
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_energy_req.Code, nil, GameUIKrypton.gachaCallback, true, nil)
  end
  if cmd == "help_1" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_KRYPTON_INFO_CHAR_1"))
  end
  if cmd == "help_2" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_KRYPTON_INFO_CHAR_2"))
  end
  if cmd == "help_3" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_KRYPTON_INFO_CHAR_3"))
  end
  if cmd == "krypton_sort" then
    GameUIKrypton:SortKrypton()
  end
  if cmd == "ComposeKryptonButton_pos" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, "empty", GameUIMaskLayer.MaskStyle.small)
  end
  if cmd == "ComposeKryptonComposite_pos" and GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
    local param = LuaUtils:string_split(arg, "\001")
    GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_15"), GameUIMaskLayer.MaskStyle.small)
  end
  if cmd == "GotoKryptonMaster" and self.currentSlectedIndex then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.currentSlectedIndex]
    if fleet and fleet.id ~= "" then
      GameUIMaster.CurrentScope = {
        [1] = tonumber(fleet.id)
      }
      GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.KryptonMaster
      GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.krypton_enchance
      GameUIMaster.CurMatrixId = FleetMatrix.system_index
      GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    end
  end
  GameUIKrypton:RefineCmdHandle(cmd, arg)
end
function GameUIKrypton:CallBatchDecomposeKryptons()
  if false == GameUIKrypton.inmultiselectedmode then
    return
  end
  local req_params = {
    krypton_id = {}
  }
  local selectSlotCount = #GameUIKrypton.multiselectedSlot
  for i = 1, selectSlotCount do
    local slot = GameUIKrypton.multiselectedSlot[i]
    local item = GameUIKrypton.kryptonBag[slot]
    if nil ~= item then
      local kryptonId = item.id
      table.insert(req_params.krypton_id, kryptonId)
    end
  end
  NetMessageMgr:SendMsg(NetAPIList.krypton_decompose_batch_req.Code, req_params, GameUIKrypton.KrypTonBatchDecomposeCallBack, true, nil)
end
function GameUIKrypton.KrypTonBatchDecomposeCallBack(msgType, content)
  if msgType == NetAPIList.krypton_decompose_batch_ack.Code then
    GameUIKrypton:RequestKryptonInBag()
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.increase_kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    GameUIKrypton.inmultiselectedmode = false
    GameUIKrypton:SetBagList()
    GameUIKrypton.multiselectedSlot = {}
    GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "setToBatchSelectMode", GameStateKrypton.currentTab, false)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_decompose_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIKrypton:SetKryptonDecomposeLevel(level)
  DebugOut("GameUIKrypton:SetKryptonDecomposeLevel : " .. level)
  GameUIKrypton.KryptonAutoDecomposeLevel = level
  local msgPrams = {
    change_num = GameUIKrypton.KryptonAutoDecomposeLevel
  }
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.change_auto_decompose_req.Code, msgPrams, nil, false, nil)
  GameUIKrypton:UpdateAutoComposeLvBar()
end
function GameUIKrypton:testKrypton(v)
  v.formation = v.formation or {
    [1] = {
      formation_id = 1,
      fleet_id = 1,
      slot = 1
    },
    [2] = {
      formation_id = 2,
      fleet_id = 2,
      slot = 2
    }
  }
end
function GameUIKrypton:ProcessMatrixInfo(t)
  local r = {}
  for k, v in pairs(t) do
    for tk, tv in pairs(v.formation) do
      if tv.formation_id == FleetMatrix.system_index then
        r[tv.slot] = v
      end
    end
  end
  DebugTable(r)
  return r
end
function GameUIKrypton:ProcessBagInfo(t)
  local r = {}
  for k, v in pairs(t) do
    r[v.slot] = v
  end
  DebugTable(r)
  return r
end
function GameUIKrypton.DownloadKryptonBag(msgType, content)
  if msgType == NetAPIList.krypton_store_ack.Code then
    GameUIKrypton.grid_capacity = content.grid_capacity
    GameUIKrypton.adv_cost = content.gacha_need_credit
    GameUIKrypton.kryptonBag = content.kryptons
    GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
    GameUIKrypton:SetBagList()
    GameUIKrypton:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIKrypton.gachaCallback(msgType, content)
  if msgType == NetAPIList.krypton_gacha_one_ack.Code then
    GameUIKrypton.actionKryptonMsgType = msgType
    GameUIKrypton.actionKryptonContent = content
    GameUIKrypton.adv_cost = content.gacha_need_credit
    GameUIKrypton:AfterAnimationRefreshBag()
    if QuestTutorialUseKrypton:IsActive() and not GameUtils:GetTutorialHelp() then
      QuestTutorialUseKrypton:SetFinish(true)
      QuestTutorialEquipKrypton:SetActive(true)
      local function callback()
        GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonTab")
      end
      GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      GameUICommonDialog:PlayStory({100036}, callback)
    end
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
      GameUtils:HideTutorialHelp()
    end
    return true
  elseif msgType == NetAPIList.krypton_gacha_some_ack.Code then
    GameUIKrypton.actionKryptonMsgType = msgType
    GameUIKrypton.actionKryptonContent = content
    GameUIKrypton.adv_cost = content.gacha_need_credit
    GameUIKrypton:AfterAnimationRefreshBag()
    if QuestTutorialUseKrypton:IsActive() and not GameUtils:GetTutorialHelp() then
      QuestTutorialUseKrypton:SetFinish(true)
      QuestTutorialEquipKrypton:SetActive(true)
      local function callback()
        GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonTab")
      end
      GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "HideTutorialBtnAction")
      GameUICommonDialog:PlayStory({100036}, callback)
    end
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
      GameUtils:HideTutorialHelp()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.krypton_energy_req.Code or content.api == NetAPIList.krypton_gacha_some_req.Code or content.api == NetAPIList.krypton_gacha_one_req.Code then
      GameUIKrypton.actionKryptonError = true
      GameUIKrypton.actionKryptonCode = content.code
      GameUIKrypton:AfterAnimationRefreshBag()
      if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "composeKrypton" then
        GameUtils:HideTutorialHelp()
      end
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIKrypton.GotoGetMoney()
  local affairBuildInfo = GameGlobalData:GetBuildingInfo("affairs_hall")
  if affairBuildInfo.level >= 1 then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateAffairInfo)
  else
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
    local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
    GameUIBuilding:DisplayUpgradeDialog("affairs_hall")
  end
end
function GameUIKrypton.NeedMoreMoney(text)
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.Green)
  local cancel = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
  local affairs = GameLoader:GetGameText("LC_MENU_GET_MORE_BUTTON")
  GameUIMessageDialog:SetRightTextButton(cancel)
  GameUIMessageDialog:SetLeftGreenButton(affairs, GameUIKrypton.GotoGetMoney)
  GameUIMessageDialog:Display("", text)
end
function GameUIKrypton:AfterAnimationRefreshBag()
  if self.animationAction then
    if self.actionKryptonError then
      local text = AlertDataList:GetTextFromErrorCode(self.actionKryptonCode)
      if tonumber(self.actionKryptonCode) == 130 then
        if not GameUICollect:CheckTutorialCollect(self.actionKryptonCode) then
          GameUIKrypton.NeedMoreMoney(text)
        end
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(self.actionKryptonCode)
      end
      self.actionKryptonError = nil
      self.actionKryptonCode = nil
      self.animationAction = nil
      self.requestComposeing = nil
      self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      GameUIKrypton.RefreshResource()
    elseif GameUIKrypton.actionKryptonMsgType and GameUIKrypton.actionKryptonContent then
      self.animationAction = nil
      self.requestComposeing = nil
      if GameUIKrypton.actionKryptonMsgType == NetAPIList.krypton_gacha_one_ack.Code then
        if GameUIKrypton.actionKryptonContent.money_or_krypton == 0 then
          GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_KRYPTON_FAIL"), GameUIKrypton.actionKryptonContent.money), 3000)
        elseif GameUIKrypton.actionKryptonContent.money_or_krypton == 2 then
          GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_KRYPTON_DECOMPOSE_ALL"), GameUIKrypton.actionKryptonContent.increase_kenergy), 3000)
        elseif GameUIKrypton.actionKryptonContent.money_or_krypton == 3 then
          local numberText = tostring(1000)
          local nameText = GameLoader:GetGameText("LC_MENU_krypton_energy")
          local tipText = string.format(GameLoader:GetGameText("LC_MENU_ITEM_REWARDS_ALERT"), nameText .. "x" .. numberText)
          GameTip:Show(tipText)
        else
          GameUIKrypton.kryptonBag = LuaUtils:table_values(GameUIKrypton.kryptonBag)
          table.insert(GameUIKrypton.kryptonBag, GameUIKrypton.actionKryptonContent.krypton)
          GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
          self.animationAction = nil
          self.requestComposeing = nil
          GameUIKrypton:RefreshBagKrypton()
        end
        GameUIKrypton:RefreshkLevel(GameUIKrypton.actionKryptonContent.klevel, GameUIKrypton.actionKryptonContent.gacha_need_money, GameUIKrypton.actionKryptonContent.is_high, GameUIKrypton.actionKryptonContent.use_credit_count, GameUIKrypton.actionKryptonContent.gacha_need_credit, GameUIKrypton.actionKryptonContent.gacha_need_credit == 0)
      elseif GameUIKrypton.actionKryptonMsgType == NetAPIList.krypton_gacha_some_ack.Code then
        GameUIKrypton.kryptonBag = LuaUtils:table_values(GameUIKrypton.kryptonBag)
        for k, v in pairs(GameUIKrypton.actionKryptonContent.kryptons) do
          table.insert(GameUIKrypton.kryptonBag, v)
        end
        GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
        GameUIKrypton:RefreshBagKrypton()
        GameUIKrypton:RefreshkLevel(GameUIKrypton.actionKryptonContent.klevel, GameUIKrypton.actionKryptonContent.gacha_need_money, GameUIKrypton.actionKryptonContent.is_high, GameUIKrypton.actionKryptonContent.use_credit_count, GameUIKrypton.actionKryptonContent.gacha_need_credit, GameUIKrypton.actionKryptonContent.gacha_need_credit == 0)
        if GameUIKrypton.autoDecompose then
          local tipText = GameLoader:GetGameText("LC_MENU_KRYPTON_COMOSE_12_DECOMPOSE")
          tipText = string.gsub(tipText, "<kenergy_num>", GameUIKrypton.actionKryptonContent.increase_kenergy)
          tipText = string.gsub(tipText, "<cubits_num>", GameUIKrypton.actionKryptonContent.increase_money)
          GameTip:Show(tipText, 3000)
        else
          local tipText = GameLoader:GetGameText("LC_MENU_KRYPTON_COMOSE_12")
          tipText = string.gsub(tipText, "<krypton_num>", LuaUtils:table_size(GameUIKrypton.actionKryptonContent.kryptons))
          tipText = string.gsub(tipText, "<cubits_num>", GameUIKrypton.actionKryptonContent.increase_money)
          GameTip:Show(tipText, 3000)
        end
      end
      GameUIKrypton.RefreshResource()
      self:GetFlashObject():InvokeASCallback("_root", "setButtonEnableWhenAction", true)
      GameUIKrypton.actionKryptonMsgType = nil
      GameUIKrypton.actionKryptonContent = nil
    end
  end
end
function GameUIKrypton:RefreshkLevel(level, gacha_need_money, is_high, adv_level, adv_gacha_need_money, adv_is_high)
  self.currentKlevel = level
  self.currentAdvlevel = adv_level
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  local costText = GameUtils.numberConversion(gacha_need_money)
  local adv_costText = GameUtils.numberConversion(adv_gacha_need_money)
  self:GetFlashObject():InvokeASCallback("_root", "refreshkLevel", self.currentKlevel, costText, is_high, self.currentAdvlevel, adv_costText, adv_is_high, lang)
  for k, v in pairs(GameUIKrypton:GetShowNumber()) do
    self:GetFlashObject():InvokeASCallback("_root", "setWhichKryptonCanShow", v)
  end
end
function GameUIKrypton:GetShowNumber()
  if self.currentKlevel > 0 then
    local numberTable = {
      1,
      2,
      3,
      4,
      5,
      6
    }
    local getNumberTbale = {}
    for i = 1, self.currentKlevel + 1 do
      local index = math.random(#numberTable)
      table.insert(getNumberTbale, numberTable[index])
      table.remove(numberTable, index)
    end
    return getNumberTbale
  end
  return nil
end
function GameUIKrypton.DecomposeCallback(msgType, content)
  GameUIKrypton.DecomposeTag = false
  if msgType == NetAPIList.krypton_decompose_ack.Code then
    if GameUIKrypton.decomposeId == "" then
      GameUIKrypton:RequestKryptonInBag()
    else
      GameUIKrypton:RequestKryptonInBag()
      GameUIKrypton.kryptonBag = LuaUtils:table_values(GameUIKrypton.kryptonBag)
      GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
    end
    GameUIKrypton.decomposeId = ""
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.increase_kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_decompose_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIKrypton:GetKryptonLevel(itemType)
  local levelName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
  local star, _end = string.find(levelName, "LV")
  levelName = string.sub(levelName, star, string.len(levelName) + 1)
  return levelName
end
function GameUIKrypton.enhanceKryptonCallback(msgtype, content)
  if msgtype == NetAPIList.krypton_enhance_ack.Code then
    local kry = content.krypton
    local isInFleet = false
    if kry and kry.id then
      for k, v in pairs(GameUIKrypton.kryptonBag) do
        if v.id == kry.id then
          GameUIKrypton.kryptonBag[k] = kry
        end
      end
      for k, v in pairs(GameUIKrypton.currentFleetKrypton) do
        if v.id == kry.id then
          GameUIKrypton.currentFleetKrypton[k] = kry
          isInFleet = true
        end
      end
      ItemBox:SetKryptonBox(content.krypton, GameUIKrypton:GetKryptonType(content.krypton), #kry.formation)
      ItemBox:ShowKryptonLevelUp()
    end
    GameUIKrypton:RefreshBagKrypton()
    GameUIKrypton:RefreshCurFleetKrypton()
    if not isInFleet then
      GameUIKrypton:UpdataFleetSlotState(GameUIKrypton.onSelectBagSlot, false)
    end
    return true
  elseif msgtype == NetAPIList.common_ack.Code then
    if GameUIKrypton.dragItemInstance then
      GameStateFleetInfo:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function GameUIKrypton.kryptonEquipCallback(msgType, content)
  DebugOut("--------kryptonEquipCallback------------", msgType, content, GameUIKrypton.equipOffDestSlot, GameUIKrypton.equipOffSrcSlot, GameUIKrypton.equipDestSlot)
  if msgType == NetAPIList.krypton_equip_ack.Code then
    local item
    if GameUIKrypton.equipOffDestSlot ~= -1 then
      item = GameUIKrypton.kryptonBag[GameUIKrypton.equipOffDestSlot]
      GameUIKrypton.kryptonBag[GameUIKrypton.equipOffDestSlot] = GameUIKrypton.currentFleetKrypton[GameUIKrypton.equipOffSrcSlot]
      if GameUIKrypton.kryptonBag[GameUIKrypton.equipOffDestSlot] then
        GameUIKrypton.kryptonBag[GameUIKrypton.equipOffDestSlot].slot = GameUIKrypton.equipOffDestSlot
      end
    elseif GameUIKrypton.equipDestSlot ~= -1 then
      item = GameUIKrypton.currentFleetKrypton[GameUIKrypton.equipDestSlot]
      GameUIKrypton.kryptonBag[GameUIKrypton.equipSrcSlot] = item
      if GameUIKrypton.kryptonBag[GameUIKrypton.equipSrcSlot] then
        GameUIKrypton.kryptonBag[GameUIKrypton.equipSrcSlot].slot = GameUIKrypton.equipSrcSlot
      end
      item = nil
    end
    DebugOut("kryptonEquipCallback item: ", item, GameUIKrypton.dragItemInstance)
    if GameUIKrypton.dragItemInstance and item then
      GameUIKrypton.dragItemInstance.DraggedItemID = -1
      local frame = ""
      local id = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      GameUIKrypton.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
      GameUIKrypton.dragItemInstance:SetDestPosition(GameUIKrypton.dragX, GameUIKrypton.dragY)
      GameUIKrypton.dragItemInstance:StartAutoMove()
    elseif GameUIKrypton.dragItemInstance then
      GameStateKrypton:onEndDragItem()
    end
    GameUIKrypton:RequestKryptonInBag()
    GameUIKrypton.equipOffDestSlot = -1
    GameUIKrypton.equipDestSlot = -1
    if QuestTutorialEquipKrypton:IsActive() then
      QuestTutorialEquipKrypton:SetFinish(true)
      QuestTutorialEnhanceKrypton:SetActive(true)
      GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "HideTutorialEquipKrypton")
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.krypton_equip_off_req.Code or content.api == NetAPIList.krypton_equip_req.Code) then
    if GameUIKrypton.dragItemInstance then
      GameStateKrypton:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function GameUIKrypton.swapFleetCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.krypton_fleet_move_req.Code then
      if content.code == 0 then
        local item = GameUIKrypton.currentFleetKrypton[GameUIKrypton.swapSrcSlot]
        if GameUIKrypton.dragItemInstance and item then
          GameUIKrypton.dragItemInstance.DraggedItemID = -1
          local frame = ""
          local id = GameUIKrypton:GetKryptonType(item)
          if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
            local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
            local localPath = "data2/" .. fullFileName
            if DynamicResDownloader:IfResExsit(localPath) then
              ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
              frame = "item_" .. id
            else
              frame = "temp"
              DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
            end
          else
            frame = "item_" .. id
          end
          GameUIKrypton.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
          GameUIKrypton.dragItemInstance:SetDestPosition(GameUIKrypton.dragX, GameUIKrypton.dragY)
          GameUIKrypton.dragItemInstance:StartAutoMove()
        elseif GameUIKrypton.dragItemInstance then
          GameStateKrypton:onEndDragItem()
        end
      else
        GameStateKrypton:onEndDragItem()
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
function GameUIKrypton.swapBagCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_swap_req.Code then
    if content.code == 0 then
      local item = GameUIKrypton.kryptonBag[GameUIKrypton.swapSrcSlot]
      if GameUIKrypton.dragItemInstance and item then
        GameUIKrypton.dragItemInstance.DraggedItemID = -1
        local frame = ""
        local id = GameUIKrypton:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            frame = "item_" .. id
          else
            frame = "temp"
            DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          frame = "item_" .. id
        end
        GameUIKrypton.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
        GameUIKrypton.dragItemInstance:SetDestPosition(GameUIKrypton.dragX, GameUIKrypton.dragY)
        GameUIKrypton.dragItemInstance:StartAutoMove()
      elseif GameUIKrypton.dragItemInstance then
        GameStateKrypton:onEndDragItem()
      end
      GameUIKrypton.kryptonBag[GameUIKrypton.swapSrcSlot], GameUIKrypton.kryptonBag[GameUIKrypton.swapDestSlot] = GameUIKrypton.kryptonBag[GameUIKrypton.swapDestSlot], GameUIKrypton.kryptonBag[GameUIKrypton.swapSrcSlot]
      if GameUIKrypton.kryptonBag[GameUIKrypton.swapSrcSlot] then
        GameUIKrypton.kryptonBag[GameUIKrypton.swapSrcSlot].slot = GameUIKrypton.swapSrcSlot
      end
      GameUIKrypton.kryptonBag[GameUIKrypton.swapDestSlot].slot = GameUIKrypton.swapDestSlot
      GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
      GameUIKrypton:RefreshBagKrypton()
    else
      GameStateKrypton:onEndDragItem()
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIKrypton.KryptonFragmentCoreNtf(content)
  GameUIGlobalScreen:ShowAlert("error", content.code, nil)
end
if AutoUpdate.isAndroidDevice then
  function GameUIKrypton.OnAndroidBack()
    if GameUIWDStuff.currentMenu == GameUIWDStuff.menuType.menu_type_help then
      local flashObj = GameUIWDStuff:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameUIKrypton.mEnterRefine then
      GameUIKrypton:OnFSCommand("refine_back")
    else
      GameStateManager:SetCurrentGameState(GameStateKrypton.previousState)
    end
  end
end
function GameUIKrypton.Krypton_decompos_change_ntf(content)
  DebugOut("Krypton_decompos_change_ntf : " .. content.change_num)
  GameUIKrypton.KryptonAutoDecomposeLevelMax = content.max_level
  GameUIKrypton.KryptonAutoDecomposeLevelMin = content.mini_level
  GameUIKrypton.KryptonAutoDecomposeLevel = content.change_num
  if not GameUIKrypton.initDecomposeBar then
    GameUIKrypton.initDecomposeBar = true
    GameUIKrypton:UpdateAutoComposeLvBar()
  end
end
function GameUIKrypton:UpdateAutoComposeLvBar()
  DebugOut("UpdateAutoComposeLvBar")
  if GameUIKrypton:GetFlashObject() ~= nil then
    local info_parms = GameUIKrypton.KryptonAutoDecomposeLevelMin .. "\001" .. GameUIKrypton.KryptonAutoDecomposeLevelMax .. "\001" .. GameUIKrypton.KryptonAutoDecomposeLevel
    DebugOut(info_parms)
    GameUIKrypton:GetFlashObject():InvokeASCallback("_root", "setDecomposeLvBar", info_parms)
  end
end
GameUIKrypton.mCurSortType = 1
function GameUIKrypton:SortKrypton()
  GameUIKrypton:RequestSortKryptonInBag()
end
function GameUIKrypton:RequestSortKryptonInBag()
  local req = {
    type = GameUIKrypton.mCurSortType
  }
  DebugTable(req)
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_sort_req.Code, req, GameUIKrypton.RequestSortKryptonInBagCallback, true, nil)
end
function GameUIKrypton.RequestSortKryptonInBagCallback(msgType, content)
  if msgType == NetAPIList.krypton_sort_ack.Code then
    GameUIKrypton.grid_capacity = content.grid_capacity
    GameUIKrypton.adv_cost = content.gacha_need_credit
    GameUIKrypton.kryptonBag = content.kryptons
    GameUIKrypton.kryptonBag = GameUIKrypton:ProcessBagInfo(GameUIKrypton.kryptonBag)
    GameUIKrypton:SetBagList()
    GameUIKrypton:RefreshkLevel(content.klevel, content.gacha_need_money, content.is_high, content.use_credit_count, content.gacha_need_credit, content.gacha_need_credit == 0)
    GameUIKrypton.mCurSortType = GameUIKrypton.mCurSortType + 1
    if GameUIKrypton.mCurSortType > 2 then
      GameUIKrypton.mCurSortType = 1
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_sort_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIKrypton.UpdateBagListItemCallback(content)
  local baseIdx = tonumber(content.itemId)
  local k = tonumber(content.index)
  local frame = ""
  local level = ""
  local idx = (baseIdx - 1) * 4 + k
  local item = GameUIKrypton.kryptonBag[idx]
  local percent = 1
  if item then
    local id = GameUIKrypton:GetKryptonType(item)
    frame = "item_" .. id
    level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
    percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
    if percent > 101 then
      percent = 101
    end
  else
    frame = "empty"
    level = "0"
  end
  local isHide = false
  if idx > GameUIKrypton.grid_capacity then
    isHide = true
  end
  local isSelected = false
  if true == GameUIKrypton.inmultiselectedmode then
    for j = 1, #GameUIKrypton.multiselectedSlot do
      if idx == GameUIKrypton.multiselectedSlot[j] then
        isSelected = true
        break
      end
    end
  end
  local one = {
    isHide = isHide,
    fr = frame,
    lv = level,
    progress = percent,
    bSelected = isSelected
  }
  local fourItem = {}
  fourItem[k] = one
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", content.itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4], GameUIKrypton.inmultiselectedmode, GameStateKrypton.currentTab)
  end
end
function GameUIKrypton:UpdateBagListItem(itemId)
  local baseIdx = tonumber(itemId)
  local fourItem = {}
  for k = 1, 4 do
    local frame = ""
    local level = ""
    local idx = (baseIdx - 1) * 4 + k
    local item = GameUIKrypton.kryptonBag[idx]
    local percent = 1
    if item then
      local id = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, {itemId = itemId, index = k}, GameUIKrypton.UpdateBagListItemCallback)
        end
      else
        frame = "item_" .. id
      end
      level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
      percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
      if percent > 101 then
        percent = 101
      end
    else
      frame = "empty"
      level = "0"
    end
    local isHide = false
    if idx > GameUIKrypton.grid_capacity then
      isHide = true
    end
    local isSelected = false
    if true == GameUIKrypton.inmultiselectedmode then
      for j = 1, #GameUIKrypton.multiselectedSlot do
        if idx == GameUIKrypton.multiselectedSlot[j] then
          isSelected = true
          break
        end
      end
    end
    local one = {
      isHide = isHide,
      fr = frame,
      lv = level,
      progress = percent,
      bSelected = isSelected
    }
    fourItem[k] = one
  end
  DebugOut("fourItem")
  DebugTable(fourItem)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4], GameUIKrypton.inmultiselectedmode, GameStateKrypton.currentTab)
  end
end
function GameUIKrypton:SetBagList()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local cnt = math.ceil(GameUIKrypton.grid_capacity / 4)
    DebugOut("GameUIKrypton:SetBagList()" .. tostring(cnt))
    self:GetFlashObject():InvokeASCallback("_root", "SetBagList", cnt)
    self:GetFlashObject():InvokeASCallback("_root", "setKryptonCount", self:GetKryptonCount(), GameUIKrypton.grid_capacity, string.gsub(GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_8"), "<number1>", tostring(GameUIKrypton.grid_capacity)))
  end
end
function GameUIKrypton:BagItemClicked(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local idx = (tonumber(param[1]) - 1) * 4 + tonumber(param[2])
  local newArg = tostring(idx) .. "\001" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
  GameUIKrypton:BagItemReleased(newArg)
end
function GameUIKrypton:RefineCmdHandle(cmd, arg)
  if cmd == "BagItemClicked" then
    GameUIKrypton:BagItemClicked(arg)
    self:UpdataFleetSlotState(self.onSelectBagSlot, false)
  elseif cmd == "NeedUpdateBagListItem" then
    GameUIKrypton:UpdateBagListItem(tonumber(arg))
  elseif cmd == "refine_show" then
    GameUIKrypton:CheckCanEnterRefine()
  elseif cmd == "refine_back" then
    GameFleetInfoBackground:SetRootVisible(true)
    GameUIKrypton:HideRefinePanel()
  elseif cmd == "refine_closed" then
    GameUIKrypton.mEnterRefine = false
    GameUIKrypton:RequestKryptonInBag()
  elseif cmd == "show_target_select_panel" then
    GameUIKrypton:CheckTutorialRefine1End()
    GameUIKrypton.mTargetCurClickedIdx = GameUIKrypton.mTargetCurIdx
    if -1 == GameUIKrypton.mTargetCurClickedIdx and GameUIKrypton.mRefineList and #GameUIKrypton.mRefineList > 0 then
      GameUIKrypton.mTargetCurClickedIdx = 1
    end
    GameUIKrypton:SetTargetSelectList()
    GameUIKrypton:ShowTargetSelectPanel()
  elseif cmd == "close_target_select_panel" then
    GameUIKrypton:HideTargetSelectPanel()
  elseif cmd == "show_src_select_panel" then
    GameUIKrypton:CheckTutorialRefine2End()
    GameUIKrypton:RequestInjectList()
  elseif cmd == "close_src_select_panel" then
    GameUIKrypton:HideSrcSelectPanel()
  elseif cmd == "NeedUpdateTargetListItem" then
    GameUIKrypton:UpdateTargetListItem(tonumber(arg))
  elseif cmd == "NeedUpdateSrcListItem" then
    GameUIKrypton:UpdateSrcListItem(tonumber(arg))
  elseif cmd == "refine_help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_REFINE_HELP"))
  elseif cmd == "refine_clicked" then
    GameUIKrypton:RequestRefine()
  elseif cmd == "pour_clicked" then
    GameUIKrypton:RequestInject()
  elseif cmd == "refine_add" then
    GameUIKrypton:RequestPrice()
  elseif cmd == "TargetItemClicked" then
    GameUIKrypton:TargetItemClicked(arg)
  elseif cmd == "target_choose" then
    GameUIKrypton:TargetItemSelect()
  elseif cmd == "SrcItemClicked" then
    GameUIKrypton:SrcItemClicked(arg)
  elseif cmd == "src_choose" then
    GameUIKrypton:SrcItemSelect()
  elseif cmd == "item_use_clicked" then
    GameUIKrypton:UseItemClicked()
  elseif cmd == "inject_animation_over" then
    GameUIKrypton:InjectAnimationOver()
  elseif cmd == "crit_animation_over" then
    GameUIKrypton:CritAnimationOver()
  end
end
GameUIKrypton.mEnterRefine = false
GameUIKrypton.mRefineInfo = nil
GameUIKrypton.mRefineList = nil
GameUIKrypton.mRefineNextList = nil
GameUIKrypton.mInjectList = nil
GameUIKrypton.mTargetCurClickedIdx = -1
GameUIKrypton.mTargetCurIdx = -1
GameUIKrypton.mSrcCurClickedIdx = -1
GameUIKrypton.mSrcCurIdx = -1
GameUIKrypton.mIsUseItemOn = false
GameUIKrypton.mTargetChooseOrUnload = true
GameUIKrypton.mSrcChooseOrUnload = true
GameUIKrypton.mCurInjectListForTargetId = ""
GameUIKrypton.mSrcCurIdxBackup = -1
GameUIKrypton.mIsCrit = false
GameUIKrypton.mGotKenergyTip = nil
function GameUIKrypton:ResetRefineData()
  GameUIKrypton.mRefineInfo = nil
  GameUIKrypton.mRefineList = nil
  GameUIKrypton.mRefineNextList = nil
  GameUIKrypton.mInjectList = nil
  GameUIKrypton.mTargetCurClickedIdx = -1
  GameUIKrypton.mTargetCurIdx = -1
  GameUIKrypton.mSrcCurClickedIdx = -1
  GameUIKrypton.mSrcCurIdx = -1
  GameUIKrypton.mIsUseItemOn = false
  GameUIKrypton.mTargetChooseOrUnload = true
  GameUIKrypton.mSrcChooseOrUnload = true
  GameUIKrypton.mCurInjectListForTargetId = ""
  GameUIKrypton.mSrcCurIdxBackup = -1
  GameUIKrypton.mIsCrit = false
  GameUIKrypton.mGotKenergyTip = nil
end
function GameUIKrypton:EnterRefine()
  GameUIKrypton:SetRefinePanelInfo()
  if not GameUIKrypton.mEnterRefine then
    DebugOut("GameUIKrypton:EnterRefine")
    GameUIKrypton.mEnterRefine = true
    GameFleetInfoBackground:SetRootVisible(false)
    GameUIKrypton:ShowRefinePanel()
  end
  GameUIKrypton:CheckShowTutorialRefine1()
end
function GameUIKrypton:ShowRefinePanel()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowRefinePanel")
  end
end
function GameUIKrypton:HideRefinePanel()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideRefinePanel")
  end
end
function GameUIKrypton:SetRefinePanelInfo()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj and GameUIKrypton.mRefineInfo then
    local itemNum = GameUtils.numberConversion(GameUIKrypton.mRefineInfo.item_count)
    local resource = GameGlobalData:GetData("resource")
    local kryptonNum = GameUtils.numberConversion(resource.kenergy)
    local creditNum = GameUtils.numberConversion(resource.credit)
    flashObj:InvokeASCallback("_root", "SetRefineMoneys", kryptonNum, creditNum, itemNum)
    GameUIKrypton:SetItemMoneyItemIcon()
  end
  GameUIKrypton:SetItemCurAndMaxCount()
  GameUIKrypton:RefreshUseItem()
  GameUIKrypton:SetCurTargetInfo()
  GameUIKrypton:SetCurSrcIcon()
end
function GameUIKrypton.RefreshMoneyIcon()
  GameUIKrypton:SetItemMoneyItemIcon()
end
function GameUIKrypton:SetItemMoneyItemIcon()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj and GameUIKrypton.mRefineInfo then
    local fr
    local typeId = GameUIKrypton.mRefineInfo.item_id
    if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        fr = "item_" .. typeId
      else
        fr = "temp"
        DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, GameUIKrypton.RefreshMoneyIcon)
      end
    else
      fr = "item_" .. typeId
    end
    flashObj:InvokeASCallback("_root", "SetItemMoneyItemIcon", fr)
  end
end
function GameUIKrypton:ShowTargetSelectPanel()
  GameUIKrypton:SetTargetClickedInfo(GameUIKrypton.mTargetCurClickedIdx)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowTargetSelectPanel")
  end
end
function GameUIKrypton:HideTargetSelectPanel()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideTargetSelectPanel")
  end
end
function GameUIKrypton:SetTargetSelectPanelInfo()
end
function GameUIKrypton:SetTargetSelectList()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local lineCount = math.ceil(#GameUIKrypton.mRefineList / 3)
    flashObj:InvokeASCallback("_root", "SetTargetList", lineCount)
  end
end
function GameUIKrypton:UpdateTargetListItem(itemId)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj and GameUIKrypton.mRefineList then
    local info = {}
    for k = 1, 3 do
      local item = GameUIKrypton.mRefineList[(itemId - 1) * 3 + k]
      if item then
        local fr
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. item.level
        local typeId = GameUIKrypton:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            fr = "item_" .. typeId
          else
            fr = "temp"
            DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          fr = "item_" .. typeId
        end
        local isSelected = false
        if (itemId - 1) * 3 + k == GameUIKrypton.mTargetCurClickedIdx then
          isSelected = true
        end
        local percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
        if percent > 101 then
          percent = 101
        end
        info[k] = {
          fr = fr,
          lv = lv,
          isSelected = isSelected,
          progress = percent
        }
      end
    end
    DebugOut("UpdateTargetListItem ")
    DebugTable(info)
    flashObj:InvokeASCallback("_root", "UpdateTargetListItem", itemId, info[1], info[2], info[3])
  end
end
function GameUIKrypton:TargetItemClicked(arg)
  local array = LuaUtils:string_split(arg, "\001")
  local oldItemId = -1
  if -1 ~= GameUIKrypton.mTargetCurClickedIdx then
    oldItemId = math.ceil(GameUIKrypton.mTargetCurClickedIdx / 3)
  end
  GameUIKrypton.mTargetCurClickedIdx = (array[1] - 1) * 3 + array[2]
  if -1 ~= oldItemId then
    GameUIKrypton:UpdateTargetListItem(oldItemId)
  end
  GameUIKrypton:UpdateTargetListItem(tonumber(array[1]))
  GameUIKrypton:SetTargetClickedInfo(GameUIKrypton.mTargetCurClickedIdx)
end
function GameUIKrypton:SetTargetClickedInfo(idx)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local leftName = ""
    local leftIcon
    local leftRank = 0
    local paramText = ""
    local paramNum = ""
    local paramNum2 = ""
    local isInFormation = false
    if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[idx] then
      local item = GameUIKrypton.mRefineList[idx]
      local typeId = GameUIKrypton:GetKryptonType(item)
      leftName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      leftRank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          leftIcon = "item_" .. typeId
        else
          leftIcon = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        leftIcon = "item_" .. typeId
      end
      local addon1 = item.addon[1]
      if addon1.type == 20 then
        paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
      else
        paramNum = addon1.value
        paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
        if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 then
          paramNum = paramNum / 10 .. "%"
        end
        paramNum = "+" .. paramNum
      end
      isInFormation = 0 < #item.formation
    end
    local rightName = ""
    local rightIcon
    local rightRank = 0
    if GameUIKrypton.mRefineNextList and GameUIKrypton.mRefineNextList[idx] then
      local item = GameUIKrypton.mRefineNextList[idx]
      local typeId = GameUIKrypton:GetKryptonType(item)
      rightName = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      rightRank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          rightIcon = "item_" .. typeId
        else
          rightIcon = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        rightIcon = "item_" .. typeId
      end
      local addon1 = item.addon[1]
      if addon1.type == 20 then
      else
        paramNum2 = addon1.value
        if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 then
          paramNum2 = paramNum2 / 10 .. "%"
        end
        paramNum2 = "+" .. paramNum2
      end
    end
    DebugOut("leftName " .. leftName .. " rightName" .. rightName)
    local isJp = false
    if GameSettingData.Save_Lang and GameSettingData.Save_Lang == "jp" then
      isJp = true
    end
    flashObj:InvokeASCallback("_root", "SetTargetPanelRightInfo", paramText, paramNum, paramNum2, leftName, leftIcon, leftRank, rightName, rightIcon, rightRank, isJp, isInFormation)
  end
  GameUIKrypton:SetTargetChooseBtnText()
end
function GameUIKrypton:SetTargetChooseBtnText()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local btnText = ""
    if -1 ~= GameUIKrypton.mTargetCurIdx and GameUIKrypton.mTargetCurClickedIdx == GameUIKrypton.mTargetCurIdx then
      btnText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
      GameUIKrypton.mTargetChooseOrUnload = false
    else
      btnText = GameLoader:GetGameText("LC_MENU_REFINE_CHOOSE_BUTTON")
      GameUIKrypton.mTargetChooseOrUnload = true
    end
    flashObj:InvokeASCallback("_root", "SetTargetChooseBtnText", btnText)
  end
end
function GameUIKrypton:TargetItemSelect()
  if GameUIKrypton.mTargetChooseOrUnload then
    if GameUIKrypton.mTargetCurIdx == GameUIKrypton.mTargetCurClickedIdx then
    else
      GameUIKrypton.mTargetCurIdx = GameUIKrypton.mTargetCurClickedIdx
      GameUIKrypton.mInjectList = nil
      GameUIKrypton.mSrcCurClickedIdx = -1
      GameUIKrypton.mSrcCurIdx = -1
      GameUIKrypton.mSrcCurIdxBackup = -1
      GameUIKrypton:SetCurTargetInfo()
      GameUIKrypton:SetCurSrcIcon()
      GameUIKrypton:CheckShowTutorialRefine2()
    end
    GameUIKrypton:HideTargetSelectPanel()
  else
    GameUIKrypton.mTargetCurIdx = -1
    GameSettingData.CurRefineKryptonId = -1
    GameUtils:SaveSettingData()
    GameUIKrypton.mInjectList = nil
    GameUIKrypton.mSrcCurClickedIdx = -1
    GameUIKrypton.mSrcCurIdx = -1
    GameUIKrypton.mSrcCurIdxBackup = -1
    GameUIKrypton:SetCurTargetInfo()
    GameUIKrypton:SetCurSrcIcon()
    GameUIKrypton:SetTargetClickedInfo(GameUIKrypton.mTargetCurClickedIdx)
  end
end
function GameUIKrypton:SetCurTargetIcon()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local fr
    local isVisible = false
    if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
      local item = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx]
      local typeId = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          fr = "item_" .. typeId
        else
          fr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        fr = "item_" .. typeId
      end
      isVisible = true
    end
    flashObj:InvokeASCallback("_root", "SetCurTargetIcon", isVisible, fr)
  end
end
function GameUIKrypton:SetCurTargetInfo()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local percent = 0
    local percentStr = "0.0%"
    local kenergy = "0"
    local isShowAfter = false
    local beforeExp = 0
    local beforeNeedExp = 0
    local afterStr = ""
    local isShowFreeInfo = false
    local isShowItemCountInfo = false
    if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
      local item = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx]
      beforeExp = item.refine_exp
      beforeNeedExp = item.refine_need_exp
      percent = item.refine_exp / item.refine_need_exp * 100
      if percent > 99.9 and percent < 100 then
        percent = 99.9
      end
      percentStr = string.format("%0.1f", percent) .. "%"
      kenergy = GameUtils.numberConversion(item.refine_need_kenergy)
      if item.rank <= 4 then
        isShowFreeInfo = true
        isShowItemCountInfo = false
      else
        isShowFreeInfo = false
        isShowItemCountInfo = true
      end
      GameSettingData.CurRefineKryptonId = item.id
    else
      isShowFreeInfo = false
      isShowItemCountInfo = false
      GameSettingData.CurRefineKryptonId = -1
    end
    GameUtils:SaveSettingData()
    if GameUIKrypton.mInjectList and GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx] then
      local item = GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx]
      local itemExp = 0
      if GameUIKrypton.mIsUseItemOn then
        itemExp = GameUIKrypton.mRefineInfo.item_exp
      end
      local afterExp = beforeExp + item.inject_exp + itemExp
      local percent2 = afterExp / beforeNeedExp * 100
      if percent2 > 999.9 then
        percent2 = 999.9
      end
      afterStr = string.format("%0.1f", percent2) .. "%"
      isShowAfter = true
    end
    percent = math.floor(percent)
    flashObj:InvokeASCallback("_root", "SetProgress", percent + 1, percentStr, isShowAfter, afterStr)
    flashObj:InvokeASCallback("_root", "SetRefineBtnKenergy", kenergy)
    flashObj:InvokeASCallback("_root", "SetItemCurAndMaxCountVisible", isShowItemCountInfo)
    flashObj:InvokeASCallback("_root", "SetRefineAddBtnVisible", isShowItemCountInfo and GameUIKrypton.mRefineInfo.times_can_buy)
    flashObj:InvokeASCallback("_root", "SetFreeInfoVisible", isShowFreeInfo)
  end
  GameUIKrypton:SetCurTargetIcon()
end
function GameUIKrypton:ShowSrcSelectPanel()
  GameUIKrypton:SetSrcClickedInfo(GameUIKrypton.mSrcCurClickedIdx)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowSrcSelectPanel")
  end
end
function GameUIKrypton:HideSrcSelectPanel()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideSrcSelectPanel")
  end
end
function GameUIKrypton:SetSrcSelectPanelInfo()
end
function GameUIKrypton:SetSrcSelectList()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local itemCnt = 0
    if GameUIKrypton.mInjectList then
      itemCnt = math.ceil(#GameUIKrypton.mInjectList / 3)
    end
    flashObj:InvokeASCallback("_root", "SetSrcList", itemCnt)
  end
end
function GameUIKrypton:SetChoseButtonStates()
  local buttonState = 0
  if 0 < #self.mInjectList then
    buttonState = 1
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setChoseButtonStates", buttonState)
  end
end
function GameUIKrypton:UpdateSrcListItem(itemId)
  local flashObj = GameUIKrypton:GetFlashObject()
  if GameUIKrypton.mInjectList and flashObj then
    local info = {}
    for k = 1, 3 do
      local item = GameUIKrypton.mInjectList[(itemId - 1) * 3 + k]
      if item then
        local fr
        local lv = GameLoader:GetGameText("LC_MENU_Level") .. item.level
        local typeId = GameUIKrypton:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            fr = "item_" .. typeId
          else
            fr = "temp"
            DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          fr = "item_" .. typeId
        end
        local isSelected = false
        if (itemId - 1) * 3 + k == GameUIKrypton.mSrcCurClickedIdx then
          isSelected = true
        end
        local percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
        if percent > 101 then
          percent = 101
        end
        info[k] = {
          fr = fr,
          lv = lv,
          isSelected = isSelected,
          progress = percent
        }
      end
    end
    DebugOut("UpdateSrcListItem ")
    DebugTable(info)
    flashObj:InvokeASCallback("_root", "UpdateSrcListItem", itemId, info[1], info[2], info[3])
  end
end
function GameUIKrypton:SrcItemClicked(arg)
  local array = LuaUtils:string_split(arg, "\001")
  local oldItemId = -1
  if -1 ~= GameUIKrypton.mSrcCurClickedIdx then
    oldItemId = math.ceil(GameUIKrypton.mSrcCurClickedIdx / 3)
  end
  GameUIKrypton.mSrcCurClickedIdx = (array[1] - 1) * 3 + array[2]
  if -1 ~= oldItemId then
    GameUIKrypton:UpdateSrcListItem(oldItemId)
  end
  GameUIKrypton:UpdateSrcListItem(tonumber(array[1]))
  GameUIKrypton:SetSrcClickedInfo(GameUIKrypton.mSrcCurClickedIdx)
end
function GameUIKrypton:SetSrcClickedInfo(idx)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local nameText = ""
    local iconFr
    local rank = 0
    local isInFormation = false
    if GameUIKrypton.mInjectList and GameUIKrypton.mInjectList[idx] then
      local item = GameUIKrypton.mInjectList[idx]
      local typeId = GameUIKrypton:GetKryptonType(item)
      nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. typeId)
      rank = item.rank
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          iconFr = "item_" .. typeId
        else
          iconFr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        iconFr = "item_" .. typeId
      end
      isInFormation = 0 < #item.formation
    end
    local isJp = false
    if GameSettingData.Save_Lang and GameSettingData.Save_Lang == "jp" then
      isJp = true
    end
    flashObj:InvokeASCallback("_root", "SetSrcPanelLeftInfo", nameText, iconFr, rank, isJp, isInFormation)
  end
  GameUIKrypton:SetSrcChooseBtnText()
end
function GameUIKrypton:SetSrcChooseBtnText()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local btnText = ""
    if -1 ~= GameUIKrypton.mSrcCurIdx and GameUIKrypton.mSrcCurClickedIdx == GameUIKrypton.mSrcCurIdx then
      btnText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
      GameUIKrypton.mSrcChooseOrUnload = false
    else
      btnText = GameLoader:GetGameText("LC_MENU_REFINE_CHOOSE_BUTTON")
      GameUIKrypton.mSrcChooseOrUnload = true
    end
    flashObj:InvokeASCallback("_root", "SetSrcChooseBtnText", btnText)
  end
end
function GameUIKrypton:SrcItemSelect()
  if GameUIKrypton.mSrcChooseOrUnload then
    if GameUIKrypton.mInjectList then
      GameUIKrypton.mSrcCurIdx = GameUIKrypton.mSrcCurClickedIdx
      GameUIKrypton.mSrcCurIdxBackup = GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx].id
      GameUIKrypton:HideSrcSelectPanel()
      GameUIKrypton:SetCurSrcIcon()
      GameUIKrypton:SetCurTargetInfo()
    end
  else
    GameUIKrypton.mSrcCurIdx = -1
    GameUIKrypton.mSrcCurIdxBackup = -1
    GameUIKrypton:SetCurSrcIcon()
    GameUIKrypton:SetCurTargetInfo()
    GameUIKrypton:SetSrcClickedInfo(GameUIKrypton.mSrcCurClickedIdx)
  end
end
function GameUIKrypton:SetCurSrcIcon()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local fr
    local isVisible = false
    if GameUIKrypton.mInjectList and GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx] then
      local item = GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx]
      local typeId = GameUIKrypton:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(typeId, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(typeId .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          fr = "item_" .. typeId
        else
          fr = "temp"
          DynamicResDownloader:AddDynamicRes(typeId .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        fr = "item_" .. typeId
      end
      isVisible = true
    end
    DebugOut("SetCurSrcIcon " .. tostring(isVisible))
    flashObj:InvokeASCallback("_root", "SetCurSrcIcon", isVisible, fr)
  end
end
function GameUIKrypton:UseItemClicked()
  if not GameUIKrypton.mIsUseItemOn and GameUIKrypton.mRefineInfo and GameUIKrypton.mRefineInfo.item_count <= 0 then
    local tip = GameLoader:GetGameText("LC_MENU_PROPS_INADEQUATE_INFO")
    GameTip:Show(tip)
    return
  end
  GameUIKrypton.mIsUseItemOn = not GameUIKrypton.mIsUseItemOn
  GameUIKrypton:RefreshUseItem()
  GameUIKrypton:SetCurTargetInfo()
end
function GameUIKrypton:RefreshUseItem()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetItemUseSelected", GameUIKrypton.mIsUseItemOn)
  end
end
function GameUIKrypton:SetItemCurAndMaxCount()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    local info = ""
    if GameUIKrypton.mRefineInfo then
      info = tostring(GameUIKrypton.mRefineInfo.remain_times) .. "/" .. tostring(GameUIKrypton.mRefineInfo.max_times)
    end
    flashObj:InvokeASCallback("_root", "SetItemCurAndMaxCount", info)
  end
end
function GameUIKrypton:CheckKryptonRefineEnterBtnVisible()
  local isVisible = false
  local modules = GameGlobalData:GetData("modules_status").modules
  for _, v in pairs(modules) do
    if v.name == "krypton_refine_open" and v.status then
      isVisible = true
      break
    end
  end
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetEnterRefineBtnVisible", isVisible)
  end
end
function GameUIKrypton:CheckCanEnterRefine()
  local canEnter = false
  local modules = GameGlobalData:GetData("modules_status").modules
  for _, v in pairs(modules) do
    if v.name == "krypton_refine" and v.status then
      canEnter = true
      break
    end
  end
  if canEnter then
    GameUIKrypton.mEnterRefine = false
    GameUIKrypton:ResetRefineData()
    GameUIKrypton:RequestRefineInfo()
  else
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_UNOPENED_ERROR")
    GameTip:Show(tip)
  end
end
function GameUIKrypton:PlayInjectAnimation()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "PlayInjectAnimation")
  end
end
function GameUIKrypton:InjectAnimationOver()
  if GameUIKrypton.mIsCrit then
    GameUIKrypton.mIsCrit = false
    GameUIKrypton:PlayCritAnimation()
  else
    local function callback()
      GameUIKrypton:ResetRefineData()
      GameUIKrypton:RequestRefineInfo()
    end
    if GameUIKrypton.mGotKenergyTip then
      GameUIKrypton.mGotKenergyTip = nil
    else
      callback()
    end
  end
end
function GameUIKrypton:PlayCritAnimation()
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "PlayCritAnimation")
  end
end
function GameUIKrypton:CritAnimationOver()
  local function callback()
    GameUIKrypton:ResetRefineData()
    GameUIKrypton:RequestRefineInfo()
  end
  if GameUIKrypton.mGotKenergyTip then
    GameUIKrypton.mGotKenergyTip = nil
  else
    callback()
  end
end
function GameUIKrypton:CheckShowTutorialRefine1()
  if not TutorialQuestManager.QuestTutorialRefine:IsFinished() and not TutorialQuestManager.QuestTutorialRefine:IsActive() then
    TutorialQuestManager.QuestTutorialRefine:SetActive(true, false)
    GameUIKrypton:SetRefineLeftTipVisible(true)
    GameUICommonDialog:PlayStory({1100047}, nil)
  end
end
function GameUIKrypton:CheckShowTutorialRefine2()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() then
    GameUIKrypton:SetRefineRightTipVisible(true)
    GameUICommonDialog:PlayStory({1100048}, nil)
  end
end
function GameUIKrypton:CheckTutorialRefine1End()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() then
    GameUIKrypton:SetRefineLeftTipVisible(false)
  end
end
function GameUIKrypton:CheckTutorialRefine2End()
  if TutorialQuestManager.QuestTutorialRefine:IsActive() and not TutorialQuestManager.QuestTutorialRefine:IsFinished() then
    GameUIKrypton:SetRefineRightTipVisible(false)
    TutorialQuestManager.QuestTutorialRefine:SetActive(false, true)
    TutorialQuestManager.QuestTutorialRefine:SetFinish(true)
  end
end
function GameUIKrypton:SetRefineLeftTipVisible(isVisible)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRefineLeftTipVisible", isVisible)
  end
end
function GameUIKrypton:SetRefineRightTipVisible(isVisible)
  local flashObj = GameUIKrypton:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetRefineRightTipVisible", isVisible)
  end
end
function GameUIKrypton:RequestRefineInfo()
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_refine_info_req.Code, nil, self.RequestRefineInfoCallback, true, nil)
end
function GameUIKrypton.RequestRefineInfoCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_info_req.Code then
    DebugOut("RequestRefineInfoCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_info_ack.Code then
    DebugOut("RequestRefineInfoCallback ok.")
    DebugTable(content)
    GameUIKrypton.mRefineInfo = content
    GameUIKrypton:RequestRefineList()
    return true
  end
  return false
end
function GameUIKrypton:GetIdxInList(list, id)
  local idx = -1
  if list and #list > 0 then
    for k = 1, #list do
      if tostring(list[k].id) == tostring(id) then
        idx = k
        break
      end
    end
  end
  return idx
end
function GameUIKrypton:RequestRefineList()
  DebugOut("RequestRefineList.")
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_refine_list_req.Code, nil, self.RequestRefineListCallback, true, nil)
end
function GameUIKrypton.RequestRefineListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_list_req.Code then
    DebugOut("RequestRefineListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_list_ack.Code then
    DebugOut("RequestRefineListCallback ok.")
    DebugTable(content)
    GameUIKrypton.mRefineList = content.kryptons
    GameUIKrypton.mRefineNextList = content.up_kryptons
    if GameSettingData.CurRefineKryptonId then
      DebugOut("GameSettingData.CurRefineKryptonId " .. GameSettingData.CurRefineKryptonId)
      GameUIKrypton.mTargetCurIdx = GameUIKrypton:GetIdxInList(GameUIKrypton.mRefineList, GameSettingData.CurRefineKryptonId)
      if -1 == GameUIKrypton.mTargetCurIdx then
        GameSettingData.CurRefineKryptonId = -1
        GameUtils:SaveSettingData()
      end
    end
    GameUIKrypton:EnterRefine()
    return true
  end
  return false
end
function GameUIKrypton:RequestInjectList()
  if -1 == GameUIKrypton.mTargetCurIdx or nil == GameUIKrypton.mRefineList or nil == GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_KRYPTON_ERROR")
    GameTip:Show(tip)
    return
  end
  if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
    local item = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx]
    if item.refine_exp >= item.refine_need_exp then
      local tip = GameLoader:GetGameText("LC_MENU_REFINE_FULL_ENERGY_ERROR")
      GameTip:Show(tip)
      return
    end
  end
  local reqId = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx].id
  if reqId == GameUIKrypton.mCurInjectListForTargetId and GameUIKrypton.mInjectList then
    GameUIKrypton.mSrcCurIdx = GameUIKrypton:GetIdxInList(GameUIKrypton.mInjectList, GameUIKrypton.mSrcCurIdxBackup)
    GameUIKrypton.mSrcCurClickedIdx = GameUIKrypton.mSrcCurIdx
    if -1 == GameUIKrypton.mSrcCurClickedIdx and #GameUIKrypton.mInjectList > 0 then
      GameUIKrypton.mSrcCurClickedIdx = 1
    end
    DebugTable(GameUIKrypton.mInjectList)
    DebugOut("GameUIKrypton.mSrcCurIdx " .. tostring(GameUIKrypton.mSrcCurIdx) .. " GameUIKrypton.mSrcCurIdxBackup " .. tostring(GameUIKrypton.mSrcCurIdxBackup))
    GameUIKrypton:SetSrcSelectList()
    GameUIKrypton:ShowSrcSelectPanel()
  else
    GameUIKrypton.mInjectList = nil
    GameUIKrypton.mSrcCurIdxBackup = -1
    GameUIKrypton.mCurInjectListForTargetId = reqId
    DebugOut("RequestInjectList.")
    local req = {
      refined_id = GameUIKrypton.mCurInjectListForTargetId
    }
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_inject_list_req.Code, req, self.RequestInjectListCallback, true, nil)
  end
end
function GameUIKrypton.RequestInjectListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_inject_list_req.Code then
    DebugOut("RequestInjectListCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_inject_list_ack.Code then
    DebugOut("RequestInjectListCallback ok.")
    DebugTable(content)
    GameUIKrypton.mInjectList = content.kryptons
    GameUIKrypton.mSrcCurIdx = GameUIKrypton:GetIdxInList(GameUIKrypton.mInjectList, GameUIKrypton.mSrcCurIdxBackup)
    GameUIKrypton.mSrcCurClickedIdx = GameUIKrypton.mSrcCurIdx
    if -1 == GameUIKrypton.mSrcCurClickedIdx and #GameUIKrypton.mInjectList > 0 then
      GameUIKrypton.mSrcCurClickedIdx = 1
    end
    GameUIKrypton:SetSrcSelectList()
    GameUIKrypton:ShowSrcSelectPanel()
    GameUIKrypton:SetChoseButtonStates()
    return true
  end
  return false
end
function GameUIKrypton:RequestRefine()
  DebugOut("RequestRefine.")
  if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
    local item = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx]
    if item.refine_exp < item.refine_need_exp then
      local tip = GameLoader:GetGameText("LC_MENU_REFINE_CAN_NOT_REFINE_ERROR")
      GameTip:Show(tip)
      return
    end
    GameUIKrypton.DoRequestRefine(item.id)
  else
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_KRYPTON_ERROR")
    GameTip:Show(tip)
  end
end
function GameUIKrypton.DoRequestRefine(itemId)
  DebugOut("DoRequestRefine")
  local req = {refine_id = itemId}
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_refine_req.Code, req, GameUIKrypton.RequestRefineCallback, true, nil)
end
function GameUIKrypton.RequestRefineCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_refine_req.Code then
    DebugOut("RequestRefineCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_refine_ack.Code then
    DebugOut("RequestRefineCallback ok.")
    DebugTable(content)
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    GameUIKrypton:ResetRefineData()
    GameUIKrypton:RequestRefineInfo()
    return true
  end
  return false
end
function GameUIKrypton:RequestInject()
  if not GameUIKrypton.mInjectList then
    GameUIKrypton.DoRequestInject()
    return
  end
  local item = GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx]
  if item and #item.formation > 0 then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    GameUIMessageDialog:SetYesButton(GameUIKrypton.DoRequestInject)
    local formations = ""
    for i, v in ipairs(item.formation) do
      formations = formations .. FleetMatrix.GetRomaNumber(v.formation_id) .. "/"
    end
    formations = string.sub(formations, 1, -2)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_SAVE_FORMATION_INFO_6") .. formations)
  else
    GameUIKrypton.DoRequestInject()
  end
end
function GameUIKrypton.DoRequestInject()
  DebugOut("RequestInject.")
  if -1 == GameUIKrypton.mTargetCurIdx or nil == GameUIKrypton.mRefineList or nil == GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] or nil == GameUIKrypton.mInjectList or nil == GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx] then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_CONDITION_NOT_MET_ERROR")
    GameTip:Show(tip)
    return
  end
  local isFree = false
  if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] then
    local item = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx]
    if item.rank <= 4 then
      isFree = true
    end
  end
  if GameUIKrypton.mRefineInfo and GameUIKrypton.mRefineInfo.remain_times and GameUIKrypton.mRefineInfo.remain_times <= 0 and not isFree then
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_MAX_TIME_ERROR")
    GameTip:Show(tip)
    return
  end
  if GameUIKrypton.mIsUseItemOn and GameUIKrypton.mRefineInfo and 0 >= GameUIKrypton.mRefineInfo.item_count then
    local tip = GameLoader:GetGameText("LC_MENU_PROPS_INADEQUATE_INFO")
    GameTip:Show(tip)
    return
  end
  if GameUIKrypton.mRefineList and GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx] and GameUIKrypton.mInjectList and GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx] then
    local req = {
      injected_id = GameUIKrypton.mRefineList[GameUIKrypton.mTargetCurIdx].id,
      inject_id = GameUIKrypton.mInjectList[GameUIKrypton.mSrcCurIdx].id,
      with_item = GameUIKrypton.mIsUseItemOn
    }
    GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_inject_req.Code, req, GameUIKrypton.RequestInjectCallback, true, nil)
  else
  end
end
function GameUIKrypton.RequestInjectCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_inject_req.Code then
    DebugOut("RequestInjectCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_inject_ack.Code then
    DebugOut("RequestInjectCallback ok.")
    DebugTable(content)
    local tip = GameLoader:GetGameText("LC_MENU_REFINE_GET_KRYPTON_ENERGY_INFO")
    local _param = {}
    local param = {}
    _param.item_type = "kenergy"
    _param.number = content.kenergy
    _param.no = 1
    _param.level = 0
    table.insert(param, _param)
    ItemBox:ShowRewardBox(param)
    GameUIKrypton.mIsCrit = content.is_critical
    GameUIKrypton:PlayInjectAnimation()
    return true
  end
  return false
end
function GameUIKrypton:RequestPrice()
  local req = {
    price_type = "krypton_inject",
    type = 0
  }
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.price_req.Code, req, self.RequestPriceCallback, true, nil)
end
function GameUIKrypton.RequestPriceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.price_ack.Code then
    DebugOut("GameUIKrypton.RequestCrossPriceCallback")
    DebugTable(content)
    local function okCallback()
      GameUIKrypton:RequestBuy()
    end
    local info = string.format(GameLoader:GetGameText("LC_MENU_REFINE_BUY_INFO"), content.price)
    GameUIGlobalScreen:ShowMessageBox(2, "", info, okCallback, nil)
    return true
  end
  return false
end
function GameUIKrypton:RequestBuy()
  DebugOut("RequestPour.")
  GameUIKrypton:SendMsgWithMatrix(NetAPIList.krypton_inject_buy_req.Code, nil, self.RequestBuyCallback, true, nil)
end
function GameUIKrypton.RequestBuyCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_inject_buy_req.Code then
    DebugOut("RequestBuyCallback err.")
    DebugTable(content)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.krypton_inject_buy_ack.Code then
    DebugOut("RequestBuyCallback ok.")
    DebugTable(content)
    if GameUIKrypton.mRefineInfo then
      GameUIKrypton.mRefineInfo.remain_times = content.remain_times
      GameUIKrypton.mRefineInfo.max_times = content.max_times
      GameUIKrypton:SetItemCurAndMaxCount()
    end
    return true
  end
  return false
end
function GameUIKrypton:GetHelpTutorialPos(...)
  if GameUtils:GetTutorialHelp() then
    self:GetFlashObject():InvokeASCallback("_root", "GetHelpTutorialPos")
  end
end
function GameUIKrypton:GetHelpTutorialCompositePos(...)
  if GameUtils:GetTutorialHelp() then
    self:GetFlashObject():InvokeASCallback("_root", "GetHelpTutorialCompositePos")
  end
end
