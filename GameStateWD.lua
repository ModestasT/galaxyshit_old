local GameStateWD = GameStateManager.GameStateWD
local GameUIWDPlanetSelect = LuaObjectManager:GetLuaObject("GameUIWDPlanetSelect")
local GameUIWDInBattle = LuaObjectManager:GetLuaObject("GameUIWDInBattle")
local GameUIWDEnemyScene = LuaObjectManager:GetLuaObject("GameUIWDEnemyScene")
local GameObjectBattleMapBG = LuaObjectManager:GetLuaObject("GameObjectBattleMapBG")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameWDAttkBox = LuaObjectManager:GetLuaObject("GameWDAttkBox")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
GameStateWD.curMainUIObj = nil
GameStateWD.lastState = nil
GameStateWD.isEnemyIn = false
GameStateWD.lastEnemyState = false
GameStateWD.RestoreAttkBox = false
GameStateWD.ManuallyExit = false
GameStateWD.inActive = false
GameStateWD.firstTimeEnterChat = true
GameStateWD.isCheckHistory = false
GameStateWD.previousWDState = nil
function GameStateWD:InitGameState()
end
function GameStateWD:OnFocusGain(previousState)
  GameStateWD.isEnemyIn = false
  self:ChangeMainWin()
  GameUIBarLeft:HideForWD()
  if GameStateWD.lastEnemyState then
    GameStateWD:EnterEnemyScene()
  end
end
function GameStateWD:OnFocusLost(newState)
  DebugWD("GameStateWD:OnFocusLost()")
  if self.curMainUIObj then
    DebugOut("unload current flash obj")
    self.curMainUIObj:UnloadFlashObject()
  end
  if GameUIWDPlanetSelect:GetFlashObject() then
    DebugOut("release GameUIWDPlanetSelect")
    GameUIWDPlanetSelect:UnloadFlashObject()
  end
  GameStateWD.lastEnemyState = GameStateWD.isEnemyIn
  if GameStateWD.isEnemyIn and GameWDAttkBox.attkID ~= nil then
    GameStateWD.RestoreAttkBox = true
    DebugOut("GameStateWD.RestoreAttkBox became true", GameStateWD.RestoreAttkBox)
  end
  GameStateWD:MoveOutEnemyScene()
  GameUIBarLeft:ShowForWD()
  if GameStateWD.ManuallyExit == true then
    DebugOut("GameStateWD.ManuallyExit")
    GameStateWD.BothAllianceInfo = nil
    GameStateWD.NeedChangeBaseScreen = false
    GameStateWD.BattleResultInfo = nil
    GameStateWD.fetchBattleInfoTime = 0
    GameStateWD.OpponentAllianceDefenceInfo = nil
    GameStateWD.ScoreInfo = nil
    GameStateWD.wdList = nil
    GameStateWD.activeWD = nil
    GameStateWD.fetchWDListTime = 0
    GameObjectMainPlanet.fetchNtfForWD = false
    GameObjectMainPlanet.waitFetchNtfForWD = false
    GameStateWD.ManuallyExit = false
    GameWDAttkBox.attkID = nil
    GameWDAttkBox.isDenfence = false
    GameWDAttkBox.isSelect = 0
    GameStateWD.RestoreAttkBox = false
  end
  self:EraseObject(GameUIWDPlanetSelect)
end
function GameStateWD:ChangeMainWin()
  if self.curMainUIObj ~= nil then
    self:EraseObject(GameUIWDPlanetSelect)
    self.curMainUIObj:UnloadFlashObject()
    self.curMainUIObj = nil
  end
  local currentUI
  if self.WDBaseInfo.State == self.WDState.FREE then
    self.curMainUIObj = GameUIWDPlanetSelect
    currentUI = "GameUIWDPlanetSelect"
  else
    self.curMainUIObj = GameUIWDInBattle
    currentUI = "GameUIWDInBattle"
  end
  DebugOut(currentUI)
  self:AddObject(self.curMainUIObj)
end
function GameStateWD:CheckHistoryInfoWhenBattle()
  DebugOut("GameStateWD:CheckHistoryInfoWhenBattle")
  GameUIWDPlanetSelect.isInBattle = true
  GameStateWD.previousWDState = GameStateWD.WDBaseInfo.State
  GameStateWD.WDBaseInfo.State = GameStateWD.WDState.FREE
  GameStateWD:ChangeMainWin()
  GameStateWD.isCheckHistory = true
end
function GameStateWD:closeHistoryInfoWhenBattle()
  DebugOut("GameStateWD:closeHistoryInfoWhenBattle")
  GameUIWDPlanetSelect.isInBattle = false
  GameStateWD.WDBaseInfo.State = GameStateWD.previousWDState
  GameStateWD:ChangeMainWin()
  GameStateWD.isCheckHistory = false
end
function GameStateWD:GetPreState()
  return self.lastState
end
function GameStateWD:MoveOutEnemyScene()
  if GameStateWD.isEnemyIn then
    GameStateWD.isEnemyIn = false
    self:EraseObject(GameUIWDEnemyScene)
    self:EraseObject(GameObjectBattleMapBG)
    GameUIWDEnemyScene:UnloadFlashObject()
    GameObjectBattleMapBG:UnloadFlashObject()
    self:EraseObject(GameUIBarLeft)
    self:EraseObject(GameUIBarRight)
    if GameStateWD:IsObjectInState(GameWDAttkBox) then
      self:EraseObject(GameWDAttkBox)
    end
    if GameStateWD:IsObjectInState(GameUIWDStuff) then
      self:EraseObject(GameUIWDStuff)
    end
    if GameStateWD:IsObjectInState(GameUIFriend) then
      self:EraseObject(GameUIFriend)
    end
    DebugOut("nlaooooooo")
    if GameStateWD:IsObjectInState(GameUIWDInBattle) then
      GameUIWDInBattle:SetUIInfo()
    end
  end
end
function GameStateWD:EnterEnemyScene()
  if not GameStateWD.isEnemyIn then
    GameStateWD.isEnemyIn = true
    self:AddObject(GameObjectBattleMapBG)
    self:AddObject(GameUIWDEnemyScene)
    self:AddObject(GameUIBarLeft)
    self:AddObject(GameUIBarRight)
    GameUIBarRight:ShowGalaxyIcon(false)
    GameObjectBattleMapBG:LoadBGMap(1, 1)
  end
end
GameStateWD.WDState = {
  FREE = 0,
  NO_DECLARATION = 1,
  WAITING_OPPONENTS = 2,
  BATTLE = 3
}
GameStateWD.WDBaseInfo = {}
GameStateWD.WDBaseInfo.State = GameStateWD.WDState.FREE
GameStateWD.WDBaseInfo.WaitingOpponentTime = nil
GameStateWD.WDBaseInfo.WaitingOpponentFetchTime = nil
GameStateWD.BothAllianceInfo = nil
GameStateWD.NeedChangeBaseScreen = false
function GameStateWD.OnBothAllianceInfoChange(content)
  DebugWD("OnBothAllianceInfoChange")
  DebugWD("GameStateWD.BothAllianceInfo")
  DebugWDTable(content)
  if GameStateWD.BothAllianceInfo == nil then
    GameStateWD.BothAllianceInfo = LuaUtils:table_rcopy(content)
    GameStateWD.WDBaseInfo.State = GameStateWD.BothAllianceInfo.status
    DebugWD("--------------GameStateWD.BothAllianceInfo.status", GameStateWD.BothAllianceInfo.status)
  else
    local oldState = GameStateWD.WDBaseInfo.State
    GameStateWD.BothAllianceInfo = LuaUtils:table_rcopy(content)
    GameStateWD.WDBaseInfo.State = GameStateWD.BothAllianceInfo.status
    DebugWD("oldState", oldState)
    DebugWD("GameStateWD.BothAllianceInfo.status", GameStateWD.BothAllianceInfo.status)
    DebugWD(GameStateManager:GetCurrentGameState():IsObjectInState(GameUIWDInBattle))
    if oldState ~= GameStateWD.WDBaseInfo.State and GameStateManager:GetCurrentGameState():IsObjectInState(GameUIWDInBattle) then
      if GameStateWD:AllDataFetched() and GameStateWD:OpponentDataFetched() or GameStateWD.WDBaseInfo.State ~= GameStateWD.WDState.BATTLE then
        if oldState ~= GameStateWD.WDState.BATTLE then
          GameUIWDInBattle:ChangeBaseScreen()
        else
          GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
          GameUIMessageDialog:SetOkButton(function()
            GameStateWD.lastEnemyState = false
            GameStateWD:MoveOutEnemyScene()
            GameUIWDInBattle:ChangeBaseScreen()
          end)
          GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_VERIFY"), GameLoader:GetGameText("LC_MENU_WD_BATTLE_FIELD_END_CONFIRM"))
        end
      else
        GameStateWD.NeedChangeBaseScreen = true
      end
    end
  end
  GameStateWD.WDBaseInfo.WaitingOpponentTime = content.cost_seconds
  GameStateWD.WDBaseInfo.WaitingOpponentFetchTime = os.time()
end
GameStateWD.BattleResultInfo = nil
GameUIWDInBattle.BattleCoreLoseCount = "0"
function GameStateWD.OnBattleResultChange(content)
  DebugWD("OnBattleResultChange")
  DebugWD("GameStateWD.BattleResultInfo ")
  DebugWDTable(content)
  if GameStateWD.BattleResultInfo == nil then
    GameStateWD.BattleResultInfo = LuaUtils:table_rcopy(content)
  else
    for i, v in ipairs(content.users) do
      local inOldList = false
      for k, j in ipairs(GameStateWD.BattleResultInfo.users) do
        if v.user_id == j.user_id then
          GameStateWD.BattleResultInfo.users[k] = LuaUtils:table_rcopy(v)
          inOldList = true
          break
        end
      end
      if not inOldList then
        table.insert(GameStateWD.BattleResultInfo.users, LuaUtils:table_rcopy(v))
      end
    end
    GameUIWDEnemyScene:RefreshTarget()
  end
  GameUIWDInBattle.enabledBattleForDisplay = nil
  GameUIWDInBattle.enabledBattleForDisplay = {}
  for i, v in ipairs(GameStateWD.BattleResultInfo.users) do
    if (v.point > 0 or 0 < v.win_count or 0 < v.lose_count) and v.user_id > 1000 then
      table.insert(GameUIWDInBattle.enabledBattleForDisplay, LuaUtils:table_rcopy(v))
    end
    if v.user_id == 2 and 0 < v.lose_count then
      GameUIWDInBattle.BattleCoreLoseCount = v.lose_count
      GameWDAttkBox:SetDefenceCoreAttackedNum(GameUIWDInBattle.BattleCoreLoseCount)
    end
  end
  local SortCallback = function(a, b)
    if a.point > b.point then
      return true
    else
      return false
    end
  end
  table.sort(GameUIWDInBattle.enabledBattleForDisplay, SortCallback)
  DebugOut("should refresh wd")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIWDInBattle) then
    DebugOut("not refresh")
    GameUIWDInBattle:SetUIInfo()
  end
end
function GameStateWD:GetBattleMemberResult(userID)
  if not GameStateWD.BattleResultInfo or not GameStateWD.BattleResultInfo.users or not GameStateWD.BattleResultInfo.users[userID] then
    return nil
  else
    return GameStateWD.BattleResultInfo.users[userID].win_count, GameStateWD.BattleResultInfo.users[userID].lose_count, GameStateWD.BattleResultInfo.users[userID].point
  end
end
function GameStateWD:GetUserInfoByID(userID)
  if GameStateWD.BothAllianceInfo and #GameStateWD.BothAllianceInfo.infoes >= 2 then
    DebugWD(userID)
    for i, v in ipairs(GameStateWD.BothAllianceInfo.infoes[1].members) do
      if tostring(v.id) == tostring(userID) then
        return v
      end
    end
    for i, v in ipairs(GameStateWD.BothAllianceInfo.infoes[2].members) do
      if tostring(v.id) == tostring(userID) then
        return v
      end
    end
  end
  return nil
end
function GameStateWD:GetUserBattleInfoByID(userID)
  if GameStateWD.BattleResultInfo then
    for i, v in ipairs(GameStateWD.BattleResultInfo.users) do
      if tostring(v.user_id) == tostring(userID) then
        return v
      end
    end
  end
  return nil
end
function GameStateWD:GetMyAllianceWin()
end
GameStateWD.fetchBattleInfoTime = 0
GameStateWD.OpponentAllianceDefenceInfo = nil
function GameStateWD.OnOpponentDefenceInfoChange(content)
  DebugWD("OnOpponentDefenceInfoChange")
  DebugWD("GameStateWD.OpponentAllianceDefenceInfo")
  DebugWDTable(content)
  GameStateWD.OpponentAllianceDefenceInfo = LuaUtils:table_rcopy(content.info)
  GameStateWD.fetchBattleInfoTime = os.time()
  GameUIWDEnemyScene:RefreshDefendHP()
  GameUIWDEnemyScene:RefreshTarget()
  if GameStateWD:IsObjectInState(GameUIWDInBattle) then
    DebugOut("set in battle ui info")
    GameUIWDInBattle:SetUIInfo()
  end
end
GameStateWD.wdList = nil
GameStateWD.activeWD = nil
GameStateWD.fetchWDListTime = 0
function GameStateWD:TryFetchWDList()
  DebugOut("beging to fetch", self.wdList)
  if not self.wdList then
    local content = {id = 0}
    NetMessageMgr:SendMsg(NetAPIList.wd_star_req.Code, content, GameStateWD.FetchWDListCallback, false, nil)
  end
end
function GameStateWD:InsertTestWDData()
  local preListLength = #GameUIWDPlanetSelect.BattleList.Item
  for i = 1, 30 do
    local item = {
      flag = 0,
      alliance_leader = "null",
      alliance_name = "null",
      alliance_id = 0,
      extra = "1",
      left_time = 0,
      id = 1,
      fight_name = "italy"
    }
    item.id = i + preListLength
    table.insert(GameUIWDPlanetSelect.BattleList.Item, item)
  end
  DebugOut("InsertTestWDData:")
  DebugTable(GameUIWDPlanetSelect.BattleList.Item)
end
function GameStateWD.FetchWDListCallback(msgType, content)
  if msgType == NetAPIList.wd_star_ack.Code then
    DebugOut("fuck_FetchWDListCallback", msgType)
    DebugTable(content)
    GameStateWD.fetchWDListTime = os.time()
    GameStateWD.wdList = LuaUtils:table_rcopy(content.infoes)
    GameUIWDPlanetSelect.BattleList.Item = LuaUtils:table_rcopy(content.infoes)
    local haveActiveBattle = false
    local finishedMaxWDIndex = 0
    for i, v in ipairs(GameStateWD.wdList) do
      if 0 < v.left_time then
        GameStateWD.activeWD = LuaUtils:table_rcopy(v)
        haveActiveBattle = true
        if 0 < v.left_time then
          GameStateWD.inActive = true
        end
        GameObjectMainPlanet.isShowWDEndTime = v.left_time + os.time()
      end
      if v.left_time == 0 and finishedMaxWDIndex < v.id then
        finishedMaxWDIndex = v.id
      end
    end
    DebugWD(GameSettingData.Last_WD_Result_ID)
    DebugWD(finishedMaxWDIndex)
    if (GameSettingData.Last_WD_Result_ID == nil or finishedMaxWDIndex > GameSettingData.Last_WD_Result_ID) and GameStateWD:IfWDEnabled() and finishedMaxWDIndex > 0 then
      GameSettingData.Last_WD_Result_ID = finishedMaxWDIndex
      GameUtils:SaveSettingData()
      GameNotice:AddNotice("WDResult")
    end
    if haveActiveBattle and GameStateWD:IfWDEnabled() then
      local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
      GameStateMainPlanet.WDActive = true
      local flash = GameObjectMainPlanet:GetFlashObject()
      if flash then
        flash:InvokeASCallback("_root", "ShowTutorialWorld")
      end
    end
    DebugWD(not GameStateWD:IfAllianceEnabled())
    DebugWD(GameStateWD.BothAllianceInfo)
    if GameStateWD.BothAllianceInfo then
      if not GameStateWD:IfAllianceEnabled() then
        GameStateWD.WDBaseInfo.State = GameStateWD.WDState.NO_DECLARATION
      else
        GameStateWD.WDBaseInfo.State = GameStateWD.BothAllianceInfo.status
      end
    elseif haveActiveBattle then
      GameStateWD.WDBaseInfo.State = GameStateWD.WDState.NO_DECLARATION
    else
      GameStateWD.WDBaseInfo.State = GameStateWD.WDState.FREE
    end
    return true
  end
  return false
end
function GameStateWD:HaveAlliance()
  if not GameStateWD:IfAllianceEnabled() then
    return false
  else
    local alliance = GameGlobalData:GetData("alliance")
    if alliance.id == "" or alliance.id == 0 then
      return false
    else
      return true
    end
  end
end
function GameStateWD:IfAllianceEnabled()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local enable = false
  for k, v in pairs(rightMenu) do
    if v.name == "alliance" then
      enable = v.status
      break
    end
  end
  return enable
end
function GameStateWD:IfWDEnabled()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local enable = false
  for k, v in pairs(rightMenu) do
    if v.name == "wd" then
      enable = v.status
      break
    end
  end
  return enable
end
function GameStateWD:GetIconFrame(item)
  if item and item.id ~= -1 then
    if item.left_time == 0 then
      local fileName = "data2/LAZY_LOAD_WD_" .. item.fight_name .. ".png"
      if ext.crc32.crc32(fileName) ~= "" and ext.crc32.crc32(fileName) ~= nil then
        return item.fight_name
      elseif AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_WD_" .. item.fight_name .. ".png") then
        return item.fight_name
      end
      return "default"
    end
    return item.fight_name
  else
    return "unknown"
  end
end
function GameStateWD:GetAlliancePoints(myAllianceOrNot)
  local points = 0
  if not GameStateWD.BattleResultInfo then
    return points
  end
  for i, v in ipairs(GameStateWD.BattleResultInfo.users) do
    if myAllianceOrNot and not v.is_defender then
      points = points + v.point
    elseif not myAllianceOrNot and v.is_defender then
      points = points + v.point
    end
  end
  return points
end
function GameStateWD:GetAllianceWins(myAllianceOrNot)
  local win = 0
  if not GameStateWD.BattleResultInfo then
    return win
  end
  for i, v in ipairs(GameStateWD.BattleResultInfo.users) do
    if myAllianceOrNot and not v.is_defender then
      win = win + v.win_count
    elseif not myAllianceOrNot and v.is_defender then
      win = win + v.win_count
    end
  end
  return win
end
function GameStateWD:AllDataFetched()
  local GameUIWDDefence = LuaObjectManager:GetLuaObject("GameUIWDDefence")
  if GameStateWD.BothAllianceInfo and GameStateWD.BattleResultInfo and GameUIWDDefence.DefenceInfo and GameStateWD.ScoreInfo then
    return true
  else
    return false
  end
end
function GameStateWD:OpponentDataFetched()
  if GameStateWD.OpponentAllianceDefenceInfo then
    return true
  else
    return false
  end
end
GameStateWD.ScoreInfo = nil
function GameStateWD.OnScoreChange(content)
  DebugWD("GameStateWD.OnScoreChange")
  DebugWDTable(content)
  GameStateWD.ScoreInfo = LuaUtils:table_rcopy(content)
end
function GameStateWD.CheckAndTryToRemoveEnemyUI(gs)
  if GameStateWD.WDBaseInfo.State ~= GameStateWD.WDState.BATTLE and gs == GameStateWD then
    GameStateWD.isEnemyIn = false
    GameStateWD.lastEnemyState = false
    GameStateWD:MoveOutEnemyScene()
  end
end
