local half_portrait_resource = {
  classify = {
    [1] = {
      [1] = "LAZY_LOAD_Half_Portrait_k1_atlas10100",
      [2] = "LAZY_LOAD_Half_Portrait_k1_atlas10114"
    },
    [2] = {
      [1] = "LAZY_LOAD_Half_Portrait_k2_atlas10200",
      [2] = "LAZY_LOAD_Half_Portrait_k2_atlas10224"
    },
    [3] = {
      [1] = "LAZY_LOAD_Half_Portrait_k3_atlas10300"
    },
    [4] = {
      [1] = "LAZY_LOAD_Half_Portrait_k4_atlas10400",
      [2] = "LAZY_LOAD_Half_Portrait_k4_atlas10430"
    },
    [5] = {
      [1] = "LAZY_LOAD_Half_Portrait_k5_atlas10500"
    },
    [6] = {},
    [7] = {
      [1] = "LAZY_LOAD_Half_Portrait_k7_atlas10700"
    },
    [8] = {
      [1] = "LAZY_LOAD_Half_Portrait_k8_atlas10800"
    }
  }
}
return half_portrait_resource
