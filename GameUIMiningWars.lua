require("GameTextEdit.tfl")
local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
local GameUIMiningWorld = LuaObjectManager:GetLuaObject("GameUIMiningWorld")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameStateMiningWorld = GameStateManager.GameStateMiningWorld
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
GameUIMiningWars.basicData = nil
GameUIMiningWars.popData = nil
GameUIMiningWars.curUI = "main"
GameUIMiningWars.curOpenId = -1
GameUIMiningWars.curMapId = -1
GameUIMiningWars.onAddStateCallBack = nil
GameUIMiningWars.showTabType = 1
GameUIMiningWars.isGroup = 1
local _fleetStatu = {
  none = 0,
  prepare = 1,
  leader = 2,
  member = 3
}
function GameUIMiningWars:OnInitGame()
end
function GameUIMiningWars:Show()
  GameUIMiningWars:ReqMiningWarData()
  GameStateManager:GetCurrentGameState():AddObject(GameUIMiningWars)
end
function GameUIMiningWars:OnAddToGameState()
  DebugOut("GameUIMiningWars:OnAddToGameState")
  GameUIMiningWars.curUI = "main"
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
  GameUIMiningWars:initText()
  GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "InitFlashUI", GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_" .. GameUIMiningWars.curMapId))
  GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "MoveIn")
  if GameUIMiningWars.onAddStateCallBack then
    GameUIMiningWars.onAddStateCallBack()
    GameUIMiningWars.onAddStateCallBack = nil
  end
  RegisterKeyboardHideNotify(GameUIMiningWars.onKeyboardHide)
  GameUIMiningWars:setFleetStatu(GameUIMiningWars.clickFleetStatu)
end
function GameUIMiningWars:OnEraseFromGameState(state)
  RemoveKeyboardHideNotify(GameUIMiningWars.onKeyboardHide)
  self:UnloadFlashObject()
end
function GameUIMiningWars:initText()
  local data = {}
  data.PLANETCRAFT_OWNER = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_OWNER")
  data.PLANETCRAFT_ROB_COST = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ROB_COST")
  data.PLANETCRAFT_PLANET_OUTPUT = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_OUTPUT")
  data.PLANETCRAFT_BUTTON_ROB = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_BUTTON_ROB")
  data.PLANETCRAFT_BUTTON_RETREAT = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_BUTTON_RETREAT")
  data.PLANETCRAFT_BUTTON_RUSH = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_BUTTON_RUSH")
  data.PLANETCRAFT_TODAY_REMAINING = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_TODAY_REMAINING")
  data.PLANETCRAFT_UNKNOWN = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_UNKNOWN")
  data.SW_PLANET_TOTLE_INCOME = GameLoader:GetGameText("LC_MENU_SW_PLANET_TOTLE_INCOME")
  data.SW_PLANET_BATTLE_INCOME = GameLoader:GetGameText("LC_MENU_SW_PLANET_BATTLE_INCOME")
  GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "initText", data)
end
function GameUIMiningWars:OnEraseFromGameState()
  GameUIMiningWars.basicData = nil
  GameUIMiningWars.popData = nil
  GameUIMiningWars.showTabType = 1
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUIMiningWars.statusChangeNtf(content)
  local item = {}
  item.id = content.star_result.star_info.id
  item.pos = GameUIMiningWars:GetPosByStarId(item.id)
  item.leftTime = content.star_result.star_info.occupie_left_time
  item.flag = content.star_result.star_info.flag
  item.isMy = content.star_result.star_info.is_my
  item.isOccupied = content.star_result.star_info.is_occupied
  item.baseTime = os.time()
  item.starLevel = content.star_result.star_info.star_level
  item.percent = content.star_result.star_info.percent
  item.starFrame = tonumber(content.star_result.star_info.star_frame)
  item.starName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_TYPE_" .. content.star_result.star_info.name_key)
  item.produceText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_OUTPUT")
  item.consumeText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ROB_COST")
  item.occupieText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_OWNER")
  item.battleItems = {}
  for k, v in ipairs(content.star_result.star_info.battle_limit or {}) do
    local tmp = {}
    tmp.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
    tmp.maxDisplayCount = GameUtils.numberConversion(math.floor(v.max))
    tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    tmp.model = v
    item.battleItems[#item.battleItems + 1] = tmp
  end
  if GameUIMiningWars:GetStarInfoByStarId(item.id) then
    local k = GameUIMiningWars:GetIndexByStarPos(item.pos)
    GameUIMiningWars.basicData.starList[k] = item
  end
  if content.status == 2 and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMiningWorld then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ONLINE_ROBED_TIP"))
    if GameUIMiningWorld:GetFlashObject() then
      GameUIMiningWorld:ReqMiningWorldData()
    end
  end
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj and GameUIMiningWars:GetStarInfoByStarId(item.id) then
    flashObj:InvokeASCallback("_root", "UpdateStarInfoByPos", item.pos, item)
  end
  if GameUIMiningWars.curUI == "pop" and content.star_result.star_info.id == GameUIMiningWars.curOpenId then
    GameUIMiningWars:GenerateStarData(content.star_result)
  end
end
function GameUIMiningWars._OnTimerTick()
  if GameUIMiningWars.basicData then
    local left = GameUIMiningWars.basicData.refreshLeftTime - (os.time() - GameUIMiningWars.basicData.baseTime)
    local str = ""
    local data = {}
    data.isFree = false
    data.starTimeStr = {}
    if left <= 0 then
      left = 0
      data.isFree = true
      data.timeStr = ""
    else
      data.timeStr = GameUtils:formatTimeStringAsPartion2(left)
    end
    local needRefresh = false
    for k, v in pairs(GameUIMiningWars.basicData.starList) do
      local aleft = v.leftTime - (os.time() - v.baseTime)
      local str = ""
      if aleft > 0 then
        str = GameUtils:formatTimeStringAsPartion2(aleft)
        v.needRefresh = true
      elseif v.needRefresh and aleft < 0 then
        needRefresh = true
        v.needRefresh = false
        v = GameUIMiningWars:ResertStarInfo(v)
        local flashObj = GameUIMiningWars:GetFlashObject()
        if flashObj and GameUIMiningWars:GetStarInfoByStarId(v.id) then
          flashObj:InvokeASCallback("_root", "UpdateStarInfoByPos", v.pos, v)
        end
        if GameUIMiningWars.curUI == "pop" and v.id == GameUIMiningWars.curOpenId then
          GameUIMiningWars.popData = GameUIMiningWars:ResertStarInfo(GameUIMiningWars.popData)
          local isleader = 0
          if GameUIMiningWars.clickFleetStatu == _fleetStatu.leader or GameUIMiningWars.clickFleetStatu == _fleetStatu.none then
            isleader = 1
          end
          local showgroup = 1
          if GameUIMiningWars.isGroup == 0 then
            showgroup = 0
          end
          GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "ShowStarInfo", GameUIMiningWars.popData, GameUIMiningWars.showTabType, showgroup, isleader)
        end
      end
      local item = {}
      item.pos = v.pos
      item.timeStr = str
      data.starTimeStr[#data.starTimeStr + 1] = item
    end
    if needRefresh then
      GameUIMiningWars:RefreshBasicData()
    end
    if GameUIMiningWars:GetFlashObject() then
      GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "setUpdateTime", data)
    end
  end
  GameUIMiningWars._OnResTimerTick()
  if GameStateMiningWorld:IsObjectInState(GameUIMiningWars) then
    return 1000
  end
  return nil
end
function GameUIMiningWars._OnResTimerTick()
  if GameUIMiningWars.curUI == "pop" and GameUIMiningWars.popData and GameUIMiningWars.popData.isOccupied then
    for k, v in pairs(GameUIMiningWars.popData.baseItems or {}) do
      local base = GameUIMiningWars:GetStarInfoByStarId(GameUIMiningWars.popData.id)
      if base.leftTime - (os.time() - base.baseTime) > 0 then
        local baseCount = GameHelper:GetAwardCount(v.model.item_type, v.model.number, v.model.no)
        if not GameHelper:IsResource(v.model.item_type) then
          baseCount = baseCount / 1000
        end
        v.count = baseCount + GameUIMiningWars.popData.produceItems[k].count / 3600 * (os.time() - v.baseTime)
        v.displayCount = GameUtils.numberConversion(v.count)
        if not GameUIMiningWars.popData.isMy and GameUIMiningWars.popData.needItems[k] then
          GameUIMiningWars.popData.needItems[k].count = v.count * (GameUIMiningWars.popData.percent / 1000)
          GameUIMiningWars.popData.needItems[k].displayCount = GameUtils.numberConversion(math.floor(GameUIMiningWars.popData.needItems[k].count))
        end
        local flashObj = GameUIMiningWars:GetFlashObject()
        if flashObj then
          if not GameUIMiningWars.popData.isMy and GameUIMiningWars.popData.needItems[k] then
            flashObj:InvokeASCallback("_root", "UpdateResourceData", k, v.displayCount, GameUIMiningWars.popData.needItems[k].displayCount)
          else
            flashObj:InvokeASCallback("_root", "UpdateResourceData", k, v.displayCount, "0")
          end
        end
      end
    end
  end
end
function GameUIMiningWars:ResertStarInfo(item)
  item.leftTime = 0
  item.isOccupied = false
  item.isMy = false
  item.flag = ""
  item.occupieName = ""
  item.allianceName = ""
  item.battleItems = {}
  return item
end
function GameUIMiningWars:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "close" then
    GameUIMiningWorld:Show()
    GameStateManager:GetCurrentGameState():EraseObject(GameUIMiningWars)
  elseif cmd == "add_count" then
    GameUIMiningWars:OnBuyCount()
  elseif cmd == "refresh" then
    GameUIMiningWars:OnRefresh()
  elseif cmd == "touchStar" then
    GameUIMiningWars:OnTouchStar(tonumber(arg))
    GameUIMiningWars:RefreshBasicData()
  elseif cmd == "ShowPop" then
    GameUIMiningWars.curUI = "pop"
  elseif cmd == "ClosePop" then
    GameUIMiningWars.curUI = "main"
    GameUIMiningWars.popData = nil
    GameUIMiningWars.curOpenId = -1
    GameUIMiningWars.showTabType = 1
  elseif cmd == "occupy" then
    local starID, playerID, logicID = unpack(LuaUtils:string_split(arg, ":"))
    GameUIMiningWars:OnOccupy(tonumber(starID), tonumber(playerID), tonumber(logicID))
  elseif cmd == "collect" then
    GameUIMiningWars:OnCollect(tonumber(arg))
  elseif cmd == "Switch_Tab" then
    GameUIMiningWars.showTabType = tonumber(arg)
  elseif cmd == "show_fleet" then
    GameUIMiningWars:OnClickFleet()
  elseif cmd == "show_report" then
    GameUIMiningWars:OnClickShowReport()
  elseif cmd == "onClick_report" then
    local nid, nround = unpack(LuaUtils:string_split(arg, ":"))
    GameUIMiningWars:OnClickReportItem(nid, nround)
  elseif cmd == "click_info" then
    local power, count = unpack(LuaUtils:string_split(arg, ":"))
    GameUIMiningWars:OnClickInfo(tonumber(power), tonumber(count))
  elseif cmd == "click_prepare" then
    GameUIMiningWars:OnClickPrepare()
  elseif cmd == "onClick_army_list" then
    local id = tonumber(arg)
    GameUIMiningWars:onClick_army_list(id)
  elseif cmd == "changeOrder" then
    local orders = LuaUtils:string_split(arg, ":")
    GameUIMiningWars:onChangeOrder(orders)
  elseif cmd == "ShowDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    local item
    if itemType == "produce" then
      item = GameUIMiningWars.popData.produceItems[tonumber(index)].model
    elseif itemType == "needItems" then
      item = GameUIMiningWars.popData.needItems[tonumber(index)].model
    elseif itemType == "battle" then
      item = GameUIMiningWars.popData.battleItems[tonumber(index)].model
    end
    if item and item.item_type == "item" then
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    elseif item and item.item_type == "fleet" then
      local fleetID = item.number
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  end
end
function GameUIMiningWars:ReqMiningWarData()
  local param = {}
  param.id = GameUIMiningWars.curMapId
  if param.id == 5 then
    GameUIMiningWars.isGroup = 1
  else
    GameUIMiningWars.isGroup = 0
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_req.Code, param, GameUIMiningWars.miningWarsCallBack, true, nil)
end
function GameUIMiningWars:RefreshBasicData()
  local param = {}
  param.id = GameUIMiningWars.curMapId
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_req.Code, param, GameUIMiningWars.miningWarsRefreshCallBack, false, nil)
end
function GameUIMiningWars.miningWarsCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_ack.Code then
    GameUIMiningWars:GenerateData(content)
    GameUIMiningWars:SetBasicData()
    return true
  end
  return false
end
function GameUIMiningWars.miningWarsRefreshCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_ack.Code then
    GameUIMiningWars:GenerateData(content)
    local flashObj = GameUIMiningWars:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "RefreshBasicData", GameUIMiningWars.basicData)
    end
    return true
  end
  return false
end
function GameUIMiningWars:GenerateData(content)
  local data = {}
  data.titleText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_MAP_" .. GameUIMiningWars.curMapId)
  data.leftCount = content.left_count
  data.maxCount = content.max_count
  data.refreshLeftTime = content.refresh_left_time
  data.baseTime = os.time()
  data.refreshCredit = content.refresh_credit
  data.buyCount = content.buy_count
  data.buyCredit = content.buy_count_credit
  data.starList = {}
  data.layer = content.layer_frame or 1
  for k, v in ipairs(content.star_list) do
    local item = {}
    item.id = v.id
    item.pos = v.pos
    item.leftTime = v.occupie_left_time
    item.flag = v.flag
    item.allianceName = v.alliance_name
    item.isMy = v.is_my
    item.isOccupied = v.is_occupied
    item.baseTime = os.time()
    item.starLevel = v.star_level
    item.playerId = v.player_id
    item.logicId = v.logic_id
    item.starFrame = v.star_frame
    item.percent = v.percent
    item.starName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_TYPE_" .. v.name_key)
    item.battleItems = {}
    for k1, v1 in ipairs(v.battle_limit or {}) do
      local tmp = {}
      tmp.count = GameHelper:GetAwardCount(v1.item_type, v1.number, v1.no)
      tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
      tmp.maxDisplayCount = GameUtils.numberConversion(math.floor(v1.max))
      tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v1)
      tmp.model = v1
      item.battleItems[#item.battleItems + 1] = tmp
    end
    if item.leftTime <= 0 then
      item = GameUIMiningWars:ResertStarInfo(item)
    end
    data.starList[#data.starList + 1] = item
    if #data.starList == 20 then
      break
    end
  end
  GameUIMiningWars.basicData = data
end
function GameUIMiningWars:SetBasicData()
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetBasicData", GameUIMiningWars.basicData, GameUIMiningWars.isGroup)
    GameUIMiningWars._OnTimerTick()
  end
end
function GameUIMiningWars:OnBuyCount()
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    local param = {
      block = GameUIMiningWars.curMapId
    }
    NetMessageMgr:SendMsg(NetAPIList.mining_wars_buy_count_req.Code, param, GameUIMiningWars.buyCountCallBack, true, nil)
  end)
  local contentText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_RUSH")
  local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
  contentText = string.gsub(contentText, "<credits_num>", tostring(GameUIMiningWars.basicData.buyCredit))
  GameUIMessageDialog:Display(titleText, contentText)
end
function GameUIMiningWars.buyCountCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_buy_count_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_buy_count_ack.Code then
    GameUIMiningWars.basicData.leftCount = content.left_count
    GameUIMiningWars.basicData.buyCredit = content.next_buy_credit
    GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "setLeftCount", GameUIMiningWars.basicData.leftCount, GameUIMiningWars.basicData.maxCount)
    return true
  end
  return false
end
function GameUIMiningWars:OnRefresh()
  if GameUIMiningWars.basicData.refreshLeftTime - (os.time() - GameUIMiningWars.basicData.baseTime) <= 0 then
    local param = {}
    param.id = GameUIMiningWars.curMapId
    NetMessageMgr:SendMsg(NetAPIList.mining_wars_refresh_req.Code, param, GameUIMiningWars.buyRefreshCallBack, true, nil)
  else
    local info = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_REFRESH")
    info = string.gsub(info, "<credits_num>", tostring(GameUIMiningWars.basicData.refreshCredit))
    GameUtils:CreditCostConfirm(info, function()
      local param = {}
      param.id = GameUIMiningWars.curMapId
      NetMessageMgr:SendMsg(NetAPIList.mining_wars_refresh_req.Code, param, GameUIMiningWars.buyRefreshCallBack, true, nil)
    end)
  end
end
function GameUIMiningWars.buyRefreshCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_refresh_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_ack.Code then
    GameUIMiningWars:GenerateData(content)
    GameUIMiningWars:SetBasicData()
    return true
  end
  return false
end
function GameUIMiningWars:OnTouchStar(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_star_req.Code, param, GameUIMiningWars.reqStarCallBack, true, nil)
end
function GameUIMiningWars.reqStarCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_star_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_star_ack.Code then
    DebugOut("NetAPIList.mining_wars_star_ack.Code")
    GameUIMiningWars:GenerateStarData(content)
    GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "MoveIn_Pop")
    return true
  end
  return false
end
function GameUIMiningWars:GenerateStarData(content)
  local data = {}
  DebugOut("GenerateStarData:")
  DebugTable(content)
  data.id = content.star_info.id
  data.isMy = content.star_info.is_my
  data.isOccupied = content.star_info.is_occupied
  data.occupieName = content.star_info.occupie_name == "" and "" or content.star_info.occupie_name
  data.allianceName = content.star_info.alliance_name and content.star_info.alliance_name or ""
  data.playerId = content.star_info.player_id
  data.logicId = content.star_info.logic_id
  data.flag = content.star_info.flag
  data.baseTime = os.time()
  data.percent = content.star_info.percent
  data.starName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_TYPE_" .. content.star_info.name_key)
  data.produceText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_OUTPUT")
  data.consumeText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ROB_COST")
  data.occupieText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_OWNER")
  data.battleItems = {}
  for k, v in ipairs(content.star_info.battle_limit or {}) do
    local tmp = {}
    tmp.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
    tmp.maxDisplayCount = GameUtils.numberConversion(math.floor(v.max))
    tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    tmp.model = v
    data.battleItems[#data.battleItems + 1] = tmp
  end
  data.needItems = {}
  for k, v in pairs(content.consume_item or {}) do
    local item = {}
    item.name = GameHelper:GetAwardText(v.item_type, v.number, v.no)
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    if not GameHelper:IsResource(v.item_type) then
      item.count = item.count / 1000
    end
    item.displayCount = GameUtils.numberConversion(math.floor(item.count))
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.model = v
    item.baseTime = os.time()
    data.needItems[#data.needItems + 1] = item
  end
  data.produceItems = {}
  for k, v in pairs(content.produce_item or {}) do
    local item = {}
    item.name = GameHelper:GetAwardText(v.item_type, v.number, v.no)
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    if not GameHelper:IsResource(v.item_type) then
      item.count = item.count / 1000
    end
    item.displayCount = GameUtils.numberConversion(item.count)
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.model = v
    item.baseTime = os.time()
    data.produceItems[#data.produceItems + 1] = item
  end
  DebugOut("qj data.produceItems:")
  DebugTable(data.produceItems)
  data.baseItems = {}
  for k, v in pairs(content.base_item or {}) do
    local item = {}
    item.name = GameHelper:GetAwardText(v.item_type, v.number, v.no)
    item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    if not GameHelper:IsResource(v.item_type) then
      item.count = item.count / 1000
    end
    item.displayCount = GameUtils.numberConversion(item.count)
    item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    item.model = v
    item.baseTime = os.time()
    data.baseItems[#data.baseItems + 1] = item
  end
  GameUIMiningWars.popData = data
  local isleader = 0
  if GameUIMiningWars.clickFleetStatu == _fleetStatu.leader or GameUIMiningWars.clickFleetStatu == _fleetStatu.none then
    isleader = 1
  end
  local showgroup = 1
  if GameUIMiningWars.isGroup == 0 then
    showgroup = 0
  end
  if GameUIMiningWars:GetFlashObject() ~= nil then
    GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "ShowStarInfo", data, GameUIMiningWars.showTabType, showgroup, isleader)
  end
  GameUIMiningWars.curOpenId = data.id
end
function GameUIMiningWars:StartBattle(reportData, success, battleResult)
  local battle_report_data
  if GameObjectBattleReplay.GroupBattleReportArr[1] and #GameObjectBattleReplay.GroupBattleReportArr[1].headers > 0 then
    battle_report_data = GameObjectBattleReplay.GroupBattleReportArr[1].headers
  else
    error("no battle report")
  end
  local lastGameState = GameStateManager:GetCurrentGameState()
  GameStateBattlePlay.curBattleType = "starwar"
  GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report)
  GameStateBattlePlay:RegisterOverCallback(function()
    GameUIBattleResult:LoadFlashObject()
    GameUIBattleResult:SetFightReport(GameObjectBattleReplay.GroupBattleReportArr, nil, nil, true)
    local data = {}
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      elseif GameUtils:IsChinese() and GameUtils:IsSimplifiedChinese() then
        lang = "zh"
      end
    end
    data.lang = lang
    data.isChinese = GameUtils:IsChinese()
    data.isWin = battleResult.is_win
    data.starFrame = tonumber(battleResult.star_frame)
    data.consumeList = {}
    for k, v in pairs(battleResult.consume_item or {}) do
      local item = {}
      item.name = GameHelper:GetAwardText(v.item_type, v.number, v.no)
      item.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      item.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
      item.displayCount = GameUtils.numberConversion(math.floor(item.count))
      item.model = v
      data.consumeList[#data.consumeList + 1] = item
    end
    GameUIBattleResult:SetStarWarResult(data)
    local function callback()
      GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
    end
    GameUIMiningWars.onAddStateCallBack = callback
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMiningWorld)
    GameUIMiningWars:Show()
  end, nil)
  GameStateManager:GetCurrentGameState():EraseObject(GameUIMiningWars)
  GameStateManager:SetCurrentGameState(GameStateBattlePlay)
end
function GameUIMiningWars:OnOccupy(id, playerid, logicid, useCredit)
  local curflag = GameUtils:GetFlagByCountryCode()
  local param = {}
  param.id = id
  param.player_id = playerid
  param.logic_id = logicid
  param.use_credit = useCredit or false
  param.flag = curflag
  param.pos = GameUIMiningWars:GetPosByStarId(id)
  local k = GameUIMiningWars:GetIndexByStarPos(param.pos)
  param.is_intrusion = GameUIMiningWars.basicData.starList[k].isOccupied
  GameUIMiningWars.curStarID = id
  GameUIMiningWars.curPlayerID = playerid
  GameUIMiningWars.curLogicID = logicid
  if GameUIMiningWars.isGroup == 1 then
    param.is_team_fight = true
  else
    param.is_team_fight = false
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_battle_req.Code, param, GameUIMiningWars.reqBattleCallBack, true, nil)
end
function GameUIMiningWars.reqBattleCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_battle_req.Code then
    if content.code ~= 0 then
      if content.code == 49210 then
        local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
        GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
        GameUIMessageDialog:SetYesButton(function()
          local param = {
            block = GameUIMiningWars.curMapId
          }
          NetMessageMgr:SendMsg(NetAPIList.mining_wars_buy_count_req.Code, param, GameUIMiningWars.buyCountCallBack, true, nil)
        end)
        local contentText = GameLoader:GetGameText("LC_ALERT_sw_no_remaining_plunder_times") .. "\n" .. GameLoader:GetGameText("LC_MENU_PLANETCRAFT_COST_TIP_RUSH")
        contentText = string.gsub(contentText, "<credits_num>", tostring(GameUIMiningWars.basicData.buyCredit))
        local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
        GameUIMessageDialog:Display(titleText, contentText)
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
        if content.code == 49209 then
          GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "ClosePop")
          GameUIMiningWars:RefreshBasicData()
        end
      end
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_battle_ack.Code then
    if 0 < content.error_code then
      local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
      GameUIMessageDialog:SetYesButton(function()
        GameUIMiningWars:OnOccupy(GameUIMiningWars.curStarID, GameUIMiningWars.curPlayerID, GameUIMiningWars.curLogicID, true)
        GameUIMiningWars.curStarID = nil
        GameUIMiningWars.curPlayerID = nil
        GameUIMiningWars.curLogicID = nil
      end)
      local contentText = GameLoader:GetGameText("LC_MENU_LACK_OF_RESOURCES")
      local name = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_BUTTON_ROB")
      contentText = string.gsub(contentText, "<name1>", name)
      contentText = string.gsub(contentText, "<name2>", tostring(content.switch_credit))
      local titleText = GameLoader:GetGameText("LC_MENU_LOCAL_TEXT_SETTING_PAYREMIND")
      GameUIMessageDialog:Display(titleText, contentText)
      return true
    end
    GameUIMiningWars.basicData.leftCount = GameUIMiningWars.basicData.leftCount - 1
    GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "setLeftCount", GameUIMiningWars.basicData.leftCount, GameUIMiningWars.basicData.maxCount)
    GameUIMiningWars:onBattleResult(content)
    return true
  end
  return false
end
function GameUIMiningWars:onBattleResult(content)
  if content.success then
    local item = {}
    item.id = content.result.star_info.id
    item.pos = GameUIMiningWars:GetPosByStarId(item.id)
    item.leftTime = content.result.star_info.occupie_left_time
    item.flag = content.result.star_info.flag
    item.isMy = content.result.star_info.is_my
    item.isOccupied = content.result.star_info.is_occupied
    item.baseTime = os.time()
    item.percent = content.result.star_info.percent
    item.starLevel = content.result.star_info.star_level
    item.starFrame = tonumber(content.result.star_info.star_frame)
    item.starName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_TYPE_" .. content.result.star_info.name_key)
    item.produceText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_OUTPUT")
    item.consumeText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ROB_COST")
    item.occupieText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_OWNER")
    item.battleItems = {}
    for k, v in ipairs(content.result.star_info.battle_limit or {}) do
      local tmp = {}
      tmp.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
      tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
      tmp.maxDisplayCount = GameUtils.numberConversion(math.floor(v.max))
      tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
      tmp.model = v
      item.battleItems[#item.battleItems + 1] = tmp
    end
    if GameUIMiningWars:GetIndexByStarPos(item.pos) then
      local k = GameUIMiningWars:GetIndexByStarPos(item.pos)
      GameUIMiningWars.basicData.starList[k] = item
    end
    local flashObj = GameUIMiningWars:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "UpdateStarInfoByPos", item.pos, item)
    end
    GameUIMiningWars:GenerateStarData(content.result)
  else
    GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "ClosePop")
    GameUIMiningWars:StartBattle(content.report, content.success, content.battle_result)
  end
end
function GameUIMiningWars:OnCollect(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.mining_wars_collect_req.Code, param, GameUIMiningWars.reqCollectCallBack, true, nil)
end
function GameUIMiningWars.reqCollectCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.mining_wars_collect_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.mining_wars_collect_ack.Code then
    GameUIMiningWars:onCollectResult(content)
    return true
  end
  return false
end
function GameUIMiningWars:onCollectResult(content)
  local item = {}
  item.id = content.result.star_info.id
  item.pos = GameUIMiningWars:GetPosByStarId(item.id)
  item.leftTime = content.result.star_info.occupie_left_time
  item.flag = content.result.star_info.flag
  item.isMy = content.result.star_info.is_my
  item.isOccupied = content.result.star_info.is_occupied
  item.baseTime = os.time()
  item.percent = content.result.star_info.percent
  item.starLevel = content.result.star_info.star_level
  item.starFrame = tonumber(content.result.star_info.star_frame)
  item.starName = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_TYPE_" .. content.result.star_info.name_key)
  item.produceText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_PLANET_OUTPUT")
  item.consumeText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_ROB_COST")
  item.occupieText = GameLoader:GetGameText("LC_MENU_PLANETCRAFT_OWNER")
  item.battleItems = {}
  for k, v in ipairs(content.result.star_info.battle_limit or {}) do
    local tmp = {}
    tmp.count = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    tmp.displayCount = GameUtils.numberConversion(math.floor(tmp.count))
    tmp.maxDisplayCount = GameUtils.numberConversion(math.floor(v.max))
    tmp.frame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v)
    tmp.model = v
    item.battleItems[#item.battleItems + 1] = tmp
  end
  if GameUIMiningWars:GetIndexByStarPos(item.pos) then
    local k = GameUIMiningWars:GetIndexByStarPos(item.pos)
    GameUIMiningWars.basicData.starList[k] = item
  end
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateStarInfoByPos", item.pos, item)
    flashObj:InvokeASCallback("_root", "ClosePop")
  end
end
function GameUIMiningWars:GetIndexByStarPos(pos)
  if GameUIMiningWars.basicData then
    for k, v in pairs(GameUIMiningWars.basicData.starList or {}) do
      if v.pos == pos then
        return k
      end
    end
  end
  return nil
end
function GameUIMiningWars:GetStarInfoByStarId(id)
  if GameUIMiningWars.basicData then
    for k, v in pairs(GameUIMiningWars.basicData.starList or {}) do
      if v.id == id then
        return v
      end
    end
  end
  return nil
end
function GameUIMiningWars:GetPosByStarId(id)
  if GameUIMiningWars.basicData then
    for k, v in pairs(GameUIMiningWars.basicData.starList or {}) do
      if v.id == id then
        return v.pos
      end
    end
  end
  return nil
end
GameUIMiningWars.clickFleetStatu = _fleetStatu.none
GameUIMiningWars.create_army_credit = 0
function GameUIMiningWars:setFleetStatu(statu)
  GameUIMiningWars.clickFleetStatu = statu
  local txt = ""
  if statu == _fleetStatu.none then
    txt = GameLoader:GetGameText("LC_MENU_FST_title_fleet")
  else
    txt = GameLoader:GetGameText("LC_MENU_FST_my_fleet")
  end
  local param = {
    {
      path = "_root.star_all.star_all1.btnFleet.Text_Fleet",
      txt = txt
    }
  }
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetText", param)
  end
end
local _getAvator = function(avator)
  if avator ~= 0 and avator ~= 1 then
    return ...
  else
    return ...
  end
end
GameUIMiningWars.groupdata = {
  forces_list = {},
  my_forces = {},
  self_user_id,
  self_server_id
}
function _onClickFleet_none()
  if GameUIMiningWars.clickFleetStatu == _fleetStatu.none then
    local alliance_info = GameGlobalData:GetData("alliance")
    if alliance_info == nil or alliance_info.id == "" then
      GameUIGlobalScreen:ShowAlert("error", 42007, nil)
      return
    end
  end
  local function _req_armylist()
    local function _callback(msgtype, content)
      if msgtype ~= NetAPIList.forces_ack.Code then
        return false
      end
      local params = {
        tlist = {},
        credit = GameUIMiningWars.create_army_credit
      }
      GameUIMiningWars.groupdata.forces_list = {}
      for k, v in pairs(content.forces_list) do
        if v.status ~= 2 then
          local tlist = {}
          tlist.reqATK = v.enter_power
          tlist.needcnt = v.force_count
          tlist.left_time = v.left_time
          tlist.id = v.id
          tlist.name = v.name
          tlist.tmember = {}
          for kk, vv in pairs(v.forces) do
            local tmember = {}
            tmember.strname = vv.name
            tmember.strhead = _getAvator(tonumber(vv.avator))
            tmember.ATK = vv.power
            table.insert(tlist.tmember, tmember)
          end
          table.insert(params.tlist, tlist)
          table.insert(GameUIMiningWars.groupdata.forces_list, v)
        end
      end
      local currentForce = GameHelper:GetFleetsForce()
      table.sort(params.tlist, function(a, b)
        if a.reqATK > currentForce and b.reqATK > currentForce then
          return a.left_time < b.left_time
        elseif a.reqATK < currentForce and b.reqATK < currentForce then
          return a.left_time < b.left_time
        elseif a.reqATK > currentForce and b.reqATK < currentForce then
          return false
        elseif a.reqATK < currentForce and b.reqATK > currentForce then
          return true
        end
      end)
      local most_show = 5
      if most_show < #params.tlist then
        for i = #params.tlist, most_show + 1, -1 do
          table.remove(params.tlist, i)
        end
      end
      local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
      local flashObj = GameUIMiningWars:GetFlashObject()
      flashObj:InvokeASCallback("_root", "show_army_list", params)
      return true
    end
    NetMessageMgr:SendMsg(NetAPIList.forces_req.Code, nil, _callback, true, nil)
  end
  local function _getCost(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.price_req.Code then
      assert(content.code ~= 0)
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    if msgType == NetAPIList.price_ack.Code then
      if 0 <= content.price then
        GameUIMiningWars.create_army_credit = content.price
        _req_armylist()
      end
      return true
    end
    return false
  end
  local requestParam = {
    price_type = "sw_create_team",
    type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.price_req.Code, requestParam, _getCost, true, nil)
end
function _onClickFleet_prepare(strtype)
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.my_forces_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    end
    if msgtype ~= NetAPIList.my_forces_ack.Code then
      return false
    end
    local param = {}
    param.count = content.team.force_count
    param.ntime = content.team.left_time
    param.tlist = {}
    for k, v in pairs(content.team.forces) do
      local t = {}
      t.strhead = _getAvator(tonumber(v.avator))
      t.ATK = v.power
      t.strname = v.name
      table.insert(param.tlist, t)
    end
    local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
    local flashObj = GameUIMiningWars:GetFlashObject()
    flashObj:InvokeASCallback("_root", "show_hire", param)
    return true
  end
  NetMessageMgr:SendMsg(NetAPIList.my_forces_req.Code, nil, _callback, true, nil)
end
function _onClickFleet_lm(strtype)
  local function _callback(msgtype, content)
    if msgtype ~= NetAPIList.my_forces_ack.Code then
      return false
    end
    local param = {}
    param.tlist = {}
    if strtype == "leader" then
      param.isleader = 1
      param.count = #content.team.forces
    else
      param.isleader = 0
      if content.team.status == 2 then
        param.count = #content.team.forces
      else
        param.count = content.team.force_count
      end
    end
    param.ntime = content.team.left_time
    for k, v in pairs(content.team.forces) do
      local t = {}
      t.strname = v.name
      t.ATK = v.power
      t.strhead = _getAvator(tonumber(v.avator))
      t.server_id = v.server_id
      t.user_id = v.user_id
      t.isleader = v.position
      table.insert(param.tlist, t)
    end
    local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
    local flashObj = GameUIMiningWars:GetFlashObject()
    flashObj:InvokeASCallback("_root", "show_order", param)
    return true
  end
  NetMessageMgr:SendMsg(NetAPIList.my_forces_req.Code, nil, _callback, true, nil)
end
function GameUIMiningWars:OnClickFleet()
  if GameUIMiningWars.clickFleetStatu == _fleetStatu.none then
    _onClickFleet_none()
  elseif GameUIMiningWars.clickFleetStatu == _fleetStatu.prepare then
    _onClickFleet_prepare()
  elseif GameUIMiningWars.clickFleetStatu == _fleetStatu.leader then
    _onClickFleet_lm("leader")
  elseif GameUIMiningWars.clickFleetStatu == _fleetStatu.member then
    _onClickFleet_lm("member")
  else
    error("bad statu " .. tostring(GameUIMiningWars.clickFleetStatu))
  end
end
function GameUIMiningWars:onChangeOrder(orders)
  if #orders % 2 ~= 0 then
    error("bad param orders " .. #orders)
  end
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.change_atk_seq_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
      end
      return true
    end
    return false
  end
  local param = {}
  param.seq = {}
  for i = 1, #orders / 2 do
    local t = {
      key = tonumber(orders[2 * i - 1]),
      value = tonumber(orders[2 * i])
    }
    table.insert(param.seq, t)
  end
  NetMessageMgr:SendMsg(NetAPIList.change_atk_seq_req.Code, param, _callback, true, nil)
end
function GameUIMiningWars:onClick_army_list(id)
  assert(GameUIMiningWars.groupdata.forces_list ~= nil, "error no forceslist")
  local thelist
  for k, v in pairs(GameUIMiningWars.groupdata.forces_list) do
    if v.id == id then
      thelist = v
      break
    end
  end
  if thelist == nil then
    error("notfinded list " .. id .. " " .. LuaUtils:serializeTable(GameUIMiningWars.groupdata.forces_list))
  end
  local function _callback()
    local param = {id = id}
    local function _onMsg(msgtype, content)
      if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.force_join_req.Code then
        if content.code ~= 0 then
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        else
          local flashObj = GameUIMiningWars:GetFlashObject()
          flashObj:InvokeASCallback("_root", "hide_army_list")
          GameUIMiningWars:setFleetStatu(_fleetStatu.member)
          _onClickFleet_lm("member")
        end
        return true
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.force_join_req.Code, param, _onMsg, true, nil)
  end
  local info = GameLoader:GetGameText("LC_MENU_FST_confirm_join")
  local title = ""
  local uimessage = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  uimessage:SetStyle(uimessage.DialogStyle.YesNo)
  uimessage:SetStyle(uimessage.DialogStyle.YesNo)
  uimessage:SetYesButton(_callback)
  uimessage:Display(title, info)
end
function GameUIMiningWars:OnClickInfo(power, count)
  local function _doCreate()
    local function _callback(msgtype, content)
      if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.concentrate_req.Code then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        return true
      end
      if msgtype ~= NetAPIList.concentrate_ack.Code then
        return false
      end
      local teamid = content.team_id
      local teaminfo
      for k, v in pairs(GameUIMiningWars.groupdata.forces_list) do
        if v.id == teamid then
          teaminfo = v
          break
        end
      end
      assert(teaminfo ~= nil, "not find teamid " .. teamid .. " " .. LuaUtils:serializeTable(GameUIMiningWars.groupdata))
      local GameUIMiningWars = LuaObjectManager:GetLuaObject("GameUIMiningWars")
      local flashObj = GameUIMiningWars:GetFlashObject()
      local param = {}
      param.count = teaminfo.force_count
      param.ntime = teaminfo.left_time
      param.tlist = {}
      local t = {}
      local forceinfo = teaminfo.forces[1]
      assert(forceinfo ~= nil, "not find forceinfo " .. LuaUtils:serializeTable(GameUIMiningWars.groupdata))
      t.strhead = _getAvator(tonumber(forceinfo.avator))
      t.ATK = forceinfo.power
      t.strname = forceinfo.name
      table.insert(param.tlist, t)
      flashObj:InvokeASCallback("_root", "hide_army_list")
      flashObj:InvokeASCallback("_root", "hide_hire_info")
      flashObj:InvokeASCallback("_root", "show_hire", param)
      flashObj:InvokeASCallback("_root", "show_hire", param)
      GameUIMiningWars:setFleetStatu(_fleetStatu.prepare)
      return true
    end
    local param = {}
    param.power = power
    param.count = count
    NetMessageMgr:SendMsg(NetAPIList.concentrate_req.Code, param, _callback, true, nil)
  end
  local enablePayRemind = GameSettingData.EnablePayRemind
  if enablePayRemind == nil then
    enablePayRemind = true
  end
  if enablePayRemind then
    local msgBoxContent = string.format(GameLoader:GetGameText("LC_MENU_FST_Refresh_with_credits"), GameUIMiningWars.create_army_credit)
    local TitleInfo = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, TitleInfo, msgBoxContent, _doCreate, nil)
  else
    _doCreate()
  end
end
function GameUIMiningWars:OnClickPrepare()
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.concentrate_end_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        GameUIMiningWars:setFleetStatu(_fleetStatu.leader)
        local flashObj = GameUIMiningWars:GetFlashObject()
        flashObj:InvokeASCallback("_root", "hide_hire")
        _onClickFleet_lm("leader")
      end
      return true
    end
    return false
  end
  local param = {}
  param.id = -1
  NetMessageMgr:SendMsg(NetAPIList.concentrate_end_req.Code, param, _callback, true, nil)
end
function GameUIMiningWars.GroupFightNtf(content)
end
function GameUIMiningWars.ForcesNtf(content)
  GameUIMiningWars.groupdata.forces_list = content.forces_list
  GameUIMiningWars.groupdata.self_user_id = content.self_user_id
  GameUIMiningWars.groupdata.self_server_id = content.self_server_id
  GameUIMiningWars.clickFleetStatu = _fleetStatu.none
  for k, v in pairs(content.forces_list) do
    for kk, vv in pairs(v.forces) do
      if content.self_user_id == vv.user_id then
        if vv.position == 0 then
          GameUIMiningWars.clickFleetStatu = _fleetStatu.member
          break
        end
        if vv.position == 1 then
          if v.status == 1 then
            GameUIMiningWars.clickFleetStatu = _fleetStatu.prepare
            break
          end
          GameUIMiningWars.clickFleetStatu = _fleetStatu.leader
        end
        break
      end
    end
    if GameUIMiningWars.clickFleetStatu ~= _fleetStatu.none then
      break
    end
  end
  GameUIMiningWars:setFleetStatu(GameUIMiningWars.clickFleetStatu)
end
function GameUIMiningWars:OnClickShowReport()
  if GameUIMiningWars.clickFleetStatu == _fleetStatu.none then
    local alliance_info = GameGlobalData:GetData("alliance")
    if alliance_info == nil or alliance_info.id == "" then
      GameUIGlobalScreen:ShowAlert("error", 42007, nil)
      return
    end
  end
  local function _callback(msgtype, content)
    if msgtype ~= NetAPIList.mining_team_log_ack.Code then
      return false
    end
    local param = {}
    param.tlist = {}
    for i, v in pairs(content.logs) do
      local t = {}
      if v.type == 1 then
        t.ntype = 0
      else
        t.ntype = 1
      end
      t.strcontent = v.desc
      t.id = v.report_id
      t.nround = v.round
      t.ntime = v.time
      table.insert(param.tlist, t)
    end
    local flashObj = GameUIMiningWars:GetFlashObject()
    flashObj:InvokeASCallback("_root", "show_report", param)
    return true
  end
  NetMessageMgr:SendMsg(NetAPIList.mining_team_log_req.Code, nil, _callback, true, nil)
end
function GameUIMiningWars:OnClickReportItem(nid, nround)
  local function _callback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.team_fight_report_req.Code then
      if content.code ~= 0 then
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      else
        local battleplay = GameStateManager.GameStateBattlePlay
        battleplay.curBattleType = "starwar"
        if GameObjectBattleReplay.GroupBattleReportArr[1] and 0 < #GameObjectBattleReplay.GroupBattleReportArr[1].headers then
          battleplay:InitBattle(battleplay.MODE_HISTORY, GameObjectBattleReplay.GroupBattleReportArr[1].headers[1].report)
          GameStateManager:SetCurrentGameState(battleplay)
        end
      end
      return true
    end
    return false
  end
  local param = {}
  param.report_id = nid
  param.round = nround
  GameStateBattlePlay.report_id = nid
  NetMessageMgr:SendMsg(NetAPIList.team_fight_report_req.Code, param, _callback, true, nil)
end
GameUIMiningWars.last_hide_frame = 0
function GameUIMiningWars.onKeyboardHide()
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj ~= nil and GameStateManager.m_frameCounter - GameUIMiningWars.last_hide_frame > 30 then
    flashObj:InvokeASCallback("_root", "onKeyboardHide")
  end
  GameUIMiningWars.last_hide_frame = GameStateManager.m_frameCounter
end
function GameUIMiningWars:Update(dt)
  local flashObj = GameUIMiningWars:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIMiningWars.OnAndroidBack()
    if not GameUIMiningWars:GetFlashObject() then
      return
    end
    if GameUIMiningWars.curUI == "main" then
      GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "MoveOut")
    elseif GameUIMiningWars.curUI == "pop" then
      GameUIMiningWars:GetFlashObject():InvokeASCallback("_root", "ClosePop")
    end
  end
end
