local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
GameFleetEquipment.zhenxin_teamleague = {}
local zhenxin_teamleague = GameFleetEquipment.zhenxin_teamleague
local zhenxin = GameFleetEquipment.zhenxin
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameFleetEquipmentPop = LuaObjectManager:GetLuaObject("GameFleetEquipmentPop")
local GameStateFormation = GameStateManager.GameStateFormation
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameStateDaily = GameStateManager.GameStateDaily
local GameStateTlcFormation = GameStateManager.GameStateTlcFormation
local GameStateTeamLeagueAtkFormation = GameStateManager.GameStateTeamLeagueAtkFormation
require("FleetTeamLeagueMatrix.tfl")
zhenxin_teamleague.CurSelectFleet = 1
local FormationFleets = {}
zhenxin_teamleague.battle_matrix = {}
zhenxin_teamleague.AdjutantButtoState = {
  unlocked = 0,
  locked = 1,
  can = 2,
  have = 3
}
local needChangeCurSelectShip = true
function zhenxin_teamleague:OnAddToGameState()
  DebugOut("zhenxin_teamleague ~~~~~ OnAddToGameState ")
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", zhenxin_teamleague.OnEnhanceFleetinfoChange)
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhenxin.OnEnhanceFleetinfoChange)
  GameGlobalData:RemoveDataChangeCallback("matrix", zhenxin.matrixUpdate)
  GameGlobalData:RemoveDataChangeCallback("leaderlist", zhenxin.OnEnhanceFleetinfoChange)
end
function zhenxin_teamleague:OnEraseFromGameState()
  DebugOut("zhenxin_teamleague ~~~~~ OnEraseFromGameState ")
  zhenxin_teamleague.CurSelectFleet = 1
  zhenxin_teamleague.newSelectedFleetID = nil
  GameGlobalData:RemoveDataChangeCallback("fleetinfo", zhenxin_teamleague.OnEnhanceFleetinfoChange)
end
function zhenxin_teamleague:GetFlashObject()
  return (...), GameFleetEquipment
end
function zhenxin_teamleague:OnFSCommand(cmd, arg)
  if cmd == "switchFormation" then
    BackToFormation()
    return true
  elseif cmd == "onClickClose" then
    BackToFormation()
    return true
  elseif cmd == "FormationFleetClicked" then
    local param = LuaUtils:string_split(arg, "\001")
    local identity = tonumber(param[1])
    local idx = tonumber(param[2])
    if identity == 0 then
    elseif identity > 0 then
      self.CurSelectFleet = idx
      self:OnFleetSelected()
    else
      local tip = GameLoader:GetGameText("LC_ALERT_fleet_count_over_max_new")
      local function okCallback()
        GameHelper:SetEnterPage(GameHelper.PagePrestige)
        GameStateManager:SetCurrentGameState(GameStateDaily)
      end
      local cancelCallback = function()
      end
      DebugOut("GameUIGlobalScreen3")
      GameUIGlobalScreen:ShowMessageBox(2, "", tip, okCallback, cancelCallback)
    end
    return true
  elseif cmd == "AdjutantClicked" then
    local fleetId, index = self:GetCurFleetContent().identity, nil
    GameUIAdjutant.mFleetIdBeforeEnter = fleetId
    GameUIAdjutant.IsTeamLeague = true
    GameUIAdjutant:EnterAdjutantUI(nil, GameFleetEquipment.teamLeagueType)
    GameUIAdjutant.IsShowAdjutantUI = true
    return true
  elseif cmd == "showFleetPopView" then
    local fleet, index = self:GetCurFleetContent()
    local leaderlist = GameGlobalData:GetData("leaderlist")
    if leaderlist and fleet.identity == 1 then
      ItemBox:ShowCommanderDetail2(GameFleetEquipment:GetLeaderFleet(), fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
    return true
  elseif cmd == "GotoMasterPage" then
    local fleet, index = self:GetCurFleetContent()
    if fleet and fleet.identity then
      GameUIMaster.CurrentScope = {
        [1] = fleet.id
      }
      GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.EquipMaster
      GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.equip_enchance
      GameUIMaster.CurMatrixId = GameFleetEquipment:GetCurMatrixId()
      GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    end
    return true
  else
    if cmd == "changeShip" then
      zhenxin_teamleague:OnChangeShip()
    else
    end
  end
  return false
end
function sortzhenxin_teamleagueFleet(a, b)
  if a.commander_vesselstype == "TYPE_6" and b.commander_vesselstype ~= "TYPE_6" then
    return true
  elseif a.commander_vesselstype ~= "TYPE_6" and b.commander_vesselstype == "TYPE_6" then
    return false
  else
    return a.force > b.force
  end
end
function zhenxin_teamleague:LoadTeam(teamIndex)
  for _, v in ipairs(GameFleetEquipment:TeamLeagueMatrix():GetMatrixByIndex(teamIndex).cells) do
    zhenxin_teamleague.battle_matrix[v.cell_id] = v.fleet_identity
  end
  FormationFleets = {}
  for k, v in pairs(zhenxin_teamleague.battle_matrix) do
    local fleetDetail = {}
    if v > 0 then
      local commander_data = GameGlobalData:GetFleetInfo(v)
      local tempid = v
      local commander_level = 0
      if v == 1 then
        tempid = GameFleetEquipment:GetLeaderFleet() or 1
      end
      if commander_data == nil then
        if tempid == 1 then
          commander_data = {}
          commander_data.identity = 1
        end
        DebugOut("error:id_commander")
        return
      else
        commander_level = commander_data.level
      end
      local ability = GameDataAccessHelper:GetCommanderAbility(tempid, commander_level)
      fleetDetail.commander_identity = commander_data.identity
      fleetDetail.commanderType = GameGlobalData:GetCommanderTypeWithIdentity(tempid)
      fleetDetail.commander_avatar = GameDataAccessHelper:GetFleetAvatar(tempid, commander_level)
      fleetDetail.commander_vessels = GameDataAccessHelper:GetCommanderVesselsImage(tempid, commander_level, false)
      fleetDetail.commander_vesselstype = GameDataAccessHelper:GetCommanderVesselsType(tempid, commander_level)
      fleetDetail.commander_color = GameDataAccessHelper:GetCommanderColorFrame(tempid, commander_level)
      fleetDetail.commander_sex = GameDataAccessHelper:GetCommanderSex(tempid)
      fleetDetail.commander_rank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin_teamleague.DownloadRankImageCallback, #FormationFleets + 1)
      fleetDetail.force = commander_data.force
      table.insert(FormationFleets, fleetDetail)
    end
  end
  table.sort(FormationFleets, sortzhenxin_teamleagueFleet)
  for i = 1, 5 do
    if not FormationFleets[i] then
      if i <= GameFleetEquipment:TeamLeagueMatrix():GetMatrixByIndex(teamIndex).count or i == 2 then
        local fleetDetail = {}
        fleetDetail.commander_identity = 0
        fleetDetail.force = 0
        FormationFleets[i] = fleetDetail
      else
        local fleetDetail = {}
        fleetDetail.commander_identity = -1
        fleetDetail.force = -1
        FormationFleets[i] = fleetDetail
      end
    end
  end
  if zhenxin_teamleague.newSelectedFleetID then
    for k, v in ipairs(FormationFleets) do
      if v.commander_identity == zhenxin_teamleague.newSelectedFleetID then
        zhenxin_teamleague.CurSelectFleet = k
        zhenxin_teamleague.newSelectedFleetID = nil
        break
      end
    end
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    DebugOutPutTable(FormationFleets, "FormationFleetsxx:" .. self.CurSelectFleet)
    flashObj:InvokeASCallback("_root", "SetFormationFleetInfo", FormationFleets, self.CurSelectFleet)
    flashObj:InvokeASCallback("_root", "chooseTeam", teamIndex)
  end
end
function zhenxin_teamleague:InitMenu()
  local uiText = {}
  if GameFleetEquipment.teamLeagueType == 1 then
    uiText.team1Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM1_ATTACK_FLEETS_CHAR")
    uiText.team2Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM2_ATTACK_FLEETS_CHAR")
    uiText.team3Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM3_ATTACK_FLEETS_CHAR")
  elseif GameFleetEquipment.teamLeagueType == 2 then
    uiText.team1Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM1_DEFENSIVE_FLEETS_CHAR")
    uiText.team2Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM2_DEFENSIVE_FLEETS_CHAR")
    uiText.team3Text = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM3_DEFENSIVE_FLEETS_CHAR")
  end
  uiText.fleetBtnText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_TEAM_FORMATION_BTN")
  self:GetFlashObject():InvokeASCallback("_root", "initAsTeamLeague", uiText)
  if GameFleetEquipment.curTeamIndex == nil then
    GameFleetEquipment.curTeamIndex = 1
  end
  self:LoadTeam(GameFleetEquipment.curTeamIndex)
  self:OnFleetSelected()
end
function zhenxin_teamleague:GetCurFleetContent()
  if FormationFleets == nil or FormationFleets[self.CurSelectFleet] == nil then
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local content, idx
  for k, v in ipairs(fleets) do
    if v.identity == FormationFleets[self.CurSelectFleet].commander_identity then
      content = v
      idx = k
      break
    end
  end
  DebugOutPutTable(content, "zhenxin_teamleague:GetCurFleetContent ==== ")
  return content
end
function zhenxin_teamleague:SetFleetDetailMenu()
  local content, index = self:GetCurFleetContent()
  local leaderlist = GameGlobalData:GetData("leaderlist")
  DebugOut("RefreshFleetAvatar:" .. content.identity)
  if not content then
    return
  end
  TutorialAdjutantManager:StartTutorialAdjutantBind_NewEnhance(content.identity)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(content.identity, content.level)
  local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(content.identity, content.level)
  local level = GameLoader:GetGameText("LC_MENU_Level") .. GameGlobalData:GetData("levelinfo").level
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(content.identity, content.level)
  local force = GameUtils.numberConversion(content.force)
  local fleetID = content.identity
  local fleetIcon = GameDataAccessHelper:GetShip(content.identity, nil, content.level)
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleetID, content.level)
  local forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetID, content.level)
  local skill = basic_info.SPELL_ID
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local ability = GameDataAccessHelper:GetCommanderAbility(fleetID, content.level)
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin_teamleague.UpdateRankImageDownLoadCallback)
  local sex = GameDataAccessHelper:GetCommanderSex(fleetID)
  if content.identity == 1 then
    local tempid = GameFleetEquipment:GetLeaderFleet()
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, content.level)
    fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(tempid, content.level)
    vessleType = GameDataAccessHelper:GetCommanderVesselsType(tempid, content.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, content.level)
    basic_info = GameDataAccessHelper:GetCommanderBasicInfo(tempid, content.level)
    skill = basic_info.SPELL_ID
    skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
    ability = GameDataAccessHelper:GetCommanderAbility(tempid, content.level)
    fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin_teamleague.UpdateRankImageDownLoadCallback)
    sex = GameDataAccessHelper:GetCommanderSex(tempid)
    fleetIcon = GameDataAccessHelper:GetShip(GameFleetEquipment:GetLeaderFleet(), nil, content.level)
  end
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  DebugOut("SetFleetAvatar:")
  DebugOut(fleetIcon, fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank)
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetAvatar", fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank, sex, fleetIcon)
  self:SetAdjutant(content)
  if GameFleetEquipment.currentSelect == "zhuangbei" or GameFleetEquipment.currentSelect == "xunzhang" or GameFleetEquipment.currentSelect == "kejin" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowShipDetailInfoMenu")
  end
  if GameFleetEquipment.currentSelect == "zhuangbei" or GameFleetEquipment.currentSelect == "xunzhang" then
    self:GetFlashObject():InvokeASCallback("_root", "ShowDetailFleet")
  end
end
function zhenxin_teamleague:SetAdjutant(content)
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(content)
  if curAdjutantButton == zhenxin_teamleague.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(content.cur_adjutant)
    DebugOutPutTable(adjutant, "SetAdjutantfsf")
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin_teamleague.DownloadAdjuantRankImageCallback)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
  if self:FleetInMatrix(content.identity) then
    DebugOut("enheng?", curAdjutantButton)
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == zhenxin_teamleague.AdjutantButtoState.can)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
  end
end
function zhenxin_teamleague:SetAdjutantRedpoint(isShow)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setAdjutantRedpoint", false)
  end
end
function zhenxin_teamleague:GetAdjutant(identity)
  local adjutant
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOutPutTable(zhenxin_teamleague.DismissFleet, "zhenxin_teamleague:GetAdjutant( " .. identity)
  DebugOutPutTable(fleets, "fleets===")
  for _, v in pairs(fleets) do
    if v.identity == identity then
      adjutant = v
      break
    end
  end
  for _, v in pairs(zhenxin_teamleague.DismissFleet) do
    if v.identity == identity then
      adjutant = v
      break
    end
  end
  if adjutant == nil then
    DebugOut("error: adjutant is nil")
  end
  return adjutant
end
function zhenxin_teamleague:GetAdjutantButtonState(content)
  local currentState = ""
  local playerLelevinfo = GameGlobalData:GetData("levelinfo")
  if playerLelevinfo.level < 45 then
    currentState = zhenxin_teamleague.AdjutantButtoState.unlocked
    return currentState
  end
  DebugOut("zhenxin_teamleague:GetAdjutantButtonState  content.cur_adjutant = == " .. content.cur_adjutant)
  DebugOutPutTable(zhenxin_teamleague:GetAdjutant(content.cur_adjutant), "iiiiiii=")
  if content.cur_adjutant > 1 and zhenxin_teamleague:GetAdjutant(content.cur_adjutant) then
    DebugOut("GetAdjutantButtonState === have")
    currentState = zhenxin_teamleague.AdjutantButtoState.have
  else
    if #content.can_adjutant > 0 and zhenxin_teamleague:GetAdjutantStateFr(content.can_adjutant) then
      currentState = zhenxin_teamleague.AdjutantButtoState.can
    else
      currentState = zhenxin_teamleague.AdjutantButtoState.locked
    end
    if zhenxin_teamleague:GetMajorId(content.identity) then
      currentState = zhenxin_teamleague.AdjutantButtoState.locked
    end
  end
  DebugOut("GetAdjutantButtonState = " .. currentState)
  return currentState
end
function zhenxin_teamleague:GetAdjutantStateFr(adjutants)
  local matrixCells = GameGlobalData:GetData("matrix").cells
  DebugOut("GetAdjutantStateFr = ")
  DebugTable(adjutants)
  DebugTable(matrixCells)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if not zhenxin_teamleague.DismissFleet then
    zhenxin_teamleague:GetDismissFleet()
    return nil
  end
  if matrixCells and #matrixCells > 0 then
    for _, v in ipairs(adjutants) do
      local stateMatrix = false
      local stateAdjutantsByother = false
      local stateHaveAdjutant = false
      for _, m in ipairs(matrixCells) do
        if v == m.fleet_identity then
          stateMatrix = true
        end
      end
      for _, ajutant in ipairs(zhenxin_teamleague.DismissFleet) do
        if ajutant.cur_adjutant == v then
          stateAdjutantsByother = true
        end
        if ajutant.identity == v and ajutant.cur_adjutant > 1 then
          stateHaveAdjutant = true
        end
      end
      for _, ajutant in ipairs(fleets) do
        if ajutant.cur_adjutant == v then
          stateAdjutantsByother = true
        end
        if ajutant.identity == v and ajutant.cur_adjutant > 1 then
          stateHaveAdjutant = true
        end
      end
      if not stateMatrix and not stateAdjutantsByother and not stateHaveAdjutant then
        return true
      end
    end
  end
  return false
end
function zhenxin_teamleague:GetMajorId(identity)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  for _, v in ipairs(fleets) do
    if v.cur_adjutant == identity then
      return true
    end
  end
  return false
end
function zhenxin_teamleague:FleetInMatrix(fleetid)
  local matrix = GameGlobalData:GetData("matrix").cells
  DebugOut("matrix::")
  DebugTable(matrix)
  for k, v in pairs(matrix or {}) do
    if v.fleet_identity == fleetid then
      return true
    end
  end
  return false
end
function zhenxin_teamleague:GetDismissFleet()
  zhenxin_teamleague.DismissFleet = {}
  NetMessageMgr:SendMsg(NetAPIList.fleet_dismiss_req.Code, nil, zhenxin_teamleague.AllFleetDataCallback, true, nil)
end
function zhenxin_teamleague.AllFleetDataCallback(msgType, content)
  if NetAPIList.fleet_dismiss_ack.Code == msgType then
    DebugOut("AllFleetDataCallback:")
    zhenxin_teamleague.DismissFleet = content.fleets
    if #FormationFleets > 0 then
      local fleet, index = zhenxin_teamleague:GetCurFleetContent()
      zhenxin_teamleague:UpdateAdjutantState(fleet.identity, fleet.cur_adjutant, fleet)
    end
    return true
  end
  return false
end
function zhenxin_teamleague:UpdateAdjutantState(fleetid, curAdjutant, content)
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(content)
  if curAdjutantButton == zhenxin_teamleague.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(curAdjutant)
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, zhenxin_teamleague.DownloadRankImageCallback)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
    if self:FleetInMatrix(fleetid) then
      DebugOut("enheng?", curAdjutantButton)
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == zhenxin_teamleague.AdjutantButtoState.can and not self:IsMaxAdjutantNow())
    else
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
    end
  end
end
function zhenxin_teamleague:IsMaxAdjutantNow()
  local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
  local nowAdjutantNum = 0
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in ipairs(fleet_info) do
    if GameUtils:IsInMatrix(v.identity) and 0 < v.cur_adjutant then
      nowAdjutantNum = nowAdjutantNum + 1
    end
  end
  DebugOut("IsMaxAdjutantNow", nowAdjutantNum, adjutantUnlockCnt)
  return nowAdjutantNum == adjutantUnlockCnt
end
function zhenxin_teamleague:OnFleetSelected()
  local fleetInfo = GameFleetEquipment.zhenxin_teamleague:GetCurFleetContent()
  GameFleetEquipment.currentforce = fleetInfo.force
  DebugOut("OnFleetSelected --- curforce =" .. GameFleetEquipment.currentforce)
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  local fleetidx
  for k, v in ipairs(fleet_info) do
    if v.identity == fleetInfo.identity then
      fleetidx = k
      break
    end
  end
  assert(fleetidx, "must have " .. fleetInfo.identity)
  self:SetFleetDetailMenu()
  if not GameFleetEquipment.currentSelect or GameFleetEquipment.currentSelect == "zhuangbei" then
    ZhuangBeiUI():Init(fleetInfo, fleetidx, true)
    GameFleetEquipment.currentSelect = "zhuangbei"
  elseif GameFleetEquipment.currentSelect == "kejin" then
    self:GetFlashObject():InvokeASCallback("_root", "enterKrypton")
    KeJinUI().RefreshKrypton(fleetidx)
  elseif GameFleetEquipment.currentSelect == "shenjie" then
    ShenJieUI():Init(fleetInfo, fleetidx)
  elseif GameFleetEquipment.currentSelect == "xunzhang" then
    GameFleetEquipment:OnFSCommand("ShowMedalMenu")
  elseif GameFleetEquipment.currentSelect == "chuancang" then
    GameFleetEquipment:enterArtifactByID(fleetInfo)
  end
  if GameFleetEquipment.currentSelect ~= "zhuangbei" then
    ZhuangBeiUI():Init(fleetInfo, fleetidx, false)
  end
  ShenJieUI():UpdateGrowUpState(fleetInfo.identity)
  if GameFleetEquipment.currentSelect == "kejin" or GameFleetEquipment.currentSelect == "chuancang" then
    GameFleetEquipment:RefreshKryptonRedPoint()
  end
  XunZhangUI():SetMedalButtonNews()
end
function zhenxin_teamleague:OnChangeFleet(newfleetID)
  local fleetDetail, index = self:GetCurFleetContent()
  local fleetId = fleetDetail.identity
  needChangeCurSelectShip = false
  if fleetId == 1 then
    local param = {}
    if GameFleetEquipment.teamLeagueType == 1 then
      param.index = 101
    else
      param.index = 104
    end
    param.id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_tlc_leader_req.Code, param, zhenxin_teamleague.ChangeLeaderCallback, true, nil)
  else
    zhenxin_teamleague.newSelectedFleetID = newfleetID
    zhenxin_teamleague.activeNewSelectedFleet = false
    local param = {}
    param.matrix_index = GameFleetEquipment:GetCurMatrixId()
    param.old_fleet_id = fleetId
    param.new_fleet_id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_fleets_req.Code, param, zhenxin_teamleague.ChangeFleetCallBack, true, nil)
  end
end
function zhenxin_teamleague.ChangeLeaderCallback(msgType, content)
  DebugOut("hangeLeaderCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_tlc_leader_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  end
  return false
end
function zhenxin_teamleague.ChangeFleetCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_fleets_req.Code then
    if content.code ~= 0 then
      zhenxin_teamleague.newSelectedFleetID = nil
      zhenxin_teamleague.activeNewSelectedFleet = false
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function zhenxin_teamleague.matrixUpdate()
  DebugOut("zhenxin_teamleague.matrixUpdate")
  if not zhenxin_teamleague:GetFlashObject() then
    return
  end
  zhenxin_teamleague.newSelectedFleetID = FormationFleets[zhenxin_teamleague.CurSelectFleet].commander_identity
  zhenxin_teamleague:InitMenu()
  zhenxin_teamleague:GetDismissFleet()
  KeJinUI():RequestKryptonInBag()
  ZhuangBeiUI():UpdateSelectedFleetInfo()
end
function zhenxin_teamleague.OnEnhanceFleetinfoChange()
  if zhenxin_teamleague:GetFlashObject() then
    zhenxin_teamleague.newSelectedFleetID = FormationFleets[zhenxin_teamleague.CurSelectFleet].commander_identity
    DebugOut("zhenxin_teamleague.OnEnhanceFleetinfoChange --- " .. zhenxin_teamleague.newSelectedFleetID)
    zhenxin_teamleague:SetFleetDetailMenu()
    zhenxin_teamleague:GetDismissFleet()
  end
end
function zhenxin_teamleague:OnChangeShip()
  local content, index = self:GetCurFleetContent()
  local fleetId = content.identity
  local param = {}
  param.matrix_index = GameFleetEquipment:GetCurMatrixId()
  param.fleet_id = fleetId
  NetMessageMgr:SendMsg(NetAPIList.change_fleets_data_req.Code, param, zhenxin_teamleague.ChangeFleetDataCallBack, true, nil)
end
function zhenxin_teamleague:CheckFormationShow()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local unlock = false
  for k, v in pairs(rightMenu) do
    if v.name == "hire" then
      unlock = v.status
      break
    end
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setFormationShow", unlock)
  end
end
function zhenxin_teamleague.ChangeFleetDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_fleets_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.change_fleets_data_ack.Code then
    GameFleetEquipmentPop:zhenxin_showChangeFleetPop(content)
    return true
  end
  return false
end
function zhenxin_teamleague:UpdateRankImageDownLoadCallback(extInfo)
  if zhenxin_teamleague:GetFlashObject() and extInfo then
    zhenxin_teamleague:GetFlashObject():InvokeASCallback("_root", "updateFleetRankImage", extInfo.rank_id)
  end
end
function zhenxin_teamleague.DownloadRankImageCallback(extInfo)
  if zhenxin_teamleague:GetFlashObject() and extInfo then
    zhenxin_teamleague:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id, extInfo.id)
  end
end
function zhenxin_teamleague.DownloadAdjuantRankImageCallback(extInfo)
  if zhenxin_teamleague:GetFlashObject() and extInfo then
    zhenxin_teamleague:GetFlashObject():InvokeASCallback("_root", "updateAdjutantRankImage", extInfo.rank_id)
  end
end
function zhenxin_teamleague.UpdateMatrix(content)
  FleetTeamLeagueMatrix.UpdateMatrix(content)
  zhenxin_teamleague.matrixUpdate()
end
function BackToFormation()
  if GameStateManager.GameStateEquipEnhance.previousState == GameStateTlcFormation then
    GameStateTlcFormation:EnterState(GameFleetEquipment.teamLeagueType, GameFleetEquipment.curTeamIndex)
  elseif GameStateManager.GameStateEquipEnhance.previousState == GameStateTeamLeagueAtkFormation then
    GameStateTeamLeagueAtkFormation:EnterState(GameFleetEquipment.curTeamIndex)
  end
end
