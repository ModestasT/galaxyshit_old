require("GameTextEdit.tfl")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local GameStateItem = GameStateManager.GameStateItem
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
local GameFleetNewEnhance = GameFleetEquipment.kejin
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local QuestTutorialComboGachaGetHero = TutorialQuestManager.QuestTutorialComboGachaGetHero
ItemBox.currentBox = nil
ItemBox.currentItem = nil
ItemBox.currentItemType = nil
ItemBox.RenameText = ""
ItemBox.bUseMax = false
ItemBox.useMaxCheck = 0
ItemBox.mCurCommander = nil
ItemBox.choosable_items = nil
ItemBox.choosableIndex = 1
ItemBox.mycurItem = nil
ItemBox.isIdComfird = false
ItemBox.saveId = ""
ItemBox.saveName = ""
ItemBox.saveTel = ""
ItemBox.buyCallback = nil
ItemBox.isBuy = false
ItemBox.buyArg = nil
ItemBox.idConfirmCallback = nil
ItemBox.ItemChoices = nil
ItemBox.ItemChoicesCallback = nil
ItemBox.g_isRewardShowing = false
ItemBox.totalPurchaseCount = 0
ItemBox.restRewardTime = 0
ItemBox.RewardStack = {}
ItemBox.afterRewardCallback = nil
ItemBox.afterRewardCallArg = nil
ItemBox.shouldShowRewardBox = false
ItemBox.callback_erase_dialog = nil
ItemBox.shouldAddOrEraseFromState = 0
ItemBox.showItemParam = {hideCost = false}
function ItemBox:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("equipments", ItemBox.refreshEquipments)
  GameGlobalData:RegisterDataChangeCallback("fleetKryptons", ItemBox.refreshFleetKryptons)
end
function ItemBox:OnAddToGameState(game_state)
end
function ItemBox:OnEraseFromGameState(game_state)
  DebugOut("ItemBox:OnEraseFromGameState")
  ItemBox:GetFlashObject():unlockInput()
  self:UnloadFlashObject()
  if self.shouldShowRewardBox then
    self:ShowRewardBox()
  else
    ItemBox.RewardStack = {}
  end
  ItemBox.shouldShowRewardBox = false
  ItemBox.g_isRewardShowing = false
end
function ItemBox:Update(dt)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    flashObj:InvokeASCallback("_root", "UpdateSlider", dt)
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:InvokeASCallback("_root", "UpdateDiceListBox", dt)
    flashObj:InvokeASCallback("_root", "updateBtnStatus", dt)
    if self.g_isRewardShowing then
      if self.restRewardTime > 0 then
        self.restRewardTime = self.restRewardTime - dt
      else
        self.restRewardTime = 0
        self.g_isRewardShowing = false
        if self:GetFlashObject() then
          self:GetFlashObject():InvokeASCallback("_root", "HideRewardBox")
        end
      end
    end
  end
  if ItemBox.shouldAddOrEraseFromState == 1 then
    if not ItemBox.GetAppendState():IsObjectInState(ItemBox) then
      self.GetAppendState():AddObject(self)
    end
  elseif ItemBox.shouldAddOrEraseFromState == 2 then
    self.GetAppendState():EraseObject(self)
  end
  ItemBox.shouldAddOrEraseFromState = 0
end
function ItemBox.AddToState()
  if not ItemBox.GetAppendState():IsObjectInState(ItemBox) then
    ItemBox.GetAppendState():AddObject(ItemBox)
  else
    ItemBox.shouldAddOrEraseFromState = 1
  end
end
function ItemBox.EraseFromeState()
  ItemBox.shouldAddOrEraseFromState = 2
end
function ItemBox.refreshEquipments()
  if ItemBox.GetAppendState():IsObjectInState(ItemBox) and ItemBox.boxType == "Equip" and ItemBox.currentItem then
    ItemBox:SetEquipBox(ItemBox.currentItem, ItemBox.currentItemType)
  end
end
function ItemBox:SetAppendState(state)
  self.m_appendState = state
end
function ItemBox.GetAppendState()
  if ItemBox.m_appendState then
    return ItemBox.m_appendState
  else
    return (...), GameStateManager
  end
end
function ItemBox.refreshFleetKryptons()
  if ItemBox.GetAppendState():IsObjectInState(ItemBox) and ItemBox.boxType == "Krypton" and ItemBox.currentItem then
    ItemBox:SetKryptonBox(ItemBox.currentItem, ItemBox.currentItemType)
  end
end
function ItemBox:ShowPrestigeUpdate(tag, content_1_desc, content_1_parm, daily_award, prestige_level, RankUpVisible, animVisible)
  if immanentversion == 2 then
    if tag == 2 or tag == 1 or tag == 3 then
      self.currentBox = "prestige_pop"
      if not self.GetAppendState():IsObjectInState(self) then
        self.GetAppendState():AddObject(self)
      end
    end
    local paramNumText_1 = GameLoader:GetGameText("LC_MENU_Equip_param_9")
    local paramNumText_2 = GameLoader:GetGameText("LC_MENU_Equip_param_12")
    local paramNumText_3 = GameLoader:GetGameText("LC_MENU_Equip_param_17")
    local paramNumText_4 = GameLoader:GetGameText("LC_MENU_Equip_param_11")
    local ParamInfoNameText = paramNumText_1 .. "\001" .. paramNumText_2 .. "\001" .. paramNumText_3 .. "\001" .. paramNumText_4 .. "\001"
    if tag == 2 then
      self:GetFlashObject():InvokeASCallback("_root", "showPrestigePop", content_1_desc, content_1_parm, daily_award, prestige_level, ParamInfoNameText, RankUpVisible, animVisible)
    elseif tag == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "showPrestigePop_1", content_1_desc, content_1_parm, daily_award, prestige_level, ParamInfoNameText)
    elseif tag == 3 then
      self:GetFlashObject():InvokeASCallback("_root", "hidePrestigePop_1")
    end
  end
end
ItemBox.ShowItemCallParms = nil
function ItemBox:TryQueryItemDetail(itemID, callback)
  DebugOut("query item detail req ", itemID)
  local itemIDList = {}
  table.insert(itemIDList, itemID)
  local requestParam = {ids = itemIDList}
  NetMessageMgr:SendMsg(NetAPIList.items_req.Code, requestParam, ItemBox.QueryItemDetailCallBack, true, nil)
end
function ItemBox.QueryItemDetailCallBack(msgType, content)
  DebugOut("item info call back")
  DebugOut(msgType)
  DebugTable(content)
  if msgType == NetAPIList.items_ack.Code then
    if #content.items >= 1 then
      GameData.Item.Keys[content.items[1].id] = {}
      GameData.Item.Keys[content.items[1].id].ITEM_TYPE = content.items[1].id
      GameData.Item.Keys[content.items[1].id].ITEM_NAME = ""
      GameData.Item.Keys[content.items[1].id].ITEM_DESC = ""
      GameData.Item.Keys[content.items[1].id].PRICE = content.items[1].price
      GameData.Item.Keys[content.items[1].id].REQ_LEVEL = content.items[1].level_req
      GameData.Item.Keys[content.items[1].id].MAX_LEVEL = content.items[1].max_level
      GameData.Item.Keys[content.items[1].id].SP_TYPE = content.items[1].sp_type
      GameData.Item.Keys[content.items[1].id].USE_MAX = content.items[1].use_max
      GameData.Item.Keys[content.items[1].id].LOT = content.items[1].use_more
      DebugOut("QueryItemDetailCallBack")
      DebugOut(content.items[1].id)
      DebugOut(GameData.Item.Keys[content.items[1].id] == nil)
      DebugTable(GameData.Item.Keys[content.items[1].id])
      DebugOut("use_max", ItemBox.bUseMax)
      if ItemBox.ShowItemCallParms.operateLeftFunc ~= nil and ItemBox.ShowItemCallParms.item.cnt ~= nil and 1 < ItemBox.ShowItemCallParms.item.cnt and content.items[1].use_max == 1 then
        ItemBox.bUseMax = true
      end
      if ItemBox.ShowItemCallParms ~= nil then
        if GameDataAccessHelper:isChoosableItem(content.items[1].id) and ItemBox.ShowItemCallParms.operateLeftFunc then
          ItemBox.ShowItemCallParms.boxType = "ChoosableItem"
        elseif not GameDataAccessHelper:IsItemCanUse(content.items[1].id) then
          ItemBox.ShowItemCallParms.operateLeftText = ""
          ItemBox.ShowItemCallParms.operateLeftFunc = nil
        end
        if GameDataAccessHelper:GetItemPrice(content.items[1].id) == 0 then
          ItemBox.ShowItemCallParms.operateRightText = ""
          ItemBox.ShowItemCallParms.operateRightFunc = nil
        end
        ItemBox:realShowItemBox(ItemBox.ShowItemCallParms.boxType, ItemBox.ShowItemCallParms.item, ItemBox.ShowItemCallParms.itemType, ItemBox.ShowItemCallParms._x, ItemBox.ShowItemCallParms._y, ItemBox.ShowItemCallParms._w, ItemBox.ShowItemCallParms._h, ItemBox.ShowItemCallParms.operateLeftText, ItemBox.ShowItemCallParms.operateLeftFunc, ItemBox.ShowItemCallParms.operateRightText, ItemBox.ShowItemCallParms.operateRightFunc)
        ItemBox.ShowItemCallParms = nil
      end
    end
    return true
  end
  return false
end
function ItemBox:realShowItemBox(boxType, item, itemType, _x, _y, _w, _h, operateLeftText, operateLeftFunc, operateRightText, operateRightFunc, bUseMax)
  DebugOut("\230\137\147\229\141\176--->", boxType, item, itemType, _x, _y, _w, _h)
  DebugTable(item)
  self.operateLeftText = operateLeftText or ""
  self.operateRightText = operateRightText or ""
  self.operateLeftFunc = operateLeftFunc
  self.operateRightFunc = operateRightFunc
  self.boxType = boxType
  if boxType == "ChoosableItem" then
    local param = {id = itemType}
    local function callback(msgType, content)
      if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_req.Code then
        DebugOut("common_ack")
        ItemBox:ShowBox("Item", operateLeftText, operateRightText)
        ItemBox.boxType = "Item"
        ItemBox:SetItemBox(item, itemType, nil)
        ItemBox:SetPos(_x, _y, _w, _h, 1)
        return true
      elseif msgType == NetAPIList.choosable_item_ack.Code then
        ItemBox.choosable_items = content.choosable_items
        DebugOut("ItemBox.ChoosableItemChoicesCallback")
        DebugTable(ItemBox.choosable_items)
        GameItemBag:SetCurrentChoosableItems(ItemBox.choosable_items)
        GameItemBag:SetCurrentChoosableItemsIndex(1)
        ItemBox:ShowBox(boxType, operateLeftText, operateRightText)
        ItemBox:SetChoosableItemBox(item, itemType)
        ItemBox:SetPos(_x, _y, _w, _h, 1)
        return true
      end
      return false
    end
    NetMessageMgr:SendMsg(NetAPIList.choosable_item_req.Code, param, callback, true, nil)
    return
  end
  ItemBox:ShowBox(boxType, operateLeftText, operateRightText)
  local boxIndex = 1
  if boxType == "Equip" then
    boxIndex = ItemBox:SetEquipBox(item, itemType)
  elseif boxType == "Item" then
    local showSellOrUse = true
    showSellOrUse = operateLeftText or operateRightText
    ItemBox:SetItemBox(item, itemType, showSellOrUse)
  elseif boxType == "Medal" then
    local showSellOrUse = true
    showSellOrUse = operateLeftText or operateRightText
    ItemBox:SetMedalBox(item, itemType, showSellOrUse)
  else
    assert(false, boxType)
  end
  ItemBox:SetPos(_x, _y, _w, _h, boxIndex)
  if QuestTutorialComboGachaGetHero:IsActive() and TutorialQuestManager.ComboGachaHeroContent and itemType == TutorialQuestManager.ComboGachaHeroContent.id then
    ItemBox:ShowTutorialItemBoxUserButton()
  else
    ItemBox:HideTutorialItemBoxUserButton()
  end
end
function ItemBox:showGotoBtn()
  DebugOut("ItemBox:showGotoBtn")
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowGotoBtn")
end
function ItemBox:showItemBox(boxType, item, itemType, _x, _y, _w, _h, operateLeftText, operateLeftFunc, operateRightText, operateRightFunc)
  DebugOut("showItemBox")
  DebugOut(itemType)
  DebugOut(GameData.Item.Keys[itemType] == nil)
  DebugTable(GameData.Item.Keys[itemType])
  if DynamicResDownloader:IsDynamicStuff(itemType, DynamicResDownloader.resType.PIC) and GameData.Item.Keys[itemType] == nil then
    ItemBox.ShowItemCallParms = {}
    ItemBox.ShowItemCallParms.boxType = boxType
    ItemBox.ShowItemCallParms.item = item
    ItemBox.ShowItemCallParms.itemType = itemType
    ItemBox.ShowItemCallParms._x = _x
    ItemBox.ShowItemCallParms._y = _y
    ItemBox.ShowItemCallParms._w = _w
    ItemBox.ShowItemCallParms._h = _h
    ItemBox.ShowItemCallParms.operateLeftText = operateLeftText
    ItemBox.ShowItemCallParms.operateLeftFunc = operateLeftFunc
    ItemBox.ShowItemCallParms.operateRightText = operateRightText
    ItemBox.ShowItemCallParms.operateRightFunc = operateRightFunc
    self:TryQueryItemDetail(itemType)
  else
    self:realShowItemBox(boxType, item, itemType, _x, _y, _w, _h, operateLeftText, operateLeftFunc, operateRightText, operateRightFunc)
  end
end
function ItemBox:showChoosableItemsView(item, itemType)
end
function ItemBox:ShowTechBox()
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_showTechBox")
  self.currentBox = "Tech"
end
function ItemBox:HideTechBox()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_hideTechBox")
  self._activeTechInfo = nil
end
function ItemBox:ShowTutorialItemBoxUserButton()
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:InvokeASCallback("_root", "ShowTutorialItemBoxUserButton")
end
function ItemBox:HideTutorialItemBoxUserButton()
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:InvokeASCallback("_root", "HideTutorialItemBoxUserButton")
end
function ItemBox:IsShowTechQuick(...)
  local curVipLevel = GameVipDetailInfoPanel:GetVipLevel()
  local curPlayerLevel = GameGlobalData:GetData("levelinfo").level
  local limitVipLevel = GameDataAccessHelper:GetVIPLimit("one_key_technology_update")
  local limitPlayerLevel = GameDataAccessHelper:GetLevelLimit("one_key_technology_update")
  if curVipLevel >= limitVipLevel or curPlayerLevel >= limitPlayerLevel then
    return true
  else
    return false
  end
end
function ItemBox:UpdateTechBoxInfo(dataTech)
  local flash_obj = self:GetFlashObject()
  local effectTypeText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. tostring(dataTech.attr_id))
  flash_obj:SetText("tech_box.contents.name_text", GameDataAccessHelper:GetTechName(dataTech.id))
  flash_obj:SetText("tech_box.contents.level_text", GameLoader:GetGameText("LC_MENU_Level") .. tostring(dataTech.level))
  flash_obj:SetText("tech_box.contents.type_text", effectTypeText)
  flash_obj:SetText("tech_box.contents.value_text", tostring(dataTech.total) .. "+" .. tostring(dataTech.attr_value))
  flash_obj:SetText("tech_box.contents.btn_upgrade_2.num.cost_text", tostring(dataTech.cost))
  flash_obj:SetText("tech_box.contents.btn_upgrade.num.cost_text", tostring(dataTech.cost))
  flash_obj:SetBtnEnable("tech_box.contents.btn_upgrade", dataTech.enabled)
  flash_obj:SetBtnEnable("tech_box.contents.btn_upgrade_2", dataTech.enabled)
  flash_obj:SetBtnEnable("tech_box.contents.btn_upgrade_quick", dataTech.enabled)
  if self:IsShowTechQuick() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowUpdateQuickNew", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "ShowUpdateQuickNew", false)
  end
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local require_text = ""
  if dataTech.level == dataTech.level_max then
    require_text = GameLoader:GetGameText("LC_MENU_MAX_CHAR")
  elseif immanentversion170 == 4 or immanentversion170 == 5 then
    require_text = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), tostring(dataTech.level + 1))
  else
    require_text = GameLoader:GetGameText("LC_MENU_TECH_UPGRADE_LEVEL_REQUIRE") .. dataTech.level_limit
  end
  if (immanentversion170 == 4 or immanentversion170 == 5) and dataTech.enabled == 0 and dataTech.level < dataTech.level_max then
    local building = GameGlobalData:GetBuildingInfo("tech_lab")
    if levelinfo.level < dataTech.player_limit_level then
      require_text = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), tostring(dataTech.player_limit_level))
    elseif building.level < dataTech.level_limit then
      require_text = GameLoader:GetGameText("LC_MENU_TECH_UPGRADE_LEVEL_REQUIRE") .. dataTech.level_limit
    elseif levelinfo.level <= dataTech.level then
      require_text = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), tostring(dataTech.level + 1))
    elseif building.level * 5 <= dataTech.level then
      require_text = GameLoader:GetGameText("LC_MENU_TECH_UPGRADE_LEVEL_REQUIRE") .. math.ceil((dataTech.level + 1) / 5)
    end
  end
  flash_obj:InvokeASCallback("_root", "lua2fs_updateTechInfo", dataTech.id, dataTech.enabled, require_text)
  self._activeTechInfo = dataTech
  if dataTech.id == 1 and QuestTutorialUseTechLab:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTechLab", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTechLab")
  end
end
function ItemBox:UpdateDiceCellItem(index)
  local item = self.diceAwards[index]
  local iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
  local count = GameHelper:GetAwardCount(item.item_type, item.number, item.no)
  local name = GameHelper:GetAwardNameText(item.item_type, item.number)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateDiceItem", index, iconFrame, count, name)
  end
end
function ItemBox:ShowDiceItemDitale(index)
  local item = self.diceAwards[index]
  ItemBox:showItemDetil(item, item.item_type)
end
function ItemBox:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
end
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
function ItemBox:ShowCommanderInfo(commander_data)
  self.currentBox = "commander_info"
  local fleet_type = GameUtils:GetFleetRankFrame(GameDataAccessHelper:GetCommanderAbility(commander_data.identity).Rank, ItemBox.DownloadRankImageCallback)
  local data_table = {}
  data_table[#data_table + 1] = commander_data.name
  data_table[#data_table + 1] = commander_data.avatar
  data_table[#data_table + 1] = commander_data.vessels_type
  data_table[#data_table + 1] = commander_data.vessels_name
  data_table[#data_table + 1] = commander_data.commander_type
  data_table[#data_table + 1] = commander_data.skill_name
  data_table[#data_table + 1] = "TYPE_" .. commander_data.skill_rangetype
  data_table[#data_table + 1] = commander_data.skill_rangetype_name
  data_table[#data_table + 1] = commander_data.skill_desc
  data_table[#data_table + 1] = tostring(commander_data.pos_x)
  data_table[#data_table + 1] = tostring(commander_data.pos_y)
  data_table[#data_table + 1] = fleet_type
  local data_string = table.concat(data_table, "\001")
  DebugOutPutTable(data_table, "data_table")
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  local sex = GameDataAccessHelper:GetCommanderSex(commander_data.identity)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  DebugOut(data_string)
  self:GetFlashObject():InvokeASCallback("_root", "showCommanderInfo", data_string, immanentversion, sex)
end
function ItemBox.DownloadRankImageCallback(extInfo)
  if ItemBox:GetFlashObject() then
    ItemBox:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
function ItemBox:HideCommanderInfo()
  self:GetFlashObject():InvokeASCallback("_root", "hideCommanderInfo")
end
function ItemBox.GetCommanderDetailCallBack(msgType, content)
  DebugOut("detail call back")
  DebugOut(msgType)
  DebugTable(content)
  if msgType == NetAPIList.fleet_info_ack.Code then
    GameWaiting:HideLoadingScreen()
    GameGlobalData:SetFleetDetail(content.fleet_id, content.fleet_info.fleets[1])
    ItemBox:ShowCommanderDetail2(content.fleet_id, content.fleet_info.fleets[1])
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function ItemBox:requestChoosableItemChoices(itemType)
  local param = {}
  param.id = itemType
  DebugOut("ItemBox:requestChoosableItemChoices")
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.choosable_item_req.Code, param, ItemBox.ChoosableItemChoicesCallback, true, nil)
end
function ItemBox.ChoosableItemChoicesCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.choosable_item_req.Code then
    DebugOut("common_ack")
    return true
  elseif msgType == NetAPIList.choosable_item_ack.Code then
    ItemBox.choosable_items = content.choosable_items
    DebugOut("ItemBox.ChoosableItemChoicesCallback")
    DebugTable(ItemBox.choosable_items)
    GameItemBag:SetCurrentChoosableItems(ItemBox.choosable_items)
    return true
  end
  return false
end
function ItemBox:TryQueryFleetDetailReq(fleetID, fleetLevel)
  DebugOut("query fleet detail req", fleetID)
  local tmpLevel = 0
  if fleetLevel ~= nil then
    tmpLevel = fleetLevel
  end
  local req_content = {
    fleet_id = fleetID,
    level = tmpLevel,
    type = 0,
    req_type = 0
  }
  NetMessageMgr:SendMsg(NetAPIList.fleet_info_req.Code, req_content, ItemBox.GetCommanderDetailCallBack, true, nil)
end
function ItemBox.GetFleetDisplayCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.fleet_show_req.Code then
    return true
  elseif msgType == NetAPIList.fleet_show_ack.Code then
    if GameGlobalData.GlobalData.fleetinfo.fleetsDisplay == nil then
      GameGlobalData.GlobalData.fleetinfo.fleetsDisplay = {}
    end
    local fleetsDisplay = GameGlobalData.GlobalData.fleetinfo.fleetsDisplay
    for i, v in ipairs(content.fleets) do
      fleetsDisplay[v.fleet_id] = v
    end
    GameWaiting:HideLoadingScreen()
    ItemBox:ShowCommanderDetail2(content.fleets[1].fleet_id)
    return true
  end
  return false
end
function ItemBox:ShowCommanderDetail2(fleetID, fleetData, fleetLevel, onlyAdjutant, isHandbook, isPlayerName)
  if fleetData == nil and fleetData == nil then
    ItemBox:TryQueryFleetDetailReq(fleetID, fleetLevel)
    return
  end
  DebugOut("GameGlobalData.GlobalData.fleetDetails")
  DebugTable(GameGlobalData.GlobalData.fleetDetails)
  ItemBox.mCurCommander = fleetData
  DebugOut("----------show commander detail2")
  self.currentBox = "commander_detail2"
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetID, fleetData.level)
  local ability = GameDataAccessHelper:GetCommanderAbility(fleetID, fleetData.level)
  local name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(fleetID, fleetData.level))
  local userinfo = GameGlobalData:GetUserInfo()
  local leaderlist = GameGlobalData:GetData("leaderlist")
  if fleetID == 1 or leaderlist and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] == fleetID then
    name = GameUtils:GetUserDisplayName(userinfo.name)
  end
  if isHandbook then
    name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(fleetID, fleetData.level))
    if fleetID == 1 then
      name = GameLoader:GetGameText("LC_NPC_NPC_New")
    end
    if isPlayerName then
      name = GameUtils:GetUserDisplayName(userinfo.name)
    end
  end
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetID, fleetData.level)
  local vessleTypeName = GameLoader:GetGameText("LC_FLEET_" .. vessleType)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleetID, fleetData.level)
  if fleetID == 1 then
    if onlyAdjutant == nil and leaderlist and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
      iconFrame = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex], fleetData.level)
    else
      iconFrame = GameGlobalData:GetUserInfo().sex == 1 and "male" or "female"
    end
  end
  if isHandbook then
    iconFrame = GameDataAccessHelper:GetFleetAvatar(fleetID, fleetData.level)
  end
  local level = GameGlobalData:GetData("levelinfo").level
  if onlyAdjutant ~= nil then
    level = fleetData.showLevel
  end
  local fleetInfoTitle = fleetID .. "\001" .. iconFrame .. "\001" .. name .. "\001" .. level .. "\001" .. tostring(fleetData.force) .. "\001" .. vessleType .. "\001" .. vessleTypeName .. "\001" .. GameUtils:GetFleetRankFrame(ability.Rank, ItemBox.DownloadRankImageDetailCallback) .. "\001"
  local skill = basic_info.SPELL_ID
  local skillName = GameDataAccessHelper:GetSkillNameText(skill)
  local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
  local skillIconFram = GameDataAccessHelper:GetSkillIcon(skill)
  DebugOut("Skill Id", skill)
  DebugOut("skillIconFram = " .. skillIconFram)
  if skillRangeType == "TYPE_20" then
    skillRangeType = "TYPE_0"
  end
  local fleetSkill = skillIconFram .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
  local propertyName = ""
  for i = 1, 30 do
    local isGift = false
    for j = 1, #fleetData.gift do
      if fleetData.gift[j].gift == i then
        isGift = true
        break
      end
    end
    if isGift then
      if i == 18 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 21) .. "</font>" .. "\001"
      elseif i == 19 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 22) .. "</font>" .. "\001"
      elseif i == 20 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 23) .. "</font>" .. "\001"
      elseif i == 21 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 24) .. "</font>" .. "\001"
      elseif i == 22 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 31) .. "</font>" .. "\001"
      elseif i == 23 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 32) .. "</font>" .. "\001"
      elseif i == 24 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 33) .. "</font>" .. "\001"
      elseif i == 25 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 34) .. "</font>" .. "\001"
      elseif i == 26 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 35) .. "</font>" .. "\001"
      elseif i == 27 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 36) .. "</font>" .. "\001"
      elseif i == 28 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 37) .. "</font>" .. "\001"
      elseif i == 29 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 38) .. "</font>" .. "\001"
      elseif i == 30 then
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 39) .. "</font>" .. "\001"
      else
        propertyName = propertyName .. "<font color='#FC9901'>" .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "</font>" .. "\001"
      end
    elseif i == 18 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 21) .. "\001"
    elseif i == 19 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 22) .. "\001"
    elseif i == 20 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 23) .. "\001"
    elseif i == 21 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 24) .. "\001"
    elseif i == 22 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 31) .. "\001"
    elseif i == 23 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 32) .. "\001"
    elseif i == 24 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 33) .. "\001"
    elseif i == 25 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 34) .. "\001"
    elseif i == 26 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 35) .. "\001"
    elseif i == 27 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 36) .. "\001"
    elseif i == 28 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 37) .. "\001"
    elseif i == 29 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 38) .. "\001"
    elseif i == 30 then
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. 39) .. "\001"
    else
      propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "\001"
    end
  end
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleetID, fleetData.level)
  local phAttack = fleetData.ph_attack
  local enAttack = fleetData.en_attack
  if tonumber(fleetVessels) == 4 then
    phAttack = " \226\128\148 "
  else
    enAttack = " \226\128\148 "
  end
  DebugTable(fleetData)
  local crit_lv = "" .. fleetData.crit_lv / 10 .. "%"
  local crit_damage = "" .. (fleetData.crit_damage + 1500) / 10 .. "%"
  local anti_crit = "" .. fleetData.anti_crit / 10 .. "%"
  local intercept = "" .. fleetData.intercept / 10 .. "%"
  local penetration = "" .. fleetData.penetration / 10 .. "%"
  local motility = "" .. fleetData.motility / 10 .. "%"
  local hit_rate = "" .. fleetData.hit_rate / 10 .. "%"
  local damage_deepens = "" .. fleetData.damage_deepens / 10 .. "%"
  local damage_reduction = "" .. fleetData.damage_reduction / 10 .. "%"
  local skill_damage_deepens = "" .. fleetData.skill_damage_deepens / 10 .. "%"
  local skill_damage_reduction = "" .. fleetData.skill_damage_reduction / 10 .. "%"
  local anti_crit_damage = "" .. fleetData.anti_crit_damage / 10 .. "%"
  local buff_hit_rate = "" .. fleetData.buff_hit_rate / 10 .. "%"
  local anti_buff_hit_rate = "" .. fleetData.anti_buff_hit_rate / 10 .. "%"
  local propertyValue = ""
  for i = 1, 30 do
    local isGift = false
    for j = 1, #fleetData.gift do
      if fleetData.gift[j].gift == i then
        isGift = true
        break
      end
    end
    local pre = ""
    local suf = ""
    if isGift then
      pre = "<font color='#FC9901'>"
      suf = "</font>"
    end
    if i == 1 then
      propertyValue = propertyValue .. pre .. fleetData.max_durability .. suf .. "\001"
    elseif i == 2 then
      propertyValue = propertyValue .. pre .. fleetData.shield .. suf .. "\001"
    elseif i == 3 then
      propertyValue = propertyValue .. pre .. phAttack .. suf .. "\001"
    elseif i == 4 then
      propertyValue = propertyValue .. pre .. fleetData.ph_armor .. suf .. "\001"
    elseif i == 5 then
      propertyValue = propertyValue .. pre .. fleetData.sp_attack .. suf .. "\001"
    elseif i == 6 then
      propertyValue = propertyValue .. pre .. fleetData.sp_armor .. suf .. "\001"
    elseif i == 7 then
      propertyValue = propertyValue .. pre .. enAttack .. suf .. "\001"
    elseif i == 8 then
      propertyValue = propertyValue .. pre .. fleetData.en_armor .. suf .. "\001"
    elseif i == 9 then
      propertyValue = propertyValue .. pre .. crit_lv .. suf .. "\001"
    elseif i == 10 then
      propertyValue = propertyValue .. pre .. crit_damage .. suf .. "\001"
    elseif i == 11 then
      propertyValue = propertyValue .. pre .. anti_crit .. suf .. "\001"
    elseif i == 12 then
      propertyValue = propertyValue .. pre .. motility .. suf .. "\001"
    elseif i == 13 then
      propertyValue = propertyValue .. pre .. penetration .. suf .. "\001"
    elseif i == 14 then
      propertyValue = propertyValue .. pre .. intercept .. suf .. "\001"
    elseif i == 15 then
      propertyValue = propertyValue .. pre .. fleetData.fce .. suf .. "\001"
    elseif i == 16 then
      propertyValue = propertyValue .. pre .. fleetData.accumulator .. suf .. "\001"
    elseif i == 17 then
      propertyValue = propertyValue .. pre .. hit_rate .. suf .. "\001"
    elseif i == 18 then
      propertyValue = propertyValue .. pre .. damage_deepens .. suf .. "\001"
    elseif i == 19 then
      propertyValue = propertyValue .. pre .. damage_reduction .. suf .. "\001"
    elseif i == 20 then
      propertyValue = propertyValue .. pre .. skill_damage_deepens .. suf .. "\001"
    elseif i == 21 then
      propertyValue = propertyValue .. pre .. skill_damage_reduction .. suf .. "\001"
    elseif i == 22 then
      propertyValue = propertyValue .. pre .. fleetData.char_damage .. suf .. "\001"
    elseif i == 23 then
      propertyValue = propertyValue .. pre .. fleetData.accumulater_atk .. suf .. "\001"
    elseif i == 24 then
      propertyValue = propertyValue .. pre .. fleetData.accumulater_def .. suf .. "\001"
    elseif i == 25 then
      propertyValue = propertyValue .. pre .. fleetData.accumulater_crit .. suf .. "\001"
    elseif i == 26 then
      propertyValue = propertyValue .. pre .. fleetData.accumulater_intercept .. suf .. "\001"
    elseif i == 27 then
      propertyValue = propertyValue .. pre .. fleetData.accumulater_counter .. suf .. "\001"
    elseif i == 28 then
      propertyValue = propertyValue .. pre .. anti_crit_damage .. suf .. "\001"
    elseif i == 29 then
      propertyValue = propertyValue .. pre .. buff_hit_rate .. suf .. "\001"
    elseif i == 30 then
      propertyValue = propertyValue .. pre .. anti_buff_hit_rate .. suf .. "\001"
    end
  end
  DebugOut("zm test itembox propertyName:" .. propertyName)
  DebugOut("zm test itembox propertyValue:" .. propertyValue)
  local skill_upgrade_desc = fleetData.skill_upgrade_desc
  local flashObj = self:GetFlashObject()
  if flashObj then
    local adjutantSkillCnt = 0
    local hasAdjutantSkill = false
    if ItemBox.mCurCommander and ItemBox.mCurCommander.adjutant_spell and 0 < #ItemBox.mCurCommander.adjutant_spell then
      adjutantSkillCnt = #ItemBox.mCurCommander.adjutant_spell
      hasAdjutantSkill = true
    end
    local sex = GameDataAccessHelper:GetCommanderSex(fleetID)
    if sex == 1 then
      sex = "man"
    elseif sex == 0 then
      sex = "woman"
    else
      sex = "unknown"
    end
    local checkText = #skill_upgrade_desc > 0 and GameLoader:GetGameText("LC_MENU_BUTTON_CHECK_UPGRADE_SKILL") or GameLoader:GetGameText("LC_MENU_BUTTON_NOT_UPGRADE_SKILL")
    flashObj:InvokeASCallback("_root", "showcommander_detailBox2", fleetInfoTitle, fleetSkill, propertyName, propertyValue, hasAdjutantSkill, sex, #skill_upgrade_desc, checkText, onlyAdjutant)
    if adjutantSkillCnt > 0 then
      flashObj:InvokeASCallback("_root", "SetAdjutantSkillList", adjutantSkillCnt + 1)
    end
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showCommanderDetail2MoveIn")
  end
end
function ItemBox.DownloadRankImageDetailCallback(extInfo)
  if ItemBox:GetFlashObject() then
    ItemBox:GetFlashObject():InvokeASCallback("_root", "updateRankImageDetail", extInfo.rank_id)
  end
end
function ItemBox:SetContentsInfoPage()
  if ItemBox:GetFlashObject() then
    ItemBox:GetFlashObject():InvokeASCallback("_root", "setContentsInfoPage")
  end
end
function ItemBox:ShowCommanderDetail(fleetData, isNormalFleet)
  self.currentBox = "commander_detail"
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  local name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(fleetData.identity, fleetData.level))
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local userinfo = GameGlobalData:GetUserInfo()
  if fleetData.identity == 1 or leaderlist and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] == fleetData.identity then
    name = GameUtils:GetUserDisplayName(userinfo.name)
  end
  local level = GameGlobalData:GetData("levelinfo").level
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetData.identity, fleetData.level)
  local vessleTypeName = GameLoader:GetGameText("LC_FLEET_" .. vessleType)
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(fleetData.identity, fleetData.level)
  if fleetData.identity == 1 and leaderlist and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
    iconFrame = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex], fleetData.level)
  end
  local fleetInfoTitle = fleetData.identity .. "\001" .. iconFrame .. "\001" .. name .. "\001" .. level .. "\001" .. tostring(fleetData.force) .. "\001" .. vessleType .. "\001" .. vessleTypeName .. "\001"
  local skill = fleetData.active_spell
  local skillName = GameDataAccessHelper:GetSkillNameText(skill)
  local skillDesc = GameDataAccessHelper:GetSkillDesc(skill)
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill))
  local skillIconFram = GameDataAccessHelper:GetSkillIcon(skill)
  local fleetSkill = skillIconFram .. "\001" .. skillName .. "\001" .. skillDesc .. "\001" .. skillRangeType .. "\001" .. skillRangetypeName .. "\001"
  local propertyName = ""
  for i = 1, 8 do
    propertyName = propertyName .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. i) .. "\001"
  end
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleetData.identity, fleetData.level)
  local phAttack = fleetData.ph_attack
  local enAttack = fleetData.en_attack
  if tonumber(fleetVessels) == 4 then
    phAttack = " \226\128\148 "
  else
    enAttack = " \226\128\148 "
  end
  local sex = GameDataAccessHelper:GetCommanderSex(fleetData.identity)
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  local propertyValue = fleetData.max_durability .. "\001" .. fleetData.shield .. "\001" .. phAttack .. "\001" .. fleetData.ph_armor .. "\001" .. fleetData.sp_attack .. "\001" .. fleetData.sp_armor .. "\001" .. enAttack .. "\001" .. fleetData.en_armor .. "\001"
  self:GetFlashObject():InvokeASCallback("_root", "showcommander_detailBox", fleetInfoTitle, fleetSkill, propertyName, propertyValue, isNormalFleet, sex)
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showCommanderDetailMoveIn")
end
function ItemBox:UpdateChallegeTime(lefttime)
  if self.currentBox == "arena_opponent_info" and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateChallegeTime", lefttime)
  end
end
function ItemBox:UpdateChallegeTime_tlc(lefttime)
  if self.currentBox == "arena_opponent_info" and self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateChallegeTime_tlc", lefttime)
  end
end
function ItemBox:ShowArenaOpponentInfo(opponent_info)
  self:GetArenaPlayerDetailInfo(opponent_info.user_id)
  self.currentBox = "arena_opponent_info"
  local data_table = {}
  data_table[#data_table + 1] = opponent_info.name
  data_table[#data_table + 1] = opponent_info.avatar
  data_table[#data_table + 1] = opponent_info.level
  data_table[#data_table + 1] = opponent_info.rank
  data_table[#data_table + 1] = GameUtils.numberAddCommaWithBackslash(opponent_info.force, true)
  for i = 1, 5 do
    local commander_data = opponent_info.commander_array[i]
    data_table[#data_table + 1] = tostring(commander_data.commander_type)
    data_table[#data_table + 1] = tostring(commander_data.commander_avatar)
  end
  data_table[#data_table + 1] = tostring(opponent_info.pos_x)
  data_table[#data_table + 1] = tostring(opponent_info.pos_y)
  data_table[#data_table + 1] = tostring(opponent_info.nextChallengeTime)
  local data_string = table.concat(data_table, "\001")
  DebugOutPutTable(data_table, "data_table")
  DebugOut(data_string)
  self:GetFlashObject():InvokeASCallback("_root", "showArenaOpponentInfo", data_string, GameLoader:GetGameText("LC_MENU_Level"))
end
function ItemBox:HideArenaOpponentInfo()
  self:GetFlashObject():InvokeASCallback("_root", "hideArenaOpponentInfo")
  local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
  if GameUIArena:IsActive() then
    GameUIArena:SelectOpponent(-1)
  end
end
function ItemBox:GetArenaPlayerDetailInfo(id)
  local friend_detail_req_content = {user_id = id}
  NetMessageMgr:SendMsg(NetAPIList.user_brief_info_req.Code, friend_detail_req_content, self.playerDetailCallback, true, nil)
end
function ItemBox.playerDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_brief_info_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.user_brief_info_ack.Code then
    ItemBox.friend_detail = content
    return true
  end
  return false
end
function ItemBox:ShowPrestigeInfo(info)
  self.currentBox = "prestige_info"
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
    self:GetFlashObject():InvokeASCallback("_root", "showPrestigeInfoMoveIn")
  end
  self:GetFlashObject():InvokeASCallback("_root", "showPrestigeInfo", info)
end
function ItemBox:HidePrestigeInfo()
  self:GetFlashObject():InvokeASCallback("_root", "hidePrestigeInfo")
end
function ItemBox:hideEquipButton()
  self:GetFlashObject():InvokeASCallback("_root", "hideEquipButton")
end
function ItemBox:ShowReport(report_data)
  self.currentBox = "report"
  self.informant_data = report_data
  local informant_chat_info
  DebugTable(self.informant_data.evidences)
  for _, v_info in ipairs(self.informant_data.evidences) do
    if v_info.inappropriate > 0 then
      informant_chat_info = v_info
      break
    end
  end
  assert(informant_chat_info)
  local param_table = {}
  table.insert(param_table, informant_chat_info.sender)
  table.insert(param_table, informant_chat_info.content)
  local param_string = table.concat(param_table, ",")
  DebugOut(param_string)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "showReportBox", param_string)
end
function ItemBox:HideReport()
  self.currentBox = nil
  self.informant_data = nil
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_hideReportBox")
end
function ItemBox:ShowBox(boxType, operateLeftText, operateRightText)
  if not self.currentBox then
    self.GetAppendState():AddObject(self)
    self.currentBox = boxType
    if boxType == "Equip" then
      operateRightText = operateRightText or ""
      operateLeftText = operateLeftText or ""
      self:GetFlashObject():InvokeASCallback("_root", "ShowEquipBox", operateLeftText, operateRightText)
    elseif boxType == "Item" then
      self:GetFlashObject():InvokeASCallback("_root", "ShowItemBox", operateLeftText, operateRightText)
    elseif boxType == "Medal" then
      self:GetFlashObject():InvokeASCallback("_root", "ShowItemBox", operateLeftText, operateRightText)
    elseif boxType == "Tech" then
      self:ShowTechBox()
    elseif boxType == "ChoosableItem" then
      self:GetFlashObject():InvokeASCallback("_root", "showChoosableBox")
    else
      assert(false, boxType)
    end
  end
end
function ItemBox:HideBox(boxType)
  if not self:GetFlashObject() then
    return
  end
  if boxType == "Equip" then
    self:GetFlashObject():InvokeASCallback("_root", "HideEquipBox")
  elseif boxType == "Krypton" then
    self:HideKryptonBox()
  elseif boxType == "Item" then
    self:GetFlashObject():InvokeASCallback("_root", "HideItemBox")
  elseif boxType == "Tech" then
    self:HideTechBox()
  elseif boxType == "report" then
    self:HideReport()
  elseif boxType == "commander_info" then
    self:HideCommanderInfo()
  elseif boxType == "arena_opponent_info" then
    self:HideArenaOpponentInfo()
  elseif boxType == "commander_detail" then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showCommanderDetailMoveOut")
  elseif boxType == "commander_detail2" then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_showCommanderDetail2MoveOut")
  elseif boxType == "prestige_info" then
    self:GetFlashObject():InvokeASCallback("_root", "showPrestigeInfoMoveOut")
  elseif boxType == "prestige_pop" then
    self:GetFlashObject():InvokeASCallback("_root", "showprestigePopMoveOut")
  elseif boxType == "rankPlayerInfo" then
    self:GetFlashObject():InvokeASCallback("_root", "showPlayerInfoMoveOut")
  elseif boxType == "rankPlayerInfo_tlc" then
    self:GetFlashObject():InvokeASCallback("_root", "showPlayerInfoMoveOut_tlc")
  elseif boxType == "diceAwards" then
    self:GetFlashObject():InvokeASCallback("_root", "dicePanelMoveout")
  elseif boxType == "ChoosableItem" then
    self:GetFlashObject():InvokeASCallback("_root", "showChoosableItemMoveOut")
  elseif boxType == "idConfirm" then
    self:GetFlashObject():InvokeASCallback("_root", "IdConfirmBox2Moveout")
  elseif boxType == "buyConfirm" then
    self:GetFlashObject():InvokeASCallback("_root", "IdConfirmBox3Moveout")
  elseif boxType == "rewardInfo" then
    self.g_isRewardShowing = false
    self:GetFlashObject():InvokeASCallback("_root", "HideRewardBox")
  elseif boxType == "ItemChoices" then
    self:GetFlashObject():InvokeASCallback("_root", "HideItemChoices")
  end
  self.operateFunc = nil
  if self.hideBoxCallback then
    self.hideBoxCallback()
  end
end
function ItemBox:SetPos(xPos, yPos, itemW, itemH, boxIndex)
  self:GetFlashObject():InvokeASCallback("_root", "SetItemPos", self.currentBox, xPos, yPos, itemW, itemH, boxIndex)
end
function ItemBox:SetEquipBox(item, itemType)
  self.currentBox = "Equip"
  self.currentItem = item
  self.currentItemType = itemType
  local EquipItemBoxShowObj = {
    itemFrame = nil,
    nameText = nil,
    typeText = nil,
    levelText = nil,
    Params = {},
    reqlevelText = nil,
    shipText = nil,
    costText = nil,
    desText = nil,
    StrengthenLv = nil,
    canReq = false,
    matrix = false
  }
  local reqLevel
  local matrix = false
  if item then
    local itemm = GameItemBag.itemInBag[item.pos]
    matrix = itemm and #itemm.equip_matrixs > 0
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if item.item_id == v.equip_id then
        item = v
      end
    end
    DebugOut("tp's test itembox:")
    DebugTable(item)
    EquipItemBoxShowObj.nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
    EquipItemBoxShowObj.desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemType)
    EquipItemBoxShowObj.typeText = GameLoader:GetGameText("LC_MENU_EQUIP_TYPE_" .. GameDataAccessHelper:GetEquipSlot(itemType))
    EquipItemBoxShowObj.levelText = item.equip_level
    EquipItemBoxShowObj.StrengthenLv = item.enchant_level
    for k, v in ipairs(item.equip_params) do
      table.insert(EquipItemBoxShowObj.Params, {
        Color = 16749350,
        Text = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key),
        Value = GameUtils.numberConversion(v.value)
      })
    end
    if item.enchant_level ~= 0 then
      table.insert(EquipItemBoxShowObj.Params, {
        Color = 48379,
        Text = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. item.enchant_item_id),
        Value = GameLoader:GetGameText("LC_MENU_Level") .. GameUtils.numberConversion(item.enchant_level)
      })
      table.insert(EquipItemBoxShowObj.Params, {
        Color = 48379,
        Text = GameLoader:GetGameText("LC_MENU_Equip_param_" .. item.enchant_effect),
        Value = GameUtils.numberConversion(item.enchant_effect_value)
      })
    end
    reqLevel = item.equip_level_req
    reqLevel = reqLevel or GameDataAccessHelper:GetEquipReqLevel(itemType)
    EquipItemBoxShowObj.shipText = GameLoader:GetGameText("LC_MENU_EQUIP_REQUIRE_TYPE_" .. GameDataAccessHelper:GetEquipReqFleet(itemType))
    EquipItemBoxShowObj.costText = GameDataAccessHelper:GetEquipPrice(itemType)
  else
    local itemPrice = GameDataAccessHelper:GetEquipPrice(itemType)
    EquipItemBoxShowObj.nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
    EquipItemBoxShowObj.desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemType)
    EquipItemBoxShowObj.typeText = GameLoader:GetGameText("LC_MENU_EQUIP_TYPE_" .. GameDataAccessHelper:GetEquipSlot(itemType))
    EquipItemBoxShowObj.levelText = 1
    EquipItemBoxShowObj.StrengthenLvText = 0
    table.insert(EquipItemBoxShowObj.Params, {
      Color = 16749350,
      Text = GameLoader:GetGameText("LC_MENU_Equip_param_" .. GameDataAccessHelper:GetEquipParam(itemType)),
      Value = GameDataAccessHelper:GetEquipValue(itemType)
    })
    EquipItemBoxShowObj.shipText = GameLoader:GetGameText("LC_MENU_EQUIP_REQUIRE_TYPE_" .. GameDataAccessHelper:GetEquipReqFleet(itemType))
    EquipItemBoxShowObj.costText = GameDataAccessHelper:GetEquipPrice(itemType)
    reqLevel = GameDataAccessHelper:GetEquipReqLevel(itemType)
  end
  EquipItemBoxShowObj.itemFrame = "item_" .. itemType
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if reqLevel then
    EquipItemBoxShowObj.canReq = reqLevel <= levelInfo.level
    EquipItemBoxShowObj.reqlevelText = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. reqLevel
  end
  EquipItemBoxShowObj.matrix = matrix
  DebugOut("SetEquipBox: ")
  DebugTable(EquipItemBoxShowObj)
  self:GetFlashObject():InvokeASCallback("_root", "SetEquipBox", EquipItemBoxShowObj)
  return #EquipItemBoxShowObj.Params
end
function ItemBox:ShowKryptonBox(xpos, ypos, operationTable)
  local operationString = ""
  operationString = operationString .. (operationTable.btnUpgradeVisible and "1" or "0")
  operationString = operationString .. (operationTable.btnUseVisible and "1" or "0")
  operationString = operationString .. (operationTable.btnDecomposeVisible and "1" or "0")
  operationString = operationString .. (operationTable.btnUnloadVisible and "1" or "0")
  self:GetFlashObject():InvokeASCallback("_root", "showKryptonBox", xpos, ypos, operationString)
  local ActiveState = self.GetAppendState()
  self.currentBox = "Krypton"
  self.GetAppendState():AddObject(self)
end
function ItemBox:SetKryptonBox(item, itemType, exceptNumber)
  exceptNumber = exceptNumber or 0
  self.currentItem = item
  self.currentItemType = itemType
  local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
  for k, v in pairs(fleetkryptons) do
    if v and v.kryptons then
      for tk, tv in pairs(v.kryptons) do
        if item.id == tv.id then
          item = tv
          break
        end
      end
    end
  end
  local nameText = ""
  local paramText, paramText1 = "", ""
  local paramNum, paramNum1 = "", ""
  local paramNext, paramNext1 = "", ""
  local paramDetail = "null"
  nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
  local isMax = "0"
  local isDouble = "0"
  local isParamInfo = "0"
  local addon1 = item.addon[1]
  if addon1.type == 200 then
    isParamInfo = "1"
    paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
    paramDetail = paramText
  else
    if item.addon[2] ~= nil then
      isDouble = "1"
    end
    paramNum = addon1.value
    paramNext = addon1.next_value
    paramText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon1.type)
    if addon1.type >= 9 and addon1.type <= 14 or addon1.type == 17 or addon1.type == 19 or addon1.type == 20 or addon1.type >= 37 and addon1.type <= 39 then
      paramNum = paramNum / 10 .. "%"
      paramNext = paramNext / 10 .. "%"
    end
    if addon1.type == 19 then
      paramText = GameLoader:GetGameText("LC_MENU_Equip_param_21")
    end
    if addon1.type == 20 then
      paramText = GameLoader:GetGameText("LC_MENU_Equip_param_22")
    end
    paramNum = "+" .. paramNum
    paramNext = "+" .. paramNext
    paramDetail = GameLoader:GetGameText("LC_MENU_Equip_param_info_" .. addon1.type)
    if isDouble == "1" then
      local addon2 = item.addon[2]
      paramNum1 = addon2.value
      paramNext1 = addon2.next_value
      paramText1 = GameLoader:GetGameText("LC_MENU_Equip_param_" .. addon2.type)
      if addon2.type >= 9 and addon2.type <= 14 or addon2.type == 17 or addon2.type == 19 or addon2.type == 20 or addon2.type >= 37 and addon2.type <= 39 then
        paramNum1 = paramNum1 / 10 .. "%"
        paramNext1 = paramNext1 / 10 .. "%"
      end
      if addon2.type == 19 then
        paramText1 = GameLoader:GetGameText("LC_MENU_Equip_param_21")
      end
      if addon2.type == 20 then
        paramText1 = GameLoader:GetGameText("LC_MENU_Equip_param_22")
      end
      paramNum1 = "+" .. paramNum1
      paramNext1 = "+" .. paramNext1
      paramDetail = paramDetail .. "\n" .. GameLoader:GetGameText("LC_MENU_Equip_param_info_" .. addon2.type)
    end
  end
  if item.max_level then
    isMax = "1"
  end
  local datatable = {}
  datatable[#datatable + 1] = nameText
  datatable[#datatable + 1] = isMax
  datatable[#datatable + 1] = isDouble
  datatable[#datatable + 1] = isParamInfo
  datatable[#datatable + 1] = paramText
  datatable[#datatable + 1] = paramText1
  datatable[#datatable + 1] = paramNum
  datatable[#datatable + 1] = paramNum1
  datatable[#datatable + 1] = paramNext
  datatable[#datatable + 1] = paramNext1
  datatable[#datatable + 1] = paramDetail
  if DynamicResDownloader:IsDynamicStuff(itemType, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(itemType .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if not DynamicResDownloader:IfResExsit(localPath) then
      datatable[#datatable + 1] = "temp"
    else
      datatable[#datatable + 1] = "item_" .. itemType
    end
  else
    datatable[#datatable + 1] = "item_" .. itemType
  end
  datatable[#datatable + 1] = item.rank
  datatable[#datatable + 1] = item.kenergy_upgrade
  datatable[#datatable + 1] = item.decompose_kenergy
  datatable[#datatable + 1] = #item.formation - exceptNumber
  local datastring = table.concat(datatable, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "SetKryptonBox", datastring)
end
function ItemBox:ShowKryptonLevelUp()
  if self:GetFlashObject() then
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "showKryptonLevelUp", lang)
  end
end
function ItemBox:HideKryptonBox()
  self.operateFunc = nil
  self:GetFlashObject():InvokeASCallback("_root", "HideKryptonBox")
end
function ItemBox:SetChoosableItemBox(item, itemType)
  DebugOut("ItemBox:SetChoosableItemBox")
  self.currentBox = "ChoosableItem"
  self.currentItem = item
  self.currentItemType = itemType
  self.choosableIndex = 1
  local nameText = ""
  local levelTxt = GameDataAccessHelper:GetItemReqLevel(itemType)
  nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
  local choosable_items = ItemBox.choosable_items
  DebugOut("aha?")
  DebugTable(choosable_items)
  local itemFrame = "item_" .. itemType
  if DynamicResDownloader:IsDynamicStuff(itemType, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(itemType .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if not DynamicResDownloader:IfResExsit(localPath) then
      GameHelper:AddDownloadPathForGameItem(itemType, nil, nil)
      itemFrame = "temp"
    end
  end
  for k, v in ipairs(choosable_items) do
    local iinfo = GameHelper:GetItemInfo(v)
    v.cnt = iinfo.cnt
    v.iconframe = iinfo.icon_frame
    v.iconpic = iinfo.icon_pic
    v.quality = iinfo.quality
  end
  DebugOut("choosable_items:")
  DebugTable(choosable_items)
  local canUse = true
  if self.operateLeftFunc == nil then
    canUse = false
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetChoosableItemBox", nameText, levelTxt, itemFrame, choosable_items, item.cnt, canUse, GameLoader:GetGameText("LC_MENU_Level"))
  self:GetFlashObject():InvokeASCallback("_root", "SetTipText", GameLoader:GetGameText("LC_MENU_CHOOSE_ONE_BELOW"))
  DebugOut("choosable_items[1].can_use_more:", choosable_items[1].can_use_more)
  if choosable_items[1].can_use_more == 1 and canUse then
    self:GetFlashObject():InvokeASCallback("_root", "SetToOneAndDisable")
  end
end
function ItemBox:SetChoosableIndex(index)
  self.choosableIndex = index
end
function ItemBox:SetItemBox(item, itemType, showSellOrUse)
  self.currentBox = "Item"
  self.currentItem = item
  self.currentItemType = itemType
  local nameText, desText = "", ""
  nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. itemType)
  desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemType)
  if (immanentversion170 == 4 or immanentversion170 == 5) and itemType >= 101 and itemType <= 196 then
    desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. itemType .. "_N")
    local battleIdList = GameDataAccessHelper:GetAdventureDrop(itemType)
    local str = "\n"
    for k, v in ipairs(battleIdList or {}) do
      local arena = math.floor(tonumber(v) / 1000)
      str = str .. GameLoader:GetGameText("LC_MENU_BUILDING_NAME_COMMANDER_ACADEMY") .. "--" .. GameLoader:GetGameText("LC_MENU_AREA_NAME_" .. arena) .. "--" .. GameDataAccessHelper:GetAreaNameText(51, tonumber(v))
      str = str .. "\n"
    end
    desText = desText .. str
  end
  local showDes = true
  if itemType >= 101 and itemType <= 196 then
    showDes = false
  end
  local itemNum = 1
  if item then
    itemNum = item.cnt
  end
  local costText = GameDataAccessHelper:GetItemPrice(itemType)
  local reqLevel = GameDataAccessHelper:GetItemReqLevel(itemType)
  local maxLevel = GameDataAccessHelper:GetItemMaxLevel(itemType)
  local reqText = ""
  if maxLevel == -1 then
    reqText = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. reqLevel
  else
    reqText = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. reqLevel .. " - " .. maxLevel
  end
  local itemFrame = "item_" .. itemType
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local canReq = false
  if maxLevel == -1 then
    canReq = reqLevel <= levelinfo.level
  else
    canReq = reqLevel <= levelinfo.level and maxLevel >= levelinfo.level
  end
  if DynamicResDownloader:IsDynamicStuff(itemType, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(itemType .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if not DynamicResDownloader:IfResExsit(localPath) then
      itemFrame = "temp"
    end
  end
  local isLot = 0
  if GameData.Item.Keys[itemType] ~= nil then
    isLot = GameData.Item.Keys[itemType].LOT
  end
  if not showSellOrUse or not item or not item.cnt or tonumber(item.cnt) == 1 then
    isLot = 0
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetSliderMinMax", 1, item.cnt)
  end
  DebugOut("SetItemBox:isLot:", isLot)
  self:GetFlashObject():InvokeASCallback("_root", "SetItemBox", nameText, desText, costText, reqText, itemFrame, nil, canReq, isLot, showDes, -1)
  if ItemBox.showItemParam.hideCost then
    self:GetFlashObject():InvokeASCallback("_root", "ItemBoxHideCost")
    ItemBox.showItemParam.hideCost = false
  end
end
function ItemBox:GetMedalItemQulity(nid, check)
end
function ItemBox:SetMedalBox(item, nid, showSellOrUse)
  self.currentBox = "Item"
  self.currentItem = item
  self.currentItemType = nid
  local nameText, desText = "", ""
  item.quality = GameHelper:GetMedalQuality("medal_item", nid)
  nameText = GameHelper:GetAwardTypeText(item.item_type, item.number)
  if item.item_type == "medal" then
    local id = math.ceil(item.number / 100000 - 1) * 100000 + item.number % 10
    desText = GameLoader:GetGameText("LC_MENU_MEDAL_DESC_" .. id)
  elseif item.item_type == "medal_item" then
    desText = GameLoader:GetGameText("LC_MENU_MEDAL_ITEM_DESC_" .. nid)
  else
    assert(false, item.item_type)
  end
  local costText = ""
  local reqText = ""
  local showDes = false
  local canReq = false
  local isLot = false
  local itemFrame = GameHelper:GetAwardTypeIconFrameName(item.item_type, item.number)
  local itemPic = GameHelper:GetGameItemDownloadPng(item.item_type, item.number)
  self:GetFlashObject():InvokeASCallback("_root", "SetItemBox", nameText, desText, costText, reqText, itemFrame, itempic, canReq, isLot, showDes, item.quality)
  if AutoUpdate.isDeveloper then
    self:GetFlashObject():InvokeASCallback("_root", "SetItemId", item.number)
  end
  self:GetFlashObject():InvokeASCallback("_root", "ItemBoxHideCost")
end
function ItemBox:RegisterHideBoxNtf(func)
  self.hideBoxCallback = func
end
function ItemBox:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "HideBox" then
    self:HideBox(arg)
  end
  if cmd == "UseMaxSelect" then
    ItemBox.useMaxCheck = tonumber(arg)
  end
  if cmd == "boxHiden" then
    if self.currentBox == "Tech" then
      local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
      GameUITechnology:SelectTechItem(-1)
    elseif self.currentBox == "commander_info" then
      local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
      if self.GetAppendState() == GameStateManager.GameStateRecruit then
        GameUIRecruitMain:SelectCommander(GameUIRecruitMain:GetActiveCommanderType(), -1)
      end
    elseif self.currentBox == "commander_detail" then
    elseif self.currentBox == "commander_detail2" then
    elseif self.currentBox == "Equip" then
    elseif self.currentBox == "ItemChoices" then
      self.ItemChoicesCallback = nil
    end
    self.currentBox = nil
    self.currentItem = nil
    self.currentItemType = nil
    self.GetAppendState():EraseObject(self)
    ItemBox.bUseMax = false
    ItemBox.useMaxCheck = 0
    if QuestTutorialUseTechLab:IsActive() and GameUITechnology:GetFlashObject() then
      GameUITechnology:GetFlashObject():InvokeASCallback("_root", "ShowTutorialUpgrade")
    end
  end
  if cmd == "ShieldReleased" and not GameTextEdit:GetKeyboard() then
    self:HideBox(self.currentBox)
    if GameItemBag:GetFlashObject() then
      local index = math.floor(GameItemBag.lastReleasedIndex / 4) * 4 + 1
      GameItemBag:UpdateFlashListItem(index)
    end
    if GameUIKrypton:GetFlashObject() then
      GameUIKrypton:RefreshCurFleetKrypton()
    end
    if GameFleetNewEnhance:GetFlashObject() then
      GameFleetNewEnhance:RefreshCurFleetKrypton()
    end
  end
  if cmd == "Rename" then
    self.RenameText = arg
    if self.operateLeftFunc then
      self.operateLeftFunc()
      self.operateLeftFunc = nil
      self:HideBox(self.currentBox)
    end
  end
  if cmd == "operateItemLeft" then
    if GameDataAccessHelper:IsItemUsedRename(self.currentItemType) then
      local title = GameLoader:GetGameText("LC_MENU_USE_ITEM_" .. self.currentItemType .. "_TITLE")
      local desc = GameLoader:GetGameText("LC_MENU_USE_ITEM_" .. self.currentItemType .. "_DESC")
      self:GetFlashObject():InvokeASCallback("_root", "SetSendBtnText", GameLoader:GetGameText("LC_MENU_USE_CHAR"))
      self:GetFlashObject():InvokeASCallback("_root", "ShowRenameBox", title, desc)
      return
    end
    local count = tonumber(arg)
    count = count or 1
    DebugOut("operateItemLeft:", count)
    if self.operateLeftFunc then
      self.operateLeftFunc(count)
      self.operateLeftFunc = nil
      self:HideBox(self.currentBox)
    end
  elseif cmd == "operateItemRight" then
    local count = tonumber(arg)
    count = count or 1
    if self.operateRightFunc then
      self.operateRightFunc(count)
      self.operateRightFunc = nil
      self:HideBox(self.currentBox)
    end
  end
  if cmd == "upgrade_tech" then
    local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
    GameUITechnology:UpgradeTechWithID(self._activeTechInfo.id, 1)
    if QuestTutorialUseTechLab:IsActive() then
      QuestTutorialUseTechLab:SetFinish(true)
      GameUITechnology.m_tempStoryWhenClose = {
        100032,
        100046,
        100047
      }
      GameUITechnology:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
      GameUITechnology:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
      self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTechLab")
    end
    return
  end
  if cmd == "upgrade_tech_quick" then
    if self:IsShowTechQuick() then
      local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
      GameUITechnology:OneKeyUpdate(self._activeTechInfo.id, 0)
      self:HideTechBox()
      if QuestTutorialUseTechLab:IsActive() then
        QuestTutorialUseTechLab:SetFinish(true)
        GameUITechnology.m_tempStoryWhenClose = {
          100032,
          100046,
          100047
        }
        GameUITechnology:GetFlashObject():InvokeASCallback("_root", "ShowTutorialClose")
        GameUITechnology:GetFlashObject():InvokeASCallback("_root", "HideTutorialUpgrade")
        self:GetFlashObject():InvokeASCallback("_root", "HideTutorialTechLab")
      end
    else
      local limitVipLevel = GameDataAccessHelper:GetVIPLimit("one_key_technology_update")
      local limitPlayerLevel = GameDataAccessHelper:GetLevelLimit("one_key_technology_update")
      local showTips = GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_OR_VIP_INFO")
      showTips = string.gsub(showTips, "<playerLevel_num>", limitPlayerLevel)
      showTips = string.gsub(showTips, "<vipLevel_num>", limitVipLevel)
      GameUtils:ShowTips(showTips)
    end
    return
  end
  if cmd == "report_player" then
    self.informant_data.reason = arg
    NetMessageMgr:SendMsg(NetAPIList.create_tipoff_req.Code, self.informant_data, self.informantServerCallback, false, nil)
    return
  end
  if cmd == "challenge_opponent" then
    self.GetAppendState():EraseObject(self)
    local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
    if GameUIArena:IsActive() then
      GameUIArena:RequireEnemyMatrixWithRank(GameUIArena.LastSelectOpponentInfo.rank)
    end
  elseif cmd == "challenge_opponent_tlc" then
    self.GetAppendState():EraseObject(self)
    local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
    if GameUIArena:IsActive() then
      GameUIArena:RequireEnemyMatrixWithRank(GameUIArena.LastSelectOpponentInfo.rank)
    end
  elseif cmd == "refresh_challenge_time" then
    local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
    if GameUIArena:IsActive() then
      GameUIArena:CleanPVPCD()
    end
  elseif cmd == "refresh_challenge_time_tlc" then
    local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
    if GameUIArena:IsActive() then
      GameUIArena:CleanPVPCD_tlc()
    end
  end
  if cmd == "krypton_unload" then
    if GameUIKrypton.UnloadKrypton then
      GameUIKrypton.UnloadKrypton()
      GameUIKrypton.UnloadKrypton = nil
    end
    if GameFleetNewEnhance.UnloadKrypton then
      GameFleetNewEnhance.UnloadKrypton()
      GameFleetNewEnhance.UnloadKrypton = nil
    end
    ItemBox:HideKryptonBox()
  elseif cmd == "krypton_upgrade" then
    if GameUIKrypton:GetFlashObject() then
      GameUIKrypton:OnFSCommand("chargePress")
    end
    if GameFleetNewEnhance:GetFlashObject() then
      GameFleetNewEnhance:OnFSCommand("chargePress")
    end
  elseif cmd == "krypton_load" then
    if GameUIKrypton:GetFlashObject() then
      GameUIKrypton.UseKrypton()
      GameUIKrypton:RefreshCurFleetKrypton()
    end
    if GameFleetNewEnhance:GetFlashObject() then
      GameFleetNewEnhance.UseKrypton()
      GameFleetNewEnhance:RefreshCurFleetKrypton()
    end
    ItemBox:HideKryptonBox()
  elseif cmd == "krypton_decompose" then
    GameUIKrypton.requestDecompose()
    ItemBox:HideKryptonBox()
  elseif cmd == "rankUpPrstige" then
    DebugOut("fuck rankUpPrstige ", tonumber(arg))
    if self.GetAppendState():IsObjectInState(GameUIPrestige) then
      GameUIPrestige:RankUpPrstige(arg)
    end
  elseif cmd == "gotoBtnClick" then
    GameFleetEquipment:OnFSCommand("gotoBtnClick", nil)
  elseif cmd == "NeedUpdateAdjutantSkillListItem" then
    ItemBox:UpdateAdjutantSkillListItem(tonumber(arg))
  elseif cmd == "updateItemdata" then
  elseif cmd == "click_item" then
  elseif cmd == "onPaySelect_wechat" then
    ItemBox.tPayOption.callback_wechat()
  elseif cmd == "onPaySelect_zfb" then
    ItemBox.tPayOption.callback_zfb()
  elseif cmd == "onLackRes_ok" then
    ItemBox.creditconfirm_callback()
  elseif cmd == "erase_dialog" then
    ItemBox.EraseFromeState()
    if ItemBox.callback_erase_dialog then
      ItemBox.callback_erase_dialog()
      ItemBox.callback_erase_dialog = nil
    end
  end
  if cmd == "showPlayerDetailInfoLayer" then
    local GameUIPlayerView = LuaObjectManager:GetLuaObject("GameUIPlayerView")
    GameUIPlayerView:SetPlayerData(self.friend_detail)
    GameUIPlayerView:ShowPlayerView()
    ItemBox:OnFSCommand("ShieldReleased")
  elseif cmd == "UpdateUpgradeSkillDesc" and ItemBox.mCurCommander then
    local itemKey = tonumber(arg)
    local skill_desc = ItemBox.mCurCommander.skill_upgrade_desc
    local skill_item = skill_desc[itemKey]
    local desc = GameLoader:GetGameText("LC_FLEET_SKILL_DES_" .. skill_item.desc_key)
    local title = GameLoader:GetGameText("LC_MENU_REFORM_BUTTON") .. "+" .. GameDataAccessHelper:GetFleetLevelStr(skill_item.num) .. ":"
    local textDesc = ""
    local data = {}
    if skill_item.is_finish then
      textDesc = "<font color='#FFCC00'>" .. title .. "\n" .. desc .. "</font>"
    else
      textDesc = "<font color='#666666'>" .. title .. "\n" .. desc .. "</font>"
    end
    data.desc = textDesc
    data.isFinish = skill_item.is_finish
    self:GetFlashObject():InvokeASCallback("_root", "updateSkillUpgradeSkillDesc", itemKey, data)
  end
  if cmd == "choosed_item" then
    local choosed_itemindex = tonumber(arg) + 1
    self.choosableIndex = choosed_itemindex
    local item = self.choosable_items[choosed_itemindex]
    DebugOut("item.can_use_more", item.can_use_more)
    if item.can_use_more == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "SetToOneAndDisable")
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetToOneAndEnable")
    end
    GameItemBag:SetCurrentChoosableItemsIndex(choosed_itemindex)
  elseif cmd == "useChoosableItem" then
    local info = LuaUtils:string_split(arg, "^")
    local count = tonumber(info[1])
    local idx = tonumber(info[2]) + 1
    self.operateLeftFunc(count, idx)
    self.operateLeftFunc = nil
  elseif cmd == "showChoiceDetail" then
    local choosed_itemindex = tonumber(arg) + 1
    local item = self.choosable_items[choosed_itemindex]
    local iteminfo = self:getItemInfo(item, self.itemType)
    self:GetFlashObject():InvokeASCallback("_root", "showChoiceDetail", iteminfo)
  elseif cmd == "sendIdMessage" then
    local id_name = LuaUtils:string_split(arg, "!")
    local id = id_name[1]
    local name = id_name[2]
    local tel = id_name[3]
    if self.isIdComfird then
      self:HideBox(self.currentBox)
    elseif id and name and id ~= "" and name ~= "" and GameUtils:isIDNumberCorrect(id) and GameUtils:isNameCorrect(name) then
      ItemBox.saveId = id
      ItemBox.saveName = name
      if not GameUtils:isOlderThan18(id) then
        if self:GetFlashObject() then
          if not self.idConfirmCallback then
            self:GetFlashObject():InvokeASCallback("_root", "showTooYoungAlert")
          else
            GameUIGlobalScreen:ShowMessageBox(1, "\233\128\154\231\159\165", GameLoader:GetGameText("LC_MENU_POM_tips_3"), nil)
          end
        end
      elseif tel and tel ~= "" and GameUtils:isTelephoneCorrect(tel) then
        ItemBox.saveTel = tel
        self:sendIdMessage(string.upper(id), name)
        self:GetFlashObject():InvokeASCallback("_root", "idConfirmSucc")
      else
        local GameTip = LuaObjectManager:GetLuaObject("GameTip")
        GameTip:Show(GameLoader:GetGameText("LC_MENU_POM_Cellphone_error"))
      end
    else
      DebugOut("isId:", GameUtils:isIDNumberCorrect(id))
      DebugOut("isName", GameUtils:isNameCorrect(name))
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameLoader:GetGameText("LC_MENU_POM_ID_information_error"))
    end
  elseif cmd == "on_erase" then
    if ItemBox.idConfirmCallback then
      ItemBox.idConfirmCallback(self.saveId, self.saveName, self.isIdComfird)
    end
    self.currentBox = nil
    self.currentItem = nil
    self.currentItemType = nil
    self.GetAppendState():EraseObject(self)
    ItemBox.bUseMax = false
    ItemBox.useMaxCheck = 0
    ItemBox.idConfirmCallback = nil
  elseif cmd == "id_confirm1_yes" then
    self:sendIdMessage(string.upper(ItemBox.saveId), ItemBox.saveName)
    self:GetFlashObject():InvokeASCallback("_root", "idConfirmSucc")
  elseif cmd == "id_confirm1_no" then
    self.isIdComfird = false
  elseif cmd == "id_confirm3_yes" then
    self.isBuy = true
  elseif cmd == "id_confirm3_no" then
  elseif cmd == "id_confirm1_moveout" then
    if self.isIdComfird and self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "IdConfirmBox2Moveout")
    end
  elseif cmd == "id_confirm3_moveout" then
    self.currentBox = nil
    self.currentItem = nil
    self.currentItemType = nil
    ItemBox.EraseFromeState()
    if self.isBuy and self.buyCallback then
      self.buyCallback(self.buyArg)
    end
    self.buyCallback = nil
    self.buyArg = ""
    self.isBuy = false
  elseif cmd == "reward_detail_moveout" then
    if 0 < #self.RewardStack then
      self:ShowRewardBox()
      return
    end
    self.currentBox = nil
    self.currentItem = nil
    self.currentItemType = nil
    self.GetAppendState():EraseObject(self)
    if self.afterRewardCallback then
      self.afterRewardCallback(self.afterRewardCallArg)
      self.afterRewardCallback = nil
      self.afterRewardCallArg = nil
    end
    self.afterRewardCallback = nil
  elseif cmd == "needUpdateItemChoice" then
    self:UpdateItemChoice(tonumber(arg))
  elseif cmd == "ItemChoicesChoosed" then
    self:updateItemIcon(tonumber(arg))
  end
end
function ItemBox:ForceHideRewardBox()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideRewardBox")
  end
  self.g_isRewardShowing = false
  self.currentBox = nil
  self.currentItem = nil
  self.currentItemType = nil
  self.GetAppendState():EraseObject(self)
  if self.afterRewardCallback then
    self.afterRewardCallback(self.afterRewardCallArg)
  end
end
function ItemBox:SetDelayShowAfterReward(callback, arg)
  self.afterRewardCallback = callback
  self.afterRewardCallArg = arg
end
function ItemBox:ShowRewardBox(reward_detail, lastTime, isFTE)
  DebugOut("ItemBox:ShowRewardBox")
  self.restRewardTime = lastTime
  if not lastTime then
    self.restRewardTime = 3000
  end
  local param = {}
  if reward_detail then
    for k, v in ipairs(reward_detail) do
      table.insert(ItemBox.RewardStack, v)
    end
  end
  if self.g_isRewardShowing then
    DebugOut("self.g_isRewardShowing")
    return
  end
  self.shouldShowRewardBox = false
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  elseif self.currentBox ~= "rewardInfo" then
    self.shouldShowRewardBox = true
    return
  end
  ItemBox.RewardStack = ItemBox:MergeDuplicateItem(ItemBox.RewardStack)
  DebugOut("curStck:")
  DebugTable(ItemBox.RewardStack)
  if #ItemBox.RewardStack > 8 then
    for k = 1, 8 do
      local v = ItemBox.RewardStack[k]
      local item = {}
      item.item_icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, nil, nil)
      item.item_num = GameHelper:GetAwardCountWithX(v.item_type, v.number, v.no)
      table.insert(param, item)
    end
    for k = 1, 8 do
      table.remove(ItemBox.RewardStack, 1)
    end
  elseif #ItemBox.RewardStack > 0 then
    for k, v in ipairs(ItemBox.RewardStack) do
      local item = {}
      local info = GameHelper:GetItemInfo(v)
      item.item_icon = info.icon_frame
      item.item_pic = info.icon_pic
      item.item_num = GameHelper:GetAwardCountWithX(v.item_type, v.number, v.no)
      table.insert(param, item)
    end
    ItemBox.RewardStack = {}
  else
    self.currentBox = nil
    self.currentItem = nil
    self.currentItemType = nil
    self.afterRewardCallback = nil
    self.afterRewardCallArg = nil
    if self:GetFlashObject() then
      self.GetAppendState():EraseObject(self)
    end
    return
  end
  DebugOut("afterStck:")
  DebugTable(ItemBox.RewardStack)
  self.currentBox = "rewardInfo"
  self.g_isRewardShowing = true
  if self:GetFlashObject() then
    local nFTE = 0
    if isFTE then
      nFTE = 1
    end
    local lang = "en"
    if GameSettingData and GameSettingData.Save_Lang then
      lang = GameSettingData.Save_Lang
      if string.find(lang, "ru") == 1 then
        lang = "ru"
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "showRewardBox", param, lastTime, nFTE, lang)
  end
end
function ItemBox:MergeDuplicateItem(items)
  local ret = {}
  for k, v in ipairs(items or {}) do
    local canInsert = true
    for k1, v1 in ipairs(ret or {}) do
      if v.item_type == v1.item_type then
        if GameHelper:IsResource(v.item_type) then
          v1.number = v1.number + v.number
          canInsert = false
        elseif v.number == v1.number then
          v1.no = v1.no + v.no
          canInsert = false
        end
        if not canInsert then
          break
        end
      end
    end
    if canInsert then
      table.insert(ret, v)
    end
  end
  return ret
end
function ItemBox:ShowUseChoiceBox(_x, _y, items, callback)
  DebugOut("ItemBox:ShowUseChoiceBox", _x, _y)
  self.ItemChoices = items
  self.ItemChoicesCallback = callback
  self.currentBox = "ItemChoices"
  local length = 0
  if items ~= nil then
    length = math.ceil(#items / 3)
  end
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():InvokeASCallback("_root", "showItemChoices", _x, _y, length)
end
function ItemBox:UpdateItemChoice(index)
  local param = {}
  local _start = (index - 1) * 3 + 1
  local _end = _start + 2
  DebugOut("ItemBox:UpdateItemChoice", _start, _end)
  for k = _start, _end do
    local v = self.ItemChoices[k]
    if v == nil then
      break
    end
    local _param = {}
    if v.number == 1 then
      _param.item_type = "money"
      _param.number = v.no
      _param.no = 1
      _param.level = 0
    else
      _param.item_type = v.item_type
      _param.number = v.number
      _param.no = v.no
      _param.level = v.level
    end
    local item = {}
    item.item_icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(_param, nil, nil)
    item.item_num = GameHelper:GetAwardCount(v.item_type, v.number, v.no)
    table.insert(param, item)
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "updateItemChoiceItems", index, param)
  end
end
function ItemBox:updateItemIcon(index)
  local item = self.ItemChoices[index]
  if item ~= nil then
    self.ItemChoicesCallback(item)
  end
end
function ItemBox:showIdComfirmBox(callback)
  ItemBox.isIdComfird = false
  ItemBox.idConfirmCallback = callback
  DebugOut("why?")
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  DebugOut("nil")
  self.currentBox = "idConfirm"
  ItemBox.saveId = ""
  ItemBox.saveName = ""
  ItemBox.saveTel = ""
  DebugOut("hehehehtp:", "LC_MENU_POM_tips_confirm", GameLoader:GetGameText("LC_MENU_POM_tips_confirm"), "LC_MENU_POM_Identity_card", GameLoader:GetGameText("LC_MENU_POM_Identity_card"), "LC_MENU_POM_Name", GameLoader:GetGameText("LC_MENU_POM_Name"), "LC_MENU_POM_ID_Confirmation", GameLoader:GetGameText("LC_MENU_POM_ID_Confirmation"))
  self:GetFlashObject():InvokeASCallback("_root", "showIdComfirmBox", "\228\186\140\228\187\163\232\186\171\228\187\189\232\175\129\229\143\183\231\160\129", "2-4\228\184\170\231\174\128\228\189\147\228\184\173\230\150\135", "\230\137\139\230\156\186\229\143\183\231\160\129")
  local confirm_txt = GameLoader:GetGameText("LC_MENU_POM_tips_confirm")
  local identitycard_txt = GameLoader:GetGameText("LC_MENU_POM_Identity_card")
  local name_txt = GameLoader:GetGameText("LC_MENU_POM_Name")
  local tip_txt = GameLoader:GetGameText("LC_MENU_POM_ID_Confirmation")
  local tel_txt = GameLoader:GetGameText("LC_MENU_POM_Cellphone_number")
  self:GetFlashObject():InvokeASCallback("_root", "setConfirmBoxText", confirm_txt, identitycard_txt, name_txt, tel_txt, tip_txt)
end
function ItemBox:showBuyConfirmBox(callback, arg, price, item_name)
  if not self.GetAppendState():IsObjectInState(self) then
    self.GetAppendState():AddObject(self)
  end
  if self:GetFlashObject() then
    DebugOut("nil")
    self.currentBox = "buyConfirm"
    self.buyCallback = callback
    self.isBuy = false
    self.buyArg = arg
    DebugOut("text", GameLoader:GetGameText("LC_MENU_POM_Purchase_Confirmation"))
    local confirmStr = string.gsub(GameLoader:GetGameText("LC_MENU_POM_Purchase_Confirmation"), "<price>", price)
    confirmStr = string.gsub(confirmStr, "<name>", item_name)
    self:GetFlashObject():InvokeASCallback("_root", "showConSumeBox", confirmStr)
  end
end
function ItemBox:sendIdMessage(id, name)
  local param = {}
  param.id = id
  param.name = name
  local function callback(msgType, content)
    DebugOut("tp")
    DebugTable(content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.save_user_id_req.Code then
      if content.code ~= 0 then
        return true
      else
        ItemBox.isIdComfird = true
        return true
      end
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.save_user_id_req.Code, param, callback, true, nil)
end
function ItemBox:getItemInfo(item, itemType)
  local item_info = ""
  local nameTxt = GameHelper:GetAwardTypeText(item.item_type, item.number)
  if item.item_type == "money" then
    local desctxt = GameLoader:GetGameText("LC_MENU_MONEY_CHAR_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "vip_exp" then
  elseif item.item_type == "wd_supply" then
  elseif item.item_type == "technique" then
    local desctxt = GameLoader:GetGameText("LC_MENU_TECHNIQUE_CHAR_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "crystal" then
    local desctxt = GameLoader:GetGameText("LC_MENU_LOOT_CRYSTAL_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "expedition" then
    local desctxt = GameLoader:GetGameText("LC_MENU_EXPEDITION_CHAR_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "credit" then
  elseif item.item_type == "art_point" then
    local desctxt = GameLoader:GetGameText("LC_MENU_ARTIFACT_RESOURCE_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "kenergy" then
    local desctxt = GameLoader:GetGameText("LC_MENU_krypton_energy_DESC")
    item_info = item_info .. nameTxt .. " \n" .. desctxt
  elseif item.item_type == "ac_supply" then
  elseif item.item_type == "silver" then
  elseif item.item_type == "item" then
    local descTxt = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. item.number)
    item_info = item_info .. nameTxt .. "\n" .. descTxt
  elseif item.item_type == "krypton" then
    item_info = item_info .. nameTxt .. "\n"
    for k, v in ipairs(item.krypton_value) do
      item_info = item_info .. "\n" .. self:getBuffName(v.buff_id) .. " "
      item_info = item_info .. self:GetBuffNum(v.buff_id, v.buff_num)
    end
  elseif item.item_type == "fleet" then
    local ability_table = GameDataAccessHelper:GetCommanderAbility(item.number, 0)
    local commander_name = GameDataAccessHelper:GetCommanderName(item.number, 0)
    local nameTxt = GameLoader:GetGameText("LC_NPC_" .. commander_name)
    local skillDesc = GameDataAccessHelper:GetSkillDesc(ability_table.SPELL_ID)
    item_info = item_info .. nameTxt .. "\n" .. skillDesc
  elseif item.item_type == "medal_item" then
    item_info = GameHelper:GetAwardTypeDesc(item.item_type, item.number)
  elseif item.item_type == "medal" then
    item_info = GameHelper:GetAwardTypeDesc(item.item_type, item.number)
  else
    item_info = "item info"
  end
  return item_info
end
function ItemBox:getBuffName(buffid)
  local buffname = ""
  if buffid < 19 or buffid > 29 then
    buffname = GameLoader:GetGameText("LC_MENU_Equip_param_" .. buffid)
  else
    buffname = GameLoader:GetGameText("LC_MENU_Equip_param_" .. buffid + 2)
  end
  return buffname
end
function ItemBox:GetBuffNum(buffid, buff_num)
  local buffnum = ""
  if buffid > 8 and buffid < 15 or buffid == 17 or buffid > 18 and buffid < 23 or buffid == 37 then
    buffnum = buff_num * 1 / 10 .. "%"
  else
    buffnum = buff_num
  end
  return buffnum
end
function ItemBox.informantServerCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.create_tipoff_req.Code then
    if content.code == 0 then
      local GameTip = LuaObjectManager:GetLuaObject("GameTip")
      GameTip:Show(GameLoader:GetGameText("LC_MENU_COMPLAINT_SUCCESS_ALERT"))
      ItemBox:HideBox(ItemBox.currentBox)
    else
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function ItemBox.OnAndroidBack()
    ItemBox:HideBox(ItemBox.currentBox)
  end
end
function ItemBox:UpdateAdjutantSkillListItem(itemId)
  if ItemBox.mCurCommander and ItemBox.mCurCommander.adjutant_spell then
    local flashObj = self:GetFlashObject()
    if flashObj then
      local adjutant = ItemBox.mCurCommander
      if 1 == itemId then
        local skillName = GameLoader:GetGameText("LC_MENU_BRIDGE_RELATIONSHIP_TITLE")
        local skillItem = ""
        for k, v in pairs(adjutant.adjutant_vessels) do
          local vessleType = "TYPE_" .. v
          local vessleName = GameLoader:GetGameText("LC_FLEET_" .. vessleType)
          skillItem = skillItem .. vessleName .. " "
        end
        flashObj:InvokeASCallback("_root", "UpdateSkillListItem", itemId, false, skillName, "", 1, skillItem)
      else
        local skillName = GameDataAccessHelper:GetAdjutantSkillNameText(ItemBox.mCurCommander.adjutant_spell[itemId - 1].spell_id)
        local skillItem = GameDataAccessHelper:GetAdjutantSkillDesText(ItemBox.mCurCommander.adjutant_spell[itemId - 1].spell_id) .. "\001"
        local unlockDesc = GameLoader:GetGameText("LC_MENU_BRIDGE_CONDITION")
        if unlockDesc then
          unlockDesc = string.format(unlockDesc, GameDataAccessHelper:GetFleetLevelStr(ItemBox.mCurCommander.adjutant_spell[itemId - 1].level))
        else
          unlockDesc = "unlock Lv "
        end
        DebugOut(unlockDesc)
        flashObj:InvokeASCallback("_root", "UpdateSkillListItem", itemId, ItemBox.mCurCommander.adjutant_spell[itemId - 1].state ~= 1, skillName, unlockDesc, 1, skillItem)
      end
    end
  end
end
function ItemBox:showRankPlayerInfo(playerInfo)
  self.currentBox = "rankPlayerInfo"
  local flashObj = self:GetFlashObject()
  self.GetAppendState():AddObject(self)
  local datatable = {}
  datatable[#datatable + 1] = playerInfo.best_socre
  datatable[#datatable + 1] = playerInfo.best_season
  datatable[#datatable + 1] = playerInfo.best_ranking
  datatable[#datatable + 1] = GameUtils:GetPlayerRankingInWdc(playerInfo.rank_img, {
    rank_img = playerInfo.rank_img
  }, ItemBox.setPopPaihangPlayerGradeIcon)
  local avatar = ""
  if playerInfo.icon == 1 then
    avatar = GameUtils:GetPlayerAvatar(playerInfo.sex, playerInfo.flevel)
  else
    avatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.icon, playerInfo.flevel)
  end
  datatable[#datatable + 1] = avatar
  datatable[#datatable + 1] = playerInfo.user_name
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_Level") .. playerInfo.level
  datatable[#datatable + 1] = playerInfo.force
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_POWER_CHAR")
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_MEMBER_CHAR")
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_CREDIT_CHAR") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.point)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_WIN") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.atk_wins)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_DEFEND_WIN") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.def_wins)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_BEST_SCORE") .. ":" .. GameHelper:GetFoatColor("FFC926", playerInfo.best_socre)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_GLC_SEASON") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.best_season)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.best_ranking)
  for i = 1, #playerInfo.fleets do
    datatable[#datatable + 1] = GameDataAccessHelper:GetCommanderColorFrame(playerInfo.fleets[i].identity, playerInfo.fleets[i].level)
    datatable[#datatable + 1] = GameDataAccessHelper:GetFleetAvatar(playerInfo.fleets[i].identity, playerInfo.fleets[i].level)
  end
  local dataString = table.concat(datatable, "\001")
  DebugOut("ItemBox:showRankPlayerInfo = ", dataString)
  flashObj:InvokeASCallback("_root", "updatePopPaihangPlayerInfo", dataString)
end
function ItemBox:showRankPlayerInfo_tlc(playerInfo)
  self.currentBox = "rankPlayerInfo_tlc"
  local flashObj = self:GetFlashObject()
  self.GetAppendState():AddObject(self)
  local datatable = {}
  datatable[#datatable + 1] = playerInfo.best_socre
  datatable[#datatable + 1] = playerInfo.best_season
  datatable[#datatable + 1] = playerInfo.best_ranking
  datatable[#datatable + 1] = GameUtils:GetPlayerRankingInWdc(playerInfo.rank_img, {
    rank_img = playerInfo.rank_img
  }, ItemBox.setPopPaihangPlayerGradeIcon_tlc)
  local avatar = ""
  if playerInfo.icon == 1 then
    avatar = GameUtils:GetPlayerAvatar(playerInfo.sex, playerInfo.flevel)
  else
    avatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.icon, playerInfo.flevel)
  end
  datatable[#datatable + 1] = avatar
  datatable[#datatable + 1] = playerInfo.user_name
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_Level") .. playerInfo.level
  datatable[#datatable + 1] = playerInfo.force
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_POWER_CHAR")
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_MEMBER_CHAR")
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_CREDIT_CHAR") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.point)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_ATTACK_WIN") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.atk_wins)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_DEFEND_WIN") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.def_wins)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_BEST_SCORE") .. ":" .. GameHelper:GetFoatColor("FFC926", playerInfo.best_socre)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_GLC_SEASON") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.best_season)
  datatable[#datatable + 1] = GameLoader:GetGameText("LC_MENU_LEAGUE_RANK_CHAR") .. ":" .. GameHelper:GetFoatColor("BDFFFF", playerInfo.best_ranking)
  for j = 1, 3 do
    for i = 1, 5 do
      if j > #playerInfo.fleets or i > #playerInfo.fleets[j].fleets then
        datatable[#datatable + 1] = ""
        datatable[#datatable + 1] = ""
      else
        local v = playerInfo.fleets[j].fleets[i]
        if v.identity ~= 1 then
          datatable[#datatable + 1] = GameDataAccessHelper:GetCommanderColorFrame(v.identity, v.level)
          datatable[#datatable + 1] = GameDataAccessHelper:GetFleetAvatar(v.identity, v.level)
        else
          local leaderFrame, leaderAvatar
          if playerInfo.icon == 0 or playerInfo.icon == 1 then
            leaderFrame = GameDataAccessHelper:GetCommanderColorFrame(v.identity, v.level)
            leaderAvatar = GameUtils:GetPlayerAvatar(playerInfo.sex, v.level)
          else
            leaderFrame = GameDataAccessHelper:GetCommanderColorFrame(playerInfo.icon, v.level)
            leaderAvatar = GameDataAccessHelper:GetFleetAvatar(playerInfo.icon, v.level)
          end
          datatable[#datatable + 1] = leaderFrame
          datatable[#datatable + 1] = leaderAvatar
        end
      end
    end
  end
  local dataString = table.concat(datatable, "\001")
  DebugOut("ItemBox:showRankPlayerInfo_tlc = ", dataString)
  flashObj:InvokeASCallback("_root", "updatePopPaihangPlayerInfo_tlc", dataString)
end
function ItemBox.setPopPaihangPlayerGradeIcon(param)
  local iconFrame = GameUtils:GetPlayerRankingInWdc(param.rank_img, nil, nil)
  ItemBox:GetFlashObject():InvokeASCallback("_root", "setPopPaihangPlayerGradeIcon", iconFrame)
  DebugOut("setPopPaihangPlayerGradeIcon(param)", param)
end
function ItemBox.setPopPaihangPlayerGradeIcon_tlc(param)
  local iconFrame = GameUtils:GetPlayerRankingInWdc(param.rank_img, nil, nil)
  ItemBox:GetFlashObject():InvokeASCallback("_root", "setPopPaihangPlayerGradeIcon_tlc", iconFrame)
  DebugOut("setPopPaihangPlayerGradeIcon_tlc(param)", param)
end
function ItemBox:ShowPayOption(tPayOption)
  ItemBox.tPayOption = tPayOption
  ItemBox.AddToState()
  local txtzfb = GameUtils:TryGetText("LC_MENU_VERSION_WAY_ALIPAY")
  local txtweixin = GameUtils:TryGetText("LC_MENU_VERSION_WAY_WECHAT")
  ItemBox:GetFlashObject():InvokeASCallback("_root", "PaySelect", txtzfb, txtweixin)
end
function ItemBox:ShowCreditconfirm1(infoText, itemIcon, itemCnt, itemIcon2, itemCnt2, creditCnt, callback)
  ItemBox.creditconfirm_callback = callback
  ItemBox.AddToState()
  local param = {}
  param.strdesc = infoText
  param.cntRes = itemCnt
  param.cntCredit = creditCnt
  param.stricon = itemIcon
  if itemIcon2 then
    param.cntRes2 = itemCnt2
    param.stricon2 = itemIcon2
  end
  ItemBox:GetFlashObject():InvokeASCallback("_root", "showLackRes", param)
end
function ItemBox:realShowMedalDetail(tinfo)
  ItemBox.AddToState()
  local param = {}
  param.strname = GameHelper:GetAwardTypeText("medal", tinfo.type)
  param.strinfo = GameHelper:GetAwardTypeDesc("medal", tinfo.type)
  local gametxt = GameUtils:TryGetText("LC_MENU_MEDAL_MEDAL_LEVEL")
  gametxt = gametxt .. "<n1>/<n2>"
  gametxt = string.gsub(gametxt, "<n1>", tostring(tinfo.level))
  gametxt = string.gsub(gametxt, "<n2>", tostring(30))
  param.strlv1 = gametxt
  gametxt = GameUtils:TryGetText("LC_MENU_MEDAL_MEDAL_RANK")
  gametxt = gametxt .. "<n1>/<n2>"
  gametxt = string.gsub(gametxt, "<n1>", tostring(tinfo.rank))
  gametxt = string.gsub(gametxt, "<n2>", tostring(6))
  param.strlv2 = gametxt
  param.strlv = GameUtils:TryGetText("LC_MENU_Level") .. tinfo.level
  param.stricon = GameHelper:GetAwardTypeIconFrameName("medal_new_icon", tinfo.type)
  param.strpic = GameHelper:GetGameItemDownloadPng("medal_new_icon", tinfo.type)
  param.nstar = tinfo.rank
  param.quality = tinfo.quality
  param.strpropname = GameUtils:TryGetText("LC_MENU_TACTICAL_CENTER_CHARACTER_CHAR", "shuxing")
  param.tp = {}
  param.mousex = GameStateManager.m_lastReleasedX
  param.mousey = GameStateManager.m_lastReleasedY
  for _, vv in pairs(tinfo.attr) do
    local t = {}
    t.nvalue = vv.attr_value
    t.aid = vv.attr_id
    table.insert(param.tp, t)
  end
  for _, vv in pairs(tinfo.attr_rank) do
    local t = {}
    t.nvalue = vv.attr_value
    t.aid = vv.attr_id
    local finded = false
    for _, vvv in pairs(param.tp) do
      if vvv.aid == t.aid then
        finded = true
        vvv.nvalue = vvv.nvalue + vv.attr_value
        break
      end
    end
    if not finded then
      table.insert(param.tp, t)
    end
  end
  for _, vv in pairs(tinfo.attr_add) do
    local t = {}
    t.nvalue = vv.attr_value * (tinfo.level - 1)
    t.aid = vv.attr_id
    local finded = false
    for _, vvv in pairs(param.tp) do
      if vvv.aid == t.aid then
        finded = true
        vvv.nvalue = vvv.nvalue + t.nvalue
        break
      end
    end
    if not finded then
      table.insert(param.tp, t)
    end
  end
  local oldtp = param.tp
  param.tp = {}
  for _, v in pairs(oldtp) do
    if v.nvalue > 0 then
      v.nvalue = GameUtils:GetAttrValue(v.aid, v.nvalue)
      v.strproerty = GameUtils:GetAttrName(v.aid)
      table.insert(param.tp, v)
    end
  end
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowResolveDetail", param)
end
function _onMedalDetailCallback(msgtype, content)
  if msgtype == NetAPIList.medal_config_info_ack.Code then
    ItemBox:realShowMedalDetail(content.medal)
    return true
  elseif msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_config_info_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function ItemBox:ShowMedalDetail(nid)
  local param = {type = nid}
  NetMessageMgr:SendMsg(NetAPIList.medal_config_info_req.Code, param, _onMedalDetailCallback, true, nil)
end
function ItemBox:ShowGameItem(v, hide_callback)
  assert(v.item_type)
  assert(v.number)
  assert(v.no)
  if v.item_type == "item" then
    v.cnt = 1
    ItemBox:showItemBox("ChoosableItem", v, v.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif v.item_type == "medal_item" then
    v.quality = GameHelper:GetMedalQuality("medal_item", v.number)
    ItemBox:showItemBox("Medal", v, v.number, 320, 480, 200, 200, "", nil, "", nil)
  elseif v.item_type == "medal" then
    ItemBox:ShowMedalDetail(v.number)
  elseif item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(v.number))
  end
  ItemBox.callback_erase_dialog = hide_callback
end
