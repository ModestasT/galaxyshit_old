local ChapterInfos_170 = GameData.chapterdata_act9.ChapterInfos_170
ChapterInfos_170[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 5,
  EntroStory = {
    9101,
    9102,
    9103
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {
    9201,
    9202,
    9203,
    9204
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {
    9301,
    9302,
    9303
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    9401,
    9402,
    9403,
    9404
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {9501, 9502},
  AFTER_FINISH = {}
}
ChapterInfos_170[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    9601,
    9602,
    9603
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {9701, 9702},
  AFTER_FINISH = {}
}
ChapterInfos_170[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    9801,
    9802,
    9803,
    9804,
    9805,
    9806
  },
  AFTER_FINISH = {}
}
ChapterInfos_170[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
ChapterInfos_170[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  AFTER_FINISH = {}
}
