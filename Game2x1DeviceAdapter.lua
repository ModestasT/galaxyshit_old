local Game2x1DeviceAdapter = LuaObjectManager:GetLuaObject("Game2x1DeviceAdapter")
function Game2x1DeviceAdapter:OnAddToGameState()
  DebugOut("Flash2x1DeviceAdapter:OnAddToGameState")
  if self:GetFlashObject() == nil then
    self:LoadFlashObject()
  end
end
function Game2x1DeviceAdapter:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function Game2x1DeviceAdapter:AddUI()
  DebugOut("Game2x1DeviceAdapter:AddUI")
  if AutoUpdate.isAndroidDevice then
    return
  end
  if not GameStateManager.GameState2x1DeviceAdapter:IsObjectInState(Game2x1DeviceAdapter) then
    DebugOut("add")
    GameStateManager.GameState2x1DeviceAdapter:AddObject(Game2x1DeviceAdapter)
  end
end
