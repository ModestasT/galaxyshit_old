local GameUISectionFinish = LuaObjectManager:GetLuaObject("GameUISectionFinish")
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local TutorialQuestManager = TutorialQuestManager
local NetAPIList = NetAPIList
local GameGlobalData = GameGlobalData
local GameStateLoading = GameStateManager.GameStateLoading
local QuestTutorialEngineer = TutorialQuestManager.QuestTutorialEngineer
local QuestTutorialQuestCenter = TutorialQuestManager.QuestTutorialQuestCenter
local QuestTutorialColonial = TutorialQuestManager.QuestTutorialColonial
local QuestTutorialWD = TutorialQuestManager.QuestTutorialWD
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialPrimeWve = TutorialQuestManager.QuestTutorialPrimeWve
local QuestTutorialStarwar = TutorialQuestManager.QuestTutorialStarwar
local QuestTutorialTax = TutorialQuestManager.QuestTutorialTax
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local QuestTutorialGetQuestReward = TutorialQuestManager.QuestTutorialGetQuestReward
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialBuildTechLab = TutorialQuestManager.QuestTutorialBuildTechLab
local QuestTutorialBuildFactory = TutorialQuestManager.QuestTutorialBuildFactory
local QuestTutorialBuildKrypton = TutorialQuestManager.QuestTutorialBuildKrypton
local QuestTutorialBuildAffairs = TutorialQuestManager.QuestTutorialBuildAffairs
local QuestTutorialUseTechLab = TutorialQuestManager.QuestTutorialUseTechLab
local QuestTutorialUseFactory = TutorialQuestManager.QuestTutorialUseFactory
local QuestTutorialUseKrypton = TutorialQuestManager.QuestTutorialUseKrypton
local QuestTutorialUseAffairs = TutorialQuestManager.QuestTutorialUseAffairs
local QuestTutorialCollect = TutorialQuestManager.QuestTutorialCollect
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local QuestTutorialSlot = TutorialQuestManager.QuestTutorialSlot
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local QuestTutorialArena = TutorialQuestManager.QuestTutorialArena
local QuestTutorialFactoryRemodel = TutorialQuestManager.QuestTutorialFactoryRemodel
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameNewMenuItem = LuaObjectManager:GetLuaObject("GameNewMenuItem")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameUIBuildingInfo = LuaObjectManager:GetLuaObject("GameUIBuildingInfo")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameStateBase = GameStateBase
local GameStateWD = GameStateManager.GameStateWD
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local QuestTutorialInfiniteCosmos = TutorialQuestManager.QuestTutorialInfiniteCosmos
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local QuestTutorialComboGachaGetHero = TutorialQuestManager.QuestTutorialComboGachaGetHero
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local QuestTutorialTacticsCenter = TutorialQuestManager.QuestTutorialTacticsCenter
local QuestTutorialTacticsRefine = TutorialQuestManager.QuestTutorialTacticsRefine
local RefineDataManager = LuaObjectManager:GetLuaObject("RefineDataManager")
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local QuestTutorialFirstGetSilvaEquip = TutorialQuestManager.QuestTutorialFirstGetSilvaEquip
local QuestTutorialFirstGetWanaArmor = TutorialQuestManager.QuestTutorialFirstGetWanaArmor
local QuestTutorialSign = TutorialQuestManager.QuestTutorialSign
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local QuestTutorialMainTask = TutorialQuestManager.QuestTutorialMainTask
local GameFleetInfoTabBar = LuaObjectManager:GetLuaObject("GameFleetInfoTabBar")
GameStateMainPlanet.hadRequestFleetDisplayInfo = false
local EActivity = {Activity_Star_Craft = 1}
local firstGainState = true
GameStateMainPlanet.FBBALL_STATE = {
  BALL_STATE_SHOW = 1,
  BALL_STATE_SNAP = 2,
  BALL_STATE_HIDE = 3
}
GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_HIDE
function GameStateMainPlanet:CheckShowFBBall()
  DebugOut("CheckShowFBBall 0000000")
  if GameStateManager:GetCurrentGameState() ~= GameStateMainPlanet then
    GameUIBarLeft:SetFBBallVisible(false)
    return
  end
  if FacebookPopUI.mFeatureSwitch and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FBBALL_SHOW] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_FBBALL_SHOW] == 1 then
    DebugOut("CheckShowFBBall 11111")
    if Facebook:IsLoginedIn() and Facebook:IsBindFacebook() then
      if not Facebook:GetCanFetchInvitableFriendList() then
        return
      end
      DebugOut("CheckShowFBBall 222222")
      if self.mFBBallState == GameStateMainPlanet.FBBALL_STATE.BALL_STATE_HIDE then
        DebugOut("CheckShowFBBall 9999999")
        if not FacebookPopUI:InvireProgressFinished() and FacebookPopUI.mBallLoopTime > 0 then
          GameUIBarLeft:ShowFBBall()
          DebugOut("CheckShowFBBall 33333333")
        end
      elseif self.mFBBallState == GameStateMainPlanet.FBBALL_STATE.BALL_STATE_SNAP then
        if not FacebookPopUI:InvireProgressFinished() then
          GameUIBarLeft:FBBallSnapToScreen()
          DebugOut("CheckShowFBBall 44444444")
        else
          DebugOut("CheckShowFBBall 555555555")
          GameUIBarLeft:HideFBBall()
        end
      end
    elseif not Facebook:IsBindFacebook() then
      if self.mFBBallState == GameStateMainPlanet.FBBALL_STATE.BALL_STATE_HIDE then
        GameUIBarLeft:ShowFBBall()
      elseif self.mFBBallState == GameStateMainPlanet.FBBALL_STATE.BALL_STATE_SNAP then
        GameUIBarLeft:FBBallSnapToScreen()
      end
    end
  end
end
GameStateMainPlanet.Cover = nil
function GameStateMainPlanet:InitGameState()
  DebugOut("GameStateMainPlanet:InitGameState")
  self:AddObject(GameObjectMainPlanet)
  self:AddObject(GameUIBarLeft)
  self:AddObject(GameUIBarRight)
end
function GameStateMainPlanet:CheckNeedPauseMainPlanet()
  if self:IsObjectInState(GameUIBuilding) or self:IsObjectInState(GameUIBuildingInfo) or self:IsObjectInState(GameUICollect) then
    GameObjectMainPlanet:PauseAnim()
  else
    GameObjectMainPlanet:ResumeAnim()
  end
end
function GameStateMainPlanet:Update(dt)
  if GameStateMainPlanet.Cover and GameStateMainPlanet.Cover:GetFlashObject() then
    if #self.m_cammandList ~= 0 then
      local cammand = self.m_cammandList[1]
      if cammand.Type == self.CAMMANDTYPE.ADD then
        self:AddObjectInternal(cammand.Object)
        table.remove(self.m_cammandList, 1)
      elseif cammand.Type == self.CAMMANDTYPE.ERASE then
        self:EraseObjectInternal(cammand.Object)
        table.remove(self.m_cammandList, 1)
      else
        GameUtils:Error("Update Cammand: invalid type")
      end
    end
    if self.m_menuObjectList then
      for i, v in ipairs(self.m_menuObjectList) do
        if v ~= GameObjectMainPlanet then
          if v.Update ~= nil then
            v:Update(dt)
          elseif v:GetFlashObject() ~= nil then
            v:GetFlashObject():Update(dt)
          end
        end
      end
    end
    if self.m_bgObjectList then
      for i, v in ipairs(self.m_bgObjectList) do
        if v ~= GameObjectMainPlanet then
          if v.Update ~= nil then
            v:Update(dt)
          elseif v:GetFlashObject() ~= nil then
            v:GetFlashObject():Update(dt)
          end
        end
      end
    end
  else
    GameStateBase.Update(self, dt)
    self:CheckNeedPauseMainPlanet()
  end
end
function GameStateMainPlanet:Render()
  if GameStateMainPlanet.Cover and GameStateMainPlanet.Cover:GetFlashObject() then
    if self.m_menuObjectList then
      local size = #self.m_menuObjectList
      for i = 1, size do
        local obj = self.m_menuObjectList[i]
        if obj ~= GameObjectMainPlanet then
          if obj.Render ~= nil then
            obj:Render()
          elseif obj:GetFlashObject() ~= nil then
            obj:GetFlashObject():Render()
          end
        end
      end
    end
    if self.m_bgObjectList then
      local size = #self.m_bgObjectList
      for i = 1, size do
        local obj = self.m_bgObjectList[i]
        if obj ~= GameObjectMainPlanet then
          if obj.Render ~= nil then
            obj:Render()
          elseif obj:GetFlashObject() ~= nil then
            obj:GetFlashObject():Render()
          end
        end
      end
    end
  else
    GameStateBase.Render(self)
  end
end
function GameStateMainPlanet:CheckShowQuestInMainPlanet()
  DebugOut("GameStateMainPlanet:CheckShowQuestInMainPlanet")
  local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
  local quest = GameGlobalData:GetData("user_quest")
  if (quest.status == GameQuestMenu.QUEST_STS_UNLOOT or GameQuestMenu:checkIsShowActivityQuest()) and GameStateManager:GetCurrentGameState():IsObjectInState(GameObjectMainPlanet) and GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBarLeft) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameQuestMenu) and not GameStateManager.GameStateGlobalState:IsObjectInState(GameQuestMenu) and not GameStateManager.GameStateGlobalState:IsObjectInState(GameUIPrestigeRankUp) and not GameUIPrestigeRankUp.mIsGotoDetail then
    DebugOut("check ok")
    GameQuestMenu:ShowQuestMenu()
  end
end
GameStateMainPlanet.WDActive = false
GameStateMainPlanet.FirstShowSign = false
function GameStateMainPlanet:CheckShowTutorial()
  local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
  if GameStateMainPlanet:IsObjectInState(GameUIBarMiddle) then
    return
  end
  TutorialQuestManager:Init()
  TutorialQuestManager:TutorialQuestsCheck()
  DebugOut("GameStateMainPlanet:CheckShowTutorial")
  if GameStateManager:GetCurrentGameState() ~= self then
    return
  end
  if not TutorialQuestManager.QuestTutorialEngineerBarMove:IsFinished() then
    TutorialQuestManager.QuestTutorialEnterStarSystem:SetFinish(false, true)
    TutorialQuestManager.QuestTutorialEquipStarSystemItem:SetFinish(false, true)
    TutorialQuestManager:Save()
  end
  if not TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() and not TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
    TutorialQuestManager.QuestTutorialEnterStarSystem:SetActive(true)
  end
  if GameUICommonDialog:HasPendingStory() then
    return
  end
  GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideAllTutorial")
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  if not TutorialQuestManager.QuestTutorialStarSystemTujian:IsActive() and not TutorialQuestManager.QuestTutorialStarSystemTujian:IsFinished() and TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() then
    TutorialQuestManager.QuestTutorialStarSystemTujian:SetActive(true)
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCommandPost")
    GameUICommonDialog:PlayStory({9820})
  end
  if self:IsObjectInState(GameUIBuilding) then
    return
  end
  local levelInfo = GameGlobalData:GetData("levelinfo")
  local engineerBuildInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
  if (GameStateMainPlanet.WDActive or not QuestTutorialWD:IsActive()) and GameUtils:IsModuleUnlock("wd") then
    DebugOut("wd active")
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialWorld")
  end
  local tts = not QuestTutorialPrimeWve:IsFinished() and GameGlobalData:GetModuleStatus("prime_wve") or not QuestTutorialStarwar:IsFinished() and GameGlobalData:GetModuleStatus("starwar")
  if GameUtils:IsModuleUnlock("centalsystem") and RefineDataManager:GetIsOpenTacticsCenter() == 1 and not QuestTutorialTacticsCenter:IsFinished() and not QuestTutorialTacticsCenter:IsActive() and not tts then
    DebugOut("QuestTutorialTacticsCenter:SetActive")
    QuestTutorialTacticsCenter:SetActive(true, false)
  end
  DebugOut("TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() ", TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished(), "TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive()", TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive(), "GameGlobalData:GetModuleStatus(\"medal\")", GameGlobalData:GetModuleStatus("medal"))
  GameStateMainPlanet:CheckTCStatus()
  if not QuestTutorialColonial:IsActive() and not QuestTutorialColonial:IsFinished() and GameUtils:IsModuleUnlock("colonial") then
    DebugOut("colonial active")
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialColonial")
  elseif not TutorialQuestManager.QuestTutorialEngineerBarMove:IsFinished() and not GameUICommonDialog:isActive() and TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and GameGlobalData:GetModuleStatus("medal") and GameUICommonDialog:IsNeedPlayStory({9816}) then
    local function callback1()
      DebugOut("move 400,0")
      local function othercallback()
        GameStateMainPlanet.isPlayed98162 = nil
        if GameObjectMainPlanet:GetFlashObject() then
          GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialEngineer")
          GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCommandPost")
        end
      end
      GameUICommonDialog:PlayStory({9817}, othercallback)
    end
    DebugOut("play 2story")
    DebugOut("move tu")
    TutorialQuestManager.QuestTutorialEnterStarSystem:SetActive(true, true)
    TutorialQuestManager:Save()
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
    TutorialQuestManager.QuestTutorialEngineerBarMove:SetFinish(true)
    if not GameStateMainPlanet.isPlayed9816 then
      GameStateMainPlanet.isPlayed9816 = true
      GameStateMainPlanet.isPlayed9817 = true
      GameStateMainPlanet.isPlayed98162 = true
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 400, -60)
      GameUICommonDialog:PlayStory({9816}, callback1)
    end
  elseif TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and TutorialQuestManager.QuestTutorialEngineerBarMove:IsFinished() and GameGlobalData:GetModuleStatus("medal") and not GameUICommonDialog:isActive() then
    if GameStateMainPlanet.isPlayed9817 and GameStateMainPlanet.isPlayed98162 then
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
    end
    if not GameStateMainPlanet.isPlayed9817 then
      GameStateMainPlanet.isPlayed9817 = true
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCommandPost")
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 0, 0)
      GameUICommonDialog:PlayStory({9817})
    end
    DebugOut("play one story ")
  elseif TutorialQuestManager.QuestTutorialEnterStarSystem:IsFinished() and GameGlobalData:GetModuleStatus("medal") and TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() then
    DebugOut("euip active")
    GameUIBarRight:MoveInRightMenu()
    GameUIBarRight:CheckQuestTutorial()
  elseif QuestTutorialTacticsCenter:IsActive() then
    local function callback()
      GameUIBarRight:MoveInRightMenu()
      GameUIBarRight:CheckQuestTutorial()
    end
    DebugOut("QuestTutorialTacticsCenter: in GameStateMainPlanet")
    GameUICommonDialog:PlayStory({1100086}, callback)
  elseif QuestTutorialCityHall:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCityHall")
    end
    if immanentversion == 2 then
      if not self.FirstShowSign then
        GameUIActivity:ShowNews("sign")
      else
        GameUICommonDialog:PlayStory({
          51101,
          51102,
          51103,
          51106,
          51107
        }, callback)
      end
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({
        51100,
        1,
        2
      }, callback)
    end
  elseif QuestTutorialArena:IsActive() then
    local function callback()
      GameUIBarRight:MoveInRightMenu()
      GameUIBarRight:CheckQuestTutorial()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51226, 51227}, callback)
    elseif immanentversion == 1 then
      callback()
    end
  elseif QuestTutorialEngineer:IsActive() then
    DebugOut("QuestTutorialEngineer:IsActive")
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 400, -60)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
      TutorialQuestManager.QuestTutorialEngineerBarMove:SetFinish(true)
      TutorialQuestManager:Save()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51112}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif not TutorialQuestManager.QuestTutorialEngineerBarMove:IsFinished() and not GameUICommonDialog:isActive() and not TutorialQuestManager.QuestTutorialEnterStarSystem:IsActive() and engineerBuildInfo and 0 < engineerBuildInfo.level and QuestTutorialEngineer:IsFinished() then
    local function callback1()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialEngineer")
    end
    TutorialQuestManager.QuestTutorialEngineerBarMove:SetFinish(true)
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
    if not GameStateMainPlanet.isPlayed9816 then
      GameStateMainPlanet.isPlayed9816 = true
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 400, -60)
      GameUICommonDialog:PlayStory({9816}, callback1)
    end
    DebugOut("just move bar")
  elseif QuestTutorialEnhance:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 400, -60)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51113}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({100016}, callback)
    end
  elseif QuestTutorialBuildStar:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialStarPortal")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({
        51145,
        51146,
        51147
      }, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif QuestTutorialQuestCenter:IsActive() then
    local function callback()
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    GameUICommonDialog:PlayStory({1, 2}, callback)
  elseif QuestTutorialSlot:IsActive() then
    local function callback()
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51211, 51212}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({100060}, callback)
    end
  elseif QuestTutorialBuildAcademy:IsActive() and not TutorialQuestManager.QuestTutorialFirstReform:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAcademy")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51414, 51415}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif QuestTutorialsFirstKillBoss:IsActive() and not TutorialQuestManager.QuestTutorialFirstReform:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAcademy")
    end
    callback()
  elseif QuestTutorialBuildAffairs:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAffairsCenter")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({
        1407,
        1408,
        1409
      }, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif QuestTutorialUseAffairs:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialAffairsCenter")
    end
    GameUICommonDialog:PlayStory({}, callback)
  elseif QuestTutorialInfiniteCosmos:IsActive() then
    local function callback()
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    DebugOut("QuestTutorialInfiniteCosmos is active!!!!")
    GameUICommonDialog:PlayStory({1100056}, callback)
  elseif QuestTutorialUseKrypton:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonCenter")
    end
    GameUICommonDialog:PlayStory({}, callback)
  elseif QuestTutorialBuildFactory:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialFactory")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({100040}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif QuestTutorialFactoryRemodel:IsActive() then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialFactory")
  elseif QuestTutorialGetQuestReward:IsActive() then
    local function callback()
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({3, 1134}, callback)
    end
  elseif QuestTutorialStarCharge:IsActive() then
    DebugOut("hello world!!!!!")
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialStarPortal")
    end
    GameUICommonDialog:PlayStory({}, callback)
  elseif QuestTutorialRecruit:IsActive() then
    DebugOut("222222hello world!!!!!")
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialStarPortal")
    end
    GameUICommonDialog:PlayStory({100029}, callback)
  elseif QuestTutorialBuildTechLab:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTechLab")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({1414}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTechLab")
    end
  elseif QuestTutorialBuildKrypton:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonCenter")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({1707}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialKryptonCenter")
    end
  elseif QuestTutorialUseTechLab:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTechLab")
    end
    GameUICommonDialog:PlayStory({65}, callback)
  elseif QuestTutorialMine:IsActive() then
    local function callback()
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({100009}, callback)
    end
  elseif QuestTutorialUseFactory:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialFactory")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({100042}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({}, callback)
    end
  elseif QuestTutorialEnhance_second:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 400, -60)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialEngineer")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51133}, callback)
    end
  elseif QuestTutorialTax:IsActive() then
    local function callback()
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCityHall")
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({}, callback)
    elseif immanentversion == 1 then
      GameUICommonDialog:PlayStory({4, 5}, callback)
    end
  elseif QuestTutorialCollect:IsActive() then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialCityHall")
  elseif QuestTutorialEnhance_third:IsActive() then
    local function callback()
      GameUIBarRight:MoveInRightMenu()
      GameUIBarRight:CheckQuestTutorial()
    end
    GameUICommonDialog:PlayStory({}, callback)
  elseif QuestTutorialComboGachaGetHero:IsActive() then
    GameUIBarRight:CheckQuestTutorial()
  elseif QuestTutorialEquip:IsActive() then
  elseif QuestTutorialPrestige:IsActive() then
  elseif QuestTutorialFirstGetSilvaEquip:IsActive() then
  elseif QuestTutorialFirstGetWanaArmor:IsActive() then
  elseif QuestTutorialSign:IsActive() then
  elseif QuestTutorialMainTask:IsActive() then
    DebugOut("CheckMainTaskTutorial:")
    GameUIBarLeft:CheckMainTaskTutorial()
  end
  DebugOut("go og ogo hello world!!!!!")
  if QuestTutorialComboGacha:IsActive() then
    local function callback()
      AddFlurryEvent("Gacha_Dialog", {}, 1)
      GameUIBarLeft:CheckNeedShowQuestTutorial()
    end
    DebugOut("QuestTutorialComboGacha:CheckShowQuestInMainPlanet")
    GameUICommonDialog:PlayStory({1100057}, callback)
  else
    GameUIBarLeft:CheckNeedShowQuestTutorial()
  end
  GameUIPrestigeRankUp:CheckNeedDisplay()
  if QuestTutorialBattleMap:IsActive() and GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    local function callback()
      GameUIBarRight:ChechNeedShowTutorialBattleMap()
    end
    if immanentversion == 2 then
      GameUICommonDialog:PlayStory({51114, 51116}, callback)
    elseif immanentversion == 1 then
      callback()
    end
  else
    GameUIBarRight:ChechNeedShowTutorialBattleMap()
  end
end
function GameStateMainPlanet:SetStoryWhenFocusGain(story)
  DebugOut("SetStoryWhenFocusGain")
  DebugTable(story)
  self.m_storyOnFocusGain = story
end
function GameStateMainPlanet:AfterStateGain()
  GameStateBase.AfterStateGain(self)
  GameUISectionFinish:CheckSectionFinishBefore()
  local function callback()
    self:CheckNewTutorial()
    self:CheckShowTutorial()
  end
  self:CheckShowFBBall()
  if GameFleetInfoTabBar.needPromptFacebookStory then
    GameFleetInfoTabBar.needPromptFacebookStory = false
    DebugOut("FacebookPopUI.mFeatureSwitch")
    DebugTable(FacebookPopUI.mFeatureSwitch)
    if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PRESTIGE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_PRESTIGE] == 1 then
      if Facebook:IsLoginedIn() then
        FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
        FacebookPopUI:GetFacebookInvitableFriendList()
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        local function facebookPromtCallback()
          FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
          FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_PRESTIGE_CREDIT)
          local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
          AddFlurryEvent("POPFacebookBind_Prestige", {}, 1)
        end
        if not Facebook:IsBindFacebook() then
          GameUICommonDialog:PlayStory({100069, 100070}, facebookPromtCallback)
        else
          GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
        end
      end
    else
      GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
    end
  else
    GameUICommonDialog:PlayStory(self.m_storyOnFocusGain, callback)
  end
  self.m_storyOnFocusGain = nil
end
function GameStateMainPlanet:CheckNewTutorial()
  if GameUtils:IsChinese() and GameSettingData.Save_Lang ~= "zh" and GameGlobalData.GlobalData.id_status ~= nil and GameGlobalData.GlobalData.id_status.isAuth ~= true and not GameUIBarLeft.hasShowConfirm then
    return
  end
  if not QuestTutorialPrimeWve:IsFinished() and GameGlobalData:GetModuleStatus("prime_wve") and GameUICommonDialog:IsNeedPlayStory({9812}) then
    local function callback1()
      local function callback2()
        QuestTutorialPrimeWve:SetFinish(true)
        if GameObjectMainPlanet:GetFlashObject() then
          GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialPrimeWve")
        end
        GameStateMainPlanet:CheckNewTutorial()
      end
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 200, 300)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialPrimeWve")
      GameUICommonDialog:PlayStory({9813}, callback2)
    end
    GameUICommonDialog:PlayStory({9812}, callback1)
  elseif not QuestTutorialStarwar:IsFinished() and GameGlobalData:GetModuleStatus("starwar") and GameUICommonDialog:IsNeedPlayStory({9814}) and QuestTutorialPrimeWve:IsFinished() then
    local function callback1()
      local function callback2()
        QuestTutorialStarwar:SetFinish(true)
        if GameObjectMainPlanet:GetFlashObject() then
          GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "HideTutorialStarwar")
        end
      end
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "autoMoveDesPos", 0, 80)
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialStarwar")
      GameUICommonDialog:PlayStory({9815}, callback2)
    end
    GameUICommonDialog:PlayStory({9814}, callback1)
  end
end
function GameStateMainPlanet:OnFocusGain(previousState)
  GameStateMainPlanet.Cover = nil
  OnFocusGainExcuted = true
  DebugOut("GameStateMainPlanet:OnFocusGain")
  if firstGainState then
    firstGainState = false
  end
  self:AddObjectInternal(GameObjectMainPlanet)
  GameObjectMainPlanet:LoadFlashObject()
  GameStateMainPlanet.preState = previousState
  local GameUIWebView = LuaObjectManager:GetLuaObject("GameUIWebView")
  GameUIWebView:CheckAndOpenWebView()
  GameObjectMainPlanet:InitFlashObject()
  GameObjectMainPlanet:SetMapOffset()
  GameObjectMainPlanet.lastWDUpdateTime = -1
  GameObjectMainPlanet:UpdateBuildingStatus()
  GameObjectMainPlanet:UpdateColonialStatus()
  GameObjectMainPlanet:UpdatePrimeWveStatus()
  GameObjectMainPlanet:UpdateStarWarStatus()
  GameObjectMainPlanet:UpdateWorldStatus()
  GameUtils:PlayMusic("GE2_Map.mp3")
  GameGlobalData:CheckAchievementDisplay()
  GameStateMainPlanet.hadRequestFleetDisplayInfo = true
  DeviceAutoConfig.StartStatisticsInfo("MainPlanetAverageFPS")
  GameStateWD:TryFetchWDList()
  local activityList = {
    EActivity.Activity_Star_Craft
  }
  GameStateMainPlanet:RequestAllActivityState(activityList)
  GameStateMainPlanet:RequestAllAchievement_forPlatformExt()
  GameStateMainPlanet.isCanRegister = true
  if ext.GetPlatform() == "Android" then
    if GameStateManager.GameStateMainPlanet.RegisterAndroidGCmID then
      GameStateMainPlanet.isCanRegister = true
    else
      GameStateMainPlanet.isCanRegister = false
    end
  end
  if GameStateMainPlanet.isCanRegister then
    self:TryToResignDevice()
  end
  GameNotice:FetchInfo()
  if FlashMemTracker then
    FlashMemTracker.DumpAllFlashMemstate()
  end
  GameObjectMainPlanet:UpdateConsortiumStateInChinese()
  TutorialAdjutantManager:CheckActiveTutorialAdjutantStoryPlay()
  TutorialAdjutantManager:CheckStartTutorialAdjutantStoryPlay()
  GameObjectMainPlanet:SetUpforOptimize()
  if not GameUIBarRight:GetFlashObject() then
    GameUIBarRight:LoadFlashObject()
  end
  if GameSettingData.EnableBuildingName == nil then
    GameSettingData.EnableBuildingName = true
  end
  if GameStateMainPlanet:IsObjectInState(GameUIBarLeft) then
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "SetChatIconType", 1, GameUIBarRight.m_isMovedIn)
  end
  GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "SetBuildingNameShow", GameSettingData.EnableBuildingName)
  GameUIBarRight:GetFlashObject():InvokeASCallback("_root", "SetStatType", true)
end
function GameStateMainPlanet:CheckTCStatus()
  if not GameObjectMainPlanet:GetFlashObject() then
    return
  end
  if GameStateStarCraft.mStarCraftLeftTime > 0 then
    if 0 < GameStateStarCraft.mStarCraftLeftTime + GameStateStarCraft.mStartCraftFetchTime - os.time() and GameUtils:IsModuleUnlock("tc") then
      GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTerritorial")
    end
  elseif GameStateStarCraft.mStarCraftLeftTime < 0 and 0 > math.abs(GameStateStarCraft.mStarCraftLeftTime) + GameStateStarCraft.mStartCraftFetchTime - os.time() and GameUtils:IsModuleUnlock("tc") then
    GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "ShowTutorialTerritorial")
  end
end
function GameStateMainPlanet:OnFocusLost(newState)
  DeviceAutoConfig.EndStatisticsInfo("MainPlanetAverageFPS")
  GameStateMainPlanet:EraseObject(GameUIBarMiddle)
  GameObjectMainPlanet:UnloadFlashObject()
  local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
  GameUIRechargePush:ClearStatus()
  GameUIBarLeft:OnFocusLost(newState)
  self:EraseObjectInternal(GameObjectMainPlanet)
end
function GameStateMainPlanet:OnMultiTouchBegin(x1, y1, x2, y2)
  DebugOut("GameStateMainPlanet:OnMultiTouchBegin:", x1, y1, x2, y2)
  if GameStateBase.OnMultiTouchBegin(self, x1, y1, x2, y2) then
    return true
  end
  self.m_multiTouchPreCenterX = (x1 + x2) / 2
  self.m_multiTouchPreCenterY = (y1 + y2) / 2
  self.m_multiTouchPreDistance = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
  return true
end
function GameStateMainPlanet:OnMultiTouchEnd(x1, y1, x2, y2)
  if GameStateBase.OnMultiTouchEnd(self, x1, y1, x2, y2) then
    return true
  end
  return true
end
function GameStateMainPlanet:OnMultiTouchMove(x1, y1, x2, y2)
  if GameUtils:GetTutorialHelp() then
    return
  end
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  if GameStateBase.OnMultiTouchMove(self, x1, y1, x2, y2) then
    return true
  end
  if not self.m_multiTouchPreDistance then
    return
  end
  local curDistance = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
  local centerX = (x1 + x2) / 2
  local centerY = (y1 + y2) / 2
  local scaleDelta = curDistance - self.m_multiTouchPreDistance
  local offsetX = centerX - self.m_multiTouchPreCenterX
  local offsetY = centerY - self.m_multiTouchPreCenterY
  self.m_multiTouchPreCenterX = centerX
  self.m_multiTouchPreCenterY = centerY
  self.m_multiTouchPreDistance = curDistance
  GameObjectMainPlanet:GetFlashObject():InvokeASCallback("_root", "MultiMove", centerX, centerY, scaleDelta, offsetX, offsetY)
  return true
end
function GameStateMainPlanet:OnTouchPressed(x, y)
  DebugOut("GameStateMainPlanet:OnTouchPressed:", x, y)
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  GameStateBase.OnTouchPressed(self, x, y)
end
function GameStateMainPlanet:OnTouchReleased(x, y)
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  GameStateBase.OnTouchReleased(self, x, y)
end
function GameStateMainPlanet:OnTouchMoved(x, y)
  if GameObjectTutorialCutscene:IsActive() then
    return
  end
  if GameUtils:GetTutorialHelp() then
    return
  end
  GameStateBase.OnTouchMoved(self, x, y)
end
GameStateMainPlanet.SendDevickToken = false
function GameStateMainPlanet:TryToResignDevice()
  if GameStateMainPlanet.SendDevickToken then
    return
  end
  local endpointType = "apns"
  if ext.GetPlatform() == "Android" then
    endpointType = "gcm"
  end
  local token = ext.GetDeviceToken()
  if string.len(token) > 0 and string.sub(token, 1, 1) == "<" then
    token = string.sub(token, 2, string.len(token))
  end
  if string.len(token) > 0 and string.sub(token, string.len(token), string.len(token)) == ">" then
    token = string.sub(token, 1, string.len(token) - 1)
  end
  if string.len(token) > 0 then
    token = string.gsub(token, " ", "")
  end
  local deviceLang = ext.GetDeviceLanguage()
  local gcmregisterId = GameStateMainPlanet.RegisterAndroidGCmID
  gcmregisterId = gcmregisterId or " "
  local requestParam = {
    endpoint_token = token,
    device_lang = deviceLang,
    endpoint_type = endpointType,
    registration_id = gcmregisterId
  }
  DebugOut("TryToResignDevice")
  DebugTable(requestParam)
  NetMessageMgr:SendMsg(NetAPIList.device_info_req.Code, requestParam, GameStateMainPlanet.resignDeviceCallback, false, nil)
  GameStateMainPlanet.SendDevickToken = true
end
function GameStateMainPlanet.resignDeviceCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.device_info_req.Code then
    return true
  end
  return false
end
function GameStateMainPlanet:RequestAllActivityState(activityIdList)
  local content = {ids = activityIdList}
  NetMessageMgr:SendMsg(NetAPIList.activity_status_req.Code, content, GameStateMainPlanet.RequestAllActivityStateCallback, false, nil)
end
function GameStateMainPlanet.RequestAllActivityStateCallback(msgtype, content)
  if msgtype == NetAPIList.activity_status_ack.Code then
    for i, v in ipairs(content.activities) do
      if v.id == EActivity.Activity_Star_Craft then
        GameStateStarCraft.mStarCraftLeftTime = v.left_seconds
        GameStateStarCraft.mStartCraftFetchTime = os.time()
        if GameStateStarCraft.mStarCraftLeftTime >= 0 then
          GameStateStarCraft.mStarCraftActive = true
        else
          GameStateStarCraft.mStarCraftActive = false
        end
        GameStateMainPlanet:CheckTCStatus()
      end
    end
    return true
  end
  return false
end
function GameStateMainPlanet:RequestAllAchievement_forPlatformExt(...)
  DebugOut("RequestAllAchievement_forPlatformExt")
  if IPlatformExt.isAchievement_support() then
    NetMessageMgr:SendMsg(NetAPIList.achievement_list_req.Code, nil, GameStateMainPlanet._RequestAllAchievement_forPlatformExtCallback, true, nil)
  end
end
function GameStateMainPlanet._RequestAllAchievement_forPlatformExtCallback(msgtype, content)
  DebugOut("_RequestAllAchievement_forPlatformExtCallback")
  if msgtype == NetAPIList.achievement_list_ack.Code then
    for _, v in ipairs(content.achievement_list) do
      if v.is_finished == 1 then
        local AchievementPlatformTable = AchievementPlatformMap[v.id] or {}
        local AchievementPlatformID = AchievementPlatformTable[IPlatformExt.getConfigValue("PlatformName")] or "fucking_id_" .. v.id
        IPlatformExt.unlock_Achievement(AchievementPlatformID)
      end
    end
    return true
  end
  return false
end
function GameStateMainPlanet.ClientVersionNtfHandler(content)
  if AutoUpdate.isSupportQuickStartGame and AutoUpdate.isQuickStartGame then
    DebugOutPutTable(content, "ClientVersionNtfHandler")
    AutoUpdate.isQuickStartGameVerOk = false
    local bundleIdentifier = ext.GetBundleIdentifier()
    if content and content.versions then
      for k, v in ipairs(content.versions) do
        if v.appid == bundleIdentifier then
          if AutoUpdate.localVersion < v.version then
            DebugOut("Quick start game need update. local version is " .. tostring(AutoUpdate.localVersion) .. " server version is " .. tostring(v.version))
          else
            DebugOut("Quick start game version check ok.")
            AutoUpdate.isQuickStartGameVerOk = true
          end
        end
      end
    end
    AutoUpdate.isQuickStartGameVerNtfRecv = true
    if not AutoUpdate.isQuickStartGameVerOk then
      DebugOut("Quick start game version check failed.")
      local text = GameLoader:GetGameText("LC_ALERT_client_version_up")
      GameUIGlobalScreen:ShowMessageBox(1, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), text, function()
        GameUtils:RestartGame(200)
      end)
    end
  end
end
function GameStateMainPlanet.CheckNeedNormalStartNtfHandler(content)
  DebugOutPutTable(content, "CheckNeedNormalStartNtfHandler")
  if not AutoUpdate.isSupportQuickStartGame or content.server_version == content.player_version or GameSettingData.isNeedNormalStartGame then
  else
    GameSettingData.isNeedNormalStartGame = true
    GameSettingData.normalStartGameCode = content.server_version
    GameUtils.SaveSettingData()
  end
end
function GameStateMainPlanet:CheckIfSendNormalStartCode()
  if AutoUpdate.isSupportQuickStartGame and not AutoUpdate.isQuickStartGame and GameSettingData.isNeedNormalStartGame then
    local req = {
      version = GameSettingData.normalStartGameCode
    }
    DebugOutPutTable(req, "CheckIfSendNormalStartCode")
    NetMessageMgr:SendMsg(NetAPIList.version_code_update_req.Code, req, GameStateMainPlanet.SendNormalStartCodeCallback, false, nil)
  end
end
function GameStateMainPlanet.SendNormalStartCodeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.version_code_update_req.Code then
    DebugOut("SendNormalStartCodeCallback.")
    DebugTable(content)
    if 0 == content.code then
      GameSettingData.isNeedNormalStartGame = false
      GameUtils.SaveSettingData()
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
