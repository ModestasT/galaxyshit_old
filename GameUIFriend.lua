local GameUIFriend = LuaObjectManager:GetLuaObject("GameUIFriend")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIPlayerDetailInfo = LuaObjectManager:GetLuaObject("GameUIPlayerDetailInfo")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
GameUIFriend.DataType = {}
GameUIFriend.DataType.FriendList = 1
GameUIFriend.DataType.SearchFriendResult = 2
GameUIFriend.DataType.NoticeList = 3
GameUIFriend.DataType.BlockList = 4
GameUIFriend.DataType.tangoFriend = 5
GameUIFriend._current_list_mode = GameUIFriend.DataType.FriendList
GameUIFriend.tangoGooglePlayUrl = "www.tango.me/install/?src=sdk-android"
local SERVER_BRAG_URL = "galaxylegend4t://brag"
local IMAGE_URL = "https://a.portal-platform.tap4fun.com/gl/url_get/dev/dev1//images/gl_tango.jpg"
local INVITATION_URL = "galaxylegend4t://invitation"
GameUIFriend.OnEraseCallBack = nil
function GameUIFriend:OnAddToGameState(state)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if self.m_isMovedIn then
    GameUtils:Warning("GameUIFriend is movedIn already")
    return
  end
  if AutoUpdate.isAndroidDevice then
    self:GetFlashObject():InvokeASCallback("_root", "setIsTangoApp", GameUtils:IsTangoAPP())
  else
    self:GetFlashObject():InvokeASCallback("_root", "setIsTangoApp", GameUtils:IsLoginTangoAccount())
  end
  self.m_isMovedIn = true
  self:GetFlashObject():InvokeASCallback("_root", "MoveInFriendDialog", GameUIFriend._current_list_mode)
end
function GameUIFriend:OnEraseFromGameState(state)
  self:ClearData()
  self:GetFlashObject():InvokeASCallback("_root", "ClearDataList")
  self:UnloadFlashObject()
  self.isOnOperation = nil
  self.tangoFriendList = nil
  GameUIFriend.InviteTangoIDList = nil
  if GameUIFriend.OnEraseCallBack then
    GameUIFriend.OnEraseCallBack()
    GameUIFriend.OnEraseCallBack = nil
  end
end
function GameUIFriend:ClearData()
  self.FriendListData = nil
  self.SearchFriendResultData = nil
  self.NoticeListData = nil
  self.BlockListData = nil
  GameUIFriend._current_list_mode = GameUIFriend.DataType.FriendList
end
function GameUIFriend:InitFriendList(friend_list_data)
  self.FriendListData = friend_list_data
  GameUIFriend:SetNormalFriend()
end
function GameUIFriend:SetNormalFriend()
  if self:GetFlashObject() then
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.FriendList, true)
    self:GetFlashObject():InvokeASCallback("_root", "InitDataList", #self.FriendListData, GameUIFriend.DataType.FriendList, GameLoader:GetGameText("LC_MENU_NO_FRIEND"))
  end
end
function GameUIFriend:SetTangoFriend()
  if not GameUtils:IsLoginTangoAccount() then
    if AutoUpdate.isAndroidDevice and GameUtils:IsTangoAPP() and GameUtils.IsNotInstallTango then
      ext.http.openURL(GameUIFriend.tangoGooglePlayUrl)
    end
    return
  end
  if not GameUIFriend.tangoFriendList then
    GameUIFriend.tangoFriendList = {}
    GameUIFriend:GetTangoFriendList()
  else
    GameUIFriend:ShowTangoFriendList()
  end
end
function GameUIFriend.friendListCallback(msgType, content)
  if not GameUIFriend:GetFlashObject() then
    GameUIFriend:LoadFlashObject()
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friends_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.friends_ack.Code then
    if GameUIFriend.blockClicked then
      GameUIFriend.BlockListData = content.ban_friends
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.BlockList, true)
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameUIFriend.BlockListData, GameUIFriend.DataType.BlockList, GameLoader:GetGameText("LC_MENU_NO_BLOCK_FRIEND"))
      GameUIFriend.blockClicked = false
    else
      GameUIFriend:InitFriendList(content.friends)
      GameUIFriend.BlockListData = content.ban_friends
    end
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIFriend) then
      GameUIFriend._current_list_mode = GameUIFriend.DataType.FriendList
      GameStateManager:GetCurrentGameState():AddObject(GameUIFriend)
    end
    return true
  end
  return false
end
function GameUIFriend:ShowAddFriendUIAndFetchFriends()
  NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.fetchFriendListCallback, true, nil)
end
function GameUIFriend.fetchFriendListCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friends_req.Code then
    assert(content.code ~= 0)
    GameUIFriend:ShowAddFriendUI()
    return true
  elseif msgType == NetAPIList.friends_ack.Code then
    GameUIFriend:InitFriendList(content.friends)
    GameUIFriend:ShowAddFriendUI()
    return true
  end
  return false
end
function GameUIFriend:ShowAddFriendUI()
  if not GameUIFriend:GetFlashObject() then
    GameUIFriend:LoadFlashObject()
  end
  GameUIFriend._current_list_mode = GameUIFriend.DataType.SearchFriendResult
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIFriend) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIFriend)
  end
end
function GameUIFriend:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateDataList", dt)
  self:GetFlashObject():Update(dt)
end
function GameUIFriend:MoveOut()
  self.m_isMovedIn = false
  self:GetFlashObject():InvokeASCallback("_root", "MoveOutFriendDialog")
end
function GameUIFriend:GetTangoFriendList()
  GameWaiting:ShowLoadingScreen()
  ext.socialNetwork.getContactsProfile(function(errStr, friendProfiles)
    GameWaiting:HideLoadingScreen()
    if errStr then
      GameUtils:ShowTips(errStr)
    end
    if friendProfiles then
      GameUtils:printByAndroid("print---------------friendProfiles-------")
      GameUtils:DebugOutTableInAndroid(friendProfiles)
      GameUIFriend.tangoFriendList = friendProfiles
      GameUIFriend:GetCurrentPlatformTangoID()
      if not GameUIFriend.InviteTangoIDList then
        GameUIFriend:ReqInvitedTangoFriendList()
      end
    end
  end)
end
function GameUIFriend:ShowTangoFriendList()
  if not GameUIFriend.tangoFriendList then
    return
  end
  GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.tangoFriend, true)
  self:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameUIFriend.tangoFriendList, GameUIFriend.DataType.tangoFriend, GameLoader:GetGameText("LC_MENU_NO_FRIEND"))
end
function GameUIFriend:ReqInvitedTangoFriendList()
  local friends_tango = {}
  for k, v in pairs(self.tangoFriendList) do
    table.insert(friends_tango, v.tangoID)
  end
  local content = {
    tangoid = GameUtils.m_myProfile.tangoID,
    friends = friends_tango
  }
  NetMessageMgr:SendMsg(NetAPIList.tango_invited_friends_req.Code, content, GameUIFriend.ReqInvitedTangoFriendListCallback, true, nil)
end
function GameUIFriend.ReqInvitedTangoFriendListCallback(msgType, content)
  GameUtils:printByAndroid("fuck----ReqInvitedTangoFriendListCallback------msgType," .. msgType)
  if content then
    GameUtils:DebugOutTableInAndroid(content)
  end
  if msgType == NetAPIList.tango_invited_friends_ack.Code then
    GameUIFriend.InviteTangoIDList = content.invited_friends
    GameUIFriend.RegisteredTangoFriends = content.registered_friends
    if not GameUIFriend.InviteTangoIDList then
      GameUIFriend.InviteTangoIDList = {}
    end
    if not GameUIFriend.RegisteredTangoFriends then
      GameUIFriend.RegisteredTangoFriends = {}
    end
    for i = #GameUIFriend.tangoFriendList, 1, -1 do
      if #GameUIFriend.InviteTangoIDList > 0 then
        for k1, v1 in pairs(GameUIFriend.InviteTangoIDList) do
          if GameUIFriend.tangoFriendList[i] and GameUIFriend.tangoFriendList[i].tangoID and v1 and tostring(GameUIFriend.tangoFriendList[i].tangoID) == tostring(v1) then
            GameUIFriend.tangoFriendList[i].Invited = true
          end
        end
      end
      if #GameUIFriend.RegisteredTangoFriends > 0 then
        for k2, v2 in pairs(GameUIFriend.RegisteredTangoFriends) do
          if GameUIFriend.tangoFriendList[i] and GameUIFriend.tangoFriendList[i].tangoID and v2 and tostring(GameUIFriend.tangoFriendList[i].tangoID) == tostring(v2) then
            table.remove(GameUIFriend.tangoFriendList, i)
          end
        end
      end
    end
    GameUIFriend:ShowTangoFriendList()
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.tango_invited_friends_req.Code or content.api == NetAPIList.tango_invite_req.Code) then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    if content.api == NetAPIList.tango_invite_req.Code then
      if GameUIFriend.tangoFriendList and #GameUIFriend.tangoFriendList > 0 then
        for h = 1, #GameUIFriend.tangoFriendList do
          for i = 1, #GameUIFriend.sendTangoInviteList do
            if GameUIFriend.tangoFriendList[h].tangoID == GameUIFriend.sendTangoInviteList[i] then
              GameUIFriend.tangoFriendList[h].Invited = true
              GameUIFriend.tangoFriendList[h].boxClicked = false
            end
          end
          GameUIFriend:updateTangoFriendItem(h, true)
        end
      end
      GameUIFriend.sendTangoInviteList = {}
    end
    return true
  end
  return false
end
function GameUIFriend:GetCurrentPlatformTangoID()
  if not GameUIFriend.tangoFriendList then
    return
  end
  for i = 1, #GameUIFriend.tangoFriendList do
    GameUIFriend.tangoFriendList[i].boxClicked = false
    GameUIFriend.tangoFriendList[i].Invited = false
  end
end
function GameUIFriend:GetCanInviteTangoList()
end
function GameUIFriend.searchFriendCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_search_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.friend_search_ack.Code then
    GameUIFriend.SearchFriendResultData = content.friends
    if #GameUIFriend.SearchFriendResultData < 1 then
      local tipText = GameLoader:GetGameText("LC_MENU_FRIEND_SEARCH_NO_RESULT")
      GameTip:Show(tipText)
    end
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameUIFriend.SearchFriendResultData, GameUIFriend.DataType.SearchFriendResult, GameLoader:GetGameText("LC_MENU_NOT_FOUND_FRIEND"))
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.SearchFriendResult, true)
    return true
  end
  return false
end
function GameUIFriend.loadNoticeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.messages_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.messages_ack.Code then
    GameUIFriend.NoticeListData = content.messages
    local noticeShownList = {}
    for i, v in ipairs(GameUIFriend.NoticeListData) do
      local isMyFriend = GameUIFriend:IsInFriendsList(v.user_id)
      local isMyBlockedFriend = GameUIFriend:IsInBlockedFriendsList(v.user_id)
      if not isMyFriend and not isMyBlockedFriend then
        table.insert(noticeShownList, v)
      end
    end
    GameUIFriend.NoticeListData = noticeShownList
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.NoticeList, true)
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameUIFriend.NoticeListData, GameUIFriend.DataType.NoticeList, GameLoader:GetGameText("LC_MENU_NO_FRIEND_MSG"))
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetNoticeCount", tostring(#GameUIFriend.NoticeListData))
    return true
  end
  return false
end
function GameUIFriend.deleteNoticeCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.message_del_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      if GameUIFriend.NoticeListData then
        table.remove(GameUIFriend.NoticeListData, GameUIFriend.waitForDeleteMsgIndex)
      end
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.NoticeList, true)
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "InitDataList", #GameUIFriend.NoticeListData, GameUIFriend.DataType.NoticeList, GameLoader:GetGameText("LC_MENU_NO_FRIEND_MSG"))
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetNoticeCount", tostring(#GameUIFriend.NoticeListData))
    end
    return true
  end
  return false
end
function GameUIFriend:SearchFriend(searchFriendName)
  if searchFriendName == nil then
    return
  elseif string.len(searchFriendName) == 0 then
    return
  elseif string.len(searchFriendName) > 256 then
    searchFriendName = string.sub(searchFriendName, 1, 256)
  end
  local friend_search_req_data = {name = searchFriendName}
  NetMessageMgr:SendMsg(NetAPIList.friend_search_req.Code, friend_search_req_data, self.searchFriendCallback, false, nil)
end
function GameUIFriend:AddFriend(friendData)
  if friendData and self.FriendListData then
    table.insert(self.FriendListData, friendData)
  end
end
function GameUIFriend:DeleteFriendNotice()
  NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameUIFriend.friendListCallback, true, nil)
end
function GameUIFriend.friendDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.code == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_ban_req.Code then
    if GameUIFriend.current_Arg then
      local ptPos = LuaUtils:string_split(GameUIFriend.current_Arg, "\001")
      local dataInfo = GameUIFriend:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
      local content = string.format(GameLoader:GetGameText("LC_MENU_ADD_BLOCK_CHAR"), GameUtils:GetUserDisplayName(dataInfo.name))
      GameTip:Show(content)
      NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameUIFriend.friendListCallback, true, nil)
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.friend_ban_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIFriend.friendBanDelCallback(msgType, content)
  if content.code == 0 then
    if GameUIFriend.clickBlockListIndex then
      GameUIFriend.blockClicked = true
      NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, GameUIFriend.friendListCallback, true, nil)
      return true
    end
  else
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIFriend.DeleteFriendCallback(msgType, content)
  if content.code == 0 and content.api == 46 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_DELETE_FRIEND_SUCCES_CHAR"))
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "operationMoveOut")
    GameUIFriend:DeleteFriendNotice()
    return true
  else
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameUIFriend.addFriendCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.add_friend_req.Code then
    assert(content.code ~= 0)
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.add_friend_ack.Code then
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(GameLoader:GetGameText("LC_MENU_TITLE_SUCCESS_INFO"), GameLoader:GetGameText("LC_MENU_ADD_FRIEND_SUCC"))
    GameUIFriend:AddFriend(content.friend)
    GameUIFriend.waitForDeleteMsgIndex = nil
    if GameUIFriend.NoticeListData then
      for i, v in ipairs(GameUIFriend.NoticeListData) do
        if v.user_id == content.friend.id then
          local msgContent = {}
          msgContent.id = v.id
          GameUIFriend.waitForDeleteMsgIndex = i
          DebugOut("msgContent = ")
          DebugTable(msgContent)
          NetMessageMgr:SendMsg(NetAPIList.message_del_req.Code, msgContent, GameUIFriend.deleteNoticeCallback, true, nil)
        end
      end
    end
    if GameUIFriend._current_list_mode == GameUIFriend.DataType.NoticeList then
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, GameUIFriend.loadNoticeCallback, true, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, GameUIFriend.loadNoticeCallback, true, netFailedCallback)
    end
    return true
  end
  return false
end
function GameUIFriend:GetDataInfo(item_index, DataType)
  local dataInfo
  if GameUIFriend.DataType.FriendList == tonumber(DataType) then
    dataInfo = self.FriendListData[item_index]
    return dataInfo
  elseif GameUIFriend.DataType.SearchFriendResult == tonumber(DataType) then
    dataInfo = self.SearchFriendResultData[item_index]
    return dataInfo
  elseif GameUIFriend.DataType.BlockList == tonumber(DataType) then
    dataInfo = self.BlockListData[item_index]
    return dataInfo
  end
  return dataInfo
end
function GameUIFriend:OnFSCommand(cmd, arg)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  GameUtils:printByAndroid("-------<<<<<<    ..GameUIFriend:OnFSCommand.. >>>>>>----cmd--" .. cmd .. ",arg= " .. arg)
  if "Close_Friend_Windows" == cmd then
    GameUIFriend:MoveOut()
  elseif "Erase_Friend_Window" == cmd then
    GameStateManager:GetCurrentGameState():EraseObject(self)
  elseif "Friend_List_Clicked" == cmd then
    GameUIFriend._current_list_mode = GameUIFriend.DataType.FriendList
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.friendListCallback, true, nil)
  elseif "Add_Friend_Clicked" == cmd then
    GameUIFriend._current_list_mode = GameUIFriend.DataType.SearchFriendResult
    GameUIFriend:GetFlashObject():InvokeASCallback("_root", "SetSelectedPanel", GameUIFriend.DataType.SearchFriendResult, true)
  elseif "Notice_Clicked" == cmd then
    GameUIFriend._current_list_mode = GameUIFriend.DataType.NoticeList
    local function netFailedCallback()
      NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, self.loadNoticeCallback, true, netFailedCallback)
    end
    NetMessageMgr:SendMsg(NetAPIList.messages_req.Code, nil, self.loadNoticeCallback, true, netFailedCallback)
  elseif "Block_List_Clicked" == cmd then
    GameUIFriend._current_list_mode = GameUIFriend.DataType.BlockList
    self.blockClicked = true
    NetMessageMgr:SendMsg(NetAPIList.friends_req.Code, null, self.friendListCallback, true, nil)
  elseif "Search_Friend_Clicked" == cmd then
    GameUIFriend:SearchFriend(arg)
  elseif "Show_Pop_Up_Menu" == cmd then
    GameUIFriend.isOnOperation = true
    local ptPos = LuaUtils:string_split(arg, "\001")
    local item_index = tonumber(ptPos[1])
    local dataInfo = self:GetDataInfo(item_index, ptPos[2])
    self.current_Arg = arg
    local name = GameUtils:GetUserDisplayName(dataInfo.name)
    self:GetFlashObject():InvokeASCallback("_root", "ShowPopupMenu", name, arg)
  elseif "NeedUpdateItemData" == cmd then
    GameUIFriend:UpdateItemDetailDate(arg)
  elseif "move_out_operation" == cmd then
    GameUIFriend.isOnOperation = nil
  elseif "Add_Friend" == cmd then
    GameUIFriend:AddFriendToPlayer(arg)
  elseif "showNormalFriend" == cmd then
    GameUIFriend.isInviteAll = false
    GameUIFriend:SetNormalFriend()
  elseif "showTangoFriend" == cmd then
    GameUIFriend.isInviteAll = false
    GameUIFriend:SetTangoFriend()
  elseif "tango_invite_choose" == cmd then
    GameUIFriend:chooseTangoFriend(tonumber(arg))
  elseif "choose_all_tangofriend" == cmd then
    GameUIFriend:updateTangoFriendALlChoose()
  elseif "invite_all_tangofriend" == cmd then
    GameUIFriend:InviteAllTangeFriend()
  elseif "tango_invite_clicled" == cmd then
    GameUIFriend:InviteTangoFriend(tonumber(arg))
  end
  if "closeQuestCenter" == cmd then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIFriend)
  end
  if "unlock_ban_friend" == cmd then
    self:UnlockBanFriend(arg)
  end
  if cmd == "operation" then
    if arg == "view" then
      self:OperationView()
    elseif arg == "whisper" then
      self:OperationWhisper()
    elseif arg == "block" then
      self:OperationBlock()
    elseif arg == "delete" then
      self:OperationDelete()
    else
      assert(false)
    end
  end
  if cmd == "animation_over_moveout" then
    self:GetFlashObject():InvokeASCallback("_root", "hidePopMenu")
  end
  if cmd == "view_player_detail" then
    self:OperationView(arg)
  end
end
function GameUIFriend:OperationView(arg)
  if self.current_Arg then
    local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
    local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
    local coordinate = LuaUtils:string_split(arg, "\001")
    GameUIPlayerDetailInfo:Show(dataInfo.id, tonumber(coordinate[1]), tonumber(coordinate[2]))
  end
end
function GameUIFriend:OperationWhisper()
  GameUIFriend:MoveOut()
  local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
  local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
  GameUIChat:LoadFlashObject()
  GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
  GameUIChat:MoveOutOperation()
  GameUIChat.last_receiver = dataInfo.name
  GameUIChat:SelectChannel("whisper")
end
function GameUIFriend:OperationBlock()
  if GameUIFriend._current_list_mode == GameUIFriend.DataType.BlockList then
    return
  end
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  local function AddtoBlacklistCallback()
    if self.current_Arg then
      local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
      local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
      local content = {
        id = dataInfo.id,
        name = dataInfo.name
      }
      NetMessageMgr:SendMsg(NetAPIList.friend_ban_req.Code, content, self.friendDetailCallback, true, nil)
    end
  end
  GameUIMessageDialog:SetYesButton(AddtoBlacklistCallback, {})
  local info_title = ""
  local info_content = GameLoader:GetGameText("LC_MENU_BLOCK_CONFIRM_ALERT")
  GameUIMessageDialog:Display(info_title, info_content)
end
function GameUIFriend:OperationDelete()
  if self._current_list_mode ~= GameUIFriend.DataType.FriendList then
    return
  end
  local ptPos = LuaUtils:string_split(self.current_Arg, "\001")
  local dataInfo = self:GetDataInfo(tonumber(ptPos[1]), tonumber(ptPos[2]))
  local del_friend_req_content = {
    id = dataInfo.id
  }
  NetMessageMgr:SendMsg(NetAPIList.del_friend_req.Code, del_friend_req_content, self.DeleteFriendCallback, false)
end
function GameUIFriend:UnlockBanFriend(arg)
  self.clickBlockListIndex = tonumber(arg)
  local friend_ban_del_req = {
    id = self.BlockListData[tonumber(arg)].id
  }
  NetMessageMgr:SendMsg(NetAPIList.friend_ban_del_req.Code, friend_ban_del_req, self.friendBanDelCallback, false)
end
function GameUIFriend:AddFriendToPlayer(arg)
  local ptPos = LuaUtils:string_split(arg, "\001")
  local data_index = tonumber(ptPos[1])
  local data_index = tonumber(ptPos[1])
  local dataInfo, add_friend_req_content
  if GameUIFriend.DataType.SearchFriendResult == tonumber(ptPos[2]) then
    dataInfo = self.SearchFriendResultData[data_index]
    add_friend_req_content = {
      id = dataInfo.id,
      name = dataInfo.name
    }
  else
    dataInfo = self.NoticeListData[tonumber(data_index)]
    add_friend_req_content = {
      id = dataInfo.user_id,
      name = dataInfo.user_name
    }
    self.addMessageId = dataInfo.id
    self.add_frend_id = dataInfo.user_id
  end
  NetMessageMgr:SendMsg(NetAPIList.add_friend_req.Code, add_friend_req_content, self.addFriendCallback, true, nil)
end
function GameUIFriend:UpdateItemDetailDate(arg)
  local ptPos = LuaUtils:string_split(arg, "\001")
  local data_index = tonumber(ptPos[1])
  local dataInfo
  GameUtils:printByAndroid(" UpdateItemDetailDate ---data_index-> " .. data_index .. ",tonumber(ptPos[2])=" .. tonumber(ptPos[2]))
  if GameUIFriend.DataType.FriendList == tonumber(ptPos[2]) then
    dataInfo = self.FriendListData[data_index]
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateFriendItemData", data_index, avatar, name, GameLoader:GetGameText("LC_MENU_Level") .. dataInfo.level, dataInfo.online, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameUIFriend.DataType.SearchFriendResult == tonumber(ptPos[2]) then
    dataInfo = self.SearchFriendResultData[data_index]
    local addText = GameLoader:GetGameText("LC_MENU_ADD")
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateItemData", data_index, avatar, name, dataInfo.level, dataInfo.online, addText, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameUIFriend.DataType.NoticeList == tonumber(ptPos[2]) then
    dataInfo = self.NoticeListData[data_index]
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.user_name)
      local text = string.format(GameLoader:GetGameText("LC_MENU_ADD_AS_FRIEND"), name)
      local isMyFriend = self:IsInFriendsList(dataInfo.user_id)
      local addText = GameLoader:GetGameText("LC_MENU_ADD")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateNoticeItemData", data_index, text, tonumber(ptPos[2]), isMyFriend, addText)
    end
  elseif GameUIFriend.DataType.BlockList == tonumber(ptPos[2]) then
    dataInfo = self.BlockListData[data_index]
    local cancelText = GameLoader:GetGameText("LC_MENU_BUTTON_CAPTION_CANCEL")
    if dataInfo then
      local name = GameUtils:GetUserDisplayName(dataInfo.name)
      local offlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_OFFLINE_INFO")
      local onlineText = GameLoader:GetGameText("LC_MENU_ALLIANCE_ONLINE_INFO")
      local avatar = ""
      if dataInfo.icon == 0 or dataInfo.icon == 1 then
        avatar = GameUtils:GetPlayerAvatarWithSex(dataInfo.sex, dataInfo.main_fleet_level)
      else
        avatar = GameDataAccessHelper:GetFleetAvatar(dataInfo.icon, dataInfo.main_fleet_level)
      end
      DebugTable(dataInfo)
      DebugOut("FriendList:", avatar)
      self:GetFlashObject():InvokeASCallback("_root", "UpdateItemData", data_index, avatar, name, dataInfo.level, dataInfo.online, cancelText, tonumber(ptPos[2]), onlineText, offlineText)
    end
  elseif GameUIFriend.DataType.tangoFriend == tonumber(ptPos[2]) then
    GameUIFriend:updateTangoFriendItem(data_index)
  end
end
function GameUIFriend:updateTangoFriendItem(data_index, isNotUpPic)
  dataInfo = self.tangoFriendList[data_index]
  local ReplaceTextureName = "tango_avatar_" .. tostring(data_index % 10)
  if dataInfo then
    do
      local name = GameUtils:GetUserDisplayName(dataInfo.fullName)
      local url = dataInfo.profileImageUrl
      if string.len(url) < 1 then
        url = "http://sdk.tango.me/assets/t/tango/sdk/profile_placeholder_2x.png"
      end
      local picName = "tango_pic_" .. dataInfo.tangoID .. ".png"
      if not isNotUpPic then
        if GameSetting:IsDownloadFileExsit("data2/" .. picName) then
          ext.UpdateAutoUpdateFile("data2/" .. picName, 0, 0, 0)
          if GameUIFriend:GetFlashObject() then
            GameUIFriend:GetFlashObject():ReplaceTexture(ReplaceTextureName, picName)
          end
        else
          ext.http.requestDownload({
            url,
            method = "GET",
            localpath = "data2/" .. picName,
            callback = function(statusCode, content, errstr)
              if errstr ~= nil or statusCode ~= 200 then
                GameUtils:printByAndroid("get file failed")
                return
              end
              ext.ScaleImageFile("data2/" .. picName, 80, 80)
              ext.UpdateAutoUpdateFile(content, 0, 0, 0)
              if GameUIFriend:GetFlashObject() then
                GameUIFriend:GetFlashObject():ReplaceTexture(ReplaceTextureName, picName)
              end
              GameSetting:SaveDownloadFileName("data2/" .. picName)
            end
          })
        end
      end
      local inviteText = GameLoader:GetGameText("LC_MENU_INVITE")
      self:GetFlashObject():InvokeASCallback("_root", "UpdateTangoItemData", data_index, name, dataInfo.boxClicked, GameUIFriend.DataType.tangoFriend, inviteText, GameUIFriend.tangoFriendList[data_index].Invited)
    end
  end
end
function GameUIFriend:chooseTangoFriend(data_index)
  local index = tonumber(data_index)
  if not GameUIFriend.tangoFriendList[index].Invited then
    GameUIFriend.tangoFriendList[index].boxClicked = not GameUIFriend.tangoFriendList[index].boxClicked
  else
    GameUIFriend.tangoFriendList[index].boxClicked = false
  end
  GameUIFriend:updateTangoFriendItem(index)
end
function GameUIFriend:updateTangoFriendALlChoose()
  if #GameUIFriend.tangoFriendList > 0 then
    GameUIFriend.isInviteAll = not GameUIFriend.isInviteAll
    for i = 1, #GameUIFriend.tangoFriendList do
      if not GameUIFriend.tangoFriendList[i].Invited then
        GameUIFriend.tangoFriendList[i].boxClicked = GameUIFriend.isInviteAll
      else
        GameUIFriend.tangoFriendList[i].boxClicked = false
      end
      GameUIFriend:updateTangoFriendItem(i, true)
    end
  end
end
function GameUIFriend:InviteAllTangeFriend()
  GameUIFriend:GetChooseTangoFriend()
  GameUIFriend:SendInviteTangoFriend()
end
function GameUIFriend:InviteTangoFriend(data_index)
  GameUIFriend.sendTangoInviteList = {}
  table.insert(GameUIFriend.sendTangoInviteList, GameUIFriend.tangoFriendList[data_index].tangoID)
  GameUIFriend:SendInviteTangoFriend()
end
function GameUIFriend:GetChooseTangoFriend()
  GameUIFriend.sendTangoInviteList = {}
  for i = 1, #GameUIFriend.tangoFriendList do
    if GameUIFriend.tangoFriendList[i].boxClicked and not GameUIFriend.tangoFriendList[i].Invited then
      table.insert(GameUIFriend.sendTangoInviteList, self.tangoFriendList[i].tangoID)
    end
  end
  return GameUIFriend.sendTangoInviteList
end
function GameUIFriend:SendInviteTangoFriend()
  if GameUtils.m_myProfile and GameUIFriend.sendTangoInviteList and #GameUIFriend.sendTangoInviteList > 0 then
    local content = {
      tangoid = GameUtils.m_myProfile.tangoID,
      friends = GameUIFriend.sendTangoInviteList
    }
    NetMessageMgr:SendMsg(NetAPIList.tango_invite_req.Code, content, GameUIFriend.ReqInvitedTangoFriendListCallback, true, nil)
    local subText = "Tap to play!"
    local serverName = GameUtils:GetLoginInfo().ServerInfo.name or " "
    local text = string.format(GameLoader:GetGameText("LC_MENU_ANGO_INVITE_TEXT"), serverName)
    local succCallback = function()
    end
    self:TangoSendAction(GameUIFriend.sendTangoInviteList, text, IMAGE_URL, INVITATION_URL, subText, succCallback)
  end
end
function GameUIFriend:TangoSendAction(t, text, imageUrl, linkUrl, subText, succCallback, failedCallback)
  if #t < 1 then
    return
  end
  ext.socialNetwork.sendActionMessage(t, text, imageUrl, linkUrl, subText, function(errStr)
    if errStr then
      if failedCallback then
        failedCallback()
      end
      GameUtils:ShowTips(errStr)
    else
      succCallback()
    end
  end)
end
function GameUIFriend:IsInFriendsList(dataInfo_id)
  local isMyFriend = false
  if self.FriendListData then
    for k, v in pairs(self.FriendListData) do
      if v.id == dataInfo_id then
        isMyFriend = true
      end
    end
  end
  return isMyFriend
end
function GameUIFriend:IsInBlockedFriendsList(dataInfo_id)
  local isMyFriend = false
  if self.BlockListData then
    for k, v in pairs(self.BlockListData) do
      if v.id == dataInfo_id then
        isMyFriend = true
      end
    end
  end
  return isMyFriend
end
if AutoUpdate.isAndroidDevice then
  function GameUIFriend.OnAndroidBack()
    if GameUIFriend.isOnOperation then
      GameUIFriend:GetFlashObject():InvokeASCallback("_root", "operationMoveOut")
    else
      GameUIFriend:MoveOut()
    end
  end
end
