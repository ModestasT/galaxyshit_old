local GameUIStarSystemFuben = LuaObjectManager:GetLuaObject("GameUIStarSystemFuben")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateStarSystem = GameStateManager.GameStateStarSystem
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameObjectBattleReplay = LuaObjectManager:GetLuaObject("GameObjectBattleReplay")
local GameObjectFormationBackground = LuaObjectManager:GetLuaObject("GameObjectFormationBackground")
local GameStateFormation = GameStateManager.GameStateFormation
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameObjectBattleBG = LuaObjectManager:GetLuaObject("GameObjectBattleBG")
local GameUIStarSystemHandbook = LuaObjectManager:GetLuaObject("GameUIStarSystemHandbook")
GameUIStarSystemFuben.afterShowCallback = nil
GameUIStarSystemFuben.quitCallback = nil
GameUIStarSystemFuben.battle_report = {}
function GameUIStarSystemFuben:InitFlashObject()
end
function GameUIStarSystemFuben:OnFSCommand(cmd, arg)
  if GameUtils:OnFSCommand(cmd, arg, self) then
    return
  end
  if cmd == "onBtn_tujian" then
    GameUIStarSystemFuben:onSelect(cmd)
  elseif cmd == "fb_move_out" then
    GameUIStarSystemFuben:Hide(true)
    if GameUIStarSystemHandbook.gotoPrev then
      GameUIStarSystemHandbook.gotoPrev = false
      GameUIStarSystemHandbook:ComeBack()
      return
    end
    if not GameStateStarSystem:IsObjectInState(GameUIStarSystemPort) then
      GameStateStarSystem:AddObject(GameUIStarSystemPort)
      GameStateStarSystem:ForceCompleteCammandList()
    end
    GameUIStarSystemPort:Show("onBtn_fuben")
  elseif cmd == "onClickChallenge" then
    self:onClickChallenge()
  elseif cmd == "onClickRefresh" then
    self:onClickRefresh()
  elseif cmd == "onRewardIcons" then
    local tid = LuaUtils:deserializeTable(arg)
    ItemBox:ShowGameItem(tid)
  elseif cmd == "onBtn_fubenHelp" then
    local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_MEDAL_BOSS"))
  end
end
local function netRefreshCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.reset_medal_boss_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  elseif msgtype == NetAPIList.reset_medal_boss_ack.Code then
    GameUIStarSystemFuben.boss_ack.boss_info = content.boss_info
    GameUIStarSystemFuben:SetDetail()
    return true
  end
  return false
end
function GameUIStarSystemFuben:onClickRefresh()
  local function dorefresh()
    local req = {
      act_id = GameUIStarSystemFuben.boss_ack.act_id
    }
    NetMessageMgr:SendMsg(NetAPIList.reset_medal_boss_req.Code, req, netRefreshCallback, true, nil)
  end
  local bossinfo = GameUIStarSystemFuben.boss_ack.boss_info
  local tparam = {}
  if bossinfo.free_times <= 0 and bossinfo.reset_price.item_type == "credit" then
    local txt = GameUtils:TryGetText("LC_MENU_MEDAL_BOSS_PAY_WARNING", " <number> credit refresh?")
    txt = string.gsub(txt, "<number>", bossinfo.reset_price.number)
    GameUtils:CreditCostConfirm(txt, dorefresh, false, nil)
  else
    dorefresh()
  end
end
local _etMonsterHeadIconFromMatrix = function(headicon)
  if GameDataAccessHelper:IsHeadResNeedDownload(headicon) then
    if GameDataAccessHelper:CheckFleetHasAvataImage(headicon) then
      return headicon
    else
      return "head9001"
    end
  end
  return headicon
end
function GameUIStarSystemFuben:doClickChallenge()
  GameStateFormation:SetBattleID(-1, -1)
  local bossinfo = GameUIStarSystemFuben.boss_ack.boss_info
  local thematrix = bossinfo.monster_matrix
  GameStateFormation.enemyMatrixData = thematrix
  GameStateFormation.isNeedRequestMatrixInfo = false
  local config = GameData.medal_boss.Step[bossinfo.boss_id]
  assert(config, "bad config " .. bossinfo.boss_id)
  local monsterName = GameLoader:GetGameText("LC_NPC_NPC_" .. config.Monster_Name)
  local monsterAvatar = _etMonsterHeadIconFromMatrix(config.HEAD)
  GameStateFormation:SetEnemyInfo(monsterName, monsterAvatar, bossinfo.force)
  GameStateManager:SetCurrentGameState(GameStateFormation)
  GameStateStarSystem.leaveStateReson = "fuben"
end
function GameUIStarSystemFuben:onClickChallenge()
  local bossinfo = GameUIStarSystemFuben.boss_ack.boss_info
  if bossinfo.residue_degree >= bossinfo.max_degree then
    GameUIGlobalScreen:ShowAlert("error", 50028, nil)
    return
  end
  if bossinfo.can_notify then
    local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
    local function callback()
      GameUIStarSystemFuben:doClickChallenge()
    end
    GameUIMessageDialog:SetYesButton(callback, nil)
    local cancelCallback = function()
    end
    GameUIMessageDialog:SetNoButton(cancelCallback, nil)
    local infoTitle = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_CONFIRM", "force not")
    local info = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_WARNING", "force not enough")
    GameUIMessageDialog:Display(infoTitle, info)
  else
    GameUIStarSystemFuben:doClickChallenge()
  end
end
local function _onFightOver()
  local function callback()
    local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
    GameUIBattleResult:LoadFlashObject()
    GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
    GameUIBattleResult:SetFightReport(GameUIStarSystemFuben.battle_report, nil, nil, false)
    function GameUIBattleResult.closeCallBack()
      GameUIStarSystemFuben:Show()
    end
    if GameUIStarSystemFuben.net_fight_info.result == true then
      local tparam = {}
      tparam.allrewards = {}
      for k, v in pairs(GameUIStarSystemFuben.net_fight_info.awards) do
        local t = {}
        local info = GameHelper:GetItemInfo(v)
        t.strname = info.name
        t.strframe = info.icon_frame
        t.id = LuaUtils:serializeTable(v)
        if not GameHelper:IsResource(v.item_type) then
          t.strpic = info.icon_pic
        end
        t.quality = info.quality
        t.isext = info.isExt
        t.cnt = info.cnt
        table.insert(tparam.allrewards, t)
      end
      GameUIBattleResult:ShowStarSystemWin(tparam)
    else
      GameUIBattleResult:RandomizeImprovement()
      GameUIBattleResult:AnimationMoveIn("challenge_lose", false)
    end
  end
  GameStateStarSystem.cb_onFocusGain = callback
  GameStateManager:SetCurrentGameState(GameStateManager.GameStateStarSystem)
end
function GameUIStarSystemFuben:StartFight()
  local lastGameState = GameStateManager:GetCurrentGameState()
  GameUIStarSystemFuben.battle_report = GameUIStarSystemFuben.net_fight_info.report
  local curStat = GameStateManager:GetCurrentGameState()
  GameUIStarSystemFuben.battle_report.rounds = curStat.fightRoundData
  GameStateBattlePlay.curBattleType = "medal_boss"
  GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, GameUIStarSystemFuben.battle_report)
  GameStateBattlePlay:RegisterOverCallback(_onFightOver)
  local t = {
    "blue",
    "cyan",
    "red",
    "yellow",
    "ice"
  }
  local idx = os.time() % #t + 1
  GameObjectBattleBG.fixBG = t[idx]
  if GameObjectBattleBG.fixBG == nil then
    GameObjectBattleBG.fixBG = "blue"
  end
  local bossinfo = GameUIStarSystemFuben.boss_ack.boss_info
  local config = GameData.medal_boss.Step[bossinfo.boss_id]
  assert(config, "bad config " .. bossinfo.boss_id)
  local monsterName = GameLoader:GetGameText("LC_NPC_NPC_" .. config.Monster_Name)
  local monsterAvatar = _etMonsterHeadIconFromMatrix(config.HEAD)
  GameObjectBattleReplay:SetEnemyInfo(monsterName, monsterAvatar, bossinfo.force)
  GameStateManager:SetCurrentGameState(GameStateBattlePlay)
end
function GameUIStarSystemFuben:RequestFight()
  local function RequestFightCallback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.challenge_medal_boss_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgtype == NetAPIList.challenge_medal_boss_ack.Code then
      GameUIStarSystemFuben.net_fight_info = content
      GameUIStarSystemFuben:StartFight()
      GameUIStarSystemFuben.boss_ack.boss_info = content.boss_info
      GameUIStarSystemFuben:SetDetail()
      return true
    end
    return false
  end
  local req = {
    act_id = GameUIStarSystemFuben.boss_ack.act_id
  }
  NetMessageMgr:SendMsg(NetAPIList.challenge_medal_boss_req.Code, req, RequestFightCallback, true, nil)
end
function GameUIStarSystemFuben:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "ShowFuben")
  if GameUIStarSystemFuben.afterShowCallback then
    GameUIStarSystemFuben.afterShowCallback()
    GameUIStarSystemFuben.afterShowCallback = nil
  end
  self:RefreshResource()
  do
    local tparam = {}
    table.insert(tparam, {
      path = "_root.FB.difficulty.text_difficulty",
      txt = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_DIFFICULTY")
    })
    table.insert(tparam, {
      path = "_root.FB.FB_will.text_desc_get_resources",
      txt = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_WIN_AWARDS")
    })
    table.insert(tparam, {
      path = "_root.FB.award.btn_challenge.mcgray.txt",
      txt = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_CHALLENGE")
    })
    table.insert(tparam, {
      path = "_root.FB.award.btn_challenge.mcblue.txt",
      txt = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_CHALLENGE")
    })
    flash_obj:InvokeASCallback("_root", "SetText", tparam)
  end
  GameStateStarSystem.boss_ack = nil
  local function netcallback(msgtype, content)
    if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_medal_boss_req.Code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      return true
    elseif msgtype == NetAPIList.enter_medal_boss_ack.Code then
      GameUIStarSystemFuben.boss_ack = content
      GameUIStarSystemFuben:SetDetail()
      return true
    end
    return false
  end
  NetMessageMgr:SendMsg(NetAPIList.enter_medal_boss_req.Code, nil, netcallback, true, nil)
end
function GameUIStarSystemFuben:SetDetail()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local bossinfo = GameUIStarSystemFuben.boss_ack.boss_info
  local tparam = {}
  tparam.nfree = bossinfo.free_times
  local info = GameHelper:GetItemInfo(bossinfo.reset_price)
  tparam.fresh_frame = info.icon_frame
  if tparam.fresh_frame == "item" then
    tparam.fresh_pic = info.icon_pic
  end
  tparam.fresh_cost = info.cnt
  if tparam.nfree > 0 then
    local lan = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_REFRESH_FREE", "have free <number>")
    tparam.fresh_desc = string.gsub(lan, "<number>", tparam.nfree)
  else
    tparam.fresh_desc = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_REFRESH", "refresh")
  end
  tparam.ndiff = bossinfo.difficuity
  tparam.fleet_frame = GameDataAccessHelper:GetCommanderShipByDownloadState(bossinfo.ship)
  tparam.allrewards = {}
  for k, v in pairs(GameUIStarSystemFuben.boss_ack.boss_info.award_list) do
    local t = {}
    local info = GameHelper:GetItemInfo(v)
    t.strname = info.name
    t.strpic = info.icon_pic
    t.id = LuaUtils:serializeTable(v)
    t.stricon = info.icon_frame
    t.cnt = info.cnt
    if v.item_type == "medal" then
      t.quality = GameHelper:GetMedalQuality("medal", v.number)
    elseif v.item_type == "medal_item" then
      t.quality = GameHelper:GetMedalQuality("medal_item", v.number)
    elseif v.item_type then
      t.isext = true
    end
    table.insert(tparam.allrewards, t)
  end
  flash_obj:InvokeASCallback("_root", "SetDetail", tparam)
  local txtparam = {}
  local txtchallenge = GameUtils:TryGetText("LC_MENU_MADAL_BOSS_CHALLENGE")
  txtchallenge = string.format("%s %d/%d", txtchallenge, bossinfo.max_degree - bossinfo.residue_degree, bossinfo.max_degree)
  table.insert(txtparam, {
    path = "_root.FB.award.btn_challenge.mcgray.txt",
    txt = txtchallenge
  })
  table.insert(txtparam, {
    path = "_root.FB.award.btn_challenge.mcblue.txt",
    txt = txtchallenge
  })
  flash_obj:InvokeASCallback("_root", "SetText", txtparam)
end
function GameUIStarSystemFuben:OnEraseFromGameState(game_state)
  self:UnloadFlashObject()
end
function GameUIStarSystemFuben:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "Update", dt)
end
function GameUIStarSystemFuben:Show(afterShowCallback, quitCallback)
  if not GameStateStarSystem:IsObjectInState(self) then
    GameStateStarSystem:AddObject(GameUIStarSystemFuben)
    GameUIStarSystemFuben.afterShowCallback = afterShowCallback
    if quitCallback then
      GameUIStarSystemFuben.quitCallback = quitCallback
    end
  elseif afterShowCallback then
    afterShowCallback()
  end
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
end
function GameUIStarSystemFuben:Hide(fromLua)
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
  if GameUIStarSystemFuben.quitCallback then
    if not fromLua then
      local flash_obj = self:GetFlashObject()
      flash_obj:InvokeASCallback("_root", "HideFuben")
    end
    GameStateStarSystem:EraseObject(GameUIStarSystemFuben)
    GameUIStarSystemFuben.quitCallback()
    GameUIStarSystemFuben.quitCallback = nil
    GameObjectBattleBG.fixBG = nil
    return
  end
  if not fromLua then
    local flash_obj = self:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "HideFuben")
  end
  GameStateStarSystem:EraseObject(GameUIStarSystemFuben)
  GameObjectBattleBG.fixBG = nil
end
function GameUIStarSystemFuben.RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local righttype = "medal_resource"
  local rightnum = resource[righttype] or 0
  local flash_obj = GameUIStarSystemFuben:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "SetRes", rightnum)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIStarSystemFuben.OnAndroidBack()
    GameUIStarSystemFuben:Hide(false)
  end
end
