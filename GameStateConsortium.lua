local GameUIConsortium = LuaObjectManager:GetLuaObject("GameUIConsortium")
local GameStateConsortium = GameStateManager.GameStateConsortium
function GameStateConsortium:InitGameState()
end
function GameStateConsortium:OnFocusGain(previousState)
  DebugOut("GameStateConsortium:OnFocusGain", previousState)
  self.m_preState = previousState
  self:AddObject(GameUIConsortium)
end
function GameStateConsortium:OnFocusLost(newState)
  self.m_preState = nil
  self:EraseObject(GameUIConsortium)
end
function GameStateConsortium:GetPreState()
  return self.m_preState
end
