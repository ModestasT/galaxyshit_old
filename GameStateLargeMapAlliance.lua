local GameStateLargeMapAlliance = GameStateManager.GameStateLargeMapAlliance
local GameUILargeMapAlliance = LuaObjectManager:GetLuaObject("GameUILargeMapAlliance")
function GameStateLargeMapAlliance:InitGameState()
  self.m_preState = nil
end
function GameStateLargeMapAlliance:OnFocusGain(previousState)
  self.m_preState = previousState
  self:AddObject(GameUILargeMapAlliance)
end
function GameStateLargeMapAlliance:OnFocusLost(newState)
  DebugOutBattlePlay("GameStateLargeMapAlliance:OnFocusLost")
  self:EraseObject(GameUILargeMapAlliance)
  self.m_preState = nil
end
