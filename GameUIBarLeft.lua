local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIBarRight = LuaObjectManager:GetLuaObject("GameUIBarRight")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
local GameUIOfflineReward = LuaObjectManager:GetLuaObject("GameUIOfflineReward")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUISection = LuaObjectManager:GetLuaObject("GameUISection")
local GameGlobalData = GameGlobalData
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameQuestMenu = LuaObjectManager:GetLuaObject("GameQuestMenu")
local GameUIArena = LuaObjectManager:GetLuaObject("GameUIArena")
local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameUIFestival = LuaObjectManager:GetLuaObject("GameUICommonEvent")
local GameObjectWelcome = LuaObjectManager:GetLuaObject("GameObjectWelcome")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameObjectTutorialCutscene = LuaObjectManager:GetLuaObject("GameObjectTutorialCutscene")
local GameNews = LuaObjectManager:GetLuaObject("GameNews")
local QuestTutorialQuestCenter = TutorialQuestManager.QuestTutorialQuestCenter
local QuestTutorialGetQuestReward = TutorialQuestManager.QuestTutorialGetQuestReward
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialsFirstKillBoss = TutorialQuestManager.QuestTutorialsFirstKillBoss
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local QuestTutorialMine = TutorialQuestManager.QuestTutorialMine
local QuestTutorialSlot = TutorialQuestManager.QuestTutorialSlot
local QuestTutorialInfiniteCosmos = TutorialQuestManager.QuestTutorialInfiniteCosmos
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local QuestTutorialNewFunctonAC = TutorialQuestManager.QuestTutorialNewFunctonAC
local QuestTutorialNewFunctonWorldboss = TutorialQuestManager.QuestTutorialNewFunctonWorldboss
local QuestTutorialComboGacha = TutorialQuestManager.QuestTutorialComboGacha
local QuestTutorialEquipmentEvolution = TutorialQuestManager.QuestTutorialEquipmentEvolution
local m_chatMsgQueue = {}
local m_initEnter = true
local GameUILab = LuaObjectManager:GetLuaObject("GameUILab")
local GameObjectBattleMap = LuaObjectManager:GetLuaObject("GameObjectBattleMap")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameUIActivityNew = LuaObjectManager:GetLuaObject("GameUIActivityNew")
local QuestTutorialRechargePower = TutorialQuestManager.QuestTutorialRechargePower
local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
local QuestTutorialMakeUserLevel10 = TutorialQuestManager.QuestTutorialMakeUserLevel10
local QuestTutorialBuildStar = TutorialQuestManager.QuestTutorialBuildStar
local GameStateWD = GameStateManager.GameStateWD
local GameStateStarCraft = GameStateManager.GameStateStarCraft
local GameObjectAwardCenter = LuaObjectManager:GetLuaObject("GameObjectAwardCenter")
local GameStateLab = GameStateManager.GameStateLab
local QuestTutorialMainTask = TutorialQuestManager.QuestTutorialMainTask
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local GameUIBarMiddle = LuaObjectManager:GetLuaObject("GameUIBarMiddle")
local GameUIColonial = LuaObjectManager:GetLuaObject("GameUIColonial")
local GameUISlaveSelect = LuaObjectManager:GetLuaObject("GameUISlaveSelect")
local GameObjectWorldBoss = LuaObjectManager:GetLuaObject("GameObjectWorldBoss")
local GameUIStarCraftMap = LuaObjectManager:GetLuaObject("GameUIStarCraftMap")
local GameMail = LuaObjectManager:GetLuaObject("GameMail")
local GameUIRebuildFleet = LuaObjectManager:GetLuaObject("GameUIRebuildFleet")
local GameUIPlayerInfo = LuaObjectManager:GetLuaObject("GameUIPlayerInfo")
local GameStateWVE = GameStateManager.GameStateWVE
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUISevenBenefit = LuaObjectManager:GetLuaObject("GameUISevenBenefit")
local GameStateStore = GameStateManager.GameStateStore
local GameStateDaily = GameStateManager.GameStateDaily
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameUIActivityEvent = LuaObjectManager:GetLuaObject("GameObjectActivityEvent")
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialFirstBattle = TutorialQuestManager.QuestTutorialFirstBattle
GameUIBarLeft.ShowFesivalButton = nil
GameUIBarLeft.isShowFesival = false
GameUIBarLeft.leftTime = 0
GameUIBarLeft.endTime = 0
GameUIBarLeft.eventPicUrl = ""
GameUIBarLeft.eventLevelDef = 0
GameUIBarLeft.isNewActivity = false
local isFirstEnterFestival = false
GameUIBarLeft.firstTimeEnterChat = true
GameUIBarLeft.serverTimeChangeCallback = nil
GameUIBarLeft.HelpTutorial = false
GameUIBarLeft.hasShowConfirm = false
GameUIBarLeft.onlineList = nil
GameUIBarLeft.onlineItem_net = nil
GameUIBarLeft.online_lefttime = 0
GameUIBarLeft.mServerTimeUTC = 0
local m_ServerTime = 0
local m_ServerTimeFetchLocal = 0
GameUIBarLeft.gotoActivityTag = "newSign"
GameUIBarLeft.is_new = false
local m_TipsInfoPriority = {
  durabilityToLow = 1,
  canEquip = 2,
  buildingCD = 9,
  buildingCD_VIP = 10,
  enhanceCD = 7,
  arenaCD = 6,
  collectionCD = 4,
  technologyCD = 11,
  rebuildCD = 5,
  supplyCD = 8,
  recruitCD = 3
}
local m_addButtonPrioty = {expandTime = 10, doneExpandTime = 0}
local POPUPITEMTYPE_NEWS = 1
local POPUPITEMTYPE_TOPLIST = 2
local POPUPITEMTYPE_MAIL = 3
local POPUPITEMTYPE_PRIZE = 4
local POPUPITEMTYPE_ALLI_AWARD = 5
local m_popupMenuItems = {
  {
    itemType = POPUPITEMTYPE_NEWS,
    btnName = "news",
    isShown = true,
    isShiny = 0
  },
  {
    itemType = POPUPITEMTYPE_TOPLIST,
    btnName = "toplist",
    isShown = true,
    isShiny = 0
  },
  {
    itemType = POPUPITEMTYPE_MAIL,
    btnName = "mail",
    isShown = true,
    isShiny = 0
  },
  {
    itemType = POPUPITEMTYPE_FIRSTCHARGE_AWARD,
    btnName = "reward",
    isShown = true,
    isShiny = 0
  }
}
GameUIBarLeft.NotifyInfoTable = {}
local m_ActivityTags = {
  sign = false,
  online = false,
  vip = false,
  level = false
}
function GameUIBarLeft:GetActivityTagsCnt()
  local cnt = 0
  for _, v in pairs(m_ActivityTags) do
    if v == true then
      cnt = cnt + 1
    end
  end
  if m_ActivityTags.online then
    cnt = cnt - 1
  end
  DebugOut("GetActivityTagsCnt", cnt)
  return cnt
end
function GameUIBarLeft:ClearActivityTags()
  for k, v in pairs(m_ActivityTags) do
    m_ActivityTags[k] = false
  end
end
function GameUIBarLeft:UpdatePopupItembtn(itemType, btnName, isShown, isShiny)
  for i, v in ipairs(m_popupMenuItems) do
    if v.itemType == itemType then
      v.btnName = btnName
      v.isShown = isShown
      v.isShiny = isShiny
    end
  end
  GameUIBarLeft:UpdatePopupMenu()
end
function GameUIBarLeft.UpdatePopupItemAllianceIcon()
  local alliance_info = GameGlobalData:GetData("alliance")
  local btnName = "empty"
  local isShown = false
  local isShiny = 0
  if alliance_info == nil or alliance_info.id == "" then
    isShown = false
    btnName = "empty"
  else
  end
  GameUIBarLeft:UpdatePopupItembtn(POPUPITEMTYPE_ALLI_AWARD, btnName, isShown, isShiny)
end
function GameUIBarLeft:UpdateFirstChargeStatus(showBtn)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local preStatus = GameUIBarLeft.showFirstChargeBtn
  GameUIBarLeft.showFirstChargeBtn = showBtn
  if preStatus ~= showBtn then
    if showBtn then
      self:GetFlashObject():InvokeASCallback("_root", "ShowFirstChargeStatus")
    else
      self:GetFlashObject():InvokeASCallback("_root", "HideFirstChargeStatus")
    end
  end
end
function GameUIBarLeft:UpdateActivityStatus(showBtn)
  local preStatus = GameUIBarLeft.showActivityBtn
  GameUIBarLeft.showActivityBtn = showBtn
  if preStatus ~= showBtn then
    if showBtn then
      self:GetFlashObject():InvokeASCallback("_root", "ShowActivityStatus")
      if TutorialQuestManager.QuestTutorialSign:IsActive() then
        self:GetFlashObject():InvokeASCallback("_root", "SetActivityBtnTipVisible", true)
      end
      self:GetFlashObject():InvokeASCallback("_root", "NewShowActivityStatus", GameUIBarLeft.isShowFesival)
    else
      self:GetFlashObject():InvokeASCallback("_root", "HideActivityStatus")
      self:GetFlashObject():InvokeASCallback("_root", "NewHideActivityStatus")
    end
  end
end
function GameUIBarLeft:ShowFBBall()
end
function GameUIBarLeft:HideFBBall()
  if self:GetFlashObject() then
    GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_HIDE
    self:GetFlashObject():InvokeASCallback("_root", "hideFBBall")
  end
end
function GameUIBarLeft:SetFBBallVisible(fbBallVisible)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setFBBallVisible", fbBallVisible)
  end
end
function GameUIBarLeft:FBBallSnapToScreen()
  if self:GetFlashObject() then
    GameStateMainPlanet.mFBBallState = GameStateMainPlanet.FBBALL_STATE.BALL_STATE_SNAP
    self:GetFlashObject():InvokeASCallback("_root", "fbBallSnapToScreen")
  end
end
function GameUIBarLeft.OnGlobalResourceChange()
  DebugOut("\230\137\147\229\141\176_root_OnGlobalResourceChange")
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  GameUIBarLeft:GetFlashObject():SetText("_root.user_main.money_bar.CubitNum.Text", GameUtils.numberConversion(resource.money))
  GameUIBarLeft:GetFlashObject():SetText("_root.user_main.money_bar.CreditNum.Text", GameUtils.numberConversion(resource.credit))
end
function GameUIBarLeft._OnCDTimeChange()
  local buildingName = GameGlobalData:GetLowestUpgradableBuilding()
  if buildingName and buildingName ~= "" then
    local buildingCDTimes = GameGlobalData:GetBuildingCDTimes()
    if #buildingCDTimes > 0 then
      GameUIBarLeft.CDTips.buildingCD = {
        shownType = "main_planet",
        priority = m_TipsInfoPriority.buildingCD,
        targetCDTime = os.time() + buildingCDTimes[1].left_time,
        timeUpTips = GameLoader:GetGameText("LC_MENU_BUILDING_CD")
      }
    end
    if #buildingCDTimes > 1 then
      GameUIBarLeft.CDTips.buildingCD_VIP = {
        shownType = "main_planet",
        priority = m_TipsInfoPriority.buildingCD_VIP,
        targetCDTime = os.time() + buildingCDTimes[2].left_time,
        timeUpTips = GameLoader:GetGameText("LC_MENU_BUILDING_CD")
      }
    end
  else
    GameUIBarLeft.CDTips.buildingCD = nil
    GameUIBarLeft.CDTips.buildingCD_VIP = nil
  end
  local enableArena = GameUIBarLeft:IsModuleEnable("arena")
  local arenaCDTime = GameGlobalData:GetArenaCDTime()
  if arenaCDTime and arenaCDTime.can_do == 1 and enableArena then
    GameUIBarLeft.CDTips.arenaCD = {
      shownType = "main_planet",
      priority = m_TipsInfoPriority.arenaCD,
      targetCDTime = os.time() + arenaCDTime.left_time,
      timeUpTips = GameLoader:GetGameText("LC_MENU_ARENA_CD")
    }
  else
    GameUIBarLeft.CDTips.arenaCD = nil
  end
  local starPortalInfo = GameGlobalData:GetBuildingInfo("star_portal")
  local recruitCDTime = GameGlobalData:GetRecruitCDTime()
  if recruitCDTime and recruitCDTime.can_do == 1 and 0 < starPortalInfo.level then
    GameUIBarLeft.CDTips.recruitCD = {
      shownType = "main_planet",
      priority = m_TipsInfoPriority.recruitCD,
      targetCDTime = os.time() + recruitCDTime.left_time,
      timeUpTips = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_SHORTCUT")
    }
  else
    GameUIBarLeft.CDTips.recruitCD = nil
  end
  DebugOutPutTable(GameUIBarLeft.CDTips, "GameUIBarLeft.CDTips")
  GameUIBarLeft:CheckEnhanceCDTips()
  GameUIBarLeft:CheckCollectionCDTips()
  GameUIBarLeft:CheckCDTime()
  GameUIBarLeft:UpdateTipsInfo()
end
function GameUIBarLeft.BattleSupplyRefreshTimeChangeCallback()
  local refreshTime = GameGlobalData:GetRefreshTime("battle_supply_refresh_time")
  if refreshTime then
    GameUIBarLeft.CDTips.supplyCD = {
      shownType = "battle_map",
      priority = m_TipsInfoPriority.supplyCD,
      targetCDTime = os.time() + refreshTime.next_time
    }
    GameUIBarLeft:CheckCDTime()
    GameUIBarLeft:UpdateTipsInfo()
  end
end
function GameUIBarLeft.OnGlobalUserInfoChange()
  DebugOut("\230\137\147\229\141\176OnGlobalUserInfoChange")
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  local userInfo = GameGlobalData:GetData("userinfo")
  DebugTable(userinfo)
  local name_display = GameUtils:GetUserDisplayName(userInfo.name)
  local nameText = name_display
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setPlayerName", nameText)
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setVipPosition")
end
function GameUIBarLeft.OnGlobalWdcInfoChange()
  DebugOut("\230\137\147\229\141\176OnGlobalWdcInfoChange")
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  local userInfo = GameGlobalData:GetData("userinfo")
  if userInfo == nil then
    return
  end
  DebugTable(userinfo)
  local wdcPlayerInfo = GameGlobalData:GetData("wdcPlayerInfo")
  if wdcPlayerInfo == nil then
    return
  end
  DebugTable(wdcPlayerInfo)
  local name_display = GameUtils:GetUserDisplayName(userInfo.name)
  local rank = GameUtils:GetPlayerRankInWdc()
  local nameText = name_display .. rank
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setPlayerName", nameText)
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setVipPosition")
end
function GameUIBarLeft.OnGlobalLevelInfoChange()
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  DebugOut("GameUIBarLeft.OnGlobalLevelInfoChange")
  local levelInfo = GameGlobalData:GetData("levelinfo")
  if GameUIBarLeft.currentUserLevel and levelInfo.level > GameUIBarLeft.currentUserLevel and levelInfo.level % 10 == 0 then
    m_ActivityTags.level = true
    GameUIActivity.levelList = nil
    GameUIBarLeft:showHaveNowActicity()
  end
  DebugTable(levelInfo)
  GameUIBarLeft:GetFlashObject():SetText("_root.user_main.PlayerLevel.level", GameLoader:GetGameText("LC_MENU_Level") .. levelInfo.level)
  GameUIBarLeft.currentUserLevel = levelInfo.level
  local levelProgress = math.floor(100 * levelInfo.experience / levelInfo.uplevel_experience)
  if GameUtils:isPlayerMaxLevel(levelInfo.level) then
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setEexperienceNumber", "-", "-")
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setExperiencePersent", 100)
  else
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setExperiencePersent", levelProgress)
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setEexperienceNumber", levelInfo.experience, levelInfo.uplevel_experience)
  end
  ext.trackUserLevel(levelInfo.level)
  if levelInfo and 0 < levelInfo.up_level then
    local recordGameInfo = {}
    recordGameInfo.Type = "LevleUp"
    recordGameInfo.Description = "Level Achieved"
    recordGameInfo.Level = levelInfo.level
    recordGameInfo.Country = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
    GameUtils:SafeRecordGameInfo(recordGameInfo)
  end
  GameUIBarLeft:UpdateFirstChargeStatus(GameUIFirstCharge:GetIsShow())
  if immanentversion == 2 then
    GameUIBarLeft:UpdateActivityStatus(levelInfo.level and levelInfo.level >= 5)
  elseif immanentversion == 1 then
    GameUIBarLeft:UpdateActivityStatus(levelInfo.level and levelInfo.level >= 10)
  end
  if immanentversion == 2 and levelInfo.level >= 22 and not QuestTutorialBuildAcademy:IsFinished() then
    local academy = GameGlobalData:GetBuildingInfo("commander_academy")
    if academy.level > 0 then
      QuestTutorialBuildAcademy:SetFinish(true)
      if not QuestTutorialsFirstKillBoss:IsActive() and not QuestTutorialsFirstKillBoss:IsFinished() then
        QuestTutorialsFirstKillBoss:SetActive(true)
      end
    else
      QuestTutorialBuildAcademy:SetActive(true)
    end
  end
  if GameUIBarLeft.rebuildTimesData then
    GameUIBarLeft.RebuildEntranceNtf(GameUIBarLeft.rebuildTimesData)
  end
end
function GameUIBarLeft._OnRefreshUnreadMail()
  GameUIBarLeft:UpdateUnreadMail()
end
function GameUIBarLeft.OnGlobalTaskChange()
  GameUIBarLeft:UpdateFinishedMission()
end
function GameUIBarLeft.OnGlobalTodayTaskChange()
  GameUIBarLeft:UpdateFinishedMission()
end
function GameUIBarLeft.OnGlobalBattleSupplyChange()
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  local battle_supply = GameGlobalData:GetData("battle_supply")
  if battle_supply then
    local textSupplyTimes = string.format("%d/%d", battle_supply.current, battle_supply.max)
    local supplyPercent = math.floor(battle_supply.current / battle_supply.max * 100)
    if supplyPercent < 0 then
      supplyPercent = 0
    elseif supplyPercent > 100 then
      supplyPercent = 100
    end
    GameUIBarLeft:GetFlashObject():SetText("_root.user_main.ep_bar._textSupplyTimes", textSupplyTimes)
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root.user_main.ep_bar._barPercent", "gotoAndStop", supplyPercent + 1)
  end
end
function GameUIBarLeft.onGlobalVipChange()
  GameUIBarLeft:SetVipInfo()
  GameUIBarLeft:UpdateTipsInfo()
end
function GameUIBarLeft.OnModulesStatusChange()
  GameUIBarLeft:UpdateMenuItemStatus()
  GameUIBarLeft:UpdateHelpButtonStatus()
end
function GameUIBarLeft:OnInitGame()
  GameGlobalData:RegisterDataChangeCallback("resource", self.OnGlobalResourceChange)
  GameGlobalData:RegisterDataChangeCallback("userinfo", self.OnGlobalUserInfoChange)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", self.OnGlobalLevelInfoChange)
  GameGlobalData:RegisterDataChangeCallback("cdtimes", self._OnCDTimeChange)
  GameGlobalData:RegisterDataChangeCallback("offline_info", self.OnGlobalOfflineInfoChange)
  GameGlobalData:RegisterDataChangeCallback("task_statistic", self.OnGlobalTaskChange)
  GameGlobalData:RegisterDataChangeCallback("today_task_statistic", self.OnGlobalTodayTaskChange)
  GameGlobalData:RegisterDataChangeCallback("unmailinfo", self._OnRefreshUnreadMail)
  GameGlobalData:RegisterDataChangeCallback("battle_supply", self.OnGlobalBattleSupplyChange)
  GameGlobalData:RegisterDataChangeCallback("modules_status", self.OnModulesStatusChange)
  GameGlobalData:RegisterDataChangeCallback("vipinfo", self.onGlobalVipChange)
  GameGlobalData:RegisterDataChangeCallback("user_quest", self.UpdateQuestStatus)
  GameGlobalData:RegisterDataChangeCallback("battle_supply_refresh_time", self.BattleSupplyRefreshTimeChangeCallback)
  GameGlobalData:RegisterDataChangeCallback("notice_and_act", self.UpdateNoticeStatus)
  GameGlobalData:RegisterDataChangeCallback("alliance", self.UpdatePopupItemAllianceIcon)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", self.UpdateFightingCapacityText)
  GameGlobalData:RegisterDataChangeCallback("matrix", self.UpdatePlayerMatrix)
  GameGlobalData:RegisterDataChangeCallback("buttons_change", self.OnGlobalTaskChange)
  GameUIBarLeft.PopupMenuIsShown = false
  GameUIBarLeft.TextTips = {}
  GameUIBarLeft.CDTips = {}
  GameUIBarLeft.ShownTips = {}
  GameUIBarLeft.ShownTipsOnBattleMap = {}
  GameTimer:Add(GameUIBarLeft._CDTimeTimer, 1000)
  GameTimer:Add(GameUIBarLeft._ChatMsgTimer, 2000)
end
function GameUIBarLeft:GetNewsAndActivityInfo()
  if not GameUIBarLeft.newsList then
    local requestType = "servers/" .. GameUtils:GetActiveServerInfo().id .. "/notice_and_act"
    local requestParam = {
      lang = GameSettingData.Save_Lang
    }
    GameUtils:HTTP_SendRequest(requestType, requestParam, GameUIBarLeft.RequestActivityCallback, false, GameUtils.httpRequestFailedCallback, "GET", "notencrypt")
  end
  GameUIActivityNew:RequestActivity()
end
function GameUIBarLeft:setOnlineTagBytimer()
  DebugOut("GameUIBarLeft:setOnlineTagBytimer")
  m_ActivityTags.online = true
  GameUIBarLeft:showHaveNowActicity()
end
function GameUIBarLeft.checkLevelRewardsReady()
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  if not GameUIBarLeft.levelList then
    return
  end
  for k, v in pairs(GameUIBarLeft.levelList) do
    if tonumber(v.status) == 1 then
      m_ActivityTags.level = true
    end
  end
  GameUIBarLeft:showHaveNowActicity()
end
function GameUIBarLeft.checkVipRewardsReady()
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  if not GameUIBarLeft.vipList then
    return
  end
  for k, v in pairs(GameUIBarLeft.vipList) do
    if tonumber(v.status) == 1 then
      m_ActivityTags.vip = true
    end
  end
  GameUIBarLeft:showHaveNowActicity()
end
function GameUIBarLeft:showHaveNowActicity()
  if not self:GetFlashObject() then
    return
  end
  DebugOut("GameUIBarLeft:showHaveNowActicity")
  local num = 0
  local value = GameGlobalData:GetData("loot_count")
  if value ~= nil then
    num = tonumber(value) <= 9 and tonumber(value) or 9
  end
  self:GetFlashObject():InvokeASCallback("_root", "setBtnActivityHighCnt", num)
end
function GameUIBarLeft.RefreshHaveNowActicity()
  GameUIBarLeft:showHaveNowActicity()
end
function GameUIBarLeft:checkActivityStatus()
  GameUIBarLeft:ClearActivityTags()
  GameUIBarLeft.checkLevelRewardsReady()
  if immanentversion == 2 then
    GameUIBarLeft:UpdateActivityStatus(GameUIBarLeft.currentUserLevel and GameUIBarLeft.currentUserLevel >= 5)
  elseif immanentversion == 1 then
    GameUIBarLeft:UpdateActivityStatus(GameUIBarLeft.currentUserLevel and GameUIBarLeft.currentUserLevel >= 10)
  end
end
function GameUIBarLeft.NetCallbackClearGateway(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.gateway_clear_req.Code then
    if content.code ~= 0 then
    end
    return true
  end
  return false
end
function GameUIBarLeft.GetIsNewActivity(content)
  DebugOut("GameUIBarLeft GetIsNewActivity")
  GameUIBarLeft.isNewActivity = content.status
  GameUIBarLeft:setHaveNewActivity(GameUIBarLeft.isNewActivity)
end
function GameUIBarLeft.ExpPropsNotifyHandler(content)
  DebugOut("GameUIBarLeft ExpPropsNotifyHandler")
  DebugTable(content)
  GameUIBarLeft.expPropsData = content
  GameUIBarLeft.expPropsData.time = content.lefttime + os.time()
end
local lastExpPropsValue, LastExpTimeValue
function GameUIBarLeft:UpdateExpPropsTime(dt)
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  local timeTxt = ""
  local value = 0
  if GameUIBarLeft.expPropsData and 0 < GameUIBarLeft.expPropsData.bln_effect and 0 < GameUIBarLeft.expPropsData.time then
    local leftTime = GameUIBarLeft.expPropsData.time - os.time()
    if leftTime > 0 then
      value = GameUIBarLeft.expPropsData.add_percent
      timeTxt = GameUtils:formatTimeString(leftTime)
    end
  end
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setExpPropsTime", value, timeTxt)
end
function GameUIBarLeft:setHaveNewActivity(isNew)
  DebugOut("GameUIBarLeft setHaveNewActivity")
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "setHaveNewActivity", isNew)
  GameUIBarLeft.isNewActivity = false
end
function GameUIBarLeft:sendNoticeToServer()
  if not GameUIBarLeft.newsList then
    return
  end
  if not GameUIBarLeft.activityList then
    return
  end
  local activitiesList = {}
  for k, v in pairs(GameUIBarLeft.activityList) do
    if v.code then
      table.insert(activitiesList, GameUtils:GetShortActivityID(v.code))
    end
  end
  DebugOut(" ---GameUIBarLeft:sendNoticeToServer()-\227\128\139 ", os.time())
  DebugTable(GameUIBarLeft.newsList)
  DebugOut("------")
  DebugTable(activitiesList)
  local requestParam = {
    notices = GameUIBarLeft.newsList.notices,
    activities = activitiesList
  }
  NetMessageMgr:SendMsg(NetAPIList.gateway_clear_req.Code, requestParam, self.NetCallbackClearGateway, false, nil)
  NetMessageMgr:SendMsg(NetAPIList.new_activities_compare_req.Code, requestParam, self.NetCallbackClearGateway, false, nil)
end
function GameUIBarLeft:checkShowpromotions()
  if not GameUIActivityNew.promotionsList then
    return
  end
  for k, v in pairs(GameUIActivityNew.promotionsList) do
    for k1, v1 in pairs(m_popupMenuItems) do
      if v1.btnName == v.id or v1.btnName == "empty" then
        table.remove(m_popupMenuItems, k1)
      end
    end
  end
  local insert_num = #m_popupMenuItems
  for k, v in pairs(GameUIActivityNew.promotionsList) do
    if v.enabled and insert_num < 8 then
      insert_num = insert_num + 1
      if v.id ~= "month_card" and v.id ~= "update_version" then
        local tmp = {
          itemType = insert_num,
          btnName = v.id,
          isShown = v.enabled,
          isShiny = 1
        }
        DebugOut("checkShowpromotions insert")
        DebugTable(tmp)
        table.insert(m_popupMenuItems, tmp)
      end
    end
  end
  GameUIBarLeft:UpdatePopupMenu()
end
function GameUIBarLeft:updateRewardItemCount(count)
  DebugOut("updateRewardItemCount", count)
  for k, v in pairs(m_popupMenuItems) do
    if v.btnName == "reward" then
      v.isShown = true
      v.isShiny = count
      break
    end
  end
  self:UpdatePopupMenu()
end
function GameUIBarLeft:isAcityRewardBtn(arg)
  if arg == "total_credit" or arg == "levelup" or arg == "krypton2" or arg == "krypton1" or arg == "fleet" or arg == "champion_rank" or arg == "officer" or arg == "cutoff" or arg == "daily_purchase" or arg == "purchase_extra" or arg == "fleet_cutoff" or arg == "fleet_purchase_limit" or arg == "krypton" or arg == "krypton_part" or arg == "krypton_energy" then
    return true
  else
    return false
  end
end
function GameUIBarLeft:GetActivityReward(arg, meCondition)
  DebugOut("GameUIBarLeft:GetActivityReward", arg, meCondition)
  local content = {id = arg, condition = meCondition}
  GameUIBarLeft.rewarActivityType = arg
  NetMessageMgr:SendMsg(NetAPIList.promotion_award_req.Code, content, GameUIBarLeft.GetActivityRewardCallback, true, nil)
end
function GameUIBarLeft.GetSpecialActivityIsReward(content)
  GameUIBarLeft.specialActicityList = content
  GameUIBarLeft:checkActivityStatus()
end
function GameUIBarLeft.RequestActivityCallback(responseContent)
  DebugTable(responseContent)
  GameUIBarLeft.newsList = responseContent
  GameUIBarLeft.UpdateNoticeStatus()
  GameUIBarLeft:sendNoticeToServer()
  return true
end
function GameUIBarLeft.GetActivityRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.promotion_award_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.promotion_award_ack.Code then
    DebugOut("\230\137\147\229\141\176  GetActivityRewardCallback")
    local infoContent = GameLoader:GetGameText("LC_MENU_REWARD_INFO")
    infoContent = string.gsub(infoContent, "%[EVENT%]", GameUIBarLeft:GetActivityTypeName(GameUIBarLeft.rewarActivityType))
    infoContent = string.gsub(infoContent, "%[TYPE%]", ":" .. "\n" .. GameUIBarLeft:getRewardsText(content))
    infoContent = string.gsub(infoContent, "%[NUMBER%]", " ")
    local info_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(info_title, infoContent)
    GameUIActivityNew.CheckActivityType2IsReward()
    return true
  end
  return false
end
function GameUIBarLeft:GetActivityTypeName(activity_type)
  local eventName = ""
  if activity_type == "champion_rank" then
    eventName = GameLoader:GetGameText("LC_MENU_ARENA_RANK_GIFT_CHAR")
  elseif activity_type == "fleet" then
    eventName = GameLoader:GetGameText("LC_MENU_RECRUIT_GIFT_CHAR")
  elseif activity_type == "levelup" then
    eventName = GameLoader:GetGameText("LC_MENU_LIMIT_LEVEL_GIFT_CHAR")
  elseif activity_type == "total_credit" then
    eventName = GameLoader:GetGameText("LC_MENU_MULT_CHARGE_GIFT_CHAR")
  elseif activity_type == "cutoff" then
    eventName = GameLoader:GetGameText("LC_MENU_EVENT_DAILY_DIVIDEND_TPYE")
  elseif activity_type == "daily_purchase" then
    eventName = GameLoader:GetGameText("LC_MENU_EVENT_DAILT_RECHARGE_TYPE")
  elseif activity_type == "purchase_extra" then
    eventName = GameLoader:GetGameText("LC_MENU_EVENT_ONE_RECHARGE_TYPE")
  elseif activity_type == "officer" then
    eventName = GameLoader:GetGameText("LC_MENU_EVENT_DIPLOMAT_NAME_CHAR")
  end
  return eventName
end
function GameUIBarLeft:getRewardsText(content)
  DebugTable(content)
  local rewardsText = ""
  if not content.awards then
    return rewardsText
  end
  for k, v in pairs(content.awards) do
    local tempText = GameUIBarLeft:getItemDetailInfo(v.item_type, v.number, v.no)
    rewardsText = rewardsText .. tempText .. " "
  end
  return rewardsText
end
function GameUIBarLeft:getItemDetailInfo(itemType, number, no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" then
    local temp = "LC_ITEM_ITEM_NAME_" .. tostring(number)
    rewards = GameLoader:GetGameText(temp) .. "x" .. no
  elseif itemType == "krypton" then
    rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB") .. "x" .. no
  elseif itemType == "pve_supply" or itemType == "battle_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR") .. "x" .. number
  elseif itemType == "credit" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT") .. "x" .. number
  elseif itemType == "money" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT") .. "x" .. number
  elseif itemType == "vip_exp" then
    rewards = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR") .. "x" .. number
  elseif itemType == "technique" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT") .. "x" .. number
  elseif itemType == "expedition" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION") .. "x" .. number
  elseif itemType == "prestige" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_PRESTIGE") .. "x" .. number
  elseif itemType == "lepton" then
    rewards = GameLoader:GetGameText("LC_MENU_LOOT_LEPTON") .. "x" .. number
  elseif itemType == "quark" then
    rewards = GameLoader:GetGameText("LC_MENU_QUARK_CHAR") .. "x" .. number
  elseif itemType == "kenergy" then
    rewards = GameLoader:GetGameText("LC_MENU_krypton_energy") .. "x" .. number
  elseif itemType == "pvp_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR") .. "x" .. number
  elseif itemType == "ac_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR") .. "x" .. number
  elseif itemType == "laba_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_DEXTER_COIN") .. "x" .. number
  elseif itemType == "exp" then
    rewards = GameLoader:GetGameText("LC_MENU_EXP_TAB") .. "x" .. number
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return rewards
end
function GameUIBarLeft.UpdateNoticeStatus()
  local viewedNoticeId = GameGlobalData.GlobalData.notice_and_act
  DebugOut(" GameUIBarLeft viewedNoticeId--> ", os.time())
  DebugTable(viewedNoticeId)
  if not GameUIBarLeft.newsList then
    return
  end
  if not viewedNoticeId then
    return
  end
  local isHaveNew = false
  for k, v in pairs(GameUIBarLeft.newsList.notices) do
    DebugOut(k, v)
    local is_exit_id = false
    for k1, v1 in pairs(viewedNoticeId.notices) do
      if GameUtils:GetShortActivityID(v) == v1 then
        is_exit_id = true
      end
    end
    if not is_exit_id then
      isHaveNew = true
    end
  end
  local is_Shiny = 0
  if isHaveNew then
    is_Shiny = 1
  else
    is_Shiny = 0
  end
  for i, v in ipairs(m_popupMenuItems) do
    if v.itemType == POPUPITEMTYPE_NEWS then
      v.isShiny = is_Shiny
    end
  end
  GameUIBarLeft:UpdatePopupMenu()
end
function GameUIBarLeft.UpdateQuestStatus()
  DebugOut("UpdateQuestStatus")
  local quest = GameGlobalData:GetData("user_quest")
  local fourceTutorial = false
  if not GameStateMainPlanet:IsObjectInState(GameQuestMenu) and not GameStateMainPlanet:IsObjectInState(GameUICommonDialog) and (QuestTutorialQuestCenter:IsActive() or QuestTutorialGetQuestReward:IsActive()) then
    fourceTutorial = true
  end
  if not quest then
    return
  end
  if not GameQuestMenu.isWaitForRward and GameUIBarLeft:GetFlashObject() then
    if GameQuestMenu:checkIsShowActivityQuest() then
      GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "UpdateQuestStatus", 2, fourceTutorial)
    else
      GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "UpdateQuestStatus", quest.status, fourceTutorial)
    end
  end
end
function GameUIBarLeft.onGlobalQuestChange()
end
function GameUIBarLeft.OnGlobalOfflineInfoChange()
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBarLeft) and GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    GameUIBarLeft:ShowOfflineRewardIcon()
  end
end
function GameUIBarLeft:CheckNeedShowQuestTutorial()
  DebugOut("GameUIBarLeft:CheckNeedShowQuestTutorial")
  local quest = GameGlobalData:GetData("user_quest")
  local progress = GameGlobalData:GetData("progress")
  if not GameStateMainPlanet:IsObjectInState(GameQuestMenu) and not GameStateMainPlanet:IsObjectInState(GameUICommonDialog) and (QuestTutorialQuestCenter:IsActive() or QuestTutorialGetQuestReward:IsActive()) then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTuturoialMainTask", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "ShowTuturoialMainTask", false)
  end
  if immanentversion == 2 and QuestTutorialMakeUserLevel10:IsActive() then
    local levelInfo = GameGlobalData:GetData("levelinfo")
    if levelInfo.level >= 10 then
      QuestTutorialMakeUserLevel10:SetFinish(true)
      if not QuestTutorialBuildStar:IsActive() and not QuestTutorialBuildStar:IsFinished() then
        QuestTutorialBuildStar:SetActive(true)
        if GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
          GameStateMainPlanet:CheckShowTutorial()
        end
      end
    end
  end
  GameUIBarLeft:ShowIsNewFunctionInLab()
  GameUIBarLeft:ShowIsNewFunctionComboGacha()
end
function GameUIBarLeft:ShowIsNewFunctionInLab()
  if not self:GetFlashObject() then
    return
  end
  if (QuestTutorialMine:IsActive() or QuestTutorialSlot:IsActive() or QuestTutorialNewFunctonWorldboss:IsActive() or QuestTutorialNewFunctonAC:IsActive() or QuestTutorialInfiniteCosmos:IsActive()) and not TutorialQuestManager.QuestTutorialBuildTechLab:IsActive() and not TutorialQuestManager.QuestTutorialBuildFactory:IsActive() and not TutorialQuestManager.QuestTutorialBuildKrypton:IsActive() and not TutorialQuestManager.QuestTutorialBuildAffairs:IsActive() and not TutorialQuestManager.QuestTutorialUseTechLab:IsActive() and not TutorialQuestManager.QuestTutorialUseFactory:IsActive() and not TutorialQuestManager.QuestTutorialUseKrypton:IsActive() and not TutorialQuestManager.QuestTutorialUseAffairs:IsActive() then
    DebugOut("ShowLabTip !!!!!!!!!")
    self:GetFlashObject():InvokeASCallback("_root", "ShowLabTip")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideLabTip")
  end
end
function GameUIBarLeft:ShowIsNewFunctionComboGacha()
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if QuestTutorialComboGacha:IsActive() then
    DebugOut("ShowComboGachaTip")
    flash_obj:InvokeASCallback("_root", "ShowComboGachaTip")
    GameUIBarLeft:GetFlashObject():unlockInput()
  else
    DebugOut("HideComboGachaTip")
    flash_obj:InvokeASCallback("_root", "HideComboGachaTip")
  end
end
function GameUIBarLeft:CheckMainTaskTutorial()
  if not self:GetFlashObject() then
    return
  end
  local quest = GameGlobalData:GetData("user_quest")
  if QuestTutorialMainTask:IsActive() and quest.quest_id ~= 4 and quest.quest_id ~= 5 and not QuestTutorialEquipmentEvolution:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "ShowTuturoialMainTask", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "ShowTuturoialMainTask", false)
  end
end
function GameUIBarLeft:SetVipInfo()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  DebugOut("set ui bar vip level = ", curLevel)
  local m, k = 0, 0
  if curLevel >= 10 then
    m = math.floor(curLevel / 10)
    k = curLevel - m * 10
  else
    k = curLevel
  end
  self.currentVipLevel = curLevel
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "refreshVipInfo", m, k)
  end
end
function GameUIBarLeft:updateUserAvatar(id)
  local avatar = ""
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  if id == 0 then
    avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  else
    avatar = GameDataAccessHelper:GetFleetAvatar(id, player_main_fleet.level)
  end
  self.mainAvatar = avatar
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "changeUserAvatar", avatar)
  end
end
function GameUIBarLeft:ShowUILeft()
  local sex = GameGlobalData:GetUserInfo().sex
  local curGameState = GameStateManager:GetCurrentGameState()
  GameUIBarLeft:SetVipInfo()
  self:GetFlashObject():InvokeASCallback("_root.top_expand.popup_menu", "onMoveOutOver")
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "chat_bar", true)
  if curGameState == GameStateBattleMap and GameStateBattleMap.m_currentAreaID and GameStateBattleMap.m_currentAreaID == 60 and immanentversion == 2 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "quest", false)
  else
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "quest", true)
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "complete_bar", false)
  self:GetFlashObject():InvokeASCallback("_root", "MoveInQuestIcon")
  local player_main_fleet = GameGlobalData:GetFleetInfo(1)
  local player_avatar = GameUtils:GetPlayerAvatar(nil, player_main_fleet.level)
  local leaderlist = GameGlobalData:GetData("leaderlist")
  DebugTable(leaderlist)
  if leaderlist and GameGlobalData.GlobalData and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex], player_main_fleet.level)
    DebugOut("fix")
  elseif GameGlobalData:GetUserInfo().icon ~= 0 and GameGlobalData:GetUserInfo().icon ~= 1 then
    player_avatar = GameDataAccessHelper:GetFleetAvatar(GameGlobalData:GetUserInfo().icon, player_main_fleet.level)
  end
  self.mainAvatar = player_avatar
  DebugOut("hello :what's", player_avatar)
  GameUIBarLeft:RefreshRedPoint()
  GameUIBarLeft:SetPlayerFightingCapacityText()
  if curGameState == GameStateMainPlanet then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "top_expand", true)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInUserInfo", player_avatar, true)
    self:GetFlashObject():InvokeASCallback("_root", "SetChatIconType", 1, GameUIBarRight.m_isMovedIn)
    GameUIBarLeft:ShowOfflineRewardIcon()
    GameUIBarLeft:MoveInTopMenu()
    DebugOut("qqj test  1111")
    GameUIBarLeft:ShowChatIcon()
    GameUIBarLeft:ShowMoreIcon()
    GameUIBarLeft:ShowHelpButton()
    GameUIBarLeft:UpdateMenuItemStatus()
    GameUIBarLeft:ShowActivityIcon()
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetChatIconType", 2, GameUIBarRight.m_isMovedIn)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "top_expand", false)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInUserInfo", player_avatar, false)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", "offhook_mc", false)
    self:GetFlashObject():InvokeASCallback("_root", "setMoreGamesVsible", false)
    GameUIBarLeft:HideOfflineRewardIcon()
    DebugOut("qqj test  2222")
    GameUIBarLeft:ShowChatIcon()
    GameUIBarLeft:HideHelpButton()
    GameUIBarLeft:HideActivityIcon()
  end
  if GameUIBarLeft.showFirstChargeBtn then
    self:GetFlashObject():InvokeASCallback("_root", "ShowFirstChargeStatus")
  end
  if GameUIBarLeft.showActivityBtn then
    self:GetFlashObject():InvokeASCallback("_root", "ShowActivityStatus")
    if TutorialQuestManager.QuestTutorialSign:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetActivityBtnTipVisible", true)
    end
    self:GetFlashObject():InvokeASCallback("_root", "NewShowActivityStatus")
  end
  GameUITechnology:ReqTechnology()
  GameUIBarLeft:UpdateTipsInfo()
  if m_initEnter then
    self:GetFlashObject():InvokeASCallback("_root", "updateChatBarText", "")
    m_initEnter = false
  end
  self:CheckNeedShowQuestTutorial()
  self:CheckSevenDayButtonStatus()
  self:CheckShowActivityEvent()
  if GameUtils:IsNeedChinaIDAuth() and GameGlobalData.GlobalData.id_status ~= nil and GameGlobalData.GlobalData.id_status.isAuth ~= true and not GameUIBarLeft.hasShowConfirm then
    GameUIBarLeft.hasShowConfirm = true
    local function callback()
      GameStateMainPlanet:CheckNewTutorial()
    end
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:showIdComfirmBox(callback)
  end
end
function GameUIBarLeft:RequestActivityEventNTF()
  DebugOut("RequestActivityEventNTF")
  NetMessageMgr:SendMsg(NetAPIList.event_ntf_req.Code, nil, nil, false)
end
function GameUIBarLeft:CheckShowActivityEvent()
  if GameUIBarLeft.eventInfo2 then
    GameUIBarLeft._asShowEvent()
  else
    self:RequestActivityEventNTF()
  end
end
function GameUIBarLeft:HideUILeft()
  self:GetFlashObject():InvokeASCallback("_root", "MoveOutUserInfo")
  self:GetFlashObject():InvokeASCallback("_root", "MoveOutQuestIcon")
  self:GetFlashObject():InvokeASCallback("_root", "hideActivitEvent")
  self:GetFlashObject():InvokeASCallback("_root", "hideActivitEvent2")
  if GameUIBarLeft.showFirstChargeBtn then
    self:GetFlashObject():InvokeASCallback("_root", "HideFirstChargeStatus")
  end
  GameUIBarLeft:HideOfflineRewardIcon()
  GameUIBarLeft:HideMoreIcon()
  GameUIBarLeft:MoveOutTopMenu()
  if curGameState == GameStateMainPlanet then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutQuestIcon")
  else
    GameUIBarLeft:MoveOutCompleteBar()
  end
end
function GameUIBarLeft:OnParentStateGetFocus()
  DebugOut("GameUIBarLeft: onParentStateGetFocus")
  if GameStateManager:GetCurrentGameState() == GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateBattleMap then
    self:ShowUILeft()
    if self:GetFlashObject() then
      self:GetFlashObject():InvokeASCallback("_root", "ShowTuturoialMainTask", false)
    end
  else
  end
end
function GameUIBarLeft.ShowActivity(content)
  local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
  local isPopSign = false
  if content.enable and not GameGlobalData:FTPVersion() then
    if immanentversion == 2 then
      if QuestTutorialCityHall:IsFinished() or QuestTutorialCityHall:IsActive() then
        GameUIBarLeft.isFirstShow = true
        GameNotice:AddNotice("Sign")
      end
    elseif immanentversion == 1 then
      GameUIBarLeft.isFirstShow = true
      GameNotice:AddNotice("Sign")
      isPopSign = true
    end
  end
  GameUIRechargePush:SignPopReceived(isPopSign)
end
function GameUIBarLeft:OnAddToGameState(state)
  GameUIBarLeft:GetNewsAndActivityInfo()
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameUIBarLeft.UpdateChatButtonStatus)
  GameGlobalData:RegisterDataChangeCallback("loot_count", GameUIBarLeft.RefreshHaveNowActicity)
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  DebugOut("GameUIBarLeft:OnAddToGameState chatCount=", chatCount)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if chatCount and chatCount > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setNewChatNum", chatCount)
  if state ~= GameStateMainPlanet then
    GameUIBarLeft:SetFBBallVisible(false)
    self:GetFlashObject():InvokeASCallback("_root", "hideActivitEvent")
    self:GetFlashObject():InvokeASCallback("_root", "hideActivitEvent2")
    self:GetFlashObject():InvokeASCallback("_root", "SetChatIconType", 2, GameUIBarRight.m_isMovedIn)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetChatIconType", 1, GameUIBarRight.m_isMovedIn)
  end
  self:GetFlashObject():InvokeASCallback("_root", "setGivingdayButtonText", GameLoader:GetGameText("LC_MENU_BUTTON_NEW_NAME_4"))
end
function GameUIBarLeft:OnEraseFromGameState(state)
  self:UnloadFlashObject()
end
function GameUIBarLeft:CheckTutorial()
  if immanentversion == 2 then
    DebugOut("GameUIBarLeft:CheckTutorial", QuestTutorialRechargePower:IsActive())
    if self:GetFlashObject() then
      DebugOut("falsh is ok ")
    end
    if QuestTutorialRechargePower:IsActive() then
      DebugOut("_step_2")
      local function callback()
        DebugOut("_step_3")
        self:GetFlashObject():InvokeASCallback("_root", "ShowPowerTip")
      end
      GameUICommonDialog:PlayStory({51208}, callback)
      callback()
    else
      self:GetFlashObject():InvokeASCallback("_root", "HidePowerTip")
    end
  end
end
GameUIBarLeft.isShowInteritaled = false
function GameUIBarLeft:ShowChatIcon()
  if not GameUIBarRight.m_isMovedIn or GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    self:GetFlashObject():InvokeASCallback("_root", "MoveInChatIcon", false)
  end
end
function GameUIBarLeft.YysNtfCallback(content)
  DebugOut(" GameUIBarLeft:YysNtfCallback-----")
  GameUIBarLeft.isShownMoreApp = 0
  local isShowInterital = 0
  for k, v in pairs(content.config) do
    DebugOut("------->", v.key)
    DebugOut("------->", v.value)
    if v.key == 1 then
      isShowInterital = v.value
    end
    if v.key == 2 then
      GameUIBarLeft.isShownMoreApp = v.value
    end
  end
  DebugOut("GameUIBarLeft.isShownMoreApp= ", GameUIBarLeft.isShownMoreApp, isShowInterital)
  if GameUIBarLeft.isShownMoreApp == 1 and GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    GameUIBarLeft:ShowMoreIcon()
  end
  if isShowInterital == 1 and GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBarLeft) and not GameUIBarLeft.isShowInteritaled then
    GameUIBarLeft.isShowInteritaled = true
    ext.showInterstitial()
  end
end
function GameUIBarLeft:ShowMoreIcon()
  if not self:GetFlashObject() then
    return
  end
  DebugOut("ShowMoreIcon---> ")
  if GameUIBarLeft.isShownMoreApp and GameUIBarLeft.isShownMoreApp == 1 then
    DebugOut("showMoreApps--go")
    self:GetFlashObject():InvokeASCallback("_root", "setMoreGamesVsible", true)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInMoreIcon")
  end
end
function GameUIBarLeft:HideMoreIcon()
  if GameUIBarLeft.isShownMoreApp and GameUIBarLeft.isShownMoreApp == 1 then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutMoreIcon")
  end
end
function GameUIBarLeft:ShowHelpButton(...)
  if not self:GetFlashObject() then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  if resource.prestige > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "ShowHelpButtonStatus")
  else
    self:GetFlashObject():InvokeASCallback("_root", "HideHelpButtonStatus")
  end
end
function GameUIBarLeft:HideHelpButton(...)
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "HideHelpButtonStatus")
end
function GameUIBarLeft:HideChatIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "MoveOutChatIcon")
  end
end
function GameUIBarLeft:ShowOfflineRewardIcon()
  local offline_reward_data = GameGlobalData.GlobalData.offline_info
  local enable_offline = GameGlobalData:GetModuleStatus("offline")
  if offline_reward_data and enable_offline and offline_reward_data.minute > 30 and not self:CheckNotifyRepeat("offlineAward") and GameStateManager:GetCurrentGameState() == GameStateMainPlanet then
    self:PushNotifyInfo("offlineAward", "offlineAward", function()
      GameUIOfflineReward:ShowOfflineReward()
    end)
  end
end
function GameUIBarLeft:HideOfflineRewardIcon()
  self:GetFlashObject():InvokeASCallback("_root", "hideNotifyInfo", "offlineAward")
end
function GameUIBarLeft:SetCDTimerColor(newColor)
  self:GetFlashObject():InvokeASCallback("_root", "ChangeCDTimerColor", newColor)
end
function GameUIBarLeft:MoveInTopMenu()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_moveInTopMenu")
  self:UpdateLabBtn()
end
function GameUIBarLeft:MoveOutTopMenu()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_moveOutTopMenu")
end
function GameUIBarLeft:MoveInCompleteBar()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_moveInCompleteBar")
end
function GameUIBarLeft:MoveOutCompleteBar()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_moveOutCompleteBar")
end
function GameUIBarLeft:UpdateUnreadMail()
  local unread_mail = GameGlobalData:GetData("unmailinfo")
  for i, v in ipairs(m_popupMenuItems) do
    if v.itemType == POPUPITEMTYPE_MAIL then
      v.isShiny = unread_mail.count
    end
  end
  GameUIBarLeft:UpdatePopupMenu()
end
function GameUIBarLeft:UpdateSupplyLoot()
  local supply_loot_info = GameGlobalData:GetData("supply_loot")
  for i, v in ipairs(m_popupMenuItems) do
    if v.itemType == POPUPITEMTYPE_PRIZE then
      v.isShiny = supply_loot_info.enable and 1 or 0
    end
  end
  GameUIBarLeft:UpdatePopupMenu()
end
function GameUIBarLeft:UpdateFinishedMission()
  local finish_count = 0
  for k, v in pairs(GameGlobalData.ButtonsStatus or {}) do
    if v.name == "dailytask" then
      finish_count = v.count
      break
    end
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "lua2fs_updateFinishedMission", finish_count)
  end
end
function GameUIBarLeft:UpdateLabBtn()
  local lab_cnt = GameUILab:GetTodayUnusedCnt()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "lua2fs_updateLabBtn", lab_cnt)
  end
end
function GameUIBarLeft:UpdatePopupMenu()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local shinyCnt = 0
    for i, v in ipairs(m_popupMenuItems) do
      if 0 < v.isShiny then
        shinyCnt = shinyCnt + 1
      end
    end
    flash_obj:InvokeASCallback("_root", "lua2fs_updatePopupMenu", shinyCnt)
  end
end
function GameUIBarLeft:UpdateMenuItemStatus()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local modules_status = GameGlobalData:GetData("modules_status")
  DebugOutPutTable(modules_status, "modules_status+modules_status")
  for _, module_data in ipairs(modules_status.modules) do
    local item_path = self:GetFlashObject():InvokeASCallback("_root", "getMenuItemPath", module_data.name)
    if item_path ~= "" then
      if module_data.name == "task" then
        module_data.status = true
      end
      flash_obj:InvokeASCallback("_root", "setMenuItemEnabled", module_data.name, module_data.status)
    end
    DebugOut("fuck this is moudle_data_name", module_data.name)
    if immanentversion == 2 then
      if module_data.name == "worldboss" and module_data.status and not QuestTutorialNewFunctonWorldboss:IsFinished() then
        QuestTutorialNewFunctonWorldboss:SetActive(true)
      end
      if module_data.name == "ac" and module_data.status and not QuestTutorialNewFunctonAC:IsFinished() then
        QuestTutorialNewFunctonAC:SetActive(true)
      end
    end
  end
  if immanentversion == 2 then
    GameUIBarLeft:ShowIsNewFunctionInLab()
  end
end
function GameUIBarLeft:IsModuleEnable(module_name)
  local modules_status = GameGlobalData:GetData("modules_status")
  for _, v in pairs(modules_status.modules) do
    if v.name == module_name and v.status == true then
      return true
    end
  end
  return false
end
function GameUIBarLeft.ChatMsgIncoming(content)
  content.content = GameUtils:CensorWordsFilter(content.content)
  GameUIBarLeft:MsgIncoming(content)
end
function GameUIBarLeft:MsgIncoming(chatMsg)
  table.insert(m_chatMsgQueue, chatMsg)
  DebugOut("nowInsertChatMsg:")
  DebugOutPutTable(chatMsg, "chatMsg")
end
function GameUIBarLeft:UpdateChatBarText()
  GameObjectMainPlanet:CheckBattleChanelState()
  local chatMsg = table.remove(m_chatMsgQueue, 1)
  if GameUIChat.mChannelStatus and not GameUIChat:IsBattleChannelOpened() or not GameStateStarCraft.mStarCraftActive and not GameStateWD.inActive then
    while chatMsg and chatMsg.channel == "tc_chat_channel" do
      chatMsg = table.remove(m_chatMsgQueue, 1)
    end
  end
  if GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateLargeMap then
    while chatMsg and (chatMsg.channel == "largemap_world_channel" or chatMsg.channel == "largemap_country_channel") do
      chatMsg = table.remove(m_chatMsgQueue, 1)
    end
  else
    while chatMsg and (chatMsg.channel == "tc_chat_channel" or string.find(chatMsg.channel, "alchat")) do
      chatMsg = table.remove(m_chatMsgQueue, 1)
    end
  end
  if chatMsg and GameUIBarLeft:GetServerTimeUTC() - chatMsg.timestamp <= 86400 then
    local flash_obj = self:GetFlashObject()
    local msgContent = chatMsg.content
    if chatMsg.emojiContent then
      msgContent = chatMsg.emojiContent
    else
      msgContent = LuaUtils:ConvertEmojiUnicode2Html(msgContent)
      chatMsg.emojiContent = msgContent
    end
    msgContent = LuaUtils:ConvertEmojiUnicode2Html(msgContent)
    if chatMsg.translateResult then
      msgContent = chatMsg.translateResult
    end
    local channelStr = GameUtils:GetChannelStr(chatMsg)
    local talkerName = GameUtils:GetUserDisplayName(chatMsg.sender_name)
    local playerType = tonumber(chatMsg.sender_role)
    if playerType == 1 then
      msgContent = " <font color='#EEEE00'> " .. msgContent .. "</font>"
    elseif playerType == 3 then
      msgContent = " <font color='#FFBBFF'> " .. msgContent .. "</font>"
    end
    local htmlText = ""
    if talkerName == "" and msgContent == "" then
      htmlText = ""
    else
      htmlText = channelStr .. " <font color='#0099FF'>" .. talkerName .. ":</font> " .. msgContent
    end
    if (GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWD or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattleMap) and flash_obj then
      flash_obj:InvokeASCallback("_root", "updateChatBarText", htmlText)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization then
      GameUIColonial:UpdateChatMessage(htmlText)
      GameUISlaveSelect:UpdateChatMessage(htmlText)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWorldBoss then
      GameObjectWorldBoss:UpdateChatMessage(htmlText)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateStarCraft then
      GameUIStarCraftMap:UpdateChatBarText(htmlText)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWVE then
      DebugOut("update bar text htmlText = ", htmlText)
      WVEGameManeger:UpdateChatBarText(htmlText)
    elseif GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
      local GameUILargeMap = LuaObjectManager:GetLuaObject("GameUILargeMap")
      GameUILargeMap:UpdateChatBarText(htmlText)
    end
  end
end
GameUIBarLeft.SYNC_SERVER_TIME_INTERVAL = 60
function GameUIBarLeft._CDTimeTimer()
  if GameStateManager:GetCurrentGameState() == GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateBattleMap and (not GameObjectTutorialCutscene:IsActive() or not GameObjectTutorialCutscene:IsNeedShowNewFakeBattle()) then
    GameUIBarLeft:CheckTechTips()
    GameUIBarLeft:CheckFleetsDurability()
    GameUIBarLeft:CheckEnhanceCDTips()
    GameUIBarLeft:CheckCollectionCDTips()
    GameUIBarLeft:CheckCDTime()
    GameUIBarLeft:UpdateTipsInfo()
  end
  local curOSTime = os.time()
  GameUIBarLeft.mServerTimeUTC = GameUIBarLeft.mServerTimeUTC + (curOSTime - m_ServerTimeFetchLocal)
  m_ServerTime = m_ServerTime + (curOSTime - m_ServerTimeFetchLocal)
  m_ServerTimeFetchLocal = curOSTime
  GameUIBarLeft:SetServerTimeDisplay()
  if GameUIBarLeft.serverTimeChangeCallback then
    GameUIBarLeft.serverTimeChangeCallback()
  end
  GameUIBarLeft.SYNC_SERVER_TIME_INTERVAL = GameUIBarLeft.SYNC_SERVER_TIME_INTERVAL - 1
  if GameUIBarLeft.SYNC_SERVER_TIME_INTERVAL <= 0 then
    NetMessageMgr:SendMsg(NetAPIList.sync_server_time_req.Code, nil, nil, false, nil)
    GameUIBarLeft.SYNC_SERVER_TIME_INTERVAL = 60
  end
  return 1000
end
function GameUIBarLeft._ChatMsgTimer()
  if GameStateManager:GetCurrentGameState() == GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattleMap or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateColonization or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWorldBoss or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateStarCraft or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWD or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWVE or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateLargeMap then
    GameUIBarLeft:UpdateChatBarText()
  end
  return 2000
end
function GameUIBarLeft:SetServerTimeChangehandler(callback)
  GameUIBarLeft.serverTimeChangeCallback = callback
end
local lastVipTime = 0
function GameUIBarLeft:Update(dt)
  local flash_obj = self:GetFlashObject()
  if flash_obj == nil then
    return
  end
  local vipTime = 0
  local vipTimeStr
  if GameVipDetailInfoPanel.mTmpInfoFetchTime and 0 < GameVipDetailInfoPanel.mTmpInfoFetchTime then
    vipTime = GameVipDetailInfoPanel.mTmpInfo.left_time - (os.time() - GameVipDetailInfoPanel.mTmpInfoFetchTime)
  end
  if lastVipTime ~= vipTime then
    if vipTime > 0 then
      vipTimeStr = GameUtils:formatTimeString(vipTime)
    else
      vipTime = 0
    end
    lastVipTime = vipTime
    DebugOut("lastVipTime = ", lastVipTime)
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_onUpdateFrame", dt, vipTimeStr)
  end
  GameUIBarLeft:UpdateExpPropsTime()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "OnUpdateFrame", dt)
  GameUIActivity:UpdateOnlineTime()
  if self.mainAvatar == "head9001" then
    local id = 0
    if leaderlist and GameGlobalData.GlobalData and GameGlobalData.GlobalData.curMatrixIndex and leaderlist.leader_ids and leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex] then
      id = leaderlist.leader_ids[GameGlobalData.GlobalData.curMatrixIndex]
    elseif GameGlobalData:GetUserInfo().icon ~= 0 and GameGlobalData:GetUserInfo().icon ~= 1 then
      id = GameGlobalData:GetUserInfo().icon
    end
    self:updateUserAvatar(tonumber(id))
  end
end
function GameUIBarLeft:CheckTechTips()
  local canUpgradeTechType = GameUITechnology:GetCanUpgradeTechType()
  local needShowTechTips = canUpgradeTechType and canUpgradeTechType ~= nil
  if needShowTechTips then
    if GameUIBarLeft.TextTips.technologyCD == nil then
      GameUIBarLeft.TextTips.technologyCD = {
        priority = m_TipsInfoPriority.technologyCD,
        arrivalTime = os.time(),
        text = GameLoader:GetGameText("LC_MENU_TECH_CD")
      }
    end
  else
    GameUIBarLeft.TextTips.technologyCD = nil
  end
end
function GameUIBarLeft:CheckFleetsDurability()
  if GameGlobalData:NeedShowDurabilityTooLowTips() then
    if GameUIBarLeft.TextTips.durabilityToLow == nil then
      GameUIBarLeft.TextTips.durabilityToLow = {
        priority = m_TipsInfoPriority.durabilityToLow,
        arrivalTime = os.time(),
        text = GameLoader:GetGameText("LC_MENU_SLAC_FLEETS_STATUS_DESC_FACTORY")
      }
    end
  else
    GameUIBarLeft.TextTips.durabilityToLow = nil
  end
end
function GameUIBarLeft:CheckEnhanceCDTips()
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  if curLevel and curLevel >= 1 then
    GameUIBarLeft.TextTips.enhanceCD = nil
  else
    local enhanceCD = GameGlobalData:GetEnhanceCDTime()
    if enhanceCD.can_do == 1 then
      if GameUIBarLeft.TextTips.enhanceCD == nil then
        GameUIBarLeft.TextTips.enhanceCD = {
          priority = m_TipsInfoPriority.enhanceCD,
          arrivalTime = os.time(),
          text = GameLoader:GetGameText("LC_MENU_ENHANCE_CD")
        }
      end
    else
      GameUIBarLeft.TextTips.enhanceCD = nil
    end
  end
end
function GameUIBarLeft:CheckCollectionCDTips()
  local collectionCD = GameGlobalData:GetCollectionCDTime()
  if collectionCD.can_do == 1 then
    if GameUIBarLeft.TextTips.collectionCD == nil then
      GameUIBarLeft.TextTips.collectionCD = {
        priority = m_TipsInfoPriority.collectionCD,
        arrivalTime = os.time(),
        text = GameLoader:GetGameText("LC_MENU_COLLECTION_CD")
      }
    end
  else
    GameUIBarLeft.TextTips.collectionCD = nil
  end
end
function GameUIBarLeft.RebuildEntranceNtf(content)
  if content then
    GameUIBarLeft.rebuildTimesData = content
  end
  if content and content.times > 0 and GameUIRebuildFleet:IsRebuildVisible(false) then
    if GameUIBarLeft.TextTips.rebuildCD == nil then
      GameUIBarLeft.TextTips.rebuildCD = {
        priority = m_TipsInfoPriority.rebuildCD,
        arrivalTime = os.time(),
        text = GameLoader:GetGameText("LC_MENU_REBUILD_TITLE")
      }
      GameUIBarLeft:UpdateTipsInfo()
    end
  else
    GameUIBarLeft.TextTips.rebuildCD = nil
    GameUIBarLeft:UpdateTipsInfo()
  end
end
function GameUIBarLeft:CheckCDTime()
  for k, v in pairs(GameUIBarLeft.CDTips) do
    if v.targetCDTime then
      if os.time() > v.targetCDTime and v.timeUpTips == nil then
        v.targetCDTime = nil
        v.shownCDInfo = false
      elseif not v.shownCDInfo then
        v.shownCDInfo = true
      end
    elseif v.shownCDInfo then
      v.shownCDInfo = false
    end
  end
end
function GameUIBarLeft:UpdateCDTimeInfo()
  for k, v in pairs(GameUIBarLeft.CDTips) do
    if v.shownCDInfo and v.targetCDTime then
      local leftTime = v.targetCDTime - os.time()
      if leftTime < 0 then
        leftTime = 0
      end
      local shownTable = v.shownType == "battle_map" and self.ShownTipsOnBattleMap or self.ShownTips
      if leftTime > 0 then
        table.insert(shownTable, {
          tipType = k,
          priority = v.priority,
          targetCDTime = v.targetCDTime,
          tipText = GameUtils:formatTimeString(leftTime)
        })
      elseif v.timeUpTips then
        table.insert(shownTable, {
          tipType = k,
          priority = v.priority,
          targetCDTime = v.targetCDTime,
          tipText = v.timeUpTips
        })
      end
    end
  end
end
function GameUIBarLeft:UpdateTextTipInfo()
  for k, v in pairs(GameUIBarLeft.TextTips) do
    table.insert(self.ShownTips, {
      tipType = k,
      priority = v.priority,
      targetCDTime = v.arrivalTime,
      tipText = v.text
    })
  end
  local quest = GameGlobalData:GetData("user_quest")
  if quest then
    local condTypeText, condNumText = GameDataAccessHelper:GetMainQuestTitileText(quest.quest_id), tostring(quest.cond_count) .. "/" .. tostring(quest.cond_total)
    local tipText_tmp = condTypeText .. condNumText
    local tipData = {
      tipType = "excuteQuest",
      priority = 0,
      targetCDTime = 0,
      tipText = tipText_tmp,
      status = self:CheckQuestStatus(quest)
    }
    table.insert(self.ShownTips, tipData)
    self:AddPVEQuestToBattleMapTips(quest, tipData)
  end
end
function GameUIBarLeft:AddPVEQuestToBattleMapTips(quest, tipData)
  local prompt = quest.prompt
  local tipDataTemp = LuaUtils:table_copy(tipData)
  if prompt ~= "undefined" then
    local fun = loadstring(" return " .. prompt)
    local promptTable = fun()
    tipDataTemp.status = 0
    table.insert(self.ShownTipsOnBattleMap, tipDataTemp)
  end
end
function GameUIBarLeft:CheckQuestStatus(quest)
  if tonumber(quest.cond_count) >= tonumber(quest.cond_total) then
    return 1
  end
  local prompt = quest.prompt
  if prompt ~= "undefined" then
    local fun = loadstring(" return " .. prompt)
    local promptTable = fun()
    local command = promptTable[1]
    local param = promptTable[2]
    if command == "building_up" then
      for k, v in pairs(self.ShownTips) do
        if v.tipType == "buildingCD" and v.targetCDTime - os.time() <= 0 then
          return 2
        end
      end
      return 0
    elseif command == "arena" then
      local pvp_supply = GameGlobalData:GetData("pvp_supply")
      if pvp_supply and 0 < tonumber(pvp_supply.current) then
        return 2
      end
      return 0
    elseif command == "pve_latest" then
      local progress = GameGlobalData:GetData("progress")
      if progress then
        local monsterInfo = GameDataAccessHelper:GetBattleMonsterInfo(progress.act, progress.chapter)
        local levelInfo = GameGlobalData:GetData("levelinfo")
        if monsterInfo and tonumber(monsterInfo._level) <= tonumber(levelInfo.level) then
          return 2
        end
      end
      return 0
    elseif command == "pve" then
      if quest.cond_int then
        local act, battleID = math.modf(tonumber(quest.cond_int) / 1000000)
        battleID = tonumber(quest.cond_int) - act * 1000000
        local monsterInfo = GameDataAccessHelper:GetBattleMonsterInfo(act, battleID)
        local levelInfo = GameGlobalData:GetData("levelinfo")
        if monsterInfo and levelinfo and tonumber(monsterInfo._level) <= tonumber(levelInfo.level) then
          return 2
        end
      end
      return 0
    end
  end
  return 2
end
function GameUIBarLeft:UpdateTipsInfo(tipText)
  self.ShownTips = {}
  self.ShownTipsOnBattleMap = {}
  self:UpdateCDTimeInfo()
  self:UpdateTextTipInfo()
  if GameStateManager:GetCurrentGameState() == GameStateMainPlanet and #self.ShownTips > 0 then
    self:ShowTipsInfo(self.ShownTips)
  elseif GameStateManager:GetCurrentGameState() == GameStateBattleMap and #self.ShownTipsOnBattleMap > 0 then
    self:ShowTipsInfo(self.ShownTipsOnBattleMap)
  else
    self:ShowTipsInfo({})
  end
end
function GameUIBarLeft:ShowTipsInfo(shownTipsTable)
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    local ShowTipsSortComp = function(a, b)
      if a.priority == b.priority then
        return a.targetCDTime < b.targetCDTime
      else
        return a.priority < b.priority
      end
    end
    table.sort(shownTipsTable, ShowTipsSortComp)
    for i, v in ipairs(shownTipsTable) do
      if i <= 4 then
        local isHilight = false
        if v.tipType == "excuteQuest" then
          local quest = GameGlobalData:GetData("user_quest")
          if quest.status == GameQuestMenu.QUEST_STS_UNLOOT then
            isHilight = true
          end
        end
        flash_obj:InvokeASCallback("_root", "UpdateTipContent", i, v.tipType, v.tipText, isHilight, v.status or 0)
        if v.tipType == "supplyCD" then
          flash_obj:InvokeASCallback("_root", "SetTipBarTitle", i, v.tipType, GameLoader:GetGameText("LC_MENU_SUPPLY_RECOVERY_CD"))
        end
      end
    end
    flash_obj:InvokeASCallback("_root", "UpdateTipsCount", #shownTipsTable)
  end
end
function GameUIBarLeft:UnshowPopupMenu()
  if self.PopupMenuIsShown then
    self:GetFlashObject():InvokeASCallback("_root", "HidePopupMenu")
    self.PopupMenuIsShown = false
  end
end
function GameUIBarLeft:PushNotifyInfo(iconType, notifyType, notifyCallback)
  local newNotifyInfo = {}
  newNotifyInfo.iconType = iconType
  newNotifyInfo.notifyType = notifyType
  newNotifyInfo.notifyCallback = notifyCallback
  table.insert(self.NotifyInfoTable, newNotifyInfo)
  self:UpdateNotifyInfoIcon()
end
function GameUIBarLeft:UpdateNotifyInfoIcon()
  local dataTable = {}
  for itemIndex = 1, 3 do
    local notifyInfo = self.NotifyInfoTable[itemIndex]
    if notifyInfo then
      table.insert(dataTable, notifyInfo.iconType)
    else
      table.insert(dataTable, -1)
    end
  end
  local dataString = table.concat(dataTable, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "setNotifyInfo", dataString)
end
function GameUIBarLeft:ProcessNotifyInfo(notifyType)
  local processed = false
  for itemIndex, notifyInfo in ipairs(self.NotifyInfoTable) do
    if notifyInfo.iconType == notifyType then
      table.remove(self.NotifyInfoTable, itemIndex)
      notifyInfo.notifyCallback()
      processed = true
      break
    end
  end
  assert(processed)
  self:UpdateNotifyInfoIcon()
end
function GameUIBarLeft:CheckLabNewFuncTutorial()
  if QuestTutorialNewFunctonAC:IsActive() then
    QuestTutorialNewFunctonAC:SetFinish(true)
  end
  if QuestTutorialNewFunctonWorldboss:IsActive() then
    QuestTutorialNewFunctonWorldboss:SetFinish(true)
  end
  if QuestTutorialSlot:IsActive() or QuestTutorialMine:IsActive() then
    AddFlurryEvent("TutorialSlot_EnterDexLab", {}, 2)
  end
  GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "HideLabTip")
end
function GameUIBarLeft:CheckNotifyRepeat(iconType, notifyType)
  for itemIndex, notifyInfo in ipairs(self.NotifyInfoTable) do
    if notifyInfo.notifyType == notifyType or notifyInfo.iconType == iconType then
      return true
    end
  end
  return false
end
function GameUIBarLeft:OnFSCommand(cmd, arg)
  if cmd == "Popup_Chat_Windows" then
    self:HideChatIcon()
    GameStateManager:GetCurrentGameState():AddObject(GameUIChat)
    if GameStateManager:GetCurrentGameState() == GameStateManager.GameStateWD then
      if GameStateWD.firstTimeEnterChat == true then
        GameUIChat:SelectChannel("battle")
      elseif GameUIChat.activeChannel == "largemap_world" or GameUIChat.activeChannel == "largemap_country" then
        GameUIChat:SelectChannel("battle")
      else
        GameUIChat:SelectChannel(GameUIChat.activeChannel)
      end
    elseif GameUIBarLeft.firstTimeEnterChat == true then
      GameUIChat:SelectChannel("world")
    elseif GameUIChat.activeChannel == "largemap_world" or GameUIChat.activeChannel == "largemap_country" then
      GameUIChat:SelectChannel("world")
    else
      GameUIChat:SelectChannel(GameUIChat.activeChannel)
    end
  elseif cmd == "FBBallClicked" then
    if Facebook:IsLoginedIn() and Facebook:IsBindFacebook() then
      FacebookPopUI.mInviteTitleText = GameLoader:GetGameText("LC_MENU_NEW_FACEBOOK_LOG_IN_BUTTON_4")
      FacebookPopUI:GetFacebookInvitableFriendList(false)
    elseif not Facebook:IsBindFacebook() then
      FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
      FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_VIP_CREDIT)
    end
  elseif "View_Offline_Reward" == cmd then
    GameUIOfflineReward:ShowOfflineReward()
  elseif cmd == "clicked_btn_Lab" then
    GameUIBarLeft:CheckLabNewFuncTutorial()
    GameStateLab.mCurSubState = STATE_LAB_SUB_STATE.SUBSTATE_NORMAL
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateLab)
  elseif cmd == "clicked_btn_more_game" then
    ext.showMoreApps()
    DebugOut("fuck clicked_btn_more_game")
  elseif cmd == "clicked_btn_mission" then
    if not GameGlobalData:FTPVersion() then
      GameStateManager:SetCurrentGameState(GameStateDaily)
    end
  elseif cmd == "clicked_btn_popupMenu" then
    if not GameUIBarLeft.PopupMenuIsShown then
      local popupMenuBtnNames = ""
      for i, v in ipairs(m_popupMenuItems) do
        if v.isShown then
          popupMenuBtnNames = popupMenuBtnNames .. v.btnName .. "\002" .. v.isShiny .. "\001"
        end
      end
      local lang = "en"
      if GameSettingData and GameSettingData.Save_Lang then
        lang = GameSettingData.Save_Lang
        if string.find(lang, "ru") == 1 then
          lang = "ru"
        end
      end
      self:GetFlashObject():InvokeASCallback("_root", "ShowPopupMenu", popupMenuBtnNames, lang)
      GameUIBarLeft.PopupMenuIsShown = true
    else
      self:UnshowPopupMenu()
    end
  elseif cmd == "animation_over" then
    GameUIBarLeft.onGlobalQuestChange()
  elseif cmd == "closePopupMenu" then
    self:UnshowPopupMenu()
  elseif cmd == "clicked_PopupMenuItem" then
    local index = tonumber(arg)
    local commandName = m_popupMenuItems[index].btnName
    if commandName == "news" then
      GameNews:ShowNews("news")
    elseif commandName == "toplist" then
      GameStateManager:SetCurrentGameState(GameStateManager.GameStateToprank)
    elseif commandName == "mail" then
      if not GameGlobalData:FTPVersion() then
        local GameMail = LuaObjectManager:GetLuaObject("GameMail")
        GameStateMainPlanet:AddObject(GameMail)
        GameMail:ShowBox(GameMail.MAILBOX_ONLINE_TAB)
      end
    elseif commandName == "prize4" then
      local supply_loot_info = GameGlobalData:GetData("supply_loot")
      assert(supply_loot_info)
      if supply_loot_info.enable then
        local function netCall()
          NetMessageMgr:SendMsg(NetAPIList.supply_loot_get_req.Code, nil, self.NetCallbackSupplyLootGet, true, netCall)
        end
        netCall()
      else
        local infoContent = GameLoader:GetGameText("LC_MENU_ALREADY_RECEIVING")
        GameTip:Show(infoContent)
      end
    elseif commandName == "reward" then
      DebugOut("clicked reward")
      GameStateMainPlanet:AddObject(GameObjectAwardCenter)
    elseif commandName == "prize6" or commandName == "prize7" then
      local function netCall()
        NetMessageMgr:SendMsg(NetAPIList.alliance_weekend_award_req.Code, nil, self.AllianceWeekendAwardCallback, true, nil)
      end
      netCall()
    elseif self:isAcityRewardBtn(commandName) then
      self:GetActivityReward(commandName, 0)
    else
      DebugOut("\230\156\170\229\164\132\231\144\134\231\154\132\229\188\185\229\135\186\232\143\156\229\141\149\229\145\189\228\187\164\239\188\154" .. commandName)
    end
    self:UnshowPopupMenu()
  elseif cmd == "EnterVip" then
    if not GameGlobalData:FTPVersion() then
      GameUIPlayerInfo:addUIPlayerInfo()
    end
  elseif cmd == "btn_Add" then
    if not GameGlobalData:FTPVersion() then
      GameVip:showVip(false)
    end
  elseif cmd == "showQuestMenu" then
    local QuestTutorialCityHall = TutorialQuestManager.QuestTutorialCityHall
    if QuestTutorialCityHall:IsFinished() and QuestTutorialGetQuestReward:IsActive() then
      AddFlurryEvent("ClickCityHallUpgradeQuestBtn", {}, 1)
    end
    self:DoShowQuestMenu()
    self:GetFlashObject():InvokeASCallback("_root", "HideQuestAnimTip")
    DebugOut("OnFSCommand:showQuestMenu")
  elseif cmd == "showEventMenu" then
    GameUIActivityEvent.curEventId = tonumber(arg)
    if GameStateManager:GetCurrentGameState() ~= GameStateManager.GameStateMainPlanet then
      GameStateManager.GameStateMainPlanet:EraseObject(GameUIActivityEvent)
    end
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityEvent)
  elseif cmd == "recover_supply_times" then
    GameUISection:AddBattleSupply()
  elseif cmd == "process_notify_info" then
    self:ProcessNotifyInfo(arg)
  elseif cmd == "pressCDTip" then
    self:PressCDTip(arg)
  elseif cmd == "clickedFirstChargeBtn" then
    DebugOut("clickedFirstChargeBtn")
    GameStateManager:GetCurrentGameState():AddObject(GameUIFirstCharge)
  elseif cmd == "clickedActivityBtn" then
    if not GameGlobalData:FTPVersion() then
      if not self.gotoActivityTag then
        self.gotoActivityTag = "newSign"
      end
      GameUIActivity:ShowNews(self.gotoActivityTag)
    end
  elseif cmd == "addButtonIncreament" then
    m_addButtonPrioty.doneExpandTime = m_addButtonPrioty.doneExpandTime + 1
    if m_addButtonPrioty.doneExpandTime == m_addButtonPrioty.expandTime then
      local flash_obj = GameUIBarLeft:GetFlashObject()
      if flash_obj ~= nil then
        DebugOut("InvokeASCallback lua2fs_setAddButtonExpand")
        flash_obj:InvokeASCallback("_root", "lua2fs_setAddButtonExpand", 0)
      end
    end
  elseif cmd == "enterFestivalReleased" then
    DebugOut("enterActivity")
    if QuestTutorialComboGacha:IsActive() then
      AddFlurryEvent("Gacha_ClickIcon", {}, 1)
    end
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivityNew)
    GameStateMainPlanet.Cover = GameUIActivityNew
  elseif cmd == "sevendaybenefit" then
    GameStateManager:GetCurrentGameState():AddObject(GameUISevenBenefit)
  elseif cmd == "clickedHelpButton" then
    GameAndroidBackManager:AddCallback(GameUIBarLeft.OnAndroidBackCustom)
    GameUIBarLeft.HelpTutorial = true
    self:ShowHelpButtonLayer()
  elseif cmd == "allHelpButton" then
    GameUIBarMiddle:HideMiddleMenu()
    GameUIBarRight:MoveOutRightMenu()
    self:ShowHelpDetaiLayer(arg)
  elseif cmd == "allHelpDetailButton" then
    self:ShowHelpEveryButton(arg)
  elseif cmd == "OnActiveIcon" then
    self:OnActiveIcon(tonumber(arg))
  elseif cmd == "onclick_activityItem" then
    self:Onclick_activityItem(arg)
  elseif cmd == "OnClickEvent" and GameUIBarLeft.eventHightlight then
    NetMessageMgr:SendMsg(NetAPIList.event_click_req.Code, nil, function()
      return false
    end, false, nil)
    GameUIBarLeft.eventHightlight = false
  end
end
function GameUIBarLeft:PressCDTip(tipTypeName)
  GameUIBarMiddle:HideMiddleMenu()
  if tipTypeName == "durabilityToLow" then
    local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
    GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
  elseif tipTypeName == "buildingCD" or tipTypeName == "buildingCD_VIP" then
    local buildingName = GameGlobalData:GetLowestUpgradableBuilding()
    if buildingName and buildingName ~= "" then
      local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
      GameUIBuilding:DisplayUpgradeDialog(buildingName)
    end
  elseif tipTypeName == "enhanceCD" then
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    GameStateEquipEnhance.isNeedShowEnhanceUI = true
    GameStateEquipEnhance:EnterEquipEnhance()
  elseif tipTypeName == "arenaCD" then
    GameUIArena:SetArenaType(GameUIArena.ARENA_TYPE.normalArena)
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateArena)
  elseif tipTypeName == "collectionCD" then
    local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
    GameUICollect:ShowCollect()
  elseif tipTypeName == "technologyCD" then
    local GameUITechnology = LuaObjectManager:GetLuaObject("GameUITechnology")
    GameUITechnology:EnterTechnology()
  elseif tipTypeName == "excuteQuest" then
    local quest = GameGlobalData:GetData("user_quest")
    if tonumber(quest.cond_count) >= tonumber(quest.cond_total) then
      GameQuestMenu:ExecuteCurrentQuest()
    else
      GameQuestMenu:ShowQuestMenu()
    end
  elseif tipTypeName == "rebuildCD" then
    local GameUIFactory = LuaObjectManager:GetLuaObject("GameUIFactory")
    GameStateManager:GetCurrentGameState():AddObject(GameUIFactory)
    GameUIFactory.EnterRebuild = true
  elseif tipTypeName == "recruitCD" then
    local GameStateRecruit = GameStateManager.GameStateRecruit
    GameStateManager:SetCurrentGameState(GameStateRecruit)
  elseif tipTypeName == "canEquip" then
    GameUIBarLeft:SendToServerEquipClicked()
    local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
    local defFleetIdx = -1
    local defSlot = -1
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    if fleets and #fleets > 0 and GameUIBarLeft.NewEquipNtfData and GameUIBarLeft.NewEquipNtfData.display then
      for k = 1, #fleets do
        if tostring(GameUIBarLeft.NewEquipNtfData.fleet_id) == tostring(fleets[k].id) then
          defFleetIdx = k
          defSlot = GameUIBarLeft.NewEquipNtfData.equip_slot
          break
        end
      end
    end
    GameUIBarLeft.NewEquipNtfData = nil
    GameStateEquipEnhance:SetDefaultFleetAndSlot(defFleetIdx, defSlot)
    GameStateEquipEnhance:EnterEquipEnhance()
  end
end
function GameUIBarLeft.NetCallbackSupplyLootGet(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.supply_loot_get_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameGlobalData:GetData("supply_loot").enable = false
      GameUIBarLeft:UpdateSupplyLoot()
    end
    return true
  end
  return false
end
function GameUIBarLeft.AllianceWeekendAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_weekend_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  elseif msgType == NetAPIList.alliance_weekend_award_ack.Code then
    local alliance_info = GameGlobalData:GetData("alliance")
    alliance_info.weekend_award = false
    GameUIBarLeft.UpdatePopupItemAllianceIcon()
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    for key, v in pairs(content) do
      local numberText = tostring(v)
      local nameText = GameHelper:GetAwardTypeTextAndIcon(key, v)
      local tipText = GameHelper:GetRewardInfo(nameText, numberText)
      GameTip:Show(tipText)
    end
    return true
  end
  return false
end
function GameUIBarLeft.UpdateServerTime(content)
  DebugOut("!!!GameUIBarLeft.UpdateServerTime")
  DebugTable(content)
  if content.login_time_current_utc then
    GameUIBarLeft.mServerTimeUTC = content.login_time_current_utc
  end
  if content.google_trans_tag then
    if 0 == content.google_trans_tag then
      GameUIChat.mIsTranslateOpen = false
    else
      GameUIChat.mIsTranslateOpen = true
    end
  else
    GameUIChat.mIsTranslateOpen = false
  end
  if content.mail_trans_tag then
    if 0 == content.mail_trans_tag then
      GameMail.mIsTranslateOpen = false
    else
      GameMail.mIsTranslateOpen = true
    end
  else
    GameMail.mIsTranslateOpen = false
  end
  m_ServerTime = content.login_time_current
  m_ServerTimeFetchLocal = os.time()
  GameUIBarLeft:SetServerTimeDisplay()
  return true
end
function GameUIBarLeft:IsDownloadPNGExsit(filename)
  return ...
end
function GameUIBarLeft:ShowEventIcon()
  DebugOut("GameUIBarLeft:ShowEventIcon")
  GameUIBarLeft.ShowFesivalButton = true
  local flash_obj = GameUIBarLeft:GetFlashObject()
  if GameUIBarLeft.isShowFesival and flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "setFestivalEntranceIcon", isFirstEnterFestival, true)
    GameUIBarLeft:UpdateFestivalCDTime()
  end
end
function GameUIBarLeft:DownEventIcon()
  local pngName = string.match(GameUIBarLeft.eventPicUrl, "([^/]*)$")
  local localPath = "data2/" .. pngName
  local replaceName = "common_event_entrance.png"
  if DynamicResDownloader:IfResExsit(localPath) then
    if GameUIBarLeft:GetFlashObject() then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      GameUIBarLeft:GetFlashObject():ReplaceTexture(replaceName, pngName)
    end
  else
    local extInfo = {}
    extInfo.localPath = localPath
    extInfo.pngName = pngName
    extInfo.replaceName = replaceName
    DynamicResDownloader:AddDynamicRes(pngName, DynamicResDownloader.resType.FESTIVAL_BANNER, extInfo, GameUIBarLeft.bannerDownloaderCallback)
  end
end
function GameUIBarLeft.bannerDownloaderCallback(extInfo)
  if DynamicResDownloader:IfResExsit(extInfo.localPath) and GameUIBarLeft:GetFlashObject() then
    GameUIBarLeft:GetFlashObject():ReplaceTexture(extInfo.replaceName, extInfo.pngName)
  end
end
function GameUIBarLeft:GetServerTimeUTC()
  return GameUIBarLeft.mServerTimeUTC
end
function GameUIBarLeft:GetServerTime()
  return GameUIBarLeft.fuckGameUIBarLeftServerTime
end
function GameUIBarLeft:SetServerTimeDisplay()
  local serverTimeInDay = m_ServerTime % 86400
  GameUIBarLeft.fuckGameUIBarLeftServerTime = m_ServerTime
  local serverTimeText = GameLoader:GetGameText("LC_MENU_UTC_CHAR") .. GameUtils:formatTimeStringWithoutSeconds(serverTimeInDay)
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "setServerTimeDisplay", serverTimeText)
  end
end
function GameUIBarLeft:HideForWD()
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "hideForWD")
  end
end
function GameUIBarLeft:ShowForWD()
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "restoreForWD")
  end
end
function GameUIBarLeft:ShowAddButtonExpand(count)
  DebugOut("GameUIBarLeft:ShowAddButtonExpand: " .. count)
  if count > 0 then
    m_addButtonPrioty.expandTime = count
    m_addButtonPrioty.doneExpandTime = 0
    local flash_obj = self:GetFlashObject()
    if flash_obj ~= nil then
      flash_obj:InvokeASCallback("_root", "lua2fs_setAddButtonExpand", count)
    end
  end
end
function GameUIBarLeft:ShowAddButtonBreath(flag)
  DebugOut("GameUIBarLeft:ShowAddButtonBreath: ")
  DebugOut(flag)
  local flash_obj = self:GetFlashObject()
  if flash_obj ~= nil then
    flash_obj:InvokeASCallback("_root", "lua2fs_setAddButtonBreath", flag)
  end
end
function GameUIBarLeft.PayAnimationNtfHandler(content)
  GameUIBarLeft:ShowAddButtonExpand(content.twink_time)
  GameUIBarLeft:ShowAddButtonBreath(content.breath_time)
end
function GameUIBarLeft.awardCountNtfHandler(content)
  DebugOut("awardCountNtfHandler", content.count)
  if content.count > 0 then
    totalAwardCount = content.count
    GameUIBarLeft:updateRewardItemCount(totalAwardCount)
  end
end
function GameUIBarLeft.UpdateChatButtonStatus()
  DebugOut("UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  DebugOut(chatCount)
  local flash_obj = GameUIBarLeft:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
  flash_obj:InvokeASCallback("_root", "lua2fs_setNewChatNum", chatCount)
end
GameUIBarLeft.NewEquipNtfData = nil
function GameUIBarLeft.NewEquipNtf(content)
  DebugOut("NewEquipNtf")
  DebugTable(content)
  if content.display then
    GameUIBarLeft.NewEquipNtfData = content
    if GameUIBarLeft.TextTips.canEquip == nil then
      GameUIBarLeft.TextTips.canEquip = {
        priority = m_TipsInfoPriority.canEquip,
        arrivalTime = os.time(),
        text = GameLoader:GetGameText("LC_MENU_NEW_EQUIP_AVAILABLE")
      }
      GameUIBarLeft:UpdateTipsInfo()
    end
  else
    GameUIBarLeft.NewEquipNtfData = nil
    GameUIBarLeft.TextTips.canEquip = nil
  end
end
function GameUIBarLeft:SendToServerEquipClicked()
  GameUIBarLeft.TextTips.canEquip = nil
  GameUIBarLeft:UpdateTipsInfo()
  NetMessageMgr:SendMsg(NetAPIList.equip_pushed_req.Code, req, GameUIBarLeft.SendToServerEquipClickedCallback, false, nil)
end
function GameUIBarLeft.SendToServerEquipClickedCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equip_pushed_req.Code then
    if 0 ~= content.code then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIBarLeft:UpdateSevenDayBenifitStatus(openflag, buttonFlag)
end
function GameUIBarLeft:CheckSevenDayButtonStatus()
  DebugOut("CheckSevenDayButtonStatus")
end
function GameUIBarLeft.UpdatePlayerMatrix()
  GameUIBarLeft:SetPlayerFightingCapacityText()
end
function GameUIBarLeft.UpdateFightingCapacityText()
  DebugOut("UpdateFightingCapacityText")
  GameUIBarLeft:SetPlayerFightingCapacityText()
end
function GameUIBarLeft:SetPlayerFightingCapacityText()
  local currentForce = GameHelper:GetFleetsForce()
  local text = GameLoader:GetGameText("LC_MENU_TOTAL_FORCE_CHAR")
  local currentForceText = "<font color='#0078BE'>" .. text .. "</font>" .. " " .. "<font color='#83E3FF'>" .. tostring(GameUtils.numberAddComma(currentForce)) .. "</font>"
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetForceText", currentForceText)
  end
end
GameUIBarLeft.HelpButton = {
  creatManor = {
    "planetary_fortress",
    "engineering_bay",
    "tech_lab",
    "krypton_center"
  },
  upgrade = {
    "putonEquip",
    "upgradeEquip",
    "upgradeTech",
    "composeKrypton"
  },
  fight = {
    "explorePlanetary",
    "achivement",
    "collect",
    "getMoney"
  },
  bored = {
    "everyDayTask",
    "tryLuck",
    "fleets",
    "chat"
  }
}
local ItemStatus = {
  NORMAL = 1,
  TODAY_UNUSED = 2,
  NEW_FEATURE = 3,
  LOCKED = 4,
  NOT_OPEN = 5
}
GameUIBarLeft.HelpButtonStatus = {
  {
    name = "planetary_fortress",
    status = ItemStatus.LOCKED
  },
  {
    name = "engineering_bay",
    status = ItemStatus.LOCKED
  },
  {
    name = "tech_lab",
    status = ItemStatus.LOCKED
  },
  {
    name = "krypton_center",
    status = ItemStatus.LOCKED
  },
  {
    name = "putonEquip",
    status = ItemStatus.LOCKED
  },
  {
    name = "upgradeEquip",
    status = ItemStatus.LOCKED
  },
  {
    name = "upgradeTech",
    status = ItemStatus.LOCKED
  },
  {
    name = "composeKrypton",
    status = ItemStatus.LOCKED
  },
  {
    name = "explorePlanetary",
    status = ItemStatus.LOCKED
  },
  {
    name = "achivement",
    status = ItemStatus.LOCKED
  },
  {
    name = "collect",
    status = ItemStatus.LOCKED
  },
  {
    name = "getMoney",
    status = ItemStatus.LOCKED
  },
  {
    name = "everyDayTask",
    status = ItemStatus.LOCKED
  },
  {
    name = "tryLuck",
    status = ItemStatus.LOCKED
  },
  {
    name = "fleets",
    status = ItemStatus.LOCKED
  },
  {
    name = "chat",
    status = ItemStatus.LOCKED
  }
}
function GameUIBarLeft:UpdateHelpButtonStatus(...)
  local modules = GameGlobalData:GetData("modules_status").modules
  for _, v in ipairs(modules) do
  end
end
function GameUIBarLeft:ShowHelpButtonLayer()
  local nameArray = ""
  local textArray = ""
  nameArray = "creatManor" .. "\001" .. "upgrade" .. "\001" .. "fight" .. "\001" .. "bored" .. "\001"
  textArray = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_1") .. "\001" .. GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_2") .. "\001" .. GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_3") .. "\001" .. GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_4") .. "\001"
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setHelpButtonText", nameArray, textArray)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInHelpAllButton")
  end
end
function GameUIBarLeft:ShowHelpDetaiLayer(name)
  local buttonItem = GameUIBarLeft.HelpButton[name]
  DebugOut("buttonItem = ")
  DebugTable(buttonItem)
  local nameArray = ""
  local textArray = ""
  for _, v in ipairs(buttonItem) do
    nameArray = nameArray .. v .. "\001"
  end
  if name == "creatManor" then
    textArray = textArray .. self:GetHelpButtonStatus("planetary_fortress") .. "\001" .. self:GetHelpButtonStatus("engineering_bay") .. "\001" .. self:GetHelpButtonStatus("tech_lab") .. "\001" .. self:GetHelpButtonStatus("krypton_center") .. "\001"
  elseif name == "upgrade" then
    textArray = textArray .. self:GetHelpButtonStatus("putonEquip") .. "\001" .. self:GetHelpButtonStatus("upgradeEquip") .. "\001" .. self:GetHelpButtonStatus("upgradeTech") .. "\001" .. self:GetHelpButtonStatus("composeKrypton") .. "\001"
  elseif name == "fight" then
    textArray = textArray .. self:GetHelpButtonStatus("explorePlanetary") .. "\001" .. self:GetHelpButtonStatus("achivement") .. "\001" .. self:GetHelpButtonStatus("collect") .. "\001" .. self:GetHelpButtonStatus("getMoney") .. "\001"
  elseif name == "bored" then
    textArray = textArray .. self:GetHelpButtonStatus("everyDayTask") .. "\001" .. self:GetHelpButtonStatus("tryLuck") .. "\001" .. self:GetHelpButtonStatus("fleets") .. "\001" .. self:GetHelpButtonStatus("chat") .. "\001"
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setHelpDetailButtonText", nameArray, textArray)
    self:GetFlashObject():InvokeASCallback("_root", "MoveInHelpDetailButton")
  end
end
function GameUIBarLeft:_getBuildingNameTextByBuildingName(buildingName)
  local key = "LC_MENU_BUILDING_NAME_" .. string.upper(buildingName)
  local tipMsg = GameLoader:GetGameText(key)
  if nil == tipMsg or "" == tipMsg then
    tipMsg = "XXX"
  end
  return tipMsg
end
function GameUIBarLeft:GetHelpButtonStatus(name)
  DebugOut("name = " .. name)
  local modules = GameGlobalData:GetData("modules_status").modules
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  local text = ""
  local status = ""
  local nameText = ""
  local lockText = ""
  if name == "planetary_fortress" then
    status = 1
    nameText = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_PLANETARY_FORTRESS")
    lockText = "empty"
  elseif name == "engineering_bay" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enhance")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_BUILDING_TITLE_ENGINEERING_BAY")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "tech_lab" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("tech")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_BUILDING_TITLE_TECH_LAB")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "krypton_center" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("krypton")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_BUILDING_NAME_KRYPTON_CENTER")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "putonEquip" then
    status = 1
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_5")
    lockText = "empty"
  elseif name == "upgradeEquip" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enhance")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_6")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "upgradeTech" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("tech")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_7")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "composeKrypton" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("krypton")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_8")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "explorePlanetary" then
    status = 1
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_9")
    lockText = "empty"
  elseif name == "achivement" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("arena")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_10")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "collect" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("elite_challenge")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_11")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "getMoney" then
    if GameUtils:IsModuleUnlock("mining") then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_12")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_12_NEW")
    end
    lockText = GameLoader:GetGameText("LC_MENU_DEXTER_CHAPTER")
  elseif name == "everyDayTask" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("task")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_13")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  elseif name == "tryLuck" then
    if GameUtils:IsModuleUnlock("slot") then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_14")
    lockText = GameLoader:GetGameText("LC_MENU_DEXTER_CHAPTER")
  elseif name == "fleets" then
    status = 1
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_15")
    lockText = "empty"
  elseif name == "chat" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("chat")
    if playerLevel >= playerLevelNeed then
      status = 1
    else
      status = 0
    end
    nameText = GameLoader:GetGameText("LC_MENU_ASSISTANT_BUTTON_16")
    lockText = "(" .. string.format(GameLoader:GetGameText("LC_MENU_LEVEL_NAME"), playerLevelNeed) .. ")"
  end
  text = status .. "\002" .. nameText .. "\002" .. lockText .. "\002"
  return text
end
function GameUIBarLeft:getMenuPos(pos)
  if self:GetFlashObject() then
    local pos = self:GetFlashObject():InvokeASCallback("_root", "getMenuItemPos", pos)
    if pos == "" then
      return nil
    end
    return ...
  end
  return nil
end
function GameUIBarLeft:getMoneyPos(...)
  local pos = self:getMenuPos("_root.top_expand.s_2")
  if pos and GameUtils:GetTutorialHelp() then
    GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_19"), GameUIMaskLayer.MaskStyle.small)
  end
end
function GameUIBarLeft:ShowHelpEveryButton(_name)
  DebugOut("ShowHelpEveryButton = " .. _name)
  local pragam = LuaUtils:string_split(_name, "\001")
  local name = pragam[1]
  local status = tonumber(pragam[2])
  DebugOut("ShowHelpEveryButton = " .. name .. " " .. status)
  if name == "planetary_fortress" then
    GameUIMaskLayer:SetIsShow(true)
    GameUIMaskLayer:SetCurrentTutorial(name)
    GameObjectMainPlanet:SetMapOffset()
    self:MoveOutHelpAllButton()
    GameObjectMainPlanet:getPlanetaryFortressPos()
  elseif name == "engineering_bay" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enhance")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
      if buildingInfo.status == 1 or buildingInfo.status == 3 or 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getEngineeringbayPos()
      else
        local buildingName = GameUtils:GetBindingPre("engineering_bay")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "tech_lab" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("tech")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("tech_lab")
      if buildingInfo.status == 1 or buildingInfo.status == 3 or 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getTechlabPos()
      else
        local buildingName = GameUtils:GetBindingPre("tech_lab")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "krypton_center" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("krypton")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("krypton_center")
      if buildingInfo.status == 1 or buildingInfo.status == 3 or 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getKryptoncenterPos()
      else
        local buildingName = GameUtils:GetBindingPre("krypton_center")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "putonEquip" then
    GameUIMaskLayer:SetIsShow(true)
    GameUIMaskLayer:SetCurrentTutorial(name)
    GameObjectMainPlanet:SetMapOffset()
    self:MoveOutHelpAllButton()
    GameUIBarRight:getPutonEquipPos()
  elseif name == "upgradeEquip" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enhance")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("engineering_bay")
      if 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getEngineeringbayPos_action()
      elseif (buildingInfo.status == 1 or buildingInfo.status == 3) and buildingInfo.level == 0 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_enhance_DESC"))
      else
        local buildingName = GameUtils:GetBindingPre("engineering_bay")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "upgradeTech" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("tech")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("tech_lab")
      if 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getUpgradeTechPos()
      elseif (buildingInfo.status == 1 or buildingInfo.status == 3) and buildingInfo.level == 0 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_tech_DESC"))
      else
        local buildingName = GameUtils:GetBindingPre("tech_lab")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "composeKrypton" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("krypton")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("krypton_center")
      if 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getComposeKrypton()
      elseif (buildingInfo.status == 1 or buildingInfo.status == 3) and buildingInfo.level == 0 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_krypton_DESC"))
      else
        local buildingName = GameUtils:GetBindingPre("krypton_center")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "explorePlanetary" then
    GameUIMaskLayer:SetIsShow(true)
    GameUIMaskLayer:SetCurrentTutorial(name)
    self:MoveOutHelpAllButton()
    GameUIBarRight:getExplorePlanetary()
  elseif name == "achivement" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("arena")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    elseif GameUtils:IsModuleUnlock("enhance") then
      GameUIMaskLayer:SetIsShow(true)
      GameUIMaskLayer:SetCurrentTutorial(name)
      self:MoveOutHelpAllButton()
      GameUIBarRight:getPvpPos()
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_arena_DESC"))
    end
  elseif name == "collect" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("elite_challenge")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      local buildingInfo = GameGlobalData:GetBuildingInfo("commander_academy")
      if 0 < buildingInfo.level then
        GameUIMaskLayer:SetIsShow(true)
        GameUIMaskLayer:SetCurrentTutorial(name)
        GameObjectMainPlanet:SetMapOffset()
        self:MoveOutHelpAllButton()
        GameObjectMainPlanet:getCollectPos()
      elseif (buildingInfo.status == 1 or buildingInfo.status == 3) and buildingInfo.level == 0 then
        GameTip:Show(GameLoader:GetGameText("LC_MENU_elite_challenge_DESC"))
      else
        local buildingName = GameUtils:GetBindingPre("commander_academy")
        if nil == buildingName then
          return
        end
        local preBuildingNameText = self:_getBuildingNameTextByBuildingName(buildingName.name)
        GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_NEED_REBUILD_FIRST_ALERT"), preBuildingNameText))
      end
    end
  elseif name == "getMoney" then
    if status == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_1"))
    else
      GameUIMaskLayer:SetIsShow(true)
      GameUIMaskLayer:SetCurrentTutorial(name)
      self:MoveOutHelpAllButton()
      self:getMoneyPos()
    end
  elseif name == "everyDayTask" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("task")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      GameUIMaskLayer:SetIsShow(true)
      GameUIMaskLayer:SetCurrentTutorial(name)
      self:MoveOutHelpAllButton()
      local pos = self:getMenuPos("_root.top_expand.s_1")
      if pos then
        GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_19"), GameUIMaskLayer.MaskStyle.small)
      end
    end
  elseif name == "tryLuck" then
    if status == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_slot_DESC"))
    else
      GameUIMaskLayer:SetIsShow(true)
      GameUIMaskLayer:SetCurrentTutorial(name)
      self:MoveOutHelpAllButton()
      local pos = self:getMenuPos("_root.top_expand.s_2")
      if pos then
        GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_19"), GameUIMaskLayer.MaskStyle.small)
      end
    end
  elseif name == "fleets" then
    GameUIMaskLayer:SetIsShow(true)
    GameUIMaskLayer:SetCurrentTutorial(name)
    self:MoveOutHelpAllButton()
    GameUIBarRight:MoveInRightMenu()
    local pos = GameUIBarRight:getMenuPos("_root.downCorner.v161_points.s_3")
    if pos then
      GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_23"), GameUIMaskLayer.MaskStyle.small)
    end
  elseif name == "chat" then
    local playerLevelNeed = GameDataAccessHelper:GetRightMenuPlayerReqLevel("chat")
    if status == 0 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), playerLevelNeed))
    else
      GameUIMaskLayer:SetIsShow(true)
      GameUIMaskLayer:SetCurrentTutorial(name)
      self:MoveOutHelpAllButton()
      GameUIBarRight:ForceMoveOutRightMenu()
      local pos = self:getMenuPos("_root.chat_bar.s_1")
      if pos then
        GameUtils:ShowMaskLayer(tonumber(pos[1]), tonumber(pos[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_24"), GameUIMaskLayer.MaskStyle.small)
      end
    end
  end
end
function GameUIBarLeft:MoveOutHelpAllButton(...)
  GameUIBarLeft.HelpTutorial = false
  GameAndroidBackManager:RemoveCallback(GameUIBarLeft.OnAndroidBackCustom)
  self:GetFlashObject():InvokeASCallback("_root", "MoveOutHelpAllButton")
end
if AutoUpdate.isAndroidDevice then
  function GameUIBarLeft.OnAndroidBackCustom()
    if GameUIBarLeft.HelpTutorial then
      GameUIBarLeft:MoveOutHelpAllButton()
    end
  end
end
function GameUIBarLeft._asShowEvent()
  if GameUIBarLeft:GetFlashObject() and (GameStateManager:GetCurrentGameState() == GameStateMainPlanet or GameStateManager:GetCurrentGameState() == GameStateManager.GameStateBattleMap) then
    DebugOut("testing data")
    DebugTable(GameUIBarLeft.eventInfo2)
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "showEventBtn", {}, GameUIBarLeft.eventInfo2, GameUIBarLeft.eventLabel)
    GameUIBarLeft:setEventIcon({}, GameUIBarLeft.eventInfo2)
    GameUIBarLeft:GetFlashObject():InvokeASCallback("_root", "SetEventState", GameUIBarLeft.eventHightlight)
  end
end
function GameUIBarLeft.activityEventInfoNtfHandler(content)
  DebugOut("in activityEventInfoNtfHandler")
  DebugTable(content)
  if #content.list >= 0 and content.open_status == 1 then
    local sortFunc = function(a, b)
      return a.id >= b.id
    end
    LuaUtils:BubbleSort(content.list, sortFunc)
    local infoArr = {}
    local infoArr2 = {}
    for i = 1, #content.list do
      local detail = {}
      detail.id = content.list[i].id
      detail.icon = content.list[i].icon
      detail.newsNum = 0
      local strname = "LC_MENU_EVENT_ICON_NAME_" .. content.list[i].icon_name
      detail.strname = GameLoader:GetGameText(strname)
      for j = 1, #content.list[i].all_tasks do
        for k = 1, #content.list[i].all_tasks[j].sub_tasks do
          if content.list[i].all_tasks[j].sub_tasks[k].status == 0 then
            detail.newsNum = detail.newsNum + 1
          end
        end
        for _, pawards in pairs(content.list[i].all_tasks[j].progress_awards) do
          if pawards.status == 0 then
            detail.newsNum = detail.newsNum + 1
          end
        end
      end
      if detail.id > 1000 then
        table.insert(infoArr2, detail)
      else
        table.insert(infoArr, detail)
      end
    end
    for i, v in ipairs(infoArr2) do
      infoArr2.seq = i
    end
    for i, v in ipairs(infoArr) do
      infoArr.seq = i
    end
    GameUIBarLeft.eventInfo = infoArr
    GameUIBarLeft.eventInfo2 = infoArr2
    GameUIBarLeft.eventHightlight = content.gloden_show
    GameUIBarLeft.eventLabel = GameUtils:TryGetText("LC_MENU_EVENT_LABEL_NAME_" .. content.label)
    GameUIBarLeft._asShowEvent()
    GameUIActivityEvent:RefreshMenu(content.list)
  end
end
function GameUIBarLeft:ReduceNewsCnt(eventId, cnt)
  for i = 1, #GameUIBarLeft.eventInfo do
    if GameUIBarLeft.eventInfo[i].id == eventId then
      GameUIBarLeft.eventInfo[i].newsNum = GameUIBarLeft.eventInfo[i].newsNum - cnt
      if GameUIBarLeft.eventInfo[i].newsNum < 0 then
        GameUIBarLeft.eventInfo[i].newsNum = 0
      end
      break
    end
  end
  for i = 1, #GameUIBarLeft.eventInfo2 do
    if GameUIBarLeft.eventInfo2[i].id == eventId then
      GameUIBarLeft.eventInfo2[i].newsNum = GameUIBarLeft.eventInfo2[i].newsNum - cnt
      if 0 > GameUIBarLeft.eventInfo2[i].newsNum then
        GameUIBarLeft.eventInfo2[i].newsNum = 0
      end
      break
    end
  end
  if GameUIBarLeft.eventInfo or GameUIBarLeft.eventInfo2 then
    GameUIBarLeft._asShowEvent()
  end
end
function GameUIBarLeft:setEventIcon(infoArr, infoArr2)
  if #infoArr2 > 0 then
    for i = 1, #infoArr2 do
      local strtxt = string.format("event_icon%d_new.png", i)
      GameUIBarLeft:CheckEventIcon(strtxt, infoArr2[#infoArr2 - i + 1].icon)
    end
  end
end
function GameUIBarLeft:CheckEventIcon(srcIcon, curIcon)
  if curIcon and srcIcon and "" ~= curIcon and "undefined" ~= curIcon then
    curIcon = curIcon .. ".png"
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(curIcon, DynamicResDownloader.resType.WELCOME_PIC)
    DebugOut("extInfo.localPath = " .. localPath)
    if DynamicResDownloader:IfResExsit(localPath) and GameUIBarLeft:GetFlashObject() then
      DebugOut(" replaceEventIcon :", srcIcon, curIcon)
      GameUIBarLeft:GetFlashObject():ReplaceTexture(srcIcon, curIcon)
    else
      local extendInfo = {}
      extendInfo.srcRes = srcIcon
      extendInfo.curRes = curIcon
      DynamicResDownloader:AddDynamicRes(curIcon, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIBarLeft.replaceEventIcon)
    end
  end
end
function GameUIBarLeft.replaceEventIcon(extInfo)
  DebugOut("replaceEventIcon=", extInfo.srcRes, extInfo.curRes)
  if GameUIBarLeft:GetFlashObject() then
    GameUIBarLeft:GetFlashObject():ReplaceTexture(extInfo.srcRes, extInfo.curRes)
  end
end
function GameUIBarLeft:RefreshRedPoint()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetCreditRedPoint", false)
    if GameGlobalData.redPointStates then
      for k, v in pairs(GameGlobalData.redPointStates.states) do
        if (v.key == 100000002 or v.key == 100000013) and v.value == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "SetCreditRedPoint", true)
          break
        end
      end
      self:UpdateLabBtn()
    end
  end
end
function GameUIBarLeft:findNextOnlineItem()
  assert(self.onlineList)
  if #self.onlineList == 0 then
    return
  end
  table.sort(self.onlineList, function(onlineGiftInfoLhs, onlineGiftInfoRhs)
    return onlineGiftInfoLhs.gift_type < onlineGiftInfoRhs.gift_type
  end)
  for i, onlineItem in ipairs(self.onlineList) do
    if 1 == onlineItem.mark then
      return i, onlineItem
    end
  end
  for i, onlineItem in ipairs(self.onlineList) do
    if 0 == onlineItem.mark then
      return i, onlineItem
    end
  end
end
function GameUIBarLeft:ShowActivityIcon()
  if GameStateManager.GameStateMainPlanet ~= GameStateManager:GetCurrentGameState() then
    return
  end
  if not self.onlineList or #self.onlineList == 0 then
    self:GetFlashObject():InvokeASCallback("_root", "HideActivityIcon")
    return
  end
  local idx, item = self:findNextOnlineItem()
  if not idx then
    self:GetFlashObject():InvokeASCallback("_root", "HideActivityIcon")
    return
  end
  local param = {}
  local ltime = item.times_add - os.time()
  if ltime < 0 then
    ltime = 0
  end
  param.left_time = ltime
  self:GetFlashObject():InvokeASCallback("_root", "ShowActivityIcon", param)
end
function GameUIBarLeft:HideActivityIcon()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideActivityIcon")
  end
end
function GameUIBarLeft:Onclick_activityItem(arg)
  local titem = LuaUtils:deserializeTable(arg)
  if titem.item_type == "item" then
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    titem.cnt = titem.no or 1
    ItemBox:showItemBox("ChoosableItem", titem, tonumber(titem.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIBarLeft:OnActiveIcon(ltime)
  local idx, item = self:findNextOnlineItem()
  assert(idx)
  if ltime > 0 then
    local param = {
      tlist = {}
    }
    for i, v in ipairs(item.gifts) do
      local no = v.no or 1
      local type_tmp, number_tmp = GameUIActivity:getSignItemDetailInfo(v.item_type, v.number, v.no)
      local t = {}
      t.strIcon = type_tmp
      t.strname = number_tmp
      if v.item_type == "item" then
        t.strname = "X" .. (v.no or 1)
      elseif v.item_type == "pve_supply" then
        t.strname = "X" .. (v.number or 1)
      end
      t.desc = LuaUtils:serializeTable(v)
      t.num = 0
      table.insert(param.tlist, t)
    end
    self:GetFlashObject():InvokeASCallback("_root", "ShowActivityBox", param)
  else
    local content = {
      type = item.gift_type
    }
    GameUIBarLeft.onlineItem_net = item
    NetMessageMgr:SendMsg(NetAPIList.active_gifts_req.Code, content, GameUIBarLeft.receiveOnlineCallback, true, nil)
  end
end
function GameUIBarLeft.receiveOnlineCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.active_gifts_req.Code then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  elseif msgType == NetAPIList.active_gifts_ntf.Code then
    assert(GameUIBarLeft.onlineItem_net)
    local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
    ItemBox:ShowRewardBox(GameUIBarLeft.onlineItem_net.gifts)
    GameUIBarLeft.onlineItem_net = nil
    GameUIBarLeft.onActiveGiftsNTF(content)
    return true
  end
  return false
end
function GameUIBarLeft.onActiveGiftsNTF(content)
  GameUIBarLeft.onlineList = content.items
  for k, v in pairs(GameUIBarLeft.onlineList) do
    v.times_add = v.times + os.time()
  end
  if not GameUIBarLeft:GetFlashObject() then
    return
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIBarLeft) then
    return
  end
  GameUIBarLeft:ShowActivityIcon()
end
function GameUIBarLeft:DoShowQuestMenu()
  local isJustMainQuest = #GameQuestMenu.normalActivityQuest == 0
  if isJustMainQuest and tonumber(GameQuestMenu.gotoTabTag) == 2 then
    local msg = GameLoader:GetGameText("LC_MENU_CLOSE_QUEST_INFO")
    if nil == msg or "" == msg then
      msg = "activities unavailable"
    end
    DebugOut("show msg")
    GameTip:Show(msg)
    return
  end
  GameQuestMenu:checkIsShowActivityQuest()
  assert(tonumber(GameQuestMenu.gotoTabTag) == 1, tostring(GameQuestMenu.gotoTabTag))
  GameUIBarLeft:RequestReward()
end
function GameUIBarLeft:OnFocusLost(newState)
  GameUIBarLeft:HideActivityIcon()
end
function GameUIBarLeft:RequestReward()
  local quest = GameGlobalData:GetData("user_quest")
  local param = {
    quest_id = quest.quest_id
  }
  DebugOut("quest_id: ", quest.quest_id)
  GameUIBarLeft.quest_loot = quest
  NetMessageMgr:SendMsg(NetAPIList.quest_loot_req.Code, param, GameUIBarLeft.RequestRewardCallback, true)
  if immanentversion == 1 then
    if quest.quest_id == 1 and self.receivedQuest1Reward == nil then
      AddFlurryEvent("ClickCityHallUpgradeCompleteQuest", {}, 1)
      self.receivedQuest1Reward = true
    elseif quest.quest_id == 2 and self.receivedQuest2Reward == nil then
      AddFlurryEvent("ClickScoutCaptainQuestCompleteQuest", {}, 1)
      self.receivedQuest2Reward = true
    elseif quest.quest_id == 3 and self.receivedQuest3Reward == nil then
      AddFlurryEvent("ClickPirateFlagshipQuestCompleteQuest", {}, 1)
      self.receivedQuest3Reward = true
    elseif quest.quest_id == 4 and self.receivedQuest4Reward == nil then
      AddFlurryEvent("ClickRebuildEngineerHubQuestCompleteQuest", {}, 1)
      self.receivedQuest4Reward = true
    elseif quest.quest_id == 5 and self.receivedQuest5Reward == nil then
      AddFlurryEvent("ClickFleetEnhanceQuestCompleteQuest", {}, 1)
      self.receivedQuest5Reward = true
    elseif quest.quest_id == 6 and self.receivedQuest6Reward == nil then
      AddFlurryEvent("ClickRebuildAcameyQuestCompleteQuest", {}, 1)
      self.receivedQuest6Reward = true
    end
  elseif immanentversion == 2 then
    if quest.quest_id <= 9 then
      AddFlurryEvent("QuestFinish00" .. quest.quest_id, {}, 2)
    elseif quest.quest_id <= 28 then
      AddFlurryEvent("QuestFinish0" .. quest.quest_id, {}, 2)
    end
  end
end
function GameUIBarLeft.RequestRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.quest_loot_req.Code then
    DebugOut("RequestRewardCallback msgType: ", msgType, " content: ", content.api, content.code)
    if content.code ~= 0 then
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    else
      if QuestTutorialGetQuestReward:IsActive() then
        QuestTutorialGetQuestReward:SetFinish(true)
        if immanentversion == 2 then
          if not QuestTutorialTax:IsActive() and not QuestTutorialTax:IsFinished() then
            QuestTutorialTax:SetActive(true)
          end
        elseif immanentversion == 1 then
          QuestTutorialBattleMap:SetActive(true)
          QuestTutorialFirstBattle:SetActive(true)
          GameUIBarRight:ChechNeedShowTutorialBattleMap()
          GameUIBarLeft:CheckNeedShowQuestTutorial()
        end
      end
      local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
      ItemBox:ShowRewardBox(GameUIBarLeft.quest_loot.loot)
    end
    return true
  end
  return false
end
