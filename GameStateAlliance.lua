local GameStateAlliance = GameStateManager.GameStateAlliance
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameUIUnalliance = LuaObjectManager:GetLuaObject("GameUIUnalliance")
local GameUIAllianceList = LuaObjectManager:GetLuaObject("GameUIAllianceList")
local GameUIAllianceInfo = LuaObjectManager:GetLuaObject("GameUIAllianceInfo")
local AllianceApplyStatus = {}
AllianceApplyStatus.apply = 1
AllianceApplyStatus.cancel = 2
AllianceApplyStatus.disable = 3
GameStateAlliance.AllianceApplyStatus = AllianceApplyStatus
function GameStateAlliance:InitGameState()
  GameGlobalData:RegisterDataChangeCallback("alliance", function()
    if GameStateManager:GetCurrentGameState() == GameStateAlliance then
      GameStateAlliance:CheckUserAllianceStatus()
      if GameStateAlliance:IsObjectInState(GameUIUserAlliance) and GameUIUserAlliance:GetActiveTab() == "member" then
        GameUIUserAlliance:UpdateMembersList(0)
      end
    end
  end)
  GameUIUnalliance:RegisterJoinAllianceAward()
end
function GameStateAlliance:OnFocusGain(previousState)
  self.previousState = previousState
  self:CheckUserAllianceStatus()
end
function GameStateAlliance.onBufferChange(content)
  GameStateAlliance.dominationBuffer = content.icon
end
function GameStateAlliance:OnFocusLost()
  self:EraseObject(GameUIUnalliance)
  self:EraseObject(GameUIUserAlliance)
end
function GameStateAlliance:CheckApplyStatus(index_alliance)
  for _, v in ipairs(GameStateAlliance.AppliedAlliancesList) do
    if v == index_alliance then
      return AllianceApplyStatus.cancel
    end
  end
  if #GameStateAlliance.AppliedAlliancesList < 3 then
    return AllianceApplyStatus.apply
  else
    return AllianceApplyStatus.disable
  end
  assert(false)
end
function GameStateAlliance:Quit()
  GameStateManager:SetCurrentGameState(self.previousState)
end
function GameStateAlliance:UpdateAllianceListInfo(content)
  self.numTotalAlliances = content.total
  self.AlliancesList = content.alliances
  self.AppliedAlliancesList = content.applied_alliances
  self._CurrentAlliancesPage = content.index
end
function GameStateAlliance:UpdateAlliancesData(content)
  self.numTotalAlliances = content.total
  self.AlliancesList = content.alliances
  self.AppliedAlliancesList = content.applied_alliances
  self._CurrentAlliancesPage = content.index
end
function GameStateAlliance:CheckAlliancesData()
  if LuaUtils:table_empty(self.AlliancesList) and self._CurrentAlliancesPage ~= 0 then
    return false
  end
  return true
end
function GameStateAlliance:CheckUserAllianceStatus()
  local user_alliance_status = GameGlobalData:GetData("alliance")
  if user_alliance_status.id == "" then
    self:EraseObject(GameUIUserAlliance)
    if not self:IsObjectInState(GameUIUnalliance) then
      self:AddObject(GameUIUnalliance)
    end
  else
    self:EraseObject(GameUIUnalliance)
    if not self:IsObjectInState(GameUIUserAlliance) then
      self:AddObject(GameUIUserAlliance)
    end
  end
  self:ForceCompleteCammandList()
end
function GameStateAlliance.AllianceInfoNotifyHandler(content)
  local data_alliance = GameGlobalData:GetData("alliance")
  data_alliance.id = content.alliance.id
  data_alliance.position = content.alliance.position
  GameGlobalData:RefreshData("alliance")
end
