local GameUIOfflineReward = LuaObjectManager:GetLuaObject("GameUIOfflineReward")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameObjectLevelUp = LuaObjectManager:GetLuaObject("GameObjectLevelUp")
local GameGlobalData = GameGlobalData
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
function GameUIOfflineReward:OnAddToGameState(state)
  self:UpdateData(true)
  GameUIOfflineReward:MoveIn()
end
function GameUIOfflineReward:ShowOfflineReward()
  self:LoadFlashObject()
  GameStateManager:GetCurrentGameState():AddObject(GameUIOfflineReward)
end
function GameUIOfflineReward:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIOfflineReward:UpdateData(forceUpdateExpPercent)
  local data_offline = GameGlobalData:GetData("offline_info")
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  local strTime = GameUtils:formatTimeStringByMinutes(data_offline.minute)
  local strTimeMax = GameUtils:formatTimeStringByMinutes(data_offline.max_minute)
  flash_obj:SetText("_main.contents._textTime", strTime .. "/" .. strTimeMax)
  flash_obj:SetText("_main.contents._textExp", tostring(data_offline.expr_per_minute))
  local enableReceiveReward = false
  if data_offline.minute > 0 then
    enableReceiveReward = true
  end
  flash_obj:SetBtnEnable("_main.btnReceive", enableReceiveReward)
  self:UpdateExpPercent(forceUpdateExpPercent)
end
function GameUIOfflineReward:UpdateExpPercent(forceUpdate)
  local data_offline = GameGlobalData:GetData("offline_info")
  local percent = math.floor(data_offline.minute / data_offline.max_minute * 100)
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "lua2fs_updateExpPercentBar", percent, forceUpdate)
end
function GameUIOfflineReward:SetData()
  local offline_reward_data = GameGlobalData.GlobalData.offline_info
  if offline_reward_data then
    local offline_time_string = tostring(GameUtils:formatTimeStringByMinutes(offline_reward_data.minute))
    local max_time_string = tostring(GameUtils:formatTimeStringByMinutes(offline_reward_data.max_minute))
    local exp_string = tostring(offline_reward_data.expr_per_minute * offline_reward_data.minute)
    local exp_percent = math.floor(100 * offline_reward_data.minute / offline_reward_data.max_minute)
    if exp_percent > 100 then
      exp_percent = 100
    elseif exp_percent < 0 then
      exp_percent = 0
    end
    GameUIOfflineReward:GetFlashObject():InvokeASCallback("_root", "SetData", max_time_string, offline_time_string, exp_string, exp_percent)
  end
end
function GameUIOfflineReward:MoveIn()
  GameUIOfflineReward:UpdateData()
  GameUIOfflineReward:GetFlashObject():InvokeASCallback("_root", "lua2fs_playAnimationMoveIn")
end
function GameUIOfflineReward:MoveOut()
  GameUIOfflineReward:GetFlashObject():InvokeASCallback("_root", "lua2fs_playAnimationMoveOut")
end
function GameUIOfflineReward.CollectOfflineRewardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.user_collect_offline_expr_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUIOfflineReward:UpdateData(false)
      GameUIBarLeft:HideOfflineRewardIcon()
    end
    return true
  end
  return false
end
function GameUIOfflineReward:OnFSCommand(cmd, arg)
  if cmd == "view_vip" then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:showVip()
    return
  end
  if cmd == "on_close" then
    self:MoveOut()
    return
  end
  if cmd == "receive_reward" then
    NetMessageMgr:SendMsg(NetAPIList.user_collect_offline_expr_req.Code, nil, self.CollectOfflineRewardCallback, false, nil)
    return
  end
  if cmd == "moveout_animation_over" then
    GameStateManager:GetCurrentGameState():EraseObject(GameUIOfflineReward)
    return
  end
end
function GameUIOfflineReward:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "lua2fs_onUpdateFrame", dt)
end
