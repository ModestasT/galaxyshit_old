local GameFleetNewEnhance = LuaObjectManager:GetLuaObject("GameFleetNewEnhance")
local GameStateFleetInfo = GameStateManager.GameStateFleetInfo
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameFleetInfoUI = LuaObjectManager:GetLuaObject("GameFleetInfoUI")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameStateEquipEnhance = GameStateManager.GameStateEquipEnhance
local QuestTutorialEquip = TutorialQuestManager.QuestTutorialEquip
local QuestTutorialEnhance = TutorialQuestManager.QuestTutorialEnhance
local QuestTutorialEnhance_second = TutorialQuestManager.QuestTutorialEnhance_second
local QuestTutorialEquipAllByOneKey = TutorialQuestManager.QuestTutorialEquipAllByOneKey
local QuestTutorialsChangeFleets1 = TutorialQuestManager.QuestTutorialsChangeFleets1
local QuestTutorialEnhance_third = TutorialQuestManager.QuestTutorialEnhance_third
local QuestTutorialFirstReform = TutorialQuestManager.QuestTutorialFirstReform
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIFleetGrowUp = LuaObjectManager:GetLuaObject("GameUIFleetGrowUp")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local QuestTutorialBuildAcademy = TutorialQuestManager.QuestTutorialBuildAcademy
local QuestTutorialBattleMap = TutorialQuestManager.QuestTutorialBattleMap
local QuestTutorialEquipmentEvolution = TutorialQuestManager.QuestTutorialEquipmentEvolution
local GameUIAdjutant = LuaObjectManager:GetLuaObject("GameUIAdjutant")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local QuestTutorialBattleMapAfterRecruit = TutorialQuestManager.QuestTutorialBattleMapAfterRecruit
local TutorialAdjutantManager = LuaObjectManager:GetLuaObject("TutorialAdjutantManager")
local GameUIMaskLayer = LuaObjectManager:GetLuaObject("GameUIMaskLayer")
local FacebookPopUI = LuaObjectManager:GetLuaObject("FacebookPopUI")
local GameUIMaster = LuaObjectManager:GetLuaObject("GameUIMaster")
local GameUIStarSystemPort = LuaObjectManager:GetLuaObject("GameUIStarSystemPort")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
require("FleetMatrix.tfl")
local k_maxSlot = 5
local k_subStateFleetView = 1
local k_subStateFleetEquipment = 2
local k_subStateFleetEnhance = 3
local k_subStateFleetEvolution = 4
local k_subStateFleetStrengthen = 5
local k_subStateFleetIntervene = 6
local k_subStateFleetKrypton = 7
local k_fleetStackPosLeft2 = 1
local k_fleetStackPosLeft1 = 2
local k_fleetStackPosMid = 3
local k_fleetStackPosRight1 = 4
local k_fleetStackPosRight2 = 5
local k_fleetFlashPosMid = 1
local k_fleetFlashPosLeft = 2
local k_fleetFlashPosRight = 3
local k_fleetFlashPosOut = 4
local k_fleetSwitchDirectionPrev = -1
local k_fleetSwitchDirectionStay = 0
local k_fleetSwitchDirectionNext = 1
local k_equipmentInfoTypeLoad = 1
local k_equipmentInfoTypeUnload = 2
local k_equipmentInfoTypeBuy = 3
local k_equipmentInfoTypeStrengthen = 4
local MAX_LINE = 3
local LINE_COUNT = 3
local BAG_OWNER_USER = 1
local BAG_OWNER_FLEET = 2
GameFleetNewEnhance.isDragEquipedItem = false
GameFleetNewEnhance.isUnloadEquipedItem = false
GameFleetNewEnhance.isNeedShowEnhanceUI = false
local waitForRefreshCount = 0
local startQuickEquipment = false
local startQuickUnload = false
local currentJumpBtnStatus = 0
local justEquipItem = {}
justEquipItem.slot = -1
justEquipItem.isEquip = false
local currentShowBluePrint = 0
local curMatrixIndex = 0
GameFleetNewEnhance.newSelectedFleetID = nil
GameFleetNewEnhance.activeNewSelectedFleet = false
GameFleetNewEnhance.mDefaultFleetIdx = -1
GameFleetNewEnhance.mDefaultSlot = -1
GameFleetNewEnhance.allFleetGrowUpInfo = {}
GameFleetNewEnhance.art_canupgrade = 0
GameFleetNewEnhance.AllheroList = {}
GameFleetNewEnhance.StrenthenList = {}
GameFleetNewEnhance.StrenthenWeight = {}
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
GameFleetNewEnhance.GrowUpState = {
  unlocked = 0,
  locked = 1,
  can = 2
}
GameFleetNewEnhance.AdjutantButtoState = {
  unlocked = 0,
  locked = 1,
  can = 2,
  have = 3
}
GameFleetNewEnhance.mlastReceivedEnhanceInfo = nil
local stat_medal
function GameFleetNewEnhance:OnInitGame()
  self.equipOffDestSlot = -1
  self.equipOffSrcSlot = -1
  self.equipDestSlot = -1
  self.equipSrcSlot = -1
  GameGlobalData:RegisterDataChangeCallback("equipments", GameFleetNewEnhance.OnGlobalEquipmentsChange)
  GameGlobalData:RegisterDataChangeCallback("levelinfo", GameFleetNewEnhance.OnGlobalLevelChange)
  GameGlobalData:RegisterDataChangeCallback("cdtimes", GameFleetNewEnhance.OnEnhanceCDTimeChange)
  GameGlobalData:RegisterDataChangeCallback("fleetinfo", GameFleetNewEnhance.OnEnhanceFleetinfoChange)
  GameGlobalData:RegisterDataChangeCallback("resource", GameFleetNewEnhance.OnGlobalResourceChange)
  GameGlobalData:RegisterDataChangeCallback("fleetkryptons", GameFleetNewEnhance.RefreshKrypton)
  GameGlobalData:RegisterDataChangeCallback("leaderlist", GameFleetNewEnhance.refreshFleets)
  GameGlobalData:RegisterDataChangeCallback("matrix", GameFleetNewEnhance.matrixUpdate)
  GameGlobalData:RegisterDataChangeCallback("FleetMedalRedPoint", GameFleetNewEnhance.OnFleetMedalNewsChange)
  GameGlobalData:RegisterDataChangeCallback("FleetMedalButtonRedPoint", GameFleetNewEnhance.SetMedalButtonNews)
  self:Clear()
  GameTimer:Add(self._EnhanceCDTimeTimer, 1000)
end
function GameFleetNewEnhance:Init()
  curMatrixIndex = GameGlobalData:GetData("matrix").id
  self.m_currentSelectFleetIndex = GameFleetNewEnhance.mDefaultFleetIdx
  self.m_currentSelectEquipSlot = GameFleetNewEnhance.mDefaultSlot
  self.m_currentSelectEquipSlot = math.max(self.m_currentSelectEquipSlot, 1)
  self.m_currentSelectFleetIndex = math.max(self.m_currentSelectFleetIndex, 1)
  self.m_subState = math.max(self.m_subState, k_subStateFleetView)
  self.all_need_equip = {}
  self.OnGlobalResourceChange()
  self:UpdateSelectedFleetInfo(self.m_currentSelectFleetIndex, true, k_fleetSwitchDirectionStay)
  DebugOut("GameStateEquipEnhance:IsEnhanceEnable(): ", GameStateEquipEnhance:IsEnhanceEnable())
  if GameStateEquipEnhance:IsEnhanceEnable() then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhance", true)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhance", false)
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", false)
  self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", false)
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if maxFleetIndex <= self.m_currentSelectFleetIndex then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", true)
    end
    if self.m_currentSelectFleetIndex <= 1 then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", true)
    end
  end
  local commanderAcademy = GameGlobalData:GetBuildingInfo("commander_academy")
  local disableEvolution = commanderAcademy.level <= 0
  self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", false, GameFleetNewEnhance.m_subState, disableEvolution, GameFleetNewEnhance.tab_content_tab2_text)
  self:CheckEnableQuckEnhance()
  self:CheckCanUnloadAll()
  self:SetSubState(k_subStateFleetView)
  self:lockOrUnlockEvolutionBtnAccordUseable()
  self:lockOrUnlockQuickEnhanceBtnAccordUseable()
  self:CheckDownloadImage()
  self:CheckKryptonShow()
  self:CheckFormationShow()
  self:CheckMedalButton(true)
end
function GameFleetNewEnhance:FinsihDialogStarSytem()
  GameUICommonDialog:PlayStory({9818})
end
function GameFleetNewEnhance.requestMultiMatrixCallBack()
  local flash_obj = GameFleetNewEnhance:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "initMartix", FleetMatrix.matrixs_in_as, curMatrixIndex)
    if not GameFleetNewEnhance.isAskKryptonRedPoint then
      GameFleetNewEnhance:RefreshKryptonRedPoint()
    end
  end
end
GameFleetNewEnhance.needUpdateFleet = true
function GameFleetNewEnhance:OnAddToGameState()
  GameFleetNewEnhance.StrenthenList = {}
  GameFleetNewEnhance.StrenthenWeight = {}
  if not GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:LoadFlashObject()
  end
  GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "initText", GameLoader:GetGameText("LC_MENU_Level"))
  GameFleetNewEnhance:Init()
  GameFleetNewEnhance.needUpdateFleet = false
  curMatrixIndex = nil
  FleetMatrix:GetMatrixsReq(GameFleetNewEnhance.requestMultiMatrixCallBack, FleetMatrix.Matrix_Type)
  GameFleetNewEnhance.tab_content_tab2_text = GameLoader:GetGameText("LC_MENU_EVOLUTION_BUTTON")
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
end
function GameFleetNewEnhance.matrixUpdate()
  if GameFleetNewEnhance.newSelectedFleetID then
    GameFleetNewEnhance.activeNewSelectedFleet = true
  end
end
function GameFleetNewEnhance:requestArtUpgradeEnable(fleetid)
end
function GameFleetNewEnhance:requestArtUpgradeEnableCallback(msgType, content)
end
function GameFleetNewEnhance:setArtifactLockStatus(locked)
  self:GetFlashObject():InvokeASCallback("_root", "setArtifactLockStatus", true)
end
function GameFleetNewEnhance:InitOnMatrixChange()
  local hasEquip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  DebugOut("hasEquip:", hasEquip, self.m_currentSelectEquipSlot)
  if hasEquip and self.isNeedShowEnhanceUI then
    self:SetSubState(k_subStateFleetEnhance)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowPopEquipmentMenu", false)
    self:GetFlashObject():InvokeASCallback("_root", "SetEnhanceUIOpen", true)
    self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
  end
  GameFleetNewEnhance:CheckDownloadImage()
  GameFleetNewEnhance:GetAllFleetGrowUpDate()
  GameFleetNewEnhance:GetAllFleetData()
end
function GameFleetNewEnhance:CheckDownloadImage()
  if not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_newEnhance_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_newEnhance_bg.png", "territorial_map_bg.png")
  end
end
function GameFleetNewEnhance:OnEraseFromGameState()
  FacebookPopUI.mLogInAwardCallback = nil
  GameFleetNewEnhance.isAskKryptonRedPoint = nil
  self:Clear()
  self:UnloadFlashObject()
  FleetMatrix:SwitchMatrixReq(FleetMatrix.cur_index, function()
    curMatrixIndex = nil
  end, true)
  FleetMatrix.Matrix_Type = 0
  GameGlobalData:RemoveDataChangeCallback("resource", self.RefreshResource)
end
function GameFleetNewEnhance:CheckKryptonShow()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local unlock = false
  for k, v in pairs(rightMenu) do
    if v.name == "krypton" then
      unlock = v.status
      break
    end
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setKryptonShow", unlock)
  end
end
function GameFleetNewEnhance:CheckFormationShow()
  local rightMenu = GameGlobalData:GetData("modules_status").modules
  local unlock = false
  for k, v in pairs(rightMenu) do
    if v.name == "hire" then
      unlock = v.status
      break
    end
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setFormationShow", unlock)
  end
end
function GameFleetNewEnhance:ShowAdjutantTutorial()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowAdjutantTutorial")
  end
end
function GameFleetNewEnhance:HideAdjutantTutorial()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HideAdjutantTutorial")
  end
end
function GameFleetNewEnhance:Clear()
  DebugOut("self.m_currentSelectBagSlot in clear = ", self.m_currentSelectBagSlot)
  self.m_currentSelectBagSlot = -1
  self.m_currentSelectEquipSlot = -1
  self.m_currentSelectFleetIndex = -1
  self.m_optionEquipItemId = -1
  self.m_switchDirection = k_fleetSwitchDirectionStay
  self.m_subState = -1
  self.m_curFleetEquipments = {}
  self.m_fleetOnShowingStack = {}
  self.AllheroList = {}
  self.allFleetGrowUpInfo = {}
  self.art_canupgrade = 0
  self.isMedalSlot = false
end
function GameFleetNewEnhance:SetSubState(subState)
  DebugOut("GameFleetNewEnhance:SetSubState", subState, QuestTutorialEquip:IsActive(), QuestTutorialEnhance:IsActive(), QuestTutorialEquipAllByOneKey:IsActive(), QuestTutorialsChangeFleets1:IsActive(), QuestTutorialEnhance_third:IsActive())
  self.m_subState = subState
  if not self.onlyEquip then
    GameFleetNewEnhance:GetOnlyEquip()
  end
  if QuestTutorialEquip:IsActive() then
    if self.onlyEquip[1] ~= nil then
      if self.m_subState == k_subStateFleetView then
        DebugOut("set show 111111")
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
      end
      if self.m_subState == k_subStateFleetEquipment then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
      end
    elseif TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    else
      GameFleetNewEnhance:ShowCloseTip()
    end
  end
  if QuestTutorialEnhance:IsActive() then
  end
  if QuestTutorialEquipAllByOneKey:IsActive() and self.m_subState == k_subStateFleetView then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowTipAutoEquipment", true)
  end
  if (QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEnhance_third:IsActive()) and not QuestTutorialEquip:IsActive() then
    if self.m_subState == k_subStateFleetView then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", true)
    end
  elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not QuestTutorialEquip:IsActive() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    if self.m_subState == k_subStateFleetView and 4 ~= fleetId then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", false)
    end
  end
end
function GameFleetNewEnhance:CheckEnableQuckEnhance()
  if GameVip:IsFastEnhanceUnlocked() or Facebook:IsBindFacebook() then
    DebugOut("GameFleetNewEnhance:CheckEnableQuckEnhance unlocked")
    self:GetFlashObject():InvokeASCallback("_root", "SetEnableQuickEnhance", true)
  else
    DebugOut("GameFleetNewEnhance:CheckEnableQuckEnhance locked")
    self:GetFlashObject():InvokeASCallback("_root", "SetEnableQuickEnhance", true)
  end
end
function GameFleetNewEnhance.CheckUnlockEnhaceall()
  GameFleetNewEnhance:CheckEnableQuckEnhance()
  GameFleetNewEnhance:lockOrUnlockQuickEnhanceBtnAccordUseable()
end
function GameFleetNewEnhance:GetFleet(index)
  if GameGlobalData:GetData("fleetinfo").fleets then
    return GameGlobalData:GetData("fleetinfo").fleets[index]
  end
  return nil
end
function GameFleetNewEnhance:FleetInMatrix(fleetid)
  local matrix = GameGlobalData:GetData("matrix").cells
  DebugOut("matrix::")
  DebugTable(matrix)
  for k, v in pairs(matrix or {}) do
    if v.fleet_identity == fleetid then
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:UpdateSelectedFleetInfo(fleetIndex, forceRefresh, switchDirection)
  if GameFleetNewEnhance.needUpdateFleet == false then
    GameFleetNewEnhance.needUpdateFleet = true
    return false
  end
  DebugOut("UpdateSelectedFleetInfo:", fleetIndex, ",", forceRefresh, ",", switchDirection)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if fleets ~= nil then
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if GameFleetNewEnhance.newSelectedFleetID and GameFleetNewEnhance.activeNewSelectedFleet then
      for k, v in ipairs(fleets) do
        if GameFleetNewEnhance.newSelectedFleetID == v.identity then
          fleetIndex = k
          break
        end
      end
      GameFleetNewEnhance.newSelectedFleetID = nil
      GameFleetNewEnhance.activeNewSelectedFleet = false
    end
    if fleetIndex > 0 and maxFleetIndex >= fleetIndex then
      self.m_switchDirection = switchDirection or k_fleetSwitchDirectionStay
      if self:GetFlashObject() ~= nil then
        self:GetFlashObject():InvokeASCallback("_root", "SetSelectedFleetId", fleetIndex, maxFleetIndex)
      end
      local content = fleets[fleetIndex]
      self.m_currentFleetsId = content.id
      self.m_curreetFleetsForce = content.force
      if self.m_curreetFleetsForce then
        self:GetFlashObject():InvokeASCallback("_root", "setForceText", GameUtils.numberConversion(self.m_curreetFleetsForce))
      end
      self:FleetSelect(fleetIndex, forceRefresh)
      self.m_currentSelectFleetIndex = fleetIndex
      self:UpdateFleetShowStack(switchDirection)
      self:UpdateFleetShow(switchDirection)
      DebugTable(content)
      DebugOut("fleet_id:", self.m_currentFleetsId, ",", content.identity)
      TutorialAdjutantManager:StartTutorialAdjutantBind_NewEnhance(content.identity)
      if (immanentversion170 == 4 or immanentversion170 == 5) and QuestTutorialFirstReform:IsActive() then
        local fleets = GameGlobalData:GetData("fleetinfo").fleets
        local fleetId = fleets[self.m_currentSelectFleetIndex].identity
        DebugOut("asjdjasdkjajksdjkadjk:", fleetId)
        if self.m_subState == k_subStateFleetView and 1 ~= fleetId then
          DebugOut("hjasjdhadajjhaaaaa")
          self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevTip", true)
          self:GetFlashObject():InvokeASCallback("_root", "ShowReformTutorial", false)
        else
          self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevTip", false)
          self:GetFlashObject():InvokeASCallback("_root", "ShowReformTutorial", true)
        end
      end
      GameFleetNewEnhance.art_canupgrade = false
      GameFleetNewEnhance:UpdateGrowUpState(content.identity)
      DebugOut("xixixixixi")
      local currentState = self:GetArtUpgradeState()
      self:RefreshKryptonRedPoint()
      self:GetFlashObject():InvokeASCallback("_root", "setArtifactButtonState", currentState)
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "setShipBtnShow", GameFleetNewEnhance:FleetInMatrix(content.identity))
      end
      GameFleetNewEnhance:UpdateAdjutantState(content.identity, content.cur_adjutant)
      GameFleetNewEnhance.RefreshKrypton()
      GameFleetNewEnhance:RefreshKryptonRedPoint()
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:UpdateAdjutantState(fleetid, curAdjutant)
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(fleetid)
  if curAdjutantButton == GameFleetNewEnhance.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(curAdjutant)
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, GameFleetNewEnhance.DownloadRankImageCallback)
    end
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
    if self:FleetInMatrix(fleetid) then
      DebugOut("enheng?", curAdjutantButton)
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == GameFleetNewEnhance.AdjutantButtoState.can and not self:IsMaxAdjutantNow())
    else
      self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
    end
  end
end
function GameFleetNewEnhance:IsMaxAdjutantNow()
  local adjutantUnlockCnt = GameGlobalData:GetData("adjutant_max_count")
  local nowAdjutantNum = 0
  local fleet_info = GameGlobalData:GetData("fleetinfo").fleets
  for k, v in ipairs(fleet_info) do
    if GameUtils:IsInMatrix(v.identity) and 0 < v.cur_adjutant then
      nowAdjutantNum = nowAdjutantNum + 1
    end
  end
  DebugOut("IsMaxAdjutantNow", nowAdjutantNum, adjutantUnlockCnt)
  return nowAdjutantNum == adjutantUnlockCnt
end
function GameFleetNewEnhance:UpdateGrowUpState(fleetid)
  local curGrowUpState = self:GetFleetGrowUpState(fleetid)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setGrowUpButtonState", curGrowUpState)
    if self:isCurFleetInMatrix() then
      self:GetFlashObject():InvokeASCallback("_root", "setGrowupRedpoint", curGrowUpState == GameFleetNewEnhance.GrowUpState.can)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setGrowupRedpoint", false)
    end
  end
end
function GameFleetNewEnhance.DownloadRankImageCallback(extInfo)
  DebugOut("xxxxx = ")
  DebugTable(extinfo)
  if GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
function GameFleetNewEnhance:GetAllFleetGrowUpDate(...)
  NetMessageMgr:SendMsg(NetAPIList.can_enhance_req.Code, nil, GameFleetNewEnhance.AllFleetGrowUpCallback, true, nil)
end
function GameFleetNewEnhance.AllFleetGrowUpCallback(msgType, content)
  if msgType == NetAPIList.can_enhance_ack.Code then
    DebugOut("AllFleetGrowUpCallback = ")
    DebugTable(content)
    GameFleetNewEnhance.allFleetGrowUpInfo = content.fleet_info
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].identity
    GameFleetNewEnhance:UpdateGrowUpState(fleetId)
    return true
  end
  return false
end
function GameFleetNewEnhance:GetArtUpgradeStatusUpdate(fleetid)
  local param = {}
  param.fleet_id = fleetid
  NetMessageMgr:SendMsg(NetAPIList.art_can_upgrade_req.Code, param, GameFleetNewEnhance.GetArtUpgradeStatusUpdateCallback, true, nil)
end
function GameFleetNewEnhance.GetArtUpgradeStatusUpdateCallback(msgType, content)
  if msgType == NetAPIList.art_can_upgrade_ack.Code then
    GameFleetNewEnhance.art_canupgrade = content.can_upgrade
    return true
  end
  return false
end
function GameFleetNewEnhance:GetFleetGrowUpState(identity)
  local currentState = ""
  local playerLevelInfo = GameGlobalData:GetData("levelinfo")
  if immanentversion170 == nil then
    if playerLevelInfo.level < 35 then
      currentState = GameFleetNewEnhance.GrowUpState.unlocked
      return currentState
    end
  elseif (immanentversion170 == 4 or immanentversion170 == 5) and not GameUtils:IsModuleUnlock("reform") then
    currentState = GameFleetNewEnhance.GrowUpState.unlocked
    return currentState
  end
  for _, v in pairs(self.allFleetGrowUpInfo) do
    if v.identity == identity then
      if v.can_enhance then
        currentState = GameFleetNewEnhance.GrowUpState.can
        break
      end
      currentState = GameFleetNewEnhance.GrowUpState.locked
      break
    end
  end
  DebugOut("GetFleetGrowUpState = " .. currentState)
  return currentState
end
function GameFleetNewEnhance:GetMedalBtnState()
  return stat_medal
end
function GameFleetNewEnhance:GetArtUpgradeState()
  local currentState = ""
  local playerLevelInfo = GameGlobalData:GetData("levelinfo")
  DebugOut("playerLevelInfo.level:" .. playerLevelInfo.level)
  if GameGlobalData:GetModuleStatus("artifact") == true then
    DebugOut("GetModuleStatus:true")
  else
    DebugOut("GetModuleStatus:false")
  end
  if playerLevelInfo.level < 90 or GameGlobalData:GetModuleStatus("artifact") == false then
    currentState = GameFleetNewEnhance.GrowUpState.unlocked
    return currentState
  else
    currentState = GameFleetNewEnhance.GrowUpState.locked
  end
  if self.art_canupgrade == 1 then
    DebugOut("DebugOut:art_canupgrade:" .. self.art_canupgrade)
    currentState = GameFleetNewEnhance.GrowUpState.can
  end
  return currentState
end
function GameFleetNewEnhance:GetAllFleetData(...)
  NetMessageMgr:SendMsg(NetAPIList.fleet_dismiss_req.Code, nil, GameFleetNewEnhance.AllFleetDataCallback, true, nil)
end
function GameFleetNewEnhance.AllFleetDataCallback(msgType, content)
  if NetAPIList.fleet_dismiss_ack.Code == msgType then
    DebugOut("AllFleetDataCallback:")
    local list = GameGlobalData:GetData("fleetinfo").fleets
    for k = 1, #list do
      table.insert(GameFleetNewEnhance.AllheroList, list[k])
    end
    if content.fleets and #content.fleets > 0 then
      for k = 1, #content.fleets do
        local isRepeat = false
        for index = 1, #list do
          if content.fleets[k].identity == list[index].identity then
            isRepeat = true
            break
          end
        end
        if not isRepeat then
          table.insert(GameFleetNewEnhance.AllheroList, content.fleets[k])
        end
      end
    end
    DebugTable(GameFleetNewEnhance.AllheroList)
    local fleet = list[GameFleetNewEnhance.m_currentSelectFleetIndex]
    GameFleetNewEnhance:UpdateAdjutantState(fleet.identity, fleet.cur_adjutant)
    return true
  end
  return false
end
function GameFleetNewEnhance:GetAdjutantButtonState(identity)
  local currentState = ""
  local playerLelevinfo = GameGlobalData:GetData("levelinfo")
  if playerLelevinfo.level < 45 then
    currentState = GameFleetNewEnhance.AdjutantButtoState.unlocked
    return currentState
  end
  if self.AllheroList == nil then
    DebugOut("error : AllheroList is nil")
  end
  for _, v in pairs(self.AllheroList) do
    if v.identity == identity then
      if v.cur_adjutant > 1 then
        currentState = GameFleetNewEnhance.AdjutantButtoState.have
        break
      end
      if #v.can_adjutant > 0 and GameFleetNewEnhance:GetAdjutantStateFr(v.can_adjutant) then
        currentState = GameFleetNewEnhance.AdjutantButtoState.can
      else
        currentState = GameFleetNewEnhance.AdjutantButtoState.locked
      end
      if GameFleetNewEnhance:GetMajorId(identity) then
        currentState = GameFleetNewEnhance.AdjutantButtoState.locked
      end
      break
    end
  end
  DebugOut("GetAdjutantButtonState = " .. currentState)
  return currentState
end
function GameFleetNewEnhance:GetAdjutantStateFr(adjutants)
  local matrixCells = GameGlobalData:GetData("matrix").cells
  DebugOut("GetAdjutantStateFr = ")
  DebugTable(adjutants)
  DebugTable(matrixCells)
  if matrixCells and #matrixCells > 0 then
    for _, v in ipairs(adjutants) do
      local stateMatrix = false
      local stateAdjutantsByother = false
      local stateHaveAdjutant = false
      for _, m in ipairs(matrixCells) do
        if v == m.fleet_identity then
          stateMatrix = true
        end
      end
      for _, ajutant in ipairs(self.AllheroList) do
        if ajutant.cur_adjutant == v then
          stateAdjutantsByother = true
        end
        if ajutant.identity == v and ajutant.cur_adjutant > 1 then
          stateHaveAdjutant = true
        end
      end
      if not stateMatrix and not stateAdjutantsByother and not stateHaveAdjutant then
        return true
      end
    end
  end
  return false
end
function GameFleetNewEnhance:GetAdjutant(identity)
  local adjutant
  for _, v in pairs(self.AllheroList) do
    if v.identity == identity then
      adjutant = v
      break
    end
  end
  if adjutant == nil then
    DebugOut("error: adjutant is nil")
  end
  return adjutant
end
function GameFleetNewEnhance:GetMajorId(identity)
  for _, v in ipairs(self.AllheroList) do
    if v.cur_adjutant == identity then
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:UpdateAllheroList(...)
  local list = GameGlobalData:GetData("fleetinfo").fleets
  if list == nil then
    DebugOut("error!")
  end
  for k, v in pairs(self.AllheroList) do
    for _, m in pairs(list) do
      if v.identity == m.identity then
        table.remove(self.AllheroList, k)
        table.insert(self.AllheroList, 1, m)
      end
    end
  end
  GameFleetNewEnhance:UpdateFleetLevelInfo()
end
function GameFleetNewEnhance:UpdateFleetShowStack(dir)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local maxFleetCount = LuaUtils:table_size(fleets)
  self.m_fleetOnShowingStack = {}
  local index = 1
  local off1 = -2
  local off2 = 2
  if dir == k_fleetSwitchDirectionPrev then
    off1 = -1
    off2 = 3
  elseif dir == k_fleetSwitchDirectionNext then
    off1 = -3
    off2 = 1
  end
  for i = self.m_currentSelectFleetIndex + off1, self.m_currentSelectFleetIndex + off2 do
    if i < 1 or i > maxFleetCount then
      self.m_fleetOnShowingStack[index] = -1
    else
      self.m_fleetOnShowingStack[index] = i
    end
    index = index + 1
  end
  DebugOut("UpdateFleetShowStack")
  DebugTable(fleets)
  DebugTable(self.m_fleetOnShowingStack)
end
function GameFleetNewEnhance:UpdateFleetShow(dir)
  local switchDirection = dir or k_fleetSwitchDirectionStay
  for pos, fleetIndex in ipairs(self.m_fleetOnShowingStack) do
    local flashFleetPos = -1
    if pos == k_fleetStackPosLeft1 then
      flashFleetPos = k_fleetFlashPosLeft
    elseif pos == k_fleetStackPosMid then
      flashFleetPos = k_fleetFlashPosMid
    elseif pos == k_fleetStackPosRight1 then
      flashFleetPos = k_fleetFlashPosRight
    elseif pos == k_fleetStackPosLeft2 and switchDirection == k_fleetSwitchDirectionPrev or pos == k_fleetStackPosRight2 and switchDirection == k_fleetSwitchDirectionNext then
      flashFleetPos = k_fleetFlashPosOut
    end
    if flashFleetPos >= 0 and fleetIndex >= 0 then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local content = fleets[fleetIndex]
      local leaderlist = GameGlobalData:GetData("leaderlist")
      local curMatrixIndex_m = GameGlobalData.GlobalData.curMatrixIndex
      local fleetIcon = ""
      if content.identity == 1 and leaderlist and curMatrixIndex_m then
        fleetIcon = GameDataAccessHelper:GetShip(leaderlist.leader_ids[curMatrixIndex_m], nil, content.level)
      else
        fleetIcon = GameDataAccessHelper:GetShip(content.identity, nil, content.level)
      end
      DebugOut("ShowFleet:", flashFleetPos, fleetIcon)
      self:GetFlashObject():InvokeASCallback("_root", "ShowFleet", flashFleetPos, fleetIcon)
    elseif flashFleetPos >= 0 then
      DebugOut("HideFleet: ", flashFleetPos)
      self:GetFlashObject():InvokeASCallback("_root", "HideFleet", flashFleetPos)
    end
  end
  if switchDirection == k_fleetSwitchDirectionStay then
    self:RefreshFleetAvatarWithConfig()
  end
end
function GameFleetNewEnhance:isCurFleetInMatrix()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local cur_id = fleets[self.m_currentSelectFleetIndex].identity
  DebugOut("cur_id", cur_id, self:FleetInMatrix(cur_id))
  return ...
end
function GameFleetNewEnhance:InitFleetAvatarWithConfig()
  GameFleetNewEnhance:UpdateFleetShowStack(k_fleetSwitchDirectionStay)
  self:UpdateFleetShow(k_fleetSwitchDirectionStay)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetIndex = 1
  local content = fleets[fleetIndex]
  self.AllheroList = fleets
  local curGrowUpState = self:GetFleetGrowUpState(content.identity)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setGrowUpButtonState", curGrowUpState)
    local cur_id = content.identity
    if self:FleetInMatrix(cur_id) then
      self:GetFlashObject():InvokeASCallback("_root", "setGrowupRedpoint", curGrowUpState == GameFleetNewEnhance.GrowUpState.can)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setGrowupRedpoint", false)
    end
  end
  local adjutantIconFrame = ""
  local adjutantRank = ""
  local curAdjutantButton = self:GetAdjutantButtonState(content.identity)
  if curAdjutantButton == GameFleetNewEnhance.AdjutantButtoState.have then
    local adjutant = self:GetAdjutant(content.cur_adjutant)
    if adjutant then
      adjutantIconFrame = GameDataAccessHelper:GetFleetAvatar(adjutant.identity, adjutant.level)
      local ability = GameDataAccessHelper:GetCommanderAbility(adjutant.identity, adjutant.level)
      adjutantRank = GameUtils:GetFleetRankFrame(ability.Rank, GameFleetNewEnhance.DownloadRankImageCallback)
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setAdjutantButtonState", curAdjutantButton, adjutantIconFrame, adjutantRank)
  if self:FleetInMatrix(content.identity) then
    DebugOut("enheng?", curAdjutantButton)
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", curAdjutantButton == GameFleetNewEnhance.AdjutantButtoState.can)
  else
    self:GetFlashObject():InvokeASCallback("_root", "setAdjutantRedpoint", false)
  end
  return true
end
function GameFleetNewEnhance:RefreshFleetAvatarWithConfig()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetIndex = self.m_fleetOnShowingStack[k_fleetStackPosMid]
  local content = fleets[fleetIndex]
  local leaderlist = GameGlobalData:GetData("leaderlist")
  local CurMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  DebugOut("RefreshFleetAvatar:" .. fleetIndex)
  DebugTable(fleets)
  if not content then
    return
  end
  local iconFrame = GameDataAccessHelper:GetFleetAvatar(content.identity, content.level)
  local fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(content.identity, content.level)
  local level = GameLoader:GetGameText("LC_MENU_Level") .. GameGlobalData:GetData("levelinfo").level
  local vessleType = GameDataAccessHelper:GetCommanderVesselsType(content.identity, content.level)
  local force = GameUtils.numberConversion(content.force)
  local fleetID = content.identity
  local fleetColor = GameDataAccessHelper:GetCommanderColorFrame(fleetID, content.level)
  local forceTitle = GameLoader:GetGameText("LC_MENU_FORCE_CHAR")
  local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetID, content.level)
  local skill = basic_info.SPELL_ID
  local skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
  local ability = GameDataAccessHelper:GetCommanderAbility(fleetID, content.level)
  local fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameFleetNewEnhance.UpdateRankImageDownLoadCallback)
  local sex = GameDataAccessHelper:GetCommanderSex(fleetID)
  if content.identity == 1 and leaderlist and CurMatrixIndex then
    local tempid = leaderlist.leader_ids[CurMatrixIndex]
    iconFrame = GameDataAccessHelper:GetFleetAvatar(tempid, content.level)
    fleetName = GameDataAccessHelper:GetFleetLevelDisplayName(tempid, content.level)
    vessleType = GameDataAccessHelper:GetCommanderVesselsType(tempid, content.level)
    fleetColor = GameDataAccessHelper:GetCommanderColorFrame(tempid, content.level)
    basic_info = GameDataAccessHelper:GetCommanderBasicInfo(tempid, content.level)
    skill = basic_info.SPELL_ID
    skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(skill)
    ability = GameDataAccessHelper:GetCommanderAbility(tempid, content.level)
    fleetRank = GameUtils:GetFleetRankFrame(ability.Rank, GameFleetNewEnhance.UpdateRankImageDownLoadCallback)
    sex = GameDataAccessHelper:GetCommanderSex(tempid)
  end
  if sex == 1 then
    sex = "man"
  elseif sex == 0 then
    sex = "woman"
  else
    sex = "unknown"
  end
  DebugOut("SetFleetAvatar:")
  DebugOut(fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank)
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetAvatar", fleetName, iconFrame, level, vessleType, forceTitle, force, fleetID, fleetColor, skillRangeType, fleetRank, sex)
end
function GameFleetNewEnhance.UpdateRankImageDownLoadCallback(extInfo)
  DebugOut("yyyy = ")
  DebugTable(extInfo)
  if GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "updateFleetRankImage", extInfo.rank_id)
  end
end
function GameFleetNewEnhance.SetMedalButtonNews()
  if GameFleetNewEnhance:GetFlashObject() then
    local hasNews = GameGlobalData:GetFleetMedalButtonStat(GameGlobalData.GlobalData.curMatrixIndex, GameFleetNewEnhance:GetCurFleetId())
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetMedalButtonNews", hasNews)
  end
end
function GameFleetNewEnhance:FleetSelect(index, forceRefresh)
  DebugOut("FleetSelect: ", self.m_currentSelectFleetIndex, index)
  if not GameFleetNewEnhance.doFleetSelectFrame or GameStateManager.m_frameCounter - GameFleetNewEnhance.doFleetSelectFrame > 10 or forceRefresh then
    GameFleetNewEnhance.doFleetSelectFrame = GameStateManager.m_frameCounter
  else
    return
  end
  if GameFleetNewEnhance.isMedalSlot then
    GameFleetNewEnhance:GenerateAvailableMedal()
    GameFleetNewEnhance:RefreshFleetMedal()
  else
    GameFleetNewEnhance.SetMedalButtonNews()
  end
  if self.m_currentSelectFleetIndex ~= index or forceRefresh then
    if QuestTutorialsChangeFleets1:IsActive() or QuestTutorialEnhance_third:IsActive() and not QuestTutorialEquip:IsActive() then
      if index == 2 then
        if QuestTutorialsChangeFleets1:IsActive() then
          QuestTutorialsChangeFleets1:SetFinish(true)
        end
        if QuestTutorialEnhance_third:IsActive() then
          QuestTutorialEnhance_third:SetFinish(true)
        end
        self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", false)
      end
    elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not QuestTutorialEquip:IsActive() then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      if 4 == fleets[index].identity then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowNextTip", false)
      end
    end
    if self.m_currentSelectFleetIndex ~= index then
      DebugOut(" self.m_currentSelectEquipSlot: ", self.m_currentSelectEquipSlot)
      self.m_currentSelectEquipSlot = 1
      self:RefreshEvoluteRedPoint()
    end
    self.m_currentSelectFleetIndex = index
    do
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local fleetId = fleets[self.m_currentSelectFleetIndex].id
      local bag_req_content = {
        owner = fleetId,
        bag_type = BAG_OWNER_FLEET,
        pos = -1
      }
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
      end
      NetMessageMgr:SendMsg(NetAPIList.bag_req.Code, bag_req_content, self.DownloadCurrentFleetEquipment, false, netFailedCallback)
    end
  end
end
function GameFleetNewEnhance:ShowEquipment()
  if not self.equipmentShown then
    self.equipmentShown = true
    self:GetFlashObject():InvokeASCallback("_root", "ShowFleetEquipment")
  end
end
function GameFleetNewEnhance:HideEquipMent()
  if self.equipmentShown then
    self.equipmentShown = false
    self:GetFlashObject():InvokeASCallback("_root", "HideFleetEquipment")
  end
end
function GameFleetNewEnhance:RefreshFleetBagAndSlot()
  DebugOut("RefreshFleetBagAndSlot")
  self:FleetSelect(GameFleetNewEnhance.m_currentSelectFleetIndex, true)
end
function GameFleetNewEnhance.DownloadCurrentFleetEquipment(msgType, content)
  if msgType == NetAPIList.bag_ack.Code then
    DebugOut("DownloadCurrentFleetEquipment:", GameFleetNewEnhance.m_subState)
    if startQuickEquipment and waitForRefreshCount <= 0 then
      startQuickEquipment = false
    end
    if startQuickUnload and waitForRefreshCount <= 0 then
      startQuickUnload = false
    end
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetNewEnhance) then
      return true
    end
    GameFleetNewEnhance.m_curFleetEquipments = GameFleetNewEnhance:processFleetEquipments(content.grids)
    GameItemBag.equipedItem = GameItemBag:processEquipedItem(content.equipedItems)
    GameFleetNewEnhance:CheckCanUnloadAll()
    GameFleetNewEnhance:RefreshCurFleetEquipment()
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "upgradeEquip" then
      GameFleetNewEnhance.m_currentSelectEquipSlot = GameFleetNewEnhance:TheFirstHaveEquipmentInSlotId()
      GameFleetNewEnhance.m_currentSelectEquipSlot = math.max(1, GameFleetNewEnhance.m_currentSelectEquipSlot)
      GameFleetNewEnhance:getUpgradeEquipmentTutorialPos()
    end
    local slot = math.max(1, GameFleetNewEnhance.m_currentSelectEquipSlot)
    GameFleetNewEnhance:ShowEquipment()
    GameFleetNewEnhance:RefreshFleetEquipmentSelect()
    GameFleetNewEnhance:SelectEquipment(GameFleetNewEnhance.m_optionEquipItemId)
    GameFleetNewEnhance.m_optionEquipItemId = -1
    DebugOut("RefreshEquipmentInfo in DownloadCurrentFleetEquipment")
    GameFleetNewEnhance:RefreshEquipmentInfo()
    GameFleetNewEnhance:CheckFinishWanaAndSilvaTutorial()
    GameFleetNewEnhance:getHelpTutorialPos()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.bag_req.Code then
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetNewEnhance) then
      return true
    end
    if content.api == NetAPIList.bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetNewEnhance:TheFirstHaveEquipmentInSlotId(...)
  local index = -1
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    if equip then
      index = i
      break
    end
  end
  return index
end
function GameFleetNewEnhance:IsEquipmentInSlot(slot)
  DebugOut("m_curFleetEquipments = ")
  DebugTable(self.m_curFleetEquipments)
  return self.m_curFleetEquipments[slot]
end
function GameFleetNewEnhance:IsEquipmentInFleet(item_id)
  for k, v in pairs(self.m_curFleetEquipments) do
    if v.item_id == item_id then
      return true, k
    end
  end
  return false
end
function GameFleetNewEnhance:GetDynamic(frame, itemKey, index)
  local frame_temp = frame
  if DynamicResDownloader:IsDynamicStuff(frame_temp, DynamicResDownloader.resType.PIC) then
    local fullFileName = DynamicResDownloader:GetFullName(frame_temp .. ".png", DynamicResDownloader.resType.PIC)
    local localPath = "data2/" .. fullFileName
    if DynamicResDownloader:IfResExsit(localPath) then
      ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
    else
      frame_temp = "temp"
      self:AddDownloadPath(frame_temp, itemKey, index)
    end
  end
  return frame_temp
end
function GameFleetNewEnhance:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameFleetNewEnhance:IsContainEquipment(filterEquip)
  for k, v in pairs(filterEquip) do
    if not GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:RefreshCurFleetEquipment()
  DebugOut("GameFleetNewEnhance:RefreshCurFleetEquipment")
  self:GetOnlyEquip()
  GameUtils:printTable(self.m_curFleetEquipments)
  GameFleetNewEnhance:CheckTutorialEnhance(GameFleetNewEnhance:IsEquipmentInSlot(1))
  local frame, level, StrengthenLv, showText = "", "", "", ""
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    local equipments = GameGlobalData:GetData("equipments")
    if equip then
      do
        local currentEquipId = equip.item_id
        local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
          return v.equip_id == currentEquipId
        end))[1]
        if currentEquipInfo then
          frame = frame .. "item_" .. GameFleetNewEnhance:GetDynamic(currentEquipInfo.equip_type) .. "\001"
          level = level .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
          StrengthenLv = StrengthenLv .. currentEquipInfo.enchant_level .. "\001"
          showText = showText .. "" .. "\001"
        end
      end
    else
      frame = frame .. "empty_e" .. i .. "\001"
      level = level .. "no" .. "\001"
      StrengthenLv = StrengthenLv .. 0 .. "\001"
      DebugOut("filter:" .. tostring(i))
      DebugTable(self.filterEquip)
      if self.filterEquip[i] then
        DebugOut("test:", self:IsContainEquipment(self.filterEquip[i]), ",", GameFleetNewEnhance:IsHasEquipmentToLoad(i))
      end
      if self.filterEquip and self.filterEquip[i] and self:IsContainEquipment(self.filterEquip[i]) and GameFleetNewEnhance:IsHasEquipmentToLoad(i) then
        showText = showText .. GameLoader:GetGameText("LC_MENU_UNEQUIPPED_BUTTON") .. "\001"
        DebugOut("showText:1", showText)
      else
        showText = showText .. GameLoader:GetGameText("LC_MENU_BUY_CHAR") .. "\001"
        DebugOut("showText:2", showText)
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "SetFleetEquipment", frame, level, StrengthenLv, true, showText)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  if (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    if 1 == fleetId and self.m_subState == k_subStateFleetView and nil == self:IsEquipmentInSlot(2) then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
      self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    end
  elseif (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].identity
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    if 4 == fleetId then
      if nil == self:IsEquipmentInSlot(1) then
        if self.m_subState == k_subStateFleetView then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
        elseif self.m_subState == k_subStateFleetEquipment and self.m_currentSelectEquipSlot == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", true)
        end
      elseif nil == self:IsEquipmentInSlot(2) then
        if self.m_subState == k_subStateFleetView then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
        elseif self.m_subState == k_subStateFleetEquipment and self.m_currentSelectEquipSlot == 2 then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", true)
        end
      end
    end
  end
  local equipment_action_list_req_param = {
    ids = {}
  }
  for i = 1, 5 do
    local equip = GameFleetNewEnhance:IsEquipmentInSlot(i)
    if equip then
      table.insert(equipment_action_list_req_param.ids, tonumber(equip.item_id))
    else
      table.insert(equipment_action_list_req_param.ids, -1)
    end
    self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, false)
  end
  NetMessageMgr:SendMsg(NetAPIList.equipment_action_list_req.Code, equipment_action_list_req_param, GameFleetNewEnhance.EquipActionListCallback, true, nil)
  self:ShowEquipmentItem()
end
function GameFleetNewEnhance:CheckBuildAcademyTutorial()
  if immanentversion == 2 then
    if not QuestTutorialBattleMap:IsFinished() then
      QuestTutorialBattleMap:SetActive(true)
    end
  elseif immanentversion == 1 and not QuestTutorialBuildAcademy:IsFinished() then
    QuestTutorialBuildAcademy:SetActive(true)
  end
end
function GameFleetNewEnhance:CheckTutorialEnhance(equip)
  if not self:GetFlashObject() then
    return
  end
  DebugOut("self.m_curFleetEquipments")
  DebugTable(self.m_curFleetEquipments)
  DebugOut("equip = ", equip)
  if (QuestTutorialEnhance:IsActive() or QuestTutorialEnhance_second:IsActive()) and not QuestTutorialEquip:IsActive() then
    if QuestTutorialEnhance_second:IsActive() then
    elseif self.m_subState == k_subStateFleetView or self.m_subState == k_subStateFleetEquipment then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", true)
      if equip then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", true)
        self:GetFlashObject():InvokeASCallback("_root", "SetShowTutEnhance", true)
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
        self:CheckEnableQuckEnhance()
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
        self:GetFlashObject():InvokeASCallback("_root", "SetShowTutEnhance", false)
        self:CheckEnableQuckEnhance()
        QuestTutorialEnhance:SetFinish(true)
        if (immanentversion170 == 4 or immanentversion170 == 5) and not TutorialQuestManager.QuestTutorialPveMapPoint:IsFinished() then
          TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
        end
        AddFlurryEvent("TutoriaEngineUse_EnhanceEqip", {}, 2)
        GameUICommonDialog:PlayStory({100044}, nil)
        self:CheckBuildAcademyTutorial()
      end
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowTutEnhance", false)
    self:CheckEnableQuckEnhance()
  end
end
function GameFleetNewEnhance:CheckTutorialEquip()
  if immanentversion == 2 then
    if QuestTutorialEquip:IsActive() and not QuestTutorialEquip:IsFinished() then
      AddFlurryEvent("FirstEquipSuccess", {}, 1)
      QuestTutorialEquip:SetFinish(true)
      AddFlurryEvent("TutorialEquip", {}, 2)
      if not QuestTutorialSkill:IsFinished() and not QuestTutorialSkill:IsActive() then
        local function callback()
          QuestTutorialSkill:SetActive(true)
          GameStateBattleMap:SetStoryWhenFocusGain({1100014})
          GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
          GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
          GameFleetNewEnhance:ShowCloseTip()
        end
        GameUICommonDialog:PlayStory({11000131}, callback)
      else
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
        if QuestTutorialEnhance:IsActive() then
          GameFleetNewEnhance:CheckTutorialEnhance(true)
        else
          GameFleetNewEnhance:ShowCloseTip()
        end
      end
    elseif QuestTutorialEquipAllByOneKey:IsActive() and not QuestTutorialEquipAllByOneKey:IsFinished() then
      AddFlurryEvent("TutorialItem_Equip", {}, 2)
      QuestTutorialEquipAllByOneKey:SetFinish(true)
    end
  elseif immanentversion == 1 and QuestTutorialEquip:IsActive() then
    AddFlurryEvent("FirstEquipSuccess", {}, 1)
    QuestTutorialEquip:SetFinish(true)
    if immanentversion170 == 4 or immanentversion170 == 5 then
      TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
    end
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEqipTip", false)
    if QuestTutorialEnhance:IsActive() then
      GameFleetNewEnhance:CheckTutorialEnhance(true)
    elseif TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    else
      GameFleetNewEnhance:ShowCloseTip()
    end
    GameStateBattleMap:SetStoryWhenFocusGain({1108})
  end
end
function GameFleetNewEnhance:CheckTutorialEvolution(firstCanEvolution)
  DebugOut("Check QuestTutorialEquipmentEvolution:", QuestTutorialEquipmentEvolution:IsActive(), ",", QuestTutorialEquipmentEvolution:IsFinished(), ",", GameFleetNewEnhance.m_currentSelectFleetIndex, ",", firstCanEvolution)
  DebugOut(self.m_subState)
  if self.m_subState == k_subStateFleetEquipment or self.m_subState == k_subStateFleetEvolution or self.m_subState == k_subStateFleetStrengthen then
    return
  end
  if not QuestTutorialEquipmentEvolution:IsActive() or QuestTutorialEquipmentEvolution:IsFinished() then
    return
  end
  if 1 == GameFleetNewEnhance.m_currentSelectFleetIndex then
    if firstCanEvolution then
      local flashObj = GameFleetNewEnhance:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      end
    elseif self:IsEquipmentInSlot(1) then
      local flashObj = GameFleetNewEnhance:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
      end
      QuestTutorialEquipmentEvolution:SetFinish(true)
    else
      local flashObj = GameFleetNewEnhance:GetFlashObject()
      if flashObj then
        flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
      end
    end
  else
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    end
  end
end
function GameFleetNewEnhance.EquipActionListCallback(msgType, content)
  if msgType == NetAPIList.equipment_action_list_ack.Code then
    DebugOut("EquipActionListCallback", msgType)
    DebugTable(content)
    GameFleetNewEnhance.equipStatus = content.equipments
    local canEvolution = ""
    local firstCanEvolution = false
    for i = 1, 5 do
      local equip = GameFleetNewEnhance:IsEquipmentInSlot(i)
      if equip then
        if GameFleetNewEnhance.equipStatus[i] and GameFleetNewEnhance.equipStatus[i] ~= -1 and GameFleetNewEnhance.equipStatus[i].can_evolute == 1 then
          canEvolution = canEvolution .. "true" .. "\001"
          if i == 1 then
            firstCanEvolution = true
          end
        else
          canEvolution = canEvolution .. "false" .. "\001"
        end
      else
        canEvolution = canEvolution .. "false" .. "\001"
      end
    end
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      if GameFleetNewEnhance:isCurFleetInMatrix() then
        flashObj:InvokeASCallback("_root", "SetEquipArrows", canEvolution)
      else
        DebugOut("not in matrix")
        local ancanEvolution = "" .. "false" .. "\001" .. "false" .. "\001" .. "false" .. "\001" .. "false" .. "\001" .. "false" .. "\001"
        flashObj:InvokeASCallback("_root", "SetEquipArrows", ancanEvolution)
      end
    end
    GameFleetNewEnhance:CheckTutorialEvolution(firstCanEvolution)
    if GameFleetNewEnhance.m_subState == k_subStateFleetView then
      local defSlot = GameFleetNewEnhance.mDefaultSlot
      if -1 ~= defSlot then
        GameFleetNewEnhance.mDefaultSlot = -1
        GameFleetNewEnhance:OnFSCommand("EquipmentSelect", defSlot)
      end
    end
    if currentSelectEquipSlot ~= nil and currentSelectEquipSlot > 0 or 0 < justEquipItem.slot and justEquipItem.isEquip then
      if not currentSelectEquipSlot or currentSelectEquipSlot <= 0 then
        currentSelectEquipSlot = justEquipItem.slot
      end
      local hasEquipment = GameFleetNewEnhance:IsEquipmentInSlot(currentSelectEquipSlot)
      if hasEquipment then
        local curStatus = GameFleetNewEnhance.equipStatus[currentSelectEquipSlot]
        if curStatus and next(curStatus) ~= nil then
          if curStatus.can_evolute ~= currentJumpBtnStatus then
            DebugOut("RefreshEquipmentInfo in EquipActionListCallback")
            GameFleetNewEnhance:RefreshEquipmentInfo()
          else
            DebugOut("curStatus.can_evolute", curStatus.can_evolute)
            DebugOut("currentJumpBtnStatus", currentJumpBtnStatus)
          end
        else
          DebugOut("curStatus")
          DebugTable(curStatus)
        end
      else
        DebugOut("hasEquipment", hasEquipment)
      end
    else
      DebugOut("currentSelectEquipSlot", currentSelectEquipSlot)
      DebugOut("justEquipItem.slot", justEquipItem.slot)
    end
    GameFleetNewEnhance:RefreshRedPoint()
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.equipment_action_list_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetNewEnhance:processFleetEquipments(curFleetEquipments)
  local dest = {}
  for k, v in pairs(curFleetEquipments) do
    dest[v.pos] = v
  end
  return dest
end
function GameFleetNewEnhance:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
    self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipmentInBag")
    self:GetFlashObject():InvokeASCallback("_root", "updateEquipSlot", dt)
    self:GetFlashObject():Update(dt)
    if self.dragBagKrypton then
      self:UpdataFleetSlotState(self.onSelectBagSlot, false)
    end
  end
  self:UpdateReleased(dt)
end
function GameFleetNewEnhance:UpdateReleased(dt)
  if self.doubClicked then
    self.doubClicked = false
    DebugOut("self.doubclicked")
    self:DoubClicked()
  elseif self.clicked and self.clickedTimer > 0 then
    self.clickedTimer = self.clickedTimer - dt
    if self.clickedTimer <= 0 then
      DebugOut("self.lastReleasedIndex: ", self.lastReleasedIndex)
      self.clicked = false
      DebugOut("self.clicked")
    end
  end
end
function GameFleetNewEnhance:HasEquipment()
end
function GameFleetNewEnhance:EnhanceEquipmentSelect(slot, forceRefresh)
  DebugOut("GameFleetNewEnhance:EnhanceEquipmentSelect", slot, self.m_currentSelectEquipSlot)
  if self.m_currentSelectEquipSlot ~= slot or forceRefresh then
    local can_evolute = self.equipStatus and self.equipStatus[slot] and self.equipStatus[slot] ~= -1 and self.equipStatus[slot].can_evolute == 1
    GameFleetNewEnhance:FleetEquipmentSelect(slot)
    self:OnEquipmentSelect()
  end
end
function GameFleetNewEnhance:FleetEquipmentSelect(slot)
  DebugOut("FleetEquipmentSelect", slot)
  self.m_currentSelectEquipSlot = slot
  local frame, level, StrengthenLv
  local equip = self:IsEquipmentInSlot(slot)
  local equipments = GameGlobalData:GetData("equipments")
  self.m_currentSelectEquipSlot = slot
  self:RefreshEvoluteRedPoint()
  if equip then
    do
      local currentEquipId = equip.item_id
      local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
        return v.equip_id == currentEquipId
      end))[1]
      frame = "item_" .. GameFleetNewEnhance:GetDynamic(currentEquipInfo.equip_type)
      level = GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level
      StrengthenLv = currentEquipInfo.enchant_level
    end
  else
    frame = "empty_e" .. slot
    level = "no"
    StrengthenLv = 0
    local defIdx = GameFleetNewEnhance:GetCurFirstCanEquipBagSlotIdx(self.m_currentSelectFleetIndex, slot)
    if defIdx > 0 then
      DebugOut("self.m_currentSelectBagSlot in FleetEquipmentSelect = ", self.m_currentSelectBagSlot)
      self.m_currentSelectBagSlot = defIdx
    end
    DebugOut("defIdx = ", defIdx)
    DebugOut("self.m_currentSelectFleetIndex = ", self.m_currentSelectFleetIndex)
  end
  self:GetFlashObject():InvokeASCallback("_root", "SelectFleetEquipment", slot, frame, level, StrengthenLv)
  self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
end
function GameFleetNewEnhance:RefreshFleetEquipmentSelect()
  local frame, level, StrengthenLv
  local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  local equipments = GameGlobalData:GetData("equipments")
  if equip then
    do
      local currentEquipId = equip.item_id
      local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
        return v.equip_id == currentEquipId
      end))[1]
      frame = "item_" .. GameFleetNewEnhance:GetDynamic(currentEquipInfo.equip_type)
      level = GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level
      StrengthenLv = currentEquipInfo.enchant_level
    end
  else
    frame = "empty_e" .. self.m_currentSelectEquipSlot
    level = "no"
    StrengthenLv = 0
  end
  DebugOut("RefreshFleetEquipmentSelect: ", frame, level)
  self:GetFlashObject():InvokeASCallback("_root", "SetHilightEquipment", frame, level, StrengthenLv)
end
function GameFleetNewEnhance:SelectEquipment(item_id)
  if tonumber(item_id) < 0 then
    return
  end
  local inlist, slot1, slot2 = self:IsEquipmentInFilterList(item_id)
  if inlist then
    self:EquipmentSelectInBag(slot2)
    return
  end
  DebugOut("select item id = ", item_id)
  inlist, slot = self:IsEquipmentInFleet(item_id)
  if inlist then
    self:EquipmentUnselectInBag()
    self:ShowEquipmentInfo()
    return
  end
end
function GameFleetNewEnhance:EquipmentSelectInBag(bagSlot)
  DebugOut("EquipmentSelectInBag", bagSlot)
  DebugOut("self.m_currentSelectBagSlot in EquipmentSelectInBag = ", self.m_currentSelectBagSlot)
  self.m_currentSelectBagSlot = bagSlot
  GameFleetNewEnhance.LastStrengthenItem = nil
  self:ShowEquipmentInfo()
end
function GameFleetNewEnhance:EquipmentUnselectInBag()
  DebugOut("self.m_currentSelectBagSlot in  un EquipmentUnselectInBag = ", self.m_currentSelectBagSlot)
  self.m_currentSelectBagSlot = -1
  GameFleetNewEnhance.LastStrengthenItem = nil
end
function GameFleetNewEnhance:OnEquipmentSelect()
  local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  DebugOut("OnEquipmentSelect", self.m_currentSelectEquipSlot)
  if equip then
    DebugOut("OnEquipmentSelect 1")
    local equipment_detail_req_param = {
      id = equip.item_id
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_detail_req.Code, equipment_detail_req_param, self.SetUpgradeMenu, false, nil)
  else
    DebugOut("OnEquipmentSelect 2")
    self:SetBuyMenu()
    local commanderAcademy = GameGlobalData:GetBuildingInfo("commander_academy")
    local disableEvolution = commanderAcademy.level <= 0
    self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", false, GameFleetNewEnhance.m_subState, disableEvolution, GameFleetNewEnhance.tab_content_tab2_text)
  end
end
function GameFleetNewEnhance:SetBuyMenu()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetIdentity = fleets[self.m_currentSelectFleetIndex].identity
  local fleetLevel = fleets[self.m_currentSelectFleetIndex].level
  local slot = self.m_currentSelectEquipSlot
  local shopItemType = GameDataAccessHelper:GetEquipSlotShopType(slot, fleetIdentity, fleetLevel)
  self.curBuyEquipType = shopItemType
  local defEquip = self:IsEquipmentInSlot(slot)
  if nil == defEquip then
    local defIdx = GameFleetNewEnhance:GetCurFirstCanEquipBagSlotIdx(self.m_currentSelectFleetIndex, slot)
    if defIdx > 0 then
      DebugOut("self.m_currentSelectBagSlot in  SetBuyMenug = ", self.m_currentSelectBagSlot)
      self.m_currentSelectBagSlot = defIdx
    end
  end
  local canEquip = false
  local equip = GameFleetNewEnhance.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot][GameFleetNewEnhance.m_currentSelectBagSlot]
  if equip then
    canEquip = true
    shopItemType = equip.item_type
  end
  if shopItemType ~= -1 then
    local itemFrame = "item_" .. GameFleetNewEnhance:GetDynamic(shopItemType)
    local desText = GameLoader:GetGameText("LC_ITEM_ITEM_DESC_" .. shopItemType)
    local nameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. shopItemType)
    local costText = GameDataAccessHelper:GetEquipPrice(shopItemType)
    local lvText = GameLoader:GetGameText("LC_MENU_Level") .. GameDataAccessHelper:GetEquipReqLevel(shopItemType)
    local btnText = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
    if canEquip then
      btnText = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON")
    end
    self:GetFlashObject():InvokeASCallback("_root", "SetBuyMenu", itemFrame, nameText, costText, desText, lvText, btnText, canEquip)
  end
end
function GameFleetNewEnhance.equipmentEnhanceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_enhance_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 8604 then
        local text = AlertDataList:GetTextFromErrorCode(130)
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    else
      if GameFleetNewEnhance:GetFlashObject() then
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnim")
      end
      if QuestTutorialEnhance:IsActive() then
        QuestTutorialEnhance:SetFinish(true)
        if (immanentversion170 == 4 or immanentversion170 == 5) and not TutorialQuestManager.QuestTutorialPveMapPoint:IsFinished() then
          TutorialQuestManager.QuestTutorialPveMapPoint:SetActive(true, false)
        end
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowShild", false)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowTutEnhance", false)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
        GameFleetNewEnhance:CheckEnableQuckEnhance()
      end
    end
    return true
  end
  return false
end
function GameFleetNewEnhance:FleetEquipmentUnSelect()
  DebugOut("FleetEquipmentUnSelect")
  self:GetFlashObject():InvokeASCallback("_root", "UnSelectFleetEquipmentOver")
end
function GameFleetNewEnhance:UpdateInterveneData(strengthLv)
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  local equipments = GameGlobalData:GetData("equipments")
  if equip then
    do
      local currentEquipId = equip.item_id
      equip = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
        return v.equip_id == currentEquipId
      end))[1]
    end
  end
  local item = {}
  local data = {}
  item.item_type = 3101
  local showInfo = GameFleetNewEnhance:GetShowEquipmentInfoObject(item, k_equipmentInfoTypeStrengthen, equip)
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
  data.canReq = reqLevel <= levelinfo.level
  data.InterveneBatchText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH")
  data.InterveneText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  data.canText = ""
  if not data.canReq then
    data.canText = GameLoader:GetGameText("LC_MENU_ALIIANCE_REQUIRMENT") .. ": " .. reqLevel
  end
  data.strengthenLv = strengthLv
  data.useBtnText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_ADDITIVE_CHECK")
  GameFleetNewEnhance.StrengthenExtParms = showInfo.StrengthenExtParms
  local StrengthenExtParms_info = {}
  if showInfo.StrengthenExtParms then
    StrengthenExtParms_info.CanPrim = showInfo.StrengthenExtParms.CanPrim
    StrengthenExtParms_info.AdditionItem = showInfo.StrengthenExtParms.AdditionItem
    StrengthenExtParms_info.AdditionItemNum = showInfo.StrengthenExtParms.AdditionItemNum
    StrengthenExtParms_info.AdditionItemCount = showInfo.StrengthenExtParms.AdditionItemCount
    StrengthenExtParms_info.AdditionProbability = showInfo.StrengthenExtParms.AdditionProbability
    StrengthenExtParms_info.AdditionUseText = showInfo.StrengthenExtParms.AdditionUseText
    StrengthenExtParms_info.DoPrimeText = showInfo.StrengthenExtParms.DoPrimeText
    StrengthenExtParms_info.IsResetCheckbox = showInfo.StrengthenExtParms.IsResetCheckbox
  end
  data.StrengthenExtParms_info = StrengthenExtParms_info
  data.paramList = {}
  for k, v in ipairs(showInfo.params) do
    local it = {}
    it.color = v.COLOR
    it.typeName = v.TYPETEXT
    it.valueText = v.VALUETEXT
    data.paramList[#data.paramList + 1] = it
  end
  DebugOut("GameFleetNewEnhance:UpdateInterveneData")
  DebugTable(data.paramList)
  data.equip = {}
  data.equip.frame = "item_" .. GameFleetNewEnhance:GetDynamic(equip.equip_type)
  data.equip.level = equip.equip_level
  data.equip.name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. equip.equip_type)
  local list = self.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot]
  local zItem
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      zItem = v
      break
    end
  end
  data.Zcount = 0
  if zItem then
    data.Zcount = zItem.cnt
  end
  if data.Zcount <= 0 and data.canReq then
    data.canReq = false
    data.canText = GameLoader:GetGameText("LC_MENU_INTERFERE_LACK_OF_BOSON")
  end
  GameFleetNewEnhance.inventedData = data
  DebugOut("UpdateInterveneData")
  DebugTable(GameFleetNewEnhance.inventedData)
  local flashObj = GameFleetNewEnhance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "setIntervalData", data)
  end
end
function GameFleetNewEnhance.SetUpgradeMenu(msgType, content)
  if msgType == NetAPIList.equipment_detail_ack.Code then
    DebugOut("SetUpgradeMenu")
    DebugTable(content)
    local StrengthenLv = content.enchant_level
    local enableEvolution = content.enable_evolution == 0
    local enhance = content.enhance
    local evolution = content.evolution
    local recipe = content.recipe
    local recipe_total = content.recipe.recipe_total
    local enhanceIcon, evolutionIcon, recipeIcon = "", "", ""
    enhanceIcon = "item_" .. GameFleetNewEnhance:GetDynamic(enhance.equip_type)
    if enableEvolution then
      evolutionIcon = "item_" .. GameFleetNewEnhance:GetDynamic(evolution.equip_type)
      recipeIcon = "item_" .. GameFleetNewEnhance:GetDynamic(recipe.equip_type) .. "\001"
    else
      local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
      local equipments = GameGlobalData:GetData("equipments")
      if equip then
        do
          local currentEquipId = equip.item_id
          local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
            return v.equip_id == currentEquipId
          end))[1]
          evolutionIcon = "item_" .. GameFleetNewEnhance:GetDynamic(currentEquipInfo.equip_type)
        end
      end
    end
    GameFleetNewEnhance.enhance_equip_type = enhance.equip_type
    GameFleetNewEnhance.enhance_equip = enhance
    GameFleetNewEnhance.evolution_equip_type = evolution.equip_type
    GameFleetNewEnhance.evolution_equip = evolution
    GameFleetNewEnhance.recipe_equip_type = recipe.equip_type
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      if not enableEvolution then
        local reqText = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.enable_evolution)
      end
      flashObj:InvokeASCallback("_root", "SetUpgradeMenu", enhanceIcon, evolutionIcon, recipeIcon, enableEvolution, StrengthenLv, reqText)
    end
    local enhanceSrcLevel = enhance.equip_level ~= 0 and enhance.equip_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local enhanceDestLevel = enhance.equip_next_level ~= 0 and enhance.equip_next_level or GameLoader:GetGameText("LC_MENU_MAX_CHAR")
    local paramText, enhanceSrcParam, enhanceDestParam, evolutionDestParam = "", "", "", ""
    for k, v in pairs(enhance.equip_params) do
      paramText = paramText .. GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key) .. "\001"
      enhanceSrcParam = enhanceSrcParam .. v.value .. "\001"
    end
    for k, v in pairs(enhance.equip_next_params) do
      enhanceDestParam = enhanceDestParam .. v.value .. "\001"
    end
    for k, v in pairs(evolution.equip_params) do
      evolutionDestParam = evolutionDestParam .. v.value .. "\001"
    end
    local enhanceCost = enhance.cost
    local oneKeyEnhanceCost = GameFleetNewEnhance.numberConversionForQuickEnhanceBtnText(content.one_enhance)
    local evolutionCost = evolution.cost
    local evolutionLevel, recipeText, enhanceNameText = "", "", ""
    enhanceNameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. enhance.equip_type)
    local canUpgrade, showWoring = false, false
    local needPlayerLevel = ""
    local levelinfo = GameGlobalData:GetData("levelinfo")
    if enableEvolution then
      evolutionLevel = evolution.equip_level
      recipeText = tostring(recipe.existed) .. "/" .. tostring(recipe_total)
      canUpgrade = levelinfo.level >= recipe.player_level_limit
    else
      evolutionReq = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), content.enable_evolution)
      canUpgrade = false
    end
    if levelinfo.level < recipe.player_level_limit then
      showWoring = true
    end
    needPlayerLevel = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), recipe.player_level_limit)
    local commanderAcademy = GameGlobalData:GetBuildingInfo("engineering_bay")
    local disableEvolution = 0 >= GameGlobalData:GetBuildingInfo("commander_academy").level
    local resource = GameGlobalData:GetData("resource")
    local enableEnhance = false
    local enhanceTip = ""
    local maxbuild = commanderAcademy.level * 5
    if maxbuild > levelinfo.level then
      maxbuild = levelinfo.level or maxbuild
    end
    if enhanceDestLevel <= maxbuild then
      enableEnhance = true
    else
      enableEnhance = false
      local needLevel = math.ceil(enhanceDestLevel / 5)
      DebugOut("ajsdhajsdj:", commanderAcademy.level * 5, levelinfo.level)
      if enhanceDestLevel > commanderAcademy.level * 5 then
        enhanceTip = GameLoader:GetGameText("LC_MENU_TECH_UPGRADE_LEVEL_REQUIRE") .. needLevel
      elseif enhanceDestLevel > levelinfo.level then
        enhanceTip = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), enhanceDestLevel)
      elseif enhanceCost > resource.money then
        enhanceTip = ""
      end
    end
    if immanentversion170 == nil then
      enableEnhance = true
    end
    if flashObj then
      if GameFleetNewEnhance.isNeedShowEnhanceUI then
        GameFleetNewEnhance.isNeedShowEnhanceUI = false
        flashObj:InvokeASCallback("_root", "ShowEnhanceUIDirectly")
      end
      local quickBtnText = GameLoader:GetGameText("LC_MENU_AUTO_ENHANCE_BUTTON")
      flashObj:InvokeASCallback("_root", "SetUpgradeMenuText", enhanceNameText, enhanceSrcLevel, enhanceDestLevel, evolutionLevel, paramText, enhanceSrcParam, enhanceDestParam, evolutionDestParam, enhanceCost, recipeText, evolutionCost, oneKeyEnhanceCost, quickBtnText)
      flashObj:InvokeASCallback("_root", "enableUpgrad", canUpgrade, needPlayerLevel, showWoring)
      flashObj:InvokeASCallback("_root", "SetEnableEnhance", enableEnhance, enhanceTip)
      local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
      flashObj:InvokeASCallback("_root", "showEquipEnhance", equip ~= nil, GameFleetNewEnhance.m_subState, disableEvolution, GameFleetNewEnhance.tab_content_tab2_text)
    end
    GameFleetNewEnhance:UpdateInterveneData(StrengthenLv)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_detail_req.Code then
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj and GameFleetNewEnhance.isNeedShowEnhanceUI then
      GameFleetNewEnhance.isNeedShowEnhanceUI = false
      flashObj:InvokeASCallback("_root", "SetEnhanceUIOpen", false)
    end
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
GameFleetNewEnhance.enhanceOneCD = 120
GameFleetNewEnhance.maxEnhanceLevel = 6
GameFleetNewEnhance.enhanceVIP = 4
function GameFleetNewEnhance:CheckEnhanceCDTime()
  if self.enhanceCDTime and os.time() > self.enhanceCDTime then
    self.enhanceCDTime = nil
  end
  self:RefreshCDTimer()
end
function GameFleetNewEnhance:RefreshCDTimer()
  if not self:GetFlashObject() then
    return
  end
  if immanentversion170 == 4 or immanentversion170 == 5 then
    GameFleetNewEnhance.enhanceOneCD = 10
  end
  local left_time = 0
  local level = self.maxEnhanceLevel
  if self.enhanceCDTime then
    local cd_time = GameGlobalData:GetEnhanceCDTime()
    left_time = self.enhanceCDTime - os.time()
    local show_time = left_time % GameFleetNewEnhance.enhanceOneCD
    level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
    if level < 0 then
      show_time = show_time + -level * self.enhanceOneCD
    end
    self:GetFlashObject():InvokeASCallback("_root", "RefreshCDTimer", self:HaveEnhanceCD(), cd_time.status == 2, GameUtils:formatTimeString(show_time), level)
  else
    self:GetFlashObject():InvokeASCallback("_root", "RefreshCDTimer", self:HaveEnhanceCD(), true, GameUtils:formatTimeString(left_time), level)
  end
end
function GameFleetNewEnhance:GetCanUpCountCd()
  if immanentversion170 == 4 or immanentversion170 == 5 then
    GameFleetNewEnhance.enhanceOneCD = 10
  end
  local left_time = 0
  local level = self.maxEnhanceLevel
  if self.enhanceCDTime then
    left_time = self.enhanceCDTime - os.time()
    level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
  end
  return level
end
function GameFleetNewEnhance:CanEnhance()
  if self.enhanceCDTime == nil then
    return true
  end
  if immanentversion170 == 4 or immanentversion170 == 5 then
    GameFleetNewEnhance.enhanceOneCD = 10
  end
  local left_time = self.enhanceCDTime - os.time()
  local level = self.maxEnhanceLevel - math.ceil(left_time / self.enhanceOneCD)
  return level > 0
end
function GameFleetNewEnhance._EnhanceCDTimeTimer()
  if GameStateManager:GetCurrentGameState() == GameStateEquipEnhance then
    GameFleetNewEnhance:CheckEnhanceCDTime()
  end
  return 1000
end
function GameFleetNewEnhance:HaveEnhanceCD()
  local viplimit = GameDataAccessHelper:GetVIPLimit("no_enhance_cd")
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return viplimit > curLevel
end
function GameFleetNewEnhance:UnlockQuickEnhance()
  local viplimit = 1
  local curLevel = GameVipDetailInfoPanel:GetVipLevel()
  return viplimit <= curLevel
end
function GameFleetNewEnhance:CheckCDCanEnhance()
  if not self:HaveEnhanceCD() then
    return true
  end
  return (...), self
end
function GameFleetNewEnhance.BuyEquip()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id
  local shop_buy_and_wear_req_param = {
    formation_id = GameGlobalData:GetData("matrix").id,
    fleet_id = fleetId,
    equip_type = GameFleetNewEnhance.curBuyEquipType
  }
  DebugOut("shop_buy_and_wear_req_param:")
  DebugTable(shop_buy_and_wear_req_param)
  NetMessageMgr:SendMsg(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetNewEnhance.BuyAndEquipCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetNewEnhance.BuyAndEquipCallback, true, nil)
end
function GameFleetNewEnhance:IsEquipmentInList(itemId)
  for k, v in pairs(self.onlyEquip) do
    if v.item_id == itemId then
      return true, k
    end
  end
  return false
end
function GameFleetNewEnhance:IsEquipmentInFilterList(itemId)
  for k1, v1 in pairs(self.filterEquip) do
    for k2, v2 in pairs(v1) do
      if v2.item_id == itemId then
        return true, k1, k2
      end
    end
  end
  return false
end
function GameFleetNewEnhance:IsHasEquipmentToLoad(slot)
  if GameFleetNewEnhance:GetCurFirstCanEquipBagSlotIdx(self.m_currentSelectFleetIndex, slot) > 0 then
    return true
  else
    return false
  end
end
function GameFleetNewEnhance.UnloadEquipCallback(msgType, content)
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    DebugOut("GameFleetNewEnhance.UnloadEquipCallback")
    if content.result == 0 then
      if GameFleetNewEnhance.dragItemInstance then
        GameFleetNewEnhance:onEndDragItem()
      end
      if startQuickUnload then
        if waitForRefreshCount > 1 then
          waitForRefreshCount = waitForRefreshCount - 1
          local equip = GameFleetNewEnhance:IsEquipmentInSlot(waitForRefreshCount)
          if equip then
            GameFleetNewEnhance:UnloadEquipBySlot(equip.item_id, waitForRefreshCount)
          end
          DebugOut("waitForRefreshCount in unload = ", waitForRefreshCount)
        else
          DebugOut("whats happen")
          waitForRefreshCount = 0
          startQuickUnload = false
          GameFleetNewEnhance.OnEnhanceFleetinfoChange()
        end
      end
      GameFleetNewEnhance:CheckCanUnloadAll()
      return true
    else
      waitForRefreshCount = 0
      startQuickUnload = false
      GameFleetNewEnhance.OnEnhanceFleetinfoChange()
      GameTip:Show(AlertDataList:GetTextFromErrorCode(content.result), 3000)
      if GameFleetNewEnhance.dragItemInstance then
        GameFleetNewEnhance:onEndDragItem()
      end
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:UnloadEquip(itemId)
  DebugOut("GameFleetNewEnhance:UnloadEquip(itemId): ", itemId)
  local inlist, slot = self:IsEquipmentInList(itemId)
  slot = slot or self.m_currentSelectEquipSlot
  self.unloadSlot = slot
  local equip = self:IsEquipmentInSlot(slot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if equip then
    self.m_optionEquipItemId = itemId
    local swap_bag_grid_req_content = {
      matrix_id = curMatrixIndex,
      orig_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      oirg_grid_type = BAG_OWNER_FLEET,
      oirg_grid_pos = slot,
      target_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      target_grid_type = BAG_OWNER_USER,
      target_grid_pos = -1
    }
    local function netFailedCallback()
      GameFleetNewEnhance:onEndDragItem()
      self.refreshFleets()
    end
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.UnloadEquipCallback, true, netFailedCallback)
  end
end
function GameFleetNewEnhance:UnloadEquipBySlot(itemId, slot)
  DebugOut("GameFleetNewEnhance:UnloadEquip(slot): ", slot)
  self.unloadSlot = slot
  local equip = self:IsEquipmentInSlot(slot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if equip then
    self.m_optionEquipItemId = itemId
    local swap_bag_grid_req_content = {
      matrix_id = curMatrixIndex,
      orig_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      oirg_grid_type = BAG_OWNER_FLEET,
      oirg_grid_pos = slot,
      target_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      target_grid_type = BAG_OWNER_USER,
      target_grid_pos = -1
    }
    local function netFailedCallback()
      GameFleetNewEnhance:onEndDragItem()
      self.refreshFleets()
    end
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.UnloadEquipCallback, true, netFailedCallback)
  end
end
function GameFleetNewEnhance:UnloadAllEquip()
  startQuickUnload = false
  waitForRefreshCount = 0
  DebugOut("unload equip in slot i = ", i)
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(5)
  if equip then
    DebugTable(equip)
    startQuickUnload = true
    waitForRefreshCount = 5
    GameFleetNewEnhance:UnloadEquipBySlot(equip.item_id, waitForRefreshCount)
  end
end
function GameFleetNewEnhance:CheckCanUnloadAll()
  if startQuickUnload then
    return false
  end
  local canUnloadAll = true
  for i = 1, 5 do
    local equip = GameFleetNewEnhance:IsEquipmentInSlot(i)
    DebugOut("equip = ", equip)
    if not equip then
      canUnloadAll = false
      if self:GetFlashObject() then
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipArrowsBySlot", i, false)
      end
    end
  end
  if self:GetFlashObject() then
    if canUnloadAll then
      DebugOut("can unload all")
      local txtStr = GameLoader:GetGameText("LC_MENU_AUTO_UNLOAD_EQUIP")
      self:GetFlashObject():InvokeASCallback("_root", "SetQuickEquipButtonText", txtStr)
    else
      DebugOut("can not unload all")
      local txtStr = GameLoader:GetGameText("LC_MENU_AUTO_EQUIP_BUTTON")
      self:GetFlashObject():InvokeASCallback("_root", "SetQuickEquipButtonText", txtStr)
    end
  end
  return canUnloadAll
end
function GameFleetNewEnhance.refreshFleets()
  DebugOut("failed!!!!!!!!!!")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetNewEnhance) then
    GameFleetNewEnhance:UpdateSelectedFleetInfo(GameFleetNewEnhance.m_currentSelectFleetIndex)
  end
end
function GameFleetNewEnhance:DoubClicked()
  self:Equip()
end
function GameFleetNewEnhance:CanEquip(itemType)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  if not fleets then
    return
  end
  if not fleets[self.m_currentSelectFleetIndex] then
    return
  end
  local fleetIdentity = fleets[self.m_currentSelectFleetIndex].identity
  local fleetLevel = fleets[self.m_currentSelectFleetIndex].level
  if GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_energy and GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_physics and not GameDataAccessHelper:IsEnergyAttack(fleetIdentity, fleetLevel) then
    return true
  elseif GameDataAccessHelper:GetEquipReqFleet(itemType) == GameDataAccessHelper.ATK_TYPE_common then
    return true
  end
  GameTip:Show(GameLoader:GetGameText("LC_MENU_EQUIP_TYPE_ERR"), 3000)
  return false
end
function GameFleetNewEnhance:Equip(itemId)
  if startQuickEquipment then
    return
  end
  DebugOut("GameFleetNewEnhance:Equip", itemId)
  local _, index = self:IsEquipmentInList(itemId)
  index = index or self.lastReleasedIndex
  if not self.onlyEquip[index] then
    return
  end
  local itemType = self.onlyEquip[index].item_type
  local itemId = self.onlyEquip[index].item_id
  local srcPos = self.onlyEquip[index].pos
  self.equipIndex = index
  DebugOut("GameFleetNewEnhance:Equip equipIndex is : " .. self.equipIndex)
  local slot = GameDataAccessHelper:GetEquipSlot(itemType) or -1
  if slot == -1 then
    return
  end
  self.equipSlot = slot
  justEquipItem.slot = slot
  justEquipItem.isEquip = false
  if self:CanEquip(itemType) then
    self.m_optionEquipItemId = itemId
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local swap_bag_grid_req_content = {
      matrix_id = curMatrixIndex,
      orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      oirg_grid_type = BAG_OWNER_USER,
      oirg_grid_pos = srcPos,
      target_grid_owner = fleets[self.m_currentSelectFleetIndex].id,
      target_grid_type = BAG_OWNER_FLEET,
      target_grid_pos = slot
    }
    local function netFailedCallback()
      GameFleetNewEnhance:onEndDragItem()
      GameItemBag:RequestBag()
    end
    startQuickEquipment = true
    waitForRefreshCount = 1
    DebugOut("GameFleetNewEnhance:Equip yes")
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, self.EquipCallback, true, netFailedCallback)
  elseif self.dragItemInstance then
    self.dragItemInstance:SetDestPosition(self.dragItemInstance.InitPosX, self.dragItemInstance.InitPosY)
    self.dragItemInstance:StartAutoMove()
    self.dragItemInstance.DraggedItemID = -1
  end
end
function GameFleetNewEnhance:onBeginDragItem(dragitem)
  self.dragItemInstance = dragitem
  local itemId = self.dragItemInstance.DraggedItemID
  local inlist, index = self:IsEquipmentInList(itemId)
  DebugOut("onBeginDragItem: ", itemId, inlist, index)
  if self.isDragEquipedItem then
    self:GetFlashObject():InvokeASCallback("_root", "onDragEquipmentItem", self.m_currentSelectEquipSlot)
  else
    self:GetFlashObject():InvokeASCallback("_root", "onDragItem", index)
    self:GetFlashObject():InvokeASCallback("_root", "onBeginDragItem", self.dragBagItem, self.dragFleetItem, self.pressBagSlot, self.pressFleetSlot)
  end
end
function GameFleetNewEnhance:onEndDragItem()
  DebugOut("GameFleetNewEnhance:onEndDragItem()")
  self.dragItem = false
  self.dragItemInstance = nil
  self.dragBagKrypton = false
  self.dragBagItem = false
  self.dragFleetItem = false
  self:GetFlashObject():InvokeASCallback("_root", "onEndDragItem")
  if GameFleetNewEnhance.m_subState == k_subStateFleetKrypton then
    GameFleetNewEnhance:RefreshBagKrypton()
    GameFleetNewEnhance:RefreshCurFleetKrypton()
  end
end
function GameFleetNewEnhance:RefreshBagKrypton()
  local cnt = math.ceil(GameFleetNewEnhance.grid_capacity / 4)
  if cnt > 0 then
    for k = 1, cnt do
      GameFleetNewEnhance:UpdateBagListItem(k)
    end
  end
end
function GameFleetNewEnhance:AllEquipAction()
  DebugOut("AllEquipAction", #self.all_need_equip)
  DebugTable(self.all_need_equip)
  if #self.all_need_equip > 1 then
    return
  end
  if startQuickEquipment then
    return
  end
  local currentFleetEquipMentTemp = {}
  local currentGameItemBagTemp = {}
  local equipments = GameGlobalData:GetData("equipments")
  for k, v in pairs(equipments) do
    for k1, v1 in pairs(self.m_curFleetEquipments) do
      if v1.item_id == v.equip_id then
        table.insert(currentFleetEquipMentTemp, v)
      end
    end
    for k2, v2 in pairs(self.onlyEquip) do
      if v2.item_id == v.equip_id then
        v.pos = v2.pos
        table.insert(currentGameItemBagTemp, v)
      end
    end
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[self.m_currentSelectFleetIndex]
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleet.identity)
  local isEnFleet = false
  if tonumber(fleetVessels) == 4 then
    isEnFleet = true
  end
  self.all_need_equip = {}
  local tmp_table = {}
  for i = 1, 5 do
    if i == 1 then
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          if isEnFleet and tonumber(string.sub(v.equip_type, -1)) == 6 then
            table.insert(tmp_table, v)
          elseif not isEnFleet and tonumber(string.sub(v.equip_type, -1)) ~= 6 then
            table.insert(tmp_table, v)
          end
        end
      end
    else
      tmp_table = {}
      for k, v in pairs(currentGameItemBagTemp) do
        if v.equip_slot == i then
          table.insert(tmp_table, v)
        end
      end
    end
    DebugOut("-------------------------------------" .. i .. "   --------")
    DebugTable(tmp_table)
    if #tmp_table > 0 then
      table.sort(tmp_table, function(v1, v2)
        if v1.equip_type == v2.equip_type then
          return v1.equip_level > v2.equip_level
        else
          return v1.equip_type > v2.equip_type
        end
      end)
      local canReplaceEquip = true
      for k, v in pairs(currentFleetEquipMentTemp) do
        if v.equip_slot == i then
          if v.equip_type == tmp_table[1].equip_type and v.equip_level >= tmp_table[1].equip_level then
            canReplaceEquip = false
          elseif v.equip_type > tmp_table[1].equip_type then
            canReplaceEquip = false
          end
        end
      end
      if canReplaceEquip then
        table.insert(self.all_need_equip, tmp_table[1])
      end
    end
  end
  if #self.all_need_equip > 0 then
    self:ReplaceAllEquipmentByOneKey()
  elseif QuestTutorialEquipAllByOneKey:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
    QuestTutorialEquipAllByOneKey:SetFinish(true)
  end
end
function GameFleetNewEnhance:ShowCloseTip()
  self:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
end
function GameFleetNewEnhance.EquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.swap_bag_grid_req.Code then
    return true
  end
  if msgType == NetAPIList.swap_bag_grid_ack.Code then
    DebugOut("\230\137\147\229\141\176--GameFleetNewEnhance.EquipCallback----")
    DebugTable(content)
    if content.result == 0 then
      if startQuickEquipment then
        if waitForRefreshCount > 0 then
          waitForRefreshCount = waitForRefreshCount - 1
          DebugOut("waitForRefreshCount in equip = ", waitForRefreshCount)
        end
        if waitForRefreshCount == 0 then
          DebugOut("receive all")
          if QuestTutorialEquipAllByOneKey:IsActive() then
            GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowTipClose", true)
            QuestTutorialEquipAllByOneKey:SetFinish(true)
          end
        end
      else
        justEquipItem.isEquip = true
      end
      if GameFleetNewEnhance.dragItemInstance then
        GameStateEquipEnhance:onEndDragItem()
      end
      if QuestTutorialEquip:IsActive() then
        GameFleetNewEnhance:CheckTutorialEquip()
      end
      GameFleetNewEnhance:CheckCanUnloadAll()
      DebugOut("lalalalal2222")
      if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() then
        DebugOut("lalalalal333333")
        local fleets = GameGlobalData:GetData("fleetinfo").fleets
        local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].identity
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
        if 4 == fleetId then
          DebugOut("equip here ")
          if GameFleetNewEnhance:IsEquipmentInSlot(1) and GameFleetNewEnhance.m_currentSelectEquipSlot == 1 then
            DebugOut("equip here set visible false 11")
            GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
            GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
          elseif nil == GameFleetNewEnhance:IsEquipmentInSlot(2) and GameFleetNewEnhance.m_currentSelectEquipSlot == 2 then
            DebugOut("equip here set visible false 22")
            GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
            GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
          end
        end
      end
      GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation()
      GameFleetNewEnhance._forceFinishTutorialEquipAndTutorialFirstGetWanaArmorIfTutorialFirstGetSilvaEquipIsActive()
      GameFleetNewEnhance:CheckFinishWanaAndSilvaTutorial()
      return true
    else
      waitForRefreshCount = 0
      startQuickEquipment = false
      GameFleetNewEnhance.all_need_equip = {}
      GameUIGlobalScreen:ShowAlert("error", content.result, nil)
      if GameFleetNewEnhance.dragItemInstance then
        GameStateEquipEnhance:onEndDragItem()
      end
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:ReplaceAllEquipmentByOneKey()
  DebugOut("ReplaceAllEquipmentByOneKey")
  DebugTable(self.all_need_equip)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local function netFailedCallback()
    GameFleetNewEnhance.all_need_equip = {}
    GameItemBag:RequestBag()
  end
  waitForRefreshCount = 0
  startQuickEquipment = false
  while #GameFleetNewEnhance.all_need_equip > 0 do
    local swap_bag_grid_req_content = {
      matrix_id = curMatrixIndex,
      orig_grid_owner = GameGlobalData:GetData("userinfo").player_id,
      oirg_grid_type = BAG_OWNER_USER,
      oirg_grid_pos = GameFleetNewEnhance.all_need_equip[1].pos,
      target_grid_owner = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id,
      target_grid_type = BAG_OWNER_FLEET,
      target_grid_pos = GameFleetNewEnhance.all_need_equip[1].equip_slot
    }
    table.remove(GameFleetNewEnhance.all_need_equip, 1)
    NetMessageMgr:SendMsg(NetAPIList.swap_bag_grid_req.Code, swap_bag_grid_req_content, GameFleetNewEnhance.EquipCallback, true, netFailedCallback)
    waitForRefreshCount = waitForRefreshCount + 1
    startQuickEquipment = true
  end
end
function GameFleetNewEnhance.BuyAndEquipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    DebugOut("BuyAndEquipCallback:")
    DebugTable(content)
    if content.api == NetAPIList.shop_buy_and_wear_req.Code then
      if content.code ~= 0 then
        if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 709 then
          local text = AlertDataList:GetTextFromErrorCode(130)
          local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
          GameUIKrypton.NeedMoreMoney(text)
        else
          GameUIGlobalScreen:ShowAlert("error", content.code, nil)
        end
      else
        GameFleetNewEnhance:CheckTutorialEquip()
      end
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:ShouldEquip(itemId)
  local _, index = self:IsEquipmentInList(itemId)
  if index then
    local itemType = self.onlyEquip[index].item_type
    local slot = GameDataAccessHelper:GetEquipSlot(itemType)
    GameFleetNewEnhance:Equip(itemId)
  end
end
function GameFleetNewEnhance:GetOnlyEquip()
  DebugOut("GetOnlyEquip:")
  DebugTable(GameItemBag.itemInBag)
  self.onlyEquip = LuaUtils:table_values(LuaUtils:table_filter(GameItemBag.itemInBag or {}, function(k, v)
    return tonumber(v.item_type) > 100000 and tonumber(v.item_type) < 110000 or GameDataAccessHelper:IsItemUsedStrengthen(v.item_type)
  end))
  self.filterEquip = {}
  for i = 1, k_maxSlot do
    self.filterEquip[i] = {}
  end
  for i, value in pairs(self.onlyEquip) do
    local itemType = value.item_type
    if GameDataAccessHelper:IsItemUsedStrengthen(itemType) then
      for i = 1, k_maxSlot do
        self.filterEquip[i][#self.filterEquip[i] + 1] = value
      end
    else
      local slot = GameDataAccessHelper:GetEquipSlot(itemType)
      table.insert(self.filterEquip[slot], 1, value)
    end
  end
  DebugOut("onlyEquip:")
  DebugTable(self.onlyEquip)
  DebugOut("filterEquip:")
  DebugTable(self.filterEquip)
end
function GameFleetNewEnhance.equipmentEvolutionCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.equipment_evolution_req.Code then
    if content.code ~= 0 then
      if not GameUICollect:CheckTutorialCollect(content.code) and content.code == 130 then
        local text = AlertDataList:GetTextFromErrorCode(content.code)
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        GameUIKrypton.NeedMoreMoney(text)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    GameItemBag:RequestBag(false)
    local fleetinfo = GameGlobalData.GlobalData.fleetinfo.fleets
    for k, v in pairs(fleetinfo) do
      if v.id == GameFleetNewEnhance.m_currentFleetsId then
        local enhanceCount = v.force - GameFleetNewEnhance.m_curreetFleetsForce
        GameFleetNewEnhance.m_curreetFleetsForce = v.force
        DebugOut("showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
      end
    end
    GameFleetNewEnhance:RefreshFleetBagAndSlot()
    return true
  end
  return false
end
function GameFleetNewEnhance:RefreshEquipmentInfo()
  if GameFleetNewEnhance:GetFlashObject() then
    DebugOut("RefreshEquipmentInfo: ", self.m_subState)
    if self.m_subState == k_subStateFleetEquipment then
      GameFleetNewEnhance:ShowEquipmentInfo()
    elseif self.m_subState == k_subStateFleetEnhance or self.m_subState == k_subStateFleetEvolution or self.m_subState == k_subStateFleetIntervene then
      self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
    end
  end
end
function GameFleetNewEnhance:GetCurrentSelectItem(FeetSlot, BagSlot)
  local item, itemInBag, itemInFleet
  DebugOut("FeetSlot = ", FeetSlot)
  DebugOut("BagSlot = ", BagSlot)
  DebugTable(self.filterEquip)
  if BagSlot ~= -1 and self.filterEquip[FeetSlot][BagSlot] ~= nil then
    itemInBag = self.filterEquip[FeetSlot][BagSlot]
    if GameDataAccessHelper:IsItemUsedStrengthen(itemInBag.item_type) then
      item = itemInBag
    else
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInBag.item_id == v.equip_id then
          itemInBag = v
          break
        end
      end
    end
  end
  if FeetSlot ~= -1 then
    itemInFleet = self:IsEquipmentInSlot(FeetSlot)
    if itemInFleet then
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInFleet.item_id == v.equip_id then
          itemInFleet = v
          break
        end
      end
    end
  end
  return item, itemInBag, itemInFleet
end
function GameFleetNewEnhance:MatchStrengthenItem(sDetail, StrengthenItemType, src_Equip)
  local result = {}
  local addIndex = 1
  result.result = false
  result.CanPrim = false
  DebugOut("MatchStrengthenItem:")
  DebugOut(StrengthenItemType)
  DebugTable(src_Equip)
  for kItem, vItem in pairs(GameItemBag.itemInBag) do
    if vItem.item_type == StrengthenItemType and vItem.cnt > 0 and not src_Equip.invented then
      result.CanPrim = true
    end
  end
  if GameFleetNewEnhance.StrengthenExtParms ~= nil and GameFleetNewEnhance.StrengthenExtParms.AdditionItem ~= nil then
    for k, v in pairs(sDetail.add_item) do
      if addIndex ~= #sDetail.add_item and v[1] == GameFleetNewEnhance.StrengthenExtParms.AdditionItem then
        for kItem, vItem in pairs(GameItemBag.itemInBag) do
          if vItem.item_type == v[1] and vItem.cnt >= v[2] then
            result.result = true
            result.type = v[1]
            result.num = v[2]
            result.weight = v[3]
            result.count = vItem.cnt
            return result
          end
        end
      end
      addIndex = addIndex + 1
    end
  end
  addIndex = 1
  for k, v in pairs(sDetail.add_item) do
    if addIndex ~= #sDetail.add_item then
      for kItem, vItem in pairs(GameItemBag.itemInBag) do
        if vItem.item_type == v[1] and vItem.cnt >= v[2] then
          result.result = true
          result.type = v[1]
          result.num = v[2]
          result.weight = v[3]
          result.count = vItem.cnt
          return result
        end
      end
    end
    addIndex = addIndex + 1
  end
  return result
end
function GameFleetNewEnhance:GetStrenthenList(sDetail, sAddition, StrengthenItemType, src_Equip)
  local addIndex = 1
  local result = {}
  for k, v in ipairs(sDetail.add_item) do
    if addIndex ~= #sDetail.add_item then
      for kItem, vItem in pairs(GameItemBag.itemInBag) do
        if vItem.item_type == v[1] and vItem.cnt >= v[2] then
          local param = {}
          param.item_type = "item"
          param.number = vItem.item_type
          param.no = vItem.cnt
          param.needcnt = v[2]
          param.level = 0
          param.weight = v[3]
          table.insert(result, param)
        end
      end
    end
    addIndex = addIndex + 1
  end
  local weight = {}
  weight.TotleWeight = 0
  weight.SucessWeight = 0
  weight.AdditionWeight = sAddition
  for k, v in pairs(sDetail.probability) do
    weight.TotleWeight = weight.TotleWeight + v[2]
    if v[1] > 0 then
      weight.SucessWeight = weight.SucessWeight + v[2]
    end
  end
  return result, weight
end
function GameFleetNewEnhance:GetStrengthenParmasInfo(src_Equip, StrengthenItemType, PlayerLevel)
  DebugOut("StrengthenParms:GetStrengthenParmasInfo")
  local StrengthenParms = {}
  StrengthenParms.itemStrengthenType = {}
  StrengthenParms.itemStrengthenDetail = {}
  StrengthenParms.itemStrengthenProbability = {}
  StrengthenParms.Ext = {}
  StrengthenParms.Ext.CanPrim = false
  StrengthenParms.Ext.AdditionItem = 1
  StrengthenParms.Ext.AdditionItemNum = 0
  StrengthenParms.Ext.AdditionItemCount = 0
  StrengthenParms.Ext.AdditionProbability = nil
  StrengthenParms.Ext.AdditionUseText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_ADDITIVE_CHECK")
  StrengthenParms.Ext.DoPrimeText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_INTERFERE_BUTTON")
  StrengthenParms.Ext.LimitLv = 0
  StrengthenParms.Ext.IsResetCheckbox = false
  local AdditionWeight = 0
  if not src_Equip then
    DebugOut("not src_Equip")
    src_Equip = {
      equip_type = 0,
      enchant_level = 0,
      enchant_item_id = 0,
      enchant_effect_value = 0,
      invented = true
    }
    StrengthenParms.Ext.CanPrim = false
  end
  local EquipStrengthenLv = 0
  if StrengthenItemType == src_Equip.enchant_item_id then
    EquipStrengthenLv = src_Equip.enchant_level
  end
  DebugOut("adhssahsgadsj:", StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  local sDetail, sAddition = GameDataAccessHelper:GetStrengthenMaterialInfo(StrengthenItemType, EquipStrengthenLv, PlayerLevel)
  DebugOut("sDetail:")
  DebugTable(sDetail)
  DebugOut("=======>GameItemBag")
  DebugTable(GameItemBag.itemInBag)
  local matchResult = self:MatchStrengthenItem(sDetail, StrengthenItemType, src_Equip)
  GameFleetNewEnhance.StrenthenList, GameFleetNewEnhance.StrenthenWeight = self:GetStrenthenList(sDetail, sAddition, StrengthenItemType, src_Equip)
  DebugTable(matchResult)
  StrengthenParms.Ext.CanPrim = matchResult.CanPrim
  if matchResult.result then
    StrengthenParms.Ext.AdditionItem = matchResult.type
    StrengthenParms.Ext.AdditionItemNum = matchResult.num
    StrengthenParms.Ext.AdditionItemCount = matchResult.count
    GameFleetNewEnhance.PreCheckbox = false
    AdditionWeight = matchResult.weight
  end
  if StrengthenParms.Ext.AdditionItem == 1 then
    StrengthenParms.Ext.AdditionItemNum = sDetail.add_item[#sDetail.add_item][1]
    local resource = GameGlobalData:GetData("resource")
    StrengthenParms.Ext.AdditionItemCount = resource.credit
    if GameFleetNewEnhance.PreCheckbox == nil or not GameFleetNewEnhance.PreCheckbox then
      StrengthenParms.Ext.IsResetCheckbox = true
    else
      StrengthenParms.Ext.IsResetCheckbox = false
    end
    GameFleetNewEnhance.PreCheckbox = true
    AdditionWeight = sDetail.add_item[#sDetail.add_item][2]
  end
  local equipType = src_Equip.equip_type % 10
  StrengthenParms.itemStrengthenType = {
    K = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. StrengthenItemType),
    V = 0
  }
  if src_Equip and StrengthenItemType == src_Equip.enchant_item_id then
    StrengthenParms.itemStrengthenType.V = src_Equip.enchant_level
  end
  if src_Equip.invented or sDetail["equip_" .. equipType] and 1 < #sDetail["equip_" .. equipType] then
    if StrengthenItemType ~= src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_RANDOM_ATTRIBUTE"),
        V = 0
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. src_Equip.enchant_effect),
        V = src_Equip.enchant_effect_value
      }
    end
  else
    local effect_id = 0
    for k, v in pairs(sDetail.effect) do
      if v[1] == sDetail["equip_" .. equipType][1][1] then
        effect_id = v[2]
      end
    end
    if StrengthenItemType == src_Equip.enchant_item_id then
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = src_Equip.enchant_effect_value
      }
    else
      StrengthenParms.itemStrengthenDetail = {
        K = GameLoader:GetGameText("LC_MENU_Equip_param_" .. effect_id),
        V = 0
      }
    end
  end
  local TotleWeight = 0
  local SucessWeight = 0
  for k, v in pairs(sDetail.probability) do
    TotleWeight = TotleWeight + v[2]
    if v[1] > 0 then
      SucessWeight = SucessWeight + v[2]
    end
  end
  TotleWeight = TotleWeight + sAddition
  SucessWeight = SucessWeight + sAddition
  local probNum = math.ceil(SucessWeight / TotleWeight * 10000) / 100
  if probNum > 100 then
    probNum = 100
  end
  local Probability = probNum .. "%"
  StrengthenParms.itemStrengthenProbability = {
    K = GameLoader:GetGameText("LC_MENU_INTERFERENCE_SUCCESS_RATE"),
    V = Probability
  }
  local additionProbNum = math.ceil((SucessWeight + AdditionWeight) / (TotleWeight + AdditionWeight) * 10000) / 100
  if additionProbNum > 100 or AdditionWeight >= 1000 then
    additionProbNum = 100
  end
  StrengthenParms.Ext.AdditionProbability = additionProbNum .. "%"
  StrengthenParms.Ext.LimitLv = sDetail.player_level
  return StrengthenParms
end
function GameFleetNewEnhance:GetShowEquipmentInfoObject(item, showType, itemInFleet)
  local showInfo = {}
  showInfo.Frame = nil
  showInfo.itemType = nil
  showInfo.itemName = nil
  showInfo.reqLevel = nil
  showInfo.level = nil
  showInfo.StrengthenLv = 0
  showInfo.itemPrice = 0
  showInfo.itemNeedLevel = nil
  showInfo.DescText = nil
  showInfo.params = {}
  showInfo.StrengthenExtParms = nil
  if showType == k_equipmentInfoTypeStrengthen then
    showInfo.itemType = item.item_type
    showInfo.reqLevel = GameDataAccessHelper:GetItemReqLevel(showInfo.itemType)
    local playerInfo = GameGlobalData:GetData("levelinfo")
    showInfo.level = 0
    showInfo.StrengthenLv = 0
    showInfo.itemPrice = GameDataAccessHelper:GetItemPrice(showInfo.itemType)
    showInfo.DescText = GameLoader:GetGameText("LC_ITEM_ITEM_DESCSIMPLE_" .. showInfo.itemType)
    local StrengthenParms = GameFleetNewEnhance:GetStrengthenParmasInfo(itemInFleet, showInfo.itemType, GameGlobalData:GetData("levelinfo").level)
    if immanentversion170 == 4 or immanentversion170 == 5 then
      DebugOut("hjashdgagsaj:", showInfo.itemType)
      if showInfo.itemType == 3101 then
        local enchoData = GameDataAccessHelper:GetStrengthenMaterialInfo(3101, itemInFleet.enchant_level, playerInfo.level)
        showInfo.reqLevel = enchoData.player_level
        DebugOut("hjashdgagsaj:", showInfo.reqLevel)
      end
    end
    showInfo.itemNeedLevel = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. showInfo.reqLevel
    DebugOut("========StrengthenParms:")
    DebugTable(StrengthenParms)
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenType.K,
      VALUETEXT = GameLoader:GetGameText("LC_MENU_Level") .. StrengthenParms.itemStrengthenType.V
    })
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenDetail.K,
      VALUETEXT = StrengthenParms.itemStrengthenDetail.V
    })
    table.insert(showInfo.params, {
      COLOR = "B",
      TYPETEXT = StrengthenParms.itemStrengthenProbability.K,
      VALUETEXT = StrengthenParms.itemStrengthenProbability.V
    })
    showInfo.StrengthenExtParms = StrengthenParms.Ext
  else
    if showType == k_equipmentInfoTypeBuy then
      showInfo.itemType = GameFleetNewEnhance.curBuyEquipType
      showInfo.reqLevel = 1
      showInfo.level = 1
      showInfo.StrengthenLv = 0
      local native_equipParams = GameDataAccessHelper:GetEquipParams(showInfo.itemType)
      for k, v in ipairs(native_equipParams) do
        table.insert(showInfo.params, {
          COLOR = "Y",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.T),
          VALUETEXT = GameUtils.numberConversion(v.V)
        })
      end
    else
      showInfo.itemType = item.equip_type
      showInfo.reqLevel = item.equip_level_req
      showInfo.level = item.equip_level
      showInfo.StrengthenLv = item.enchant_level
      for k, v in ipairs(item.equip_params) do
        table.insert(showInfo.params, {
          COLOR = "Y",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. v.key),
          VALUETEXT = GameUtils.numberConversion(v.value)
        })
      end
      if item.enchant_level ~= 0 then
        table.insert(showInfo.params, {
          COLOR = "B",
          TYPETEXT = GameLoader:GetGameText("LC_ITEM_ITEM_EFFECT_" .. item.enchant_item_id),
          VALUETEXT = GameLoader:GetGameText("LC_MENU_Level") .. GameUtils.numberConversion(item.enchant_level)
        })
        table.insert(showInfo.params, {
          COLOR = "B",
          TYPETEXT = GameLoader:GetGameText("LC_MENU_Equip_param_" .. item.enchant_effect),
          VALUETEXT = GameUtils.numberConversion(item.enchant_effect_value)
        })
      end
    end
    showInfo.itemName = GameDataAccessHelper:GetItemNameText(showInfo.itemType)
    showInfo.itemPrice = GameDataAccessHelper:GetEquipPrice(showInfo.itemType)
    showInfo.itemNeedLevel = GameLoader:GetGameText("LC_MENU_UPGRADE_PLAYER_LVL") .. ": " .. showInfo.reqLevel
    showInfo.DescText = GameDataAccessHelper:GetItemDescText(showInfo.itemType)
  end
  showInfo.Frame = "item_" .. showInfo.itemType
  showInfo.itemName = GameDataAccessHelper:GetItemNameText(showInfo.itemType)
  return showInfo
end
GameFleetNewEnhance.LastStrengthenItem = nil
function GameFleetNewEnhance:ShowEquipmentInfo()
  local item_main_info = ""
  local equip_extra_info = ""
  local btn_info = ""
  local info_type = "N"
  local item, showType
  self.m_leftCallBack = nil
  self.m_rightCallBack = nil
  local itemInBag, itemInFleet
  DebugOut("self.m_currentSelectEquipSlot : " .. self.m_currentSelectEquipSlot)
  DebugOut("self.m_currentSelectBagSlot : " .. self.m_currentSelectBagSlot)
  local item, itemInBag, itemInFleet = GameFleetNewEnhance:GetCurrentSelectItem(self.m_currentSelectEquipSlot, self.m_currentSelectBagSlot)
  if GameFleetNewEnhance.LastStrengthenItem then
    item = GameFleetNewEnhance.LastStrengthenItem
  else
    GameFleetNewEnhance.LastStrengthenItem = item
  end
  if item then
    showType = k_equipmentInfoTypeStrengthen
  elseif itemInBag then
    showType = k_equipmentInfoTypeLoad
    item = itemInBag
  elseif itemInFleet then
    showType = k_equipmentInfoTypeUnload
    item = itemInFleet
  else
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetIdentity = fleets[self.m_currentSelectFleetIndex].identity
    local fleetLevel = fleets[self.m_currentSelectFleetIndex].level
    GameFleetNewEnhance.curBuyEquipType = GameDataAccessHelper:GetEquipSlotShopType(self.m_currentSelectEquipSlot, fleetIdentity, fleetLevel)
    showType = k_equipmentInfoTypeBuy
  end
  local showInfo = GameFleetNewEnhance:GetShowEquipmentInfoObject(item, showType, itemInFleet)
  DebugTable(showInfo)
  item_main_info = showInfo.Frame .. "\001"
  item_main_info = item_main_info .. showInfo.itemName .. "\001"
  item_main_info = item_main_info .. showInfo.itemPrice .. "\001"
  item_main_info = item_main_info .. showInfo.itemNeedLevel .. "\001"
  item_main_info = item_main_info .. showInfo.level .. "\001"
  item_main_info = item_main_info .. showInfo.DescText .. "\001"
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local canReq = levelinfo.level >= showInfo.reqLevel
  item_main_info = item_main_info .. tostring(canReq) .. "\001"
  item_main_info = item_main_info .. showInfo.StrengthenLv .. "\001"
  GameFleetNewEnhance.StrengthenExtParms = showInfo.StrengthenExtParms
  local StrengthenExtParms_info
  if showInfo.StrengthenExtParms then
    StrengthenExtParms_info = tostring(showInfo.StrengthenExtParms.CanPrim) .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. showInfo.StrengthenExtParms.AdditionItem .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. showInfo.StrengthenExtParms.AdditionItemNum .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. showInfo.StrengthenExtParms.AdditionProbability .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. showInfo.StrengthenExtParms.AdditionUseText .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. showInfo.StrengthenExtParms.DoPrimeText .. "\001"
    StrengthenExtParms_info = StrengthenExtParms_info .. tostring(showInfo.StrengthenExtParms.IsResetCheckbox) .. "\001"
    DebugOut("StrengthenExtParms_info : " .. StrengthenExtParms_info)
  end
  for k, v in ipairs(showInfo.params) do
    equip_extra_info = equip_extra_info .. v.COLOR .. "\001" .. v.TYPETEXT .. "\001" .. v.VALUETEXT .. "\001"
  end
  local leftBTNText, rightBTNText, jumpBTNText
  if showType == k_equipmentInfoTypeLoad then
    self.m_rightCallBack = self.EquipmentOperatorLoadCallback
    rightBTNText = GameLoader:GetGameText("LC_MENU_EQUIPMENT_BUTTON")
  elseif showType == k_equipmentInfoTypeUnload then
    self.m_rightCallBack = self.EquipmentOperatorUnloadCallback
    rightBTNText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
    if self:isCurrentSlotCanEvolution() then
      jumpBTNText = GameLoader:GetGameText("LC_MENU_EVOLUTION_BUTTON")
      currentJumpBtnStatus = 1
    else
      jumpBTNText = GameLoader:GetGameText("LC_MENU_ENHANCE_BUTTON")
      currentJumpBtnStatus = 0
    end
    if not GameStateEquipEnhance:IsEnhanceEnable() then
      jumpBTNText = nil
    end
  elseif showType == k_equipmentInfoTypeBuy then
    self.m_rightCallBack = self.EquipmentOperatorBuyCallback
    rightBTNText = GameLoader:GetGameText("LC_MENU_BUY_CHAR")
  end
  if leftBTNText then
    btn_info = btn_info .. leftBTNText .. "\001"
  else
    btn_info = btn_info .. "\002" .. "\001"
  end
  if rightBTNText then
    btn_info = btn_info .. rightBTNText .. "\001"
  else
    btn_info = btn_info .. "\002" .. "\001"
  end
  if jumpBTNText then
    btn_info = btn_info .. jumpBTNText .. "\001"
  else
    btn_info = btn_info .. "\002" .. "\001"
  end
  DebugOut("item_main_info : " .. item_main_info)
  DebugOut("equip_extra_info : " .. equip_extra_info)
  DebugOut("btn_info : " .. btn_info)
  if showType == k_equipmentInfoTypeStrengthen then
    info_type = "S"
  else
    info_type = "N"
  end
  self:GetFlashObject():InvokeASCallback("_root", "setEquipmentInfo", item_main_info, equip_extra_info, btn_info, info_type, StrengthenExtParms_info)
  if showType == k_equipmentInfoTypeLoad then
    if self:isCurFleetInMatrix() then
      self:GetFlashObject():InvokeASCallback("_root", "setEnhanceRedPoint", true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setEnhanceRedPoint", false)
    end
  end
  if showType == k_equipmentInfoTypeUnload and not self:isCurrentSlotCanEvolution() then
    self:setEuipEnhanceRedPoint()
  end
end
function GameFleetNewEnhance:GenarateData(item)
  local param = {}
  param.CanPrim = true
  param.AdditionItem = item.number
  param.AdditionItemNum = item.needcnt
  param.AdditionItemCount = item.no
  param.AdditionProbability = GameFleetNewEnhance.StrengthenExtParms.AdditionProbability
  param.AdditionUseText = GameFleetNewEnhance.StrengthenExtParms.AdditionUseText
  param.DoPrimeText = GameFleetNewEnhance.StrengthenExtParms.DoPrimeText
  param.IsResetCheckbox = GameFleetNewEnhance.StrengthenExtParms.IsResetCheckbox
  param.LimitLv = GameFleetNewEnhance.StrengthenExtParms.LimitLv
  return param
end
function GameFleetNewEnhance:ShowPrimeRecoverConfirmBox(isUseAdditionItem)
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(GameFleetNewEnhance.PrimerHandler, isUseAdditionItem)
  local titleText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local contentText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_COVER_CONFIRM")
  GameUIMessageDialog:Display(titleText, contentText)
end
function GameFleetNewEnhance:ShowPrimeRecoverConfirmBox2(isUseAdditionItem, count)
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(GameFleetNewEnhance.PrimerHandler2, isUseAdditionItem .. "|" .. count)
  local titleText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local contentText = GameLoader:GetGameText("LC_MENU_INTERFERENCE_COVER_CONFIRM")
  GameUIMessageDialog:Display(titleText, contentText)
end
function GameFleetNewEnhance.PrimerHandler(isUseAdditionItem)
  GameFleetNewEnhance:DoPrime(isUseAdditionItem)
end
function GameFleetNewEnhance.PrimerHandler2(args)
  local isUseAdditionItem, count = unpack(LuaUtils:string_split(args, "|"))
  GameFleetNewEnhance:DoPrimeNew(isUseAdditionItem == "true", tonumber(count))
end
function GameFleetNewEnhance:DoPrime(isUseAdditionItem)
  item, itemInBag, itemInFleet = GameFleetNewEnhance:GetCurrentSelectItem(self.m_currentSelectEquipSlot, self.m_currentSelectBagSlot)
  if not item or not itemInFleet then
    return
  end
  DebugOut("GameFleetNewEnhance.StrengthenExtParms")
  DebugTable(GameFleetNewEnhance.StrengthenExtParms)
  local enchant_params = {
    equipment_id = itemInFleet.equip_id,
    item_id = item.item_type,
    addition_item_id = 0,
    addition_item_num = 0,
    addition_credits = 0,
    uplevel_count = 1
  }
  if isUseAdditionItem then
    if GameFleetNewEnhance.StrengthenExtParms.AdditionItem == 1 then
      enchant_params.addition_credits = GameFleetNewEnhance.StrengthenExtParms.AdditionItemNum
    else
      enchant_params.addition_item_id = GameFleetNewEnhance.StrengthenExtParms.AdditionItem
      enchant_params.addition_item_num = GameFleetNewEnhance.StrengthenExtParms.AdditionItemNum
    end
  end
  DebugOut("enchant_params")
  DebugTable(enchant_params)
  NetMessageMgr:SendMsg(NetAPIList.enchant_req.Code, enchant_params, GameFleetNewEnhance.PrimeCallBack, true, nil)
end
function GameFleetNewEnhance:DoPrimeNew(isUseAdditionItem, count)
  local list = self.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot]
  local item
  for k, v in pairs(list or {}) do
    if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
      item = v
      break
    end
  end
  local itemInFleet = self:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  if itemInFleet then
    local equipments = GameGlobalData:GetData("equipments")
    for k, v in pairs(equipments) do
      if itemInFleet.item_id == v.equip_id then
        itemInFleet = v
        break
      end
    end
  end
  DebugOut("GameFleetNewEnhance.StrengthenExtParms")
  DebugTable(GameFleetNewEnhance.StrengthenExtParms)
  local enchant_params = {
    equipment_id = itemInFleet.equip_id,
    item_id = item.item_type,
    addition_item_id = 0,
    addition_item_num = 0,
    addition_credits = 0,
    uplevel_count = count
  }
  if isUseAdditionItem then
    if GameFleetNewEnhance.StrengthenExtParms.AdditionItem == 1 then
      enchant_params.addition_credits = GameFleetNewEnhance.StrengthenExtParms.AdditionItemNum
    else
      enchant_params.addition_item_id = GameFleetNewEnhance.StrengthenExtParms.AdditionItem
      enchant_params.addition_item_num = GameFleetNewEnhance.StrengthenExtParms.AdditionItemNum
    end
  end
  DebugOut("enchant_params")
  DebugTable(enchant_params)
  NetMessageMgr:SendMsg(NetAPIList.enchant_req.Code, enchant_params, GameFleetNewEnhance.PrimeCallBack, true, nil)
end
function GameFleetNewEnhance.PrimeCallBack(msgType, content)
  if msgType == NetAPIList.enchant_ack.Code then
    DebugOut("GameFleetNewEnhance.PrimeCallBack")
    DebugTable(content)
    GameFleetNewEnhance:GetOnlyEquip()
    GameFleetNewEnhance:UpdateInterveneData(content.new_level)
    local diff_level = content.new_level - content.origin_level
    local diff_effect_value = content.new_effect_value - content.origin_effect_value
    local ArrowType
    if diff_level > 0 then
      if diff_level > 1 then
        ArrowType = "up_b"
      else
        ArrowType = "up_s"
      end
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_success"))
    elseif diff_level < 0 then
      ArrowType = "down_s"
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_faild"))
    else
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_interference_faild"))
    end
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if ArrowType and flashObj then
      flashObj:InvokeASCallback("_root", "ShowEquipStrengthenChangeArrow", ArrowType, diff_level, diff_effect_value, content.new_level)
    end
    return true
  end
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enchant_req.Code then
    DebugOut("GameFleetNewEnhance.PrimeCallBack common_ack")
    DebugTable(content)
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code))
    return true
  end
  return false
end
function GameFleetNewEnhance:isCurrentSlotCanEvolution()
  DebugOut("isCurrentSlotCanEvolution")
  local canEvolution = false
  if self.m_currentSelectEquipSlot and GameFleetNewEnhance.equipStatus then
    local equip = GameFleetNewEnhance:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
    if equip then
      local curStatus = GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot]
      DebugTable(curStatus)
      if curStatus and curStatus ~= -1 and curStatus.can_evolute == 1 then
        canEvolution = true
      end
    end
  end
  return canEvolution
end
function GameFleetNewEnhance:CheckFinishWanaAndSilvaTutorial()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].identity
  if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsFinished() and 1 == fleetId and GameFleetNewEnhance:IsEquipmentInSlot(2) then
    DebugOut("set finish QuestTutorialFirstGetWanaArmor")
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetActive(false, true)
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true)
    self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
  elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() and 4 == fleetId and GameFleetNewEnhance:IsEquipmentInSlot(1) and GameFleetNewEnhance:IsEquipmentInSlot(2) then
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetActive(false, true)
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true)
    self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
  end
end
function GameFleetNewEnhance.EquipmentOperatorLoadCallback()
  local equip = GameFleetNewEnhance.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot][GameFleetNewEnhance.m_currentSelectBagSlot]
  DebugOut("EquipmentOperatorLoadCallback", GameFleetNewEnhance.m_currentSelectBagSlot)
  DebugTable(equip)
  if equip then
    GameFleetNewEnhance:Equip(equip.item_id)
    GameFleetNewEnhance:CheckFinishWanaAndSilvaTutorial()
  end
end
function GameFleetNewEnhance.EquipmentOperatorUnloadCallback()
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  DebugOut("EquipmentOperatorUnloadCallback", equip, GameFleetNewEnhance.m_currentSelectEquipSlot)
  DebugTable(GameFleetNewEnhance.m_curFleetEquipments)
  if equip then
    DebugTable(equip)
    GameFleetNewEnhance:UnloadEquip(equip.item_id)
  end
end
function GameFleetNewEnhance.EquipmentOperatorBuyCallback()
  DebugOut("EquipmentOperatorBuyCallback", equip, GameFleetNewEnhance.m_currentSelectEquipSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id
  local shop_buy_and_wear_req_param = {
    formation_id = GameGlobalData:GetData("matrix").id,
    fleet_id = fleetId,
    equip_type = GameFleetNewEnhance.curBuyEquipType
  }
  NetMessageMgr:SendMsg(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetNewEnhance.BuyAndEquipCallback, true, nil)
  GameUtils:RecordForTongdui(NetAPIList.shop_buy_and_wear_req.Code, shop_buy_and_wear_req_param, GameFleetNewEnhance.BuyAndEquipCallback, true, nil)
end
function GameFleetNewEnhance:ShowEquipmentItem()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "clearListBox")
    self:GetFlashObject():InvokeASCallback("_root", "InitItemList")
    self:AddEquipmentItem()
  end
end
function GameFleetNewEnhance:AddEquipmentItem()
  if not self:GetFlashObject() then
    return
  end
  local minItemCount = (MAX_LINE - 1) * LINE_COUNT + 1
  local slot = self.m_currentSelectEquipSlot
  local equips = self.filterEquip[slot]
  DebugTable(self.filterEquip)
  local cnt = minItemCount
  if equips then
    cnt = math.max(#equips, minItemCount)
  end
  if cnt % 3 ~= 1 then
    cnt = math.floor((cnt - 1) / 3) * 3 + 1
  end
  DebugOut("AddEquipmentItem: cnt : " .. tostring(cnt))
  for id = cnt, 1, -LINE_COUNT do
    DebugOut("AddItemId: ", id, " max: ", math.max(GameItemBag:GetItemMaxPos(self.onlyEquip), minItemCount))
    self:GetFlashObject():InvokeASCallback("_root", "insertListItem", id)
  end
  self:GetFlashObject():InvokeASCallback("_root", "checkCanMove", cnt)
end
function GameFleetNewEnhance:IsEquipment(subId)
  local isEquip = true
  if not GameDataAccessHelper:GetEquipPrice(self.onlyEquip[subId].item_type) ~= -1 then
    isEquip = false
    if self.onlyEquip[subId].item_type > 2000 then
      isEquip = true
    end
  end
  return isEquip
end
function GameFleetNewEnhance:UpdateFleetLevelInfo()
  self:UpdateSelectedFleetInfo(self.m_currentSelectFleetIndex, true, k_fleetSwitchDirectionStay)
end
function GameFleetNewEnhance:CheckIsSelEvolutionItem(selectIndex)
  DebugOut("CheckIsSelEvolutionItem:", selectIndex, ",", self.m_currentSelectFleetIndex)
  if selectIndex and 1 == selectIndex and self.m_currentSelectFleetIndex == 1 and QuestTutorialEquipmentEvolution:IsActive() and not QuestTutorialEquipmentEvolution:IsFinished() and not QuestTutorialEquip:IsActive() then
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetShowEquipSlotTip", selectIndex, false)
      flashObj:InvokeASCallback("_root", "SetEvolutionBtnTipVisible", true)
      flashObj:InvokeASCallback("_root", "SetEvolutionTipVisible", true)
      if not self:IsEquipmentInSlot(selectIndex) then
        DebugOut("SetEquipBtnTipVisible:not in slot")
        flashObj:InvokeASCallback("_root", "SetEquipBtnTipVisible", true)
      end
    end
    DebugOut("CheckIsSelEvolutionItem:ShowTipAnimation")
  else
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetEvolutionBtnTipVisible", false)
      flashObj:InvokeASCallback("_root", "SetEvolutionTipVisible", false)
    end
  end
end
function GameFleetNewEnhance:CheckFinishEvolutionTutorial()
  DebugOut("CheckFinishEvolutionTutorial:", self.m_currentSelectFleetIndex, ",", QuestTutorialEquipmentEvolution:IsActive(), ",", QuestTutorialEquipmentEvolution:IsFinished())
  if self.m_currentSelectFleetIndex == 1 and QuestTutorialEquipmentEvolution:IsActive() and not QuestTutorialEquipmentEvolution:IsFinished() then
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "SetEvolutionBtnTipVisible", false)
      flashObj:InvokeASCallback("_root", "SetEvolutionTipVisible", false)
    end
    DebugOut("finish tutorial evolution")
    QuestTutorialEquipmentEvolution:SetFinish(true)
  end
end
function GameFleetNewEnhance.OnUpdateMatrix()
  GameFleetNewEnhance:GetOnlyEquip()
  GameFleetNewEnhance:InitOnMatrixChange()
  GameFleetNewEnhance.RefreshKrypton()
  GameFleetNewEnhance.refreshFleets()
end
function GameFleetNewEnhance:OnFSCommand(cmd, arg)
  if GameUtils:OnFSCommand(cmd, arg, self) then
    return
  end
  DebugOut("OnFSCommand", cmd)
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
  if cmd == "UpdateMarix" then
  end
  if cmd == "showSrcEquip" then
    DebugOut("GameFleetNewEnhance.enhance_equip")
    DebugTable(GameFleetNewEnhance.enhance_equip)
    self:showEnhanceEquipItemBox(arg, GameFleetNewEnhance.enhance_equip, GameFleetNewEnhance.enhance_equip_type)
  end
  if cmd == "showenhanceEquip" then
    self:showEnhanceEquipItemBox(arg, GameFleetNewEnhance.enhance_equip, GameFleetNewEnhance.enhance_equip_type)
  end
  if cmd == "showitemItembox" then
    self:showEnhanceItemItemBox(arg, nil, GameFleetNewEnhance.recipe_equip_type)
  end
  if cmd == "showdestEquip" then
    self:showEnhanceEquipItemBox(arg, GameFleetNewEnhance.evolution_equip, GameFleetNewEnhance.evolution_equip_type)
  end
  if cmd == "closePress" then
    if QuestTutorialEquipAllByOneKey:IsActive() then
      GameUICommonDialog:ForcePlayStory({11000313})
      return
    end
    if startQuickEquipment or startQuickUnload then
      return
    end
    self._AllMedalData = nil
    self.isEverReqMedal = nil
    if GameStateManager.GameStateEquipEnhance:IsObjectInState(GameUIStarSystemPort) then
      GameStateManager.GameStateEquipEnhance:EraseObject(GameUIStarSystemPort)
    end
    if GameStateManager.GameStateEquipEnhance.basePrveState then
      GameStateManager.GameStateEquipEnhance.previousState = GameStateManager.GameStateEquipEnhance.basePrveState
      GameStateManager.GameStateEquipEnhance.basePrveState = nil
    end
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateEquipEnhance.previousState)
    if GameStateManager.GameStateEquipEnhance.battle_failed then
      DebugOut("battle_failed")
      GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
      local param = {cli_data = "equip"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameStateManager.GameStateEquipEnhance.battle_failed = nil
    end
  elseif cmd == "EnhanceOpenOver" then
    if QuestTutorialEnhance:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowShild", true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowTutEnhance", true)
    end
    DebugOut("self.m_subState", self.m_subState)
    DebugOut(self.m_currentSelectEquipSlot)
    if self.m_subState == k_subStateFleetView or self.m_subState == k_subStateFleetEnhance and self.m_currentSelectEquipSlot ~= -1 then
      GameFleetNewEnhance:FleetEquipmentSelect(self.m_currentSelectEquipSlot)
    end
  elseif cmd == "help" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_HELP_CONTENT_32"))
  elseif cmd == "EquipmentSelect" then
    GameFleetNewEnhance.LastStrengthenItem = nil
    GameFleetNewEnhance.m_optionEquipItemId = -1
    local slot = tonumber(arg)
    if QuestTutorialEquip:IsActive() then
      if slot ~= 1 then
        return
      end
    elseif QuestTutorialEnhance:IsActive() then
      return
    elseif QuestTutorialEquipAllByOneKey:IsActive() then
      return
    end
    if self.m_subState == k_subStateFleetEnhance or self.m_subState == k_subStateFleetEvolution or self.m_subState == k_subStateFleetIntervene then
      self:EnhanceEquipmentSelect(slot)
    else
      GameFleetNewEnhance:EquipmentUnselectInBag()
      if self:IsEquipmentInSlot(slot) then
      elseif self:IsHasEquipmentToLoad(slot) and QuestTutorialEquip:IsActive() then
        DebugOut("GameFleetNewEnhance:EquipmentSelectInBag(1)")
        GameFleetNewEnhance:EquipmentSelectInBag(1)
      end
      self:FleetEquipmentSelect(slot)
      self:ShowEquipmentItem()
      self:ShowEquipmentInfo()
      self:SetSubState(k_subStateFleetEquipment)
    end
    self:RefreshEnhanceBarRedPoint()
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    if 2 == slot and TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
    end
    self:CheckIsSelEvolutionItem(slot)
  elseif cmd == "EquipmentBGPressed" then
    self:SetSubState(k_subStateFleetView)
    self:GetFlashObject():InvokeASCallback("_root", "PopEquipmentMenu", false)
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    if (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
      if 1 == fleetId and self.m_subState == k_subStateFleetView and nil == self:IsEquipmentInSlot(2) then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
      end
    elseif (not TutorialQuestManager.QuestTutorialEquip:IsActive() or TutorialQuestManager.QuestTutorialEquip:IsFinished()) and TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, false)
      if 4 == fleetId then
        if nil == self:IsEquipmentInSlot(1) then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, true)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
        elseif nil == self:IsEquipmentInSlot(2) then
          self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 2, true)
          self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
        end
      end
    end
  elseif cmd == "EquipmentUnselectOver" then
    self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipmentHilight", false)
    self:GetFlashObject():InvokeASCallback("_root", "SetShowPopEquipmentMenu", false)
  elseif cmd == "PrevFleet" then
    local isSwitch = false
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    if fleets ~= nil then
      local fleetIndex = self.m_currentSelectFleetIndex - 1
      local maxFleetIndex = LuaUtils:table_size(fleets)
      if fleetIndex > 0 and fleetIndex <= maxFleetIndex then
        self.m_switchDirection = k_fleetSwitchDirectionPrev
        self.m_currentSelectFleetIndex = fleetIndex
        self:UpdateFleetShowStack(k_fleetSwitchDirectionPrev)
        self:UpdateFleetShow(k_fleetSwitchDirectionPrev)
        isSwitch = true
      end
    end
    GameFleetNewEnhance:RefreshFleetBagAndSlot()
    if GameFleetNewEnhance.isMedalSlot then
      GameFleetNewEnhance:RefreshFleetMedal()
    end
    if isSwitch then
      self:GetFlashObject():InvokeASCallback("_root", "StartFleetSwitch", false)
    end
  elseif cmd == "NextFleet" then
    local isSwitch = false
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    if fleets ~= nil then
      local fleetIndex = self.m_currentSelectFleetIndex + 1
      local maxFleetIndex = LuaUtils:table_size(fleets)
      if fleetIndex > 0 and fleetIndex <= maxFleetIndex then
        self.m_switchDirection = k_fleetSwitchDirectionNext
        self.m_currentSelectFleetIndex = fleetIndex
        self:UpdateFleetShowStack(k_fleetSwitchDirectionNext)
        self:UpdateFleetShow(k_fleetSwitchDirectionNext)
        isSwitch = true
      end
    end
    GameFleetNewEnhance:RefreshFleetBagAndSlot()
    if GameFleetNewEnhance.isMedalSlot then
      GameFleetNewEnhance:RefreshFleetMedal()
    end
    if isSwitch then
      self:GetFlashObject():InvokeASCallback("_root", "StartFleetSwitch", true)
    end
  elseif cmd == "nextAnimOver" then
    self:UpdateSelectedFleetInfo(self.m_currentSelectFleetIndex, false, k_fleetSwitchDirectionStay)
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if maxFleetIndex <= self.m_currentSelectFleetIndex then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", true)
    end
    if 1 >= self.m_currentSelectFleetIndex then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", true)
    end
    if self.m_subState == k_subStateFleetEnhance then
      self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
    end
  elseif cmd == "prevAnimOver" then
    self:UpdateSelectedFleetInfo(self.m_currentSelectFleetIndex, false, k_fleetSwitchDirectionStay)
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local maxFleetIndex = LuaUtils:table_size(fleets)
    if 1 >= self.m_currentSelectFleetIndex then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPrevArrow", true)
    end
    if maxFleetIndex <= self.m_currentSelectFleetIndex then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", false)
    else
      self:GetFlashObject():InvokeASCallback("_root", "SetShowNextArrow", true)
    end
    if self.m_subState == k_subStateFleetEnhance then
      self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
    end
  elseif cmd == "EnhanceBarPressed" then
    if self:isCurrentSlotCanEvolution() and self:isEvolutionUseable() then
      DebugOut("jump evolution")
      nextState = k_subStateFleetEvolution
    else
      DebugOut("jump enhance")
      nextState = k_subStateFleetEnhance
    end
    local commanderAcademy = GameGlobalData:GetBuildingInfo("commander_academy")
    local disableEvolution = 0 >= commanderAcademy.level
    if self.m_subState ~= k_subStateFleetEnhance and self.m_subState ~= k_subStateFleetEvolution and self.m_subState ~= k_subStateFleetIntervene then
      self:SetSubState(nextState)
      self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPopEquipmentMenu", false)
      self:GetFlashObject():InvokeASCallback("_root", "EnhanceMove", true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
      self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", self:IsEquipmentInSlot(self.m_currentSelectEquipSlot), self.m_subState, disableEvolution, GameFleetNewEnhance.tab_content_tab2_text)
    else
      self:GetFlashObject():InvokeASCallback("_root", "EnhanceMove", false)
      self:SetSubState(k_subStateFleetView)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEquipmentHilight", false)
    end
  elseif cmd == "EnhanceTabSelect" then
    local tabNum = tonumber(arg)
    local commanderAcademy = GameGlobalData:GetBuildingInfo("commander_academy")
    if tabNum == 2 and 0 >= commanderAcademy.level then
      GameTip:Show(GameLoader:GetGameText("LC_ALERT_not_build_commander_academy"))
      return
    end
    if tabNum == 2 then
      DebugOut("EnhanceTabSelect Evolution")
      self:SetSubState(k_subStateFleetEvolution)
      self:CheckIsSelEvolutionItem(self.m_currentSelectEquipSlot)
    elseif tabNum == 3 then
      self:SetSubState(k_subStateFleetIntervene)
    else
      self:SetSubState(k_subStateFleetEnhance)
    end
    local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
    self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", equip ~= nil, self.m_subState, 0 >= commanderAcademy.level, GameLoader:GetGameText("LC_MENU_EVOLUTION_BUTTON"))
  elseif cmd == "Enhance" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "upgradeEquip" then
      GameUtils:HideTutorialHelp()
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].id
    local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
    if equip then
      if self:CheckCDCanEnhance() then
        local equipment_enhance_req_param = {
          equip_id = equip.item_id,
          fleet_id = fleetId,
          once_more_enhance = 0,
          en_level = 1
        }
        NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
        GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, self.equipmentEnhanceCallback, true, nil)
      else
        if not GameSettingData.EnablePayRemind then
          GameSettingData.EnablePayRemind = true
          GameUtils:SaveSettingData()
        end
        if GameSettingData.EnablePayRemind then
          local callbackFunc = self.clearCD
          GameUIGlobalScreen:ShowClearCD("clear_equipment_enhance_cd", callbackFunc)
        else
          self.clearCD()
        end
      end
    else
      DebugOut("get quip failed")
    end
  elseif cmd == "QuickEnhance" then
    if GameVip:IsFastEnhanceUnlocked() or Facebook:IsBindFacebook() then
      if self:CheckCDCanEnhance() then
        GameFleetNewEnhance.sendEquipQuickEnhanceInfoRequire()
      else
        if not GameSettingData.EnablePayRemind then
          GameSettingData.EnablePayRemind = true
          GameUtils:SaveSettingData()
        end
        if GameSettingData.EnablePayRemind then
          local callbackFunc = self.clearCD
          GameUIGlobalScreen:ShowClearCD("clear_equipment_enhance_cd", callbackFunc)
        else
          self.clearCD()
        end
      end
    elseif Facebook:IsFacebookEnabled() then
      if FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_ENHANCE] and FacebookPopUI.mFeatureSwitch[FACEBOOKFEATURESWITCH.SWITCH_ENHANCE] == 1 then
        DebugOut("FacebookPopUI.mLogInAwardCallback = lalal")
        FacebookPopUI.mLogInAwardCallback = self.CheckUnlockEnhaceall
        DebugOut("FacebookPopUI.mLogInAwardCallback lalalala2 =", FacebookPopUI.mLogInAwardCallback)
        FacebookPopUI:ShowFacebookMenu(FACEBOOKMENUTYPE.TYPE_MENU_LOGIN)
        FacebookPopUI:SetBindAwardType(FACEBOOKBINDAWARDTYPE.AWARD_UNLOCK_ENHANCEALL)
        if not ext.GetDeviceCountry or not ext.GetDeviceCountry() then
          local countryStr = "unknow"
        end
      else
        self:tipVipLevelNotEnoughForQuickEquipEnhance()
      end
    else
      self:tipVipLevelNotEnoughForQuickEquipEnhance()
    end
  elseif cmd == "sendQuickEnhanceRequire" then
    GameFleetNewEnhance.sendQuickEnhanceRequire(arg)
  elseif cmd == "Evolution" then
    if not self:isEvolutionUseable() then
      self:tipEvolllutionUnuseable()
      return
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].id
    local equip = self:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
    if equip then
      self:CheckFinishEvolutionTutorial()
      local equipment_evolution_req_param = {
        equip_id = equip.item_id,
        fleet_id = fleetId
      }
      DebugOut("Evolution")
      DebugTable(equipment_evolution_req_param)
      NetMessageMgr:SendMsg(NetAPIList.equipment_evolution_req.Code, equipment_evolution_req_param, self.equipmentEvolutionCallback, true, nil)
      GameUtils:RecordForTongdui(NetAPIList.equipment_evolution_req.Code, equipment_evolution_req_param, self.equipmentEvolutionCallback, true, nil)
    end
  elseif cmd == "BuyEquip" then
    self.BuyEquip()
  elseif cmd == "LeftEquip" then
    GameFleetNewEnhance.EquipmentOperatorLoadCallback()
  elseif cmd == "needUpdateItem" then
    local id = tonumber(arg)
    local frame, isEquip, number, StrengthenLv, showTip = "", "", "", "", ""
    local isSelected = ""
    local equipments = GameGlobalData:GetData("equipments")
    local tutorialTrigger1 = false
    local tutorialTrigger2 = false
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
      if 1 == fleetId then
        tutorialTrigger1 = true
      end
    elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and 4 == fleetId then
      tutorialTrigger2 = true
    end
    local findTipPos = false
    for i = 0, MAX_LINE - 1 do
      do
        local subId = id + i
        DebugOut("needUpdateItem: " .. subId .. " i " .. i .. " id " .. id)
        local slot = self.m_currentSelectEquipSlot
        local equip = self.filterEquip[slot][subId]
        if equip and not GameDataAccessHelper:IsItemUsedStrengthen(equip.item_type) then
          DebugOut("needUpdateItem is Quip")
          DebugTable(equip)
          if GameDataAccessHelper:IsItemUsedStrengthen(equip.item_type) then
            DebugOut("IsItemUsedStrengthen : " .. tostring("item_" .. self:GetDynamic(equip.item_type)))
            frame = frame .. "item_" .. equip.item_type .. "\001"
            isEquip = isEquip .. "true" .. "\001"
            number = number .. "x" .. equip.cnt .. "\001"
            StrengthenLv = StrengthenLv .. 0 .. "\001"
          else
            local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
              return v.equip_id == equip.item_id
            end))[1]
            DebugTable(currentEquipInfo)
            frame = frame .. tostring("item_" .. self:GetDynamic(currentEquipInfo.equip_type), id, i) .. "\001"
            isEquip = isEquip .. tostring(self:IsEquipment(subId)) .. "\001"
            number = number .. GameLoader:GetGameText("LC_MENU_Level") .. currentEquipInfo.equip_level .. "\001"
            StrengthenLv = StrengthenLv .. currentEquipInfo.enchant_level .. "\001"
            DebugOut("equip level", number)
          end
          if tutorialTrigger1 and not findTipPos and 100003 == equip.item_type then
            showTip = showTip .. "false" .. "\001"
            findTipPos = true
            local isShowEquipBtnTip = true
            if self:IsEquipmentInSlot(self.m_currentSelectEquipSlot) then
              isShowEquipBtnTip = false
            end
            self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", isShowEquipBtnTip)
          elseif not tutorialTrigger1 and tutorialTrigger2 then
            if 1 == id and 0 == i then
              showTip = showTip .. "false" .. "\001"
            else
              showTip = showTip .. "false" .. "\001"
            end
            local isShowEquipBtnTip = true
            if self:IsEquipmentInSlot(self.m_currentSelectEquipSlot) or self.m_currentSelectEquipSlot ~= 1 and self.m_currentSelectEquipSlot ~= 2 then
              isShowEquipBtnTip = false
            end
            self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", isShowEquipBtnTip)
          else
            showTip = showTip .. "false" .. "\001"
          end
          if self.m_currentSelectBagSlot == subId then
            isSelected = isSelected .. "true" .. "\001"
          else
            isSelected = isSelected .. "false" .. "\001"
          end
        else
          DebugOut("needUpdateItem is not Quip")
          frame = frame .. "empty" .. "\001"
          isEquip = isEquip .. "true" .. "\001"
          number = number .. 0 .. "\001"
          StrengthenLv = StrengthenLv .. 0 .. "\001"
          showTip = showTip .. "false" .. "\001"
          isSelected = isSelected .. "false" .. "\001"
        end
      end
    end
    DebugOut("needUpdateItem: ", frame)
    self:GetFlashObject():InvokeASCallback("_root", "setItem", id, frame, isEquip, number, StrengthenLv, showTip, isSelected)
  elseif cmd == "beginPress" then
    self.pressIndex = tonumber(arg)
  elseif cmd == "FleetInfoBagItemReleased" then
    if self.doubClicked then
      return
    end
    self:EquipmentSelectInBag(tonumber(arg))
    local currentReleaasedIndex = tonumber(arg)
    if currentReleaasedIndex == self.lastReleasedIndex and self.clicked then
      self.doubClicked = true
      self.clicked = false
    else
      self.clicked = true
      self.clickedTimer = GameUtils.DOUBLE_CLICK_TIMER
    end
    self.lastReleasedIndex = currentReleaasedIndex
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsFinished() and 1 == fleetId and self.m_currentSelectEquipSlot == 2 then
      if tonumber(arg) ~= -1 then
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
      end
    elseif TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() and not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsFinished() and 4 == fleetId then
      if tonumber(arg) ~= -1 then
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "SetEquipBtnTipVisible", false)
      end
    end
  elseif cmd == "endPress" then
    self.pressIndex = -1
  elseif cmd == "dragItem" then
    local param = LuaUtils:string_split(arg, "\001")
    local initX, initY, posX, posY = unpack(param)
    self.dragX = tonumber(initX)
    self.dragY = tonumber(initY)
    self.isDragEquipedItem = false
    DebugOut("self.m_currentSelectEquipSlot", self.m_currentSelectEquipSlot, self.pressIndex)
    if self.pressIndex ~= -1 and self.pressIndex ~= nil and self.m_currentSelectEquipSlot ~= -1 then
      local slot = self.m_currentSelectEquipSlot
      local equip = self.filterEquip[slot][self.pressIndex]
      if equip then
        local item_id = equip.item_id
        local item_type = GameDataAccessHelper:GetItemRealType(equip)
        self.dragItem = true
        GameStateManager:GetCurrentGameState():BeginDragItem(item_id, item_type, initX, initY, posX, posY)
      end
    end
  elseif cmd == "dragEquipment" then
    local param = LuaUtils:string_split(arg, "\001")
    local initX, initY, posX, posY = unpack(param)
    self.dragX = tonumber(initX)
    self.dragY = tonumber(initY)
    local slot = self.m_currentSelectEquipSlot
    local equip = GameFleetNewEnhance:IsEquipmentInSlot(slot)
    local item_id = equip.item_id
    local item_type = equip.item_type
    self.dragItem = true
    self.isDragEquipedItem = true
    GameStateManager:GetCurrentGameState():BeginDragItem(item_id, item_type, initX, initY, posX, posY)
  elseif cmd == "dragUnloadEquipment" then
    local isUnload = tonumber(arg)
    if isUnload == 1 then
      GameFleetNewEnhance.EquipmentOperatorUnloadCallback()
      self.isUnloadEquipedItem = true
    else
      self.isUnloadEquipedItem = false
    end
  elseif cmd == "LeftBTNPress" then
    if self.m_leftCallBack then
      self.m_leftCallBack()
    end
  elseif cmd == "RightBTNPress" then
    if self.m_rightCallBack then
      self.m_rightCallBack()
    end
  elseif cmd == "JumpBTNPress" then
    local nextState
    if self:isCurrentSlotCanEvolution() then
      nextState = k_subStateFleetEvolution
    else
      DebugOut("jump enhance")
      nextState = k_subStateFleetEnhance
    end
    local commanderAcademy = GameGlobalData:GetBuildingInfo("commander_academy")
    local disableEvolution = 0 >= commanderAcademy.level
    if self.m_subState ~= k_subStateFleetEnhance and self.m_subState ~= k_subStateFleetEvolution and self.m_subState ~= k_subStateFleetIntervene then
      self:SetSubState(nextState)
      self:EnhanceEquipmentSelect(self.m_currentSelectEquipSlot, true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowPopEquipmentMenu", false)
      self:GetFlashObject():InvokeASCallback("_root", "EnhanceMove", true)
      self:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
      self:GetFlashObject():InvokeASCallback("_root", "showEquipEnhance", self:IsEquipmentInSlot(self.m_currentSelectEquipSlot), self.m_subState, disableEvolution, GameFleetNewEnhance.tab_content_tab2_text)
    end
  elseif cmd == "CurrentEquipmentPressed" then
    self:EquipmentUnselectInBag()
    self:ShowEquipmentInfo()
  elseif cmd == "AutoEquipment" then
    if self.m_subState == k_subStateFleetEnhance or self.m_subState == k_subStateFleetEvolution then
      return
    end
    if self:CheckCanUnloadAll() then
      self:UnloadAllEquip()
    else
      DebugOut("xxx33333333")
      if GameUtils:GetTutorialHelp() then
        GameUtils:HideTutorialHelp()
      end
      self:AllEquipAction()
      if QuestTutorialEquipAllByOneKey:IsActive() then
        self:GetFlashObject():InvokeASCallback("_root", "SetShowTipAutoEquipment", false)
      end
    end
  elseif cmd == "showFleetPopView" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.m_currentSelectFleetIndex]
    local leaderlist = GameGlobalData:GetData("leaderlist")
    local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
    if leaderlist and curMatrixIndex and fleet.identity == 1 then
      ItemBox:ShowCommanderDetail2(leaderlist.leader_ids[curMatrixIndex], fleet, 1)
    else
      ItemBox:ShowCommanderDetail2(fleet.identity, fleet, 1)
    end
  elseif cmd == "enterReform" then
    if self.m_subState == k_subStateFleetEnhance or self.m_subState == k_subStateFleetEvolution then
      return
    end
    local level_info = GameGlobalData:GetData("levelinfo")
    if immanentversion170 == nil then
      if level_info.level < 35 then
        GameTip:Show(GameLoader:GetGameText("LC_ALERT_REFORM_LOCKED_REQUIRE_LEVER_ALERT"))
        return
      end
    elseif (immanentversion170 == 4 or immanentversion170 == 5) and not GameUtils:IsModuleUnlock("reform") then
      local needLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("reform")
      local str = string.format(GameLoader:GetGameText("LC_MENU_Evolution_requirement"), needLevel)
      if needLevel <= level_info.level then
        str = GameLoader:GetGameText("LC_MENU_reform_DESC")
      end
      GameTip:Show(str)
      return
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleet = fleets[self.m_currentSelectFleetIndex]
    GameUIFleetGrowUp:Init(fleet.identity)
    GameStateEquipEnhance:GoToSubState(SUB_STATE.STATE_GROW_UP)
  elseif cmd == "doPrime" then
    local item, itemInBag, itemInFleet = GameFleetNewEnhance:GetCurrentSelectItem(self.m_currentSelectEquipSlot, self.m_currentSelectBagSlot)
    if item and itemInFleet then
      DebugOut("itemInFleet.enchant_item_id = ", itemInFleet.enchant_item_id)
      DebugOut("item.item_type = ", item.item_type)
      if item.item_type ~= itemInFleet.enchant_item_id and itemInFleet.enchant_item_id ~= 0 and itemInFleet.enchant_effect_value ~= 0 then
        GameFleetNewEnhance:ShowPrimeRecoverConfirmBox(arg == "true")
      else
        GameFleetNewEnhance:DoPrime(arg == "true")
      end
    end
  elseif cmd == "doPrimeNew" then
    local list = self.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot]
    local item
    for k, v in pairs(list or {}) do
      if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
        item = v
        break
      end
    end
    local itemInFleet = self:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
    if itemInFleet then
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInFleet.item_id == v.equip_id then
          itemInFleet = v
          break
        end
      end
    end
    if item and itemInFleet then
      DebugOut("itemInFleet.enchant_item_id = ", itemInFleet.enchant_item_id)
      DebugOut("item.item_type = ", item.item_type)
      if item.item_type ~= itemInFleet.enchant_item_id and itemInFleet.enchant_item_id ~= 0 and itemInFleet.enchant_effect_value ~= 0 then
        GameFleetNewEnhance:ShowPrimeRecoverConfirmBox2(arg, 1)
      else
        GameFleetNewEnhance:DoPrimeNew(arg == "true", 1)
      end
    end
  elseif cmd == "doOneKeyPrimeNew" then
    GameFleetNewEnhance.openQuickInterveneUI()
  elseif cmd == "sendQuickInterveneRequire" then
    local isUseAdditionItem, count = unpack(LuaUtils:string_split(arg, "|"))
    local list = self.filterEquip[GameFleetNewEnhance.m_currentSelectEquipSlot]
    local item
    for k, v in pairs(list or {}) do
      if GameDataAccessHelper:IsItemUsedStrengthen(v.item_type) then
        item = v
        break
      end
    end
    local itemInFleet = self:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
    if itemInFleet then
      local equipments = GameGlobalData:GetData("equipments")
      for k, v in pairs(equipments) do
        if itemInFleet.item_id == v.equip_id then
          itemInFleet = v
          break
        end
      end
    end
    if item and itemInFleet then
      DebugOut("itemInFleet.enchant_item_id = ", itemInFleet.enchant_item_id)
      DebugOut("item.item_type = ", item.item_type)
      if item.item_type ~= itemInFleet.enchant_item_id and itemInFleet.enchant_item_id ~= 0 and itemInFleet.enchant_effect_value ~= 0 then
        GameFleetNewEnhance:ShowPrimeRecoverConfirmBox2(isUseAdditionItem, tonumber(count))
      else
        GameFleetNewEnhance:DoPrimeNew(isUseAdditionItem == "true", tonumber(count))
      end
    end
  elseif cmd == "gotoBtnClick" then
    DebugOut("currentShowBluePrint", currentShowBluePrint)
    DebugTable(GameData.Item.Keys[currentShowBluePrint])
    local level = GameData.Item.Keys[currentShowBluePrint].REQ_LEVEL
    local GameObjectAdventure = LuaObjectManager:GetLuaObject("GameObjectAdventure")
    if immanentversion170 == 4 or immanentversion170 == 5 then
      GameObjectAdventure:gotoBossAreaDirectly170(currentShowBluePrint)
    else
      GameObjectAdventure:gotoBossAreaDirectly(level)
    end
  elseif cmd == "AdjutantClicked" then
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    GameUIAdjutant.mFleetIdBeforeEnter = fleetId
    GameUIAdjutant:EnterAdjutantUI()
    GameUIAdjutant.IsShowAdjutantUI = true
  elseif cmd == "helpTutorial_autoEquip" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "putonEquip" then
      local param = LuaUtils:string_split(arg, "\001")
      if self:CheckCanUnloadAll() then
        DebugOut("1111111111")
        GameUtils:MaskLayerMidTutorial(GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_8"))
      else
        DebugOut("2222222222")
        GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.left, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_7"), GameUIMaskLayer.MaskStyle.small)
      end
    end
  elseif cmd == "helpTutorial_upgrade" then
    if GameUtils:GetTutorialHelp() and GameUtils:GetTutorialName() == "upgradeEquip" then
      local param = LuaUtils:string_split(arg, "\001")
      local id = GameFleetNewEnhance:TheFirstHaveEquipmentInSlotId()
      if id == -1 then
        GameUtils:MaskLayerMidTutorial(GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_26"))
      else
        GameUtils:ShowMaskLayer(tonumber(param[1]), tonumber(param[2]), GameUIMaskLayer.ButtonPos.right, GameLoader:GetGameText("LC_MENU_ASSISTANT_INFO_10"), GameUIMaskLayer.MaskStyle.small)
      end
    end
  elseif cmd == "GotoMasterPage" then
    if self.m_currentFleetsId and self.m_currentFleetsId ~= "" then
      GameUIMaster.CurrentScope = {
        [1] = tonumber(self.m_currentFleetsId)
      }
      GameUIMaster.CurrentSystemType = GameUIMaster.MasterSystem.EquipMaster
      GameUIMaster.CurrentPageType = GameUIMaster.MasterPage.equip_enchance
      GameUIMaster.CurMatrixId = curMatrixIndex
      GameStateManager:GetCurrentGameState():AddObject(GameUIMaster)
    end
  elseif cmd == "enterArtifact" then
    if self.m_subState == k_subStateFleetEnhance or self.m_subState == k_subStateFleetEvolution then
      return
    end
    local level_info = GameGlobalData:GetData("levelinfo")
    if GameGlobalData:GetModuleStatus("artifact") == false then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ARTIFACT_LOCKED"))
      return
    end
    if level_info.level < 90 then
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_LEVEL_FUNCTION_OPEN_INFO"), 90))
      return
    end
    local fleets = GameGlobalData:GetData("fleetinfo").fleets
    local fleetId = fleets[self.m_currentSelectFleetIndex].identity
    local power = fleets[self.m_currentSelectFleetIndex].force
    local GameArtifact = LuaObjectManager:GetLuaObject("GameArtifact")
    DebugOut("here")
    DebugOut("tps:fleetid" .. fleetId)
    GameArtifact:Show(fleetId, power)
  elseif cmd == "changePrime" then
    local pt = LuaUtils:string_split(arg, "!")
    local cur_item, itemInBag, itemInFleet = GameFleetNewEnhance:GetCurrentSelectItem(self.m_currentSelectEquipSlot, self.m_currentSelectBagSlot)
    local items = GameFleetNewEnhance.StrenthenList
    if items ~= nil and #items > 0 then
      local _items = items
      local function callback(item)
        GameFleetNewEnhance.StrengthenExtParms = GameFleetNewEnhance:GenarateData(item)
        GameFleetNewEnhance.inventedData.StrengthenExtParms_info.AdditionItem = GameFleetNewEnhance.StrengthenExtParms.AdditionItem
        GameFleetNewEnhance.inventedData.StrengthenExtParms_info.AdditionItemNum = GameFleetNewEnhance.StrengthenExtParms.AdditionItemNum
        GameFleetNewEnhance.inventedData.StrengthenExtParms_info.AdditionItemCount = GameFleetNewEnhance.StrengthenExtParms.AdditionItemCount
        GameFleetNewEnhance:UpdateIconAndRate(item)
      end
      ItemBox:ShowUseChoiceBox(pt[1], pt[2], _items, callback)
    end
  elseif cmd == "showKrypton" then
    DebugOut("showKrypton:", self.m_subState)
    if self.m_subState ~= k_subStateFleetKrypton then
      self:SetSubState(k_subStateFleetKrypton)
      GameFleetNewEnhance.RefreshKrypton()
    end
  elseif cmd == "hideKrypton" then
    self:SetSubState(k_subStateFleetKrypton)
  elseif cmd == "switchFormation" then
    local GameStatePlayerMatrix = GameStateManager.GameStatePlayerMatrix
    if GameStatePlayerMatrix.basePrveState == nil and GameStateManager.GameStateEquipEnhance.basePrveState == nil then
      GameStateManager.GameStateEquipEnhance.basePrveState = GameStateManager.GameStateEquipEnhance.previousState
    end
    GameStateManager:SetCurrentGameState(GameStatePlayerMatrix)
  elseif cmd == "changeShip" then
    GameFleetNewEnhance:OnChangeShip()
  elseif cmd == "update_changefleet_item" then
    GameFleetNewEnhance:UpdateChangeFleetsItem(tonumber(arg))
  elseif cmd == "selectFleet" then
    GameFleetNewEnhance:OnChangeFleet(tonumber(arg))
  elseif cmd == "beginFleetPress" then
    self.pressFleetSlot = tonumber(arg)
  elseif cmd == "endFleetPress" then
    self.pressFleetSlot = -1
  elseif cmd == "beginBagPress" then
    self.pressBagSlot = tonumber(arg)
    self.onSelectBagSlot = tonumber(arg)
  elseif cmd == "endBagPress" then
    self.pressBagSlot = -1
  elseif cmd == "BagItemClicked" then
    GameFleetNewEnhance:BagItemClicked(arg)
    self:UpdataFleetSlotState(self.onSelectBagSlot, false)
  elseif cmd == "FleetKryptonItemReleased" then
    self:FleetItemReleased(arg)
  elseif cmd == "NeedUpdateBagListItem" then
    GameFleetNewEnhance:UpdateBagListItem(tonumber(arg))
  end
  if cmd == "drag_bag_item_start" then
    local param = LuaUtils:string_split(arg, "\001")
    local newArg = "" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
    self:dragKryptonItem("dragBagItem", newArg)
  elseif cmd == "dragFleetItem" or cmd == "dragBagItem" then
    self:dragKryptonItem(cmd, arg)
  elseif cmd == "moveout2end" then
    local flashObj = GameFleetNewEnhance:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "moveOut2End")
    end
  end
  if cmd == "chargePress" then
    if not self.currentChargeItemId then
      return
    end
    local isInFleet, identity = false, -1
    for k, v in pairs(self.currentFleetKrypton) do
      if v.id == self.currentChargeItemId then
        isInFleet = true
      end
    end
    if isInFleet then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      identity = fleets[self.m_currentSelectFleetIndex].identity
    end
    local krypton_enhance_req_param = {
      id = self.currentChargeItemId,
      fleet_identity = identity
    }
    GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_enhance_req.Code, krypton_enhance_req_param, self.enhanceKryptonCallback, true, nil)
  end
  if cmd == "ShowMedalMenu" then
    GameFleetNewEnhance.isMedalSlot = true
    if self._AllMedalData == nil or self._AllMedalData.formations == nil or self._AllMedalData.medals == nil or not self.isEverReqMedal then
      local function netFailedCallback()
        NetMessageMgr:SendMsg(NetAPIList.medal_equip_req.Code, nil, self.RequestMedalDataCallBack, true, netFailedCallback)
      end
      self._AllMedalData = {}
      self.isEverReqMedal = true
      NetMessageMgr:SendMsg(NetAPIList.medal_equip_req.Code, nil, self.RequestMedalDataCallBack, true, netFailedCallback)
    else
      self:RefreshFleetMedal()
    end
  elseif cmd == "ShowEquipMenu" then
    GameFleetNewEnhance.isMedalSlot = false
    self:SetMedalButtonNews()
  elseif cmd == "ShowOpenTip" then
    GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_DEXTER_INFO_2"), 80))
  elseif cmd == "MedalSelected" then
    DebugOutPutTable(GameFleetNewEnhance.curFleetMedals, "MedalSelected slot:" .. arg)
    local medal_id = GameFleetNewEnhance.curFleetMedals[tonumber(arg)]
    local medal_detail
    for k, v in ipairs(self._AllMedalData.medals) do
      if v.id == medal_id then
        medal_detail = v
      end
    end
    if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarSystemPort) then
      GameStateManager:GetCurrentGameState():AddObject(GameUIStarSystemPort)
    end
    self.curSelectedMedal = medal_id
    self.curSelectedSlot = tonumber(arg)
    DebugOut("MedalSelected", medal_id)
    DebugTable(medal_detail)
    GameUIStarSystemPort.medalEquip:GetTargetPosMedalList(self.curSelectedSlot)
    GameUIStarSystemPort.medalEquip:ShowMedalDetail(medal_detail, true, false, GameFleetNewEnhance:isCurFleetInMatrix())
  elseif cmd == "MedalSlotClicked" then
    if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") and tonumber(arg) == 1 then
      self:GetFlashObject():InvokeASCallback("_root", "SetShowMedelTip", 1, false)
    end
    self.curSelectedSlot = tonumber(arg)
    self:ShowMedalSelectMenu(tonumber(arg))
  end
end
function GameFleetNewEnhance.enhanceKryptonCallback(msgtype, content)
  if msgtype == NetAPIList.krypton_enhance_ack.Code then
    local kry = content.krypton
    local isInFleet = false
    if kry and kry.id then
      for k, v in pairs(GameFleetNewEnhance.kryptonBag) do
        if v.id == kry.id then
          GameFleetNewEnhance.kryptonBag[k] = kry
        end
      end
      for k, v in pairs(GameFleetNewEnhance.currentFleetKrypton) do
        if v.id == kry.id then
          GameFleetNewEnhance.currentFleetKrypton[k] = kry
          isInFleet = true
        end
      end
      ItemBox:SetKryptonBox(content.krypton, GameFleetNewEnhance:GetKryptonType(content.krypton), #kry.formation)
      ItemBox:ShowKryptonLevelUp()
    end
    GameFleetNewEnhance:RefreshBagKrypton()
    GameFleetNewEnhance:RefreshCurFleetKrypton()
    if not isInFleet then
      GameFleetNewEnhance:UpdataFleetSlotState(GameFleetNewEnhance.onSelectBagSlot, false)
    end
    return true
  elseif msgtype == NetAPIList.common_ack.Code then
    if GameFleetNewEnhance.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function GameFleetNewEnhance:UpdataFleetSlotState(kryptonId, isEnd)
  local currentSelectKrypton = self.kryptonBag[kryptonId]
  if currentSelectKrypton then
    self:CheckKryptonWithFleetKrypton(currentSelectKrypton, isEnd)
  end
end
function GameFleetNewEnhance:CheckKryptonWithFleetKrypton(krypton, isEnd)
  self:CheckInitGameData()
  local kryptonLabInfo = GameGlobalData:GetBuildingInfo("krypton_center")
  local state = 0
  if not isEnd then
    state = self:GetCurrentKryptonInFleetKryptonState(krypton)
  end
  local allSlotState = self:GetFleetKryptonState(state, krypton)
  local lockLevel = ""
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= kryptonLabInfo.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  DebugOut("state = " .. state)
  DebugOut("allSlotState = " .. allSlotState)
  DebugOut("lockLevelText = " .. lockLevel)
  DebugOutPutTable(kryptonLabInfo, "kryptonLabInfo ")
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "UpdateEquipmentState", allSlotState, lockLevel)
  end
end
function GameFleetNewEnhance:GetFleetKryptonState(state, currentKrypton)
  local slotState = {}
  local slotCount = 8
  if state == 0 then
    for i = 1, slotCount do
      local _state = "no"
      table.insert(slotState, _state)
    end
  elseif state == 1 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "no"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  elseif state == 2 then
    for i = 1, slotCount do
      local fleetkrypton = self.currentFleetKrypton[i]
      local _state = ""
      if fleetkrypton then
        if self:GetTwoKrytponAttribute(currentKrypton, fleetkrypton) then
          _state = "not"
        else
          _state = "cannot"
        end
      else
        _state = "cannot"
      end
      table.insert(slotState, _state)
    end
  end
  return ...
end
function GameFleetNewEnhance:FleetItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local item = self.currentFleetKrypton[slot]
  local operateText = GameLoader:GetGameText("LC_MENU_UNLOAD_BUTTON")
  local function operateFunc()
    DebugTable(item)
    if GameFleetNewEnhance:GetBagSlot() > 0 then
      self:EquipOffKrypton(item.id, GameFleetNewEnhance:GetBagSlot())
    else
      local tip = GameLoader:GetGameText("LC_ALERT_krypton_store_full")
      GameTip:Show(tip)
    end
  end
  GameFleetNewEnhance.UnloadKrypton = operateFunc
  local operationTable = {}
  operationTable.btnUnloadVisible = true
  operationTable.btnUpgradeVisible = true
  if item then
    GameFleetNewEnhance.decomposeId = item.id
    ItemBox:SetKryptonBox(item, GameFleetNewEnhance:GetKryptonType(item), #item.formation)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function GameFleetNewEnhance:dragKryptonItem(cmd, arg)
  local param = LuaUtils:string_split(arg, "\001")
  local initPosX, initPosY, posX, posY = unpack(param)
  self.dragX = tonumber(posX)
  self.dragY = tonumber(posY)
  self.initPosX = tonumber(initPosX)
  self.initPosY = tonumber(initPosX)
  local pressIndex = -1
  if cmd == "dragFleetItem" then
    pressIndex = self.pressFleetSlot
  elseif cmd == "dragBagItem" then
    pressIndex = self.pressBagSlot
  end
  DebugOut("pressIndex: ", pressIndex)
  if pressIndex ~= -1 then
    local item
    if cmd == "dragFleetItem" then
      item = self.currentFleetKrypton[pressIndex]
    else
      item = self.kryptonBag[pressIndex]
    end
    DebugOut("item: ", item)
    if item then
      if cmd == "dragFleetItem" then
        self.dragBagItem = false
        self.dragFleetItem = true
      elseif cmd == "dragBagItem" then
        self.dragFleetItem = false
        self.dragBagItem = true
        self.dragBagKrypton = true
      end
      GameStateManager:GetCurrentGameState():BeginDragItem(item.id, GameFleetNewEnhance:GetKryptonType(item), initPosX, initPosY, posX, posY)
    else
      GameStateEquipEnhance:onEndDragItem()
    end
  end
end
function GameFleetNewEnhance.UpdateBagListItemCallback(content)
  local baseIdx = tonumber(content.itemId)
  local k = tonumber(content.index)
  local frame = ""
  local level = ""
  local idx = (baseIdx - 1) * 4 + k
  local item = GameFleetNewEnhance.kryptonBag[idx]
  local percent = 1
  if item then
    local id = GameFleetNewEnhance:GetKryptonType(item)
    frame = "item_" .. id
    level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
    percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
    if percent > 101 then
      percent = 101
    end
  else
    frame = "empty"
    level = "0"
  end
  local isHide = false
  if idx > GameFleetNewEnhance.grid_capacity then
    isHide = true
  end
  local one = {
    isHide = isHide,
    fr = frame,
    lv = level,
    progress = percent
  }
  local fourItem = {}
  fourItem[k] = one
  local flashObj = GameFleetNewEnhance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", content.itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function GameFleetNewEnhance:UpdateBagListItem(itemId)
  local baseIdx = tonumber(itemId)
  local fourItem = {}
  for k = 1, 4 do
    local frame = ""
    local level = ""
    local idx = (baseIdx - 1) * 4 + k
    local item = GameFleetNewEnhance.kryptonBag[idx]
    local percent = 1
    if item then
      local id = GameFleetNewEnhance:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, {itemId = itemId, index = k}, GameFleetNewEnhance.UpdateBagListItemCallback)
        end
      else
        frame = "item_" .. id
      end
      level = GameLoader:GetGameText("LC_MENU_Level") .. item.level
      percent = math.floor(item.refine_exp / item.refine_need_exp * 100) + 1
      if percent > 101 then
        percent = 101
      end
    else
      frame = "empty"
      level = "0"
    end
    local isHide = false
    if idx > GameFleetNewEnhance.grid_capacity then
      isHide = true
    end
    local one = {
      isHide = isHide,
      fr = frame,
      lv = level,
      progress = percent
    }
    fourItem[k] = one
  end
  DebugOut("fourItem")
  DebugTable(fourItem)
  local flashObj = GameFleetNewEnhance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateBagListItem", itemId, fourItem[1], fourItem[2], fourItem[3], fourItem[4])
  end
end
function GameFleetNewEnhance:BagItemClicked(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local idx = (tonumber(param[1]) - 1) * 4 + tonumber(param[2])
  local newArg = tostring(idx) .. "\001" .. param[3] .. "\001" .. param[4] .. "\001" .. param[5] .. "\001" .. param[6]
  GameFleetNewEnhance:BagItemReleased(newArg)
end
function GameFleetNewEnhance:GetCurrentKryptonInFleetKryptonState(currentKrypton)
  local currentkryptonAttributeCount = LuaUtils:table_size(currentKrypton.addon)
  local hasTheSameAttributeSlotCount = 0
  for _, v in pairs(self.currentFleetKrypton) do
    if self:GetTwoKrytponAttribute(currentKrypton, v) then
      hasTheSameAttributeSlotCount = hasTheSameAttributeSlotCount + 1
    end
  end
  return hasTheSameAttributeSlotCount
end
function GameFleetNewEnhance:GetTwoKrytponAttribute(krypton1, krypton2)
  local krypton1AttributeCount = LuaUtils:table_size(krypton1.addon)
  local count = 0
  for _, v1 in pairs(krypton1.addon) do
    for _, v2 in pairs(krypton2.addon) do
      if v1.type == v2.type then
        count = count + 1
        break
      end
    end
  end
  return count > 0
end
function GameFleetNewEnhance.UseKrypton()
  GameFleetNewEnhance:CheckInitGameData()
  local destSlot = 0
  local building = GameGlobalData:GetBuildingInfo("krypton_center")
  local item = GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.onSelectBagSlot]
  local state = GameFleetNewEnhance:GetCurrentKryptonInFleetKryptonState(item)
  if state == 0 then
    for i = 1, 8 do
      if GameFleetNewEnhance.kryptonSlotLevel[i].labLevel <= building.level and not GameFleetNewEnhance.currentFleetKrypton[i] then
        destSlot = i
        break
      end
    end
  end
  if destSlot == 0 then
    for i = 8, 1, -1 do
      if state == 1 then
        if GameFleetNewEnhance.currentFleetKrypton[i] and GameFleetNewEnhance:GetTwoKrytponAttribute(item, GameFleetNewEnhance.currentFleetKrypton[i]) then
          destSlot = i
          DebugOut("destSlot = " .. destSlot)
          break
        end
      elseif GameFleetNewEnhance.kryptonSlotLevel[i].labLevel <= building.level then
        destSlot = i
      end
    end
  end
  if destSlot > 0 then
    GameFleetNewEnhance:EquipKrypton(GameFleetNewEnhance.decomposeId, destSlot)
  end
end
function GameFleetNewEnhance:EquipKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.equipSrcSlot = v.slot
    end
  end
  self.equipDestSlot = destSlot
  DebugOut("EquipKrypton: ", self.equipSrcSlot, " ", self.equipDestSlot)
  local krypton_equip_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipDestSlot
  }
  local function netFailedCallback()
    GameStateEquipEnhance:onEndDragItem()
    GameFleetNewEnhance:RequestKryptonInBag()
  end
  DebugTable(krypton_equip_req_param)
  GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_equip_req.Code, krypton_equip_req_param, GameFleetNewEnhance.kryptonEquipCallback, true, netFailedCallback)
end
function GameFleetNewEnhance:BagItemReleased(arg)
  local param = LuaUtils:string_split(arg, "\001")
  local slot = tonumber(param[1])
  local _x, _y, _w, _h = tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), tonumber(param[5])
  local boxType = "Krypton"
  local operateLeftText, operateLeftFunc
  local item = self.kryptonBag[slot]
  local operationTable = {}
  local number = 0
  operateLeftText = GameLoader:GetGameText("LC_MENU_USE_CHAR")
  operateLeftFunc = self.UseKrypton
  operationTable.btnUseVisible = true
  operationTable.btnUpgradeVisible = true
  number = #item.formation
  if item then
    GameFleetNewEnhance.decomposeId = item.id
    ItemBox:SetKryptonBox(item, GameFleetNewEnhance:GetKryptonType(item), number)
    ItemBox:ShowKryptonBox(_x, _y, operationTable)
    self:SetCurrentChargeKrypton(item.id)
  end
end
function GameFleetNewEnhance:SetCurrentChargeKrypton(kryptonId)
  self.currentChargeItemId = kryptonId
  self:RefreshChargeKrypton()
end
function GameFleetNewEnhance:RefreshChargeKrypton()
  if self.currentChargeItemId then
    local item
    for k, v in pairs(self.kryptonBag) do
      if v.id == self.currentChargeItemId then
        item = v
      end
    end
    if item == nil then
      for k, v in pairs(self.currentFleetKrypton) do
        if v.id == self.currentChargeItemId then
          item = v
        end
      end
    end
    if item then
      if not item.kenergy_upgrade then
        item.kenergy_upgrade = 0
      end
      local frame = ""
      local id = GameFleetNewEnhance:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", frame, item.kenergy_upgrade)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "setChargeItem", "empty", 0)
  end
end
function GameFleetNewEnhance:OnChangeFleet(newfleetID)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  if fleetId == 1 then
    local param = {}
    param.id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_leader_req.Code, param, GameFleetNewEnhance.ChangeFleetCallBack, false)
  else
    GameFleetNewEnhance.newSelectedFleetID = newfleetID
    GameFleetNewEnhance.activeNewSelectedFleet = false
    local param = {}
    param.matrix_index = curMatrixIndex
    param.old_fleet_id = fleetId
    param.new_fleet_id = newfleetID
    NetMessageMgr:SendMsg(NetAPIList.change_fleets_req.Code, param, GameFleetNewEnhance.ChangeFleetCallBack, true, nil)
  end
end
function GameFleetNewEnhance:OnChangeShip()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local param = {}
  param.matrix_index = curMatrixIndex
  param.fleet_id = fleetId
  NetMessageMgr:SendMsg(NetAPIList.change_fleets_data_req.Code, param, GameFleetNewEnhance.ChangeFleetDataCallBack, true, nil)
end
function GameFleetNewEnhance.ChangeFleetCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.change_fleets_req.Code or content.api == NetAPIList.change_leader_req.Code) then
    if content.code ~= 0 then
      GameFleetNewEnhance.newSelectedFleetID = nil
      GameFleetNewEnhance.activeNewSelectedFleet = false
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    if GameFleetNewEnhance.isMedalSlot then
      GameFleetNewEnhance:RefreshFleetMedal()
    end
    return true
  end
  return false
end
GameFleetNewEnhance.changeFleetsData = nil
function GameFleetNewEnhance.ChangeFleetDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.change_fleets_data_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.change_fleets_data_ack.Code then
    GameFleetNewEnhance:GenerateChangeFleetsData(content)
    GameFleetNewEnhance:SetChangeFleetsData()
    GameFleetNewEnhance:ShowChangeFleetsPop()
    return true
  end
  return false
end
function GameFleetNewEnhance:GenerateChangeFleetsData(content)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local data = {}
  data.fleets = {}
  for k, v in ipairs(content.fleets or {}) do
    local fleet = {}
    fleet = v
    if v.fleet_id == fleetId then
      fleet.isSelected = true
    end
    fleet.name = v.level > 0 and "+" .. v.level or ""
    fleet.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.vessels)
    fleet.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(v.vessels)
    fleet.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(v.ship)
    fleet.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.avatar, v.fleet_id)
    fleet.color = FleetDataAccessHelper:GetFleetColorFrameByColor(v.color)
    fleet.spellFrame = FleetDataAccessHelper:GetFleetSpellFrameBySpell(v.spell_id)
    fleet.rankFrame = GameUtils:GetFleetRankFrame(v.rank)
    fleet.fleet_id = v.fleet_id
    data.fleets[#data.fleets + 1] = fleet
  end
  GameFleetNewEnhance.changeFleetsData = data
end
function GameFleetNewEnhance:SetChangeFleetsData()
  if GameFleetNewEnhance.changeFleetsData and GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setChangeFleetsData", math.ceil(#GameFleetNewEnhance.changeFleetsData.fleets / 3))
  end
end
function GameFleetNewEnhance:ShowChangeFleetsPop()
  if GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "ShowChangeFleetsPop")
  end
end
function GameFleetNewEnhance:UpdateChangeFleetsItem(itemIndex)
  local list = {}
  local startIndex = (itemIndex - 1) * 3 + 1
  local item = GameFleetNewEnhance.changeFleetsData.fleets[startIndex]
  local item2 = GameFleetNewEnhance.changeFleetsData.fleets[startIndex + 1]
  local item3 = GameFleetNewEnhance.changeFleetsData.fleets[startIndex + 2]
  list[#list + 1] = item
  list[#list + 1] = item2
  list[#list + 1] = item3
  local flashObj = GameFleetNewEnhance:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "upChangeFleetsItem", list, itemIndex)
  end
end
function GameFleetNewEnhance.RefreshKrypton()
  if GameFleetNewEnhance:GetFlashObject() and GameFleetNewEnhance.m_subState == k_subStateFleetKrypton then
    DebugOut("Refresh update server krypton")
    GameFleetNewEnhance:GetFleetKrypton()
    if curMatrixIndex then
      GameFleetNewEnhance:RequestKryptonInBag()
    end
  end
end
function GameFleetNewEnhance:GetFleetKrypton()
  DebugOut("GetFleetKrypton = " .. self.m_currentSelectFleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  if not fleetId then
    return
  end
  local fleetkryptons = GameGlobalData:GetData("fleetkryptons")
  DebugOutPutTable(fleetkryptons, "fleetkryptons ")
  local fleetkrypton = LuaUtils:table_values(LuaUtils:table_filter(fleetkryptons, function(k, v)
    return v.fleet_identity == fleetId
  end))[1]
  if fleetkrypton then
    GameFleetNewEnhance.currentFleetKrypton = fleetkrypton.kryptons
  else
    GameFleetNewEnhance.currentFleetKrypton = {}
    DebugOut("select fleet's kryptons is nil", self.m_currentSelectFleetIndex)
  end
  DebugOutPutTable(GameFleetNewEnhance.currentFleetKrypton, "GetFleetKrypton")
  GameFleetNewEnhance.currentFleetKrypton = GameFleetNewEnhance.currentFleetKrypton or {}
  GameFleetNewEnhance.currentFleetKrypton = GameFleetNewEnhance:ProcessMatrixInfo(GameFleetNewEnhance.currentFleetKrypton)
  GameFleetNewEnhance:RefreshCurFleetKrypton()
end
function GameFleetNewEnhance:CheckInitGameData()
  if not self.kryptonSlotLevel then
    self.kryptonSlotLevel = GameDataAccessHelper:GetKryptonUnLockSlot()
  end
end
function GameFleetNewEnhance:RefreshCurFleetKrypton()
  self:CheckInitGameData()
  local frame, lockLevel, kryptonLV, canEvalote = "", "", "", ""
  if not self.currentFleetKrypton then
    return
  end
  local building = GameGlobalData:GetBuildingInfo("krypton_center")
  local totalKrypton = 0
  DebugOutPutTable(self.currentFleetKrypton, "self.currentFleetKrypton")
  local resource = GameGlobalData:GetData("resource")
  for i = 1, 8 do
    local item = self.currentFleetKrypton[i]
    if item then
      local id = GameFleetNewEnhance:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = frame .. "item_" .. id .. "\001"
        else
          frame = frame .. "temp" .. "\001"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = frame .. "item_" .. id .. "\001"
      end
      totalKrypton = item.decompose_kenergy
      kryptonLV = kryptonLV .. GameLoader:GetGameText("LC_MENU_Level") .. item.level .. "\001"
      if resource and resource.kenergy >= item.kenergy_upgrade and not item.max_level then
        canEvalote = canEvalote .. "true" .. "\001"
      else
        canEvalote = canEvalote .. "false" .. "\001"
      end
    else
      frame = frame .. "empty" .. "\001"
      kryptonLV = kryptonLV .. "no" .. "\001"
      canEvalote = canEvalote .. "false" .. "\001"
    end
  end
  for k, v in pairs(self.kryptonSlotLevel) do
    if v.labLevel <= building.level then
      lockLevel = lockLevel .. "no" .. "\001"
    else
      lockLevel = lockLevel .. v.labLevel .. "\001"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshEquipment", frame, lockLevel, totalKrypton, kryptonLV)
  self:GetFlashObject():InvokeASCallback("_root", "SetKryptonEquipArrows", canEvalote)
end
function GameFleetNewEnhance:ProcessMatrixInfo(t)
  local r = {}
  for k, v in pairs(t) do
    for tk, tv in pairs(v.formation) do
      if tv.formation_id == curMatrixIndex then
        r[tv.slot] = v
      end
    end
  end
  DebugTable(r)
  return r
end
function GameFleetNewEnhance:ProcessBagInfo(t)
  local r = {}
  for k, v in pairs(t) do
    r[v.slot] = v
  end
  DebugTable(r)
  return r
end
function GameFleetNewEnhance:SendMsgWithMatrix(msgType, param, callback, block, failCallback)
  DebugOut("SendMsgWithMatrix:" .. msgType)
  param = param or {}
  local isIndex = false
  for k, v in pairs(param) do
    if k == "formation_id" then
      isIndex = true
      break
    end
  end
  if not isIndex then
    param.formation_id = curMatrixIndex or 1
  end
  NetMessageMgr:SendMsg(msgType, param, callback, block, failCallback)
end
function GameFleetNewEnhance:RequestKryptonInBag()
  local function netFailedCallback()
    GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, GameFleetNewEnhance.DownloadKryptonBag, true, netFailedCallback)
  end
  GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_store_req.Code, nil, GameFleetNewEnhance.DownloadKryptonBag, true, netFailedCallback)
end
function GameFleetNewEnhance.DownloadKryptonBag(msgType, content)
  if msgType == NetAPIList.krypton_store_ack.Code then
    GameFleetNewEnhance.grid_capacity = content.grid_capacity
    GameFleetNewEnhance.adv_cost = content.gacha_need_credit
    GameFleetNewEnhance.kryptonBag = content.kryptons
    GameFleetNewEnhance.kryptonBag = GameFleetNewEnhance:ProcessBagInfo(GameFleetNewEnhance.kryptonBag)
    GameFleetNewEnhance:SetBagList()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  end
  return false
end
function GameFleetNewEnhance:EquipOffKrypton(kryptonId, destSlot)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for tk, tv in pairs(v.formation) do
        if tv.formation_id == curMatrixIndex then
          self.equipOffSrcSlot = tv.slot
        end
      end
    end
  end
  self.equipOffDestSlot = destSlot
  DebugOut("EquipOffKrypton: ", identity, kryptonId, self.equipOffDestSlot, self.equipOffSrcSlot)
  local krypton_equip_off_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    slot = self.equipOffDestSlot
  }
  local function netFailedCallback()
    GameFleetNewEnhance:onEndDragItem()
    GameFleetNewEnhance:RequestKryptonInBag()
  end
  GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_equip_off_req.Code, krypton_equip_off_req_param, GameFleetNewEnhance.kryptonEquipCallback, true, netFailedCallback)
end
function GameFleetNewEnhance.kryptonEquipCallback(msgType, content)
  DebugOut("--------kryptonEquipCallback------------", msgType, content, GameFleetNewEnhance.equipOffDestSlot, GameFleetNewEnhance.equipOffSrcSlot, GameFleetNewEnhance.equipDestSlot)
  if msgType == NetAPIList.krypton_equip_ack.Code then
    local item
    if GameFleetNewEnhance.equipOffDestSlot ~= -1 then
      item = GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipOffDestSlot]
      GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipOffDestSlot] = GameFleetNewEnhance.currentFleetKrypton[GameFleetNewEnhance.equipOffSrcSlot]
      if GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipOffDestSlot] then
        GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipOffDestSlot].slot = GameFleetNewEnhance.equipOffDestSlot
      end
    elseif GameFleetNewEnhance.equipDestSlot ~= -1 then
      item = GameFleetNewEnhance.currentFleetKrypton[GameFleetNewEnhance.equipDestSlot]
      GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipSrcSlot] = item
      if GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipSrcSlot] then
        GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.equipSrcSlot].slot = GameFleetNewEnhance.equipSrcSlot
      end
      item = nil
    end
    DebugOut("kryptonEquipCallback item: ", item, GameFleetNewEnhance.dragItemInstance)
    if GameFleetNewEnhance.dragItemInstance and item then
      GameFleetNewEnhance.dragItemInstance.DraggedItemID = -1
      local frame = ""
      local id = GameFleetNewEnhance:GetKryptonType(item)
      if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
        local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
        local localPath = "data2/" .. fullFileName
        if DynamicResDownloader:IfResExsit(localPath) then
          ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
          frame = "item_" .. id
        else
          frame = "temp"
          DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
        end
      else
        frame = "item_" .. id
      end
      GameFleetNewEnhance.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
      GameFleetNewEnhance.dragItemInstance:SetDestPosition(GameFleetNewEnhance.dragX, GameFleetNewEnhance.dragY)
      GameFleetNewEnhance.dragItemInstance:StartAutoMove()
    elseif GameFleetNewEnhance.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    GameFleetNewEnhance:RequestKryptonInBag()
    GameFleetNewEnhance.equipOffDestSlot = -1
    GameFleetNewEnhance.equipDestSlot = -1
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.krypton_equip_off_req.Code or content.api == NetAPIList.krypton_equip_req.Code) then
    if GameFleetNewEnhance.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    local text = AlertDataList:GetTextFromErrorCode(content.code)
    GameTip:Show(text, 3000)
    return true
  end
  return false
end
function GameFleetNewEnhance:GetKryptonType(item)
  if item.item_type and item.rank then
    local _type = 50000 + item.item_type * 100 + item.rank
    return _type
  end
  return -1
end
function GameFleetNewEnhance:SetBagList()
  local flashObj = GameFleetNewEnhance:GetFlashObject()
  if flashObj then
    local cnt = math.ceil(GameFleetNewEnhance.grid_capacity / 4)
    DebugOut("GameUIKrypton:SetBagList()" .. tostring(cnt))
    self:GetFlashObject():InvokeASCallback("_root", "SetBagList", cnt)
  end
end
function GameFleetNewEnhance:UpdateIconAndRate(item)
  if self:GetFlashObject() then
    local param = GameFleetNewEnhance.StrenthenWeight
    local percentnum = math.ceil((param.SucessWeight + param.AdditionWeight + item.weight) / (param.TotleWeight + param.AdditionWeight + item.weight) * 10000) / 100
    if percentnum > 100 then
      percentnum = 100
    end
    if item.weight >= 1000 then
      percentnum = 100
    end
    local percent = percentnum .. "%"
    DebugOut("percentnum", percent)
    DebugTable(param)
    DebugTable(item)
    local zCount = GameFleetNewEnhance.inventedData.Zcount
    local itemCount = item.no
    self:GetFlashObject():InvokeASCallback("_root", "changePrimeIcon", item.number, item.needcnt, percent, zCount, itemCount, GameFleetNewEnhance.inventedData)
  end
end
function GameFleetNewEnhance.clearCD()
  local param = {
    cdtype = GameUIGlobalScreen.equipmentCdType
  }
  NetMessageMgr:SendMsg(NetAPIList.cdtimes_clear_req.Code, param, GameFleetNewEnhance.clearCDCallback, false)
end
function GameFleetNewEnhance.clearCDCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.cdtimes_clear_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    end
    GameFleetNewEnhance:OnEquipmentSelect()
    return true
  end
  return false
end
function GameFleetNewEnhance.OnGlobalEquipmentsChange()
  DebugOut("NTF On: OnGlobalEquipmentsChange")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetNewEnhance) then
    GameFleetNewEnhance:FleetSelect(GameFleetNewEnhance.m_currentSelectFleetIndex, true)
  end
end
function GameFleetNewEnhance.OnGlobalLevelChange()
  DebugOut("NTF On: OnGlobalLevelChange")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameFleetNewEnhance) then
    GameFleetNewEnhance:RefreshLevelInfo()
  end
end
function GameFleetNewEnhance:RefreshLevelInfo()
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local levelText = GameLoader:GetGameText("LC_MENU_AVATAR_LEVEL") .. " " .. tostring(levelinfo.level)
  self:GetFlashObject():InvokeASCallback("_root", "RefreshLevelInfo", levelText)
end
function GameFleetNewEnhance.OnEnhanceCDTimeChange()
  DebugOut("NTF On: OnEnhanceCDTimeChange")
  local cd_time = GameGlobalData:GetEnhanceCDTime()
  if not cd_time then
    GameFleetNewEnhance.enhanceCDTime = nil
    return
  end
  if cd_time.left_time > 0 then
    GameFleetNewEnhance.enhanceCDTime = os.time() + cd_time.left_time
  else
    GameFleetNewEnhance.enhanceCDTime = os.time()
  end
  GameFleetNewEnhance:CheckEnhanceCDTime()
end
function GameFleetNewEnhance.OnEnhanceFleetinfoChange()
  DebugOut("NTF On: OnEnhanceFleetinfoChange")
  GameFleetNewEnhance.CheckTutorialBattleMapAfterRecruit()
  DebugOut(GameStateManager:GetCurrentGameState())
  if startQuickEquipment and waitForRefreshCount > 0 then
    return
  end
  if startQuickUnload and waitForRefreshCount > 0 then
    return
  end
  DebugOut("whats happen", GameFleetNewEnhance.m_subState)
  if GameFleetNewEnhance.m_subState == k_subStateFleetEnhance or GameFleetNewEnhance.m_subState == k_subStateFleetEquipment or GameFleetNewEnhance.m_subState == k_subStateFleetEvolution or GameFleetNewEnhance.m_subState == k_subStateFleetView or GameFleetNewEnhance.m_subState == k_subStateFleetIntervene or GameFleetNewEnhance.m_subState == k_subStateFleetKrypton then
    DebugOut("In!")
    GameFleetNewEnhance:RefreshFleetBagAndSlot()
    local fleetinfo = GameGlobalData.GlobalData.fleetinfo.fleets
    for k, v in pairs(fleetinfo) do
      if v.id == GameFleetNewEnhance.m_currentFleetsId and not GameFleetNewEnhance.newSelectedFleetID then
        local enhanceCount = v.force - GameFleetNewEnhance.m_curreetFleetsForce
        GameFleetNewEnhance.m_curreetFleetsForce = v.force
        DebugOut("showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "showEnhanceAnimation", enhanceCount, GameUtils.numberConversion(v.force))
        local GameArtifact = LuaObjectManager:GetLuaObject("GameArtifact")
        GameArtifact:Updatepower(v.force)
      end
    end
    GameFleetNewEnhance:UpdateAllheroList()
  end
end
function GameFleetNewEnhance.CheckTutorialBattleMapAfterRecruit()
  local fleet_info = GameGlobalData:GetData("fleetinfo")
  local resource_info = GameGlobalData:GetData("resource")
  DebugTable(fleet_info)
  if fleet_info and fleet_info.fleets and resource_info and resource_info.prestige then
    DebugOut(#fleet_info.fleets, resource_info.prestige, QuestTutorialBattleMapAfterRecruit:IsActive(), QuestTutorialBattleMapAfterRecruit:IsFinished())
    if #fleet_info.fleets > 2 and resource_info.prestige >= 500 and not QuestTutorialBattleMapAfterRecruit:IsActive() and not QuestTutorialBattleMapAfterRecruit:IsFinished() then
      DebugOut("tutorial BattleMapAfterRecruit open")
      QuestTutorialBattleMapAfterRecruit:SetActive(true)
    end
  end
end
function GameFleetNewEnhance:showBuyEquipItenBox(arg)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Equip", fleets[self.currentSlectedIndex][self.currentSelectEquipSlot], self.curBuyEquipType, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), butText)
end
function GameFleetNewEnhance:showEnhanceEquipItemBox(arg, item, item_type)
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Equip", item, item_type, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), butText)
end
function GameFleetNewEnhance:showEnhanceItemItemBox(arg, item, item_type)
  local param = LuaUtils:string_split(arg, "\001")
  local butText = ""
  ItemBox:showItemBox("Item", item, item_type, tonumber(param[1]), tonumber(param[2]), tonumber(param[3]), tonumber(param[4]), "", nil, "", nil)
  ItemBox:showGotoBtn()
  currentShowBluePrint = item_type
end
function GameFleetNewEnhance.OnGlobalResourceChange()
  DebugOut("OnGlobalResourceChange")
  GameFleetNewEnhance.CheckTutorialBattleMapAfterRecruit()
  if not GameFleetNewEnhance:GetFlashObject() then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setMoneyCredit", GameUtils.numberConversion(resource.money), GameUtils.numberConversion(resource.credit))
  GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setKryptonRes", GameUtils.numberConversion(resource.kenergy))
  local GameArtifact = LuaObjectManager:GetLuaObject("GameArtifact")
  GameArtifact:setArtPoint(resource.art_point)
end
function GameFleetNewEnhance.check_1001011_story()
  local mfleets = GameGlobalData:GetData("fleetinfo").fleets
  local have_fleet = false
  local have_surplus_equip = false
  local dst_index = 1
  for index, mfleet in pairs(mfleets) do
    if mfleet.identity == 4 then
      have_fleet = true
      dst_index = index
      break
    end
  end
  GameFleetNewEnhance:GetOnlyEquip()
  if #GameFleetNewEnhance.onlyEquip > 0 then
    have_surplus_equip = true
  end
  return dst_index, have_fleet and have_surplus_equip
end
function GameFleetNewEnhance.autoUploadEquipAfter_1001011_story()
  GameFleetNewEnhance:AllEquipAction()
end
function GameFleetNewEnhance:getQuickEquipEnhanceVipLevelLimit()
  return ...
end
function GameFleetNewEnhance:tipVipLevelNotEnoughForQuickEquipEnhance()
  local strTip = GameLoader:GetGameText("LC_MENU_UNLOCK_LEVEL_OR_VIP_INFO")
  local viplevellimit = GameDataAccessHelper:GetVIPLimit("one_key_equip_enhance")
  local playerlevellimit = GameDataAccessHelper:GetLevelLimit("one_key_equip_enhance")
  strTip = string.gsub(strTip, "<playerLevel_num>", playerlevellimit)
  strTip = string.gsub(strTip, "<vipLevel_num>", viplevellimit)
  GameUtils:ShowTips(strTip)
end
function GameFleetNewEnhance:getEvolutionUseablePlayerLevel()
  local limitLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("equip_evolution")
  DebugOut("equip evolution level limit is:" .. limitLevel)
  return limitLevel
end
function GameFleetNewEnhance:getEvolutionUnuseableTipStr()
  local strTip = GameLoader:GetGameText("LC_MENU_EQUIP_EVOLUTION_LOCKED")
  if nil == strTip or "" == strTip then
    strTip = "this function is unlocked when player level got %d"
  end
  local useablePlayerLevel = GameFleetNewEnhance:getEvolutionUseablePlayerLevel()
  return ...
end
function GameFleetNewEnhance:isEvolutionUseable()
  local playerLevel = GameGlobalData:GetData("levelinfo").level
  local evolutionUseablePlayerLevel = self:getEvolutionUseablePlayerLevel()
  DebugOut("playerLevel is:" .. playerLevel .. " evolutionUseableLevel is:" .. evolutionUseablePlayerLevel)
  return playerLevel >= evolutionUseablePlayerLevel
end
function GameFleetNewEnhance:tipEvolllutionUnuseable()
  local tipStr = string.format(self:getEvolutionUnuseableTipStr(), self:getEvolutionUseablePlayerLevel())
  GameUtils:ShowTips(tipStr)
end
function GameFleetNewEnhance:lockOrUnlockEvolutionBtnAccordUseable()
  if self:isEvolutionUseable() then
    self:GetFlashObject():InvokeASCallback("_root", "unlockEvolutionBtn")
  else
    self:GetFlashObject():InvokeASCallback("_root", "lockEvolutionBtn")
  end
end
function GameFleetNewEnhance:lockOrUnlockQuickEnhanceBtnAccordUseable()
  if GameVip:IsFastEnhanceUnlocked() or Facebook:IsBindFacebook() then
    self:GetFlashObject():InvokeASCallback("_root", "unlockQuickEnhanceBtn")
  else
    self:GetFlashObject():InvokeASCallback("_root", "lockQuickEnhanceBtn")
  end
end
if AutoUpdate.isAndroidDevice then
  function GameFleetNewEnhance.OnAndroidBack()
    if GameUIWDStuff.menuType.menu_type_help == GameUIWDStuff.currentMenu then
      local flash_obj = GameUIWDStuff:GetFlashObject()
      if flash_obj then
        flash_obj:InvokeASCallback("_root", "hideHelp")
      end
    elseif GameStateEquipEnhance.mCurState == SUB_STATE.STATE_GROW_UP then
      GameStateEquipEnhance:GoToSubState(SUB_STATE.STATE_NORMAL)
    elseif GameUIAdjutant.IsShowAdjutantUI then
      GameUIAdjutant:OnFSCommand("close")
    else
      GameFleetNewEnhance:OnFSCommand("closePress")
    end
  end
end
function GameFleetNewEnhance:GetCurFirstCanEquipBagSlotIdx(fleetIdx, slot)
  local idx = -1
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleet = fleets[fleetIdx]
  local fleetVessels = GameDataAccessHelper:GetCommanderVessels(fleet.identity, fleet.level)
  local isEnFleet = false
  if tonumber(fleetVessels) == 4 then
    isEnFleet = true
  end
  if self.filterEquip[slot] and #self.filterEquip[slot] > 0 then
    for k = 1, #self.filterEquip[slot] do
      local v = self.filterEquip[slot][k]
      local item, itemInBag, itemInFleet = GameFleetNewEnhance:GetCurrentSelectItem(slot, k)
      if nil == item and itemInBag then
        if slot == 1 then
          if isEnFleet and tonumber(string.sub(v.item_type, -1)) == 6 then
            idx = k
            break
          elseif not isEnFleet and tonumber(string.sub(v.item_type, -1)) ~= 6 then
            idx = k
            break
          end
        else
          idx = k
          break
        end
      end
    end
  end
  return idx
end
function GameFleetNewEnhance.sendEquipQuickEnhanceInfoRequire()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  local req_param = {
    equip_id = equip.item_id
  }
  NetMessageMgr:SendMsg(NetAPIList.enhance_info_req.Code, req_param, GameFleetNewEnhance.equipQuickEnhanceInfoRequireCallback, true, nil)
end
function GameFleetNewEnhance.equipQuickEnhanceInfoRequireCallback(msgType, content)
  DebugOut("GameFleetNewEnhance.equipQuickEnhanceInfoRequireCallback")
  DebugTable(content)
  if msgType == NetAPIList.enhance_info_ack.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameFleetNewEnhance.mlastReceivedEnhanceInfo = content
    GameFleetNewEnhance.useQuickEnhanceFunction()
    if QuestTutorialEnhance:IsActive() then
      GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEnhanceTip", false)
      GameFleetNewEnhance:CheckEnableQuckEnhance()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.bag_req.Code and content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetNewEnhance.useQuickEnhanceFunction()
  DebugOut("GameFleetNewEnhance.useQuickEnhanceFunction")
  local cd_level = GameFleetNewEnhance:GetCanUpCountCd()
  local levelCanUp = GameFleetNewEnhance.getQuickEnhanceCanUpLevelCount()
  GameFleetNewEnhance.levelUp = levelCanUp
  if cd_level and cd_level > 0 and cd_level < GameFleetNewEnhance.levelUp and GameFleetNewEnhance:HaveEnhanceCD() then
    DebugOut("-----ecIndex")
    GameFleetNewEnhance.levelUp = cd_level
  end
  DebugOut("GameFleetNewEnhance.levelUp = " .. GameFleetNewEnhance.levelUp)
  if GameFleetNewEnhance.levelUp <= 0 then
    GameFleetNewEnhance.sendCommonEnhanceRequire()
  else
    GameFleetNewEnhance.openQuickEnhanceUI()
  end
end
function GameFleetNewEnhance.getQuickEnhanceCanUpLevelCount()
  DebugOut("GameFleetNewEnhance.getQuickEnhanceCanUpLevelCount")
  DebugTable(GameFleetNewEnhance.mlastReceivedEnhanceInfo)
  local equipSelectNow = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  DebugTable(equipSelectNow)
  return GameFleetNewEnhance.mlastReceivedEnhanceInfo.max_level - GameFleetNewEnhance.getEquipLevelByEquipId(equipSelectNow.item_id)
end
function GameFleetNewEnhance.getEquipInfoByEquipId(itemId)
  local equipments = GameGlobalData:GetData("equipments")
  local equipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
    return v.equip_id == itemId
  end))[1]
  DebugTable(equipInfo)
  return equipInfo
end
function GameFleetNewEnhance.getEquipLevelByEquipId(itemId)
  local equipInfo = GameFleetNewEnhance.getEquipInfoByEquipId(itemId)
  return equipInfo.equip_level
end
function GameFleetNewEnhance.getEquipTypeByEquipId(itemId)
  local equipInfo = GameFleetNewEnhance.getEquipInfoByEquipId(itemId)
  return equipInfo.equip_type
end
function GameFleetNewEnhance.openQuickEnhanceUI()
  local lastReceivedEnhanceInfo = GameFleetNewEnhance.mlastReceivedEnhanceInfo
  local equipSelectNow = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  local equipNameText = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. GameFleetNewEnhance.getEquipTypeByEquipId(equipSelectNow.item_id))
  local equipLevelNow = GameFleetNewEnhance.getEquipLevelByEquipId(equipSelectNow.item_id)
  local levelCanUp = GameFleetNewEnhance.levelUp
  local baseLevelUpConsume = lastReceivedEnhanceInfo.consume
  local equipIcon = "item_" .. GameFleetNewEnhance.getEquipTypeByEquipId(equipSelectNow.item_id)
  local equipmentAttrParams = {}
  for i, attrInfo in ipairs(lastReceivedEnhanceInfo.values) do
    local attrNameText = GameLoader:GetGameText("LC_MENU_Equip_param_" .. attrInfo.type)
    table.insert(equipmentAttrParams, attrNameText)
    table.insert(equipmentAttrParams, ",")
    table.insert(equipmentAttrParams, attrInfo.bassic)
    table.insert(equipmentAttrParams, ",")
    table.insert(equipmentAttrParams, attrInfo.mature)
    table.insert(equipmentAttrParams, "|")
  end
  local resource = GameGlobalData:GetData("resource")
  local money = resource.money
  local titleText = GameLoader:GetGameText("LC_MENU_AUTO_ENHANCE_BUTTON")
  local creditCost = GameGlobalData.GetCostCredit_a_b({item_type = "money"})
  local minCreditCost = GameGlobalData.GetMinCostCredit()
  GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "openQuickEnhanceUI", titleText, equipIcon, equipNameText, equipLevelNow, levelCanUp, baseLevelUpConsume, table.concat(equipmentAttrParams), money, creditCost, minCreditCost)
end
function GameFleetNewEnhance.openQuickInterveneUI()
  local titleText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH")
  local descText = GameLoader:GetGameText("LC_MENU_FLEETS_ENTRANCE_INTERFERENCE_BATCH_HINT")
  GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "openQuickInterveneUI", titleText, descText, GameFleetNewEnhance.inventedData)
end
function GameFleetNewEnhance:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  local money = resource.money
  if GameFleetNewEnhance:GetFlashObject() then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "updateUserGold", money)
  end
end
function GameFleetNewEnhance.sendQuickEnhanceRequire(levelUp)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  if equip then
    local equipment_enhance_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId,
      once_more_enhance = 1,
      en_level = levelUp
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, GameFleetNewEnhance.equipmentEnhanceCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, GameFleetNewEnhance.equipmentEnhanceCallback, true, nil)
  end
end
function GameFleetNewEnhance.sendCommonEnhanceRequire()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].id
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(GameFleetNewEnhance.m_currentSelectEquipSlot)
  if equip then
    local equipment_enhance_req_param = {
      equip_id = equip.item_id,
      fleet_id = fleetId,
      once_more_enhance = 0,
      en_level = 1
    }
    NetMessageMgr:SendMsg(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, GameFleetNewEnhance.equipmentEnhanceCallback, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.equipment_enhance_req.Code, equipment_enhance_req_param, GameFleetNewEnhance.equipmentEnhanceCallback, true, nil)
  end
end
local million = 1000000
local billion = million * 1000
function GameFleetNewEnhance.numberConversionForQuickEnhanceBtnText(number)
  if number < million then
    return "" .. number
  elseif number < billion then
    number = number / million
    return ...
  else
    number = number / billion
    return ...
  end
end
function GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation()
  DebugOut("GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation")
  if not TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    return
  end
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[GameFleetNewEnhance.m_currentSelectFleetIndex].identity
  DebugOut("fleetId is : " .. fleetId)
  if fleetId ~= 4 then
    GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  DebugOut("m_currentSelectEquipSlot is : " .. GameFleetNewEnhance.m_currentSelectEquipSlot)
  local isSelectSlotOne = 1 == GameFleetNewEnhance.m_currentSelectEquipSlot
  local isSelectSlotTwo = 2 == GameFleetNewEnhance.m_currentSelectEquipSlot
  if not isSelectSlotOne and not isSelectSlotTwo then
    GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  if isSelectSlotTwo and nil == GameFleetNewEnhance:IsEquipmentInSlot(1) then
    GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquip()
    return
  end
  DebugOut("end of GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquipIfExecptionOperation")
end
function GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquip()
  if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    DebugOut("GameFleetNewEnhance._forceFinishTutorialFirstGetSilvaEquip")
    TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:SetFinish(true)
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowEquipSlotTip", 1, false)
  end
end
function GameFleetNewEnhance._forceFinishTutorialEquipAndTutorialFirstGetWanaArmorIfTutorialFirstGetSilvaEquipIsActive()
  if TutorialQuestManager.QuestTutorialFirstGetSilvaEquip:IsActive() then
    GameFleetNewEnhance._forceFinishTutorialEquip()
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive()
  end
end
function GameFleetNewEnhance._forceFinishTutorialEquip()
  if TutorialQuestManager.QuestTutorialEquip:IsActive() then
    DebugOut("GameFleetNewEnhance._forceFinishTutorialEquip")
    TutorialQuestManager.QuestTutorialEquip:SetFinish(true)
  end
end
function GameFleetNewEnhance._forceTutorialFirstGetWanaArmor()
  if TutorialQuestManager.QuestTutorialFirstGetWanaArmor:IsActive() then
    DebugOut("GameFleetNewEnhance._forceFinishTutorialFirstGetWanaArmor")
    TutorialQuestManager.QuestTutorialFirstGetWanaArmor:SetFinish(true)
  end
end
function GameFleetNewEnhance:getHelpTutorialPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getHelpTutorialPos")
  end
end
function GameFleetNewEnhance:getUpgradeEquipmentTutorialPos(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "getUpgradeEquipmentTutorialPos")
  end
end
function GameFleetNewEnhance:RefreshRedPoint()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "HideAllRedPoint")
    if GameGlobalData.redPointStates then
      local fleets = GameGlobalData:GetData("fleetinfo").fleets
      local fleetId = fleets[self.m_currentSelectFleetIndex].identity
      local maxFleetIndex = LuaUtils:table_size(fleets)
      self:RefreshNextFleetRedPoint(self.m_currentSelectFleetIndex)
      self:RefreshPrevFleetRedPoint(self.m_currentSelectFleetIndex)
      self:RefreshEnhanceRedPoint()
      self:RefreshWearRedPoint()
      self:RefreshEvoluteRedPoint()
      self:RefreshEnhanceBarRedPoint()
    end
  end
end
function GameFleetNewEnhance:RefreshEnhanceBarRedPoint()
  if not self.m_currentSelectEquipSlot then
    self.m_currentSelectEquipSlot = 1
  end
  local equip = GameFleetNewEnhance:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
  DebugOut("RefreshEnhanceBarRedPoint", self.m_currentSelectEquipSlot)
  if equip and self:isCurFleetInMatrix() and self.equipStatus then
    local stat = GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot]
    local levelinfo = GameGlobalData:GetData("levelinfo")
    local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
    DebugOut("levelinfo.level", levelinfo.level, "reqLevel", reqLevel)
    DebugTable(stat)
    if stat and stat ~= -1 and (stat.can_evolute == 1 or stat.can_enhance == 1 or stat.can_enchant > 0 and reqLevel <= levelinfo.level) then
      if reqLevel <= levelinfo.level then
        self:GetFlashObject():InvokeASCallback("_root", "refreshEnhanceBarRedPoint", true, stat.can_enhance, stat.can_evolute, stat.can_enchant)
      else
        self:GetFlashObject():InvokeASCallback("_root", "refreshEnhanceBarRedPoint", true, stat.can_enhance, stat.can_evolute, 0)
      end
    else
      self:GetFlashObject():InvokeASCallback("_root", "refreshEnhanceBarRedPoint", false, nil, nil, nil)
    end
  else
    self:GetFlashObject():InvokeASCallback("_root", "refreshEnhanceBarRedPoint", false, nil, nil, nil)
  end
end
function GameFleetNewEnhance:setEuipEnhanceRedPoint()
  if GameFleetNewEnhance.equipStatus then
    local stat = GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot]
    if self:isCurFleetInMatrix() and stat then
      self:GetFlashObject():InvokeASCallback("_root", "setEvoluteRedPoint", stat.can_enhance == 1)
    else
      self:GetFlashObject():InvokeASCallback("_root", "setEvoluteRedPoint", false)
    end
  end
end
function GameFleetNewEnhance:GetFleetRedPoint(fleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[fleetIndex].identity
  if GameGlobalData.redPointStates and GameGlobalData.redPointStates then
    for k, v in pairs(GameGlobalData.redPointStates.states) do
      if v.key == fleetId and v.value == 1 then
        return true
      end
    end
  end
  return false
end
function GameFleetNewEnhance:RefreshNextFleetRedPoint(curSelectFleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local maxFleetIndex = LuaUtils:table_size(fleets)
  if curSelectFleetIndex and curSelectFleetIndex < maxFleetIndex then
    for i = curSelectFleetIndex + 1, maxFleetIndex do
      if self:GetFleetRedPoint(i) and not self:GetFleetRedPoint(self.m_currentSelectFleetIndex) then
        self:GetFlashObject():InvokeASCallback("_root", "setNextArrawRedPoint", true)
        return
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setNextArrawRedPoint", false)
end
function GameFleetNewEnhance:RefreshPrevFleetRedPoint(curSelectFleetIndex)
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local maxFleetIndex = LuaUtils:table_size(fleets)
  if curSelectFleetIndex and curSelectFleetIndex > 1 then
    for i = curSelectFleetIndex - 1, 1, -1 do
      if self:GetFleetRedPoint(i) and not self:GetFleetRedPoint(self.m_currentSelectFleetIndex) then
        self:GetFlashObject():InvokeASCallback("_root", "setPrevArrawRedPoint", true)
        return
      end
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setPrevArrawRedPoint", false)
end
function GameFleetNewEnhance:RefreshEnhanceRedPoint()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local levelinfo = GameGlobalData:GetData("levelinfo")
  local reqLevel = GameDataAccessHelper:GetRightMenuPlayerReqLevel("enchant")
  DebugOut("GameFleetNewEnhance:RefreshEnhanceRedPoint")
  DebugTable(GameFleetNewEnhance.equipStatus)
  for i = 1, 5 do
    local equip = GameFleetNewEnhance:IsEquipmentInSlot(i)
    if equip and self:FleetInMatrix(fleetId) then
      if GameFleetNewEnhance.equipStatus and GameFleetNewEnhance.equipStatus[i] and GameFleetNewEnhance.equipStatus[i] ~= -1 and (GameFleetNewEnhance.equipStatus[i].can_enhance == 1 or GameFleetNewEnhance.equipStatus[i].can_enchant > 0 and reqLevel <= levelinfo.level) then
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, true)
      elseif GameFleetNewEnhance.equipStatus and GameFleetNewEnhance.equipStatus[i] and GameFleetNewEnhance.equipStatus[i] ~= -1 and GameFleetNewEnhance.equipStatus[i].can_evolute == 1 then
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, true)
      else
        self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, false)
      end
    else
      self:GetFlashObject():InvokeASCallback("_root", "setEquipmentRedPoint", i, false)
    end
  end
end
function GameFleetNewEnhance:RefreshEvoluteRedPoint()
  self:GetFlashObject():InvokeASCallback("_root", "setEvoluteRedPoint", false)
  local equipments = GameGlobalData:GetData("equipments")
  DebugOut("GameFleetNewEnhance:RefreshEvoluteRedPoint", self.m_currentSelectEquipSlot)
  if self.m_currentSelectEquipSlot and self.m_currentSelectEquipSlot > 0 then
    do
      local equip = GameFleetNewEnhance:IsEquipmentInSlot(self.m_currentSelectEquipSlot)
      if equip and self:isCurFleetInMatrix() then
        local currentEquipInfo = LuaUtils:table_values(LuaUtils:table_filter(equipments, function(k, v)
          return v.equip_id == equip.item_id
        end))[1]
        if GameFleetNewEnhance.equipStatus and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot] and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot] ~= -1 and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot].can_enhance == 1 and currentEquipInfo.equip_level < 60 then
          self:GetFlashObject():InvokeASCallback("_root", "setEvoluteRedPoint", true)
        elseif GameFleetNewEnhance.equipStatus and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot] and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot] ~= -1 and GameFleetNewEnhance.equipStatus[self.m_currentSelectEquipSlot].can_evolute == 1 then
          self:GetFlashObject():InvokeASCallback("_root", "setEvoluteRedPoint", true)
        end
      end
    end
  end
end
function GameFleetNewEnhance:RefreshWearRedPoint()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local equipments = GameGlobalData:GetData("equipments")
  for i = 1, 5 do
    local equip = self:IsEquipmentInSlot(i)
    if not equip and self.filterEquip and self.filterEquip[i] and self:IsContainEquipment(self.filterEquip[i]) and self:GetCurFleetInMatrix(fleetId) and GameFleetNewEnhance:IsHasEquipmentToLoad(i) then
      self:GetFlashObject():InvokeASCallback("_root", "setWearRedPoint", true)
      return
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "setWearRedPoint", false)
end
function GameFleetNewEnhance:RefreshKryptonRedPoint()
  if curMatrixIndex == nil then
    return
  end
  GameFleetNewEnhance.isAskKryptonRedPoint = true
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local fleetId = fleets[self.m_currentSelectFleetIndex].identity
  local param = {}
  param.matrix_index = curMatrixIndex
  param.fleet_id = fleetId
  NetMessageMgr:SendMsg(NetAPIList.krypton_redpoint_req.Code, param, GameFleetNewEnhance.KryptonRedPointCallBack, false, nil)
end
function GameFleetNewEnhance.KryptonRedPointCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_redpoint_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.krypton_redpoint_ack.Code then
    if GameFleetNewEnhance:GetFlashObject() then
      GameFleetNewEnhance.art_canupgrade = content.show_artfact_red
      DebugOut("RefreshKryptonRedPoint", content.show_artfact_red)
      DebugTable(content)
      if GameFleetNewEnhance:isCurFleetInMatrix() then
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnRedPoint", content.show_krypton_red)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setArtFactRedPoint", content.show_artupgrade_red or content.show_artfact_red)
      else
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setKryptonBtnRedPoint", false)
        GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "setArtFactRedPoint", false)
      end
    end
    return true
  end
  return false
end
function GameFleetNewEnhance:GetCurFleetInMatrix(id)
  local matrix = GameGlobalData:GetData("matrix").cells
  for k, v in pairs(matrix) do
    if v.fleet_identity == id then
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:SwapFleetKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  for k, v in pairs(self.currentFleetKrypton) do
    if v.id == kryptonId then
      for vk, vv in pairs(v.formation) do
        if vv.formation_id == curMatrixIndex then
          self.swapSrcSlot = vv.slot
        end
      end
    end
  end
  self.swapDestSlot = destSlot
  if self.swapSrcSlot == self.swapDestSlot then
    if GameFleetNewEnhance.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    return
  end
  local destItem = self.currentFleetKrypton[destSlot]
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  local identity = fleets[self.m_currentSelectFleetIndex].identity
  local krypton_fleet_move_req_param = {
    fleet_identity = identity,
    krypton_id = kryptonId,
    dest_slot = self.swapDestSlot
  }
  local function netFailedCallback()
    GameStateEquipEnhance:onEndDragItem()
    GameFleetNewEnhance:RequestKryptonInBag()
  end
  GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_fleet_move_req.Code, krypton_fleet_move_req_param, GameFleetNewEnhance.swapFleetCallback, true, netFailedCallback)
end
function GameFleetNewEnhance:SwapBagKryptonPos(kryptonId, destSlot)
  self.swapSrcSlot = -1
  for k, v in pairs(self.kryptonBag) do
    if v.id == kryptonId then
      self.swapSrcSlot = v.slot
    end
  end
  self.swapDestSlot = destSlot
  if self.swapSrcSlot == self.swapDestSlot then
    if GameFleetNewEnhance.dragItemInstance then
      GameStateEquipEnhance:onEndDragItem()
    end
    return
  end
  local destItem = self.kryptonBag[destSlot]
  DebugOut("destItem: ", destItem)
  local krypton_store_swap_req_param = {
    id_from = kryptonId,
    id_to = destItem and destItem.id or "",
    slot_to = destSlot
  }
  DebugOut("SwapBagKryptonSlot: ", self.swapSrcSlot, self.swapDestSlot)
  DebugOut("SwapBagKryptonPos: ", krypton_store_swap_req_param.id_from, krypton_store_swap_req_param.id_to, krypton_store_swap_req_param.slot_to)
  local function netFailedCallback()
    GameStateEquipEnhance:onEndDragItem()
    GameFleetNewEnhance:RequestKryptonInBag()
  end
  GameFleetNewEnhance:SendMsgWithMatrix(NetAPIList.krypton_store_swap_req.Code, krypton_store_swap_req_param, GameFleetNewEnhance.swapBagCallback, true, netFailedCallback)
end
function GameFleetNewEnhance.swapBagCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.krypton_store_swap_req.Code then
    if content.code == 0 then
      local item = GameFleetNewEnhance.kryptonBag[GameUIKrypton.swapSrcSlot]
      if GameFleetNewEnhance.dragItemInstance and item then
        GameFleetNewEnhance.dragItemInstance.DraggedItemID = -1
        local frame = ""
        local id = GameFleetNewEnhance:GetKryptonType(item)
        if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
          local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
          local localPath = "data2/" .. fullFileName
          if DynamicResDownloader:IfResExsit(localPath) then
            ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
            frame = "item_" .. id
          else
            frame = "temp"
            DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
          end
        else
          frame = "item_" .. id
        end
        GameFleetNewEnhance.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
        GameFleetNewEnhance.dragItemInstance:SetDestPosition(GameFleetNewEnhance.dragX, GameFleetNewEnhance.dragY)
        GameFleetNewEnhance.dragItemInstance:StartAutoMove()
      elseif GameFleetNewEnhance.dragItemInstance then
        GameStateEquipEnhance:onEndDragItem()
      end
      GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapSrcSlot], GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapDestSlot] = GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapDestSlot], GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapSrcSlot]
      if GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapSrcSlot] then
        GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapSrcSlot].slot = GameFleetNewEnhance.swapSrcSlot
      end
      GameFleetNewEnhance.kryptonBag[GameFleetNewEnhance.swapDestSlot].slot = GameFleetNewEnhance.swapDestSlot
      GameFleetNewEnhance.kryptonBag = GameFleetNewEnhance:ProcessBagInfo(GameFleetNewEnhance.kryptonBag)
      GameFleetNewEnhance:RefreshBagKrypton()
    else
      GameStateEquipEnhance:onEndDragItem()
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameFleetNewEnhance:GetBagSlot()
  local slot = 0
  for i = 1, GameFleetNewEnhance.grid_capacity do
    local item = self.kryptonBag[i]
    if not item then
      slot = i
      return slot
    end
  end
  return slot
end
function GameFleetNewEnhance.swapFleetCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code then
    if content.api == NetAPIList.krypton_fleet_move_req.Code then
      if content.code == 0 then
        local item = GameFleetNewEnhance.currentFleetKrypton[GameFleetNewEnhance.swapSrcSlot]
        if GameFleetNewEnhance.dragItemInstance and item then
          GameFleetNewEnhance.dragItemInstance.DraggedItemID = -1
          local frame = ""
          local id = GameFleetNewEnhance:GetKryptonType(item)
          if DynamicResDownloader:IsDynamicStuff(id, DynamicResDownloader.resType.PIC) then
            local fullFileName = DynamicResDownloader:GetFullName(id .. ".png", DynamicResDownloader.resType.PIC)
            local localPath = "data2/" .. fullFileName
            if DynamicResDownloader:IfResExsit(localPath) then
              ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
              frame = "item_" .. id
            else
              frame = "temp"
              DynamicResDownloader:AddDynamicRes(id .. ".png", DynamicResDownloader.resType.PIC, nil, nil)
            end
          else
            frame = "item_" .. id
          end
          GameFleetNewEnhance.dragItemInstance:GetFlashObject():InvokeASCallback("_root", "SetBagItem", frame)
          GameFleetNewEnhance.dragItemInstance:SetDestPosition(GameFleetNewEnhance.dragX, GameFleetNewEnhance.dragY)
          GameFleetNewEnhance.dragItemInstance:StartAutoMove()
        elseif GameFleetNewEnhance.dragItemInstance then
          GameStateEquipEnhance:onEndDragItem()
        end
      else
        GameStateEquipEnhance:onEndDragItem()
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
      return true
    end
    return false
  end
  return false
end
GameFleetNewEnhance._AllMedalData = nil
function GameFleetNewEnhance:CheckMedalButton(isEquip)
  DebugOut("CheckMedalButton", isEquip)
  local level_info = GameGlobalData:GetData("levelinfo")
  local buttonStat = 1
  local hasNews = false
  local showTip = false
  local isEquip = isEquip
  if level_info.level >= 80 then
    buttonStat = 2
  end
  if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
    showTip = true
  end
  DebugOut("buttonStat")
  DebugOut(buttonStat)
  self:GetFlashObject():InvokeASCallback("_root", "InitMedalButtonStat", buttonStat, hasNews, showTip, isEquip, GameStateEquipEnhance:IsEnhanceEnable())
end
function GameFleetNewEnhance.RequestMedalDataCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.medal_equip_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.medal_equip_ack.Code then
    GameFleetNewEnhance._AllMedalData.materials = content.materials
    GameFleetNewEnhance._AllMedalData.formations = content.formations
    DebugOutPutTable(GameFleetNewEnhance._AllMedalData, "RequestMedalDataCallBack")
    GameFleetNewEnhance:RefreshFleetMedal()
    GameFleetNewEnhance:GenerateMaterials(GameFleetNewEnhance._AllMedalData.materials)
    return true
  end
  return false
end
function GameFleetNewEnhance:RefreshFleetMedal()
  local fleetId = GameFleetNewEnhance:GetCurFleetId()
  local curmatrixindex = GameGlobalData.GlobalData.curMatrixIndex
  GameFleetNewEnhance.curFleetMedals = {}
  DebugOut("RefreshFleetMedal", fleetId, curmatrixindex)
  DebugTable(self._AllMedalData.formations)
  if curmatrixindex and self._AllMedalData and self._AllMedalData.formations then
    for k, v in ipairs(self._AllMedalData.formations) do
      if v.fleet_id == fleetId and v.formation_id == curmatrixindex then
        for k1, v1 in ipairs(v.equips) do
          GameFleetNewEnhance.curFleetMedals[v1.value] = v1.key
        end
      end
    end
  end
  GameFleetNewEnhance:SetMedalEquipSlot()
  GameFleetNewEnhance:GenerateAvailableMedal()
  if TutorialQuestManager.QuestTutorialEquipStarSystemItem:IsActive() and GameGlobalData:GetModuleStatus("medal") then
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetMedalButtonTip", false)
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "SetShowMedelTip", 1, true)
  end
end
function GameFleetNewEnhance:SetMedalEquipSlot()
  local medalEquips = {}
  DebugOutPutTable(GameFleetNewEnhance.curFleetMedals, "SetMedalEquipSlot")
  for i = 1, 5 do
    if GameFleetNewEnhance.curFleetMedals[i] then
      local medal_id = GameFleetNewEnhance.curFleetMedals[i]
      local medal = self:GetMedalDetailById(medal_id)
      if medal then
        local detail = {}
        detail.hasMedal = true
        detail.iconFrame, detail.picName = GameHelper:GetMedalIconFrameAndPic(medal.type)
        detail.Lv = medal.level
        detail.can_rank_up = medal.can_rank
        detail.id = medal_id
        detail.slot = i
        detail.quality = medal.quality
        detail.rank = medal.rank
        detail.hasNews = GameFleetNewEnhance:GetFleetMedalNews(i)
        medalEquips[#medalEquips + 1] = detail
      end
    else
      local detail = {}
      detail.hasMedal = false
      detail.hasNews = GameFleetNewEnhance:GetFleetMedalNews(i)
      medalEquips[#medalEquips + 1] = detail
    end
  end
  self:GenerateAvailableMedal()
  DebugOutPutTable(medalEquips, "SetMedalEquipSlotsff23")
  self:GetFlashObject():InvokeASCallback("_root", "SetMedalEquipSlot", medalEquips)
end
function GameFleetNewEnhance:GetFleetMedalNews(slot)
  DebugOut("GetFleetMedalNews ", slot)
  local curMatrixIndex = GameGlobalData.GlobalData.curMatrixIndex
  local newsData = GameGlobalData:GetFleetMedalRedPointDataByFormation(curMatrixIndex, GameFleetNewEnhance:GetCurFleetId())
  if not newsData then
    return false
  else
    for k, v in ipairs(newsData) do
      if v.key == slot then
        return v.value == 1
      end
    end
  end
end
function GameFleetNewEnhance.OnFleetMedalNewsChange()
  if GameFleetNewEnhance:GetFlashObject() then
    local newsArr = {}
    if not GameFleetNewEnhance.curFleetMedals then
      return
    end
    for i = 1, 5 do
      if GameFleetNewEnhance.curFleetMedals[i] then
        local detail = {}
        local medal_id = GameFleetNewEnhance.curFleetMedals[i]
        local medal = GameFleetNewEnhance:GetMedalDetailById(medal_id)
        if medal then
          detail.hasNews = GameFleetNewEnhance:GetFleetMedalNews(i)
          newsArr[#newsArr + 1] = detail
        end
      else
        local detail = {}
        detail.hasNews = GameFleetNewEnhance:GetFleetMedalNews(i)
        newsArr[#newsArr + 1] = detail
      end
    end
    GameFleetNewEnhance:GetFlashObject():InvokeASCallback("_root", "refreshMedalSlotNews", newsArr)
  end
end
function GameFleetNewEnhance:ShowMedalSelectMenu(slot)
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIStarSystemPort) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIStarSystemPort)
  end
  self.curSelectedSlot = slot
  GameUIStarSystemPort.medalEquip.isReplace = false
  GameUIStarSystemPort.medalEquip:ShowMedalSelectMenu(slot)
end
function GameFleetNewEnhance:GetCurFleetId()
  local fleets = GameGlobalData:GetData("fleetinfo").fleets
  DebugOut("GetCurFleetId", fleets[self.m_currentSelectFleetIndex].identity)
  return fleets[self.m_currentSelectFleetIndex].identity
end
function GameFleetNewEnhance:GetMedalDetailById(id)
  for k, v in ipairs(self._AllMedalData.medals) do
    if v.id == id then
      DebugTable(v)
      return v
    end
  end
  return nil
end
function GameFleetNewEnhance:GenerateAvailableMedal()
  self._AllAvailableMedal = {}
  self._CanMaterialMedal = {}
  if self._AllMedalData.medals then
    for k, v in ipairs(self._AllMedalData.medals) do
      if not self:IsMedalTypeEquiped(v.id) and (#v.formation_info == 0 or not self:IsMedalTypeEquipedInCurFormation(v.formation_info)) then
        table.insert(self._AllAvailableMedal, v)
      end
    end
    for k, v in ipairs(self._AllMedalData.medals) do
      if #v.formation_info == 0 and not self:IsMedalEquiped(v.id) and v.is_material == true then
        table.insert(self._CanMaterialMedal, v)
      end
    end
    local sorMedal = function(a, b)
      if a.quality > b.quality then
        return true
      elseif a.quality < b.quality then
        return false
      elseif a.rank > b.rank then
        return true
      elseif a.rank < b.rank then
        return false
      else
        return a.level > b.level
      end
    end
    table.sort(self._CanMaterialMedal, sorMedal)
  end
end
function GameFleetNewEnhance:IsMedalTypeEquiped(id)
  for k, v in pairs(self.curFleetMedals) do
    if v == id then
      return true
    end
  end
  return false
end
function GameFleetNewEnhance:IsMedalTypeEquipedInCurFormation(formation_info)
  local retValue = false
  for k, v in pairs(formation_info) do
    if v.formation_id == GameGlobalData.GlobalData.curMatrixIndex then
      return true
    end
  end
  return retValue
end
function GameFleetNewEnhance:IsMedalEquiped(id)
  for k, v in pairs(self.curFleetMedals) do
    if v == id then
      return true
    end
  end
  return false
end
GameFleetNewEnhance.LevelUpMaterialCnt = 0
function GameFleetNewEnhance:GenerateMaterials(pool)
  self._MaterialsDates = {}
  GameFleetNewEnhance.LevelUpMaterialCnt = 0
  table.sort(pool, function(a, b)
    return a.type < b.type
  end)
  for k, v in ipairs(pool) do
    if v.num == 1 then
      table.insert(self._MaterialsDates, v)
      if v.type < 1000 then
        GameFleetNewEnhance.LevelUpMaterialCnt = GameFleetNewEnhance.LevelUpMaterialCnt + 1
      end
    else
      for i = 1, v.num do
        table.insert(self._MaterialsDates, v)
        if v.type < 1000 then
          GameFleetNewEnhance.LevelUpMaterialCnt = GameFleetNewEnhance.LevelUpMaterialCnt + 1
        end
      end
    end
  end
end
function GameFleetNewEnhance:GetMedalCanMaterialCntByType(type, self_id)
  local cnt = 0
  for k, v in ipairs(self._AllMedalData.medals) do
    if #v.formation_info == 0 and not self:IsMedalEquiped(v.id) and v.id ~= self_id and v.type == type and v.level <= 4 then
      cnt = cnt + 1
    end
  end
  return cnt
end
function GameFleetNewEnhance:GetMaterialCntByType(type)
  if GameFleetNewEnhance._AllMedalData.materials then
    for k, v in ipairs(GameFleetNewEnhance._AllMedalData.materials) do
      if v.type == type then
        return v.num
      end
    end
  end
  return 0
end
function GameFleetNewEnhance:AddFormationData(formation_info)
  local formation
  for k, v in ipairs(self._AllMedalData.formations) do
    if v.fleet_id == formation_info.fleet_id and v.formation_id == formation_info.formation_id then
      formation = v
      break
    end
  end
  local medal = self:GetMedalDetailById(formation_info.medal_id)
  if medal then
    local medal_formation_info = {}
    medal_formation_info.medal_id = formation_info.medal_id
    medal_formation_info.fleet_id = formation_info.fleet_id
    medal_formation_info.formation_id = formation_info.formation_id
    medal_formation_info.pos = formation_info.pos
    table.insert(medal.formation_info, medal_formation_info)
    if formation then
      local equip = {
        key = formation_info.medal_id,
        value = formation_info.pos
      }
      table.insert(formation.equips, equip)
    else
      formation_info.equips = {}
      formation_info.equips[1] = {
        key = formation_info.medal_id,
        value = formation_info.pos
      }
      formation_info.medal_id = nil
      formation_info.pos = nil
      table.insert(self._AllMedalData.formations, formation_info)
    end
  else
    formation_info.equips = {}
    formation_info.equips[1] = {
      key = formation_info.medal_id,
      value = formation_info.pos
    }
    formation_info.medal_id = nil
    formation_info.pos = formation_info.pos
    GameFleetNewEnhance.curFleetMedals[formation_info.pos] = nil
    table.insert(self._AllMedalData.formations, formation_info)
  end
  DebugTable(medal)
  DebugOutPutTable(self._AllMedalData.formations, "AddFormationData")
end
function GameFleetNewEnhance:EraseFormationData(formation_info)
  local formation
  for k, v in ipairs(self._AllMedalData.formations) do
    if v.fleet_id == formation_info.fleet_id and v.formation_id == formation_info.formation_id then
      formation = v
      break
    end
  end
  if formation then
    local equip = {
      key = formation_info.medal_id,
      value = formation_info.pos
    }
    for k, v in ipairs(formation.equips) do
      if LuaUtils:table_equal(v, equip) then
        table.remove(formation.equips, k)
      end
    end
  end
  DebugTable(formation_info)
  local medal = self:GetMedalDetailById(formation_info.medal_id)
  DebugOutPutTable(medal, "EraseFormationData")
  if medal then
    for k, v in ipairs(medal.formation_info) do
      if LuaUtils:table_equal(v, formation_info) then
        table.remove(medal.formation_info, k)
      end
    end
  end
end
function GameFleetNewEnhance:OnMedalListNTF(content)
  if not self._AllMedalData then
    self._AllMedalData = {}
  end
  if not self._AllMedalData.medals then
    self._AllMedalData.medals = {}
  end
  DebugOutPutTable(content, "OnMedalListNTF " .. #content.medals)
  local isFind = false
  for k, v in ipairs(content.medals) do
    for j, w in ipairs(self._AllMedalData.medals) do
      if v.id == w.id then
        isFind = true
        self._AllMedalData.medals[j] = v
      end
    end
    if not isFind then
      table.insert(self._AllMedalData.medals, v)
    end
  end
end
function GameFleetNewEnhance.ClearXunZhangKejinCallBack(msgType, content)
  if msgType == NetAPIList.one_key_equips_ack.Code then
    if content.code_id == 1 or content.code_id == 5 or content.code_id == 2 or content.code_id == 6 then
      local GameFleetEquipment = LuaObjectManager:GetLuaObject("GameFleetEquipment")
      local xunzhang = GameFleetEquipment.xunzhang
      xunzhang:CallRequestMedal_EquipInfo()
    elseif content.code_id == 3 or content.code_id == 4 then
    end
    return true
  end
  return false
end
