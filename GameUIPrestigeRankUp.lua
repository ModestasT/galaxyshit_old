local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
local QuestTutorialPrestige = TutorialQuestManager.QuestTutorialPrestige
local GameStateBattleMap = GameStateManager.GameStateBattleMap
local GameStateGlobalState = GameStateManager.GameStateGlobalState
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameStateDaily = GameStateManager.GameStateDaily
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
GameUIPrestigeRankUp.RankUpData = {}
GameUIPrestigeRankUp.mShareData = nil
GameUIPrestigeRankUp.mIsGotoDetail = false
GameUIPrestigeRankUp.mCanShare = false
GameUIPrestigeRankUp.mShared = true
GameUIPrestigeRankUp.mPrestigeInfoData = {}
GameUIPrestigeRankUp.mTime = 0
GameUIPrestigeRankUp.mShareFleetData = nil
GameUIPrestigeRankUp.mIsShare = nil
GameUIPrestigeRankUp.mWdcRankup = false
GameUIPrestigeRankUp.mTlcRankup = false
GameUIPrestigeRankUp.mShareFleets = {}
GameUIPrestigeRankUp.isShowFleets = false
GameUIPrestigeRankUp.mHeroUnionUnlock = {}
GameUIPrestigeRankUp.ActiveFleetId = nil
function GameUIPrestigeRankUp:OnFSCommand(cmd, arg)
  if cmd == "erase_dialog" then
    if self.isShowFleets then
      table.remove(self.mShareFleets, 1)
      if #self.mShareFleets > 0 then
        self:ShowFleet()
      else
        GameUIPrestigeRankUp.isShowFleets = false
        GameStateGlobalState:EraseObject(self)
      end
    else
      if self:CheckNeedDisplay() then
        return
      end
      GameStateGlobalState:EraseObject(self)
    end
    if arg == "goto_prestige" then
      if self.curPrestigeLevel and self.curPrestigeLevel == 1 then
        if immanentversion == 1 then
          AddFlurryEvent("ClickConfirm_WhenPrestigeRankUp", {}, 1)
          AddFlurryEvent("CloseBattleResult_Enemy_1_1_4_Boss", {}, 1)
          AddFlurryEvent("TutorialDialog_15_1113", {}, 1)
        elseif immanentversion == 2 then
          AddFlurryEvent("ClickDetail_WhenPrestigeRankUp(Lv1)", {}, 1)
        end
      end
      local GameUIPrestige = LuaObjectManager:GetLuaObject("GameUIPrestige")
      GameHelper:SetEnterPage(GameHelper.PagePrestige)
      GameStateManager:SetCurrentGameState(GameStateDaily)
      if GameUIPrestigeRankUp.mCanShare and not GameUIPrestigeRankUp.mShared then
        GameUIPrestigeRankUp:GotoDetailBackup()
      end
    elseif arg == "close" then
      if self.curPrestigeLevel and self.curPrestigeLevel == 1 then
        AddFlurryEvent("ClickConfirm_WhenPrestigeRankUp", {}, 1)
      end
      if GameStateManager:GetCurrentGameState() == GameStateBattleMap then
        GameStateBattleMap:ShowDialog()
      end
    end
  elseif cmd == "close" then
    if not GameUIPrestigeRankUp.mCanShare or GameUIPrestigeRankUp.mShared or GameSettingData.fbShareCloseNoTip or GameUIPrestigeRankUp.mWdcRankup or GameUIPrestigeRankUp.mTlcRankup then
      self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveOut")
      Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
      GameUIPrestigeRankUp.mWdcRankup = false
      GameUIPrestigeRankUp.mTlcRankup = false
    else
      GameUIPrestigeRankUp:ShowDialog()
      Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
    end
  elseif cmd == "share" then
    GameUIPrestigeRankUp:Share()
  elseif cmd == "move_in_end" then
    if GameUIPrestigeRankUp.ActiveFleetId == nil then
      return
    end
    local content = GameUIPrestigeRankUp:FindUnlockHero(tonumber(GameUIPrestigeRankUp.ActiveFleetId))
    GameUIPrestigeRankUp.ActiveFleetId = nil
    if content then
      local GameHeroUnlockTip = LuaObjectManager:GetLuaObject("GameHeroUnlockTip")
      GameHeroUnlockTip.HeroUnionUnlockNtf(content)
    end
  end
end
function GameUIPrestigeRankUp:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUIPrestigeRankUp.mShareFleetData = nil
  GameUIPrestigeRankUp.mIsShare = nil
  GameUIPrestigeRankUp.mNoOk = false
  GameUIPrestigeRankUp.mDescText = ""
end
function GameUIPrestigeRankUp:OnAddToGameState(parent_state)
  self:LoadFlashObject()
  self:AnimationMoveIn()
  GameUIPrestigeRankUp.mIsGotoDetail = false
  self:GetFlashObject():SetText("_root.main.share_reward_bar.LC_MENU_SHARE_REWARDS_SLAC_", GameLoader:GetGameText("LC_MENU_SHARE_REWARDS"))
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "initFlash", lang)
end
function GameUIPrestigeRankUp:IsBuesy()
  return ...
end
function GameUIPrestigeRankUp:UpdateAwardData(prestige_data)
  DebugOut("UpdateAwardData:")
  self.curPrestigeLevel = prestige_data.prestige_level
  local prestige_name = GameDataAccessHelper:GetMilitaryRankName(prestige_data.prestige_level)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateRankInfo", prestige_data.prestige_level, prestige_name)
  local AwardSkill = GameDataAccessHelper:GetAwardSkill(prestige_data.prestige_level)
  local NewMatrixCell = GameDataAccessHelper:GetNewmatrixcell(prestige_data.prestige_level)
  local FightFleetCount = GameDataAccessHelper:GetFightfleetcnt(prestige_data.prestige_level)
  local titleText = ""
  if AwardSkill then
    titleText = GameLoader:GetGameText("LC_MENU_PRESTIGE_NEW_SKILL_CHAR")
  elseif NewMatrixCell then
    titleText = GameLoader:GetGameText("LC_MENU_UNLOCK_FORMATION_NAME")
  elseif FightFleetCount then
    titleText = GameLoader:GetGameText("LC_MENU_RECRUIT_COUNT_NAME")
  end
  GameUIPrestigeRankUp.mShareData = {type = "prestige", data = prestige_data}
  local AwardInfoString = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(prestige_data.prestige_level)
  local isFBEnable = Facebook:IsFacebookEnabled()
  local shareAwardCount = 0
  local shareAwardFrs = ""
  local shareAwardCnts = ""
  if prestige_data.shared_rewards and 0 <= #prestige_data.shared_rewards then
    for k = 1, #prestige_data.shared_rewards do
      local reward = prestige_data.shared_rewards[k]
      local iconFrame = GameHelper:GetCommonIconFrame(reward)
      local itemNum = ""
      if reward.item_type == "krypton" or reward.item_type == "item" or reward.item_type == "medal_item" then
        itemNum = reward.no
      else
        itemNum = reward.number
      end
      itemNum = GameUtils.numberConversion(tonumber(itemNum))
      shareAwardFrs = shareAwardFrs .. iconFrame .. "\001"
      shareAwardCnts = shareAwardCnts .. "x" .. itemNum .. "\001"
      shareAwardCount = shareAwardCount + 1
    end
  end
  local canShare = isFBEnable and shareAwardCount > 0
  local isHaveNextAward = false
  local nextAwardString = ""
  local nextPrestigeLevel = GameDataAccessHelper:GetNextPrestigeLevelInfo(prestige_data.prestige_level)
  if nextPrestigeLevel then
    isHaveNextAward = true
    nextAwardString = GameDataAccessHelper:GenerateMilitaryRankAwardInfo(nextPrestigeLevel)
  else
    isHaveNextAward = false
  end
  if isHaveNextAward then
    local differ = nextPrestigeLevel - prestige_data.prestige_level
    local nextAwardText = string.format(GameLoader:GetGameText("LC_MENU_NEXT_RANK_CHAR"), differ) .. ":"
    local nextAwardLevelName = GameDataAccessHelper:GetMilitaryRankName(nextPrestigeLevel)
    local nameText = nextAwardLevelName .. GameLoader:GetGameText("LC_MENU_REWARDS_CHAR")
    DebugOut("curPrestigeLevel:" .. self.curPrestigeLevel .. " nextPrestigeLevel:" .. nextPrestigeLevel .. "nextAwardText:" .. nextAwardText .. " nameText:" .. nameText)
    self:GetFlashObject():InvokeASCallback("_root", "setNextAwardInfo", nextAwardText, nameText)
  end
  GameUIPrestigeRankUp.mCanShare = canShare
  GameUIPrestigeRankUp.mShared = not canShare
  DebugOut("UpdateAwardData AwardInfoString: " .. AwardInfoString .. " shareAwardFrs: " .. shareAwardFrs .. " isFBEnable " .. tostring(isFBEnable) .. "nextAwardString:" .. nextAwardString)
  self:GetFlashObject():InvokeASCallback("_root", "SetShowType", 1)
  self:GetFlashObject():InvokeASCallback("_root", "UpdateAwardData", AwardInfoString, canShare, shareAwardCount, shareAwardFrs, shareAwardCnts, isHaveNextAward, nextAwardString, titleText)
  if not canShare then
    local btnText = GameLoader:GetGameText("LC_MENU_DETAIL_BUTTON")
    self:GetFlashObject():InvokeASCallback("_root", "SetMiddleBtnContent", btnText)
  end
end
function GameUIPrestigeRankUp:AnimationMoveIn()
  local version = 1
  if immanentversion == 2 and QuestTutorialPrestige:IsActive() then
    version = 2
  end
  self:GetFlashObject():InvokeASCallback("_root", "AnimationMoveIn", version)
end
function GameUIPrestigeRankUp:PushRankUpData(prestige_data)
  if immanentversion == 1 and QuestTutorialPrestige:IsActive() then
    QuestTutorialPrestige:SetFinish(true)
  end
  table.insert(self.RankUpData, prestige_data)
end
function GameUIPrestigeRankUp:CheckNeedDisplay()
  if DebugConfig.isSuperMode then
    return false
  end
  if GameUIPrestigeRankUp:IsBuesy() then
    return
  end
  if #self.RankUpData > 0 then
    local prestige_data = self.RankUpData[1]
    self.RankUpData = {}
    self:LoadFlashObject()
    self:UpdateAwardData(prestige_data)
    if immanentversion == 1 and prestige_data.prestige_level == 1 then
      GameStateBattleMap:SetStoryWhenFocusGain({100055, 100056})
    end
    GameStateGlobalState:AddObject(self)
    return true
  end
  return false
end
function GameUIPrestigeRankUp:DisplayAllowed()
  local ActiveState = GameStateManager:GetCurrentGameState()
  if ActiveState == GameStateBattlePlay or ActiveState:IsObjectInState(GameVip) or ActiveState:IsObjectInState(GameUIFirstCharge) then
    return false
  end
  return true
end
function GameUIPrestigeRankUp.HeroUnionUnlockNtf(content)
  GameUIPrestigeRankUp.mHeroUnionUnlock[#GameUIPrestigeRankUp.mHeroUnionUnlock + 1] = content
  DebugOut("GameUIPrestigeRankUp.HeroUnionUnlockNtf")
  DebugTable(GameUIPrestigeRankUp.mHeroUnionUnlock)
end
function GameUIPrestigeRankUp:FindUnlockHero(id)
  DebugOut("GameUIPrestigeRankUp:FindUnlockHero")
  DebugTable(GameUIPrestigeRankUp.mHeroUnionUnlock)
  for k, v in ipairs(GameUIPrestigeRankUp.mHeroUnionUnlock) do
    if v.fleet_id == id then
      local content = table.remove(GameUIPrestigeRankUp.mHeroUnionUnlock, k)
      return content
    end
  end
end
function GameUIPrestigeRankUp:ShowGetCommander(data, isShareFlag)
  local _t = {}
  _t.data = data
  _t.isShareFlag = isShareFlag
  table.insert(self.mShareFleets, _t)
  GameUIPrestigeRankUp.isShowFleets = true
  GameUIPrestigeRankUp:ShowFleet()
end
function GameUIPrestigeRankUp:ShowFleet()
  if self.mShareFleets and not LuaUtils:table_empty(self.mShareFleets) then
    self:LoadFlashObject()
    if not GameStateGlobalState:IsObjectInState(self) then
      DebugOut("GameUIPrestigeRankUp:ShowFleet")
      GameStateGlobalState:AddObject(self)
    end
    self:_showGetCommander(self.mShareFleets[1].data, self.mShareFleets[1].isShareFlag)
    GameUIPrestigeRankUp:AnimationMoveIn()
    GameUIPrestigeRankUp.ActiveFleetId = self.mShareFleets[1].data.number
  end
end
function GameUIPrestigeRankUp:_showGetCommander(data, isShareFlag)
  GameUIPrestigeRankUp.mShareFleetData = data
  GameUIPrestigeRankUp.mIsShare = isShareFlag
  if data and data.shared_rewards and #data.shared_rewards >= 0 then
    GameUIPrestigeRankUp.mShareData = {type = "commander", data = data}
    local fleetId = data.number
    local commanderInfo
    if fleetId > 0 then
      local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetId)
      local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleetId)
      local level = GameGlobalData:GetData("levelinfo").level
      commanderInfo = {}
      commanderInfo.fleetId = fleetId
      commanderInfo.rank = GameUtils:GetFleetRankFrame(commanderAbility.Rank, GameUIPrestigeRankUp.DownloadRankImageCallback)
      commanderInfo.vessleType = GameDataAccessHelper:GetCommanderVesselsType(fleetId)
      commanderInfo.head = GameDataAccessHelper:GetFleetAvatar(fleetId)
      commanderInfo.shipIcon = GameDataAccessHelper:GetShip(fleetId, nil, nil)
      commanderInfo.commanderName = GameDataAccessHelper:GetFleetLevelDisplayName(fleetId)
      commanderInfo.vessleName = GameLoader:GetGameText("LC_FLEET_" .. commanderInfo.vessleType)
      commanderInfo.skillRangeType = "TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID)
      commanderInfo.skillIcon = GameDataAccessHelper:GetSkillIcon(basic_info.SPELL_ID)
      commanderInfo.skillName = GameDataAccessHelper:GetSkillNameText(basic_info.SPELL_ID)
      commanderInfo.skillRangetypeName = GameLoader:GetGameText("LC_MENU_SKILL_RANGE_TYPE_" .. GameDataAccessHelper:GetSkillRangeType(basic_info.SPELL_ID))
      commanderInfo.damageRate = commanderAbility.Damage_Rate
      commanderInfo.defenceRate = commanderAbility.Defence_Rate
      commanderInfo.assistRate = commanderAbility.Assist_Rate
      commanderInfo.color = GameDataAccessHelper:GetCommanderColorFrame(fleetId)
      local sexx = GameDataAccessHelper:GetCommanderSex(fleetId)
      if sexx == 1 then
        sexx = "man"
      elseif sexx == 0 then
        sexx = "woman"
      else
        sexx = "unknown"
      end
      commanderInfo.sex = sexx
    end
    DebugOut("GameUIPrestigeRankUp:ShowGetItem")
    DebugTable(data)
    local flashObj = GameUIPrestigeRankUp:GetFlashObject()
    if flashObj then
      local isFBEnable = Facebook:IsFacebookEnabled()
      local shareAwardCount = 0
      local shareAwardFrs = ""
      local shareAwardCnts = ""
      local shareAwardPngs = ""
      for k = 1, #data.shared_rewards do
        local reward = data.shared_rewards[k]
        local iconFrame = GameHelper:GetCommonIconFrame(reward)
        local pngfile = GameHelper:GetItemInfo(reward).icon_pic
        local itemNum = ""
        if reward.item_type == "krypton" or reward.item_type == "item" or reward.item_type == "medal_item" then
          itemNum = reward.no
        else
          itemNum = reward.number
        end
        itemNum = GameUtils.numberConversion(tonumber(itemNum))
        shareAwardFrs = shareAwardFrs .. iconFrame .. "\001"
        shareAwardCnts = shareAwardCnts .. "x" .. itemNum .. "\001"
        shareAwardPngs = shareAwardPngs .. pngfile .. "\001"
        shareAwardCount = shareAwardCount + 1
      end
      local canShare = isFBEnable and shareAwardCount > 0 and isShareFlag
      local btnText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_BUTTON")
      local btnCmd = "share"
      if canShare then
        local shareRewardText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_SHARE")
        flashObj:InvokeASCallback("_root", "SetShareRewardText", shareRewardText)
      else
        btnText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        btnCmd = "close"
      end
      DebugOut("SetCommander 0")
      flashObj:InvokeASCallback("_root", "SetCommander", commanderInfo, canShare, shareAwardCount, shareAwardFrs, shareAwardCnts, shareAwardPngs)
      DebugOut("shareAwardPngs", shareAwardPngs)
      flashObj:InvokeASCallback("_root", "SetMiddleBtnContent", btnText, btnCmd)
      flashObj:InvokeASCallback("_root", "SetShowBtnCount", 1)
      flashObj:InvokeASCallback("_root", "SetShowType", 4)
      flashObj:InvokeASCallback("_root", "SetMiddleBtnShow", canShare)
      GameUIPrestigeRankUp.mCanShare = canShare
      GameUIPrestigeRankUp.mShared = not canShare
    end
  end
end
function GameUIPrestigeRankUp.DownloadRankImageCallback(extInfo)
  if GameUIPrestigeRankUp:GetFlashObject() then
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "updateRankImage", extInfo.rank_id)
  end
end
function GameUIPrestigeRankUp:UpdateFleetInfo(data)
  if data and data.shared_rewards and #data.shared_rewards >= 0 then
    GameUIPrestigeRankUp.mShareData = {type = "commander", data = data}
    local fleetId = data.number
    local commanderInfo
    if fleetId > 0 then
      local basic_info = GameDataAccessHelper:GetCommanderBasicInfo(fleetId)
      local commanderAbility = GameDataAccessHelper:GetCommanderAbility(fleetId)
      local level = GameGlobalData:GetData("levelinfo").level
      commanderInfo = {}
      commanderInfo.head = GameDataAccessHelper:GetFleetAvatar(fleetId)
      commanderInfo.shipIcon = GameDataAccessHelper:GetShip(fleetId, nil, nil)
    end
    if GameUIPrestigeRankUp:GetFlashObject() then
      GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "SetFleetInfo", commanderInfo)
    end
  end
end
function GameUIPrestigeRankUp:Update(dt)
  if not self:GetFlashObject() then
    return
  end
  self:GetFlashObject():Update(dt)
  GameUIPrestigeRankUp.mTime = GameUIPrestigeRankUp.mTime + dt
  if GameUIPrestigeRankUp.mTime > 1000 then
    GameUIPrestigeRankUp.mTime = 0
    if GameUIPrestigeRankUp.mShareFleetData ~= nil and GameUIPrestigeRankUp.mIsShare ~= nil then
      DebugOut("GameUIPrestigeRankUp:Update = ")
      DebugTable(GameUIPrestigeRankUp.mShareFleetData)
      GameUIPrestigeRankUp:UpdateFleetInfo(GameUIPrestigeRankUp.mShareFleetData)
    end
  end
end
function GameUIPrestigeRankUp:ShowAchievement(data)
  if GameUIPrestigeRankUp:IsBuesy() then
    return
  end
  if data and data.shared_rewards and #data.shared_rewards >= 0 then
    GameUIPrestigeRankUp.mShareData = {
      type = "achievement",
      data = data
    }
    GameUIPrestigeRankUp:LoadFlashObject()
    DebugOut("GameUIPrestigeRankUp:ShowAchievement")
    DebugTable(data)
    local flashObj = GameUIPrestigeRankUp:GetFlashObject()
    if flashObj then
      local icon = "icon0" .. data.level
      local GameUIAchievement = LuaObjectManager:GetLuaObject("GameUIAchievement")
      local name = GameUIAchievement:GetAchievementTitle(data.id)
      local desc = GameUIAchievement:GetAchievementDesc(data.id)
      local isFBEnable = Facebook:IsFacebookEnabled()
      local shareAwardCount = 0
      local shareAwardFrs = ""
      local shareAwardCnts = ""
      local shareAwardPngs = ""
      for k = 1, #data.shared_rewards do
        local reward = data.shared_rewards[k]
        local iconFrame = GameHelper:GetCommonIconFrame(reward)
        local pngfile = GameHelper:GetItemInfo(reward).icon_pic
        local itemNum = ""
        if reward.item_type == "krypton" or reward.item_type == "item" or reward.item_type == "medal_item" then
          itemNum = reward.no
        else
          itemNum = reward.number
        end
        itemNum = GameUtils.numberConversion(tonumber(itemNum))
        shareAwardFrs = shareAwardFrs .. iconFrame .. "\001"
        shareAwardCnts = shareAwardCnts .. "x" .. itemNum .. "\001"
        shareAwardPngs = shareAwardPngs .. pngfile .. "\001"
        shareAwardCount = shareAwardCount + 1
      end
      local awardCount = 0
      local awardFrs = ""
      local awardCnts = ""
      local awardPngs = ""
      for k = 1, #data.awards do
        local reward = data.awards[k]
        local iconFrame = GameHelper:GetCommonIconFrame(reward)
        local pngfile = GameHelper:GetItemInfo(reward).icon_pic
        local itemNum = ""
        if reward.item_type == "krypton" or reward.item_type == "item" or reward.item_type == "medal_item" then
          itemNum = reward.no
        else
          itemNum = reward.number
        end
        itemNum = GameUtils.numberConversion(tonumber(itemNum))
        awardFrs = awardFrs .. iconFrame .. "\001"
        awardCnts = awardCnts .. "x" .. itemNum .. "\001"
        awardPngs = awardPngs .. pngfile .. "\001"
        awardCount = awardCount + 1
      end
      local canShare = isFBEnable and shareAwardCount > 0
      local btnText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_BUTTON")
      local btnCmd = "share"
      if canShare then
        local shareRewardText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_SHARE")
        flashObj:InvokeASCallback("_root", "SetShareRewardText", shareRewardText)
      else
        btnText = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
        btnCmd = "close"
      end
      DebugOut("icon:" .. icon .. " name" .. name .. " desc" .. desc .. " awardCount" .. awardCount .. " awardFrs" .. awardFrs .. " awardCnts" .. awardCnts .. "shareAwardCount" .. shareAwardCount .. " shareAwardFrs" .. shareAwardFrs .. " shareAwardCnts" .. shareAwardCnts)
      flashObj:InvokeASCallback("_root", "SetMiddleBtnContent", btnText, btnCmd)
      flashObj:InvokeASCallback("_root", "SetShowBtnCount", 1)
      flashObj:InvokeASCallback("_root", "SetShowType", 2)
      flashObj:InvokeASCallback("_root", "SetAchievement", icon, name, desc, awardCount, awardFrs, awardCnts, awardPngs, canShare, shareAwardCount, shareAwardFrs, shareAwardCnts, shareAwardPngs)
      DebugOut("shareAwardPngs", shareAwardPngs)
      DebugOut("awardPngs", awardPngs)
      GameUIPrestigeRankUp.mCanShare = canShare
      GameUIPrestigeRankUp.mShared = not canShare
      GameStateGlobalState:AddObject(GameUIPrestigeRankUp)
    end
  end
end
function GameUIPrestigeRankUp:Share()
  if GameUIPrestigeRankUp.mShareData then
    local gameUrl = "http://tap4fun.com/en/game/7"
    local title = "GL Title 2"
    local subTitle = "Galaxy Legend"
    local describe = " "
    local picUrl = " "
    if "prestige" == GameUIPrestigeRankUp.mShareData.type then
      title = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_INFO_3")
      DebugOut("prestige: " .. title)
      if title then
        local prestige_name = GameDataAccessHelper:GetMilitaryRankName(GameUIPrestigeRankUp.mShareData.data.prestige_level)
        title = string.format(title, prestige_name)
      else
        title = "Rank Up"
      end
      picUrl = Facebook.mImgUrl .. "images/prestige_" .. tostring(GameUIPrestigeRankUp.mShareData.data.prestige_level) .. ".png"
    elseif "achievement" == GameUIPrestigeRankUp.mShareData.type then
      title = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_INFO_1")
      DebugOut("achievement: " .. title)
      if title then
        local GameUIAchievement = LuaObjectManager:GetLuaObject("GameUIAchievement")
        local achieveName = GameUIAchievement:GetAchievementTitle(GameUIPrestigeRankUp.mShareData.data.id)
        local level = GameLoader:GetGameText("LC_ACHIEVEMENT_LEVEL_" .. tostring(GameUIPrestigeRankUp.mShareData.data.level))
        title = string.gsub(title, "<ranklevel>", level)
        title = string.gsub(title, "<rankname>", achieveName)
      else
        title = "Achievement"
      end
      picUrl = Facebook.mImgUrl .. "images/achievement_lv_" .. tostring(GameUIPrestigeRankUp.mShareData.data.level) .. ".png"
    elseif "item" == GameUIPrestigeRankUp.mShareData.type then
      title = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_INFO_4")
      DebugOut("item: " .. title)
      if title then
        local itemName = GameHelper:GetAwardText(GameUIPrestigeRankUp.mShareData.data.item_type, GameUIPrestigeRankUp.mShareData.data.number, GameUIPrestigeRankUp.mShareData.data.no)
        title = string.format(title, itemName)
      else
        title = "Item"
      end
      picUrl = Facebook.mImgUrl .. "images/item_" .. tostring(GameUIPrestigeRankUp.mShareData.data.number) .. ".png"
    elseif "commander" == GameUIPrestigeRankUp.mShareData.type then
      title = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_INFO_5")
      DebugOut("commander: " .. title)
      if title then
        local commanderName = GameDataAccessHelper:GetFleetLevelDisplayName(GameUIPrestigeRankUp.mShareData.data.number)
        title = string.format(title, commanderName)
      else
        title = "Commander"
      end
      picUrl = Facebook.mImgUrl .. "images/commander_" .. tostring(GameUIPrestigeRankUp.mShareData.data.number) .. ".png"
    end
    local function callback(success)
      if success then
        GameUIPrestigeRankUp.mShared = true
        GameUIPrestigeRankUp:SetShareBtnEnable(false)
        GameUIPrestigeRankUp:SendShareSuccess()
      end
    end
    Facebook:ShareLink(gameUrl, title, subTitle, describe, picUrl, callback)
  end
end
GameUIPrestigeRankUp.mGetRewardReq = nil
function GameUIPrestigeRankUp:SendShareSuccess()
  if GameUIPrestigeRankUp.mShareData then
    local req = {}
    if "prestige" == GameUIPrestigeRankUp.mShareData.type then
      req = {
        type = 3,
        itemkey = {
          item_type = "no",
          number = 1,
          no = 1,
          level = 1
        },
        intkey = GameUIPrestigeRankUp.mShareData.data.prestige_level
      }
    elseif "achievement" == GameUIPrestigeRankUp.mShareData.type then
      req = {
        type = 2,
        itemkey = {
          item_type = "no",
          number = 1,
          no = 1,
          level = 1
        },
        intkey = GameUIPrestigeRankUp.mShareData.data.id
      }
    elseif "item" == GameUIPrestigeRankUp.mShareData.type then
      req = {
        type = 1,
        itemkey = {
          item_type = GameUIPrestigeRankUp.mShareData.data.item_type,
          number = GameUIPrestigeRankUp.mShareData.data.number,
          no = GameUIPrestigeRankUp.mShareData.data.no,
          level = GameUIPrestigeRankUp.mShareData.data.level
        },
        intkey = 1
      }
    elseif "commander" == GameUIPrestigeRankUp.mShareData.type then
      req = {
        type = 1,
        itemkey = {
          item_type = GameUIPrestigeRankUp.mShareData.data.item_type,
          number = GameUIPrestigeRankUp.mShareData.data.number,
          no = GameUIPrestigeRankUp.mShareData.data.no,
          level = GameUIPrestigeRankUp.mShareData.data.level
        },
        intkey = 1
      }
    end
    if NetMessageMgr.ReConnect then
      DebugOut("Now is reconnect server.")
      GameUIPrestigeRankUp.mGetRewardReq = req
      NetMessageMgr:AddReconnectSuccessCall(GameUIPrestigeRankUp.SendCacheReq)
    else
      DebugOut("SendShareSuccess")
      DebugTable(req)
      NetMessageMgr:SendMsg(NetAPIList.give_share_awards_req.Code, req, GameUIPrestigeRankUp.SendShareSuccessCallback, true, nil)
    end
    GameUIPrestigeRankUp.mShareData = nil
  end
end
function GameUIPrestigeRankUp.SendCacheReq()
  if GameUIPrestigeRankUp.mGetRewardReq then
    DebugOut("SendShareSuccess")
    DebugTable(req)
    NetMessageMgr:SendMsg(NetAPIList.give_share_awards_req.Code, GameUIPrestigeRankUp.mGetRewardReq, GameUIPrestigeRankUp.SendShareSuccessCallback, true, nil)
  end
end
function GameUIPrestigeRankUp.SendShareSuccessCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.give_share_awards_req.Code then
    DebugOut("SendShareSuccessCallback err.")
    DebugTable(content)
    if content.code ~= 0 then
      local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
    end
    return true
  end
  return false
end
function GameUIPrestigeRankUp:SetShareBtnEnable(isEnable)
  local flashObj = GameUIPrestigeRankUp:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetMiddleBtnEnable", isEnable)
    flashObj:InvokeASCallback("_root", "SetRightBtnEnable", isEnable)
    flashObj:InvokeASCallback("_root", "SetRankupShareButton", isEnable)
  end
end
function GameUIPrestigeRankUp:GotoDetailBackup()
  if GameUIPrestigeRankUp.mShareData then
    GameUIPrestigeRankUp.mIsGotoDetail = true
  else
    GameUIPrestigeRankUp.mIsGotoDetail = false
  end
end
function GameUIPrestigeRankUp:BackFromDetail()
  GameUIPrestigeRankUp.mIsGotoDetail = false
  if GameUIPrestigeRankUp.mShareData then
    GameUIPrestigeRankUp:PushRankUpData(GameUIPrestigeRankUp.mShareData.data)
    GameUIPrestigeRankUp:CheckNeedDisplay()
  end
end
function GameUIPrestigeRankUp:ShowDialog()
  local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
  local function yesCallback()
    if 1 == GameUIMessageDialog.isCheck then
      GameSettingData.fbShareCloseNoTip = true
    end
    local flashObj = GameUIPrestigeRankUp:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "AnimationMoveOut")
    end
  end
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(yesCallback)
  GameUIMessageDialog:SetNoButton(nil)
  local tip = GameLoader:GetGameText("LC_MENU_FACEBOOK_SHARE_ALERT_1")
  GameUIMessageDialog:Display("", tip)
  GameUIMessageDialog.isCheck = 0
  local notShowAgain = GameLoader:GetGameText("LC_MENU_DONT_SHOW_AGAIN_CHAR")
  GameUIMessageDialog:ShowCheckBox(notShowAgain)
end
function GameUIPrestigeRankUp.PrestigeInfoNtfNetCallback(content)
  if content then
    GameUIPrestigeRankUp.mPrestigeInfoData = content.prestige_infos
  end
end
function GameUIPrestigeRankUp:GetNextPrestigeLevelHaveAward()
  for key, value in ipairs(GameUIPrestigeRankUp.mPrestigeInfoData) do
    if value.prestige_level > self.curPrestigeLevel and value.new_matrix_cell > 0 then
      DebugTable(value)
      return value
    end
  end
end
function GameUIPrestigeRankUp:PushTlcPlayerRankUpdate(content)
  DebugOut("PushTlcPlayerRankUpdate = ")
  DebugTable(content)
  if GameUIPrestigeRankUp:IsBuesy() then
    return
  end
  GameUIPrestigeRankUp.mWdcRankup = true
  self.rankImg = content.rank_img
  local extInfo = {
    rankImg = content.rank_img
  }
  local rankFrame = GameUtils:GetPlayerRankingInWdc(self.rankImg, extInfo, GameUIPrestigeRankUp.DownloadOverCallbackTlcRank)
  rankFrame = rankFrame .. "_tlc"
  DebugOut("PushTlcPlayerRankUpdate_rankFrame " .. rankFrame)
  local rankText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADENAME_" .. content.rank_name)
  local nextRankText = GameLoader:GetGameText("LC_MENU_TEAMLEAGUE_GRADENAME_" .. content.next_rank_name)
  local _text = string.gsub(GameLoader:GetGameText("LC_MENU_TLC_RANK_UPGRADE_NEXT"), "<number1>", nextRankText)
  local _score = content.next_rank_score - content.score
  local info = string.gsub(_text, "<number2>", _score)
  if tonumber(content.next_rank_score) == 0 then
    info = ""
  end
  GameUIPrestigeRankUp:LoadFlashObject()
  if GameUIPrestigeRankUp:GetFlashObject() then
    DebugOut("xd")
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "SetShowType", 6)
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "setTlcRankInfo", rankFrame, rankText, info)
  end
  GameStateGlobalState:AddObject(self)
end
function GameUIPrestigeRankUp:PushWdcPlayerRankUpdate(content)
  DebugOut("PushWdcPlayerRankUpdate = ")
  DebugTable(content)
  if GameUIPrestigeRankUp:IsBuesy() then
    return
  end
  GameUIPrestigeRankUp.mTlcRankup = true
  self.rankImg = content.rank_img
  local extInfo = {
    rankImg = content.rank_img
  }
  local rankFrame = GameUtils:GetPlayerRankingInWdc(self.rankImg, extInfo, GameUIPrestigeRankUp.DownloadOverCallbackWdcRank)
  local rankText = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDALNAME_" .. content.rank_name)
  local nextRankText = GameLoader:GetGameText("LC_MENU_LEAGUE_MEDALNAME_" .. content.next_rank_name)
  local _text = string.gsub(GameLoader:GetGameText("LC_MENU_GLC_RANK_UPGRADE_NEXT"), "<number1>", nextRankText)
  local _score = content.next_rank_score - content.score
  local info = string.gsub(_text, "<number2>", _score)
  if tonumber(content.next_rank_score) == 0 then
    info = ""
  end
  GameUIPrestigeRankUp:LoadFlashObject()
  if GameUIPrestigeRankUp:GetFlashObject() then
    DebugOut("xd")
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "SetShowType", 5)
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "setWdcRankInfo", rankFrame, rankText, info)
  end
  GameStateGlobalState:AddObject(self)
end
function GameUIPrestigeRankUp.DownloadOverCallbackWdcRank(extInfo)
  local rankFrame = GameUtils:GetPlayerRankingInWdc(extInfo.rankImg, nil, nil)
  DebugOut("rankFrame = " .. rankFrame)
  if GameUIPrestigeRankUp:GetFlashObject() then
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "updateWdcRankInfo", rankFrame)
  end
end
function GameUIPrestigeRankUp.DownloadOverCallbackTlcRank(extInfo)
  local rankFrame = GameUtils:GetPlayerRankingInWdc(extInfo.rankImg, nil, nil)
  rankFrame = rankFrame .. "_tlc"
  DebugOut("rankFrame = " .. rankFrame)
  if GameUIPrestigeRankUp:GetFlashObject() then
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "updateTlcRankInfo", rankFrame)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIPrestigeRankUp.OnAndroidBack()
    GameUIPrestigeRankUp:GetFlashObject():InvokeASCallback("_root", "AnimationMoveOut")
  end
end
