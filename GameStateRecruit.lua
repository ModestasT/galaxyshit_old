local GameStateRecruit = GameStateManager.GameStateRecruit
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameUIRecruitWarpgate = LuaObjectManager:GetLuaObject("GameUIRecruitWarpgate")
local GameUIBuilding = LuaObjectManager:GetLuaObject("GameUIBuilding")
local GameUIRecruitMainNew = LuaObjectManager:GetLuaObject("GameUIRecruitMainNew")
local GameUIHeroUnion = LuaObjectManager:GetLuaObject("GameUIHeroUnion")
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local QuestTutorialStarCharge = TutorialQuestManager.QuestTutorialStarCharge
GameStateRecruit.Tabtype = "recruitNormal"
GameStateRecruit.buildingName = "star_portal"
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local NetMessageMgr = NetMessageMgr
local NetAPIList = NetAPIList
function GameStateRecruit:SelectTab(tab_type)
  DebugOut("SelectTab ", tab_type)
  GameUIRecruitTopbar:SelectTabItem(tab_type)
  local tab_type = self:GetActiveTab()
  DebugOut(tab_type)
  GameUIRecruitTopbar:GetFlashObject():InvokeASCallback("_root", "HideWarpGateTutorial")
  GameUIRecruitTopbar:GetFlashObject():InvokeASCallback("_root", "HideRecruitNormalTutorial")
  if tab_type == "recruitNormal" then
    GameUIRecruitMain:SetVisible(nil, false)
    GameUIHeroUnion:SetVisible(nil, false)
    GameUIRecruitMainNew:SetVisible(nil, true)
    GameUIRecruitMainNew:UpdateGameData()
    GameSetting:SetUnlockNewFleet(false)
    GameUIRecruitTopbar:SetShowFilterMenu(false)
  elseif tab_type == "warpgate" then
    GameUIRecruitMain:SetVisible(nil, false)
    GameUIRecruitMainNew:SetVisible(nil, false)
    GameUIHeroUnion:SetVisible(nil, true)
    GameUIHeroUnion:UpdateGameData()
    GameUIRecruitTopbar:SetShowFilterMenu(false)
  elseif tab_type == "owned" then
    GameUIRecruitMainNew:SetVisible(nil, false)
    GameUIHeroUnion:SetVisible(nil, false)
    GameUIRecruitMain:SetVisible(nil, true)
    GameUIRecruitMain:UpdateGameData()
    GameUIRecruitTopbar:SetShowFilterMenu(false)
  elseif tab_type == "gallary" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateHeroHandbook)
  else
    assert(false)
  end
end
function GameStateRecruit:GetActiveTab()
  return (...), GameUIRecruitTopbar
end
function GameStateRecruit:InitGameState()
end
function GameStateRecruit:OnFocusGain(pre_gamestate)
  self._prevActiveGameState = pre_gamestate
  GameUIRecruitMain:LoadFlashObject()
  GameUIHeroUnion:LoadFlashObject()
  GameUIRecruitMainNew:LoadFlashObject()
  GameUIRecruitTopbar:LoadFlashObject()
  self:AddObject(GameUIRecruitMain)
  self:AddObject(GameUIHeroUnion)
  self:AddObject(GameUIRecruitMainNew)
  self:AddObject(GameUIRecruitTopbar)
  self:SelectTab(self.Tabtype)
end
function GameStateRecruit:SetTabtype(tabtype)
  self.Tabtype = tabtype
end
function GameStateRecruit:OnFocusLost(next_gamestate)
  GameUIRecruitMain:ClearLocalData()
  self:EraseObject(GameUIRecruitMain)
  self:EraseObject(GameUIHeroUnion)
  self:EraseObject(GameUIRecruitMainNew)
  self:EraseObject(GameUIRecruitTopbar)
  self.Tabtype = "recruitNormal"
end
function GameStateRecruit:Quit()
  if self._prevActiveGameState ~= GameStateManager.GameStateHeroHandbook then
    if self._prevActiveGameState == GameStateManager.GameStateInstance then
      GameStateManager.GameStateInstance:TryToRecoverLadderData()
    end
    GameStateManager:SetCurrentGameState(self._prevActiveGameState)
    if GameStateRecruit.battle_failed then
      DebugOut("xxxxxxxx")
      GameStateManager:GetCurrentGameState():AddObject(GameUIBattleResult)
      local param = {cli_data = "recruit"}
      NetMessageMgr:SendMsg(NetAPIList.battle_rate_req.Code, param, nil, false, nil)
      GameStateRecruit.battle_failed = nil
    end
  else
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateMainPlanet)
  end
end
