local GameStateWVE = GameStateManager.GameStateWVE
local WVEGameManeger = LuaObjectManager:GetLuaObject("WVEGameManeger")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local WVEGameTutorial = LuaObjectManager:GetLuaObject("WVEGameTutorial")
function GameStateWVE:InitGameState()
end
function GameStateWVE:OnFocusGain(previousState)
  self.lastState = previousState
  if WVEGameManeger:GetGameState() == EWVEGameState.STATE_ENTER_TUTORIAL then
    self:AddObject(WVEGameTutorial)
    WVEGameTutorial:StartTurorail()
  else
    self:AddObject(WVEGameManeger)
  end
end
function GameStateWVE:OnFocusLost(newState)
  GameStateWVE.nextState = newState
  self:EraseObject(WVEGameManeger)
end
function GameStateWVE:OnMultiTouchBegin(x1, y1, x2, y2)
  if GameStateBase.OnMultiTouchBegin(self, x1, y1, x2, y2) then
    return true
  end
  self.m_multiTouchPreCenterX = (x1 + x2) / 2
  self.m_multiTouchPreCenterY = (y1 + y2) / 2
  self.m_multiTouchPreDistance = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
  return true
end
function GameStateWVE:OnMultiTouchEnd(x1, y1, x2, y2)
  if GameStateBase.OnMultiTouchEnd(self, x1, y1, x2, y2) then
    return true
  end
  return true
end
function GameStateWVE:OnMultiTouchMove(x1, y1, x2, y2)
  if GameStateBase.OnMultiTouchMove(self, x1, y1, x2, y2) then
    return true
  end
  if not self.m_multiTouchPreDistance then
    return
  end
  local curDistance = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
  local centerX = (x1 + x2) / 2
  local centerY = (y1 + y2) / 2
  local scaleDelta = curDistance - self.m_multiTouchPreDistance
  local offsetX = centerX - self.m_multiTouchPreCenterX
  local offsetY = centerY - self.m_multiTouchPreCenterY
  self.m_multiTouchPreCenterX = centerX
  self.m_multiTouchPreCenterY = centerY
  self.m_multiTouchPreDistance = curDistance
  if WVEGameManeger:GetFlashObject() then
    WVEGameManeger:GetFlashObject():InvokeASCallback("_root", "WVEMAP_MultiMove", centerX, centerY, scaleDelta, offsetX, offsetY)
  end
  return true
end
