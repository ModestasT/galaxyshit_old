local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
DynamicResDownloader = {
  waitDownloadResList = {},
  curDownloadIndex = -1,
  initFinished = false,
  resType = {
    PIC = 1,
    MINE_PIC = 2,
    EXTRA_TEXT = 3,
    DATA = 4,
    STORE_BANNER = 5,
    FESTIVAL_BANNER = 6,
    NET_ALERT_CODE = 7,
    FLEET_DISPLAY_INFO = 8,
    EMOJI_PIC = 9,
    PAY_PIC = 10,
    INFINITE_PIC = 11,
    RANK_PIC = 12,
    RANKING_PIC = 13,
    WELCOME_PIC = 14,
    HALFPORTRAIT = 15,
    SCRATCH_RESOURCE = 16,
    MEDAL = 17
  },
  cndURL = ""
}
LocalExtraVerTable = nil
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local versionPrefix
function DynamicResDownloader.getTimeHourStr()
  local time = os.time()
  local timeStr = os.date("!%H", time)
  return timeStr or "Unknow"
end
function DynamicResDownloader:Init()
  DebugOut("init immanentversion = ", immanentversion)
  ext.dofile("levl.tfl")
  if LocalExtraVerTable == nil then
    LocalExtraVerTable = {}
    LocalExtraVerTable.text = 0
    LocalExtraVerTable.netProtocal = 0
    LocalExtraVerTable.images = 0
    LocalExtraVerTable.v1fleetInfo = 0
    LocalExtraVerTable.v2fleetInfo = 0
    ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
  end
  if LocalExtraVerTable.v1fleetInfo == nil then
    LocalExtraVerTable.v1fleetInfo = 0
    LocalExtraVerTable.v2fleetInfo = 0
    ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
  end
  ext.dofile("EXTRA_TEXT.tfl")
  ext.dofile("AlertDataListExtra.tfl")
  ext.dofile("pcl.tfl")
  ext.dofile("ExtraArtifactData.tfl")
  ext.dofile("v1/" .. "ExtraFleetInfo.tfl")
  ext.dofile("v2/" .. "ExtraFleetInfo.tfl")
  self:CheckExtraCDNUrl()
end
function DynamicResDownloader:GetFullName(resFile, resType)
  if resType == self.resType.PIC then
    return "LAZY_LOAD_dynamic_" .. resFile
  elseif resType == self.resType.MINE_PIC then
    return "LAZY_LOAD_dynamic_" .. resFile
  elseif resType == self.resType.STORE_BANNER or resType == self.resType.FESTIVAL_BANNER then
    return resFile
  elseif resType == self.resType.TEXT then
    return "EXTRA_TEXT.tfl"
  elseif resType == self.resType.DATA then
    return resFile
  elseif resType == self.resType.NET_ALERT_CODE then
    return "AlertDataListExtra.tfl"
  elseif resType == self.resType.FLEET_DISPLAY_INFO then
    return versionPrefix .. "ExtraFleetInfo.tfl"
  elseif resType == self.resType.EMOJI_PIC then
    return resFile
  elseif resType == self.resType.PAY_PIC then
    return resFile
  elseif resType == self.resType.INFINITE_PIC then
    return resFile
  elseif resType == self.resType.RANK_PIC then
    return resFile
  elseif resType == self.resType.RANKING_PIC then
    return resFile
  elseif resType == self.resType.WELCOME_PIC then
    return resFile
  elseif resType == self.resType.HALFPORTRAIT then
    return resFile
  elseif resType == self.resType.SCRATCH_RESOURCE then
    return resFile
  elseif resType == self.resType.MEDAL then
    return resFile
  else
    return ""
  end
end
function DynamicResDownloader:IsDynamicStuffByGameItem(game_item)
  if GameHelper:IsResource(game_item.item_type) then
    return false
  else
    return ...
  end
end
function DynamicResDownloader:IsDynamicStuffByBuffItem(buff_item)
  if GameHelper:IsResource(buff_item.key) then
    return false
  else
    return ...
  end
end
function DynamicResDownloader:IsDynamicStuff(itemId, resType)
  local isDynamicStuff = false
  if resType == self.resType.PIC then
    if type(itemId) == "number" then
      if isDynamicStuff == false then
        isDynamicStuff = itemId >= 20001 and itemId < 30000
      end
      if isDynamicStuff == false and itemId >= 50000 and itemId <= 59999 then
        isDynamicStuff = itemId > 52100 or itemId % 10 == 6 or itemId % 10 == 7 or itemId % 10 == 8 or itemId > 51500 and itemId < 51506
      end
    end
  elseif resType == self.resType.MINE_PIC and type(itemId) == "number" then
    isDynamicStuff = itemId >= 50 and itemId < 100
  end
  return isDynamicStuff
end
function DynamicResDownloader:AddDynamicRes(resFileName, resType, extendInfo, callback)
  local fullName = self:GetFullName(resFileName, resType)
  for i, v in ipairs(self.waitDownloadResList) do
    if v.resName == resFileName then
      local callbackItem = {}
      if type(extendInfo) == "table" then
        callbackItem.extInfo = LuaUtils:table_rcopy(extendInfo)
      else
        callbackItem.extInfo = nil
      end
      if type(callback) ~= "function" then
        callbackItem.callback = nil
      else
        callbackItem.callback = callback
      end
      table.insert(v.callbackItems, callbackItem)
      if i > self.curDownloadIndex + 1 and i > 4 then
        if self.curDownloadIndex > 0 then
          local st = LuaUtils:table_rcopy(v)
          table.remove(self.waitDownloadResList, i)
          table.insert(self.waitDownloadResList, self.curDownloadIndex + 1, st)
        else
          local st = LuaUtils:table_rcopy(v)
          table.remove(self.waitDownloadResList, i)
          table.insert(self.waitDownloadResList, 1, st)
        end
      end
      return
    end
  end
  local resItem = {}
  resItem.resType = resType
  resItem.resName = resFileName
  resItem.localName = fullName
  resItem.lastTryDownloadTimestamp = -1
  resItem.callbackItems = {}
  local callbackItem = {}
  if type(extendInfo) == "table" then
    callbackItem.extInfo = LuaUtils:table_rcopy(extendInfo)
  else
    callbackItem.extInfo = nil
  end
  if type(callback) ~= "function" then
    callbackItem.callback = nil
  else
    callbackItem.callback = callback
  end
  table.insert(resItem.callbackItems, callbackItem)
  table.insert(self.waitDownloadResList, resItem)
end
function DynamicResDownloader:Update()
  if not self.initFinished then
    self.initFinished = true
    print("init here!!!!!")
    DynamicResDownloader:Init()
    return
  end
  if self.cndURL == "" then
    return
  end
  if #self.waitDownloadResList <= 0 or self.curDownloadIndex ~= -1 then
    return
  end
  for i, v in ipairs(self.waitDownloadResList) do
    if v.lastTryDownloadTimestamp == -1 or os.time() - v.lastTryDownloadTimestamp > 600 then
      self.curDownloadIndex = i
      v.lastTryDownloadTimestamp = os.time()
      break
    else
    end
  end
  if self.curDownloadIndex == -1 then
    return
  end
  local resFileName = self.waitDownloadResList[self.curDownloadIndex].resName
  local localPath = "data2/" .. self.waitDownloadResList[self.curDownloadIndex].localName
  local resUrl = ""
  if self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.MINE_PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.STORE_BANNER then
    resUrl = self.waitDownloadResList[self.curDownloadIndex].callbackItems[1].extInfo.pngFullPath
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.FESTIVAL_BANNER then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.EXTRA_TEXT then
    resUrl = self.cndURL .. "/text/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.NET_ALERT_CODE then
    resUrl = self.cndURL .. "/netProtocal/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.FLEET_DISPLAY_INFO then
    resUrl = self.cndURL .. "/extraFleetInfo/" .. versionPrefix .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.EMOJI_PIC then
    resUrl = self.cndURL .. "/emoji/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.PAY_PIC then
    resUrl = self.cndURL .. "/pay/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.INFINITE_PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.RANK_PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.RANKING_PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.WELCOME_PIC then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.HALFPORTRAIT then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.SCRATCH_RESOURCE then
    resUrl = self.cndURL .. "/images/" .. resFileName
  elseif self.waitDownloadResList[self.curDownloadIndex].resType == self.resType.MEDAL then
    resUrl = self.cndURL .. "/images/" .. resFileName
  else
    self.curDownloadIndex = -1
    return
  end
  local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
  DebugOut("begin download res file", resUrl, localPath)
  ext.http.requestDownload({
    resUrl,
    method = "GET",
    localpath = localPath,
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        DebugOut("download error " .. localPath .. tostring(errstr))
        table.insert(self.waitDownloadResList, self.waitDownloadResList[self.curDownloadIndex])
        table.remove(self.waitDownloadResList, self.curDownloadIndex)
        self.curDownloadIndex = -1
        return
      end
      ext.UpdateAutoUpdateFile(content, 0, 0, 0)
      GameSetting:SaveDownloadFileName(localPath)
      for i, v in ipairs(self.waitDownloadResList[self.curDownloadIndex].callbackItems) do
        if v.callback ~= nil then
          v.callback(v.extInfo)
        end
      end
      table.remove(self.waitDownloadResList, self.curDownloadIndex)
      self.curDownloadIndex = -1
      DebugStore("download finished ,", resUrl)
    end
  })
end
function DynamicResDownloader:IfResExsit(resName)
  return GameSetting:IsDownloadFileExsit(resName) and self:IsPngCrcCorrect(resName)
end
function DynamicResDownloader:CheckExtraCDNUrl()
  ext.http.request({
    AutoUpdate.gameGateway .. "servers/update_addr",
    param = {background = 1},
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      DebugOut("errstr = ", errstr)
      DebugOut("statusCode = ", statusCode)
      DebugOut("CheckExtraCDNUrl")
      if statusCode ~= 200 then
        DebugOut("check extra res getURL error ", statusCode)
      elseif content then
        if content.error and content.error ~= "" then
          DebugOut("check extra res getURL error", statusCode)
        else
          DynamicResDownloader.cndURL = content.url2
          Facebook.mImgUrl = content.fb
          self:CheckExtraFleetInfo(1, "v1/")
          self:CheckExtraFleetInfo(2, "v2/")
          self:CheckExtraText()
          self:CheckExtraAlertData()
          self:CheckExtraPngCrcList()
          self:CheckExtraArtifactConfig()
          return
        end
      end
    end
  })
end
function DynamicResDownloader:CheckExtraFleetInfo(curversion, versionStr)
  if not DynamicResDownloader.cndURL then
    return
  end
  local verVerUrl = DynamicResDownloader.cndURL .. "/extraFleetInfo/" .. versionStr .. "ver.txt"
  ext.http.request({
    verVerUrl,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        print(errstr)
        return
      end
      DebugOut("content = ", content)
      local _, _, serverVer, serverCRC = string.find(content, [[
(%d+)
(%d+)]])
      serverVer = tonumber(serverVer)
      local localVer = 0
      local localCRC = ext.crc32.crc32(versionStr .. "ExtraFleetInfo.tfl")
      if curversion == 1 then
        localVer = LocalExtraVerTable.v1fleetInfo
      else
        localVer = LocalExtraVerTable.v2fleetInfo
      end
      DebugOut("ExtraFleetInfo.tfl => serverVer, serverCRC", serverVer, serverCRC)
      DebugOut("ExtraFleetInfo.tfl => localVer, localCRC", localVer, localCRC)
      if serverVer > localVer or localCRC ~= serverCRC then
        DebugOut("begin download ExtraFleetInfo.tfl")
        local extFileUrl = DynamicResDownloader.cndURL .. "/extraFleetInfo/" .. versionStr .. "ExtraFleetInfo"
        if ext.IsArm64 and ext.IsArm64() then
          extFileUrl = extFileUrl .. "_64.tfl"
        else
          extFileUrl = extFileUrl .. ".tfl"
        end
        DebugOut("extFileUrl = ", extFileUrl)
        ext.http.requestDownload({
          extFileUrl,
          method = "GET",
          localpath = versionStr .. "ExtraFleetInfo.tfl",
          callback = function(statusCode, filename, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              print(errstr)
              return
            end
            if curversion == 1 then
              v1extra_herosbasic = nil
              v1extra_herosAbility = nil
            else
              v2extra_herosbasic = nil
              v2extra_herosAbility = nil
            end
            ext.dofile(versionStr .. "ExtraFleetInfo.tfl")
            ext.UpdateAutoUpdateFile(versionStr .. "ExtraFleetInfo.tfl", 0, 0, 0)
            if curversion == 1 then
              LocalExtraVerTable.v1fleetInfo = serverVer
            else
              LocalExtraVerTable.v2fleetInfo = serverVer
            end
            ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
          end
        })
      end
    end
  })
end
function DynamicResDownloader:CheckExtraArtifactConfig()
  if not DynamicResDownloader.cndURL then
    return
  end
  DebugOut("DynamicResDownloader:CheckExtraArtifactConfig")
  local verVerUrl = DynamicResDownloader.cndURL .. "/extraArtifactConfig/v1/ver.txt"
  ext.http.request({
    verVerUrl,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        print(errstr)
        return
      end
      local _, _, serverVer, serverCRC = string.find(content, [[
(%d+)
(%d+)]])
      serverVer = tonumber(serverVer)
      local localVer = LocalExtraVerTable.text
      local localCRC = ext.crc32.crc32("ExtraArtifactData.tfl")
      DebugOut("ExtraArtifactData.tfl => serverVer, serverCRC", serverVer, serverCRC)
      DebugOut("ExtraArtifactData.tfl => localVer, localCRC", localVer, localCRC)
      if serverVer > localVer or localCRC ~= serverCRC then
        DebugOut("begin download ExtraArtifactData.tfl")
        local extFileUrl = DynamicResDownloader.cndURL .. "/extraArtifactConfig/v1/ExtraArtifactData"
        if ext.IsArm64 and ext.IsArm64() then
          extFileUrl = extFileUrl .. "_64.tfl"
        else
          extFileUrl = extFileUrl .. ".tfl"
        end
        DebugOut("extFileUrl = ", extFileUrl)
        ext.http.requestDownload({
          extFileUrl,
          method = "GET",
          localpath = "ExtraArtifactData.tfl",
          callback = function(statusCode, filename, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              print(errstr)
              return
            end
            EXTRA_TEXT = nil
            ext.dofile("ExtraArtifactData.tfl")
            ext.UpdateAutoUpdateFile("ExtraArtifactData.tfl", 0, 0, 0)
            LocalExtraVerTable.text = serverVer
            ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
          end
        })
      end
    end
  })
end
function DynamicResDownloader:CheckExtraText()
  if not DynamicResDownloader.cndURL then
    return
  end
  local verVerUrl = DynamicResDownloader.cndURL .. "/text/ver.txt"
  ext.http.request({
    verVerUrl,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        print(errstr)
        return
      end
      local _, _, serverVer, serverCRC = string.find(content, [[
(%d+)
(%d+)]])
      serverVer = tonumber(serverVer)
      local localVer = LocalExtraVerTable.text
      local localCRC = ext.crc32.crc32("EXTRA_TEXT.tfl")
      DebugOut("EXTRA_TEXT.tfl => serverVer, serverCRC", serverVer, serverCRC)
      DebugOut("EXTRA_TEXT.tfl => localVer, localCRC", localVer, localCRC)
      if serverVer > localVer or localCRC ~= serverCRC then
        DebugOut("begin download EXTRA_TEXT.tfl")
        local extFileUrl = DynamicResDownloader.cndURL .. "/text/EXTRA_TEXT"
        if ext.IsArm64 and ext.IsArm64() then
          extFileUrl = extFileUrl .. "_64.tfl"
        else
          extFileUrl = extFileUrl .. ".tfl"
        end
        DebugOut("extFileUrl = ", extFileUrl)
        ext.http.requestDownload({
          extFileUrl,
          method = "GET",
          localpath = "EXTRA_TEXT.tfl",
          callback = function(statusCode, filename, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              print(errstr)
              return
            end
            EXTRA_TEXT = nil
            ext.dofile("EXTRA_TEXT.tfl")
            ext.UpdateAutoUpdateFile("EXTRA_TEXT.tfl", 0, 0, 0)
            LocalExtraVerTable.text = serverVer
            ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
          end
        })
      end
    end
  })
end
function DynamicResDownloader:CheckExtraAlertData()
  local verVerUrl = DynamicResDownloader.cndURL .. "/netProtocal/ver.txt"
  ext.http.request({
    verVerUrl,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        print(errstr)
        return
      end
      local _, _, serverVer, serverCRC = string.find(content, [[
(%d+)
(%d+)]])
      serverVer = tonumber(serverVer)
      local localVer = LocalExtraVerTable.netProtocal
      local localCRC = ext.crc32.crc32("AlertDataListExtra.tfl")
      if serverVer > localVer or localCRC ~= serverCRC then
        local extFileUrl = DynamicResDownloader.cndURL .. "/netProtocal/AlertDataListExtra"
        if ext.IsArm64 and ext.IsArm64() then
          extFileUrl = extFileUrl .. "_64.tfl"
        else
          extFileUrl = extFileUrl .. ".tfl"
        end
        DebugOut("extFileUrl = ", extFileUrl)
        ext.http.requestDownload({
          extFileUrl,
          method = "GET",
          localpath = "AlertDataListExtra.tfl",
          callback = function(statusCode, filename, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              print(errstr)
              return
            end
            AlertDataListExtra = nil
            ext.dofile("AlertDataListExtra.tfl")
            ext.UpdateAutoUpdateFile("AlertDataListExtra.tfl", 0, 0, 0)
            LocalExtraVerTable.netProtocal = serverVer
            ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
          end
        })
      end
    end
  })
end
function DynamicResDownloader:CheckExtraPngCrcList()
  local verVerUrl = DynamicResDownloader.cndURL .. "/images/ver.txt"
  ext.http.request({
    verVerUrl,
    mode = "notencrypt",
    callback = function(statusCode, content, errstr)
      if errstr ~= nil or statusCode ~= 200 then
        print(errstr)
        return
      end
      local _, _, serverVer, serverCRC = string.find(content, [[
(%d+)
(%d+)]])
      serverVer = tonumber(serverVer)
      local localVer = LocalExtraVerTable.images or 0
      local localCRC = ext.crc32.crc32("pcl.tfl")
      if serverVer > localVer or localCRC ~= serverCRC then
        local extFileUrl = DynamicResDownloader.cndURL .. "/images/pcl"
        if ext.IsArm64 and ext.IsArm64() then
          extFileUrl = extFileUrl .. "_64.tfl"
        else
          extFileUrl = extFileUrl .. ".tfl"
        end
        DebugOut("extFileUrl = ", extFileUrl)
        ext.http.requestDownload({
          extFileUrl,
          method = "GET",
          localpath = "pcl.tfl",
          callback = function(statusCode, filename, errstr)
            if errstr ~= nil or statusCode ~= 200 then
              print(errstr)
              return
            end
            pngcrcchecklist = nil
            ext.dofile("pcl.tfl")
            ext.UpdateAutoUpdateFile("pcl.tfl", 0, 0, 0)
            LocalExtraVerTable.images = serverVer
            ext.SaveGameData("levl.tfl", GameUtils:formatSaveString(LocalExtraVerTable, "LocalExtraVerTable"))
          end
        })
      end
    end
  })
end
function DynamicResDownloader:IsPngCrcCorrect(fileName)
  if string.find(fileName, ".png") ~= nil and pngcrcchecklist then
    for k, v in pairs(pngcrcchecklist) do
      if string.find(fileName, k) ~= nil then
        local filecrc = ext.crc32.crc32(fileName)
        return filecrc == v.crc
      end
    end
  end
  return true
end
