local vip = GameData.vip_exp.vip
vip[0] = {level = 0, exp = 0}
vip[1] = {level = 1, exp = 600}
vip[2] = {level = 2, exp = 1250}
vip[3] = {level = 3, exp = 2750}
vip[4] = {level = 4, exp = 7000}
vip[5] = {level = 5, exp = 15000}
vip[6] = {level = 6, exp = 25000}
vip[7] = {level = 7, exp = 75000}
vip[8] = {level = 8, exp = 150000}
vip[9] = {level = 9, exp = 300000}
vip[10] = {level = 10, exp = 750000}
vip[11] = {level = 11, exp = 1500000}
vip[12] = {level = 12, exp = 4500000}
