local GameUIHalfPortrait = LuaObjectManager:GetLuaObject("GameUIHalfPortrait")
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local ResFiles = {
  effect = {
    [1] = {
      key = "LAZY_LOAD_DOWN_EFFECT_huoxing_%d.png",
      min = 0,
      max = 41
    },
    [2] = {
      key = "LAZY_LOAD_DOWN_EFFECT_banxue_%d.png",
      min = 0,
      max = 29
    }
  },
  bg = {
    [1] = {
      key = "LAZY_LOAD_DOWN_EFFECT_banhuo1_%d.png",
      min = 0,
      max = 29
    },
    [2] = {
      key = "LAZY_LOAD_DOWN_EFFECT_bingbeijin_%d.png",
      min = 0,
      max = 29
    }
  }
}
function GameUIHalfPortrait:PlayEffect(isLeft, head, ship, banner, avatar, shipName)
  local shipNS = LuaUtils:string_split(shipName, "_")
  DebugOut("GameUIHalfPortrait:PlayEffect:" .. "," .. tostring(isLeft) .. "," .. head .. "," .. ship .. "," .. banner .. "," .. avatar .. "," .. shipName)
  GameUtils:printByAndroid("GameUIHalfPortrait:PlayEffect:" .. "," .. tostring(isLeft) .. "," .. head .. "," .. ship .. "," .. banner .. "," .. avatar .. "," .. shipName)
  self:GetFlashObject():InvokeASCallback("_root", "show", isLeft, head, ship, banner, avatar, shipNS[#shipNS])
end
function GameUIHalfPortrait:PreLoadRes(img)
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self:GetFlashObject():OpenTextureForLua(img)
end
function GameUIHalfPortrait:OnFSCommand(cmd, arg)
  if cmd == "on_effect_over" then
    self:GetFlashObject():InvokeASCallback("_root", "stop_effect")
    GameStateBattlePlay:OnEffectEnd()
  end
end
function GameUIHalfPortrait:OnAddToGameState(state)
  self:LoadFlashObject()
end
function GameUIHalfPortrait:OnEraseFromGameState()
  self:UnloadFlashObject()
end
