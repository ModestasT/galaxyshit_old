local matrix = require("Matrix")
local Camera = require("Camera")
local mtx, m1, m2
mtx = matrix({
  {
    3,
    5,
    1
  },
  {
    2,
    4,
    5
  },
  {
    1,
    2,
    2
  }
})
local mtxinv = matrix({
  {
    2,
    8,
    -21
  },
  {
    -1,
    -5,
    13
  },
  {
    0,
    1,
    -2
  }
})
assert(mtx ^ -1 == mtxinv)
mtx = matrix({
  {
    1,
    0,
    2
  },
  {
    4,
    1,
    1
  },
  {
    3,
    2,
    -7
  }
})
local mtxinv = matrix({
  {
    -9,
    4,
    -2
  },
  {
    31,
    -13,
    7
  },
  {
    5,
    -2,
    1
  }
})
assert(mtx ^ -1 == mtxinv)
m1 = matrix({
  {
    8,
    4,
    1
  },
  {
    6,
    8,
    3
  }
})
m2 = matrix({
  {3, 1},
  {2, 5},
  {7, 4}
})
assert(m1 * m2 == matrix({
  {39, 32},
  {55, 58}
}))
print(tostring(m1 * m2))
m1 = matrix({
  {
    8,
    4,
    1,
    2
  },
  {
    6,
    8,
    3,
    4
  },
  {
    2,
    1,
    2,
    3
  },
  {
    1,
    3,
    6,
    7
  }
})
m2 = matrix({
  {1},
  {4},
  {2},
  {7}
})
print(tostring(m1 * m2))
print(m1[1][2])
print(m2[2][1], matrix.size(m2))
local function testCamera(...)
  print("----------testCamera")
  Camera.SetParam({
    0,
    0,
    0
  }, {
    0,
    0,
    1
  }, {1024, 768})
  print("view")
  print(tostring(Camera.worldToCameraMatrix))
  print("proj")
  print(tostring(Camera.projectionMatrix))
  print("----------testCamera")
  local woldpos = {
    1.2,
    -2.3,
    5
  }
  local newpos = Camera.WorldToScreenPoint(woldpos)
  print("out pos", newpos[1], newpos[2], newpos[3])
end
testCamera()
