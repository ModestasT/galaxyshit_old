local GameObjectShakeScreen = LuaObjectManager:GetLuaObject("GameObjectShakeScreen")
function GameObjectShakeScreen:OnAddToGameState()
  self.m_isShaking = false
end
function GameObjectShakeScreen:ResetShake()
  if self:GetFlashObject() then
    self:GetFlashObject():SetShake(0, 0, 0)
  else
    assert(false)
  end
end
function GameObjectShakeScreen:StartShaking(idx)
  self.m_isShaking = true
  if idx == nil then
    idx = 1
  end
  self:GetFlashObject():InvokeASCallback("_root", "StartShake", idx)
end
function GameObjectShakeScreen:Update(dt)
  if self:GetFlashObject() then
    self:GetFlashObject():Update(dt)
    if self.m_isShaking then
      self:GetFlashObject():InvokeASCallback("_root", "OnUpdate", dt)
    end
  end
end
function GameObjectShakeScreen:OnFSCommand(cmd, args)
  if cmd == "StopShake" then
    self:ResetShake()
    self.m_isShaking = false
  elseif cmd == "ShakeScreen" then
    local argParam = LuaUtils:string_split(args, "^")
    local x = tonumber(argParam[1]) / ext.SCREEN_WIDTH
    local y = tonumber(argParam[2]) / ext.SCREEN_HEIGHT
    self:GetFlashObject():SetShake(1, x, y)
  end
end
