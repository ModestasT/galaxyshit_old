local ChapterInfos = GameData.chapterdata_act1.ChapterInfos
ChapterInfos[1] = {
  ChaperID = 1,
  ICON = "icon1",
  MapIndex = 1,
  BossPos = 2,
  EntroStory = {1104, 1135},
  BEFORE_FINISH = {1125, 1126},
  AFTER_FINISH = {}
}
ChapterInfos[2] = {
  ChaperID = 2,
  ICON = "icon2",
  MapIndex = 2,
  BossPos = 10,
  EntroStory = {
    1206,
    1207,
    1208
  },
  BEFORE_FINISH = {
    1227,
    1228,
    1231,
    1232,
    1233,
    1234
  },
  AFTER_FINISH = {}
}
ChapterInfos[3] = {
  ChaperID = 3,
  ICON = "icon30",
  MapIndex = 3,
  BossPos = 16,
  EntroStory = {1301},
  BEFORE_FINISH = {
    1312,
    1313,
    1314,
    1315
  },
  AFTER_FINISH = {}
}
ChapterInfos[4] = {
  ChaperID = 4,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {1401, 1402},
  BEFORE_FINISH = {1414},
  AFTER_FINISH = {}
}
ChapterInfos[5] = {
  ChaperID = 5,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {1505, 1506},
  BEFORE_FINISH = {1515, 1601},
  AFTER_FINISH = {}
}
ChapterInfos[6] = {
  ChaperID = 6,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {1602, 1603},
  BEFORE_FINISH = {
    1617,
    1618,
    1619,
    1620,
    1621,
    1622,
    1623,
    1624,
    1625
  },
  AFTER_FINISH = {}
}
ChapterInfos[7] = {
  ChaperID = 7,
  ICON = "icon6",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {
    1701,
    1702,
    1703
  },
  BEFORE_FINISH = {
    1714,
    1715,
    1716,
    1717
  },
  AFTER_FINISH = {}
}
ChapterInfos[8] = {
  ChaperID = 8,
  ICON = "icon400",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {1801},
  BEFORE_FINISH = {
    1810,
    1811,
    1812,
    1813,
    1814,
    1815,
    1816
  },
  AFTER_FINISH = {}
}
ChapterInfos[9] = {
  ChaperID = 9,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  BEFORE_FINISH = {},
  AFTER_FINISH = {}
}
ChapterInfos[10] = {
  ChaperID = 10,
  ICON = "icon5",
  MapIndex = 1,
  BossPos = 18,
  EntroStory = {},
  BEFORE_FINISH = {},
  AFTER_FINISH = {}
}
