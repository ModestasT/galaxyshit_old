local GameUIActivity = LuaObjectManager:GetLuaObject("GameUIActivity")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBarLeft = LuaObjectManager:GetLuaObject("GameUIBarLeft")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameCommonBackground = LuaObjectManager:GetLuaObject("GameCommonBackground")
local GameUIRecruitMain = LuaObjectManager:GetLuaObject("GameUIRecruitMain")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameObjectMainPlanet = LuaObjectManager:GetLuaObject("GameObjectMainPlanet")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameItemBag = LuaObjectManager:GetLuaObject("GameItemBag")
local GameVipDetailInfoPanel = LuaObjectManager:GetLuaObject("GameVipDetailInfoPanel")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
local ifNoNeedToSlide = false
GameUIActivity.SignCanNotReceive = 0
GameUIActivity.SignCanReceive = 1
GameUIActivity.SignReceived = 2
GameUIActivity.ACTIVITY_STS_NO = 1
GameUIActivity.ACTIVITY_STS_RUNNING = 2
GameUIActivity.ACTIVITY_STS_NOT_BEGIN = 3
GameUIActivity.ACTIVITY_STS_END = 4
GameUIActivity.useFakeData = false
GameUIActivity.isSortActivity = false
local hasShowReceiveBtn = false
GameUIActivity.isShareFacebook = true
GameUIActivity.faceBookFriendList = {}
GameUIActivity.isShowFacebookLogin = false
GameUIActivity.isShowFacebookDialog = true
GameUIActivity.isRefreshSign = false
function GameUIActivity:OnInitGame()
  GameUIActivity.currentLab = ""
  GameUIActivity.showContent = false
end
local CommanderRanks = {
  [1] = "rank_E",
  [2] = "rank_D",
  [3] = "rank_C",
  [4] = "rank_B",
  [5] = "rank_A",
  [6] = "rank_S",
  [7] = "rank_SS"
}
local LoginFundPurchaseState = {UNPURCHASE = 1, PURCHASED = 2}
function GameUIActivity:OnAddToGameState(...)
  DebugOut("GameUIActivity:OnAddToGameState")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  self.isNeedUpdateRemindNews = true
  self:_RealUpdateAllActivityRemind()
  GameUIActivity.mWaitForSendFBGift = false
  GameUIActivity:SetSignTypeText()
  self:GetFlashObject():InvokeASCallback("_root", "hideSomeMenu")
  DebugOut("GameUIActivity:OnAddToGameState:register vip")
  NetMessageMgr:RegisterMsgHandler(NetAPIList.vip_sign_item_use_award_ntf.Code, GameUIActivity.VipSignAwardNtf)
  NetMessageMgr:RegisterMsgHandler(NetAPIList.pay_sign_board_ntf.Code, GameUIActivity.BoardNtf)
  self:SetVipSignRollTextInfo()
end
function GameUIActivity:SetVipSignRollTextInfo()
  if not self.currentLab == "vipSign" then
    return
  end
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    local message = ""
    if GameUIActivity.boardsMessage then
      message = GameUIActivity.boardsMessage
    end
    DebugOut("SetVipSignRollTextInfo:", message)
    flashObj:InvokeASCallback("_root", "setVIPSignRollTextInfo", message)
  end
end
function GameUIActivity:hideAllNewsRemind()
  self:setSignRemindStatus(false)
  self:setVipSignRemindStatus(false)
  self:setOpenFundOuterRemindStatus(false)
  self:setOpenFundInnerRemindStatus(false)
  self:setLoginFundRemindStates(false)
  self:setAllWelfaceInnerRemindStates(false)
  self:setLevelLootRemindStatus(false)
  self:setOnlineRemindStatus(false)
  self:setNewSignNewsStatus(false)
end
function GameUIActivity.BoardNtf(content)
  DebugOut("BoardNtf:")
  DebugTable(content)
  if not content then
    return
  end
  local message = ""
  for k, v in pairs(content.boards) do
    message = message .. v .. "    "
  end
  GameUIActivity.boardsMessage = message
  DebugOut(GameUIActivity.boardsMessage)
  GameUIActivity:SetVipSignRollTextInfo()
end
function GameUIActivity.VipSignAwardNtf(content)
  DebugOut("VipSignAwardNtf:")
  DebugTable(content)
  if not content then
    return
  end
  for key, v in pairs(content.award_list) do
    if v.shared_rewards and #v.shared_rewards > 0 then
      local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
      if v.item_type == "fleet" then
        GameUIPrestigeRankUp:ShowGetCommander(v, true)
        return
      else
        GameUIPrestigeRankUp:ShowGetItem(v, true)
        return
      end
    end
  end
  for key, v in pairs(content.award_list) do
    local nameText = GameHelper:GetAwardText(v.item_type, v.number, v.no)
    local tip_text = GameLoader:GetGameText("LC_MENU_VIP_ITEM_REWARDS_ALERT")
    tip_text = string.format(tip_text, nameText)
    GameTip:Show(tip_text)
  end
end
function GameUIActivity:OnEraseFromGameState()
  DebugOut("GameUIActivity:OnEraseFromGameState")
  GameUIActivity.currentLab = ""
  self:UnloadFlashObject()
  GameUIActivity.signlist = nil
  GameUIActivity.vipSignList = nil
  GameUIActivity.vipList = nil
  GameUIActivity.vipKrypton = nil
  self.receiveVipLevel = nil
  GameUIActivity.levelList = nil
  GameUIActivity.levelKrypton = nil
  GameUIActivity.receivePlayerLevel = nil
  GameUIActivity.currentLabType2Name = nil
  GameUIActivity.currentActivityContentTitle = nil
  GameUIActivity.currentWebSite = nil
  GameUIActivity.OpenFundData = nil
  GameUIActivity.AllBenefitData = nil
  GameUIActivity.isBuyFlag = 0
  GameUIActivity.isRefreshSign = false
  GameUIActivity.isShareFacebook = true
  GameUIActivity.isNeedUpdateRemindNews = false
  GameUIActivity.newSignList = nil
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivityNew) then
    GameStateMainPlanet.Cover = GameUIActivityNew
  end
end
function GameUIActivity:ShowNews(label)
  DebugActivity("GameUIActivity:ShowNews self.currentLab: ", self.currentLab, " label: ", label)
  GameUIActivity:setTitleText()
  GameUIActivity:CheckTabType()
  if self.currentLab == label then
    return
  end
  if label == "sign" then
    if GameUIActivity.signlist then
      GameUIActivity:AddToGameState()
      table.sort(GameUIActivity.signlist.item, GameUIActivity.sortSignItem)
      GameUIActivity:ShowSign()
    else
      self:RequestSign()
    end
    GameUIActivity.currentLab = "sign"
  elseif label == "level" then
    GameUIActivity:AddToGameState()
    GameUIActivity:InitLevelList()
    if GameUIActivity.levelList then
      GameUIActivity:showLevelList()
    else
      self:RequestLevel(true)
    end
    GameUIActivity.currentLab = "level"
  elseif label == "online" then
    GameUIActivity:ShowOnline()
  elseif label == "vipSign" then
    if GameUIActivity.vipSignList then
      GameUIActivity:AddToGameState()
      table.sort(GameUIActivity.vipSignList.items, function(v1, v2)
        return v1.day < v2.day
      end)
      GameUIActivity:ShowVipSign()
    else
      self:RequestVipSign()
    end
    GameUIActivity.currentLab = "vipSign"
  elseif label == "open" then
    GameUIActivity.currentLab = "open"
    GameUIActivity:ShowOpen()
  elseif label == "newSign" then
    GameUIActivity.currentLab = "newSign"
    if GameUIActivity.newSignList then
      GameUIActivity:AddToGameState()
      table.sort(GameUIActivity.newSignList.awards, GameUIActivity.sortNewSignItem)
      GameUIActivity:ShowNewSign()
    else
      self:RequestNewSign()
    end
  end
end
function GameUIActivity:ShowOnline()
  GameUIActivity.currentLab = "online"
  GameUIActivity:InitOnlineList()
  if GameUIActivity.onlineList then
    GameUIActivity:AddToGameState()
    GameUIActivity.showOnlineList()
  end
end
function GameUIActivity:ShowSigned()
  if GameUIActivity.signlist then
    GameUIActivity:AddToGameState()
    GameUIActivity.currentLab = "sign"
    table.sort(GameUIActivity.signlist.item, GameUIActivity.sortSignItem)
    GameUIActivity:ShowSign()
  else
    self:RequestSign()
  end
end
function GameUIActivity:ShowOpen()
  GameUIActivity:InitOpenList()
  GameUIActivity:AddToGameState()
  GameUIActivity:ShowOpenfundAndAllbenefit(1)
end
function GameUIActivity:setTitleText()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local signText = GameLoader:GetGameText("LC_MENU_TITLE_SIGN")
  local activityText = GameLoader:GetGameText("LC_MENU_CHAR_ACTIVITY")
  local newsText = GameLoader:GetGameText("LC_MENU_CHAR_NEWS")
  local vip = GameLoader:GetGameText("LC_MENU_VIP_REWARDS_TAP")
  local level = GameLoader:GetGameText("LC_MENU_LEVEL_REWARDS_TAP")
  local active = GameLoader:GetGameText("LC_MENU_ACTIVE_REWARDS_TAP")
  local purcharge = GameLoader:GetGameText("LC_MENU_VIP_REWARDS_PURCHASE_BUTTON")
  local vipSign = GameLoader:GetGameText("LC_MENU_LUXURY_REGISTRATION_BUTTON")
  local open = GameLoader:GetGameText("LC_MENU_WELFARE_SERVER_FUND_BUTTON")
  local newSign = GameLoader:GetGameText("LC_MENU_LOGIN_DAY_TITLE")
  self:GetFlashObject():InvokeASCallback("_root", "initTabText", signText, activityText, newsText, level, active, purcharge, vipSign, open, newSign)
end
function GameUIActivity:AddToGameState()
  DebugActivity("--\230\137\147\229\141\176AddToGameState--")
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) then
    GameStateManager:GetCurrentGameState():AddObject(GameUIActivity)
    GameStateManager:GetCurrentGameState():AddObject(GameCommonBackground)
    if not GameUIActivity:GetFlashObject() then
      GameUIActivity:LoadFlashObject()
    end
    self:GetFlashObject():InvokeASCallback("_root", "moveIn")
  end
  DebugActivity("--\230\137\147\229\141\176AddToGameState-- end")
  self:RequestAllDataIfNotInit()
end
function GameUIActivity:RequestAllDataIfNotInit()
  if nil == self.onlineList then
  end
  if nil == self.newSignList then
    NetMessageMgr:SendMsg(NetAPIList.enter_login_sign_req.Code, nil, GameUIActivity.requestAllDataCallback, true)
  end
  if nil == self.signlist then
    NetMessageMgr:SendMsg(NetAPIList.sign_activity_req.Code, nil, GameUIActivity.requestAllDataCallback, true)
  end
  if nil == self.levelList then
    NetMessageMgr:SendMsg(NetAPIList.level_loot_req.Code, nil, GameUIActivity.requestAllDataCallback, true)
  end
  DebugOut("RequestAllDataIfNotInit:", self.vipSignList)
  if nil == self.vipSignList then
    NetMessageMgr:SendMsg(NetAPIList.pay_sign_req.Code, nil, GameUIActivity.requestAllDataCallback, true)
  end
  if nil == self.OpenFundData then
    local param = {num = 0, flag = 0}
    NetMessageMgr:SendMsg(NetAPIList.open_server_fund_req.Code, param, GameUIActivity.requestAllDataCallback, true)
  end
  if nil == self.AllBenefitData then
    local param = {num = 0, flag = 0}
    NetMessageMgr:SendMsg(NetAPIList.all_welfare_req.Code, param, GameUIActivity.requestAllDataCallback, true, nil)
  end
  if nil == self.LoginFundData then
    NetMessageMgr:SendMsg(NetAPIList.login_fund_req.Code, nil, GameUIActivity.requestAllDataCallback, true, nil)
  end
end
function GameUIActivity.requestAllDataCallback(msgType, content)
  if msgType == NetAPIList.sign_activity_ack.Code then
    GameUIActivity.signlist = content
    table.sort(GameUIActivity.signlist.item, function(v1, v2)
      return v1.days < v2.days
    end)
    return true
  elseif msgType == NetAPIList.pay_sign_ack.Code then
    DebugOut("requestAllDataCallback vipSignList")
    GameUIActivity.vipSignList = content
    table.sort(GameUIActivity.vipSignList.items, function(v1, v2)
      return v1.day < v2.day
    end)
    return true
  elseif msgType == NetAPIList.level_loot_ack.Code then
    GameUIActivity.levelList = content.levels
    return true
  elseif msgType == NetAPIList.open_server_fund_ack.Code then
    GameUIActivity.OpenFundData = content
    table.sort(GameUIActivity.OpenFundData.info, GameUIActivity.SortOpenFunDataOfLevel)
    return true
  elseif msgType == NetAPIList.all_welfare_ack.Code then
    GameUIActivity.AllBenefitData = content
    table.sort(GameUIActivity.AllBenefitData, GameUIActivity.SortAllBenefitDataOfNum)
    return true
  elseif msgType == NetAPIList.enter_login_sign_ack.Code then
    GameUIActivity.newSignList = content
    table.sort(GameUIActivity.newSignList.awards, GameUIActivity.sortNewSignItem)
    DebugOutPutTable(content, "GameUIActivity login_sign_ack")
    GameUIActivity:CheckTabType()
    return true
  elseif msgType == NetAPIList.login_fund_ack.Code then
    GameUIActivity.LoginFundData = content
    GameUIActivity:SortLoginFundData()
    DebugOut("login_fund_ack")
    DebugOut(not GameUIActivity.LoginFundData.bln_buy)
    DebugOut(not GameUIActivity:GetLoginFundPrice())
    if GameUIActivity.LoginFundData.bln_buy ~= 1 and GameUIActivity:GetLoginFundPrice() == nil then
      GameVip:RequestProductIdentifierList()
      GameVip:ReqGiftData()
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and (content.api == NetAPIList.sign_activity_req.Code or content.api == NetAPIList.pay_sign_req.Code or content.api == NetAPIList.level_loot_req.Code or content.api == NetAPIList.open_server_fund_req.Code or content.api == NetAPIList.all_welfare_req.Code or content.api == NetAPIList.enter_login_sign_ack.Code or content.api == NetAPIList.login_fund_req.Code) then
    return true
  end
  return false
end
function GameUIActivity:SortLoginFundData()
  table.sort(self.LoginFundData.reward_list, function(v1, v2)
    return v1.status < v2.status or v1.status == v2.status and v1.days < v2.days
  end)
end
function GameUIActivity:GetLoginFundPrice()
  local ret = GameVip.GetPriceByProductID(GameVip:GetLoginFundProductId())
  DebugOut(ret)
  DebugOut("GetLoginFundPrice " .. debug.traceback())
  if ret == "err" or not ret then
    DebugOut("GetLoginFundPrice false")
    return nil
  end
  return ret
end
GameUIActivity.remind_update_time_need = 1000
GameUIActivity.remindTimeCounter = 0
GameUIActivity.isNeedUpdateRemindNews = false
function GameUIActivity:UpdateAllActivityRemind(dt)
  if not self.isNeedUpdateRemindNews then
    return
  end
  self.remindTimeCounter = self.remindTimeCounter + dt
  if self.remindTimeCounter < self.remind_update_time_need then
    return
  end
  self.remindTimeCounter = 0
  self:_RealUpdateAllActivityRemind()
end
function GameUIActivity:CheckTabType()
  DebugOutPutTable(GameUIActivity.newSignList, "GameUIActivity:CheckTabType")
  local tabType = 1
  if GameUIActivity.newSignList and #GameUIActivity.newSignList.awards > 0 then
    tabType = 2
  end
  if GameUIActivity:GetFlashObject() then
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "SetMenuTabType", tabType)
  end
end
function GameUIActivity:_RealUpdateAllActivityRemind()
  local isRemindSign = false
  local isRemindVipSign = false
  local isRemindOuterOpenFund = false
  local isRemindInnerOpenFund = false
  local isRemindLoginFund = false
  local isRemindInnerAllWelface = false
  local isRemindLevelLoot = false
  local isRemindOnline = false
  local isRemindNewSign = false
  if self:isSignHaveReadyItem() then
    isRemindSign = true
  end
  if self:isVipSignHaveReadyItem() then
    isRemindVipSign = true
  end
  if self:isOpenFundOrAllWelfaceHaveReadyItem() then
    isRemindOuterOpenFund = true
  end
  if self:isOpenFundHaveReadyItem() then
    isRemindInnerOpenFund = true
  end
  if self:isLoginFundHaveReadyItem() then
    isRemindLoginFund = true
  end
  if self:isAllWelfaceHaveReadyItem() then
    isRemindInnerAllWelface = true
  end
  if self:isLevelLootHaveReadyItem() then
    isRemindLevelLoot = true
  end
  if self:isOnlineHaveReadyItem() then
    isRemindOnline = true
  end
  if self:isNewSignHaveReadyItem() then
    isRemindNewSign = true
  end
  self:setSignRemindStatus(isRemindSign)
  self:setVipSignRemindStatus(isRemindVipSign)
  self:setOpenFundOuterRemindStatus(isRemindOuterOpenFund)
  self:setOpenFundInnerRemindStatus(isRemindInnerOpenFund)
  self:setLoginFundRemindStates(isRemindLoginFund)
  self:setAllWelfaceInnerRemindStates(isRemindInnerAllWelface)
  self:setLevelLootRemindStatus(isRemindLevelLoot)
  self:setOnlineRemindStatus(isRemindOnline)
  self:setNewSignNewsStatus(isRemindNewSign)
end
function GameUIActivity:isSignUIShowNow()
  return "sign" == self.currentLab
end
function GameUIActivity:isVipSignUIShowNow()
  return "vipSign" == self.currentLab
end
function GameUIActivity:isOpenFundOrAllWelfaceShowNow()
  return "open" == self.currentLab
end
function GameUIActivity:isInnerOpenFundUIShowNow()
  return self:isOpenFundOrAllWelfaceShowNow() and 1 == GameUIActivity.currentOFABtab
end
function GameUIActivity:isInnerAllWelfaceUIShowNow()
  return self:isOpenFundOrAllWelfaceShowNow() and 2 == GameUIActivity.currentOFABtab
end
function GameUIActivity:isLevelLootUIShowNow()
  return "level" == self.currentLab
end
function GameUIActivity:isOnlineUIShowNow()
  return "online" == self.currentLab
end
function GameUIActivity:isNewSignUIShowNow()
  return "newSign" == self.currentLab
end
function GameUIActivity:isTheNewSignItemReady(signItem)
  return 1 == signItem.award_status
end
function GameUIActivity:isNewSignHaveReadyItem()
  if nil == self.newSignList or #self.newSignList.awards == 0 then
    return false
  end
  local signItems = self.newSignList.awards
  for i, signItem in ipairs(signItems) do
    if self:isTheNewSignItemReady(signItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheSignItemReady(signItem)
  return 1 == signItem.status
end
function GameUIActivity:isSignHaveReadyItem()
  if nil == self.signlist then
    return false
  end
  local signItems = self.signlist.item
  for i, signItem in ipairs(signItems) do
    if self:isTheSignItemReady(signItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheVipSignItemReady(vipSignItem)
  return 1 == vipSignItem.status
end
function GameUIActivity:isVipSignHaveReadyItem()
  if nil == self.vipSignList then
    return false
  end
  local vipSignItems = self.vipSignList.items
  for i, vipSignItem in ipairs(vipSignItems) do
    if self:isTheVipSignItemReady(vipSignItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheLevelLootReady(levelLootItem)
  return 1 == levelLootItem.status
end
function GameUIActivity:isLevelLootHaveReadyItem()
  if nil == self.levelList then
    return false
  end
  local levelLootItems = self.levelList
  for i, levelLootItem in ipairs(levelLootItems) do
    if self:isTheLevelLootReady(levelLootItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheOnlineItemReady(onlineItem)
  return 1 == onlineItem.mark
end
function GameUIActivity:isOnlineHaveReadyItem()
  if nil == self.onlineList then
    return false
  end
  local activeItems = self.onlineList
  for i, activeItem in ipairs(activeItems) do
    if self:isTheOnlineItemReady(activeItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheOpenFundItemReady(fundItem)
  return 3 == fundItem.flag
end
function GameUIActivity:isLoginFundHaveReadyItem()
  if nil == self.LoginFundData then
    return false
  end
  local fundItems = self.LoginFundData.reward_list
  for i, fundItem in ipairs(fundItems) do
    if fundItem.status == 1 then
      return true
    end
  end
  return false
end
function GameUIActivity:isOpenFundHaveReadyItem()
  if nil == self.OpenFundData then
    return false
  end
  local fundItems = self.OpenFundData.info
  for i, fundItem in ipairs(fundItems) do
    if self:isTheOpenFundItemReady(fundItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isTheAllWelfaceItemReady(benifitItem)
  return 3 == benifitItem.flag
end
function GameUIActivity:isAllWelfaceHaveReadyItem()
  if nil == self.AllBenefitData then
    return false
  end
  local benifitItems = self.AllBenefitData.info
  for i, benifitItem in ipairs(benifitItems) do
    if self:isTheAllWelfaceItemReady(benifitItem) then
      return true
    end
  end
  return false
end
function GameUIActivity:isOpenFundOrAllWelfaceHaveReadyItem()
  return self:isAllWelfaceHaveReadyItem() or self:isOpenFundHaveReadyItem() or self:isLoginFundHaveReadyItem()
end
local boolToNumber = function(is)
  if is then
    return 1
  end
  return 0
end
function GameUIActivity:setNewSignNewsStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setNewSignNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setSignRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setSignNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setVipSignRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setVipSignNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setLevelLootRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setLevelLootNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setOpenFundOuterRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setOpenFundOuterNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setOpenFundInnerRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setOpenFundInnerNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setAllWelfaceInnerRemindStates(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setBenifiteInnerNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setLoginFundRemindStates(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setLoginFundNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:setOnlineRemindStatus(isShow)
  self:GetFlashObject():InvokeASCallback("_root", "setOnlineNewsStatus", boolToNumber(isShow))
end
function GameUIActivity:RequestSign()
  DebugOut("GameUIActivity:RequestSign")
  NetMessageMgr:SendMsg(NetAPIList.sign_activity_req.Code, nil, GameUIActivity.RequestSignCallbak, true)
end
function GameUIActivity:RequestNewSign()
  DebugOut("GameUIActivity:RequestSign")
  NetMessageMgr:SendMsg(NetAPIList.enter_login_sign_req.Code, nil, GameUIActivity.RequestNewSignCallbak, true)
end
function GameUIActivity:RequestVipSign()
  DebugOut("GameUIActivity:RequestVipSign")
  NetMessageMgr:SendMsg(NetAPIList.pay_sign_req.Code, nil, GameUIActivity.RequestVipSignCallback, true)
end
function GameUIActivity:RequestOnline()
end
function GameUIActivity:RequestLevel(isForceRequest)
  if isForceRequest or not GameUIActivity.levelList or not GameUIActivity.levelKrypton then
    NetMessageMgr:SendMsg(NetAPIList.level_loot_req.Code, nil, GameUIActivity.RequestLevelCallback, false, nil)
  end
end
function GameUIActivity:RequestVip(isForceRequest)
  if isForceRequest or not GameUIActivity.vipList or not GameUIActivity.vipKrypton then
    NetMessageMgr:SendMsg(NetAPIList.vip_loot_req.Code, nil, GameUIActivity.RequestVipCallback, false, nil)
  end
end
function GameUIActivity.RequestSignCallbak(msgType, content)
  if msgType == NetAPIList.sign_activity_ack.Code then
    if GameUIBarLeft.isFirstShow and (not content.item or content.status ~= GameUIActivity.ACTIVITY_STS_RUNNING) then
      GameUIBarLeft.isFirstShow = nil
      return true
    end
    GameUIActivity.currentLab = "sign"
    GameUIActivity.signlist = content
    DebugOutPutTable(content, "RequestSignCallbak")
    GameUIActivity:AddToGameState()
    table.sort(GameUIActivity.signlist.item, function(v1, v2)
      return v1.days < v2.days
    end)
    ifNoNeedToSlide = false
    GameUIActivity:ShowSign()
    GameUIActivity:SetSignTypeText()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.sign_activity_req.Code then
    GameUIBarLeft.isFirstShow = nil
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  end
  return false
end
function GameUIActivity.RequestNewSignCallbak(msgType, content)
  if msgType == NetAPIList.enter_login_sign_ack.Code then
    GameUIActivity:AddToGameState()
    GameUIActivity.currentLab = "newSign"
    GameUIActivity.newSignList = content
    GameUIActivity:CheckTabType()
    table.sort(GameUIActivity.newSignList.awards, GameUIActivity.sortNewSignItem)
    if GameUIActivity.newSignList and #GameUIActivity.newSignList.awards > 0 then
      GameUIActivity:ShowNewSign()
    else
      GameUIActivity:ShowNews("sign")
    end
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_login_sign_req.Code then
    GameUIBarLeft.isFirstShow = nil
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  end
  return false
end
function GameUIActivity.RequestVipSignCallback(msgType, content)
  DebugOut("RequestVipSignCallback === 1")
  if msgType == NetAPIList.pay_sign_ack.Code then
    if GameUIBarLeft.isFirstShow and (not content.item or content.status ~= GameUIActivity.ACTIVITY_STS_RUNNING) then
      DebugOut("RequestVipSignCallback === 2")
      GameUIBarLeft.isFirstShow = nil
      return true
    end
    GameUIActivity:AddToGameState()
    GameUIActivity.currentLab = "vipSign"
    DebugOut("RequestVipSignCallback === 3")
    GameUIActivity.vipSignList = content
    table.sort(GameUIActivity.vipSignList.items, function(v1, v2)
      return v1.day < v2.day
    end)
    ifNoNeedToSlide = false
    GameUIActivity:ShowVipSign()
    GameUIActivity:SetVipSignTypeText()
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_sign_req.Code then
    GameUIBarLeft.isFirstShow = nil
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  end
  return false
end
function GameUIActivity.RequestVipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_loot_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.vip_loot_ack.Code then
    GameUIActivity.vipList = content.levels
    GameUIActivity.vipKrypton = content.kryptons
    table.sort(GameUIActivity.vipList, function(v1, v2)
      return v1.level < v2.level
    end)
    GameUIBarLeft.vipList = GameUIActivity.vipList
    GameUIBarLeft:checkActivityStatus()
    return true
  end
  return false
end
function GameUIActivity.RequestLevelCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.level_loot_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.level_loot_ack.Code then
    DebugOut("\230\137\147\229\141\176 RequestLevelCallback 1")
    DebugTable(content.levels)
    GameUIActivity.levelList = content.levels
    GameUIActivity.levelKrypton = content.kryptons
    DebugOut("\230\137\147\229\141\176 levelKrypton")
    DebugTable(content.kryptons)
    GameUIBarLeft.levelList = content.levels
    GameUIBarLeft:checkActivityStatus()
    DebugActivity("\230\137\147\229\141\176 RequestLevelCallback 2")
    GameUIActivity:clearLevelList()
    GameUIActivity:showLevelList()
    return true
  end
  return false
end
function GameUIActivity.Receivecallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.sign_activity_reward_req.Code then
    if content.code == 0 then
      GameUIActivity:RequestSign()
    else
      local text = AlertDataList:GetTextFromErrorCode(content.code)
      GameTip:Show(text, 3000)
    end
    return true
  end
  return false
end
function GameUIActivity.ReceiveVipSignCallback(msgType, content)
  DebugOut("ReceiveVipSignCallback:", msgType)
  DebugTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.pay_sign_reward_req.Code then
    if content.code == 0 then
      GameUIActivity:RequestVipSign()
    else
      local text = AlertDataList:GetTextFromErrorCode(content.code)
      GameTip:Show(text, 3000)
    end
    return true
  end
  return false
end
function GameUIActivity.sortSignItem(v1, v2)
  if v1 and v2 and v1.days < v2.days then
    return true
  end
  return false
end
function GameUIActivity.sortNewSignItem(v1, v2)
  if v1 and v2 then
    if v1.award_status ~= v2.award_status then
      if v1.award_status == 1 then
        return true
      elseif v2.award_status == 1 then
        return false
      else
        return v1.award_status < v2.award_status
      end
    else
      return v1.award_id < v2.award_id
    end
  end
  return false
end
GameUIActivity.mWaitForSendFBGift = false
function GameUIActivity:RequestReceiveSign(index)
  local item = self.signlist.item[index]
  self.signReceiveIndex = index
  GameUIActivity.param = {
    day = item.days
  }
  DebugOut("RequestReceiveSign:")
  DebugTable(self.signlist)
  DebugOut("vv-curentItem")
  DebugTable(item)
  if item and item.status == 1 then
    if Facebook:IsFacebookEnabled() and GameUIActivity.signlist.share_friends and GameUIActivity.isShareFacebook then
      GameUIActivity.mWaitForSendFBGift = true
    end
    NetMessageMgr:SendMsg(NetAPIList.sign_activity_reward_req.Code, GameUIActivity.param, GameUIActivity.Receivecallback, true)
  end
end
function GameUIActivity:RequestReceiveVipSign(index)
  local item = self.vipSignList.items[index]
  self.vipSignReceiveIndex = index
  GameUIActivity.vipParam = {
    day = item.day
  }
  if item and item.status == 1 then
    NetMessageMgr:SendMsg(NetAPIList.pay_sign_reward_req.Code, GameUIActivity.vipParam, GameUIActivity.ReceiveVipSignCallback, true)
  elseif item and item.status == 3 then
    GameUIBarLeft.isFirstShow = nil
    GameVip:showVip(false, self.RequestVipSign)
  end
end
function GameUIActivity:GetLastSignedIndex()
  local lastSignedIndex = 1
  for k, v in ipairs(GameUIActivity.signlist.item) do
    if tonumber(v.status) == 2 then
      lastSignedIndex = k
    else
      break
    end
  end
  return lastSignedIndex
end
function GameUIActivity:GetLastVipSignedIndex()
  local lastSignedIndex = 1
  for k, v in ipairs(GameUIActivity.vipSignList.items) do
    if tonumber(v.status) == 2 then
      lastSignedIndex = k
    else
      break
    end
  end
  return lastSignedIndex
end
function GameUIActivity:GetMaxSignedIndex()
  if GameUIActivity.signlist then
    return #GameUIActivity.signlist.item
  else
    return 1
  end
end
function GameUIActivity:GetMaxVipSignedIndex()
  if GameUIActivity.vipSignList then
    return #GameUIActivity.vipSignList.items
  else
    return 1
  end
end
function GameUIActivity:InitSign()
  DebugActivity("InitSign")
  if not self:GetFlashObject() then
    return
  end
  local key = 1
  local itemsList = GameUIActivity.signlist.item
  local left = 1
  local right = #itemsList
  while left <= right do
    local mid = math.floor((left + right) / 2)
    if tonumber(itemsList[mid].status) == 2 then
      key = mid
      left = mid + 1
    else
      right = mid - 1
    end
  end
  local needForceShow
  if key < 30 then
    if tonumber(itemsList[key + 1].status) == 1 then
      needForceShow = math.ceil((key + 1) / 7)
    else
      needForceShow = math.ceil(key / 7)
    end
  end
  local totalItemCnt = math.ceil(#GameUIActivity.signlist.item / 7)
  DebugActivity("\230\137\147\229\141\176 KEY,totalItemCnt", totalItemCnt, needForceShow)
  self:GetFlashObject():InvokeASCallback("_root", "initSignlist", totalItemCnt)
  if needForceShow and needForceShow > 3 then
    self:GetFlashObject():InvokeASCallback("_root", "SetSignListTail", 5)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetSignListHead", needForceShow)
  end
end
function GameUIActivity.DownloadRankImageTargetSignCallback(extInfo)
  if GameUIActivity:GetFlashObject() then
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "updateRankImageTargetSign", extInfo.rank_id, extInfo.id)
  end
end
function GameUIActivity:InitVipSign()
  DebugActivity("InitVipSign")
  if not self:GetFlashObject() then
    return
  end
  local key = 1
  local itemsList = GameUIActivity.vipSignList.items
  local left = 1
  local right = #itemsList
  while left <= right do
    local mid = math.floor((left + right) / 2)
    if tonumber(itemsList[mid].status) == 1 or tonumber(itemsList[mid].status) == 2 then
      key = mid
      left = mid + 1
    else
      right = mid - 1
    end
  end
  local needForceShow
  if key < 7 then
    if tonumber(itemsList[key + 1].status) == 3 then
      needForceShow = math.ceil(key + 1)
    else
      needForceShow = math.ceil(key)
    end
  end
  local totalItemCnt = #itemsList
  DebugActivity("\230\137\147\229\141\176 KEY", key)
  DebugTable(GameUIActivity.vipSignList.items)
  self:GetFlashObject():InvokeASCallback("_root", "initVipSignlist", totalItemCnt)
  if needForceShow and needForceShow > 4 then
    self:GetFlashObject():InvokeASCallback("_root", "SetVipSignListTail", 7)
  else
    self:GetFlashObject():InvokeASCallback("_root", "SetVipSignListHead", needForceShow)
  end
end
function GameUIActivity.DownloadRankImageTargetVipSignCallback(extInfo)
  if GameUIActivity:GetFlashObject() then
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "updateRankImageTargetVipSign", extInfo.rank_id, extInfo.id)
  end
end
function GameUIActivity:ShowVipSign()
  table.sort(GameUIActivity.vipSignList.items, function(v1, v2)
    return v1.day < v2.day
  end)
  DebugActivity("ShowVipSign")
  DebugActivityTable(self.vipSignList)
  local hintText = GameLoader:GetGameText("LC_MENU_NEXT_SIGN_CHAR")
  local showText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_INFO_C")
  DebugActivity("ShowVipSign:", hintText, ",", showText)
  if not ifNoNeedToSlide then
    self:InitVipSign()
  else
    if self.vipSignReceiveIndex then
      self:SetVipSignListItem(math.ceil(self.vipSignReceiveIndex))
    end
    self.vipSignReceiveIndex = -1
    ifNoNeedToSlide = false
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "showVipSign", hintText, showText)
    local vipSignText = GameLoader:GetGameText("LC_MENU_REGISTRATION_REWARDS")
    flashObj:InvokeASCallback("_root", "SetVipSignTypeText", vipSignText)
    self:RefreshVipSign()
    local lastIdx = GameUIActivity:GetLastVipSignedIndex()
    local maxIdx = GameUIActivity:GetMaxVipSignedIndex()
    local curFirstIdx = lastIdx
    if maxIdx - lastIdx < 4 then
      curFirstIdx = maxIdx - 4
    end
    if curFirstIdx <= 0 then
      curFirstIdx = 1
    end
  end
end
function GameUIActivity:ShowSign()
  table.sort(GameUIActivity.signlist.item, function(v1, v2)
    return v1.days < v2.days
  end)
  GameUIActivity.isShareFacebook = true
  if nil ~= GameSettingData.isSignShareFacebook then
    GameUIActivity.isShareFacebook = GameSettingData.isSignShareFacebook
  end
  if GameUIActivity.signlist.is_first_day then
    GameUIActivity.isRefreshSign = true
  end
  if Facebook:IsFacebookEnabled() and not GameUIActivity.signlist.is_first_day and not GameUIActivity.isRefreshSign then
    GameUIActivity.IsShowShareFacebookFunction = true
  else
    GameUIActivity.IsShowShareFacebookFunction = false
  end
  if GameUIActivity.IsShowShareFacebookFunction then
    DebugActivity("ShowSign1")
    DebugActivityTable(self.signlist)
    local isRunning, hintText = false, ""
    if Facebook:IsBindFacebook() and GameUIActivity.isShareFacebook then
      GameUIActivity.isShareFacebook = true
    else
      GameUIActivity.isShareFacebook = false
    end
    DebugOut("GameUIActivity.isShareFacebook:", Facebook:IsBindFacebook(), GameUIActivity.isShareFacebook, GameSettingData.isSignShareFacebook)
    DebugOut(GameUIActivity.isShareFacebook)
    local showText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_INFO_W")
    if self.signlist.status == GameUIActivity.ACTIVITY_STS_RUNNING then
      DebugActivity("ACTIVITY_STS_RUNNING")
      isRunning = true
      if not ifNoNeedToSlide then
        self:InitSign()
      else
        if self.signReceiveIndex then
          if self.signReceiveIndex == 30 then
            self:InitSign()
          else
            self:SetSignListItem(math.ceil(self.signReceiveIndex / 7))
          end
        end
        self.signReceiveIndex = -1
        ifNoNeedToSlide = false
      end
      DebugActivity("hintText", hintText)
      do
        local flashObj = self:GetFlashObject()
        if flashObj then
          flashObj:InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, GameUIActivity.isShareFacebook)
          DebugOut("RefreshSign")
          self:RefreshSign()
          do
            local lastIdx = GameUIActivity:GetLastSignedIndex()
            if TutorialQuestManager.QuestTutorialSign:IsActive() then
              flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx, false, true)
              local item = self.signlist.item[lastIdx]
              if 2 == item.status then
                flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx, false, false)
                flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx + 1, true, false)
                local function callback()
                  flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx + 1, false, false)
                end
                GameUICommonDialog:PlayStory({1100045}, callback)
              end
            end
          end
        end
      end
    elseif self.signlist.status == GameUIActivity.ACTIVITY_STS_NOT_BEGIN then
      DebugActivity("ACTIVITY_STS_NOT_BEGIN")
      hintText = GameLoader:GetGameText("LC_MENU_NEXT_SIGN_CHAR")
      DebugActivity("hintText", hintText)
      self:GetFlashObject():InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, GameUIActivity.isShareFacebook)
    else
      hintText = GameLoader:GetGameText("LC_MENU_NO_SIGN_CHAR")
      DebugActivity("hintText", hintText)
      self:GetFlashObject():InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, GameUIActivity.isShareFacebook)
    end
  else
    GameUIActivity.isShareFacebook = false
    DebugActivity("ShowSign")
    DebugActivityTable(self.signlist)
    local isRunning, hintText = false, ""
    local showText = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_INFO_C")
    if self.signlist.status == GameUIActivity.ACTIVITY_STS_RUNNING then
      DebugActivity("ACTIVITY_STS_RUNNING")
      isRunning = true
      if not ifNoNeedToSlide then
        self:InitSign()
      else
        if self.signReceiveIndex then
          if self.signReceiveIndex == 30 then
            self:InitSign()
          else
            self:SetSignListItem(math.ceil(self.signReceiveIndex / 7))
          end
        end
        self.signReceiveIndex = -1
        ifNoNeedToSlide = false
      end
      DebugActivity("hintText", hintText)
      do
        local flashObj = self:GetFlashObject()
        if flashObj then
          flashObj:InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, nil)
          self:RefreshSign()
          do
            local lastIdx = GameUIActivity:GetLastSignedIndex()
            if TutorialQuestManager.QuestTutorialSign:IsActive() then
              flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx, false, true)
              local item = self.signlist.item[lastIdx]
              if 2 == item.status then
                flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx, false, false)
                flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx + 1, true, false)
                local function callback()
                  flashObj:InvokeASCallback("_root", "SetSignListItemTip", lastIdx + 1, false, false)
                end
                GameUICommonDialog:PlayStory({1100045}, callback)
              end
            end
          end
        end
      end
    elseif self.signlist.status == GameUIActivity.ACTIVITY_STS_NOT_BEGIN then
      DebugActivity("ACTIVITY_STS_NOT_BEGIN")
      hintText = GameLoader:GetGameText("LC_MENU_NEXT_SIGN_CHAR")
      DebugActivity("hintText", hintText)
      self:GetFlashObject():InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, nil)
    else
      hintText = GameLoader:GetGameText("LC_MENU_NO_SIGN_CHAR")
      DebugActivity("hintText", hintText)
      self:GetFlashObject():InvokeASCallback("_root", "showSign", isRunning, hintText, GameUIActivity.IsShowShareFacebookFunction, showText, nil)
    end
  end
end
function GameUIActivity:ShowNewSign()
  local text = " "
  if self.newSignList.is_new then
    text = GameLoader:GetGameText("LC_MENU_SIGN_ACTIVITY_DESC_OLD")
  else
    text = GameLoader:GetGameText("LC_MENU_SIGN_ACTIVITY_DESC_NEW")
  end
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showNewSign", text, #self.newSignList.awards)
  end
end
function GameUIActivity:IsSingItemInRecommandList(itemId)
  for k, v in ipairs(GameUIActivity.signlist.recommands) do
    if v == itemId then
      return true
    end
  end
  return false
end
function GameUIActivity:SetSignListItem(itemId)
  DebugOut("SetSignListItem qq", itemId)
  if itemId then
    local all_days_list = {}
    for i = 1, 7 do
      local item = self.signlist.item[(itemId - 1) * 7 + i]
      if item then
        DebugActivityTable(item)
        local detail = {}
        local name = item.days
        detail._status = item.status
        local rewards = item.rewards[1].number
        local itemType = item.rewards[1].item_type
        detail.itemType, detail.rewards = self:getSignItemDetailInfo(itemType, rewards, 1)
        detail.received = GameLoader:GetGameText("LC_MENU_RECEIVED_BUTTON")
        detail.receive = GameLoader:GetGameText("LC_MENU_SIGN_CHAR")
        detail.days = item.days .. " DAY"
        if GameSettingData.Save_Lang == "ru" then
          detail.days = "\208\148\208\149\208\157\208\172 " .. item.days
        end
        detail.nameText = GameLoader:GetGameText("LC_MENU_CHAR_SIGN") .. " " .. name
        detail.fleet_type = CommanderRanks[1]
        local isNormalFleet = true
        detail.is_fleet = false
        if item.rewards[1].item_type == "fleet" then
          detail.is_fleet = true
          detail.fleet_type = GameUtils:GetFleetRankFrame(GameDataAccessHelper:GetCommanderAbility(item.rewards[1].number).Rank, GameUIActivity.DownloadRankImageTargetSignListCallback, itemId)
          isNormalFleet = not (100 <= tonumber(item.rewards[1].number)) or not (tonumber(item.rewards[1].number) <= 300)
        elseif item.rewards[1].item_type == "item" and item.rewards[1].number >= 1200 and item.rewards[1].number <= 1299 then
          detail.rewards = GameUtils.numberConversion(item.rewards[1].no)
        end
        detail.vip = item.vip
        detail.vip_crit = item.vip_crit
        detail.BgType = 1
        if self:IsSingItemInRecommandList((itemId - 1) * 7 + i) then
          detail.BgType = 3
        end
        table.insert(all_days_list, detail)
      end
    end
    local weekText = string.format(GameLoader:GetGameText("LC_MENU_SIGNIN_DESC"), itemId)
    DebugOutPutTable(all_days_list, "__SetSignListItem__" .. itemId .. "    " .. weekText)
    self:GetFlashObject():InvokeASCallback("_root", "SetSignListItem", itemId, all_days_list, weekText)
  end
end
function GameUIActivity.DownloadRankImageTargetSignListCallback(extInfo)
  if GameUIActivity:GetFlashObject() then
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "updateRankImageSignList", extInfo.rank_id, extInfo.id)
  end
end
function GameUIActivity:IsVipSignRecommandListContainItem(itemId)
  if not GameUIActivity.vipSignList.recommands then
    return false
  end
  for k, v in pairs(GameUIActivity.vipSignList.recommands) do
    if v == itemId then
      return true
    end
  end
  return false
end
function GameUIActivity:SetVipSignListItem(itemId)
  DebugActivity("SetVipSignListItem", itemId)
  local item = self.vipSignList.items[itemId]
  DebugActivityTable(item)
  if item then
    local rewardDetails = {}
    local itemCnt = #item.rewards
    local name = item.day .. " DAY"
    if GameSettingData.Save_Lang == "ru" then
      name = "\208\148\208\149\208\157\208\172 " .. item.day
    end
    local status = item.status
    local received = GameLoader:GetGameText("LC_MENU_FACEBOOK_REWARDS_HISTORY")
    local receive = GameLoader:GetGameText("LC_MENU_FACEBOOK_REWARDS_BUTTON")
    local topText = GameLoader:GetGameText("LC_MENU_VIP_REWARDS_PURCHASE_BUTTON")
    local days = GameLoader:GetGameText("LC_MENU_UNOPENED_BUTTON")
    isRecommand = false
    if self:IsVipSignRecommandListContainItem(itemId) then
      isRecommand = true
    end
    for k, v in ipairs(item.rewards) do
      local rewardDetail = {}
      local rewards = v.number
      local itemType = v.item_type
      rewardDetail.itemType, rewardDetail.rewards = self:getSignItemDetailInfo2(itemType, rewards, GameHelper:GetAwardCount(itemType, v.number, v.no))
      rewardDetail.isResource = GameHelper:IsResource(v.item_type)
      table.insert(rewardDetails, rewardDetail)
    end
    DebugActivity("SetSignListItem--", name, rewards)
    DebugOutPutTable(rewardDetails, "SetSignListItem--" .. itemId)
    self:GetFlashObject():InvokeASCallback("_root", "SetVipSignListItem", itemId, name, status, received, receive, topText, days, isRecommand, itemCnt, rewardDetails)
  end
end
function GameUIActivity.DownloadRankImageVipSignListCallback(extInfo)
  if GameUIActivity:GetFlashObject() then
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "updateRankImageVipSignList", extInfo.rank_id, extInfo.id)
  end
end
function GameUIActivity:getSignItemDetailInfo(itemType, number, no)
  DebugActivity("getSignItemDetailInfo", ",", itemType, ",", number, ",", no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    if isitem then
      rewards = GameLoader:GetGameText("LC_MENU_ITEM_CHAR")
    else
      rewards = GameLoader:GetGameText("LC_MENU_KRYPTON_TAB")
    end
    if no > 1 then
      rewards = rewards .. "X" .. no
    end
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIActivity:getSignItemDetailInfo2(itemType, number, no)
  DebugActivity("getSignItemDetailInfo", ",", itemType, ",", number, ",", no)
  local itemType = itemType
  local rewards = tonumber(number)
  no = no or 1
  if itemType == "fleet" then
    itemType = GameDataAccessHelper:GetFleetAvatar(rewards)
    rewards = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
  elseif itemType == "item" or itemType == "krypton" then
    local isitem = itemType == "item"
    if DynamicResDownloader:IsDynamicStuff(rewards, DynamicResDownloader.resType.PIC) then
      local fullFileName = DynamicResDownloader:GetFullName(rewards .. ".png", DynamicResDownloader.resType.PIC)
      local localPath = "data2/" .. fullFileName
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
        itemType = "item_" .. rewards
      else
        itemType = "temp"
        self:AddDownloadPath(rewards, 0, 0)
      end
    else
      itemType = "item_" .. rewards
    end
    rewards = "x" .. no
  elseif itemType == "pve_supply" then
    rewards = GameLoader:GetGameText("LC_MENU_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  return itemType, rewards
end
function GameUIActivity:RefreshSign()
  local countText, totalText, itemFrame, numberText = "", "", "empty", ""
  local rewards = self.signlist.rewards
  local status = self.signlist.status
  assert(rewards)
  countText = GameLoader:GetGameText("LC_MENU_HAS_ATTENDANCE_CHAR") .. self.signlist.accumulate .. " " .. GameLoader:GetGameText("LC_MENU_SING_TIMES_CHAR")
  totalText = GameLoader:GetGameText("LC_MENU_SIGN_CHAR") .. " " .. self.signlist.total
  if rewards[1].item_type == "item" then
    itemFrame = "item_" .. rewards[1].number
  else
    itemFrame = rewards[1].item_type
    numberText = rewards[1].number
  end
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSign", countText, totalText, itemFrame, numberText, status)
end
function GameUIActivity:RefreshVipSign()
end
function GameUIActivity:AddDownloadPath(itemID, itemKey, index)
  DebugStore("Add download path", itemID, itemKey, index)
  local resName = itemID .. ".png"
  local extInfo = {}
  extInfo.itemKey = itemKey
  extInfo.index = index
  extInfo.item_id = itemID
  extInfo.resPath = "data2/LAZY_LOAD_dynamic_" .. resName
  DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PIC, extInfo, nil)
end
function GameUIActivity:RefreshSignList()
  self:GetFlashObject():InvokeASCallback("_root", "RefreshSignList")
end
function GameUIActivity.RequestNewsErrCallback(responseContent)
  DebugActivity("GameUIActivity.RequestNewsErrCallback")
  DebugActivityTable(responseContent)
end
function GameUIActivity:InitNewsList()
  self:GetFlashObject():InvokeASCallback("_root", "showNewsList")
end
function GameUIActivity:InitOnlineList()
  hasShowReceiveBtn = false
  self:GetFlashObject():InvokeASCallback("_root", "showOnlineList")
end
function GameUIActivity:InitLevelList()
  self:GetFlashObject():InvokeASCallback("_root", "showLevelList")
end
function GameUIActivity:InitVipList()
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showVipList")
  end
end
function GameUIActivity:InitOpenList(...)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "showOpenList")
  end
end
function GameUIActivity.NetCallbackClearGateway(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.new_activities_compare_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIActivity:ShowVipList()
  if not self:GetFlashObject() then
    return
  end
  if not GameUIActivity.vipList then
    return
  end
  local key = 1
  for k, v in pairs(GameUIActivity.vipList) do
    if tonumber(v.status) == 1 then
      key = k
      break
    end
  end
  for k, v in pairs(GameUIActivity.vipList) do
    if k == key then
      self:GetFlashObject():InvokeASCallback("_root", "addVipListitem", k, true)
    else
      self:GetFlashObject():InvokeASCallback("_root", "addVipListitem", k, false)
    end
  end
end
function GameUIActivity:sortLevelList()
  table.sort(self.levelList, function(levelLootLhs, levelLootRhs)
    return levelLootLhs.level < levelLootRhs.level
  end)
end
function GameUIActivity:findTheFirstGetableItemInLevelLootList()
  for i, levelLoot in ipairs(GameUIActivity.levelList) do
    if self:isTheLevelLootReady(levelLoot) then
      return i, levelLoot
    end
  end
  return nil
end
function GameUIActivity:showLevelList()
  if not GameUIActivity.levelList then
    return
  end
  if not self:GetFlashObject() then
    return
  end
  self:sortLevelList()
  for i, levelLoot in ipairs(GameUIActivity.levelList) do
    self:GetFlashObject():InvokeASCallback("_root", "addLevelListItem", i, false)
  end
  local index = self:findTheFirstGetableItemInLevelLootList()
  if nil ~= index then
    index = self:FixedSetListHeadPosError(#GameUIActivity.levelList, index, 4)
    self:GetFlashObject():InvokeASCallback("_root", "setLevelListHead", index)
  end
end
function GameUIActivity:FixedSetListHeadPosError(length, index, showItemCount)
  local afterFixedIndex = index
  if showItemCount < length then
    if length - index >= showItemCount - 1 then
      afterFixedIndex = index
    else
      afterFixedIndex = length - (showItemCount - 1)
    end
  end
  return afterFixedIndex
end
function GameUIActivity:sortOnlineList()
  table.sort(self.onlineList, function(onlineGiftInfoLhs, onlineGiftInfoRhs)
    return onlineGiftInfoLhs.gift_type < onlineGiftInfoRhs.gift_type
  end)
end
function GameUIActivity:findTheFirstGetableItemInOnlineList()
  DebugOut("GameUIActivity:findTheFirstGetableItemInOnlineList")
  DebugTable(self.onlineList)
  for i, onlineItem in ipairs(self.onlineList) do
    DebugOut("")
    if self:isTheOnlineItemReady(onlineItem) then
      return i, onlineItem
    end
  end
  return nil
end
function GameUIActivity.showOnlineList()
  DebugOut("showOnlineList")
  DebugTable(GameUIActivity.onlineList)
  if not GameUIActivity.onlineList then
    return
  end
  GameUIActivity:sortOnlineList()
  for i, onlineGift in ipairs(GameUIActivity.onlineList) do
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "addOnlineListItem", i, false)
  end
  local index = GameUIActivity:findTheFirstGetableItemInOnlineList()
  DebugOut("ertwjrjgrkgdfksgdfgst345534")
  DebugOut(index)
  if nil ~= index then
    index = GameUIActivity:FixedSetListHeadPosError(#GameUIActivity.onlineList, index, 4)
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "setOnlineListHead", index)
  end
end
function GameUIActivity.getOnlineList(content)
  DebugOut("GameUIActivity.getOnlineList")
  GameUIActivity.onlineList = content.items
  for k, v in pairs(GameUIActivity.onlineList) do
    if v.times > 0 then
      GameUIActivity.currentOnlineCDTime = v.times + os.time()
      hasShowReceiveBtn = false
      break
    end
  end
  GameUIBarLeft:checkActivityStatus()
  if not GameUIActivity:GetFlashObject() then
    return
  end
  if not GameStateManager:GetCurrentGameState():IsObjectInState(GameUIActivity) then
    return
  end
  if GameUIActivity.isShowFacebookLogin then
    GameUIActivity.isShowFacebookLogin = false
    GameUIActivity.ShowSigned()
  else
    GameUIActivity:ShowOnline()
  end
end
function GameUIActivity.IsHavePromCommander()
  return GameUIActivity.m_isHavePromCommander
end
function GameUIActivity:IsDownloadPNGExsit(filename)
  return ...
end
function GameUIActivity:IsCanReward(code)
  if not GameUIBarLeft.specialActicityList then
    return
  end
  local isCanReward = false
  if GameUIBarLeft.specialActicityList.news_ids and #GameUIBarLeft.specialActicityList.news_ids > 0 then
    for k, v in pairs(GameUIBarLeft.specialActicityList.news_ids) do
      if tonumber(v) == tonumber(code) then
        isCanReward = true
      end
    end
  end
  return isCanReward
end
function GameUIActivity:IsMixedItem(item_type)
  if item_type == "item" or item_type == "krypton" then
    return true
  end
  return false
end
function GameUIActivity:setVipListItem(itemId)
  local item = GameUIActivity.vipList[itemId]
  local item_type = ""
  local item_number = ""
  local cnt_number = ""
  for i = 1, 5 do
    if item.loots[i] then
      local item_type_tmp = item.loots[i].item_type
      local item_number_tmp = tonumber(item.loots[i].number)
      item_type_tmp, item_number_tmp = self:getSignItemDetailInfo(item_type_tmp, item_number_tmp, item.loots[i].no)
      item_type = item_type .. item_type_tmp .. "\001"
      item_number = item_number .. item_number_tmp .. "\001"
      cnt_number = cnt_number .. item.loots[i].no .. "\001"
    else
      item_type = item_type .. "empty" .. "\001"
      item_number = item_number .. 0 .. "\001"
      cnt_number = cnt_number .. "1" .. "\001"
    end
  end
  local rewardText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
  local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  DebugActivity("\230\137\147\229\141\176_setVipListItem ", itemId, item.level, item_type, item_number, item.canloot, cnt_number)
  self:GetFlashObject():InvokeASCallback("_root", "setVipListItem", itemId, item.level, item_type, item_number, item.status, rewardText, aleadyText, cnt_number)
end
function GameUIActivity:setLevelListItem(itemId)
  DebugOut("FUCK_setLevelListItem .." .. itemId)
  DebugTable(GameUIActivity.levelList)
  local item = GameUIActivity.levelList[itemId]
  local item_type = ""
  local item_number = ""
  local cnt_number = ""
  for i = 1, 5 do
    if item.loots[i] then
      local item_type_tmp = item.loots[i].item_type
      local item_number_tmp = tonumber(item.loots[i].number)
      item_type_tmp, item_number_tmp = self:getSignItemDetailInfo(item_type_tmp, item_number_tmp, item.loots[i].no)
      item_type = item_type .. item_type_tmp .. "\001"
      item_number = item_number .. item_number_tmp .. "\001"
      cnt_number = cnt_number .. item.loots[i].no .. "\001"
    else
      item_type = item_type .. "empty" .. "\001"
      item_number = item_number .. 0 .. "\001"
      cnt_number = cnt_number .. "1" .. "\001"
    end
  end
  DebugActivity("\230\137\147\229\141\176 ", itemId, item.level, item_type, item_number, item.canloot)
  local rewardText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
  local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  self:GetFlashObject():InvokeASCallback("_root", "setlevelistItem", itemId, GameLoader:GetGameText("LC_MENU_Level") .. item.level, item_type, item_number, item.status, rewardText, aleadyText, cnt_number)
end
GameUIActivity.totalCreitReward_time1 = {
  1000,
  2000,
  5000,
  8000,
  10000,
  20000,
  50000,
  100000,
  200000
}
GameUIActivity.totalCreitReward_time2 = {
  2000,
  5000,
  8000,
  18000,
  50000,
  100000,
  200000
}
function GameUIActivity:setOnlineListItem(itemId)
  DebugActivity("setOnlineListItem", itemId)
  if not self:GetFlashObject() then
    return
  end
  local item = GameUIActivity.onlineList[itemId]
  DebugTable(item)
  local item_type = ""
  local item_number = ""
  local cnt_number = " "
  for i = 1, 5 do
    if item.gifts[i] then
      local type_tmp = item.gifts[i].item_type
      local number_tmp = tonumber(item.gifts[i].number)
      local no = item.gifts[i].no
      no = no or 1
      type_tmp, number_tmp = self:getSignItemDetailInfo(type_tmp, number_tmp, no)
      item_type = item_type .. type_tmp .. "\001"
      item_number = item_number .. number_tmp .. "\001"
      cnt_number = cnt_number .. no .. "\001"
    else
      item_type = item_type .. "empty" .. "\001"
      item_number = item_number .. 0 .. "\001"
      cnt_number = cnt_number .. "1" .. "\001"
    end
  end
  local rewardText = GameLoader:GetGameText("LC_MENU_CAPTION_RECEIVE")
  local aleadyText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  self:GetFlashObject():InvokeASCallback("_root", "setOnlineListItem", itemId, item.gift_type, item_type, item_number, item.mark, rewardText, aleadyText, item.times, cnt_number)
end
function GameUIActivity:UpdateOnlineTime()
  if not GameUIActivity.onlineList then
    return
  end
  if not GameUIActivity.currentOnlineCDTime then
    return
  end
  local m_flash = self:GetFlashObject()
  for k, v in pairs(GameUIActivity.onlineList) do
    local left_times = GameUIActivity.currentOnlineCDTime - os.time()
    if v.times > 0 then
      if left_times > 0 then
        if m_flash then
          m_flash:InvokeASCallback("_root", "UpdateOnlineTime", k, left_times)
        end
        break
      end
      if left_times == 0 and hasShowReceiveBtn == false then
        DebugOut("time have been come 1")
        hasShowReceiveBtn = true
        GameUIBarLeft:setOnlineTagBytimer()
        if m_flash then
          m_flash:InvokeASCallback("_root", "UpdateOnlineTime", k, left_times)
        end
        v.mark = 1
        break
      end
      if left_times < 0 and not hasShowReceiveBtn then
        DebugOut("time have been come 2")
        hasShowReceiveBtn = true
        if m_flash then
          m_flash:InvokeASCallback("_root", "UpdateOnlineTime", k, 0)
        end
        GameUIBarLeft:setOnlineTagBytimer()
        v.mark = 1
      end
      break
    end
  end
end
function GameUIActivity:setNoticeIsRead(itemId)
  local readID = GameUtils:GetShortActivityID(itemId)
  local param = {type = 2, id = readID}
  NetMessageMgr:SendMsg(NetAPIList.notice_set_read_req.Code, param, GameUIActivity.SetNoticeIsReadCallback, false)
end
function GameUIActivity.SetNoticeIsReadCallback(msgType, content)
  DebugOut("\230\137\147\229\141\176SetNoticeIsReadCallback", msgType)
  DebugTable(content)
  if msgType == NetAPIList.gateway_info_ntf.Code then
    GameGlobalData.RefreshNewsAndAct(content)
    return true
  end
  return false
end
function GameUIActivity:updateActivityType2Content(content)
  GameUIActivity.activityContentRewareType2 = content
  DebugActivity("GameUIActivity:updateActivityType2Content")
  DebugActivityTable(content)
  DebugActivityTable(GameUIActivity.activityContentRewareType2.sections)
  local descText, titleText = "", ""
  if GameUIActivity.currentLabType2Name == "officer" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_DIPLOMAT_DES_" .. GameUIActivity.currentLabType2Code .. "_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_DIPLOMAT_ALERT_CHAR"), tonumber(content.value))
  end
  if GameUIActivity.currentLabType2Name == "fleet" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_RECRUIT_DES_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_RECRUIT_ALERT_CHAR"), tonumber(content.value))
  end
  if GameUIActivity.currentLabType2Name == "total_credit" then
    descText = GameLoader:GetGameText("LC_MENU_EVENT_PURCHES_DES_CHAR")
    titleText = string.format(GameLoader:GetGameText("LC_MENU_EVENT_PURCHES_ALERT_CHAR"), tonumber(content.value))
  end
  GameUIActivity:GetFlashObject():InvokeASCallback("_root", "shownewsContentType2", descText, titleText, GameUIActivity.currentActivityContentTitle)
  GameUIActivity:ShowActivityRewardType2List()
end
function GameUIActivity.PromotionsDetailCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.promotion_detail_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.promotion_detail_ack.Code then
    DebugOut("GameUIActivity.PromotionsDetailCallbac ", msgType)
    DebugTable(content)
    if GameUIActivity.m_currentTap == 2 then
      GameUIActivity:updateActivityType2Content(content)
    elseif GameUIActivity.m_currentTap == 4 then
      local item = GameUIActivity.activityList[GameUIActivity.activityContentIndex]
      local title = item.title
      local function foo()
        GameUIActivity:GetSltText(content)
      end
      if pcall(foo) then
      end
      local content = GameUIActivity.m_currentContent
      DebugOut("\230\137\147\229\141\176 ----\227\128\139promotion_detail_ack", title, content)
      GameUIActivity:GetFlashObject():InvokeASCallback("_root", "showNewsContent", title, content, tonumber(GameUIActivity.activityContentIndex), false, true, false)
    end
    return true
  end
  return false
end
function GameUIActivity:GetSltText(content)
  local slt = require("slt2.tfl")
  local awards = content.sections
  local data = {}
  DebugOut("---FUCK-----")
  DebugTable(awards)
  DebugOut("----")
  for i, v in ipairs(awards) do
    DebugOut(v.condition)
    data[i] = {}
    data[i].condition = v.condition
    data[i].status = v.status
    data[i].game_items = {}
    for i_1, v_1 in ipairs(v.awards) do
      data[i].game_items[i_1] = {}
      data[i].game_items[i_1].name, data[i].game_items[i_1].count = GameUIActivity:GetSltItemDetailInfo(v_1.item_type, v_1.number, v_1.no)
    end
  end
  DebugOut(GameUIActivity.m_currentContent)
  DebugOut("--")
  DebugTable(data)
  DebugOut("---FUCK----everybody-")
  local temp = slt.loadstring(GameUIActivity.m_currentContent, "#{", "}#")
  local t = {data = data}
  local result = {}
  function t.outputfn(s)
    table.insert(result, s)
  end
  t.user = {name = ""}
  t.ipairs = ipairs
  t.string = string
  slt.render(temp, t)
  local new_content = table.concat(result)
  new_content = string.gsub(new_content, "&nbsp;", "")
  new_content = string.gsub(new_content, "<br />", " ")
  DebugOut(" ------->", new_content)
  GameUIActivity.m_currentContent = new_content
end
function GameUIActivity:GetSltItemDetailInfo(itemType, number, no)
  DebugOut("---start-", itemType, number, no)
  local name = itemType
  local rewards = tonumber(number)
  no = no or 1
  if name == "fleet" then
    name = GameLoader:GetGameText("LC_NPC_" .. GameDataAccessHelper:GetCommanderName(rewards))
    rewards = no
  elseif name == "item" or name == "krypton" or name == "equip" then
    name = GameLoader:GetGameText("LC_ITEM_ITEM_NAME_" .. number)
    rewards = no
  elseif name == "technique" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_TECHPOINT")
  elseif name == "expedition" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_EXPEDITION")
  elseif name == "credit" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CREDIT")
  elseif name == "money" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_LOOT_CUBIT")
  elseif name == "vip_exp" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_VIP_EXP_CHAR")
  elseif name == "ac_supply" then
    rewards = GameUtils.numberConversion(rewards)
    name = GameLoader:GetGameText("LC_MENU_AC_SUPPLY_CHAR")
  else
    rewards = GameUtils.numberConversion(rewards)
  end
  DebugOut(" name = ", name, "rewards = ", rewards)
  return name, rewards
end
function GameUIActivity.GetCutOffListInfoCallback(msgType, content)
  DebugActivity("GetCutOffListInfoCallback", msgType)
  DebugActivityTable(content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.cut_off_list_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.cut_off_list_ack.Code then
    GameUIActivity.CutOffList = content.cutoff_list
    local buyCredit = ""
    local totalCredit = ""
    local titleText = GameLoader:GetGameText("LC_MENU_FUND_1_CHAR") .. "\001" .. GameLoader:GetGameText("LC_MENU_FUND_2_CHAR") .. "\001" .. GameLoader:GetGameText("LC_MENU_FUND_3_CHAR") .. "\001" .. GameLoader:GetGameText("LC_MENU_FUND_4_CHAR") .. "\001"
    local descText = GameLoader:GetGameText("LC_MENU_FUND_DES_CHAR")
    local onlyText = GameLoader:GetGameText("LC_MENU_FUND_ALERT_CHAR")
    for i = 1, 4 do
      local totalCredit_tmp = GameUIActivity.CutOffList[i].credit * 1.5
      buyCredit = buyCredit .. GameUIActivity.CutOffList[i].credit .. "\001"
      totalCredit = totalCredit .. string.gsub(GameLoader:GetGameText("LC_MENU_FUND_NOTE_CHAR"), "<credits_num>", tonumber(totalCredit_tmp)) .. "\001"
    end
    GameUIActivity:GetFlashObject():InvokeASCallback("_root", "shownewsContentType3", descText, onlyText, titleText, buyCredit, totalCredit, GameUIActivity.currentActivityContentTitle)
    return true
  end
  return false
end
function GameUIActivity:ShowExchangeCodeInfo(item, itemId)
  DebugActivity("GameUIActivity:ShowExchangeCodeInfo()")
  GameUIActivity:GetFlashObject():InvokeASCallback("_root", "showExchangeContent", item.title, tonumber(itemId))
end
function GameUIActivity:GetPromotionDetail(itemId)
  local content = {
    id = itemId,
    index = GameUIActivity.currentLabType2Code
  }
  GameUIActivity.currentLabType2Name = itemId
  NetMessageMgr:SendMsg(NetAPIList.promotion_detail_req.Code, content, GameUIActivity.PromotionsDetailCallback, true)
end
function GameUIActivity:GetCutOffListInfo()
  NetMessageMgr:SendMsg(NetAPIList.cut_off_list_req.Code, nil, GameUIActivity.GetCutOffListInfoCallback, true)
end
function GameUIActivity:Update(dt)
  self:GetFlashObject():InvokeASCallback("_root", "OnUpdate")
  self:GetFlashObject():Update(dt)
  GameUIActivity:UpdateAllBenefitTime(dt)
  GameUIActivity:UpdateAllActivityRemind(dt)
  if self.currentLab == "vipSign" then
    self:GetFlashObject():InvokeASCallback("_root", "updateVIPSignRollTextPos", dt)
  end
end
function GameUIActivity:OnFSCommand(cmd, arg)
  DebugActivity("GameUIActivity:OnFSCommand", cmd)
  if cmd == "CloseContent" then
    assert(0)
    GameUIActivity.currentWebSite = nil
    if self.currentLab == "news" then
      self:GetFlashObject():InvokeASCallback("_root", "contentReturn", true)
      GameUIActivity:InitNewsList()
      GameUIActivity:ShowNewsList()
    else
      self:GetFlashObject():InvokeASCallback("_root", "contentReturn", false)
      GameUIActivity:InitActicity()
      GameUIActivity:ShowActivityList()
    end
  end
  if cmd == "gotoSeeMore" and GameUIActivity.currentWebSite then
    ext.http.openURL(GameUIActivity.currentWebSite)
  end
  if cmd == "showNewsDetail" then
    GameUIActivity.currentWebSite = nil
    local itemId = tonumber(arg)
    self:showNewsContent(itemId)
  end
  if cmd == "showActivityDetail" then
    local itemId = tonumber(arg)
    self:showNewsContent(itemId)
  end
  if cmd == "close_menu" then
    GameUIBarLeft:checkActivityStatus()
    GameStateManager:GetCurrentGameState():EraseObject(GameUIActivity)
    GameStateManager:GetCurrentGameState():EraseObject(GameCommonBackground)
    local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
    if not GameStateMainPlanet.FirstShowSign and immanentversion == 2 then
      GameStateMainPlanet.FirstShowSign = true
      GameStateMainPlanet:CheckShowTutorial()
    end
    if TutorialQuestManager.QuestTutorialSign:IsActive() then
      TutorialQuestManager.QuestTutorialSign:SetActive(false, true)
      TutorialQuestManager.QuestTutorialSign:SetFinish(true, true)
      local callback = function()
      end
      GameUICommonDialog:PlayStory({1100046}, callback)
    end
  end
  if cmd == "showItemDetail" then
    DebugOut("showItemDetail_arg:" .. arg)
    local param = LuaUtils:string_split(arg, "\001")
    GameUIActivity:ShowLoginFundItem(tonumber(param[1]), tonumber(param[2]))
  end
  if cmd == "updateSignList" then
    local itemId = tonumber(arg)
    self:SetSignListItem(itemId)
  elseif cmd == "updateVipSignList" then
    self:SetVipSignListItem(tonumber(arg))
  end
  if cmd == "updateVip" then
    local itemId = tonumber(arg)
    self:setVipListItem(itemId)
  elseif cmd == "updateNewList" then
    local itemId = tonumber(arg)
    self:SetNewsListItem(itemId)
  elseif cmd == "updateLevel" then
    local itemId = tonumber(arg)
    self:setLevelListItem(itemId)
  elseif cmd == "updateOnline" then
    local itemId = tonumber(arg)
    self:setOnlineListItem(itemId)
  end
  if cmd == "updaeActivtity" then
    local itemId = tonumber(arg)
    self:setActivityItem(itemId)
  end
  if cmd == "showactivity" then
    self:ShowNews("activity")
  end
  if cmd == "updateActivitContenRewardType2" then
    local itemId = tonumber(arg)
    self:SetActivityContentRewardType2(itemId)
  end
  if cmd == "shownews" then
    self:ShowNews("news")
  end
  if cmd == "showsign" then
    self:ShowNews("sign")
  end
  if cmd == "showNewSign" then
    self:ShowNews("newSign")
  end
  if cmd == "showOnline" then
    self:ShowNews("online")
  end
  if cmd == "showVipSign" then
    self:ShowNews("vipSign")
  end
  if cmd == "showOpen" then
    self:ShowNews("open")
  end
  if cmd == "showLevel" then
    self:ShowNews("level")
  end
  if cmd == "receivesign" then
    DebugOut("receivesign:", tonumber(arg))
    self:RequestReceiveSign(tonumber(arg))
  end
  if cmd == "receive_vip_sign" then
    DebugOut("receive_vip_sign:", tonumber(arg))
    self:RequestReceiveVipSign(tonumber(arg))
  end
  if cmd == "releaseNewsListItem" then
  end
  if cmd == "gotoVip" then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip.GotoPayment()
  end
  if cmd == "receiveVipItem" then
    local itemId = tonumber(arg)
    GameUIActivity:getVipReward(itemId)
  elseif cmd == "receiveLevelItem" then
    local itemId = tonumber(arg)
    GameUIActivity:getLevelReward(itemId)
  elseif cmd == "receiveActivityContentRewardType2" then
    local itemId = tonumber(arg)
    GameUIActivity:GetActivityContentRewardType2(itemId)
  elseif cmd == "receiveOnline" then
    local itemId = tonumber(arg)
    DebugActivity("---itemId--\227\128\139 ", itemId)
    GameUIActivity:GetOnlineReward(itemId)
  elseif cmd == "buycutoffCard" then
    local itemId = arg
    GameUIActivity:GetActivityContentRewardType3(itemId)
  elseif cmd == "showTargetSignDetail" then
    local item = GameUIActivity.signlist.item[GameUIActivity.signlist.recommands[tonumber(arg)]].rewards[1]
    GameUIActivity:showItemDetil(item, item.item_type)
  elseif cmd == "showTargetVipSignDetail" then
    local item = GameUIActivity.vipSignList.items[GameUIActivity.vipSignList.recommands[tonumber(arg)]].rewards[1]
    GameUIActivity:showItemDetil(item, item.item_type)
  elseif cmd == "click_vipsign_item_detail" then
    DebugOut("click_vipsign_item_detail", arg)
    DebugTable(GameUIActivity.vipSignList.items[tonumber(arg)])
    local item = GameUIActivity.vipSignList.items[tonumber(arg)].rewards[1]
    GameUIActivity:showItemDetil(item, item.item_type)
  elseif cmd == "clicklevelistItemIcon" then
    local item_indexs = LuaUtils:string_split(arg, "\001")
    local item = GameUIActivity.levelList[tonumber(item_indexs[1])].loots[tonumber(item_indexs[2])]
    if item.item_type == "krypton" and GameUIActivity.levelKrypton then
      for i, v in pairs(GameUIActivity.levelKrypton) do
        if tonumber(v.id) == item.number then
          DebugOut("v = ")
          DebugTable(v)
          GameUIActivity:showItemDetil(item, "krypton")
          break
        end
      end
    else
      GameUIActivity:showItemDetil(item, item.item_type)
    end
  elseif cmd == "clickviplistItemIcon" then
    local item_indexs = LuaUtils:string_split(arg, "\001")
    local item = GameUIActivity.vipList[tonumber(item_indexs[1])].loots[tonumber(item_indexs[2])]
    if item.item_type == "krypton" then
      for i, v in pairs(GameUIActivity.vipKrypton) do
        if tonumber(v.id) == item.number then
          DebugOut("v = ")
          DebugTable(v)
          GameUIActivity:showItemDetil(item, "krypton")
          break
        end
      end
    else
      GameUIActivity:showItemDetil(item, item.item_type)
    end
  elseif cmd == "clickonlinelistItemIcon" then
    local item_indexs = LuaUtils:string_split(arg, "\001")
    local item = GameUIActivity.onlineList[tonumber(item_indexs[1])].gifts[tonumber(item_indexs[2])]
    GameUIActivity:showItemDetil(item, item.item_type)
  elseif cmd == "clickActicityRewardsType2ItemIcon" then
    local item_indexs = LuaUtils:string_split(arg, "\001")
    local item = GameUIActivity.activityContentRewareType2.sections[tonumber(item_indexs[1])].awards[tonumber(item_indexs[2])]
    GameUIActivity:showItemDetil(item, item.item_type)
  elseif cmd == "openpagetab" then
    GameUIActivity:ShowOpenfundAndAllbenefit(tonumber(arg))
  elseif cmd == "UpdateOpenPageData" then
    GameUIActivity:UpdateOpenPageData(tonumber(arg))
  elseif cmd == "get_openfund_award" then
    GameUIActivity:GetOpenfunAward(tonumber(arg))
  elseif cmd == "get_allbenefit_award" then
    GameUIActivity:GetAllBenefitAward(tonumber(arg))
  elseif cmd == "get_loginfund_award" then
    GameUIActivity:GetLoginFundAward(tonumber(arg))
  elseif cmd == "buy_loginfund" then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:BuyLoginFund()
  elseif cmd == "buy_openfund" then
    local isCanBuy = false
    local viplevel = GameVipDetailInfoPanel:GetVipLevel()
    if viplevel >= GameUIActivity.OpenFundData.vip then
      GameUIActivity:BuyOpenfund()
    else
      GameTip:Show(string.format(GameLoader:GetGameText("LC_MENU_WELFAREE_VIP_LEVEL"), tostring(GameUIActivity.OpenFundData.vip)))
    end
  elseif cmd == "goto_buy" then
    GameUIActivity:ShowOpenfundAndAllbenefit(1)
  elseif cmd == "ShowBoxAwardItemDetail" then
    local id, subid = unpack(LuaUtils:string_split(arg, "\001"))
    DebugOut("ShowBoxAwardItemDetail", id, subid)
    DebugTable(self.vipSignList.items)
    local item = self.vipSignList.items[tonumber(id)].rewards[tonumber(subid)]
    if item then
      GameUIActivity:showItemDetil(item, item.item_type)
    end
  elseif cmd == "ShowSignItemDetail" then
    local item = self.signlist.item[tonumber(arg)].rewards[1]
    if item then
      GameUIActivity:showItemDetil(item, item.item_type)
    end
  elseif cmd == "updateNewSignList" then
    self:updateNewSignList(tonumber(arg))
  elseif cmd == "GetNewSignReward" then
    self:GetNewSignReward(tonumber(arg))
  elseif cmd == "OnNewSignRewardItemClicked" then
    local id, subid = unpack(LuaUtils:string_split(arg, "\001"))
    local item = GameUIActivity.newSignList.awards[tonumber(id)].awards[tonumber(subid)]
    if item then
      GameUIActivity:showItemDetil(item, item.item_type)
    end
  end
  if cmd == "recharge_released" then
    if string.len(arg) == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_EXCHANGE_CHAR"), 2000)
    else
      DebugActivity("input exchange code = ", arg)
      local param = {code = arg}
      NetMessageMgr:SendMsg(NetAPIList.verify_redeem_code_req.Code, param, GameUIActivity.exchangeCallback, true, nil)
    end
  end
  if cmd == "share_confirm" then
    GameUIActivity:isShare()
  end
  if cmd == "share_cancel" then
    GameUIActivity:shareShowDialog()
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUIActivity.newSignIconCallBack(extInfo)
  local item = GameUIActivity.newSignList.awards[extInfo.itemId].awards[extInfo.subId]
  if item then
    local award_icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
    if GameUIActivity:GetFlashObject() then
      GameUIActivity:GetFlashObject():InvokeASCallback("_root", "SetNewSignListItemIcon", extInfo.itemId, extInfo.subId, award_icon)
    end
  end
end
function GameUIActivity:updateNewSignList(itemId)
  local itemInfo = self.newSignList.awards[itemId]
  if itemInfo then
    local titleText = string.format(GameLoader:GetGameText("LC_MENU_LOGIN_DAY_DESC_OLD"), itemInfo.award_id)
    if self.newSignList.is_new and itemInfo.vip_level > 0 then
      titleText = string.format(GameLoader:GetGameText("LC_MENU_LOGIN_DAY_DESC_NEW"), itemInfo.award_id, itemInfo.vip_level)
    end
    local progressText = GameLoader:GetGameText("LC_MENU_LOGIN_DAY_PROGRESS") .. self.newSignList.now_login .. "/" .. itemInfo.award_id
    local stat = itemInfo.award_status
    local rewards = {}
    local received = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
    local receive = GameLoader:GetGameText("LC_MENU_CLAIM_BUTTON")
    local days = itemInfo.award_id .. " DAY"
    if GameSettingData.Save_Lang == "ru" then
      days = "\208\148\208\149\208\157\208\172 " .. itemInfo.award_id
    end
    for k, v in ipairs(itemInfo.awards) do
      local reward = {}
      local extInfo = {}
      extInfo.itemId = itemId
      extInfo.subId = k
      reward.award_icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v, extInfo, GameUIActivity.newSignIconCallBack)
      reward.award_icon, reward.nameText = self:getSignItemDetailInfo(v.item_type, v.number, v.no)
      table.insert(rewards, reward)
    end
    if self:GetFlashObject() then
      DebugOut("updateNewSignList :", itemId, titleText, progressText, stat)
      DebugTable(rewards)
      self:GetFlashObject():InvokeASCallback("_root", "SetNewSignListItem", itemId, titleText, progressText, rewards, stat, days, receive, received)
    end
  end
end
local NowRequestNewSignItemId
function GameUIActivity:GetNewSignReward(itemId)
  local itemInfo = self.newSignList.awards[itemId]
  if itemInfo then
    local param = {}
    param.award_id = itemInfo.award_id
    NowRequestNewSignItemId = itemId
    NetMessageMgr:SendMsg(NetAPIList.get_login_sign_award_req.Code, param, GameUIActivity.GetNewSignRewardCallBack, true, nil)
  end
end
function GameUIActivity.GetNewSignRewardCallBack(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.get_login_sign_award_req.Code then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  elseif msgType == NetAPIList.get_login_sign_award_ack.Code then
    if GameUIActivity:GetFlashObject() and NowRequestNewSignItemId and content.code == 0 then
      local itemInfo = GameUIActivity.newSignList.awards[NowRequestNewSignItemId]
      itemInfo.award_status = 2
      NowRequestNewSignItemId = nil
      table.sort(GameUIActivity.newSignList.awards, GameUIActivity.sortNewSignItem)
      GameUIActivity:GetFlashObject():InvokeASCallback("_root", "initNewSignlist", #GameUIActivity.newSignList.awards)
    end
    return true
  end
  return false
end
function GameUIActivity:showItemDetil(item_, item_type)
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 480, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 480, operationTable)
    end
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number), nil, nil)
  end
  if item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
  if item_type == "item" or item_type == "fleet" or item_type == "krypton" then
    GameUIActivity.IsShowItemDetail = true
    ItemBox:RegisterHideBoxNtf(GameUIActivity.OnItemBoxClose)
  end
end
function GameUIActivity.OnItemBoxClose()
  GameUIActivity.IsShowItemDetail = false
end
function GameUIActivity:getVipReward(itemId)
  if GameUIActivity.vipList[itemId].status ~= 1 then
    return
  end
  local content = {
    level = GameUIActivity.vipList[itemId].level
  }
  self.receiveVipLevel = GameUIActivity.vipList[itemId].level
  GameUIActivity.updateVipListItemIndex = itemId
  NetMessageMgr:SendMsg(NetAPIList.vip_loot_get_req.Code, content, GameUIActivity.receiveVipCallback, true, nil)
end
function GameUIActivity:GetOnlineReward(itemId)
  DebugOut("GetOnlineReward", itemId)
  local content = {
    type = GameUIActivity.onlineList[itemId].gift_type
  }
  DebugActivity(" \230\137\147\229\141\176 GameUIActivity:GetOnlineReward", GameUIActivity.onlineList[itemId].gift_type)
  NetMessageMgr:SendMsg(NetAPIList.active_gifts_req.Code, content, GameUIActivity.receiveOnlineCallback, true, nil)
end
function GameUIActivity:getLevelReward(itemId)
  if GameUIActivity.levelList[itemId].status ~= 1 then
    return
  end
  local content = {
    level = GameUIActivity.levelList[itemId].level
  }
  self.receivePlayerLevel = GameUIActivity.levelList[itemId].level
  GameUIActivity.updateLevelListItemIndex = itemId
  NetMessageMgr:SendMsg(NetAPIList.level_loot_get_req.Code, content, GameUIActivity.receiveLevelCallback, true, nil)
end
function GameUIActivity.BuyCutoffCardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.buy_cut_off_req.Code then
    local GameVip = LuaObjectManager:GetLuaObject("GameVip")
    GameVip:CheckIsNeedShowVip(content.code)
    return true
  end
  return false
end
function GameUIActivity.receiveOnlineCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.active_gifts_req.Code then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  elseif msgType == NetAPIList.active_gifts_ntf.Code then
    GameUIActivity.getOnlineList(content)
    return true
  end
  return false
end
function GameUIActivity.receiveVipCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_loot_req.Code then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.vip_loot_get_req.Code then
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  end
  return false
end
function GameUIActivity.receiveLevelCallback(msgType, content)
  DebugOut("GameUIActivity.receiveLevelCallback")
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.level_loot_req.Code then
    DebugOut("GameUIActivity.receiveLevelCallback  1")
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    GameUIActivity:RequestLevel(true)
    return true
  elseif msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.level_loot_get_req.Code then
    DebugOut("GameUIActivity.receiveLevelCallback  2")
    GameTip:Show(AlertDataList:GetTextFromErrorCode(content.code), 3000)
    return true
  end
  return false
end
function GameUIActivity:shareShowDialog()
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    GameSettingData.isSignShareFacebook = false
    GameUtils:SaveSettingData()
    GameUIActivity.isShareFacebook = false
    GameUIActivity:isShareFacebookFun(GameUIActivity.isShareFacebook)
  end)
  GameUIMessageDialog:SetNoButton(function()
    GameSettingData.isSignShareFacebook = true
    GameUtils:SaveSettingData()
    GameUIActivity.isShareFacebook = true
    GameUIActivity:isShareFacebookFun(GameUIActivity.isShareFacebook)
  end)
  local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local textInfo = GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_SHARE_ALERT")
  GameUIMessageDialog:Display(textTitle, textInfo)
end
function GameUIActivity:isShareFacebookFun(isShare)
  self:GetFlashObject():InvokeASCallback("_root", "ShowShareFacebook", isShare)
end
function GameUIActivity:isShare()
  DebugOut("GameUIActivity:isShare")
  if Facebook:IsBindFacebook() then
    GameUIActivity.isShareFacebook = true
    GameSettingData.isSignShareFacebook = true
    GameUtils:SaveSettingData()
    GameUIActivity:isShareFacebookFun(GameUIActivity.isShareFacebook)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_FACEBOOK_SIGN_ALERT_1"), nil)
    GameUIActivity.isShareFacebook = false
    GameUIActivity:isShareFacebookFun(GameUIActivity.isShareFacebook)
  end
end
function GameUIActivity:SignAndFaceBook()
  DebugOut("GameUIActivity:SignAndFaceBook")
  DebugOut(GameUIActivity.isShareFacebook)
  if GameUIActivity.isShareFacebook then
    GameUIActivity:GetFaceBookFriendsList()
  else
    DebugOut("no share")
  end
end
function GameUIActivity:GetFaceBookFriendsList()
  DebugOut("GameUIActivity:getFaceBookFriendsList:")
  GameUIActivity.isShowFacebookLogin = true
  Facebook:GetFriendList(GameUIActivity.GetFaceBookFriendsListCallBack)
end
function GameUIActivity.GetFaceBookFriendsListCallBack(success, friends)
  DebugOut("GameUIActivity.getFaceBookFriendsListCallBack")
  if success and friends and friends.friendList then
    DebugTable(friends)
    GameUIActivity.faceBookFriendList = friends.friendList
    if #GameUIActivity.faceBookFriendList > 0 then
      local title = GameLoader:GetGameText("LC_MENU_APPLICATION_REQUEST_TITLE")
      local msg = GameLoader:GetGameText("LC_MENU_APPLICATION_REQUEST_INFO_1")
      local faceBookFriendIdList = {}
      for key, value in ipairs(GameUIActivity.faceBookFriendList) do
        table.insert(faceBookFriendIdList, value.id)
      end
      if GameUIActivity.isShowFacebookDialog then
        GameUIActivity.isShowFacebookDialog = false
        Facebook:SendGiftMsgToFriend(title, msg, faceBookFriendIdList, GameUIActivity.SendGiftMsgToFriendCallBack)
      end
    end
  end
end
function GameUIActivity:SendFaceBookFriendToServer(fbfriends)
  local param = {type = 1, friends = fbfriends}
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.give_friends_gift_req.Code, param, GameUIActivity.ReceiveGetFriendsCallback, true, nil)
end
function GameUIActivity.ReceiveGetFriendsCallback(msgType, content)
  if msgType == NetAPIList.give_friends_gift_ack.Code then
    return true
  end
  return false
end
function GameUIActivity.SendGiftMsgToFriendCallBack(success)
  DebugOut("GameUIActivity.SendGiftMsgToFriendCallBack:")
  DebugOut(success)
  GameUIActivity.isShowFacebookDialog = true
  if success then
    do
      local faceBookFriendIdList = {}
      for key, value in ipairs(GameUIActivity.faceBookFriendList) do
        table.insert(faceBookFriendIdList, value.id)
      end
      local function delayCall()
        GameUIActivity:SendFaceBookFriendToServer(faceBookFriendIdList)
      end
      if NetMessageMgr.ReConnect then
        DebugOut("GameUIActivity Now is reconnect server.")
        NetMessageMgr:AddReconnectSuccessCall(delayCall)
      else
        GameUIActivity:SendFaceBookFriendToServer(faceBookFriendIdList)
      end
    end
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIActivity.OnAndroidBack()
    if GameUIActivity.IsShowItemDetail then
      ItemBox:OnFSCommand("ShieldReleased")
    else
      GameAndroidBackManager:RemoveCallback(GameUIActivity.OnAndroidBack)
      GameUIActivity:GetFlashObject():InvokeASCallback("_root", "moveOut")
    end
  end
end
function GameUIActivity:SetSignTypeText()
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj and GameUIActivity.signlist then
    local txt = ""
    if 1 == GameUIActivity.signlist.type then
      txt = GameLoader:GetGameText("LC_MENU_SIGN_DES_1")
    elseif 2 == GameUIActivity.signlist.type then
      txt = GameLoader:GetGameText("LC_MENU_SIGN_DES_2")
    end
    flashObj:InvokeASCallback("_root", "SetSignTypeText", txt)
  end
end
function GameUIActivity:SetVipSignTypeText()
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    local txt = ""
    if AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_MYCARD == ext.GetBundleIdentifier() then
      txt = GameLoader:GetGameText("LC_MENU_REGISTRATION_REWARDS_MYCARD")
    else
      txt = GameLoader:GetGameText("LC_MENU_REGISTRATION_REWARDS")
    end
    DebugOut("SetVipSignTypeText", txt)
    flashObj:InvokeASCallback("_root", "SetVipSignTypeText", txt)
  end
end
GameUIActivity.currentOFABtab = 1
GameUIActivity.AllBenefitTime = 0
GameUIActivity.OpenFundData = nil
GameUIActivity.AllBenefitData = nil
GameUIActivity.isBuyFlag = 0
GameUIActivity.peopleNumber = 0
function GameUIActivity:ShowOpenfundAndAllbenefit(ofabTabid)
  GameUIActivity.currentOFABtab = ofabTabid
  if ofabTabid == 1 then
    if GameUIActivity.OpenFundData then
      GameUIActivity:ShowOpenFund()
    else
      GameUIActivity:GetOpenFundData()
    end
  elseif ofabTabid == 2 then
    if GameUIActivity.AllBenefitData and GameUIActivity.isBuyFlag == 0 then
      GameUIActivity:ShowAllBenefit()
    else
      GameUIActivity:GetAllBenefitData()
    end
  elseif ofabTabid == 3 then
    if GameUIActivity.LoginFundData then
      if GameUIActivity.LoginFundData.bln_buy ~= 1 and GameUIActivity:GetLoginFundPrice() == nil then
        GameVip:RequestProductIdentifierList()
        GameVip:ReqGiftData()
      end
      GameUIActivity:ShowLoginFund()
    else
      NetMessageMgr:SendMsg(NetAPIList.login_fund_req.Code, nil, function(msgType, content)
        if msgType == NetAPIList.login_fund_ack.Code then
          GameUIActivity.LoginFundData = content
          GameUIActivity:SortLoginFundData()
          if GameUIActivity.LoginFundData.bln_buy ~= 1 and GameUIActivity:GetLoginFundPrice() == nil then
            GameVip:RequestProductIdentifierList()
            GameVip:ReqGiftData()
          end
          GameUIActivity:ShowLoginFund()
          return true
        end
        return false
      end, true, nil)
    end
  end
end
function GameUIActivity:ShowLoginFund()
  local flashObj = GameUIActivity:GetFlashObject()
  if not flashObj or not GameUIActivity.LoginFundData then
    return
  end
  local creditText = GameUIActivity:GetLoginFundPrice()
  if GameUIActivity.LoginFundData.bln_buy ~= 1 and creditText == nil then
    creditText = "???"
    GameUIActivity.tryTimes = 0
    GameWaiting:ShowLoadingScreen()
    GameTimer:Add(function()
      if GameUIActivity:GetLoginFundPrice() == nil then
        GameUIActivity.tryTimes = GameUIActivity.tryTimes + 1
        if GameUIActivity.tryTimes > 5 then
          GameWaiting:HideLoadingScreen()
          return false
        end
        return 500
      end
      flashObj:InvokeASCallback("_root", "SetLoginFundButtonText", GameUIActivity:GetLoginFundPrice())
      GameWaiting:HideLoadingScreen()
      return false
    end, 1000)
  end
  local loginDay = GameUIActivity.LoginFundData.acc_login_days
  local isBuy = GameUIActivity.LoginFundData.bln_buy == 1
  local awardsCount = #GameUIActivity.LoginFundData.reward_list
  flashObj:InvokeASCallback("_root", "InitLoginFundInfo", GameUIActivity.currentOFABtab, loginDay, isBuy, awardsCount, creditText)
end
function GameUIActivity:GetOpenFundData(...)
  local number = 0
  local param = {
    num = GameUIActivity.peopleNumber,
    flag = GameUIActivity.isBuyFlag
  }
  DebugOut("param")
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.open_server_fund_req.Code, param, GameUIActivity.GetOpenFundDataCallback, true, nil)
end
function GameUIActivity:GetAllBenefitData(...)
  local number = 0
  local param = {
    num = GameUIActivity.peopleNumber,
    flag = GameUIActivity.isBuyFlag
  }
  DebugOut("param")
  DebugTable(param)
  NetMessageMgr:SendMsg(NetAPIList.all_welfare_req.Code, param, GameUIActivity.GetAllBenefitDataCallback, true, nil)
end
function GameUIActivity:ShowOpenFund()
  table.sort(GameUIActivity.OpenFundData.info, GameUIActivity.SortOpenFunDataOfLevel)
  local flashObj = GameUIActivity:GetFlashObject()
  if not flashObj then
    return
  end
  local vipText = string.format(GameLoader:GetGameText("LC_MENU_WELFAREE_VIP_LEVEL"), tostring(GameUIActivity.OpenFundData.vip))
  local chargedAmountText = string.format(GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_AD"), GameUIActivity.OpenFundData.charged_amount)
  local acc_credit = GameUIActivity.OpenFundData.acc_credit * 100
  local credit = GameUIActivity.OpenFundData.credit
  local get_credit = GameUIActivity.OpenFundData.acc_credit * credit
  local creditText = GameLoader:GetGameText("LC_MENU_WELFARE_SERVER_FUND_INFO")
  creditText = string.gsub(creditText, "<number1>", "<font color='#FFC600'>" .. tostring(credit) .. "</font>")
  creditText = string.gsub(creditText, "<number2>", "<font color='#FFC600'>" .. tostring(get_credit) .. "</font>")
  DebugOut("acc_credit:" .. acc_credit .. " creditText:" .. creditText .. " get_credit:" .. get_credit .. " vipText:" .. vipText .. " chargedAmountText:" .. chargedAmountText)
  flashObj:InvokeASCallback("_root", "InitOpenFundLeftPalantInfo", GameUIActivity.OpenFundData.does_charged, acc_credit, creditText, get_credit, vipText, chargedAmountText, credit)
  flashObj:InvokeASCallback("_root", "InitOpenFundAndAllBenefitData", GameUIActivity.currentOFABtab, #GameUIActivity.OpenFundData.info)
end
function GameUIActivity:ShowAllBenefit()
  table.sort(GameUIActivity.AllBenefitData.info, GameUIActivity.SortAllBenefitDataOfNum)
  if GameUIActivity.AllBenefitData.amount > 10000 then
    GameUIActivity.AllBenefitData.amount = 9999
  end
  DebugOut("ShowAllBenefit:" .. GameUIActivity.AllBenefitData.amount .. " " .. GameUIActivity.AllBenefitData.time)
  local flashObj = GameUIActivity:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "InitAllBenefitLeftPalantNumber", GameUIActivity.AllBenefitData.amount, GameUIActivity.OpenFundData.does_charged)
  flashObj:InvokeASCallback("_root", "InitOpenFundAndAllBenefitData", GameUIActivity.currentOFABtab, #GameUIActivity.AllBenefitData.info)
  local timeText = ""
  if GameUIActivity.AllBenefitData.time == 0 then
    timeText = GameLoader:GetGameText("LC_MENU_ACTIVITY_OVER")
  else
    timeText = GameUtils:formatTimeString(GameUIActivity.AllBenefitData.time)
  end
  flashObj:InvokeASCallback("_root", "SetAllBenefitTime", timeText)
end
function GameUIActivity:SetOpenFundData(content)
  GameUIActivity.OpenFundData = content
  table.sort(GameUIActivity.OpenFundData.info, GameUIActivity.SortOpenFunDataOfLevel)
  GameUIActivity:ShowOpenFund()
end
function GameUIActivity:SetAllBenefitData(content)
  GameUIActivity.AllBenefitData = content
  table.sort(GameUIActivity.AllBenefitData.info, GameUIActivity.SortAllBenefitDataOfNum)
  GameUIActivity:ShowAllBenefit()
end
function GameUIActivity.SortOpenFunDataOfLevel(v1, v2)
  if v1 and v2 then
    if v1.flag == v2.flag then
      return v1.level < v2.level
    else
      return v1.flag > v2.flag
    end
  end
  return false
end
function GameUIActivity.SortAllBenefitDataOfNum(v1, v2)
  if v1 and v2 then
    if v1.flag == v2.flag then
      return v1.num < v2.num
    else
      return v1.flag > v2.flag
    end
  end
  return false
end
function GameUIActivity:UpdateOpenPageData(itemId)
  if GameUIActivity.currentOFABtab == 1 then
    GameUIActivity:UpdateOpenFundItem(itemId)
  elseif GameUIActivity.currentOFABtab == 2 then
    GameUIActivity:UpdateAllBenefitItem(itemId)
  elseif GameUIActivity.currentOFABtab == 3 then
    GameUIActivity:UpdateLoginFundItem(itemId)
  end
end
function GameUIActivity:UpdateOpenFundItem(itemId)
  local curentItem = GameUIActivity.OpenFundData.info[itemId]
  if not curentItem then
    return
  end
  local contentText = ""
  contentText = contentText .. tostring(curentItem.credit) .. GameLoader:GetGameText("LC_MENU_WELFAREE_CREDIT") .. "\001"
  contentText = contentText .. string.format(GameLoader:GetGameText("LC_MENU_WELFARE_RECEIVE_INFO_1"), tostring(curentItem.level)) .. "\001"
  contentText = contentText .. curentItem.flag .. "\001"
  local buttonText = ""
  if curentItem.flag == 1 then
    buttonText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  else
    buttonText = GameLoader:GetGameText("LC_MENU_CLAIM_BUTTON")
  end
  DebugOut(" buttonText " .. buttonText)
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateOpenPageData", itemId, GameUIActivity.currentOFABtab, contentText, buttonText)
  end
end
function GameUIActivity:ShowLoginFundItem(item_id, index)
  if GameUIActivity.LoginFundData == nil then
    return
  end
  DebugOut("ShowLoginFundItem_1 " .. tostring(item_id) .. " " .. tostring(index))
  DebugTable(GameUIActivity.LoginFundData)
  local curentItem = GameUIActivity.LoginFundData.reward_list[item_id]
  if not curentItem or index > #curentItem.awards then
    return
  end
  local item = curentItem.awards[index]
  DebugOut("GameUIActivity_showItemDetil")
  DebugTable(item)
  GameUIActivity:showItemDetil(item, item.item_type)
end
function GameUIActivity:showItemDetil(item_, item_type)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  ItemBox:ShowGameItem(item, hide_callback)
end
function GameUIActivity:UpdateLoginFundItem(itemId)
  if GameUIActivity.LoginFundData == nil then
    return
  end
  local curentItem = GameUIActivity.LoginFundData.reward_list[itemId]
  if not curentItem then
    return
  end
  local contentText = ""
  for key, value in ipairs(curentItem.awards) do
    contentText = contentText .. GameHelper:GetCommonIconFrame(value) .. "\001"
    contentText = contentText .. GameHelper:GetAwardCount(value.item_type, value.number, value.no) .. "\001"
  end
  local titleTxt = string.gsub(GameLoader:GetGameText("LC_MENU_CUM_LOGIN_DAYS_TASK_CHAR"), "<DAYS>", tostring(curentItem.days))
  local receive = GameLoader:GetGameText("LC_MENU_CUM_LOGIN_DAYS_TASK_REWARD_CLAIM_BTN1")
  local received = GameLoader:GetGameText("LC_MENU_CUM_LOGIN_DAYS_TASK_REWARD_CLAIM_BTN2")
  local ButtonTxt = receive
  if curentItem.status == 3 then
    ButtonTxt = received
  end
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateLoginFundData", itemId, contentText, titleTxt, curentItem.status, ButtonTxt)
  end
end
function GameUIActivity:UpdateAllBenefitItem(itemId)
  local curentItem = GameUIActivity.AllBenefitData.info[itemId]
  if not curentItem then
    return
  end
  local contentText = ""
  contentText = contentText .. string.format(GameLoader:GetGameText("LC_MENU_WELFARE_RECEIVE_INFO_2"), curentItem.num) .. "\001"
  contentText = contentText .. tostring(curentItem.flag) .. "\001"
  contentText = contentText .. #curentItem.award .. "\001"
  for key, value in ipairs(curentItem.award) do
    local awardIcon = GameHelper:GetCommonIconFrame(value)
    local awardText = GameHelper:GetAwardText(value.item_type, value.number, value.no)
    contentText = contentText .. awardIcon .. "\001"
    contentText = contentText .. awardText .. "\001"
  end
  local buttonText = ""
  if curentItem.flag == 1 then
    buttonText = GameLoader:GetGameText("LC_MENU_CLAIMED_BUTTON")
  elseif curentItem.flag == 4 then
    buttonText = GameLoader:GetGameText("LC_MENU_ACTIVITY_OVER")
  else
    buttonText = GameLoader:GetGameText("LC_MENU_CLAIM_BUTTON")
  end
  DebugOut(contentText)
  DebugOut(" buttonText " .. buttonText)
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "UpdateOpenPageData", itemId, GameUIActivity.currentOFABtab, contentText, buttonText)
  end
end
function GameUIActivity:BuyOpenfund()
  GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
  GameUIMessageDialog:SetYesButton(function()
    NetMessageMgr:SendMsg(NetAPIList.charge_open_server_fund_req.Code, nil, GameUIActivity.BuyOpenfundCallback, true, nil)
  end)
  GameUIMessageDialog:SetNoButton(function()
  end)
  local textTitle = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
  local textInfo = GameLoader:GetGameText("LC_MENU_WELFARE_BUY_SERVER_FUND_ALERT")
  GameUIMessageDialog:Display(textTitle, textInfo)
end
function GameUIActivity.BuyOpenfundCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.charge_open_server_fund_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    if content.code == 0 then
      GameUIActivity.isBuyFlag = 1
      GameUIActivity.peopleNumber = GameUIActivity.OpenFundData.charged_amount
      GameUIActivity.OpenFundData = nil
      GameUIActivity:ShowOpenfundAndAllbenefit(GameUIActivity.currentOFABtab)
    end
    return true
  end
  return false
end
function GameUIActivity:GetOpenfunAward(itemId)
  local currentItem = GameUIActivity.OpenFundData.info[itemId]
  DebugOut("currentItem:")
  DebugTable(currentItem)
  if currentItem then
    local parm = {
      level = currentItem.level
    }
    DebugTable(parm)
    NetMessageMgr:SendMsg(NetAPIList.open_server_fund_award_req.Code, parm, GameUIActivity.GetOpenfunAwardCallback, true, nil)
  end
end
function GameUIActivity.GetOpenfunAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.open_server_fund_award_req.Code then
    if content.code == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_WELFARE_RECIEVE_SUCCESS_ALERT"))
      GameUIActivity.OpenFundData = nil
      GameUIActivity:ShowOpenfundAndAllbenefit(GameUIActivity.currentOFABtab)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    GameUIActivity.OpenFundData = nil
    GameUIActivity:ShowOpenfundAndAllbenefit(GameUIActivity.currentOFABtab)
    return true
  end
  return false
end
function GameUIActivity:GetLoginFundAward(itemId)
  local currentItem = GameUIActivity.LoginFundData.reward_list[itemId]
  DebugOut("GetLoginFundAward currentItem: " .. tostring(itemId))
  DebugTable(currentItem)
  if currentItem then
    local parm = {
      days = currentItem.days
    }
    DebugTable(parm)
    NetMessageMgr:SendMsg(NetAPIList.login_fund_reward_req.Code, parm, GameUIActivity.GetLoginFundAwardCallback, true, nil)
  end
end
function GameUIActivity.GetLoginFundAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.login_fund_reward_req.Code then
    if content.code == 0 then
      GameUIActivity.LoginFundData = nil
      GameUIActivity:ShowOpenfundAndAllbenefit(GameUIActivity.currentOFABtab)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIActivity:GetAllBenefitAward(itemId)
  local currentItem = GameUIActivity.AllBenefitData.info[itemId]
  DebugOut("currentItem:")
  DebugTable(currentItem)
  if currentItem then
    local parm = {
      amount = currentItem.num
    }
    DebugTable(parm)
    NetMessageMgr:SendMsg(NetAPIList.all_welfare_award_req.Code, parm, GameUIActivity.GetAllBenefitAwardCallback, true, nil)
  end
end
function GameUIActivity.GetAllBenefitAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.all_welfare_award_req.Code then
    if content.code == 0 then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_WELFARE_RECIEVE_SUCCESS_ALERT"))
      GameUIActivity.AllBenefitData = nil
      GameUIActivity:ShowOpenfundAndAllbenefit(GameUIActivity.currentOFABtab)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  return false
end
function GameUIActivity.GetOpenFundDataCallback(msgType, content)
  DebugOut("GetOpenFundDataCallback:" .. msgType)
  DebugTable(content)
  DebugOut(NetAPIList.open_server_fund_ack.Code)
  if msgType == NetAPIList.open_server_fund_ack.Code then
    GameUIActivity:SetOpenFundData(content)
    return true
  elseif msgType == NetAPIList.open_server_fund_ack.Code and content and content.api == NetAPIList.open_server_fund_req.Code then
    return true
  end
  return false
end
function GameUIActivity.GetAllBenefitDataCallback(msgType, content)
  DebugOut("GetAllBenefitDataCallback:" .. msgType)
  DebugTable(content)
  DebugOut(NetAPIList.all_welfare_ack.Code)
  if msgType == NetAPIList.all_welfare_ack.Code then
    GameUIActivity:SetAllBenefitData(content)
    return true
  elseif msgType == NetAPIList.all_welfare_ack.Code and content and content.api == NetAPIList.all_welfare_req.Code then
    return true
  end
  return false
end
function GameUIActivity:ChangeAllBenefitData()
  if GameUIActivity.AllBenefitData then
    for key, value in ipairs(GameUIActivity.AllBenefitData.info) do
      value.flag = 4
    end
    table.sort(GameUIActivity.AllBenefitData.info, GameUIActivity.SortAllBenefitDataOfNum)
    if GameUIActivity.currentOFABtab == 2 then
      GameUIActivity:ShowAllBenefit()
    end
  end
end
function GameUIActivity:UpdateAllBenefitTime(dt)
  GameUIActivity.AllBenefitTime = GameUIActivity.AllBenefitTime + dt
  if GameUIActivity.AllBenefitTime >= 1000 then
    GameUIActivity.AllBenefitTime = 0
    if GameUIActivity.AllBenefitData and 0 < GameUIActivity.AllBenefitData.time then
      GameUIActivity.AllBenefitData.time = GameUIActivity.AllBenefitData.time - 1
      if 0 <= GameUIActivity.AllBenefitData.time then
        local timeText = ""
        if GameUIActivity.AllBenefitData.time == 0 then
          GameUIActivity:ChangeAllBenefitData()
        else
          timeText = GameUtils:formatTimeString(GameUIActivity.AllBenefitData.time)
          local flashObj = GameUIActivity:GetFlashObject()
          if flashObj then
            flashObj:InvokeASCallback("_root", "SetAllBenefitTime", timeText)
          end
        end
      else
        GameUIActivity.AllBenefitData.time = 0
      end
    end
  end
end
function GameUIActivity:clearLevelList()
  local flashObj = GameUIActivity:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "clearLevelList", timeText)
  end
end
