local GameStateAccountSelect = GameStateManager.GameStateAccountSelect
local GameStateLoading = GameStateManager.GameStateLoading
local GameAccountSelector = LuaObjectManager:GetLuaObject("GameAccountSelector")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIMessageDialog = LuaObjectManager:GetLuaObject("GameUIMessageDialog")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameSetting = LuaObjectManager:GetLuaObject("GameSetting")
GameAccountSelector.tryLoginFB = false
GameAccountSelector.fbLoginType = 0
GameAccountSelector.mFBRelogin = false
GameAccountSelector.mCurAccountIsFBAndInvalid = false
local serverHasRoleList = {}
local currentServerShowingRolesIndex = 0
local AccountInfoCache = {}
local currentRequestPassport, selectedAccountAndRole, successPassportString
GameAccountSelector.isRegistByDelegate = false
function GameAccountSelector:IsShowText()
  GameUtils:printByAndroid("---IsShowText ---- GetBundleIdentifier----" .. ext.GetBundleIdentifier())
  DebugOut("GameStateLoading:---IsShowText ---- GetBundleIdentifier--" .. ext.GetBundleIdentifier())
  self:GetFlashObject():InvokeASCallback("_root", "IsShowText", false)
  if GameUtils:IsChinese() then
    self:GetFlashObject():InvokeASCallback("_root", "IsShowText", true)
  end
end
function GameAccountSelector:OnAddToGameState()
  self:LoadFlashObject()
  GameAccountSelector:SetFindPasswordText()
  if GameUtils:GetLoginInfo() then
    if GameUtils:GetLoginInfo().ServerInfo == nil or GameUtils:GetLoginInfo().ServerInfo.domain_port == nil then
      GameUtils:SetLoginInfo(nil)
    else
      GameAccountSelector:UpdateLoginInfo()
    end
  end
  GameAccountSelector:IsShowText()
  GameAccountSelector.isShowSelector = false
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    lang = GameSettingData.Save_Lang
    if string.find(lang, "ru") == 1 then
      lang = "ru"
    end
  end
  local agreementText = GameLoader:GetGameText("LC_MENU_USER_PROTOCOL_CHAR")
  DebugOut("agree text:" .. agreementText)
  self:GetFlashObject():InvokeASCallback("_root", "InitFlashUI", GameLoader:GetGameText("LC_MENU_LOGIN_TAP_FUN_ACCOUNT_BUTTON"), GameLoader:GetGameText("LC_MENU_LOGIN_REGISTER_TITLE"), lang, agreementText)
  self:GetFlashObject():InvokeASCallback("_root", "setPrivacytext", GameLoader:GetGameText("LC_MENU_PRIVACY_POLICY_CHAR"))
  if GameGlobalData.isNeedPrivacy == nil and not GameUtils:IsChinese() then
    ext.http.request({
      AutoUpdate.gameGateway .. "gamers/privacy_policy",
      param = {},
      mode = "notencrypt",
      callback = function(statusCode, content, errstr)
        DebugOut("errstr = ", errstr)
        DebugOut("statusCode = ", statusCode)
        if statusCode ~= 200 then
          DebugOut("check is need privacy error ", statusCode)
        elseif content then
          if content.error and content.error ~= "" then
            DebugOut("check is need privacy error ", statusCode)
          else
            GameGlobalData.isNeedPrivacy = content.show
            GameAccountSelector:setShowPrivacyBtn(content.show)
          end
        end
      end
    })
  end
end
function GameAccountSelector:InitFlashObject()
  local FlashObject = self:GetFlashObject()
  if FlashObject then
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_LOGIN_NEW_ACCOUNT", GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_LOGIN_EDIT_BUTTON", GameLoader:GetGameText("LC_MENU_LOGIN_EDIT_BUTTON"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_WORD_CHARACTER_INFO", GameLoader:GetGameText("LC_MENU_WORD_CHARACTER_INFO"))
    FlashObject:InvokeASCallback("_root", "setLocalText", "LC_MENU_PASSWORD_CHARACTER_INFO", GameLoader:GetGameText("LC_MENU_PASSWORD_CHARACTER_INFO"))
    FlashObject:InvokeASCallback("_root", "setShowPrivacyBtn", false)
    if not GameUtils:IsChinese() then
      FlashObject:InvokeASCallback("_root", "setShowPrivacyBtn", GameGlobalData.isNeedPrivacy == true)
    end
  end
end
function GameAccountSelector:setShowPrivacyBtn(isShow)
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "setShowPrivacyBtn", isShow)
  end
end
function GameAccountSelector:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeSelectPlayer)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeplayerOperation)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeAccountOperation)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeNewAccountOperation)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeLoginBox)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeRegisterBox)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeServerSelector)
  GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeAgreementBox)
  successPassportString = nil
end
function GameAccountSelector:OnFSCommand(cmd, arg)
  if cmd == "EnterGame" then
    if GameUtils:IsQihooApp() then
      if GameUtils.qihoo_token and GameUtils.qihoo_token ~= "-1" then
        local LoginInfo = GameUtils:GetLoginInfo()
        if LoginInfo and LoginInfo.AccountInfo then
          LoginInfo.AccountInfo.password = GameUtils.qihoo_token
          GameUtils:SetLoginInfo(LoginInfo)
          self:EnterGame()
        else
          IPlatformExt.Login()
        end
      else
        IPlatformExt.Login()
      end
    elseif IPlatformExt.getConfigValue("UseExtLogin") == "True" then
      if not GameUtils:GetLoginInfo() then
        GameStateAccountSelect.TryUpdateServer()
      else
        GameUtils.IPlatformExtLogin_Standard_Server()
      end
    else
      self:EnterGame()
    end
  elseif cmd == "facebook_btn_released" then
    DebugOut("facebook_btn_released")
    local LoginInfo = GameUtils:GetLoginInfo()
    self:SetLoginInfoForFB(LoginInfo)
    DebugOut("LoginInfoForFB")
    DebugTable(LoginInfo)
    local forBind = 1
    self:AccountFacebookLogin(forBind)
  elseif cmd == "login_facebbok_released" then
    ext.socialShare.CloseSession()
    Facebook:Reset()
    self:SetLoginInfoForFB(nil)
    DebugOut("LoginInfoForFB")
    DebugTable(LoginInfo)
    local forLogin = 2
    self:AccountFacebookLogin(forLogin)
  elseif cmd == "ShowLoginSelector" then
    GameAccountSelector.isRegistByDelegate = false
    if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
      if not GameUtils:GetLoginInfo() then
        GameStateAccountSelect.TryUpdateServer()
      elseif GameUtils.IPlatformExtLoginInfo_json == nil then
        GameUtils.IPlatformExtLogin_Standard_GateWay()
      else
        self:SetLoginInfoEditing(GameUtils:GetLoginInfo())
        GameAccountSelector:RequestServerList()
      end
    elseif GameUtils:IsQihooApp() then
      if GameUtils.qihoo_token and GameUtils.qihoo_token ~= "-1" then
        local LoginInfo = self.LoginTable[1]
        if LoginInfo then
          GameUtils:printByAndroid("--------ShowLoginSelector------->ShowServerSelector----")
          self:SetLoginInfoEditing(LoginInfo)
          local accountInfo = {}
          accountInfo.passport = self.LoginInfoEditing.AccountInfo.passport
          accountInfo.passport = GameUtils.qihoo_token
          accountInfo.accType = GameUtils.AccountType.qihoo
          GameAccountSelector:RequestServerList()
        else
          IPlatformExt.Login()
        end
      else
        IPlatformExt.Login()
      end
    else
      self:ShowLoginSelector()
    end
  elseif cmd == "UpdateAccountTable" then
    self:SyncAccountTable()
  elseif cmd == "UpdateLoginItem" then
    self:UpdateLoginItem(tonumber(arg))
  elseif cmd == "UpdateServerItem" then
    self:UpdateServerItem(tonumber(arg))
  elseif cmd == "UpdateGroupItem" then
    self:UpdateGroupItem(tonumber(arg))
  elseif cmd == "GroupSelect" then
    self:GroupSelect(tonumber(arg))
  elseif cmd == "AccountEdit" then
    GameAccountSelector.mCurAccountIsFBAndInvalid = false
    local LoginInfo = self.LoginTable[tonumber(arg)]
    if LoginInfo.AccountInfo and GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) then
      local function CheckFBAccountValidateCallback(content)
        GameWaiting:HideLoadingScreen()
        if content.is_valid then
        else
          DebugTable(content)
          GameAccountSelector.mCurAccountIsFBAndInvalid = true
        end
        self:LoginInfoEdit(tonumber(arg))
      end
      GameWaiting:ShowLoadingScreen()
      GameAccountSelector:CheckFBAccountValid(LoginInfo.AccountInfo.passport, LoginInfo.AccountInfo.password, CheckFBAccountValidateCallback)
    else
      self:LoginInfoEdit(tonumber(arg))
    end
  elseif cmd == "AccountLogout" then
    local function OnInfoOver()
      DebugOut("Logout!!!!")
      local duplicate = GameAccountSelector:GetDuplicateLoginInfo(GameAccountSelector.LoginInfoEditing)
      DebugOutPutTable(duplicate, "duplicate")
      if duplicate and duplicate.loginInfo and GameUtils:IsFacebookAccount(duplicate.loginInfo.AccountInfo) then
        ext.socialShare.CloseSession()
        Facebook:Reset()
        DebugOut("Logout facebook!!!!")
      end
      if duplicate and duplicate.loginInfo and duplicate.loginInfo.AccountInfo then
        GameUtils:RemoveAccountLocalData(duplicate.loginInfo.AccountInfo.passport)
        DebugOut("remove duplicate account.")
      end
      if GameUtils:IsFacebookAccount(GameAccountSelector.LoginInfoEditing.AccountInfo) then
        ext.socialShare.CloseSession()
        Facebook:Reset()
        DebugOut("Logout facebook!!!!")
      end
      GameUtils:RemoveAccountLocalData(GameAccountSelector.LoginInfoEditing.AccountInfo.passport)
      local LoginInfo = GameUtils:GetLoginInfo()
      if LoginInfo and LoginInfo.AccountInfo and LoginInfo.AccountInfo.passport == GameAccountSelector.LoginInfoEditing.AccountInfo.passport then
        LoginInfo.AccountInfo.password = nil
      end
      if GameUtils:IsFacebookAccount(GameAccountSelector.LoginInfoEditing.AccountInfo) then
        GameUtils:SetLoginInfo(nil)
      else
        GameAccountSelector:UpdateLoginInfo()
      end
      GameStateAccountSelect.TryUpdateServer()
    end
    local TitleInfo = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    local ContentInfo = GameLoader:GetGameText("LC_MENU_LOGIN_LOGOUT_ALERT")
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_YesNo, TitleInfo, ContentInfo, OnInfoOver, nil)
  elseif cmd == "AccountSelectServer" then
    assert(self.LoginInfoEditing.AccountInfo)
  elseif cmd == "ShowServerSelector" then
    if GameAccountSelector.mCurAccountIsFBAndInvalid then
      GameUIGlobalScreen:ShowMessageBox(1, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), GameLoader:GetGameText("LC_ALERT_facebook_account_timeout"), nil)
    else
      assert(self.LoginInfoEditing)
      DebugOutPutTable(self.LoginInfoEditing, "self.LoginInfoEditing")
      GameAccountSelector:RequestServerList()
      GameAccountSelector.selectedGroupItem = 1
    end
  elseif cmd == "AccountSelect" then
    self:AccountSelect(tonumber(arg))
  elseif cmd == "ServerSelect" then
    self:ServerSelect(tonumber(arg))
  elseif cmd == "AccountLogin" then
    if not self.PassportString or self.PassportString == "" then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CAN_NOT_EMPTY_CHAR"))
      return
    end
    if not self.PasswordString or self.PasswordString == "" then
      GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CAN_NOT_EMPTY_CHAR"))
      return
    end
    local AccountInfo = {}
    AccountInfo.passport = self.PassportString
    AccountInfo.password = self.PasswordString
    GameUtils:Tap4funAccountValidate(AccountInfo, self.AccountValidateCallback)
  elseif cmd == "AccountRegister" then
    self:AccountRegister()
  elseif cmd == "AccountAddOperation" then
    self:ShowAccountAddOperation()
  elseif cmd == "ShowAccountLogin" then
    self:ShowAccountLogin("", "")
  elseif cmd == "ShowAccountRegister" then
    self:ShowAccountRegister()
  elseif cmd == "oldPassword" then
    self.oldPassword = arg
  elseif cmd == "newPasswordString" then
    self.newPasswordString = arg
  elseif cmd == "newPasswordConfirmString" then
    self.newPasswordConfirmString = arg
  elseif cmd == "PasswordChange" then
    DebugOut(self.oldPassword)
    DebugOut(self.newPasswordString)
    DebugOut(self.newPasswordConfirmString)
    local isValid = self:CheckPasswordModifyValidation(self.oldPassword, self.newPasswordString, self.newPasswordConfirmString)
    if isValid then
      GameUtils:Tap4funModifyPassword(self.LoginInfoEditing.AccountInfo, self.newPasswordString, GameAccountSelector.passwordModifyCallback)
    end
  elseif cmd == "PassportString" then
    self.PassportString = arg
    if self.PassportString then
      self.PassportString = LuaUtils:string_trim(self.PassportString)
    end
  elseif cmd == "PasswordString" then
    self.PasswordString = arg
  elseif cmd == "PasswordConfirmString" then
    self.PasswordConfirmString = arg
  elseif cmd == "IsAgree" then
    self.IsAgree = tonumber(arg)
  elseif cmd == "DebugCommand" then
    if DebugCommand then
      DebugCommand:Excute(arg)
    end
  elseif cmd == "removeCloseSelectPlayCallback" then
    self.isShowSelector = false
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeSelectPlayer)
  elseif cmd == "closePlayOperation" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closePlayOperation)
  elseif cmd == "closeaccountOperation" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeAccountOperation)
  elseif cmd == "close_newAccountOperation" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeNewAccountOperation)
  elseif cmd == "close_loginBox" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeLoginBox)
  elseif cmd == "close_registerBox" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeRegisterBox)
  elseif cmd == "close_serverSelector" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeServerSelector)
  elseif cmd == "close_agreement_box" then
    GameAndroidBackManager:RemoveCallback(GameAccountSelector.closeAgreementBox)
  elseif cmd == "showAgreement" then
    GameAndroidBackManager:AddCallback(GameAccountSelector.closeAgreementBox)
  elseif cmd == "Press01" then
    GameCheat:CheatInput("0")
  elseif cmd == "Press02" then
    GameCheat:CheatInput("1")
  elseif cmd == "showRecommendServer" then
    GameAccountSelector:showRecommendServer(tonumber(arg))
  elseif cmd == "onUpdateServerHasRoleItem" then
    self:setRoleHasListItem(tonumber(arg))
  elseif cmd == "closeServerHasRoleList" then
    currentServerShowingRolesIndex = 0
  elseif cmd == "ServerHasRoleSelect" then
    DebugOut(arg)
    self:ServerHasRoleSelect(tonumber(arg))
  elseif cmd == "find_password" then
    GameAccountSelector:FindPassword(arg)
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameAccountSelector:SetActiveWindow(WindowName)
  self:GetFlashObject():InvokeASCallback("_root", "setActiveWindow", WindowName)
end
function GameAccountSelector:AccountLogin()
  DebugOut("passport:" .. self.PassportString)
  DebugOut("password:" .. self.PasswordString)
  local AccountInfo = {}
  AccountInfo.passport = self.PassportString
  AccountInfo.password = self.PasswordString
  if self:CheckAccountFormat(AccountInfo) then
    GameWaiting:ShowLoadingScreen()
    GameUtils:Tap4funAccountValidate(AccountInfo, self.AccountValidateCallback)
  end
end
function GameAccountSelector:AccountRegister()
  if self.IsAgree == 0 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PROTOCOL_CONFIRM_ALERT_CHAR"))
    return
  end
  if not self.PassportString or self.PassportString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not self.PasswordString or self.PasswordString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CAN_NOT_EMPTY_CHAR"))
    return
  end
  if not self.PasswordConfirmString or self.PasswordConfirmString == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_EMPTY_CHAR"))
    return
  end
  if self.PasswordConfirmString ~= self.PasswordString then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_ERRO_CHAR"))
    return
  end
  if GameUtils:CheckSpaceCharFromString(self.PasswordString) then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_illegal_character_password"))
    return
  end
  local AccountInfo = {}
  AccountInfo.passport = self.PassportString
  AccountInfo.password = self.PasswordString
  if self:CheckAccountFormat(AccountInfo) then
    GameUtils:Tap4funAccountRegister(AccountInfo, self.AccountRegisterCallback)
  end
end
function GameAccountSelector:CheckAccountFormat(tap4fun_account)
  local ret = true
  length = string.len(tap4fun_account.password)
  if length < 6 or length > 12 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_LENGTH_ERRO_CHAR"))
    ret = false
  end
  return ret
end
function GameAccountSelector:CheckPasswordModifyValidation(oldPassword, newPassword, newPasswordConfirm)
  DebugOut("GameAccountSelector:CheckPasswordModifyValidation")
  if self.LoginInfoEditing.AccountInfo.password ~= oldPassword then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CHANGE_WRONG_OLD_PASSWORD_HINT"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", true, false, false)
    return false
  end
  if newPassword ~= newPasswordConfirm then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CONFIRM_ERRO_CHAR"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", false, true, true)
    return false
  end
  if not newPassword or newPassword == "" then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_CAN_NOT_EMPTY_CHAR"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", false, true, true)
    return false
  end
  if GameUtils:CheckSpaceCharFromString(newPassword) then
    GameTip:Show(GameLoader:GetGameText("LC_ALERT_illegal_character_password"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", false, true, true)
    return false
  end
  if newPassword == oldPassword then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_CHANGE_PASSWORD_ERROR_HINT"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", false, true, true)
    return false
  end
  local length = string.len(newPassword)
  if length < 6 or length > 12 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_PASSWORD_LENGTH_ERRO_CHAR"))
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_clearModifyPasswordInputBox", false, true, true)
    return false
  end
  return true
end
function GameAccountSelector:UpdateLoginInfo()
  DebugOut("GameAccountSelector:UpdateLoginInfo")
  local LoginInfo = GameUtils:GetLoginInfo()
  selectedAccountAndRole = LuaUtils:table_rcopy(LoginInfo)
  DebugTable(LoginInfo)
  GameUtils:DebugOutTableInAndroid(LoginInfo)
  if LoginInfo then
    local DataTable = {}
    local IndexData = 0
    IndexData = IndexData + 1
    if LoginInfo.AccountInfo and LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest then
      if GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) or tonumber(LoginInfo.AccountInfo.accType) == 3 then
        DataTable[IndexData] = LoginInfo.AccountInfo.displayName
      else
        DataTable[IndexData] = LoginInfo.AccountInfo.passport
      end
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT_BUTTON")
    end
    IndexData = IndexData + 1
    if LoginInfo.PlayerInfo then
      DataTable[IndexData] = GameUtils:CutOutUsernameByLastDot(LoginInfo.PlayerInfo.name)
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ROLE_BUTTON")
    end
    IndexData = IndexData + 1
    if LoginInfo.ServerInfo then
      DataTable[IndexData] = LoginInfo.ServerInfo.name
    elseif LoginInfo.PlayerInfo then
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_SERVER_UNVALID_CHAR")
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_CHOOSE_SERVER_CHAR")
    end
    IndexData = IndexData + 1
    if GameUtils:IsQihooApp() then
      DataTable[IndexData] = "true"
    else
      DataTable[IndexData] = tostring(LoginInfo.ServerInfo ~= nil)
    end
    IndexData = IndexData + 1
    if not LoginInfo.AccountInfo and LoginInfo.PlayerInfo and not GameUtils:hasFBLogined() and self:fbFeatureEnabled() then
      DataTable[IndexData] = "true"
    else
      DataTable[IndexData] = "false"
    end
    IndexData = IndexData + 1
    if not LoginInfo.ServerInfo then
      DataTable[IndexData] = "none"
    elseif LoginInfo.ServerInfo and LoginInfo.ServerInfo.is_in_maintain then
      DataTable[IndexData] = "close"
    else
      DataTable[IndexData] = "open"
    end
    if not self.isRegistByDelegate then
      self:GetFlashObject():InvokeASCallback("_root", "setLoginInfo", table.concat(DataTable, "\001"))
    end
  end
  if DebugConfig.isEnterGameDirectly then
    DebugOut("isEnterGameDirectly", isEnterGameDirectly)
    self:EnterGame()
  end
end
function GameAccountSelector:ShowAccountAddOperation()
  self:SetLoginInfoEditing(nil)
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeNewAccountOperation)
  local enableFacebook = false
  local btnText = GameLoader:GetGameText("LC_MENU_LOGIN_FACEBOOK_LOGIN_TITLE")
  if self:fbFeatureEnabled() then
    enableFacebook = true
    btnText = GameLoader:GetGameText("LC_MENU_LOGIN_FACEBOOK_LOGIN_TITLE")
  end
  self:GetFlashObject():InvokeASCallback("_root", "showAccountAddOperation", enableFacebook, btnText)
end
function GameAccountSelector:ShowLoginSelector()
  if GameStateAccountSelect.mAccountData then
    if self.isShowSelector then
      return
    end
    self.isShowSelector = true
    self:SyncAccountTable()
    self:GetFlashObject():InvokeASCallback("_root", "showLoginSelector")
    GameAndroidBackManager:AddCallback(GameAccountSelector.closeSelectPlayer)
  else
    local function callback()
      GameStateAccountSelect.TryUpdateServer()
    end
    local info = GameLoader:GetGameText("LC_MENU_LOGIN_UPDATE_FROM_GATEWAY_FAILED_ALERT")
    GameTip:Show(info, 300, callback)
  end
end
function GameAccountSelector:JustShowTangoAccout()
  if AutoUpdate.isAndroidDevice then
    GameUtils:printByAndroid("GameAccountSelector:JustShowTangoAccout \229\188\128\229\167\139")
    GameUtils:DebugOutTableInAndroid(self.LoginTable)
  end
  if GameUtils:IsTangoAPP() then
    for k, v in pairs(self.LoginTable) do
      if v.AccountInfo and tonumber(v.AccountInfo.accType) == GameUtils.AccountType.tango then
        GameAccountSelector.ishaveTangAccount = true
      end
    end
    for i = #self.LoginTable, 1, -1 do
      if GameUtils.m_myProfile then
        if not self.LoginTable[i].AccountInfo or not self.LoginTable[i].AccountInfo.passport or self.LoginTable[i].AccountInfo.passport ~= GameUtils.m_myProfile.tangoID then
          table.remove(self.LoginTable, i)
        end
      elseif ishaveTangAccount and (not self.LoginTable[i].AccountInfo or not self.LoginTable[i].AccountInfo.passport or self.LoginTable[i].AccountInfo.accType ~= GameUtils.AccountType.tango) then
        table.remove(self.LoginTable, i)
      end
    end
  end
  if AutoUpdate.isAndroidDevice then
    GameUtils:printByAndroid("GameAccountSelector:JustShowTangoAccout \232\191\135\229\144\142")
    GameUtils:DebugOutTableInAndroid(self.LoginTable)
  end
end
function GameAccountSelector:HideLoginSelector()
  self:GetFlashObject():InvokeASCallback("_root", "hideLoginSelector")
end
function GameAccountSelector:SyncAccountTable()
  local accountNum = 0
  if GameUtils:IsLoginTangoAccount() then
    accountNum = #self.LoginTable
  elseif GameUtils:IsQihooApp() then
    accountNum = #self.LoginTable
  elseif IPlatformExt.getConfigValue("UseExtLogin") == "True" then
    accountNum = #self.LoginTable
  else
    accountNum = #self.LoginTable + 1
  end
  if not self.isRegistByDelegate then
    if self:GetFlashObject() == nil then
      self:LoadFlashObject()
    end
    self:GetFlashObject():InvokeASCallback("_root", "syncAccountTable", accountNum)
  end
end
function GameAccountSelector:UpdateLoginItem(indexAccount)
  local LoginInfo = self.LoginTable[indexAccount]
  local dataString = -1
  if LoginInfo then
    local DataTable = {}
    local IndexData = 0
    IndexData = IndexData + 1
    if LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest then
      if GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) then
        DataTable[IndexData] = LoginInfo.AccountInfo.displayName or GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT_BUTTON")
      else
        local accountName = LoginInfo.AccountInfo.passport
        local duplicateItem = GameAccountSelector:GetDuplicateLoginInfo(LoginInfo)
        if duplicateItem then
        end
        DataTable[IndexData] = accountName or GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT_BUTTON")
      end
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT_BUTTON")
    end
    IndexData = IndexData + 1
    if LoginInfo.PlayerInfo and LoginInfo.PlayerInfo.name then
      DataTable[IndexData] = GameUtils:CutOutUsernameByLastDot(LoginInfo.PlayerInfo.name)
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ROLE_BUTTON")
    end
    IndexData = IndexData + 1
    if LoginInfo.ServerInfo then
      DataTable[IndexData] = LoginInfo.ServerInfo.name
    elseif LoginInfo.PlayerInfo and LoginInfo.PlayerInfo.name then
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_SERVER_UNVALID_CHAR")
    else
      DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_LOGIN_CHOOSE_SERVER_CHAR")
    end
    IndexData = IndexData + 1
    if LoginInfo.ServerInfo then
      DataTable[IndexData] = "ShowBtnArrow"
    else
      DataTable[IndexData] = "notShow"
    end
    IndexData = IndexData + 1
    DataTable[IndexData] = GameLoader:GetGameText("LC_MENU_TAP_TO_SELECT")
    IndexData = IndexData + 1
    if not LoginInfo.ServerInfo then
      DataTable[IndexData] = "none"
    elseif LoginInfo.ServerInfo.is_in_maintain then
      DataTable[IndexData] = "close"
    else
      DataTable[IndexData] = "open"
    end
    dataString = table.concat(DataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setAccountData", indexAccount, dataString)
end
function GameAccountSelector:LoginInfoEdit(indexAccount)
  local LoginInfo = self.LoginTable[indexAccount]
  self:SetLoginInfoEditing(LoginInfo)
  DebugOut("LoginInfoEdit  " .. indexAccount)
  DebugTable(LoginInfo)
  if LoginInfo.AccountInfo and LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest then
    if LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest and not LoginInfo.AccountInfo.password then
      local info = GameLoader:GetGameText("LC_MENU_LOGIN_UNLOGIN_ALERT")
      local function callback()
        self:ShowAccountLogin(LoginInfo.AccountInfo.passport, "")
      end
      GameTip:Show(info, 3000, callback)
      return
    end
    local playername = ""
    GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ROLE_BUTTON")
    if LoginInfo.PlayerInfo and LoginInfo.PlayerInfo.name then
      playername = LoginInfo.PlayerInfo.name
    else
      playername = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ROLE_BUTTON")
    end
    local ServerName = ""
    if LoginInfo.ServerInfo then
      ServerName = LoginInfo.ServerInfo.name
    elseif LoginInfo.PlayerInfo and LoginInfo.PlayerInfo.name then
      ServerName = GameLoader:GetGameText("LC_MENU_LOGIN_SERVER_UNVALID_CHAR")
    else
      ServerName = GameLoader:GetGameText("LC_MENU_LOGIN_CHOOSE_SERVER_CHAR")
    end
    local displayName = LoginInfo.AccountInfo.passport
    local enabledPwdBtn = true
    if GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) or GameUtils:IsLoginTangoAccount() then
      enabledPwdBtn = false
      displayName = LoginInfo.AccountInfo.displayName
    end
    if LoginInfo.AccountInfo.accType == GameUtils.AccountType.guest then
      enabledPwdBtn = false
      displayName = GameLoader:GetGameText("LC_MENU_LOGIN_NEW_ACCOUNT_BUTTON")
    end
    local hideViewAfterClickServerBtn = true
    if GameAccountSelector.mCurAccountIsFBAndInvalid then
      hideViewAfterClickServerBtn = false
    end
    self:AccountEditOperation(displayName, playername, ServerName, enabledPwdBtn, hideViewAfterClickServerBtn)
  elseif LoginInfo.PlayerInfo then
    local ServerName = ""
    if LoginInfo.ServerInfo then
      ServerName = LoginInfo.ServerInfo.name
    else
      ServerName = GameLoader:GetGameText("LC_MENU_LOGIN_CHOOSE_SERVER_CHAR")
    end
    self:PlayerEditOperation(LoginInfo.PlayerInfo.name, ServerName)
  else
    GameAccountSelector:RequestServerList()
  end
end
function GameAccountSelector:getAccountPlayerTableWithCache(AccountInfo)
  local cacheTable = AccountInfoCache[AccountInfo.passport]
  currentRequestPassport = AccountInfo.passport
  GameUtils:RequestAccountPlayerTable(AccountInfo, self.AccountTableHasRoleCallback, self.AccountTableHasRoleError)
end
function GameAccountSelector:showRecommendServer(indexAccount)
  DebugOut("GameAccountSelector:showRecommendServer", indexAccount)
  local LoginInfo = self.LoginTable[indexAccount]
  self:SetLoginInfoEditing(LoginInfo)
  if currentServerShowingRolesIndex == indexAccount then
    DebugOut("same indexAccount", indexAccount)
    return
  end
  if LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest and not LoginInfo.AccountInfo.password then
    local info = GameLoader:GetGameText("LC_MENU_LOGIN_UNLOGIN_ALERT")
    local function callback()
      DebugOut("need relogin", LoginInfo.AccountInfo.passport)
      self:closeRecommendServer()
      self:ShowAccountLogin(LoginInfo.AccountInfo.passport, "")
    end
    GameTip:Show(info, 3000, callback)
    return
  end
  currentServerShowingRolesIndex = indexAccount
  self:getAccountPlayerTableWithCache(LoginInfo.AccountInfo)
end
function GameAccountSelector:setRoleHasListItem(index)
  DebugOut("GameAccountSelector:setRoleHasListItem", index)
  local roleItem = serverHasRoleList[index]
  local username = GameUtils:CutOutUsernameByLastDot(roleItem.role.name)
  local userlevel = GameLoader:GetGameText("LC_MENU_Level")
  if roleItem.role.level and roleItem.role.level > 0 then
    userlevel = userlevel .. roleItem.role.level
  else
    userlevel = userlevel .. " -"
  end
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_UpdateServerHasRoleItem", index, roleItem.server.name, username, userlevel, roleItem.selected, roleItem.server.is_in_maintain)
end
function GameAccountSelector:GenerateServerHasRoleList(roleContent)
  DebugOut("GameAccountSelector:GenerateServerHasRoleList")
  if not selectedAccountAndRole then
    DebugOut("has default login role")
    DebugTable(self.LoginTable)
    selectedAccountAndRole = self.LoginTable[1]
  else
    DebugOut("already has selectedAccountAndRole")
  end
  DebugOut("selectedAccountAndRole", currentServerShowingRolesIndex)
  DebugTable(selectedAccountAndRole)
  serverHasRoleList = roleContent
  for _, v in ipairs(roleContent) do
    if self.LoginInfoEditing.PlayerInfo and self.LoginInfoEditing.PlayerInfo.name == v.role.name then
      v.selected = true
    end
  end
  local SortCallback = function(a, b)
    if a.role.updated_at > b.role.updated_at then
      return true
    end
  end
  table.sort(serverHasRoleList, SortCallback)
  DebugOutPutTable(serverHasRoleList, "serverHasRoleList")
end
function GameAccountSelector:GenerateAccountTable()
  DebugOut("GameAccountSelector:GenerateAccountTable")
  self.LoginTable = {}
  self.DuplicateLoginTable = {}
  if GameStateAccountSelect.mAccountData and GameStateAccountSelect.mAccountData.last_logins then
    for _, v in ipairs(GameStateAccountSelect.mAccountData.last_logins) do
      local loginInfo = {}
      DebugOut("kkklllllklkkkkklllkkk")
      DebugTable(v)
      loginInfo.PlayerInfo = v.role
      loginInfo.ServerInfo = v.server
      loginInfo.RegistAppId = v.register_appid
      loginInfo.isCreateActor = v.change_name or 1
      local accountInfo = {
        accType = v.acctype
      }
      if v.acctype == GameUtils.AccountType.guest then
        accountInfo.passport = ""
        accountInfo.password = ""
      elseif v.role then
        local tmpAccount = GameUtils:GetAccountLocalData(v.accname, v.acctype)
        if tmpAccount then
          accountInfo = tmpAccount
        else
          accountInfo.passport = v.accname
        end
      else
        local tmpAccount = GameUtils:GetAccountLocalData(v.accname, v.acctype)
        if tmpAccount then
          accountInfo = tmpAccount
        else
          accountInfo.passport = v.accname
        end
      end
      loginInfo.AccountInfo = accountInfo
      if not loginInfo.PlayerInfo and accountInfo.updated_at then
        local playerInfo = {
          updated_at = accountInfo.updated_at
        }
        loginInfo.PlayerInfo = playerInfo
      end
      if IPlatformExt.getConfigValue("UseExtLogin") == "True" then
        if loginInfo.AccountInfo.accType and loginInfo.AccountInfo.accType == tonumber(IPlatformExt.getConfigValue("AccType")) then
          table.insert(self.LoginTable, loginInfo)
        end
      elseif GameUtils:IsQihooApp() then
        if loginInfo.AccountInfo.accType and loginInfo.AccountInfo.accType == GameUtils.AccountType.qihoo then
          GameUtils:printByAndroid("--------Add QihooAccount to LoginTable-----------")
          table.insert(self.LoginTable, loginInfo)
        end
      else
        table.insert(self.LoginTable, loginInfo)
      end
    end
  end
  if GameSettingData.PassportTable then
    for _, AccountLocalInfo in ipairs(GameSettingData.PassportTable) do
      local findlocal = false
      for _, LoginInfo in ipairs(self.LoginTable) do
        local localAccType = AccountLocalInfo.accType or GameUtils.AccountType.mail
        local loginAccType = LoginInfo.AccountInfo.accType or GameUtils.AccountType.mail
        if LoginInfo.AccountInfo and LoginInfo.AccountInfo.passport == AccountLocalInfo.passport and loginAccType == localAccType then
          if not LoginInfo.PlayerInfo and AccountLocalInfo.updated_at then
            local playerInfo = {
              updated_at = AccountLocalInfo.updated_at
            }
            LoginInfo.PlayerInfo = playerInfo
          end
          findlocal = true
          DebugOut("findlocal = true")
        end
      end
      if not findlocal then
        local LoginInfo = {
          AccountInfo = {}
        }
        LoginInfo.AccountInfo.passport = AccountLocalInfo.passport
        LoginInfo.AccountInfo.password = AccountLocalInfo.password
        LoginInfo.AccountInfo.accType = AccountLocalInfo.accType or GameUtils.AccountType.mail
        if AccountLocalInfo.updated_at then
          local playerInfo = {
            updated_at = AccountLocalInfo.updated_at
          }
          LoginInfo.PlayerInfo = playerInfo
        end
        DebugOut("insert local to table ")
        DebugTable(AccountLocalInfo)
        table.insert(self.LoginTable, LoginInfo)
      end
    end
  end
  local function SortCallback(a, b)
    if a.PlayerInfo and not b.PlayerInfo then
      return true
    elseif not a.PlayerInfo and b.PlayerInfo then
      return false
    elseif a.PlayerInfo and b.PlayerInfo then
      if successPassportString then
        if a.PlayerInfo.passport == successPassportString and b.PlayerInfo.passport ~= successPassportString then
          return true
        elseif a.PlayerInfo.passport ~= successPassportString and b.PlayerInfo.passport == successPassportString then
          return false
        end
      end
      return a.PlayerInfo.updated_at > b.PlayerInfo.updated_at
    end
    return false
  end
  local newLoginTable, duplicateTable = GameAccountSelector:MergeSameAccount(self.LoginTable)
  self.LoginTable = newLoginTable
  self.DuplicateLoginTable = duplicateTable
  table.sort(self.LoginTable, SortCallback)
  DebugTable(self.LoginTable)
  self:SyncAccountTable()
  if GameSettingData.LastLogin then
    DebugOut("GameSettingData.LastLogin")
    DebugTable(GameSettingData.LastLogin)
    for _, LoginInfo in ipairs(self.LoginTable) do
      DebugTable(LoginInfo.PlayerInfo)
      DebugTable(LoginInfo.AccountInfo)
      if LoginInfo.PlayerInfo and GameSettingData.LastLogin.PlayerInfo and GameSettingData.LastLogin.PlayerInfo.name == LoginInfo.PlayerInfo.name and LoginInfo.AccountInfo and GameSettingData.LastLogin.AccountInfo and LoginInfo.AccountInfo.passport == GameSettingData.LastLogin.AccountInfo.passport then
        DebugOut("Find last logint!!")
        GameSettingData.LastLogin = LoginInfo
        GameUtils:SaveSettingData()
        GameUtils:SetLoginInfo(LuaUtils:table_rcopy(LoginInfo))
        DelayLoad()
        GameAccountSelector:UpdateLoginInfo()
        break
      end
    end
  end
end
function GameAccountSelector:GetDuplicateLoginInfo(loginInfo)
  if loginInfo and self.DuplicateLoginTable then
    for k, v in pairs(self.DuplicateLoginTable) do
      if v.loginInfo.PlayerInfo and loginInfo.PlayerInfo and v.loginInfo.PlayerInfo.slogic_id == loginInfo.PlayerInfo.slogic_id and v.loginInfo.PlayerInfo.name == loginInfo.PlayerInfo.name then
        return v
      end
    end
  end
  return nil
end
function GameAccountSelector:MergeSameAccount(list)
  if nil == list then
    return nil, nil
  end
  DebugOutPutTable(GameSettingData.PassportTable, "GameSettingData.PassportTable")
  DebugOutPutTable(list, "org list")
  local newList = {}
  local sameList = {}
  if #list > 0 then
    for k = 1, #list do
      local v = list[k]
      local isDuplicate = false
      local itemInNewList, keyInNewList
      for k2, v2 in pairs(newList) do
        if v.PlayerInfo and v2.PlayerInfo and v.PlayerInfo.slogic_id == v2.PlayerInfo.slogic_id and v.PlayerInfo.name == v2.PlayerInfo.name and v.AccountInfo and v.AccountInfo.password and v2.AccountInfo and v2.AccountInfo.password then
          itemInNewList = v2
          keyInNewList = k2
          isDuplicate = true
          break
        end
      end
      if isDuplicate and GameSettingData.PassportTable then
        local find = false
        for k3, v3 in pairs(GameSettingData.PassportTable) do
          if v3.accType == GameUtils.AccountType.facebook then
            if itemInNewList.AccountInfo.passport == v3.passport then
              local item = {}
              item.loginInfo = v
              item.displayName = v3.displayName
              table.insert(sameList, item)
              DebugOut("insert to sameList = ", k, k3)
              DebugTable(item)
              find = true
              break
            elseif v.AccountInfo.passport == v3.passport then
              local sameItem = LuaUtils:table_rcopy(itemInNewList)
              local displayName = v.AccountInfo.displayName
              itemInNewList = v
              itemInNewList.displayName = displayName
              newList[keyInNewList] = v
              local item = {}
              item.loginInfo = sameItem
              item.displayName = v3.displayName
              table.insert(sameList, item)
              DebugOut("insert to sameList222 = ", k, k3)
              DebugTable(item)
              find = true
              break
            end
          end
        end
        if not find then
          DebugOut("insert to newList222 =")
          DebugTable(v)
          table.insert(newList, v)
        end
      else
        DebugOut("insert to newList111 = ", k)
        DebugTable(v)
        table.insert(newList, v)
      end
    end
  end
  DebugOutPutTable(newList, "newList")
  DebugOutPutTable(sameList, "DuplicateLoginTable")
  return newList, sameList
end
function GameAccountSelector:UpdateServerItem(indexServer)
  local DataString = -1
  local ServerInfo, RoleInfo
  if indexServer > 0 and indexServer <= #self.GServerList then
    ServerInfo = self.GServerList[indexServer].server
    RoleInfo = self.GServerList[indexServer].role
  end
  if ServerInfo then
    local DataTable = {}
    table.insert(DataTable, ServerInfo.name)
    table.insert(DataTable, "normal")
    table.insert(DataTable, tostring(RoleInfo ~= nil))
    local status = ServerInfo.is_in_maintain
    if status then
      table.insert(DataTable, "close")
    else
      table.insert(DataTable, "open")
    end
    DataString = table.concat(DataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setServerData", indexServer, DataString)
end
function GameAccountSelector:UpdateGroupItem(indexGroup)
  local DataString = -1
  local GroupInfo
  if indexGroup > 0 and indexGroup <= #self.GroupDetailList then
    GroupInfo = self.GroupDetailList[indexGroup]
  end
  if GroupInfo then
    local DataTable = {}
    table.insert(DataTable, GroupInfo.name)
    table.insert(DataTable, GroupInfo.hasPlayer)
    if GameAccountSelector.selectedGroupItem == indexGroup then
      table.insert(DataTable, "selected")
    else
      table.insert(DataTable, "unselected")
    end
    DataString = table.concat(DataTable, "\001")
  end
  self:GetFlashObject():InvokeASCallback("_root", "setGroupData", indexGroup, DataString)
end
GameAccountSelector.GServerList = {}
GameAccountSelector.selectedGroupItem = 1
function GameAccountSelector:GroupSelect(indexGroup)
  self.GServerList = {}
  GameAccountSelector.selectedGroupItem = indexGroup
  local groupServers = self.GroupDetailList[indexGroup].groupServers
  if #groupServers > 0 then
    local SortCallback = function(a, b)
      if a.role and not b.role then
        return true
      elseif not a.role and b.role then
        return false
      elseif a.role and b.role then
        return a.role.updated_at > b.role.updated_at
      end
      return a.server.logic_id > b.server.logic_id
    end
    local SortCallbackB = function(a, b)
      if a.role and not b.role then
        return true
      elseif not a.role and b.role then
        return false
      end
      return a.server.logic_id > b.server.logic_id
    end
    if indexGroup == 1 then
      table.sort(groupServers, SortCallbackB)
      local last_LoginServer_data = "0"
      local last_loingServer = {}
      local index = 1
      for i = 1, #groupServers do
        if groupServers[i].role and last_LoginServer_data < groupServers[i].role.updated_at then
          last_LoginServer_data = groupServers[i].role.updated_at
          index = i
          last_loingServer = groupServers[i]
        end
      end
      if index ~= 1 then
        table.remove(groupServers, index)
        table.insert(groupServers, 1, last_loingServer)
      end
    else
      table.sort(groupServers, SortCallback)
    end
  end
  self.GServerList = groupServers
  GameAccountSelector:ShowServerList()
end
function GameAccountSelector:ShowServerList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowServerList", #self.GServerList)
  end
end
GameAccountSelector.GroupDetailList = {}
function GameAccountSelector:initGroupData()
  if not self.mServerList then
    self:RequestServerList()
    return
  end
  if not self.mGroupList then
    GameAccountSelector.GServerList = self.mServerList
    self:ShowServerList()
    return
  end
  GameAccountSelector.GroupDetailList = {}
  DebugOut("initGroupData = ")
  DebugTable(self.mGroupList)
  for k, v in pairs(self.mGroupList) do
    local detail = {name = k, hasPlayer = "false"}
    local groupServers = {}
    detail.lang = v.lang
    detail.sort = v.sort
    for i = #v.range, 1, -1 do
      for _, Server in pairs(self.mServerList) do
        for j = v.range[i][1], v.range[i][2] do
          if Server.server.logic_id == j then
            table.insert(groupServers, Server)
            if detail.hasPlayer == "false" and Server.role then
              detail.hasPlayer = "true"
            end
            break
          end
        end
      end
    end
    detail.groupServers = groupServers
    if #groupServers > 0 then
      table.insert(GameAccountSelector.GroupDetailList, detail)
    end
  end
  local device_lang = GameUtils.DeviceLangToText(ext.GetDeviceLanguage())
  local function SortCallback(a, b)
    if a.lang == device_lang and b.lang ~= device_lang then
      return true
    elseif a.lang ~= device_lang and b.lang == device_lang then
      return false
    end
    return a.sort > b.sort
  end
  table.sort(GameAccountSelector.GroupDetailList, SortCallback)
  local detail = {
    name = GameLoader:GetGameText("LC_MENU_ALL_CHARACTER_CHAR")
  }
  local groupServers = {}
  for _, vServer in pairs(self.mServerList) do
    if vServer.role then
      table.insert(groupServers, 1, vServer)
    end
  end
  if #groupServers > 0 then
    detail.groupServers = groupServers
    detail.hasPlayer = "true"
    table.insert(GameAccountSelector.GroupDetailList, 1, detail)
  end
  self:showGroupList()
end
function GameAccountSelector:showGroupList()
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "showGroupList", #self.GroupDetailList)
  end
  self:GroupSelect(1)
end
function GameAccountSelector:ShowServerSelector()
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeServerSelector)
  local flashObj = self:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "showServerSelector")
  end
end
function GameAccountSelector:HideServerSelector()
  self:GetFlashObject():InvokeASCallback("_root", "hideServerSelector")
end
function GameAccountSelector:ServerSelect(indexServer)
  local serverinfo = self.GServerList[indexServer].server
  local playerInfo = self.GServerList[indexServer].role
  local registAppid = self.GServerList[indexServer].register_appid
  local isCreateActor = self.GServerList[indexServer].change_name or 1
  local localAppid = ext.GetBundleIdentifier()
  if registAppid and registAppid ~= "" and GameUtils:CanLoginByAppid(registAppid, localAppid) == false then
    GameUtils:ShowErrorLoginInfo(registAppid, localAppid)
    return
  end
  if serverinfo.is_mix and AutoUpdate.localAppVersion < 10607 and (localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or localAppid == "com.tap4fun.galaxyempire2_android_taptap" or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO) then
    GameUtils:ShowVersonError(localAppid)
    return
  end
  assert(serverinfo)
  if self.LoginInfoEditing == nil then
    self.LoginInfoEditing = {}
  end
  if serverinfo then
    self.LoginInfoEditing.ServerInfo = LuaUtils:table_rcopy(serverinfo)
    if playerInfo then
      self.LoginInfoEditing.PlayerInfo = LuaUtils:table_rcopy(playerInfo)
    else
      self.LoginInfoEditing.PlayerInfo = nil
    end
    if registAppid then
      self.LoginInfoEditing.RegistAppId = registAppid
    else
      self.LoginInfoEditing.RegistAppId = ""
    end
    self.LoginInfoEditing.isCreateActor = isCreateActor
    GameAccountSelector:SyncAccountTable()
    GameUtils:SetLoginInfo(LuaUtils:table_rcopy(self.LoginInfoEditing))
    DelayLoad()
    GameAccountSelector:UpdateLoginInfo()
    GameAccountSelector:SetLoginInfoEditing(nil)
    GameAccountSelector:SetActiveWindow("StartLogin")
    if GameUtils:IsLoginTangoAccount() then
      GameAccountSelector:EnterGame()
    end
  end
end
function GameAccountSelector:ServerHasRoleSelect(indexServer)
  DebugOut("GameAccountSelector:ServerHasRoleSelect", indexServer)
  local roleInfo = serverHasRoleList[indexServer].role
  local serverinfo = serverHasRoleList[indexServer].server
  local registAppid = serverHasRoleList[indexServer].register_appid or ""
  local isCreateActor = serverHasRoleList[indexServer].change_name or 1
  local localAppid = ext.GetBundleIdentifier()
  if registAppid and registAppid ~= "" and GameUtils:CanLoginByAppid(registAppid, localAppid) == false then
    GameUtils:ShowErrorLoginInfo(registAppid, localAppid)
    return
  end
  if serverinfo.is_mix and AutoUpdate.localAppVersion < 10607 and (localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or localAppid == "com.tap4fun.galaxyempire2_android_taptap" or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO) then
    GameUtils:ShowVersonError(localAppid)
    return
  end
  assert(serverinfo)
  assert(self.LoginInfoEditing)
  self.LoginInfoEditing.ServerInfo = LuaUtils:table_rcopy(serverinfo)
  self.LoginInfoEditing.PlayerInfo = LuaUtils:table_rcopy(roleInfo)
  self.LoginInfoEditing.RegistAppId = registAppid
  self.LoginInfoEditing.isCreateActor = isCreateActor
  GameAccountSelector:SyncAccountTable()
  GameUtils:SetLoginInfo(LuaUtils:table_rcopy(self.LoginInfoEditing))
  DelayLoad()
  GameAccountSelector:UpdateLoginInfo()
  GameAccountSelector:SetLoginInfoEditing(nil)
  GameAccountSelector:SetActiveWindow("StartLogin")
  if GameUtils:IsLoginTangoAccount() then
    GameAccountSelector:EnterGame()
  end
  self:closeRecommendServer()
end
function GameAccountSelector:ShowAccountLogin(AccountPassport, AccountPassword)
  AccountPassport = AccountPassport or ""
  AccountPassword = AccountPassword or ""
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeLoginBox)
  self:GetFlashObject():InvokeASCallback("_root", "showAccountLogin", AccountPassport, AccountPassword)
end
function GameAccountSelector:HideAccountLogin()
  self:GetFlashObject():InvokeASCallback("_root", "hideAccountLogin")
end
function GameAccountSelector:AccountSelect(indexAccount)
  DebugOut("GameAccountSelector:AccountSelect", indexAccount)
  local LoginInfo = self.LoginTable[indexAccount]
  assert(LoginInfo)
  local registAppid = LoginInfo.RegistAppId
  local localAppid = ext.GetBundleIdentifier()
  if registAppid and registAppid ~= "" and GameUtils:CanLoginByAppid(registAppid, localAppid) == false then
    GameUtils:ShowErrorLoginInfo(registAppid, localAppid)
    return
  end
  if LoginInfo.ServerInfo and LoginInfo.ServerInfo.is_mix and AutoUpdate.localAppVersion < 10607 and (localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or localAppid == "com.tap4fun.galaxyempire2_android_taptap" or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO) then
    GameUtils:ShowVersonError(localAppid)
    return
  end
  self:SetLoginInfoEditing(LoginInfo)
  selectedAccountAndRole = LoginInfo
  if LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest then
    if not LoginInfo.AccountInfo.password then
      local info = GameLoader:GetGameText("LC_MENU_LOGIN_UNLOGIN_ALERT")
      local function OnInfoOver()
        GameAccountSelector:ShowAccountLogin(LoginInfo.AccountInfo.passport, "")
      end
      GameTip:Show(info, 3000, OnInfoOver)
      return
    end
    if not LoginInfo.ServerInfo then
      local info = GameLoader:GetGameText("LC_MENU_LOGIN_WRONG_SERVER_ALERT")
      local function callback()
        GameAccountSelector:RequestServerList()
      end
      if not LoginInfo.PlayerInfo or LoginInfo.PlayerInfo and not LoginInfo.PlayerInfo.passport then
        info = GameLoader:GetGameText("LC_MENU_LOGIN_SELECT_SERVER_ALERT")
        function callback()
          GameAccountSelector:RequestServerList()
        end
      end
      GameTip:Show(info, 3000, callback)
      return
    end
    GameUtils:SetLoginInfo(LuaUtils:table_rcopy(LoginInfo))
    DelayLoad()
    self:UpdateLoginInfo()
    self:HideLoginSelector()
    if tonumber(LoginInfo.AccountInfo.accType) == GameUtils.AccountType.tango then
    end
    return
  else
    if not LoginInfo.ServerInfo then
      local info = GameLoader:GetGameText("LC_MENU_LOGIN_WRONG_SERVER_ALERT")
      local function callback()
        GameAccountSelector:RequestServerList()
      end
      GameTip:Show(info, 3000, callback)
      return
    end
    GameUtils:SetLoginInfo(LuaUtils:table_rcopy(LoginInfo))
    DelayLoad()
    self:UpdateLoginInfo()
    self:HideLoginSelector()
  end
  self:closeRecommendServer()
end
function GameAccountSelector:SetLoginInfoEditing(LoginInfo)
  self.LoginInfoEditing = LoginInfo
end
function GameAccountSelector:SetLoginInfoForFB(LoginInfo)
  self.LoginInfoForFB = LoginInfo
end
function GameAccountSelector:SetLoginInfoForTango(LoginInfo)
  self.LoginInfoForTango = LoginInfo
  GameUtils:printByAndroid("LoginInfoForTango---print")
  GameUtils:DebugOutTableInAndroid(self.LoginInfoForTango)
end
function GameAccountSelector:ShowAccountRegister()
  self:GetFlashObject():InvokeASCallback("_root", "showAccountRegister")
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeRegisterBox)
end
function GameAccountSelector:HideAccountRegister()
  if GameAccountSelector.isRegistByDelegate then
    return
  end
  self:GetFlashObject():InvokeASCallback("_root", "hideAccountRegister")
end
function GameAccountSelector:AccountFacebookLogin(loginPurpose)
  if self.tryLoginFB == false then
    self.fbLoginType = loginPurpose
    self.tryLoginFB = true
    GameUtils:tryToLoginFB()
  end
end
function GameAccountSelector:UpdateFB()
  if self.tryLoginFB == false or self.fbLoginType == 0 then
    return
  end
  if GameUtils.curFacebookInfo.success == 1 then
    DebugOut("GameAccountSelector:UpdateFB()")
    GameWaiting:ShowLoadingScreen()
    local AccountInfo = {}
    AccountInfo.passport = GameUtils.curFacebookInfo.fbID
    AccountInfo.password = GameUtils.curFacebookInfo.token
    GameUtils:FacebookAccountValidate(AccountInfo, self.FacebookAccountValidateCallback)
    self.tryLoginFB = false
    self.fbLoginType = 0
  elseif GameUtils.curFacebookInfo.success == 0 then
    GameTip:Show(GameLoader:GetGameText("LC_MENU_FACEBOOK_LOGIN_CANCEL_ALERT"), 2000)
    self.fbLoginType = 0
    self.tryLoginFB = false
  end
end
function GameAccountSelector:fbFeatureEnabled()
  return (...), Facebook
end
function GameAccountSelector:AccountEditOperation(passport, playername, servername, enabledPwdBtn, hideViewAfterClickServerBtn)
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeAccountOperation)
  self:GetFlashObject():InvokeASCallback("_root", "showAccountEditOperation", passport, playername, servername, enabledPwdBtn, GameUtils:IsLoginTangoAccount(), hideViewAfterClickServerBtn)
end
function GameAccountSelector:PlayerEditOperation(PlayerName, ServerName)
  if not AccountName then
    AccountName = ""
  end
  PlayerName = PlayerName or ""
  ServerName = ServerName or ""
  GameAndroidBackManager:AddCallback(GameAccountSelector.closeplayerOperation)
  self:GetFlashObject():InvokeASCallback("_root", "showPlayerEditOperation", PlayerName, ServerName, GameUtils:IsLoginTangoAccount())
end
function GameAccountSelector:EnterGame()
  local LoginInfo = GameUtils:GetLoginInfo()
  local registAppid = LoginInfo.RegistAppId
  local localAppid = ext.GetBundleIdentifier()
  DebugOut("Enter Game sss:", registAppid, localAppid)
  if not AutoUpdate.isDeveloper and registAppid and registAppid ~= "" and GameUtils:CanLoginByAppid(registAppid, localAppid) == false then
    GameUtils:ShowErrorLoginInfo(registAppid, localAppid)
    return
  end
  if not GameAccountSelector.mFBRelogin then
    if LoginInfo.AccountInfo and GameUtils:IsFacebookAccount(LoginInfo.AccountInfo) and "" == LoginInfo.AccountInfo.password then
      GameAccountSelector.mFBRelogin = true
      GameAccountSelector:AccountFacebookLogin(2)
      return
    end
  else
    GameAccountSelector.mFBRelogin = false
  end
  if LoginInfo.AccountInfo and LoginInfo.AccountInfo.accType ~= GameUtils.AccountType.guest and not LoginInfo.AccountInfo.password then
    local function OnInfoOver()
      self:ShowLoginSelector()
    end
    local info = GameLoader:GetGameText("LC_MENU_LOGIN_UNLOGIN_ALERT")
    GameTip:Show(info, 3000, OnInfoOver)
    return
  end
  if not LoginInfo.ServerInfo then
    local function OnInfoOver()
      if GameUtils:IsQihooApp() then
        local LoginInfo = self.LoginTable[1]
        self:SetLoginInfoEditing(LoginInfo)
        local accountInfo = {}
        accountInfo.passport = self.LoginInfoEditing.AccountInfo.passport
        accountInfo.passport = GameUtils.qihoo_token
        accountInfo.accType = GameUtils.AccountType.qihoo
        GameAccountSelector:RequestServerList()
      else
        self:ShowLoginSelector()
      end
    end
    local info = ""
    if LoginInfo.PlayerInfo then
      info = GameLoader:GetGameText("LC_MENU_LOGIN_WRONG_SERVER_ALERT")
    else
      info = GameLoader:GetGameText("LC_MENU_LOGIN_SELECT_SERVER_ALERT")
    end
    GameTip:Show(info, 3000, OnInfoOver)
    return
  end
  if LoginInfo.ServerInfo.is_mix and AutoUpdate.localAppVersion < 10607 and (localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_CN or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_LOCAL or localAppid == "com.tap4fun.galaxyempire2_android_taptap" or localAppid == AutoUpdate.bundleIdentifierType.BUNDLE_IDENTIFIER_NORMAL_ANDROID_PLATFORM_QIHOO) then
    GameUtils:ShowVersonError(localAppid)
    return
  end
  local function SocketConnectCall()
    local function socketConnectCallBack(checkres)
      if checkres and checkres >= 0 then
        GameStateManager:SetCurrentGameState(GameStateLoading)
      else
        local failedinfo = GameLoader:GetGameText("LC_MENU_CAN_NOT_INIT_NETWORK")
        if failedinfo ~= "" then
          GameTip:Show(failedinfo, 3000, function()
            GameAccountSelector.UpdateCall = SocketConnectCall
          end)
        end
        GameUtils:SyncServerInfo(nil)
      end
    end
    local result = NetMessageMgr:InitNet(socketConnectCallBack)
  end
  if LoginInfo.ServerInfo.is_in_maintain then
    local AccountInfo
    AccountInfo = LoginInfo.AccountInfo
    local function servercallback(content)
      DebugOut("server callback ---")
      for k, v in ipairs(content.servers) do
        if v.server.logic_id == LoginInfo.ServerInfo.logic_id then
          LoginInfo.ServerInfo = v.server
          break
        end
      end
      self.UpdateCall = SocketConnectCall
    end
    GameUtils:RequestServerList(AccountInfo, servercallback, nil)
  else
    self.UpdateCall = SocketConnectCall
  end
end
function GameAccountSelector:RefreshAccountAndServerData()
end
function GameAccountSelector:Update(dt)
  local flashObject = self:GetFlashObject()
  if flashObject then
    flashObject:InvokeASCallback("_root", "onUpdateAll", dt)
    flashObject:Update(dt)
  end
  if self.UpdateCall then
    local callfunc = self.UpdateCall
    self.UpdateCall = nil
    callfunc()
  end
  self:UpdateFB()
end
function GameAccountSelector.AccountTableHasRoleCallback(content)
  DebugOut("GameAccountSelector.AccountTableHasRoleCallback")
  DebugTable(content)
  if currentRequestPassport then
    AccountInfoCache[currentRequestPassport] = content.role_list
  end
  GameAccountSelector:GenerateServerHasRoleList(content.role_list)
  GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "lua2fs_showServerHasRole", #serverHasRoleList)
  currentRequestPassport = nil
end
function GameAccountSelector.AccountTableHasRoleError(content)
  DebugOut("GameAccountSelector.AccountTableHasRoleError")
  GameWaiting:HideLoadingScreen()
  local info = GameLoader:GetGameText("LC_MENU_LOGIN_WRONG_ACCOUNT_PLAYER_INFO_ALERT")
  GameTip:Show(info)
end
function GameAccountSelector.AccountValidateCallback(content)
  GameWaiting:HideLoadingScreen()
  DebugOut("Login Info:")
  DebugTable(content)
  if content.is_valid then
    do
      local NewAccountData = {}
      NewAccountData.passport = GameAccountSelector.PassportString
      NewAccountData.password = GameAccountSelector.PasswordString
      GameUtils:AddAccountLocalData(NewAccountData)
      successPassportString = GameAccountSelector.PassportString
      GameAccountSelector:HideAccountLogin()
      local function onInfoOver()
        local LoginInfo = GameAccountSelector.LoginInfoEditing
        local LoginInfo2 = GameUtils:GetLoginInfo()
        if LoginInfo and LoginInfo.PlayerInfo and LoginInfo2 and LoginInfo2.ServerInfo then
          GameUtils:Tap4funAccountBind(NewAccountData, LoginInfo.PlayerInfo.name, GameAccountSelector.AccountBindCallback, GameAccountSelector.AccountBindError)
        else
          GameStateAccountSelect.TryUpdateServer()
        end
      end
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameAccountSelector.FacebookAccountValidateCallback(content)
  GameWaiting:HideLoadingScreen()
  DebugOut("GameAccountSelector.LoginInfoForFB")
  DebugTable(GameAccountSelector.LoginInfoForFB)
  DebugTable(content)
  if content.is_valid then
    do
      local NewAccountData = {}
      NewAccountData.passport = GameUtils.curFacebookInfo.fbID
      NewAccountData.password = GameUtils.curFacebookInfo.token
      NewAccountData.accType = GameUtils.AccountType.facebook
      NewAccountData.displayName = GameUtils.curFacebookInfo.name
      GameUtils:AddAccountLocalData(NewAccountData)
      GameAccountSelector:HideAccountLogin()
      local function onInfoOver()
        local LoginInfo = GameAccountSelector.LoginInfoForFB
        DebugOut("GameAccountSelector.LoginInfoForFB")
        DebugTable(LoginInfo)
        if LoginInfo ~= nil and not LoginInfo.AccountInfo and LoginInfo.PlayerInfo then
          GameUtils:FacebookAccountBind(GameUtils.AccountType.facebook, NewAccountData, LoginInfo.PlayerInfo.name, GameAccountSelector.FacebookAccountBindCallback, GameAccountSelector.FacebookAccountBindError)
        elseif GameAccountSelector.mFBRelogin then
          GameAccountSelector:GenerateAccountTable()
          GameAccountSelector:UpdateLoginInfo()
          GameAccountSelector:EnterGame()
        else
          GameStateAccountSelect.TryUpdateServer()
          GameAccountSelector:UpdateLoginInfo()
        end
      end
      local waitTime = 3000
      if GameAccountSelector.mFBRelogin then
        waitTime = 1000
      end
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), waitTime, onInfoOver)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameAccountSelector.TangoAccountValiddataCallback(content)
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local NewAccountData = {}
    NewAccountData.passport = GameUtils.m_myProfile.tangoID
    NewAccountData.password = GameUtils.m_myProfile.token
    NewAccountData.accType = GameUtils.AccountType.tango
    NewAccountData.displayName = GameUtils.m_myProfile.fullName
    GameUtils:AddAccountLocalData(NewAccountData)
    local LoginInfo = GameAccountSelector.LoginInfoForTango
    GameAccountSelector:HideAccountLogin()
    GameUtils:printByAndroid("____fuck_NewAccountData.password=__")
    GameUtils:DebugOutTableInAndroid(LoginInfo)
    if GameAccountSelector.LoginInfoForTango ~= nil and not GameAccountSelector.LoginInfoForTango.AccountInfo and GameAccountSelector.LoginInfoForTango.PlayerInfo then
      GameUtils:printByAndroid("____fuck_LoginInfo ~= nil")
      GameUtils:FacebookAccountBind(GameUtils.AccountType.tango, NewAccountData, LoginInfo.PlayerInfo.name, GameAccountSelector.FacebookAccountBindCallback, GameAccountSelector.FacebookAccountBindError)
    else
      GameUtils:FacebookAccountBind(GameUtils.AccountType.tango, NewAccountData, GameUtils.m_myProfile.fullName, GameAccountSelector.FacebookAccountBindCallback, GameAccountSelector.FacebookAccountBindError)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameAccountSelector.QihooAccountValidateCallback(content)
  GameWaiting:HideLoadingScreen()
  GameUtils:printByAndroid("GameAccountSelector.QihooAccountValidateCallback-----content!!!!!")
  GameUtils:DebugOutTableInAndroid(content)
  if content.is_valid then
    do
      local NewAccountData = {}
      NewAccountData.passport = content.user_id
      NewAccountData.password = "Qihoo360"
      NewAccountData.accType = GameUtils.AccountType.qihoo
      GameUtils.m_qihooProfile = NewAccountData
      local mLoginInfo = GameUtils:GetLoginInfo()
      if mLoginInfo and mLoginInfo.AccountInfo then
        GameUtils:printByAndroid("GameAccountSelector.QihooAccountValidateCallback:" .. content.user_id)
        mLoginInfo.AccountInfo.passport = content.user_id
        mLoginInfo.AccountInfo.password = GameUtils.qihoo_token
      end
      GameUtils:AddAccountLocalData(NewAccountData)
      local function onInfoOver()
        local LoginInfo = GameAccountSelector.LoginInfoEditing
        if LoginInfo and LoginInfo.PlayerInfo then
          GameUtils:Tap4funAccountBind(NewAccountData, LoginInfo.PlayerInfo.name, GameAccountSelector.AccountBindCallback, GameAccountSelector.AccountBindError)
        else
          GameStateAccountSelect.TryUpdateServer()
        end
      end
      GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
    end
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
    GameUtils.qihoo_token = nil
  end
end
function GameAccountSelector.PlatformExtAccountValidateCallback_Standard(content)
  GameUtils:printByAndroid("GameAccountSelector.PlatformExtAccountValidateCallback_Standard")
  GameUtils:DebugOutTableInAndroid(content)
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    local NewAccountData = {}
    NewAccountData.passport = content.user_id
    NewAccountData.password = content.user_id
    NewAccountData.accType = tonumber(IPlatformExt.getConfigValue("AccType"))
    GameUtils:AddAccountLocalData(NewAccountData)
    GameAccountSelector:HideAccountLogin()
    local function onInfoOver()
      GameStateAccountSelect.TryUpdateServer()
      if GameUtils:GetLoginInfo() then
        GameAccountSelector:UpdateLoginInfo()
      else
        GameUtils:printByAndroid("<------- not LoginInfo --------->")
      end
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_ACCOUNT_CHEAK_RIGHT_CHAR"), 3000, onInfoOver)
  else
    GameTip:Show(GameLoader:GetGameText("LC_MENU_WRONG_USER_NAME_CHAR"))
  end
end
function GameAccountSelector.AccountRegisterCallback(content)
  DebugOutPutTable(content, "AccountRegisterCallback")
  GameWaiting:HideLoadingScreen()
  if content.is_valid then
    do
      local AccountInfo = {}
      AccountInfo.passport = GameAccountSelector.PassportString
      AccountInfo.password = GameAccountSelector.PasswordString
      local nowLoginInfo = {AccountInfo = AccountInfo}
      if GameAccountSelector.LoginInfoEditing then
        GameAccountSelector.LoginInfoEditing.AccountInfo = AccountInfo
      else
        GameAccountSelector:SetLoginInfoEditing(nowLoginInfo)
        GameSettingData.LastLogin = nowLoginInfo
        GameAccountSelector:RequestServerList(AccountInfo)
      end
      local updated_at = os.date("%Y-%m-%d", os.time()) .. "T" .. os.date("%H:%M:%S", os.time()) .. "Z"
      AccountInfo.updated_at = updated_at
      GameUtils:AddAccountLocalData(AccountInfo)
      local function onInfoOver()
        local LoginInfo = GameAccountSelector.LoginInfoEditing
        if LoginInfo and LoginInfo.PlayerInfo then
          if GameAccountSelector.isRegistByDelegate then
            LoginInfo.AccountInfo = AccountInfo
            GameUtils:SetLoginInfo(LoginInfo)
          end
          GameUtils:Tap4funAccountBind(AccountInfo, LoginInfo.PlayerInfo.name, GameAccountSelector.AccountBindCallback, GameAccountSelector.AccountBindError)
        else
          GameStateAccountSelect.TryUpdateServer()
        end
      end
      GameTip:Show(GameLoader:GetGameText("LC_MENU_REG_SUCCESS_CHAR"), 3000, onInfoOver)
    end
  else
    local tipInfo = GameLoader:GetGameText("LC_ALERT_" .. content.error)
    DebugOut("show alert", tipInfo)
    if tipInfo then
      GameTip:Show(tipInfo)
    else
      DebugOut("unknown content.error", content.error)
      GameTip:Show("LC_ALERT_passport_not_valid")
    end
  end
end
function GameAccountSelector.AccountBindCallback(content)
  GameWaiting:HideLoadingScreen()
  GameAccountSelector:HideAccountRegister()
  local function onInfoOver()
    GameStateAccountSelect.TryUpdateServer()
  end
  GameTip:Show(GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR"), 3000, onInfoOver)
  if GameAccountSelector.isRegistByDelegate then
    GameSetting:bindNewAccountComplete()
    GameAccountSelector.isRegistByDelegate = false
  end
end
function GameAccountSelector.AccountBindError(content)
  GameStateAccountSelect.TryUpdateServer()
end
function GameAccountSelector.FacebookAccountBindCallback(content)
  GameWaiting:HideLoadingScreen()
  GameUtils:printByAndroid("FacebookAccountBindCallback")
  GameUtils:DebugOutTableInAndroid(content)
  if GameUtils:IsTangoAPP() then
    GameStateAccountSelect.TryUpdateServer()
    GameAccountSelector:UpdateLoginInfo()
  else
    local function onInfoOver()
      GameStateAccountSelect.TryUpdateServer()
      GameAccountSelector:UpdateLoginInfo()
    end
    GameTip:Show(GameLoader:GetGameText("LC_MENU_BINDING_ACCOUNT_SUCCESS_CHAR"), 3000, onInfoOver)
  end
end
function GameAccountSelector.FacebookAccountBindError(content)
  GameStateAccountSelect.TryUpdateServer()
end
function GameAccountSelector.closeSelectPlayer()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideLoginSelector")
  end
end
function GameAccountSelector.closeplayerOperation()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hidePlayerEditOperation")
  end
end
function GameAccountSelector.closeAccountOperation()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideAccountEditOperation")
  end
end
function GameAccountSelector.closeNewAccountOperation()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideAccountNewOperation")
  end
end
function GameAccountSelector.closeLoginBox()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideAccountLogin")
  end
end
function GameAccountSelector.closeRegisterBox()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideAccountRegister")
  end
end
function GameAccountSelector.closeServerSelector()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "hideServerSelector")
  end
end
function GameAccountSelector.closeAgreementBox()
  if AutoUpdate.isAndroidDevice then
    GameAccountSelector:GetFlashObject():InvokeASCallback("_root", "closeAgreement")
  end
end
function GameAccountSelector:CheckFBAccountValid(fbId, fbToken, callback)
  local AccountInfo = {}
  AccountInfo.passport = fbId
  AccountInfo.password = fbToken
  GameUtils:FacebookAccountValidate(AccountInfo, callback)
end
function GameAccountSelector:closeRecommendServer()
  if currentServerShowingRolesIndex ~= 0 then
    currentServerShowingRolesIndex = 0
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_closeServerHasRole")
  end
end
function GameAccountSelector.passwordModifyCallback(content)
  DebugOut("GameAccountSelector:passwordModifyCallback")
  DebugOut(content)
  local code = content == 1 and 0 or 1
  local id = "LC_ALERT_password_modify_response_" .. code
  DebugOut("id", id)
  local tipInfo = GameLoader:GetGameText(id)
  DebugOut("tipInfo", tipInfo)
  GameUtils:ShowTips(tipInfo)
  if code == 0 then
    DebugOut("new Password:", GameAccountSelector.newPasswordString)
    GameAccountSelector.LoginInfoEditing.AccountInfo.password = GameAccountSelector.newPasswordString
    local localAccount = GameUtils:GetAccountLocalData(GameAccountSelector.LoginInfoEditing.AccountInfo.passport)
    localAccount.password = GameAccountSelector.newPasswordString
    GameUtils:SaveSettingData()
    local flashObj = GameAccountSelector:GetFlashObject()
    if flashObj then
      flashObj:InvokeASCallback("_root", "lua2fs_hideModifyPassword")
    end
  end
end
function GameAccountSelector:RequestServerList(AccountInfo)
  DebugOut("GameAccountSelector:RequestServerList")
  if AccountInfo or self.LoginInfoEditing.AccountInfo then
    AccountInfo = AccountInfo or self.LoginInfoEditing.AccountInfo
    GameUtils:RequestServerList(AccountInfo, GameAccountSelector.RequestServerListSuccessCallback, GameAccountSelector.RequestServerListFailedCallback)
  end
end
function GameAccountSelector.RequestServerListSuccessCallback(content)
  GameAccountSelector.mServerList = content.servers
  GameAccountSelector.mGroupList = content.groups
  GameAccountSelector.GServerList = content.servers
  GameAccountSelector:HideAccountRegister()
  GameAccountSelector:initGroupData()
  GameAccountSelector:ShowServerSelector()
end
function GameAccountSelector.RequestServerListFailedCallback(content)
end
function GameAccountSelector:SetFindPasswordText()
  local flashObj = GameAccountSelector:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "SetFindPasswordText", GameLoader:GetGameText("LC_MENU_FOTGOT_PASSWORD"))
  end
end
function GameAccountSelector:FindPassword(userName)
  local username = "1"
  if userName and string.len(userName) > 0 then
    username = userName
  end
  local theAppVersion
  if not AutoUpdate.localAppVersion then
    local _, _, AppVer1, AppVer2, AppVer3 = string.find(ext.GetAppVersion(), "(%d+).(%d+).(%d+)")
    theAppVersion = tonumber(AppVer3) + 100 * tonumber(AppVer2) + 10000 * tonumber(AppVer1)
  else
    theAppVersion = AutoUpdate.localAppVersion
  end
  local server = "1"
  if GameSettingData and GameSettingData.LastLogin and GameSettingData.LastLogin.ServerInfo and GameSettingData.LastLogin.ServerInfo.name then
    server = GameSettingData.LastLogin.ServerInfo.name
  end
  local IP = GameSetting.localIP or ""
  if ext.GetPlatform() == "iOS" and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) and GameUtils:GetLeftNumFromOSVersion(ext.GetOSVersion()) < 7 then
    local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
    GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
    GameUIMessageDialog:Display(text_title, GameLoader:GetGameText("LC_MENU_IOS_VERSION_LOWER"))
  elseif theAppVersion < 10706 then
    local isSupporthelpshift = false
    local updateText
    if ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.ts.spacelancer" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2.chinese" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.ts.galaxyempire2_android_global" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_amazon" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_cafe" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_local" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android_taptap" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.qihoo360" then
      isSupporthelpshift = true
    elseif ext.GetBundleIdentifier() == "com.tap4fun.galaxyempire2_android.platform.uc" then
      isSupporthelpshift = true
    else
      isSupporthelpshift = false
    end
    if isSupporthelpshift then
      updateText = "LC_MENU_CS_UPDATE_TO_NEW_VERSION"
      do
        local localAppid = ext.GetBundleIdentifier()
        if localAppid then
          local function callback()
            ext.http.openURL(GameUtils.AppDownLoadURL[localAppid])
          end
          local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
          GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.YesNo)
          GameUIMessageDialog:SetYesButton(callback)
          GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
        end
      end
    else
      updateText = "LC_MENU_CS_CAN_NOT_CONNECT"
      local text_title = GameLoader:GetGameText("LC_MENU_TITLE_VERIFY")
      GameUIMessageDialog:SetStyle(GameUIMessageDialog.DialogStyle.OK)
      GameUIMessageDialog:Display(text_title, GameLoader:GetGameText(updateText))
    end
  else
    ext.http.openFAQ("-1", 0, 0, username, ext.GetAppVersion(), "FindPassword", ext.GetIOSOpenUdid(), server, IP)
  end
end
