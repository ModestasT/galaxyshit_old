FacebookGraphStorySharer = {
  mFeatureSwitch = {},
  mFeatureState = {},
  mAwards = {},
  mMaxTextCount = 3,
  mMaxImgCount = 4,
  lastRandomIndex = nil,
  mAwardReq = {}
}
FacebookGraphStorySharer.StoryType = {
  STORY_TYPE_WD_ENTER = 101,
  STORY_TYPE_WORLDCHAMPION_JOIN = 102,
  STORY_TYPE_WORLDCHAMPION_SUPPORT = 103,
  STORY_TYPE_WORLDCHAMPION_LIKE_REPORT = 104,
  STORY_TYPE_WORLDCHAMPION_LIKE_CHMPION = 105,
  STORY_TYPE_LABA_AWARD = 106,
  STORY_TYPE_ARENA_WIN = 107,
  STORY_TYPE_ARENA_BEST_RANK = 108,
  STORY_TYPE_ARENA_DAY_AWARD = 109,
  STORY_TYPE_MINE_WIN = 110,
  STORY_TYPE_COLONIAL_WIN = 111,
  STORY_TYPE_COLONIAL_ORDER_SLAVER = 112,
  STORY_TYPE_COLONIAL_SLAVER_TRICK = 113,
  STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN = 114,
  STORY_TYPE_CRUSADE_AWARD = 115,
  STORY_TYPE_STARALLIANCE_MODIFY_NOTE = 116,
  STORY_TYPE_REBUILDSHIP_REQUET_HELP = 117,
  STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER = 118,
  STORY_TYPE_STARALLIANCE_SEND_BRICK = 119,
  STORY_TYPE_HANDBOOK_LIKE = 120,
  STORY_TYPE_MAIL = 121
}
FacebookGraphStorySharer.FeatureType = {FEATRUE_SHARE_AWARD = 122}
function FacebookGraphStorySharer:OnInitGame()
  math.randomseed(tostring(os.time()):reverse():sub(1, 6))
  NetMessageMgr:RegisterMsgHandler(NetAPIList.story_status_ntf.Code, FacebookGraphStorySharer.FeatureStateNtf)
end
function FacebookGraphStorySharer.FeatureStateNtf(content)
  DebugOut("FeatureStateNtf content = ")
  DebugTable(content)
  for i, v in ipairs(content.story_list) do
    FacebookGraphStorySharer.mFeatureState[v.key] = v.value
  end
  for i, v in ipairs(content.story_awards) do
    FacebookGraphStorySharer.mAwards[v.story_id] = v
  end
end
function FacebookGraphStorySharer:InitSharer()
end
function FacebookGraphStorySharer:ShareStory(storyType, extraInfo)
  if not self:IsStoryEnabled(storyType) then
    return false
  end
  DebugOut("ShareStory type = ", storyType)
  local actionTypeWithNameSpace, objectType, objectTypeWithNameSpace, storyTitle, storyDesc, storyImgUrl = FacebookGraphStorySharer:GetStoryFBDefineInfo(storyType, extraInfo)
  DebugOut("ShareStory type 2222= ", storyType)
  if FacebookGraphStorySharer.mCurrentStoryType ~= nil then
    return false
  end
  FacebookGraphStorySharer.mCurrentStoryType = storyType
  Facebook:ShareGraphStory(objectTypeWithNameSpace, objectType, actionTypeWithNameSpace, storyTitle or "", storyDesc or "", storyImgUrl or "", FacebookGraphStorySharer.ShareStoryCallback)
  DebugOut("ShareStory type 3333= ", storyType)
  if extraInfo then
    FacebookGraphStorySharer.mCurrentStoryFleet = extraInfo.fleetID or extraInfo.mailID
    FacebookGraphStorySharer.mCurrentMailID = extraInfo.mailID
  else
    FacebookGraphStorySharer.mCurrentStoryFleet = nil
    FacebookGraphStorySharer.mCurrentMailID = nil
  end
  if FacebookGraphStorySharer.mFeatureSwitch[FacebookGraphStorySharer.FeatureType.FEATRUE_SHARE_AWARD] == 1 then
    local FacebookStoryAwardUI = LuaObjectManager:GetLuaObject("FacebookStoryAwardUI")
    local awardInfo = FacebookStoryAwardUI:GetAwardByType(storyType)
    if not Facebook:HasPublishPermission() then
      FacebookGraphStorySharer.waitPublishPermission = true
    end
    if #awardInfo and Facebook:HasPublishPermission() then
      FacebookStoryAwardUI:Show(awardInfo)
      if FacebookGraphStorySharer.mCurrentStoryType then
        local FacebookStoryAwardUI = LuaObjectManager:GetLuaObject("FacebookStoryAwardUI")
        local awardInfo = FacebookStoryAwardUI:GetAwardByType(FacebookGraphStorySharer.mCurrentStoryType)
        local req = {
          story_id = FacebookGraphStorySharer.mCurrentStoryType,
          fleet_id = FacebookGraphStorySharer.mCurrentStoryFleet or 0
        }
        DebugOut("req = ")
        DebugTable(req)
        NetMessageMgr:SendMsg(NetAPIList.story_award_req.Code, req, FacebookGraphStorySharer.RequestAwardCallback, false, nil)
        FacebookGraphStorySharer.mCurrentStoryType = nil
      else
        Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
      end
    end
  end
  return true
end
function FacebookGraphStorySharer.ShareStoryCallback(success)
  DebugOut("Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)")
  Facebook:SetStatus(FACEBOOK_ACTION_STATUS.IDLE)
  FacebookGraphStorySharer.mCurrentStoryType = nil
  FacebookGraphStorySharer.waitPublishPermission = false
end
function FacebookGraphStorySharer:AddAwardReq(reqParam)
  if reqParam then
    table.insert(FacebookGraphStorySharer.mAwardReq)
  end
end
function FacebookGraphStorySharer.RequestAwardCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.story_award_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgType == NetAPIList.story_award_ack.Code then
    return true
  end
  return false
end
function FacebookGraphStorySharer:GetStoryDetail(storyType, extraInfo)
  if extraSubType then
    local storyTitle = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. extraSubType .. "_" .. randomIndex
    local storyDesc = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. extraSubType .. "_" .. randomIndex
    local storyImgUrl = Facebook.mImgUrl .. "images/fb_story_" .. "_" .. randomImageIndex .. ".png"
    return storyTitle, storyDesc, storyImgUrl
  else
    local storyTitle = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    local storyDesc = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local storyImgUrl = Facebook.mImgUrl .. "images/fb_story_" .. "_" .. randomImageIndex .. ".png"
    return storyTitle, storyDesc, storyImgUrl
  end
end
function FacebookGraphStorySharer:GetStoryFBDefineInfo(storyType, extraInfo)
  local randomIndex = math.random(1, 120)
  local randomImageIndex = math.random(1, self.mMaxImgCount)
  local split = 40
  if randomIndex <= split then
    randomIndex = 1
  elseif randomIndex <= 2 * split then
    randomIndex = 2
  else
    randomIndex = 3
  end
  DebugOut("randomIndex = ", randomIndex)
  DebugOut("randomImageIndex = ", randomImageIndex)
  local actionTypeWithNameSpace = "galaxylegend:taste"
  local objectType = "dish"
  local objectTypeWithNameSpace = "galaxylegend:dish"
  local storyTitle = ""
  local storyDesc = ""
  local storyImgUrl = Facebook.mImgUrl .. "images/fb_story_%04d.png"
  storyImgUrl = string.format(storyImgUrl, randomImageIndex)
  DebugOut("storyImgUrl = ", storyImgUrl)
  if storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_WD_ENTER then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    local myAlliance = extraInfo.myAlliance
    local enemyAlliance = extraInfo.enemyAlliance
    local starName = extraInfo.wdStarName
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<enemyAlliance>", enemyAlliance)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = string.gsub(storyDesc, "<enemyAlliance>", enemyAlliance)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_JOIN then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_SUPPORT then
    actionTypeWithNameSpace = "galaxylegend:voice"
    objectType = "opinion"
    objectTypeWithNameSpace = "galaxylegend:opinion"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyTitle = string.gsub(storyDesc, "<player>", extraInfo.playerName)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_LIKE_REPORT then
    actionTypeWithNameSpace = "galaxylegend:voice"
    objectType = "opinion"
    objectTypeWithNameSpace = "galaxylegend:opinion"
    storyTitle = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDesc = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_WORLDCHAMPION_LIKE_CHMPION then
    actionTypeWithNameSpace = "galaxylegend:voice"
    objectType = "opinion"
    objectTypeWithNameSpace = "galaxylegend:opinion"
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_LABA_AWARD then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. extraInfo.subType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. extraInfo.subType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_WIN then
    actionTypeWithNameSpace = "galaxylegend:win"
    objectType = "battle"
    objectTypeWithNameSpace = "galaxylegend:battle"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_BEST_RANK then
    actionTypeWithNameSpace = "galaxylegend:voice"
    objectType = "opinion"
    objectTypeWithNameSpace = "galaxylegend:opinion"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<rank>", tostring(extraInfo.bestRank))
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = string.gsub(storyDesc, "<rank>", tostring(extraInfo.bestRank))
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_ARENA_DAY_AWARD then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_MINE_WIN then
    actionTypeWithNameSpace = "galaxylegend:win"
    objectType = "battle"
    objectTypeWithNameSpace = "galaxylegend:battle"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.ownerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = string.gsub(storyDesc, "<player>", extraInfo.ownerName)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_WIN then
    actionTypeWithNameSpace = "galaxylegend:win"
    objectType = "battle"
    objectTypeWithNameSpace = "galaxylegend:battle"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_ORDER_SLAVER then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_TRICK then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_COLONIAL_SLAVER_FIGHT_WIN then
    actionTypeWithNameSpace = "galaxylegend:win"
    objectType = "battle"
    objectTypeWithNameSpace = "galaxylegend:battle"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<player>", extraInfo.playerName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_CRUSADE_AWARD then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyTitle = string.gsub(storyTitle, "<spaceNum>", extraInfo.spaceNum)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_MODIFY_NOTE then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. 1
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. 1
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = storyDesc .. extraInfo.allianceNote
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_REBUILDSHIP_REQUET_HELP then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<denfencerName>", extraInfo.defencerName)
    storyTitle = string.gsub(storyTitle, "<myAlliance>", extraInfo.allianceName)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = string.gsub(storyDesc, "<denfencerName>", extraInfo.defencerName)
    storyDesc = string.gsub(storyDesc, "<myAlliance>", extraInfo.allianceName)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "story"
    objectTypeWithNameSpace = "galaxylegend:story"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    local facebookName = Facebook.mUserName or ""
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyTitle = string.gsub(storyTitle, "<facebookName>", facebookName)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyDesc = string.gsub(storyDesc, "<myAlliance>", extraInfo.allianceName)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_HANDBOOK_LIKE then
    actionTypeWithNameSpace = "galaxylegend:voice"
    objectType = "opinion"
    objectTypeWithNameSpace = "galaxylegend:opinion"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. randomIndex
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. randomIndex
    storyTitle = GameLoader:GetGameText(storyTitleID)
    storyDesc = GameLoader:GetGameText(storyDescID)
    storyTitle = string.gsub(storyTitle, "<heroName>", extraInfo.heroName)
    storyDesc = string.gsub(storyDesc, "<heroName>", extraInfo.heroName)
  elseif storyType == FacebookGraphStorySharer.StoryType.STORY_TYPE_MAIL then
    actionTypeWithNameSpace = "galaxylegend:share"
    objectType = "mail"
    objectTypeWithNameSpace = "galaxylegend:mail"
    storyTitleID = "LC_MENU_FB_STORY_TITLE_" .. storyType .. "_" .. 1
    storyDescID = "LC_MENU_FB_STORY_Desc_" .. storyType .. "_" .. 1
    storyTitle = extraInfo.title
    storyDesc = extraInfo.mailText
  end
  DebugOut("storyTitle = ", storyTitle)
  DebugOut("storyDesc = ", storyDesc)
  return actionTypeWithNameSpace, objectType, objectTypeWithNameSpace, storyTitle, storyDesc, storyImgUrl
end
function FacebookGraphStorySharer:IsStoryEnabled(storyType)
  if AutoUpdate.isWin32 then
    return false
  end
  if not Facebook:HasPublishPermission() then
    return false
  end
  if AutoUpdate.isAndroidDevice and not AutoUpdate.isShareStorySupport then
    return false
  end
  if not Facebook:IsFacebookEnabled() then
    return false
  end
  if not Facebook:CanShareStory() then
    return false
  end
  DebugOut("11IsFacebookEnabled true")
  if not Facebook:IsLoginedIn() then
    DebugOut("IsLoginedIn false")
    return false
  end
  if not Facebook:IsBindFacebook() then
    return false
  end
  DebugOut("storyType = ", storyType)
  DebugOut("self.mFeatureState[storyType] = ", self.mFeatureState[storyType])
  DebugOut("self.mFeatureSwitch[storyType] = ", self.mFeatureSwitch[storyType])
  if self.mFeatureState[storyType] == 1 and self.mFeatureSwitch[storyType] == 1 then
    return true
  end
  return false
end
