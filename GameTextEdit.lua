GameTextEdit = {
  m_currentTextEdit = nil,
  m_currentRenderFx = nil,
  m_currentInputType = nil,
  m_keyboardObj = nil,
  m_delKeyboards = {},
  m_maxLength = 0,
  m_textfiled = nil,
  m_classType = nil,
  m_stageWD = nil,
  m_stageHT = nil,
  m_lastClassType = nil,
  m_lastRenderFx = nil,
  m_lastTextEdit = nil
}
g_onKeyboardShowNotify = {}
g_onKeyboardHideNotify = {}
GameTextEdit.keyboardHeight = 0.458
function RegisterKeyboardShowNotify(func)
  local registered = false
  for k, v in pairs(g_onKeyboardShowNotify) do
    if v == func then
      registered = true
      break
    end
  end
  if not registered then
    table.insert(g_onKeyboardShowNotify, func)
  end
end
function RemoveKeyboardShowNotify(func)
  if #g_onKeyboardShowNotify > 0 then
    for k = 1, #g_onKeyboardShowNotify do
      if g_onKeyboardShowNotify[k] == func then
        table.remove(g_onKeyboardShowNotify, k)
        break
      end
    end
  end
end
function RegisterKeyboardHideNotify(func)
  local registered = false
  for k, v in pairs(g_onKeyboardHideNotify) do
    if v == func then
      registered = true
      break
    end
  end
  if not registered then
    table.insert(g_onKeyboardHideNotify, func)
  end
end
function RemoveKeyboardHideNotify(func)
  if #g_onKeyboardHideNotify > 0 then
    for k = 1, #g_onKeyboardHideNotify do
      if g_onKeyboardHideNotify[k] == func then
        table.remove(g_onKeyboardHideNotify, k)
        break
      end
    end
  end
end
ext.RegisteTextInputClass("TextEditInput_t")
function ext.keyboard.onKeyboardShow(_w, _h)
  DebugOut("ssssss key bo 0:" .. tostring(#g_onKeyboardShowNotify))
  if GameTextEdit.m_currentRenderFx and GameTextEdit.m_keyboardObj then
    if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice then
      local width = ext.SCREEN_WIDTH
      local usewidth = math.floor(width * 2252 / 2436)
      _w = usewidth / width
    end
    GameTextEdit.keyboardHeight = _h
    GameTextEdit.m_currentRenderFx:InvokeASCallback(GameTextEdit.m_currentTextEdit, "_onChangeKeyBoardHeight", _w, _h)
  end
  if #g_onKeyboardShowNotify > 0 then
    for k = 1, #g_onKeyboardShowNotify do
      g_onKeyboardShowNotify[k]()
    end
  end
end
function ext.keyboard.onKeyboardHide()
  if GameTextEdit.m_lastRenderFx and GameTextEdit.m_lastTextEdit then
    GameTextEdit.m_lastRenderFx:InvokeASCallback(GameTextEdit.m_lastTextEdit, "lua2fs_noitfyShowKeyboard", false)
    GameTextEdit.m_lastRenderFx = nil
    GameTextEdit.m_lastTextEdit = nil
    for _, v in pairs(GameTextEdit.m_delKeyboards) do
      v:Destroy()
    end
    GameTextEdit.m_delKeyboards = {}
  end
  if #g_onKeyboardHideNotify > 0 then
    for k = 1, #g_onKeyboardHideNotify do
      g_onKeyboardHideNotify[k]()
    end
  end
end
function ext.keyboard.onKeyboardReturnPress()
  GameTextEdit.m_currentRenderFx:InvokeASCallback("_root", "onReturnPressed")
  return 0
end
function TextEditInput_t:changeCharacter(str, position)
  if GameTextEdit.m_currentInputType == "NumberPad" then
    if str ~= "\b" and not tonumber(str) then
      return false
    end
  elseif GameTextEdit.m_currentInputType ~= "Password" or str == "\b" or tonumber(str) or str == "_" then
  else
    local _start, _end = string.find(str, "%a")
    if not _start and _end ~= #str then
      return false
    end
  end
  return true
end
function TextEditInput_t:endEditing()
  GameTextEdit:_OnHideKeyboard()
end
function GameTextEdit:GetKeyboard()
  return self.m_keyboardObj
end
function GameTextEdit:convertXToScreenCoord(x, isSpecial)
  if ext.is2x1 and ext.is2x1() and not AutoUpdate.isAndroidDevice then
    local UseHeight = math.floor(ext.SCREEN_WIDTH * 2252 / 2436)
    local x_start = math.floor(ext.SCREEN_WIDTH * 92 / 2436)
    if isSpecial then
      return x * UseHeight / self.m_stageWD + x_start
    else
      return x * UseHeight / self.m_stageWD
    end
  else
    return x * ext.SCREEN_WIDTH / self.m_stageWD
  end
end
function GameTextEdit:convertYToScreenCoord(y)
  return y * ext.SCREEN_HEIGHT / self.m_stageHT
end
function GameTextEdit:OnTouchReleased(x, y)
  if self.m_keyboardObj then
    DebugOut("keyBroad:44444")
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_HideKeyboardBefore")
    self.m_keyboardObj:Hide()
  end
  return false
end
function GameTextEdit:HideKeyboard(x, y)
  if self.m_keyboardObj then
    local text, length = self.m_keyboardObj:GetText()
    text = string.gsub(text, "\226\128\134", "")
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_setText", text, length)
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_HideKeyboardBefore")
    function closeBroad()
      self.m_keyboardObj:Hide()
    end
    pcall(closeBroad)
    if AutoUpdate.isWin32 then
      TextEditInput_t:endEditing()
    end
  end
  return false
end
function GameTextEdit:_OnHideKeyboard()
  if self.m_keyboardObj then
    local text, length = self.m_keyboardObj:GetText()
    text = string.gsub(text, "\226\128\134", "")
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_setText", text, length)
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_onTextUpdated")
    self.m_lastTextEdit = self.m_currentTextEdit
    self.m_lastRenderFx = self.m_currentRenderFx
    self.m_lastClassType = self.m_classType
    self.m_keyboardObj:Destroy()
    self.m_keyboardObj = nil
    self.isInUse = false
  end
end
function GameTextEdit:GetCurrentContent()
  if self.m_keyboardObj then
    local text, length = self.m_keyboardObj:GetText()
    text = string.gsub(text, "\226\128\134", "")
    return text
  end
  return ""
end
function GameTextEdit:SetCurrentContent(str)
  if self.m_keyboardObj then
    self.m_keyboardObj:SetText(str)
  end
end
function GameTextEdit:fscommand(renderFx, cmd, arg)
  if cmd == "FS_TEXTEDIT,onRelease" then
    print("GameTextEdit:fscommand FS_TEXTEDIT,onRelease")
    GameTextEdit.TouchInInput = true
    if self.isInUse then
      return
    end
    self.isInUse = true
    local inputType, maxLength, mcPath, editText, _x, _y, _w, _h, classType, stageWD, stageHT, initStr = unpack(LuaUtils:string_split(arg, "\001"))
    initStr = initStr or ""
    print("initStr = ", initStr)
    print("inputType = ", inputType)
    print("maxLength = ", maxLength)
    print("mcPath = ", mcPath)
    print("editText = ", editText)
    print("_x = ", _x)
    print("_y = ", _y)
    print("_w = ", _w)
    print("_h = ", _h)
    print("classType = ", classType)
    maxLength = tonumber(maxLength)
    self.m_stageWD = tonumber(stageWD)
    self.m_stageHT = tonumber(stageHT)
    if self.m_stageWD and ext.IsArm64 then
      _x = self:convertXToScreenCoord(tonumber(_x), true)
      _w = self:convertXToScreenCoord(tonumber(_w))
    end
    if self.m_stageHT and ext.IsArm64 then
      _h = self:convertYToScreenCoord(tonumber(_h))
      _y = self:convertYToScreenCoord(tonumber(_y))
    end
    self.m_currentTextEdit = mcPath
    self.m_currentRenderFx = renderFx
    self.m_maxLength = maxLength
    self.m_textfiled = editText
    self.m_currentInputType = inputType
    self.m_classType = classType
    self.m_keyboardObj = TextEditInput_t:new()
    local fontSize = self.m_currentRenderFx:GetFontSize(editText)
    local fontColor = self.m_currentRenderFx:GetFontColor(editText)
    local alpha = 0
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_ShowKeyboardBefore", GameTextEdit.keyboardHeight)
    self.m_keyboardObj:Init(classType, initStr, _x, _y, _w, _h, 1, alpha, fontSize, fontColor, inputType)
    self.m_keyboardObj:SetMaxLength(self.m_maxLength)
    self.m_keyboardObj:Show()
    self.m_currentRenderFx:InvokeASCallback(self.m_currentTextEdit, "lua2fs_noitfyShowKeyboard", true)
    if self.TouchInputCallback then
      self.TouchInputCallback()
    end
  elseif cmd == "FS_TEXTEDIT,changeKeyBoardHeight" then
    local _x, _y, _w, _h = unpack(LuaUtils:string_split(arg, "\001"))
    if self.m_stageWD and ext.IsArm64 then
      _x = self:convertXToScreenCoord(tonumber(_x), true)
      _w = self:convertXToScreenCoord(tonumber(_w))
    end
    if self.m_stageHT and ext.IsArm64 then
      _y = self:convertYToScreenCoord(tonumber(_y))
      _h = self:convertYToScreenCoord(tonumber(_h))
    end
    self.m_keyboardObj:SetPos(_x, _y, _w, _h)
  elseif cmd == "send_message" then
    DebugOut("HideKeyboard  333333")
    GameTextEdit.TouchInInput = true
  end
end
