require("data1/InterstellarAdventureConst.tfl")
require("data1/InterstellarAdventureAward.tfl")
require("data1/InterstellarSpace.tfl")
local InterstellarAdventure = LuaObjectManager:GetLuaObject("InterstellarAdventure")
local GameStateInterstellar = GameStateManager.GameStateInterstellar
local GameStateMainPlanet = GameStateManager.GameStateMainPlanet
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUIWDStuff = LuaObjectManager:GetLuaObject("GameUIWDStuff")
function InterstellarAdventure:Init()
  self.mInterstellarSpaces = nil
  self.mCurrentSpaceIndex = -1
  self.mCurrentSelectSpaceIndex = -1
  self.mCurrentSpaceID = -1
  self.mCurrentCoinID = nil
  self.mCurrentActivitySubID = -1
  self.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
end
function InterstellarAdventure:Reset()
  self.mInterstellarSpaces = nil
  self.mCurrentSpaceIndex = -1
  self.mCurrentSelectSpaceIndex = -1
  self.mCurrentSpaceID = -1
  self.mCurrentCoinID = nil
  self.mCurrentRefreshCost = -1
  self.mCurrentActivitySubID = -1
end
function InterstellarAdventure:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  mCurrentSelectSpaceIndex = -1
  local lang = "en"
  if GameSettingData and GameSettingData.Save_Lang then
    local la = GameSettingData.Save_Lang
    if string.find(la, "ru") == 1 then
      lang = "ru"
    end
  end
  self:GetFlashObject():InvokeASCallback("_root", "initLanguageFrame", lang)
  GameGlobalData:RegisterDataChangeCallback("item_count", self.RefreshItemCount)
end
function InterstellarAdventure:OnEraseFromGameState()
  self:UnloadFlashObject()
  collectgarbage("collect")
  GameGlobalData:RemoveDataChangeCallback("item_count", self.RefreshItemCount)
end
function InterstellarAdventure.RefreshItemCount()
  local flashObj = InterstellarAdventure:GetFlashObject()
  if not flashObj then
    return
  end
  InterstellarAdventure:SetUpSpaceSelectUI()
end
function InterstellarAdventure:EnterInterstellarAdventure(activityID)
  self.mCurrentActivitySubID = activityID
  local requestParam = {}
  requestParam.activity_id = activityID
  NetMessageMgr:SendMsg(NetAPIList.enter_plante_req.Code, requestParam, self.EnterInterstellarAdventureCallback, true)
end
function InterstellarAdventure.EnterInterstellarAdventureCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.enter_plante_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.enter_plante_ack.Code then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateInterstellar)
    InterstellarAdventure.mCurrentCoinID = content.item_id
    InterstellarAdventure.mCurrentSpaceID = content.star_id
    InterstellarAdventure.mCurrentRefreshCost = content.refresh_cost
    InterstellarAdventure.mInterstellarSpaces = {}
    local spaceTable = content.star
    local sortFun = function(a, b)
      return a.star_id < b.star_id
    end
    table.sort(spaceTable, sortFun)
    for i, v in ipairs(spaceTable) do
      InterstellarAdventure:AddSpace(v)
    end
    InterstellarAdventure:CalcCurrentIndex()
    InterstellarAdventure:SetUpSpaceSelectUI()
    InterstellarAdventure:RealShowSpaceSelectUI()
    return true
  end
  return false
end
function InterstellarAdventure:QuitInterstellarAdventure()
  self:Reset()
end
function InterstellarAdventure.RefreshInterstellarAdventure()
  local requestParam = {}
  requestParam.activity_id = InterstellarAdventure.mCurrentActivitySubID
  NetMessageMgr:SendMsg(NetAPIList.plante_refresh_req.Code, requestParam, InterstellarAdventure.RefreshInterstellarAdventureCallback, true)
end
function InterstellarAdventure.RefreshInterstellarAdventureCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.plante_refresh_req.Code then
    GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    return true
  elseif msgType == NetAPIList.plante_refresh_ack.Code then
    InterstellarAdventure.mCurrentCoinID = content.item_id
    InterstellarAdventure.mCurrentSpaceID = content.star_id
    InterstellarAdventure.mCurrentRefreshCost = content.refresh_cost
    InterstellarAdventure.mInterstellarSpaces = {}
    local spaceTable = content.star
    local sortFun = function(a, b)
      return a.star_id < b.star_id
    end
    table.sort(spaceTable, sortFun)
    for i, v in ipairs(spaceTable) do
      InterstellarAdventure:AddSpace(v)
    end
    InterstellarAdventure:CalcCurrentIndex()
    InterstellarAdventure:SetUpSpaceSelectUI()
    return true
  end
  return false
end
function InterstellarAdventure:AddSpace(netContent)
  local space = InterstellarSpace.new(netContent.star_id, netContent.all_award, netContent.cdtimes, netContent.cost_type, netContent.cost_num)
  space:Init(netContent.all_award, self.mCurrentSpaceID)
  table.insert(self.mInterstellarSpaces, space)
end
function InterstellarAdventure:CalcCurrentIndex()
  for i, v in ipairs(self.mInterstellarSpaces) do
    if v.mID == self.mCurrentSpaceID then
      self.mCurrentSpaceIndex = i
    end
  end
end
function InterstellarAdventure:RealShowSpaceSelectUI()
  DebugOut("show select UI 11111")
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  DebugOut("show select UI")
  flashObj:InvokeASCallback("_root", "showSelectSpaceUI", true)
end
function InterstellarAdventure:QuitSpaceSelectUI()
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "showSelectSpaceUI", false)
end
function InterstellarAdventure:SetUpSpaceSelectUI()
  if not self.mInterstellarSpaces then
    return
  end
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  local resource = GameGlobalData:GetData("resource")
  local itemsCount = GameGlobalData:GetData("item_count")
  local coinCount = 0
  local creditFrame = "credit"
  if itemsCount then
    for i, v in ipairs(itemsCount.items) do
      if self.mCurrentCoinID and v.item_id == self.mCurrentCoinID then
        coinCount = v.item_no
        break
      end
    end
  end
  local coinItem = GameUtils:MakeGameitem("item", self.mCurrentCoinID, coinCount, nil)
  local coinFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(coinItem)
  flashObj:InvokeASCallback("_root", "setUpSelectSpace", self.mInterstellarSpaces, creditFrame, GameUtils.numberConversion(resource.credit), coinFrame, coinCount, self.mCurrentRefreshCost)
end
function InterstellarAdventure:EnterSpace(spaceIndex)
  self.mCurrentSelectSpaceIndex = spaceIndex
  self:SetUpCurrentSpaceUI()
  self.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "showSpaceUI", true, spaceIndex)
end
function InterstellarAdventure:QuitSpace()
  self:CalcCurrentIndex()
  self:SetUpSpaceSelectUI()
  self.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "showSpaceUI", false, self.mCurrentSelectSpaceIndex)
  self.mCurrentSelectSpaceIndex = -1
end
function InterstellarAdventure:SlideToPrevSpace()
end
function InterstellarAdventure:SlideToNextSpace()
end
function InterstellarAdventure:OnFSCommand(cmd, arg)
  if cmd == "willSlideToPrev" then
  elseif cmd == "willSlideToNext" then
  elseif cmd == "didSlideToNext" then
  elseif cmd == "didSlideToNext" then
  elseif cmd == "doAdventure" then
    local curSpace = InterstellarAdventure.mInterstellarSpaces[InterstellarAdventure.mCurrentSpaceID]
    local costDesc = curSpace.mCurrentAdventureCostType
    if costDesc == "credit" then
      local confirmTxt = GameLoader:GetGameText("LC_MENU_INTERSTELLAR_HUNT_EXPLORE_COST")
      self:CreateCostConfirmPanel(confirmTxt, curSpace.mCurrentAdventureCostValue, self.OldDoAdventure)
    else
      self.DoAdventure(0)
    end
  elseif cmd == "onAdventureWillEnd" then
    self:AwardGetAnim(tonumber(arg))
  elseif cmd == "onAdventureDidEnd" then
    self.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
  elseif cmd == "enterSpace" then
    self:EnterSpace(tonumber(arg))
  elseif cmd == "nextSectionType" then
    self:RefreshSpaceUI(self.mCurrentSelectSpaceIndex)
  elseif cmd == "refreshAllSpace" then
    local confirmTxt = GameLoader:GetGameText("LC_MENU_INTERSTELLAR_HUNT_REFRESH_EXPLORE")
    self:CreateCostConfirmPanel(confirmTxt, self.mCurrentRefreshCost, self.RefreshInterstellarAdventure)
  elseif cmd == "quitSpace" then
    self:QuitSpace()
  elseif cmd == "quitSelectSpace" then
    self:QuitSpaceSelectUI()
  elseif cmd == "helpClicked" then
    GameUIWDStuff:Show(GameUIWDStuff.menuType.menu_type_help)
    GameUIWDStuff:SetHelpText(GameLoader:GetGameText("LC_MENU_SA_HELP_TEXT"))
  elseif cmd == "clickAwardItem" then
    local curSelectSpace = self.mInterstellarSpaces[self.mCurrentSelectSpaceIndex]
    local curClickedAward = curSelectSpace.mAwardsList[tonumber(arg)]
    self:ShowItemDetil(curClickedAward.mAwardItem)
  elseif cmd == "close_menu" then
    GameStateManager:SetCurrentGameState(GameStateManager.GameStateInterstellar.lastState)
  end
end
function InterstellarAdventure:SetUpCurrentSpaceUI()
  self:SetUpSpaceUI(self.mCurrentSelectSpaceIndex)
end
function InterstellarAdventure:RefreshNeibourSpace()
end
function InterstellarAdventure:SetUpSpaceUI(spaceIndex)
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  local spaceData = self.mInterstellarSpaces[spaceIndex]
  spaceData:GenerateAllAwardsDisplayData()
  local awardList = spaceData:GetAwardsList()
  local restIndexList = spaceData:GetRestAwardIndexList()
  local disPlayDesc, disPlayFrame = spaceData:GetCurrentDisplayData()
  local isCurSpace = 0
  if self.mCurrentSpaceID == spaceData.mID then
    isCurSpace = 1
  end
  flashObj:InvokeASCallback("_root", "setUpSpaceUI", awardList, restIndexList, disPlayDesc, disPlayFrame, isCurSpace, spaceData.mName)
end
function InterstellarAdventure:RefreshSpaceUI(spaceIndex)
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  local spaceData = self.mInterstellarSpaces[spaceIndex]
  DebugOut("test:" .. spaceIndex)
  DebugTable(spaceData)
  if spaceData then
    spaceData:GenerateAllAwardsDisplayData()
    local awardList = spaceData:GetAwardsList()
    local restIndexList = spaceData:GetRestAwardIndexList()
    flashObj:InvokeASCallback("_root", "refreshRestPanel", awardList, restIndexList)
  end
end
function InterstellarAdventure.DoAdventure(stateId)
  local requestParam = {}
  requestParam.activity_id = InterstellarAdventure.mCurrentActivitySubID
  requestParam.star_id = InterstellarAdventure.mCurrentSpaceID
  requestParam.state = stateId
  NetMessageMgr:SendMsg(NetAPIList.plante_explore_req.Code, requestParam, InterstellarAdventure.DoAdventureCallback, true)
  local curSpace = InterstellarAdventure.mInterstellarSpaces[InterstellarAdventure.mCurrentSpaceID]
  InterstellarAdventure.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_DOING
end
function InterstellarAdventure.DoAdventureCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.plante_explore_req.Code then
    if 0 ~= content.code and nil ~= content.code then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
      local flashObj = InterstellarAdventure:GetFlashObject()
      if not flashObj then
        return true
      end
      flashObj:InvokeASCallback("_root", "stopAdventure")
      InterstellarAdventure.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
      return true
    end
  elseif msgType == NetAPIList.plante_explore_ack.Code then
    if content then
      local curSpace = InterstellarAdventure.mInterstellarSpaces[InterstellarAdventure.mCurrentSpaceID]
      local curSectionIndex = curSpace:GetIndexInRestList(content.award_id)
      if not curSpace:IsFinalAwards() then
        InterstellarAdventure:StartAdventure()
      end
      local nextSpaceID = content.star_id
      if nextSpaceID ~= InterstellarAdventure.mCurrentSpaceID then
        InterstellarAdventure.mCurrentSpaceID = nextSpaceID
        local nextSpace = InterstellarAdventure.mInterstellarSpaces[content.star_id]
        nextSpace:RefreshSpace(content.cd_time, content.cost_type, content.cost_num)
      end
      curSpace:GetAwards(content.award_id)
      curSpace:RefreshSpace(content.cd_time, content.cost_type, content.cost_num)
      local nextSpaceID = content.star_id
      DebugOut("setTargetSectionIndex = ")
      local flashObj = InterstellarAdventure:GetFlashObject()
      if not flashObj then
        return true
      end
      if curSpace:IsAllAwardsGet() then
        DebugOut("IsAllAwardsGet truw")
        InterstellarAdventure.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
        flashObj:InvokeASCallback("_root", "setTargetSectionIndex", -1, content.award_id)
        flashObj:InvokeASCallback("_root", "onAdventureWillEnd", content.award_id)
      else
        DebugOut("IsAllAwardsGet false")
        flashObj:InvokeASCallback("_root", "setTargetSectionIndex", curSectionIndex, content.award_id)
      end
    else
      InterstellarAdventure.mCurState = EInterstellarAdventureState.ADVENTURE_STATE_IDLE
      local flashObj = InterstellarAdventure:GetFlashObject()
      if not flashObj then
        return true
      end
      flashObj:InvokeASCallback("_root", "stopAdventure")
    end
    return true
  elseif msgType == NetAPIList.credit_exchange_ack.Code and content.api == NetAPIList.plante_explore_req.Code then
    DebugOut("credit_exchange_ack:--plante_explore_req: ")
    DebugTable(content)
    local itemName = GameHelper:GetAwardNameText(content.item.item_type, content.item.number)
    local str = string.gsub(GameLoader:GetGameText("LC_MENU_LACK_OF_RESOURCES"), "<name1>", itemName)
    local infoText = string.gsub(str, "<name2>", content.credit)
    GameUtils:CreditCostConfirm(infoText, InterstellarAdventure.CostCreditDoneThisAction, true, nil)
    return true
  end
  return false
end
function InterstellarAdventure.OldDoAdventure(...)
  InterstellarAdventure.DoAdventure(0)
end
function InterstellarAdventure.CostCreditDoneThisAction(...)
  InterstellarAdventure.DoAdventure(1)
end
function InterstellarAdventure:AwardGetAnim(awardIndex)
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return true
  end
  flashObj:InvokeASCallback("_root", "awardGetAnim", tonumber(awardIndex))
  if self.mCurrentSelectSpaceIndex and self.mCurrentSelectSpaceIndex ~= -1 then
    local curSpace = self.mInterstellarSpaces[self.mCurrentSelectSpaceIndex]
    local curAward = curSpace.mAwardsList[tonumber(awardIndex)].mAwardItem
    local param = {}
    table.insert(param, curAward)
    ItemBox:ShowRewardBox(param)
  end
end
function InterstellarAdventure:StartAdventure()
  local flashObj = self:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:InvokeASCallback("_root", "setTargetSectionIndex", 4)
  flashObj:InvokeASCallback("_root", "onEnterSpeedUP")
end
function InterstellarAdventure:Update(dt)
  if not self:GetFlashObject() then
    return
  end
  local disPlayFrame
  local disPlayDesc = ""
  if self.mCurState == EInterstellarAdventureState.ADVENTURE_STATE_IDLE then
    if self.mInterstellarSpaces then
      local space = self.mInterstellarSpaces[self.mCurrentSelectSpaceIndex]
      if space then
        if space.mID ~= self.mCurrentSpaceID then
          disPlayFrame = nil
          disPlayDesc = ""
        else
          disPlayDesc, disPlayFrame = space:GetCurrentDisplayData()
        end
      end
    end
    self:GetFlashObject():InvokeASCallback("_root", "OnUpdateIdle", dt, disPlayDesc, disPlayFrame)
  else
    self:GetFlashObject():InvokeASCallback("_root", "onUpdateAdventure", dt)
  end
  self:GetFlashObject():Update(dt)
end
function InterstellarAdventure:CreateCostConfirmPanel(costStr, costValue, confirmCallback)
  local confirmingText = string.format(costStr, costValue)
  GameUtils:CreditCostConfirm(confirmingText, confirmCallback, true, nil)
end
function InterstellarAdventure:ShowItemDetil(item_)
  if GameHelper:IsResource(item_type) then
    return
  end
  local item = item_
  local item_type = item.item_type
  if item_type == "krypton" then
    if item.level == 0 then
      item.level = 1
    end
    local detail = GameUIKrypton:TryQueryKryptonDetail(item.number, item.level, function(msgType, content)
      local ret = GameUIKrypton.QueryKryptonDetailCallBack(msgType, content)
      if ret then
        local tmp = GameGlobalData:GetKryptonDetail(item.number, item.level)
        if tmp == nil then
          return false
        end
        ItemBox:SetKryptonBox(tmp, item.number)
        local operationTable = {}
        operationTable.btnUnloadVisible = false
        operationTable.btnUpgradeVisible = false
        operationTable.btnUseVisible = false
        operationTable.btnDecomposeVisible = false
        ItemBox:ShowKryptonBox(320, 240, operationTable)
      end
      return ret
    end)
    if detail == nil then
    else
      ItemBox:SetKryptonBox(detail, item.number)
      local operationTable = {}
      operationTable.btnUnloadVisible = false
      operationTable.btnUpgradeVisible = false
      operationTable.btnUseVisible = false
      operationTable.btnDecomposeVisible = false
      ItemBox:ShowKryptonBox(320, 240, operationTable)
    end
  end
  if item_type == "fleet" then
    ItemBox:ShowCommanderDetail2(tonumber(item.number))
  end
  ItemBox:ShowGameItem(item)
end
if AutoUpdate.isAndroidDevice then
  function InterstellarAdventure.OnAndroidBack()
    if InterstellarAdventure.mCurrentSelectSpaceIndex ~= -1 then
      InterstellarAdventure:OnFSCommand("quitSpace")
    else
      InterstellarAdventure:OnFSCommand("quitSelectSpace")
    end
  end
end
