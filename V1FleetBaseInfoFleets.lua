local Fleets = GameData.FleetBaseInfo.Fleets
Fleets[1] = {
  FLEETID = 1,
  AVATAR = "player",
  NAME = "NPC_13",
  DES = "DES_13",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_17",
  SHIP = "ship17"
}
Fleets[2] = {
  FLEETID = 2,
  AVATAR = "head1",
  NAME = "NPC_2",
  DES = "DES_2",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_18",
  SHIP = "ship18"
}
Fleets[3] = {
  FLEETID = 3,
  AVATAR = "fu  guan",
  NAME = "NPC_13",
  DES = "DES_13",
  VESSELS_NAME = "NAME_3",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[4] = {
  FLEETID = 4,
  AVATAR = "head14",
  NAME = "NPC_6",
  DES = "DES_6",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_19",
  SHIP = "ship19"
}
Fleets[5] = {
  FLEETID = 5,
  AVATAR = "head16",
  NAME = "NPC_11",
  DES = "DES_11",
  VESSELS_NAME = "NAME_5",
  PIC = "fleet_47",
  SHIP = "ship47"
}
Fleets[6] = {
  FLEETID = 6,
  AVATAR = "head22",
  NAME = "NPC_8",
  DES = "DES_8",
  VESSELS_NAME = "NAME_6",
  PIC = "fleet_42",
  SHIP = "ship42"
}
Fleets[7] = {
  FLEETID = 7,
  AVATAR = "head2",
  NAME = "NPC_12",
  DES = "DES_12",
  VESSELS_NAME = "NAME_7",
  PIC = "fleet_5",
  SHIP = "ship5"
}
Fleets[8] = {
  FLEETID = 8,
  AVATAR = "head9",
  NAME = "NPC_7",
  DES = "DES_7",
  VESSELS_NAME = "NAME_8",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[9] = {
  FLEETID = 9,
  AVATAR = "head19",
  NAME = "NPC_30",
  DES = "DES_30",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_49",
  SHIP = "ship49"
}
Fleets[10] = {
  FLEETID = 10,
  AVATAR = "head6",
  NAME = "NPC_3",
  DES = "DES_3",
  VESSELS_NAME = "NAME_10",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[11] = {
  FLEETID = 11,
  AVATAR = "head28",
  NAME = "NPC_37",
  DES = "DES_37",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_41",
  SHIP = "ship41"
}
Fleets[12] = {
  FLEETID = 12,
  AVATAR = "head27",
  NAME = "NPC_38",
  DES = "DES_38",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_38",
  SHIP = "ship38"
}
Fleets[13] = {
  FLEETID = 13,
  AVATAR = "head21",
  NAME = "NPC_36",
  DES = "DES_36",
  VESSELS_NAME = "NAME_3",
  PIC = "fleet_49",
  SHIP = "ship49"
}
Fleets[14] = {
  FLEETID = 14,
  AVATAR = "head31",
  NAME = "NPC_26",
  DES = "DES_26",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[15] = {
  FLEETID = 15,
  AVATAR = "head15",
  NAME = "NPC_34",
  DES = "DES_34",
  VESSELS_NAME = "NAME_5",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[16] = {
  FLEETID = 16,
  AVATAR = "head32",
  NAME = "NPC_28",
  DES = "DES_28",
  VESSELS_NAME = "NAME_6",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[17] = {
  FLEETID = 17,
  AVATAR = "head41",
  NAME = "NPC_19",
  DES = "DES_19",
  VESSELS_NAME = "NAME_7",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[18] = {
  FLEETID = 18,
  AVATAR = "head40",
  NAME = "NPC_17",
  DES = "DES_17",
  VESSELS_NAME = "NAME_8",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[19] = {
  FLEETID = 19,
  AVATAR = "head34",
  NAME = "NPC_21",
  DES = "DES_21",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[23] = {
  FLEETID = 23,
  AVATAR = "head42",
  NAME = "NPC_16",
  DES = "DES_16",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_37",
  SHIP = "ship37"
}
Fleets[24] = {
  FLEETID = 24,
  AVATAR = "head41",
  NAME = "NPC_19",
  DES = "DES_19",
  VESSELS_NAME = "NAME_10",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[25] = {
  FLEETID = 25,
  AVATAR = "head37",
  NAME = "NPC_21",
  DES = "DES_21",
  VESSELS_NAME = "NAME_10",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[101] = {
  FLEETID = 101,
  AVATAR = "head17",
  NAME = "NPC_1",
  DES = "DES_1",
  VESSELS_NAME = "NAME_11",
  PIC = "fleet_41",
  SHIP = "ship41"
}
Fleets[102] = {
  FLEETID = 102,
  AVATAR = "player",
  NAME = "TUTORIAL_FORCE_3",
  DES = "TUTORIAL_FORCE_3",
  VESSELS_NAME = "NAME_12",
  PIC = "fleet_17",
  SHIP = "ship17"
}
Fleets[103] = {
  FLEETID = 103,
  AVATAR = "head13",
  NAME = "TUTORIAL_FORCE_3",
  DES = "TUTORIAL_FORCE_3",
  VESSELS_NAME = "NAME_13",
  PIC = "fleet_37",
  SHIP = "ship37"
}
Fleets[104] = {
  FLEETID = 104,
  AVATAR = "head13",
  NAME = "TUTORIAL_FORCE_3",
  DES = "TUTORIAL_FORCE_3",
  VESSELS_NAME = "NAME_14",
  PIC = "fleet_37",
  SHIP = "ship37"
}
Fleets[105] = {
  FLEETID = 105,
  AVATAR = "head13",
  NAME = "TUTORIAL_FORCE_3",
  DES = "TUTORIAL_FORCE_3",
  VESSELS_NAME = "NAME_15",
  PIC = "fleet_37",
  SHIP = "ship37"
}
Fleets[106] = {
  FLEETID = 106,
  AVATAR = "head13",
  NAME = "NPC_4",
  DES = "DES_4",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[107] = {
  FLEETID = 107,
  AVATAR = "head7",
  NAME = "NPC_13",
  DES = "DES_13",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_38",
  SHIP = "ship38"
}
Fleets[111] = {
  FLEETID = 111,
  AVATAR = "head14",
  NAME = "NPC_6",
  DES = "DES_6",
  VESSELS_NAME = "NAME_5",
  PIC = "fleet_41",
  SHIP = "ship41"
}
Fleets[112] = {
  FLEETID = 112,
  AVATAR = "head22",
  NAME = "NPC_8",
  DES = "DES_8",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_42",
  SHIP = "ship42"
}
Fleets[113] = {
  FLEETID = 113,
  AVATAR = "head16",
  NAME = "NPC_11",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[114] = {
  FLEETID = 114,
  AVATAR = "head27",
  NAME = "NPC_38",
  DES = "DES_38",
  VESSELS_NAME = "NAME_5",
  PIC = "fleet_47",
  SHIP = "ship47"
}
Fleets[115] = {
  FLEETID = 115,
  AVATAR = "head1",
  NAME = "NPC_2",
  DES = "DES_2",
  VESSELS_NAME = "NAME_3",
  PIC = "fleet_48",
  SHIP = "ship48"
}
Fleets[116] = {
  FLEETID = 116,
  AVATAR = "head21",
  NAME = "NPC_36",
  DES = "DES_36",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_47",
  SHIP = "ship47"
}
Fleets[117] = {
  FLEETID = 117,
  AVATAR = "head1001",
  NAME = "NPC_54",
  DES = "DES_20",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_1001",
  SHIP = "ship1001"
}
Fleets[118] = {
  FLEETID = 118,
  AVATAR = "head48",
  NAME = "NPC_118",
  DES = "DES_118",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_51",
  SHIP = "ship51"
}
Fleets[119] = {
  FLEETID = 119,
  AVATAR = "head49",
  NAME = "NPC_119",
  DES = "DES_119",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_52",
  SHIP = "ship52"
}
Fleets[120] = {
  FLEETID = 120,
  AVATAR = "head50",
  NAME = "NPC_120",
  DES = "DES_120",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_53",
  SHIP = "ship53"
}
Fleets[121] = {
  FLEETID = 121,
  AVATAR = "head51",
  NAME = "NPC_121",
  DES = "DES_121",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_54",
  SHIP = "ship54"
}
Fleets[122] = {
  FLEETID = 122,
  AVATAR = "head52",
  NAME = "NPC_122",
  DES = "DES_122",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_55",
  SHIP = "ship55"
}
Fleets[123] = {
  FLEETID = 123,
  AVATAR = "head53",
  NAME = "NPC_123",
  DES = "DES_123",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_56",
  SHIP = "ship56"
}
Fleets[124] = {
  FLEETID = 124,
  AVATAR = "head54",
  NAME = "NPC_124",
  DES = "DES_124",
  VESSELS_NAME = "NAME_2",
  PIC = "fleet_59",
  SHIP = "ship59"
}
Fleets[125] = {
  FLEETID = 125,
  AVATAR = "head55",
  NAME = "NPC_125",
  DES = "DES_125",
  VESSELS_NAME = "NAME_3",
  PIC = "fleet_58",
  SHIP = "ship58"
}
Fleets[126] = {
  FLEETID = 126,
  AVATAR = "head56",
  NAME = "NPC_126",
  DES = "DES_126",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_57",
  SHIP = "ship57"
}
Fleets[127] = {
  FLEETID = 127,
  AVATAR = "head57",
  NAME = "NPC_127",
  DES = "DES_127",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_60",
  SHIP = "ship60"
}
Fleets[128] = {
  FLEETID = 128,
  AVATAR = "head58",
  NAME = "NPC_128",
  DES = "DES_128",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_61",
  SHIP = "ship61"
}
Fleets[129] = {
  FLEETID = 129,
  AVATAR = "head59",
  NAME = "NPC_129",
  DES = "DES_129",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_62",
  SHIP = "ship62"
}
Fleets[130] = {
  FLEETID = 130,
  AVATAR = "head60",
  NAME = "NPC_130",
  DES = "DES_130",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_63",
  SHIP = "ship63"
}
Fleets[131] = {
  FLEETID = 131,
  AVATAR = "head61",
  NAME = "NPC_131",
  DES = "DES_131",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_64",
  SHIP = "ship64"
}
Fleets[132] = {
  FLEETID = 132,
  AVATAR = "head62",
  NAME = "NPC_132",
  DES = "DES_132",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_65",
  SHIP = "ship65"
}
Fleets[133] = {
  FLEETID = 133,
  AVATAR = "head63",
  NAME = "NPC_133",
  DES = "DES_133",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_66",
  SHIP = "ship66"
}
Fleets[134] = {
  FLEETID = 134,
  AVATAR = "head64",
  NAME = "NPC_134",
  DES = "DES_134",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_67",
  SHIP = "ship67"
}
Fleets[135] = {
  FLEETID = 135,
  AVATAR = "head65",
  NAME = "NPC_138",
  DES = "DES_138",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_49",
  SHIP = "ship49"
}
Fleets[136] = {
  FLEETID = 136,
  AVATAR = "head65",
  NAME = "NPC_136",
  DES = "DES_136",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_68",
  SHIP = "ship68"
}
Fleets[137] = {
  FLEETID = 137,
  AVATAR = "head66",
  NAME = "NPC_137",
  DES = "DES_137",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_69",
  SHIP = "ship69"
}
Fleets[138] = {
  FLEETID = 138,
  AVATAR = "head67",
  NAME = "NPC_138",
  DES = "DES_138",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_70",
  SHIP = "ship70"
}
Fleets[139] = {
  FLEETID = 139,
  AVATAR = "head68",
  NAME = "NPC_139",
  DES = "DES_139",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_71",
  SHIP = "ship71"
}
Fleets[140] = {
  FLEETID = 140,
  AVATAR = "head69",
  NAME = "NPC_140",
  DES = "DES_140",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_72",
  SHIP = "ship72"
}
Fleets[141] = {
  FLEETID = 141,
  AVATAR = "head70",
  NAME = "NPC_141",
  DES = "DES_141",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_73",
  SHIP = "ship73"
}
Fleets[142] = {
  FLEETID = 142,
  AVATAR = "head71",
  NAME = "NPC_142",
  DES = "DES_142",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_74",
  SHIP = "ship74"
}
Fleets[143] = {
  FLEETID = 143,
  AVATAR = "head72",
  NAME = "NPC_143",
  DES = "DES_143",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_75",
  SHIP = "ship75"
}
Fleets[144] = {
  FLEETID = 144,
  AVATAR = "head73",
  NAME = "NPC_144",
  DES = "DES_144",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_76",
  SHIP = "ship76"
}
Fleets[145] = {
  FLEETID = 145,
  AVATAR = "head74",
  NAME = "NPC_145",
  DES = "DES_145",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_77",
  SHIP = "ship77"
}
Fleets[146] = {
  FLEETID = 146,
  AVATAR = "head75",
  NAME = "NPC_146",
  DES = "DES_146",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_78",
  SHIP = "ship78"
}
Fleets[147] = {
  FLEETID = 147,
  AVATAR = "head76",
  NAME = "NPC_147",
  DES = "DES_147",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_79",
  SHIP = "ship79"
}
Fleets[148] = {
  FLEETID = 148,
  AVATAR = "head77",
  NAME = "NPC_148",
  DES = "DES_148",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_80",
  SHIP = "ship80"
}
Fleets[149] = {
  FLEETID = 149,
  AVATAR = "head78",
  NAME = "NPC_149",
  DES = "DES_149",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_81",
  SHIP = "ship81"
}
Fleets[21] = {
  FLEETID = 21,
  AVATAR = "head63",
  NAME = "NPC_136",
  DES = "DES_136",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_66",
  SHIP = "ship66"
}
Fleets[22] = {
  FLEETID = 22,
  AVATAR = "head64",
  NAME = "NPC_137",
  DES = "DES_137",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_67",
  SHIP = "ship67"
}
Fleets[202] = {
  FLEETID = 202,
  AVATAR = "head8",
  NAME = "NPC_9",
  DES = "DES_9",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_48",
  SHIP = "ship48"
}
Fleets[203] = {
  FLEETID = 203,
  AVATAR = "head7",
  NAME = "NPC_13",
  DES = "DES_13",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_37",
  SHIP = "ship37"
}
Fleets[205] = {
  FLEETID = 205,
  AVATAR = "head19",
  NAME = "NPC_30",
  DES = "DES_30",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_20",
  SHIP = "ship20"
}
Fleets[206] = {
  FLEETID = 206,
  AVATAR = "head29",
  NAME = "NPC_35",
  DES = "DES_35",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_42",
  SHIP = "ship42"
}
Fleets[207] = {
  FLEETID = 207,
  AVATAR = "head7",
  NAME = "NPC_13",
  DES = "DES_13",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_42",
  SHIP = "ship42"
}
Fleets[208] = {
  FLEETID = 208,
  AVATAR = "head18",
  NAME = "NPC_31",
  DES = "DES_31",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_48",
  SHIP = "ship48"
}
Fleets[209] = {
  FLEETID = 209,
  AVATAR = "head5",
  NAME = "NPC_4",
  DES = "DES_4",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[210] = {
  FLEETID = 210,
  AVATAR = "head33",
  NAME = "NPC_23",
  DES = "DES_23",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[211] = {
  FLEETID = 211,
  AVATAR = "head42",
  NAME = "NPC_16",
  DES = "DES_16",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_16",
  SHIP = "ship16"
}
Fleets[212] = {
  FLEETID = 212,
  AVATAR = "head4",
  NAME = "NPC_5",
  DES = "DES_5",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_48",
  SHIP = "ship48"
}
Fleets[213] = {
  FLEETID = 213,
  AVATAR = "head28",
  NAME = "NPC_37",
  DES = "DES_37",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[214] = {
  FLEETID = 214,
  AVATAR = "head27",
  NAME = "NPC_38",
  DES = "DES_38",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[215] = {
  FLEETID = 215,
  AVATAR = "head21",
  NAME = "NPC_36",
  DES = "DES_36",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[216] = {
  FLEETID = 216,
  AVATAR = "head31",
  NAME = "NPC_26",
  DES = "DES_26",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_2",
  SHIP = "ship2"
}
Fleets[217] = {
  FLEETID = 217,
  AVATAR = "head5",
  NAME = "NPC_4",
  DES = "DES_4",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[218] = {
  FLEETID = 218,
  AVATAR = "head15",
  NAME = "NPC_34",
  DES = "DES_34",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[219] = {
  FLEETID = 219,
  AVATAR = "head32",
  NAME = "NPC_28",
  DES = "DES_28",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[220] = {
  FLEETID = 220,
  AVATAR = "head33",
  NAME = "NPC_23",
  DES = "DES_23",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[221] = {
  FLEETID = 221,
  AVATAR = "head40",
  NAME = "NPC_17",
  DES = "DES_17",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[222] = {
  FLEETID = 222,
  AVATAR = "head42",
  NAME = "NPC_16",
  DES = "DES_16",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[223] = {
  FLEETID = 223,
  AVATAR = "head41",
  NAME = "NPC_19",
  DES = "DES_19",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[224] = {
  FLEETID = 224,
  AVATAR = "head37",
  NAME = "NPC_21",
  DES = "DES_21",
  VESSELS_NAME = "NAME_1",
  PIC = "fleet_12",
  SHIP = "ship12"
}
Fleets[1001] = {
  FLEETID = 1001,
  AVATAR = "head16",
  NAME = "NPC_1001",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1002] = {
  FLEETID = 1002,
  AVATAR = "head16",
  NAME = "NPC_1002",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1003] = {
  FLEETID = 1003,
  AVATAR = "head16",
  NAME = "NPC_1003",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1004] = {
  FLEETID = 1004,
  AVATAR = "head16",
  NAME = "NPC_1004",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1005] = {
  FLEETID = 1005,
  AVATAR = "head16",
  NAME = "NPC_1005",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1011] = {
  FLEETID = 1011,
  AVATAR = "head16",
  NAME = "NPC_1011",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1012] = {
  FLEETID = 1012,
  AVATAR = "head16",
  NAME = "NPC_1012",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1013] = {
  FLEETID = 1013,
  AVATAR = "head16",
  NAME = "NPC_1013",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1014] = {
  FLEETID = 1014,
  AVATAR = "head16",
  NAME = "NPC_1014",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
Fleets[1015] = {
  FLEETID = 1015,
  AVATAR = "head16",
  NAME = "NPC_1015",
  DES = "DES_11",
  VESSELS_NAME = "NAME_4",
  PIC = "fleet_43",
  SHIP = "ship43"
}
