local limit = GameData.vip_exp.limit
limit.one_key_collect = {
  limit = "one_key_collect",
  vip_level = 0,
  player_level = 20
}
limit.skip_battle = {
  limit = "skip_battle",
  vip_level = 5,
  player_level = 0
}
limit.krypton_auto_compose = {
  limit = "krypton_auto_compose",
  vip_level = 5,
  player_level = 50
}
limit.krypton_auto_decompose = {
  limit = "krypton_auto_decompose",
  vip_level = 5,
  player_level = 0
}
limit.no_enhance_cd = {
  limit = "no_enhance_cd",
  vip_level = 0,
  player_level = 0
}
limit.wd_skip_battle = {
  limit = "wd_skip_battle",
  vip_level = 5,
  player_level = 0
}
limit.one_key_equip_enhance = {
  limit = "one_key_equip_enhance",
  vip_level = 0,
  player_level = 20
}
limit.mine_speed_up = {
  limit = "mine_speed_up",
  vip_level = 6,
  player_level = 0
}
limit.clear_mine_atk_cd = {
  limit = "clear_mine_atk_cd",
  vip_level = 4,
  player_level = 0
}
limit.one_key_technology_update = {
  limit = "one_key_technology_update",
  vip_level = 0,
  player_level = 0
}
limit.remodel_speed_up = {
  limit = "remodel_speed_up",
  vip_level = 0,
  player_level = 0
}
limit.commmon_skip_battle = {
  limit = "commmon_skip_battle",
  vip_level = 0,
  player_level = 20
}
limit.ac_skip_battle = {
  limit = "ac_skip_battle",
  vip_level = 1,
  player_level = 0
}
limit.climbtower_skip_battle = {
  limit = "climbtower_skip_battle",
  vip_level = 2,
  player_level = 0
}
limit.arena_skip_battle = {
  limit = "arena_skip_battle",
  vip_level = 3,
  player_level = 0
}
