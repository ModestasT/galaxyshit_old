local GameStateRecruit = GameStateManager.GameStateRecruit
local GameUIRecruitMainNew = LuaObjectManager:GetLuaObject("GameUIRecruitMainNew")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local QuestTutorialRecruit = TutorialQuestManager.QuestTutorialRecruit
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameUICommonDialog = LuaObjectManager:GetLuaObject("GameUICommonDialog")
local GameUIRecruitTopbar = LuaObjectManager:GetLuaObject("GameUIRecruitTopbar")
local GameUIHeroUnion = LuaObjectManager:GetLuaObject("GameUIHeroUnion")
local GameUICollect = LuaObjectManager:GetLuaObject("GameUICollect")
GameUIRecruitMainNew._CommanderTable = nil
GameUIRecruitMainNew.defaultImg = "RecruitMain_card.png"
GameUIRecruitMainNew.curRewards = nil
GameUIRecruitMainNew.fleetList = {}
GameUIRecruitMainNew.itemList = {}
GameUIRecruitMainNew.curTabData = {}
GameUIRecruitMainNew.curShowUI = "main"
function GameUIRecruitMainNew:OnInitGame()
end
function GameUIRecruitMainNew:OnAddToGameState()
  DebugOut("GameUIRecruitMainNew:OnAddToGameState")
  GameUIRecruitMainNew.curShowUI = "main"
  self:LoadFlashObject()
  GameTimer:Add(self._OnTimerTick, 1000)
  GameUIRecruitMainNew:RefreshResource()
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  if QuestTutorialRecruit:IsActive() and not QuestTutorialRecruit:IsFinished() then
    GameUICommonDialog:PlayStory({100030, 100031}, nil)
  end
  local haveTutorial = QuestTutorialRecruit:IsActive() and not QuestTutorialRecruit:IsFinished()
  self:GetFlashObject():InvokeASCallback("_root", "setRecruitTutorial", haveTutorial)
end
function GameUIRecruitMainNew:OnEraseFromGameState()
  GameUIRecruitMainNew._CommanderTable = nil
  self:UnloadFlashObject()
  collectgarbage("collect")
end
function GameUIRecruitMainNew:OnFSCommand(cmd, arg)
  if cmd == "close" then
  elseif cmd == "update_list_item" then
    self:updateListItem(tonumber(arg))
  elseif cmd == "look_reward" then
    GameUIRecruitMainNew:RecruitLookReq(tonumber(arg))
  elseif cmd == "buy_one" then
    self:RecruitReq(tonumber(arg), 1)
    if QuestTutorialRecruit:IsActive() and not QuestTutorialRecruit:IsFinished() then
      QuestTutorialRecruit:SetFinish(true)
      self:GetFlashObject():InvokeASCallback("_root", "setRecruitTutorial", false)
      GameUIRecruitMainNew:UpdateCommanderList()
    end
  elseif cmd == "buy_ten" then
    self:RecruitReq(tonumber(arg), 10)
  elseif cmd == "ShowDetail" then
    local itemType, index = unpack(LuaUtils:string_split(arg, ":"))
    if itemType == "item" then
      DebugOut(self.curRewards, index)
      DebugTable(self.curRewards)
      local item = self.curRewards[tonumber(index)].item_reward
      item.cnt = 1
      ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
    else
      local fleetID = self.curRewards[tonumber(index)].fleet_id
      ItemBox:ShowCommanderDetail2(fleetID, nil, nil)
    end
  elseif cmd == "update_look_list_item" then
    self:updateLookItemList(tonumber(arg))
  elseif cmd == "select_tab" then
    self:SetLookReward(tonumber(arg))
  elseif cmd == "close_all" then
    GameUIRecruitTopbar:SetVisible(true)
    GameUIRecruitMainNew.curShowUI = "main"
  elseif cmd == "close_buy" then
    GameUIRecruitTopbar:SetVisible(true)
    GameUIRecruitMainNew.curShowUI = "main"
  end
end
function GameUIRecruitMainNew:RefreshResource()
  local resource = GameGlobalData:GetData("resource")
  if GameUIRecruitMainNew:GetFlashObject() then
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "setResource", GameUtils.numberConversion(resource.credit), GameUtils.numberConversion(resource.money))
  end
end
function GameUIRecruitMainNew:updateListItem(itemIndex)
  local itemdata = self._CommanderTable[itemIndex]
  if itemdata then
    local localPath = "data2/" .. DynamicResDownloader:GetFullName(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC)
    DebugOut("GameUIRecruitMainNew:updateListItem:", localPath, DynamicResDownloader:IsPngCrcCorrect(localPath), ext.crc32.crc32(localPath), type(ext.crc32.crc32(localPath)))
    if ext.crc32.crc32(localPath) ~= "" then
      itemdata.imgExist = true
    else
      local extendInfo = {}
      extendInfo.img = itemdata.bgImg
      extendInfo.itemIndex = itemIndex
      DynamicResDownloader:AddDynamicRes(itemdata.bgImg, DynamicResDownloader.resType.WELCOME_PIC, extendInfo, GameUIRecruitMainNew.DynamicBackgroundCallback)
      itemdata.imgExist = false
    end
    local extInfo_buy = {}
    extInfo_buy.index = itemIndex
    extInfo_buy.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_item.item_type, itemdata.baseData.buy_item.number, itemdata.baseData.buy_item.no)
    itemdata.buyItemFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_item, extInfo_buy, GameUIRecruitMainNew.buyIconCallBack)
    local extInfo_one = {}
    extInfo_one.index = itemIndex
    extInfo_one.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_one.item_type, itemdata.baseData.buy_price_one.number, itemdata.baseData.buy_price_one.no)
    itemdata.priceOneFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_one, extInfo_one, GameUIRecruitMainNew.priceOneCallBack)
    local extInfo_ten = {}
    extInfo_ten.index = k
    extInfo_ten.frame = GameHelper:GetAwardTypeIconFrameName(itemdata.baseData.buy_price_ten.item_type, itemdata.baseData.buy_price_ten.number, itemdata.baseData.buy_price_ten.no)
    itemdata.priceTenFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(itemdata.baseData.buy_price_ten, extInfo_ten, GameUIRecruitMainNew.priceTenCallBack)
    self:GetFlashObject():InvokeASCallback("_root", "updateListItem", itemIndex, itemdata)
    GameUIRecruitMainNew._OnTimerTick()
  end
end
function GameUIRecruitMainNew.DynamicBackgroundCallback(extinfo)
  if GameUIRecruitMainNew:GetFlashObject() then
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "setItemBg", extinfo.itemIndex, extinfo.img)
  end
end
function GameUIRecruitMainNew:UpdateGameData()
  local activeTabitem = GameStateRecruit:GetActiveTab()
  if activeTabitem ~= "warpgate" then
    if not self._CommanderTable then
      local param = {type = "fleet"}
      NetMessageMgr:SendMsg(NetAPIList.recruit_list_new_req.Code, param, self._NetMessageCallback, true, nil)
    else
      self:UpdateCommanderList()
    end
  end
end
function GameUIRecruitMainNew._NetMessageCallback(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_list_new_req.Code then
    if not GameUICollect:CheckTutorialCollect(content.code) and (content.code == 709 or content.code == 130) then
      local text = AlertDataList:GetTextFromErrorCode(130)
      local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
      GameUIKrypton.NeedMoreMoney(text)
    else
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.recruit_list_new_ack.Code then
    GameUIRecruitMainNew:GenerateCommanderList(content)
    return true
  end
  return false
end
function GameUIRecruitMainNew.buyIconCallBack(extInfo)
  if GameUIRecruitMainNew:GetFlashObject() then
    DebugOut("ajksdjkahsdahdsaksdjakj")
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "setBuyFrame", extInfo)
  end
end
function GameUIRecruitMainNew.priceOneCallBack(extInfo)
  if GameUIRecruitMainNew:GetFlashObject() then
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "setPriceOneFrame", extInfo)
  end
end
function GameUIRecruitMainNew.priceTenCallBack(extInfo)
  if GameUIRecruitMainNew:GetFlashObject() then
  end
end
function GameUIRecruitMainNew:GetItemByID(id)
  for k, v in pairs(self._CommanderTable or {}) do
    if v.id == id then
      return v
    end
  end
  return nil
end
function GameUIRecruitMainNew:GenerateCommanderList(content)
  local data = {}
  for k, v in ipairs(content.recruit_list or {}) do
    local item = {}
    item.buyItemFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
    item.buyNumber = GameHelper:GetAwardCount(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
    item.priceOneFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
    item.priceOneNumber = GameHelper:GetAwardCount(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
    item.priceTenFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
    item.priceTenNumber = GameHelper:GetAwardCount(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
    item.baseTime = os.time()
    item.nextFreeTime = v.next_free_time
    item.leftFreeCount = v.free_count
    item.desc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_TIP_" .. v.desc_key)
    item.nextDesc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_GET_COMMANDER_" .. v.desc_key)
    item.buyMustText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_MUST_GET"), "<number1>", "<font color='#FFCC00'>" .. v.must_count .. "</font>")
    item.activityTime = v.activity_time
    item.isActivity = v.is_activity
    item.id = v.id
    item.buyText = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_BUTTON")
    item.buyOneText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "1")
    item.buyTenText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "10")
    item.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
    item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
    item.bgImg = v.img
    item.baseData = v
    item.imgExist = true
    data[#data + 1] = item
  end
  self._CommanderTable = data
  self:UpdateCommanderList()
end
function GameUIRecruitMainNew:GenerateItemData(itemdata)
  local v = itemdata
  local item = {}
  item.buyItemFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.buyNumber = GameHelper:GetAwardCount(v.buy_item.item_type, v.buy_item.number, v.buy_item.no)
  item.priceOneFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceOneNumber = GameHelper:GetAwardCount(v.buy_price_one.item_type, v.buy_price_one.number, v.buy_price_one.no)
  item.priceTenFrame = GameHelper:GetAwardTypeIconFrameName(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.priceTenNumber = GameHelper:GetAwardCount(v.buy_price_ten.item_type, v.buy_price_ten.number, v.buy_price_ten.no)
  item.baseTime = os.time()
  item.nextFreeTime = v.next_free_time
  item.leftFreeCount = v.free_count
  item.desc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_TIP_" .. v.desc_key)
  item.nextDesc = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_GET_COMMANDER_" .. v.desc_key)
  item.buyMustText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_MUST_GET"), "<number1>", "<font color='#FFCC00'>" .. v.must_count .. "</font>")
  item.activityTime = v.activity_time
  item.isActivity = v.is_activity
  item.id = v.id
  item.buyText = GameLoader:GetGameText("LC_MENU_WELFAREE_PURCHASE_BUTTON")
  item.buyOneText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "1")
  item.buyTenText = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_PURCHASE"), "<number1>", "10")
  item.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
  item.loadText = GameLoader:GetGameText("LC_MENU_NEWS_DOWNLOAD_CHAR")
  item.bgImg = v.img
  item.baseData = v
  item.imgExist = true
  return item
end
function GameUIRecruitMainNew:UpdateCommanderList()
  if self:GetFlashObject() == nil then
    return
  end
  if QuestTutorialRecruit:IsActive() then
    self:GetFlashObject():InvokeASCallback("_root", "initListBox", 1)
  else
    self:GetFlashObject():InvokeASCallback("_root", "initListBox", #self._CommanderTable)
  end
end
function GameUIRecruitMainNew:SetVisible(mc_path, is_visible)
  mc_path = mc_path or -1
  self:GetFlashObject():InvokeASCallback("_root", "lua2fs_setVisible", mc_path, is_visible)
end
function GameUIRecruitMainNew._OnTimerTick()
  for k, v in pairs(GameUIRecruitMainNew._CommanderTable or {}) do
    local data = {}
    if v.isActivity then
      local Aleft = v.activityTime - (os.time() - v.baseTime)
      if Aleft < 0 then
        Aleft = 0
      end
      data.isActivity = v.isActivity
      data.activityTime = GameUtils:formatTimeStringAsPartion2(Aleft)
    end
    local Fleft = v.nextFreeTime - (os.time() - v.baseTime)
    if Fleft < 0 then
      Fleft = 0
    end
    if Fleft == 0 then
      data.isFree = true
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_FREE_TIME") .. v.leftFreeCount
    else
      data.isFree = false
      data.freeTip = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_CARD_COLDDOWN") .. GameUtils:formatTimeStringAsPartion2(Fleft)
    end
    if 0 >= v.leftFreeCount then
      data.freeTip = ""
      data.isFree = false
    end
    data.freeText = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_FREE")
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "updateItemTime", k, data)
  end
  return 1000
end
function GameUIRecruitMainNew:RecruitReq(id, count)
  local function callback()
    local param = {}
    param.id = id
    param.count = count
    NetMessageMgr:SendMsg(NetAPIList.recruit_req.Code, param, self.RecruitCallBack, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.recruit_req.Code, param, self.RecruitCallBack, true, nil)
  end
  local price = 0
  local item = GameUIRecruitMainNew:GetItemByID(id)
  if count == 1 and item and item.priceOneFrame == "credit" then
    price = item.priceOneNumber
  elseif count == 10 and item and item.priceTenFrame == "credit" then
    price = item.priceTenNumber
  end
  if GameSettingData.EnablePayRemind and price > 0 then
    local text_content = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_ASK")
    text_content = string.gsub(text_content, "<number1>", tostring(price))
    text_content = string.gsub(text_content, "<number2>", tostring(count))
    GameUtils:CreditCostConfirm(text_content, callback)
  else
    callback()
  end
  GameUIHeroUnion.heroUnionData = nil
end
function GameUIRecruitMainNew.RecruitCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_req.Code then
    if content.code ~= 0 then
      if content.code == 8604 then
        local GameUIKrypton = LuaObjectManager:GetLuaObject("GameUIKrypton")
        local text = AlertDataList:GetTextFromErrorCode(130)
        GameUIKrypton.NeedMoreMoney(text)
      else
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      end
    end
    return true
  elseif msgtype == NetAPIList.recruit_ack.Code then
    DebugOut("RecruitCallBack")
    DebugTable(content)
    local id = content.recruit_info.id
    local itemData, index = GameUIRecruitMainNew:GetItemDataById(id)
    local item = GameUIRecruitMainNew:GenerateItemData(content.recruit_info)
    if itemData then
      DebugTable(item)
      GameUIRecruitMainNew._CommanderTable[index] = item
    end
    if #content.rewards > 1 then
      GameUIRecruitMainNew:SetTenReward(item, content.rewards, desc)
    else
      GameUIRecruitMainNew:SetOneReward(item, content.rewards, desc)
    end
    GameUIRecruitMainNew:updateListItem(index)
    GameUIRecruitTopbar:SetVisible(false)
    return true
  end
  return false
end
function GameUIRecruitMainNew:SetOneReward(itemData, rewards)
  local v = rewards[1]
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceOneNumber
  data.priceIcon = itemData.priceOneFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_PURCHASE_MORE"), "<number1>", "1")
  data.reward = {}
  self:PopReplayItem(v)
  if v.type == 2 or v.type == 0 then
    v.type = 1
    local extInfo = {}
    extInfo.iconframe = "item_" .. v.item_reward.number
    extInfo.index = k
    data.reward.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v.item_reward, extInfo, GameUIRecruitMainNew.dynamicDownCallBack)
    data.reward.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
    data.reward.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
    data.reward.itemType = v.item_reward.item_type
    data.reward.item_reward = v.item_reward
  else
    v.type = 2
    data.reward.color = FleetDataAccessHelper:GetFleetColorFrameByColor(v.fleet_reward.color)
    data.reward.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.fleet_reward.vessels)
    data.reward.fleetName = FleetDataAccessHelper:GetFleetLevelDisplayName(v.fleet_reward.name, v.fleet_reward.fleet_id, v.fleet_reward.color, 0)
    data.reward.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(v.fleet_reward.vessels)
    data.reward.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(v.fleet_reward.ship)
    data.reward.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.fleet_reward.avatar, v.fleet_reward.fleet_id)
    data.reward.rankFrame = GameUtils:GetFleetRankFrame(v.fleet_reward.rank, nil)
    data.reward.fleet_id = v.fleet_reward.fleet_id
    data.reward.itemType = "fleet"
    self:PopGetFleet(v.fleet_reward.fleet_id)
  end
  data.reward.type = v.type
  local redlist = {}
  redlist[1] = data.reward
  GameUIRecruitMainNew.curRewards = redlist
  if self:GetFlashObject() then
    self:GetFlashObject():InvokeASCallback("_root", "SetOneRewardInfo", data)
  end
  GameUIRecruitMainNew.curShowUI = "buyOne"
end
function GameUIRecruitMainNew:SetTenReward(itemData, rewards)
  local data = {}
  data.id = itemData.id
  data.price = itemData.priceTenNumber
  data.priceIcon = itemData.priceTenFrame
  data.buytip = string.gsub(GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_PURCHASE_MORE"), "<number1>", "10")
  data.rewards = {}
  for k, v in ipairs(rewards or {}) do
    local item = {}
    self:PopReplayItem(v)
    if v.type == 0 or v.type == 2 then
      v.type = 1
      local extInfo = {}
      extInfo.iconframe = "item_" .. v.item_reward.number
      extInfo.index = k
      item.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v.item_reward, extInfo, GameUIRecruitMainNew.dynamicDownCallBack)
      item.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
      item.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
      item.itemType = v.item_reward.item_type
      item.item_reward = v.item_reward
    else
      v.type = 2
      item.color = FleetDataAccessHelper:GetFleetColorFrameByColor(v.fleet_reward.color)
      item.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.fleet_reward.vessels)
      item.fleetName = FleetDataAccessHelper:GetFleetLevelDisplayName(v.fleet_reward.name, v.fleet_reward.fleet_id, v.fleet_reward.color, 0)
      item.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(v.fleet_reward.vessels)
      item.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(v.fleet_reward.ship)
      item.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.fleet_reward.avatar, v.fleet_reward.fleet_id)
      item.rankFrame = GameUtils:GetFleetRankFrame(v.fleet_reward.rank, nil)
      item.fleet_id = v.fleet_reward.fleet_id
      item.itemType = "fleet"
      self:PopGetFleet(v.fleet_reward.fleet_id)
    end
    item.type = v.type
    data.rewards[#data.rewards + 1] = item
  end
  GameUIRecruitMainNew.curRewards = data.rewards
  self:GetFlashObject():InvokeASCallback("_root", "SetTenRewardInfo", data)
  GameUIRecruitMainNew.curShowUI = "buyTen"
end
function GameUIRecruitMainNew:PopGetFleet(fleetID)
  local item = {}
  item.item_type = "fleet"
  item.number = fleetID
  item.no = 0
  item.level = 1
  item.shared_rewards = {}
  local function callback()
    local GameUIPrestigeRankUp = LuaObjectManager:GetLuaObject("GameUIPrestigeRankUp")
    GameUIPrestigeRankUp:ShowGetCommander(item, false)
  end
  if GameStateManager:GetCurrentGameState():IsObjectInState(ItemBox) and ItemBox.currentBox == "rewardInfo" then
    ItemBox:SetDelayShowAfterReward(callback)
  else
    callback()
  end
end
function GameUIRecruitMainNew:PopReplayItem(rewardInfo)
  if rewardInfo.is_replacement then
    local baseStr = GameLoader:GetGameText("LC_MENU_CELESTIAL_PORTAL_ALREADY_HAVE_COMMANDER")
    local ownName = GameHelper:GetAwardTypeText(rewardInfo.own_item.item_type, rewardInfo.own_item.number)
    local orinName = GameHelper:GetAwardTypeText(rewardInfo.origin_item.item_type, rewardInfo.origin_item.number)
    local desName = ""
    if rewardInfo.type == 0 or rewardInfo.type == 2 then
      desName = GameHelper:GetAwardText(rewardInfo.item_reward.item_type, rewardInfo.item_reward.number, rewardInfo.item_reward.no)
    else
      desName = FleetDataAccessHelper:GetFleetLevelDisplayName(rewardInfo.fleet_reward.name, rewardInfo.fleet_reward.fleet_id, rewardInfo.fleet_reward.color, 0)
    end
    baseStr = string.gsub(baseStr, "<owned_char>", ownName)
    baseStr = string.gsub(baseStr, "<reward_char>", orinName)
    baseStr = string.gsub(baseStr, "<sub_char>", desName)
    local GameTip = LuaObjectManager:GetLuaObject("GameTip")
    GameTip:Show(baseStr)
  end
end
function GameUIRecruitMainNew.dynamicDownCallBack(extInfo)
  if GameUIRecruitMainNew:GetFlashObject() then
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "SetRewardIcon", extInfo.index, extInfo.iconframe)
  end
end
function GameUIRecruitMainNew:GetItemDataById(id)
  for k, v in ipairs(self._CommanderTable or {}) do
    if v.id == id then
      return v, k
    end
  end
  return nil
end
function GameUIRecruitMainNew:RecruitLookReq(id)
  local param = {}
  param.id = id
  NetMessageMgr:SendMsg(NetAPIList.recruit_look_req.Code, param, self.RecruitLookCallBack, true, nil)
end
function GameUIRecruitMainNew.RecruitLookCallBack(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.recruit_look_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  elseif msgtype == NetAPIList.recruit_look_ack.Code then
    local id = content.id
    GameUIRecruitMainNew.itemList = {}
    GameUIRecruitMainNew.fleetList = {}
    for k, v in ipairs(content.rewards) do
      if v.type == 0 then
        GameUIRecruitMainNew.itemList[#GameUIRecruitMainNew.itemList + 1] = v
      else
        GameUIRecruitMainNew.fleetList[#GameUIRecruitMainNew.fleetList + 1] = v
        if v.type == 2 then
          v.type = 0
        end
      end
    end
    GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "ShowLookUI")
    GameUIRecruitTopbar:SetVisible(false)
    GameUIRecruitMainNew:SetLookReward(1)
    GameUIRecruitMainNew.curShowUI = "look"
    return true
  end
  return false
end
function GameUIRecruitMainNew:SetLookReward(TabType)
  local data = {}
  local list
  if TabType == 1 then
    list = GameUIRecruitMainNew.fleetList
  else
    list = GameUIRecruitMainNew.itemList
  end
  data.tabType = TabType
  data.rewards = {}
  for k, v in ipairs(list or {}) do
    local item = {}
    local ty = 0
    if v.type == 0 then
      ty = 1
      item.iconFrame = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(v.item_reward, nil, nil)
      item.nameText = GameHelper:GetAwardTypeText(v.item_reward.item_type, v.item_reward.number)
      item.Count = GameHelper:GetAwardCount(v.item_reward.item_type, v.item_reward.number, v.item_reward.no)
      item.itemType = v.item_reward.item_type
      item.item_reward = v.item_reward
    else
      ty = 2
      item.color = FleetDataAccessHelper:GetFleetColorFrameByColor(v.fleet_reward.color)
      item.vesselsType = FleetDataAccessHelper:GetFleetVesselsFrameByVessel(v.fleet_reward.vessels)
      item.fleetName = FleetDataAccessHelper:GetFleetLevelDisplayName(v.fleet_reward.name, v.fleet_reward.fleet_id, v.fleet_reward.color, 0)
      item.vesselsName = FleetDataAccessHelper:GetFleetVesselsTypeStringByVessel(v.fleet_reward.vessels)
      item.ship = GameDataAccessHelper:GetCommanderShipByDownloadState(v.fleet_reward.ship)
      item.avatar = FleetDataAccessHelper:GetFleetAvatarFrameByAvatar(v.fleet_reward.avatar, v.fleet_reward.fleet_id)
      item.rankFrame = GameUtils:GetFleetRankFrame(v.fleet_reward.rank, nil)
      item.fleet_id = v.fleet_reward.fleet_id
      item.itemType = "fleet"
    end
    item.type = ty
    data.rewards[#data.rewards + 1] = item
  end
  GameUIRecruitMainNew.curTabData = data
  GameUIRecruitMainNew.curRewards = data.rewards
  self:GetFlashObject():InvokeASCallback("_root", "initLookList", math.ceil(#data.rewards / 4), TabType)
end
function GameUIRecruitMainNew:updateLookItemList(itemIndex)
  local startIndex = (itemIndex - 1) * 4 + 1
  local endIndex = itemIndex * 4
  local data = {}
  for i = startIndex, endIndex do
    data[#data + 1] = GameUIRecruitMainNew.curTabData.rewards[i]
  end
  self:GetFlashObject():InvokeASCallback("_root", "updateLookListItem", itemIndex, data)
end
function GameUIRecruitMainNew:Update(dt)
  local flashObj = GameUIRecruitMainNew:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "onUpdateFrame", dt)
    flashObj:Update(dt)
  end
end
if AutoUpdate.isAndroidDevice then
  function GameUIRecruitMainNew.OnAndroidBack()
    if GameUIRecruitMainNew.curShowUI == "main" then
      GameStateRecruit:Quit()
    elseif GameUIRecruitMainNew.curShowUI == "buyOne" then
      GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "HideOnePop")
    elseif GameUIRecruitMainNew.curShowUI == "buyTen" then
      GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "HideTenPop")
    elseif GameUIRecruitMainNew.curShowUI == "look" then
      GameUIRecruitMainNew:GetFlashObject():InvokeASCallback("_root", "HideLookUI")
    end
  end
end
