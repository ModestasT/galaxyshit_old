local GameUIRechargePush = LuaObjectManager:GetLuaObject("GameUIRechargePush")
local GameVip = LuaObjectManager:GetLuaObject("GameVip")
local GameHelper = LuaObjectManager:GetLuaObject("GameHelper")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local ItemBox = LuaObjectManager:GetLuaObject("ItemBox")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIFirstCharge = LuaObjectManager:GetLuaObject("GameUIFirstCharge")
function GameUIRechargePush:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameVip.storeObject = GameVip.storeObject or StoreObject:new()
  GameVip.storeObject:RequestProductData(GameVip.productIdentifierList)
  GameVip:RequestProductIdentifierList()
  local month_card_callback = function(msgType, content)
    if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.month_card_buy_req.Code then
      return true
    end
    return false
  end
  local buy_month_param = {id = 0, gift_user = 0}
  NetMessageMgr:SendMsg(NetAPIList.month_card_buy_req.Code, buy_month_param, month_card_callback, false, nil)
  GameUIRechargePush:SendPopSuccess()
  GameUIRechargePush:SetSaleList()
  GameUIRechargePush:ShowPanel()
end
if AutoUpdate.isAndroidDevice then
  function GameUIRechargePush.OnAndroidBack()
    GameUIRechargePush:OnFSCommand("close")
  end
end
function GameUIRechargePush:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIRechargePush:OnFSCommand(cmd, arg)
  if "close" == cmd then
    GameUIRechargePush:HidePanel()
  elseif "hided" == cmd then
    GameUIRechargePush:Exit()
  elseif "NeedUpdateSaleListItem" == cmd then
    GameUIRechargePush:UpdateSaleListItem(tonumber(arg))
  elseif "get" == cmd then
    GameUIRechargePush:Buy(tonumber(arg))
  elseif cmd == "more" then
    GameUIRechargePush.mCurSelectSaleIndex = tonumber(arg)
    GameUIRechargePush:ShowMoreItemsScreen()
  elseif cmd == "show_gift_item" then
    local gift_item = GameVip.onSaleProductList[GameUIRechargePush.mCurSelectSaleIndex].gift_items[tonumber(arg)]
    GameUIRechargePush:ShowGift(gift_item)
  elseif cmd == "needUpdatemoreItemsItem" then
    GameUIRechargePush:UpdateMoreItemsItem(tonumber(arg))
  elseif cmd == "showItemDetail" then
    DebugOut("---arg:" .. arg)
    local param = LuaUtils:string_split(arg, "\001")
    local gift_item = GameVip.onSaleProductList[tonumber(param[2])].gift_items[tonumber(param[1])]
    GameUIRechargePush:ShowGift(gift_item)
  end
end
GameUIRechargePush.mData = nil
function GameUIRechargePush:GetData()
end
function GameUIRechargePush:ShowPanel()
  local flashObj = GameUIRechargePush:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "ShowPanel")
  end
end
function GameUIRechargePush:HidePanel()
  local flashObj = GameUIRechargePush:GetFlashObject()
  if flashObj then
    flashObj:InvokeASCallback("_root", "HidePanel")
  end
end
function GameUIRechargePush:SetSaleList()
  local flashObj = GameUIRechargePush:GetFlashObject()
  if flashObj and GameVip.mPayListData then
    GameUIRechargePush.mPushItemIndexList = {}
    flashObj:InvokeASCallback("_root", "SetSaleList", #GameVip.mPayListData.activity_items)
  end
end
function GameUIRechargePush:UpdateSaleListItem(itemId)
  local flashObj = GameUIRechargePush:GetFlashObject()
  if flashObj then
    local item = GameVip.mPayListData.activity_items[itemId]
    local cardName = GameHelper:GetFoatColor(item.word.title_font, GameLoader:GetGameText(item.activity_name)) or ""
    local index = -1
    for iIdentifier, vIdentifier in ipairs(GameVip.productIdentifierList) do
      if vIdentifier == item.production_id then
        index = iIdentifier
        break
      end
    end
    local cardPrice = GameVip.productList[index]
    local oldCardPrice = GameVip:getOldPriceV2(cardPrice, item.discount_rate)
    local moreGet = GameHelper:GetFoatColor(item.word.discount_font, item.discount_rate .. "%")
    local creditNum = item.credit
    local itemNameList = ""
    local itemCountList = ""
    local itemIconList = ""
    for iItem, vItem in ipairs(item.gift_items) do
      local icon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(vItem, extInfo, GameVip.DynamicResDowloadFinished)
      itemNameList = itemNameList .. GameHelper:GetAwardTypeText(vItem.item_type, vItem.number) .. "\001"
      itemCountList = itemCountList .. "x " .. GameUtils.numberConversion(GameHelper:GetAwardCount(vItem.item_type, vItem.number, vItem.no)) .. "\001"
      itemIconList = itemIconList .. icon .. "\001"
    end
    local function downloadImgCallback(content)
      local flashObj = GameUIRechargePush:GetFlashObject()
      if flashObj and GameUIRechargePush.mPushItemIndexList and #GameUIRechargePush.mPushItemIndexList > 0 then
        for k, v in pairs(GameUIRechargePush.mPushItemIndexList) do
          local resName = v.img
          local localPath = "data2/" .. DynamicResDownloader:GetFullName(resName, DynamicResDownloader.resType.PAY_PIC)
          if DynamicResDownloader:IfResExsit(localPath) then
            DebugOut("lua UpdatePushItemIcon " .. localPath)
            local orgTexture = "icon_box_" .. tostring(v.frIdx) .. ".png"
            flashObj:ReplaceTexture(orgTexture, resName)
            flashObj:InvokeASCallback("_root", "UpdatePushItemIcon", v.cardIdx, "icon_box_" .. tostring(v.frIdx))
          end
        end
      end
    end
    if item.icon and "" ~= item.icon and "undefined" ~= item.icon then
      local resName = item.icon
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(resName, DynamicResDownloader.resType.PAY_PIC)
      DebugOut("localPath " .. localPath)
      if DynamicResDownloader:IfResExsit(localPath) then
        ext.UpdateAutoUpdateFile(localPath, 0, 0, 0)
      else
        local downloadInfo = {}
        downloadInfo.localPath = "data2/" .. resName
        DynamicResDownloader:AddDynamicRes(resName, DynamicResDownloader.resType.PAY_PIC, downloadInfo, downloadImgCallback)
      end
      local imgItem = {
        cardIdx = itemId,
        frIdx = itemId,
        img = resName
      }
      GameUIRechargePush.mPushItemIndexList[itemId] = imgItem
    end
    if item.background1 and "" ~= item.background1 and "undefined" ~= item.background1 then
      local localPath = "data2/" .. DynamicResDownloader:GetFullName(item.background1, DynamicResDownloader.resType.FESTIVAL_BANNER)
      if DynamicResDownloader:IfResExsit(localPath) then
        DebugOut("extInfo.localPath = " .. localPath)
        flashObj:ReplaceTexture("LAZY_LOAD_TiaoPIC_100.png", item.background1)
      else
        GameHelper:GetBackgroundImgDynamic(itemId, "LAZY_LOAD_TiaoPIC_100.png", item.background1, GameUIRechargePush.GetBackgroundImgDynamicCallBack)
      end
    end
    local btnMoreText = GameLoader:GetGameText("LC_MENU_PAY_SPACIAL_MORE_CHAR")
    local moreText = GameHelper:GetFoatColor(item.word.discount_font, btnMoreText)
    local btnText = GameLoader:GetGameText("LC_MENU_PAY_SPACIAL_BUY_BUTTON")
    local endTimeLabText = GameHelper:GetFoatColor(item.word.end_font, GameLoader:GetGameText("LC_MENU_EVENT_GIVENDAY_END_CHAR"))
    local isHighlight = false
    if item.top == 1 then
      isHighlight = true
    end
    DebugOut("cardPrice = ", cardPrice)
    DebugOut("oldCardPrice = ", oldCardPrice)
    flashObj:InvokeASCallback("_root", "UpdateSaleListItem", itemId, cardName, cardPrice, creditNum, itemNameList, itemCountList, itemIconList, btnText, endTimeLabText, isHighlight, oldCardPrice, moreGet, btnMoreText, moreText)
    downloadImgCallback()
  end
end
function GameUIRechargePush.GetBackgroundImgDynamicCallBack(extInfo)
  DebugOut("extInfo = ")
  DebugTable(extInfo)
  local flashObj = GameUIRechargePush:GetFlashObject()
  if not flashObj then
    return
  end
  flashObj:ReplaceTexture(extInfo.localImg, extInfo.localPath)
  DebugOut("------")
end
function GameUIRechargePush:OnSaleListChange()
  if #GameVip.mPayListData.activity_items > 0 then
    GameUIRechargePush:SetSaleList()
  elseif GameStateManager:GetCurrentGameState():IsObjectInState(self) then
    GameUIRechargePush:Exit()
  end
end
function GameUIRechargePush:Exit()
  if self:GetFlashObject() then
    self:GetFlashObject():unlockInput()
  end
  GameStateManager:GetCurrentGameState():EraseObject(self)
end
function GameUIRechargePush:Show()
  if GameStateManager.GameStateMainPlanet == GameStateManager:GetCurrentGameState() and not GameStateManager:GetCurrentGameState():IsObjectInState(self) and not GameStateManager:GetCurrentGameState():IsObjectInState(GameVip) then
    GameStateManager:GetCurrentGameState():AddObject(self)
  end
end
function GameUIRechargePush:Update(dt)
  local flashObj = GameUIRechargePush:GetFlashObject()
  if flashObj then
    flashObj:Update(dt)
    GameUIRechargePush:UpdateOnSaleTime()
  end
end
function GameUIRechargePush:UpdateOnSaleTime()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  if #GameVip.onSaleProductList > 0 then
    for i, v in ipairs(GameVip.onSaleProductList) do
      local tmpLeftTime = 0
      if 0 < v.end_time and not v.timeout then
        tmpLeftTime = v.end_time - (os.time() - GameVip.OnSaleFetchTime)
        if tmpLeftTime < 0 then
          tmpLeftTime = 0
          v.timeout = true
          GameVip:RequestProductIdentifierList()
        else
          v.timeout = false
        end
        local endTimeLabText = GameHelper:GetFoatColor(v.word.end_font, GameLoader:GetGameText("LC_MENU_EVENT_GIVENDAY_END_CHAR"))
        local timeStr = GameHelper:GetFoatColor(v.word.time_font, GameUtils:formatTimeString(tmpLeftTime))
        local str = endTimeLabText .. " " .. timeStr
        flash_obj:InvokeASCallback("_root", "updateCardLeftTime", i, str)
      end
    end
  end
end
function GameUIRechargePush:ShowMoreItemsScreen()
  local flash_obj = self:GetFlashObject()
  if not flash_obj then
    return
  end
  GameUIRechargePush:RefreshMoreItemsList()
  flash_obj:InvokeASCallback("_root", "showMoreItems")
end
function GameUIRechargePush:HideMoreItemsMenu()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "hideRecoverPanel")
end
function GameUIRechargePush:RefreshMoreItemsList()
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  flash:InvokeASCallback("_root", "clearMoreItemsList")
  flash:InvokeASCallback("_root", "initMoreItemsList")
  local saleItem = GameVip.onSaleProductList[GameUIRechargePush.mCurSelectSaleIndex]
  local itemNum = #saleItem.gift_items
  for i = 1, itemNum do
    flash:InvokeASCallback("_root", "addMoreItemsItem", i)
  end
  flash:InvokeASCallback("_root", "enableAutoArrowSet", true)
end
function GameUIRechargePush:UpdateMoreItemsItem(id)
  local flash = self:GetFlashObject()
  if not flash then
    return
  end
  local itemIndex = tonumber(id)
  local saleItem = GameVip.onSaleProductList[GameUIRechargePush.mCurSelectSaleIndex]
  local item = saleItem.gift_items[itemIndex]
  local itemName = GameHelper:GetAwardTypeText(item.item_type, item.number)
  local itemNumber = "x" .. GameUtils.numberConversion(GameHelper:GetAwardCount(item.item_type, item.number, item.no))
  local itemIcon
  itemIcon = GameHelper:GetAwardTypeIconFrameNameSupportDynamic(item, nil, nil)
  flash:InvokeASCallback("_root", "setMoreItemsItem", itemIndex, itemName, itemNumber, itemIcon)
end
function GameUIRechargePush:ShowGift(item)
  if item.item_type == "krypton" then
    ItemBox:SetKryptonBox(item, tonumber(item.id))
    local operationTable = {}
    operationTable.btnUnloadVisible = false
    operationTable.btnUpgradeVisible = false
    operationTable.btnUseVisible = false
    operationTable.btnDecomposeVisible = false
    ItemBox:ShowKryptonBox(320, 240, operationTable)
  end
  if item.item_type == "fleet" then
    GameUIFirstCharge:ShowCommanderInfo(tonumber(item.number))
  end
  if item.item_type == "item" then
    item.cnt = 1
    ItemBox:showItemBox("Item", item, tonumber(item.number), 320, 480, 200, 200, "", nil, "", nil)
  end
end
function GameUIRechargePush:Buy(itemId)
  if GameUtils:isIOSBundle() and GameVip.gamePurchaseEanble == 0 then
    GameUIGlobalScreen:ShowMessageBox(GameUIGlobalScreen.MessageBoxType_OK, GameLoader:GetGameText("LC_MENU_NOTIFICATION_CHAR"), GameLoader:GetGameText("LC_MENU_UNABLE_TO_BUY_INFO"), nil)
    return
  end
  local arg = GameVip.mPayListData.activity_items[itemId].production_id
  local Ishave = false
  for k, v in pairs(GameVip.productIdentifierList) do
    if arg == v then
      Ishave = true
      arg = k
      break
    end
  end
  if not Ishave then
    DebugOut("Not found product.")
    return
  end
  if GameVip.GetPriceByProductID(GameVip.productIdentifierList[tonumber(arg)]) then
    GameVip.storeObject = GameVip.storeObject or StoreObject:new()
    GameWaiting:ShowLoadingScreen()
    DebugOut("GameVip.p[tonumber(arg)]: ", GameVip.productIdentifierList[tonumber(arg)])
    GameUtils:printByAndroid("GameVip.canShowMycard= " .. tostring(GameVip.canShowMycard) .. ", Wifi = ")
    local canShowMycard, isShowmycardType_1, isShowmycardType_2, isShowmycardType_3 = GameVip:GetMycardPurchageVisible(GameVip.productIdentifierList[tonumber(arg)])
    local mixedData = tostring(isShowmycardType_1) .. tostring(isShowmycardType_2) .. tostring(isShowmycardType_3)
    GameVip.storeObject:Buy(GameVip.productIdentifierList[tonumber(arg)], canShowMycard, mixedData)
  end
end
GameUIRechargePush.mCanPop = false
GameUIRechargePush.mSignPopReceived = false
GameUIRechargePush.mIsSignNeedPop = false
GameUIRechargePush.mWDPopReceived = false
GameUIRechargePush.mIsWDNeedPop = false
GameUIRechargePush.mIsWelcomePopReceived = false
GameUIRechargePush.mIsWelcomeNeedPop = false
function GameUIRechargePush:WelcomePopReceived(isWelcomePop)
  DebugOut("WelcomePopReceived : " .. tostring(isWelcomePop))
  GameUIRechargePush.mIsWelcomePopReceived = true
  GameUIRechargePush.mIsWelcomeNeedPop = isWelcomePop
end
function GameUIRechargePush:SignPopReceived(isSignPop)
  DebugOut("SignPopReceived : " .. tostring(isSignPop))
  GameUIRechargePush.mSignPopReceived = true
  GameUIRechargePush.mIsSignNeedPop = isSignPop
end
function GameUIRechargePush:WDPopReceived(isWDPop)
  DebugOut("WDPopReceived : " .. tostring(isWDPop))
  GameUIRechargePush.mWDPopReceived = true
  GameUIRechargePush.mIsWDNeedPop = isWDPop
  GameUIRechargePush:CheckPop()
end
function GameUIRechargePush:IsCanPop()
  if GameVip.mPayListData and GameVip.isPriceValid and GameUIRechargePush.mCanPop and GameUIRechargePush.mWDPopReceived and not GameUIRechargePush.mIsWDNeedPop then
    return true
  else
    return false
  end
end
function GameUIRechargePush:ClearStatus()
  GameUIRechargePush.mIsWelcomeNeedPop = false
  GameUIRechargePush.mIsSignNeedPop = false
  GameUIRechargePush.mIsWDNeedPop = false
end
function GameUIRechargePush:CheckPop()
  if GameUIRechargePush:IsCanPop() then
    GameUIRechargePush:AddPopNotice()
    GameUIRechargePush.mCanPop = false
  end
end
function GameUIRechargePush:AddPopNotice()
  GameNotice:AddNotice("PayPush")
  if GameStateManager.GameStateMainPlanet == GameStateManager:GetCurrentGameState() then
    GameNotice:CheckShowNotice()
  end
end
function GameUIRechargePush.PayPushNtf(content)
  if 1 == content.can_pop then
    GameUIRechargePush.mCanPop = true
    GameUIRechargePush:CheckPop()
  else
    GameUIRechargePush.mCanPop = false
  end
end
function GameUIRechargePush:SendPopSuccess()
  NetMessageMgr:SendMsg(NetAPIList.pay_push_pop_set_req.Code, nil, nil, false, nil)
end
