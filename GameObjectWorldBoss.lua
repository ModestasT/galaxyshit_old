local GameStateWorldBoss = GameStateManager.GameStateWorldBoss
local GameStateBattlePlay = GameStateManager.GameStateBattlePlay
local GameObjectWorldBoss = LuaObjectManager:GetLuaObject("GameObjectWorldBoss")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIBattleResult = LuaObjectManager:GetLuaObject("GameUIBattleResult")
local GameUIChat = LuaObjectManager:GetLuaObject("GameUIChat")
GameObjectWorldBoss.BossName = ""
GameObjectWorldBoss.BossLv = -1
function GameObjectWorldBoss:OnAddToGameState()
  self:MoveIn()
  GameStateWorldBoss:CheckQuit(GameStateWorldBoss.TemBossInfo)
  GameGlobalData:RegisterDataChangeCallback("newChatTotalCount", GameObjectWorldBoss.UpdateChatButtonStatus)
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  if chatCount and chatCount > 0 then
    self:GetFlashObject():InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  end
end
function GameObjectWorldBoss:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameGlobalData:RemoveDataChangeCallback("newChatTotalCount", GameObjectWorldBoss.UpdateChatButtonStatus)
end
function GameObjectWorldBoss:MoveIn()
  self:GetFlashObject():InvokeASCallback("_root", "showAll")
  self:UpdateChatMessage("")
end
function GameObjectWorldBoss:MoveOut()
  self:GetFlashObject():InvokeASCallback("_root", "hideAll")
end
function GameObjectWorldBoss:HideChatIcon()
  self:GetFlashObject():InvokeASCallback("_root", "hideChatIcon")
end
function GameObjectWorldBoss:ShowChatIcon()
  self:GetFlashObject():InvokeASCallback("_root", "showChatIcon")
end
function GameObjectWorldBoss:OnFSCommand(cmd, arg)
  if cmd == "Quit" then
    GameStateWorldBoss:Quit()
  elseif cmd == "PowerUp" then
    NetMessageMgr:SendMsg(NetAPIList.add_world_boss_force_rate_req.Code, nil, self.NetCallbackPowerup, true, nil)
    GameUtils:RecordForTongdui(NetAPIList.add_world_boss_force_rate_req.Code, nil, self.NetCallbackPowerup, true, nil)
  elseif cmd == "HideRobot" then
    GameSettingData.WorldBossHideRobot = true
    GameUtils:SaveSettingData()
    GameObjectWorldBoss:UpdateHideRobotSetting()
  elseif cmd == "ShowRobot" then
    GameSettingData.WorldBossHideRobot = false
    GameUtils:SaveSettingData()
    GameObjectWorldBoss:UpdateHideRobotSetting()
  elseif cmd == "Challenge" then
    if GameStateManager:GetCurrentGameState().fightRoundData then
      GameStateManager:GetCurrentGameState().fightRoundData = nil
    end
    NetMessageMgr:SendMsg(NetAPIList.challenge_world_boss_req.Code, nil, self.NetCallbackChallenge, true, nil)
  elseif cmd == "CleanChallengeCD" then
    local bossinfo = GameStateWorldBoss:GetBossInfo()
    local info = string.format(GameLoader:GetGameText("LC_MENU_COST_CREDIT_ALERT_CHAR"), bossinfo.clean_cd_cost)
    local function callback()
      NetMessageMgr:SendMsg(NetAPIList.clean_world_boss_cd_req.Code, nil, self.NetCallbackCleanCD, true, nil)
    end
    GameUtils:CreditCostConfirm(info, callback)
  elseif cmd == "PopupChatWindow" then
    self:HideChatIcon()
    GameStateWorldBoss:AddObject(GameUIChat)
    GameUIChat:SelectChannel("world")
  end
end
function GameObjectWorldBoss:UpdateWorldBossInfo()
  local bossinfo = GameStateWorldBoss:GetBossInfo()
  local flashobj = self:GetFlashObject()
  if bossinfo and flashobj then
    self:SetChallengeCDTime(bossinfo.cd)
    self:SetEndTime(bossinfo.end_time)
    self:UpdateTopPlayerList()
    self:UpdateHideRobotSetting()
    self:SetBossHPInfo(bossinfo.hp, bossinfo.total_hp, bossinfo.damage or 0)
    self:SetBossPropertiesText(bossinfo.motility, bossinfo.intercept)
  end
end
function GameObjectWorldBoss:SetBossHPInfo(BossHP, BossTotalHP, DownHP)
  self:GetFlashObject():InvokeASCallback("_root", "setBossHPInfo", BossHP, BossTotalHP, DownHP)
end
function GameObjectWorldBoss:SetBossPropertiesText(motilityNum, interceptNum)
  DebugOut("SetBossPropertiesText", motilityNum, interceptNum)
  local motilityText = GameLoader:GetGameText("LC_MENU_Equip_param_12")
  local interceptText = GameLoader:GetGameText("LC_MENU_Equip_param_14")
  self:GetFlashObject():InvokeASCallback("_root", "setBossPropertiesText", motilityNum, motilityText, interceptNum, interceptText)
end
function GameObjectWorldBoss:ShowPowerupAnimation(isSuccess)
  self:GetFlashObject():InvokeASCallback("_root", "showPowerupAnimation", isSuccess)
end
function GameObjectWorldBoss:UpdateTopPlayerList()
  local bossinfo = GameStateWorldBoss:GetBossInfo()
  local myplayerinfo = GameGlobalData:GetData("userinfo")
  local datatable = {}
  for index = 1, 11 do
    local datastring = "-1"
    local playerInfo = bossinfo.toplist[index]
    if playerInfo then
      local t = {}
      t[#t + 1] = playerInfo.top
      t[#t + 1] = playerInfo.player_name
      t[#t + 1] = GameUtils.numberConversion(playerInfo.destroy_hp)
      datastring = table.concat(t, "\002")
      if myplayerinfo.name == playerInfo.player_name then
        self:GetFlashObject():InvokeASCallback("_root", "setDamageInfo", datastring)
      end
    end
    if index ~= 11 then
      datatable[#datatable + 1] = datastring
    end
  end
  local datastring = table.concat(datatable, "\001")
  self:GetFlashObject():InvokeASCallback("_root", "setTopPlayerList", datastring)
end
function GameObjectWorldBoss:UpdatePowerupInfo()
  local powerupinfo = GameStateWorldBoss:GetPowerupInfo()
  self:GetFlashObject():InvokeASCallback("_root", "setPowerupInfo", powerupinfo.cost, powerupinfo.rate)
  DebugOut("UpdatePowerupInfo")
  DebugTable(powerupinfo)
  self:SetBossLv(powerupinfo.current_level)
  if powerupinfo.upvalue ~= nil then
    self:GetFlashObject():InvokeASCallback("_root", "showPowerupAnimation", powerupinfo.upvalue)
    powerupinfo.upvalue = nil
  end
end
function GameObjectWorldBoss:UpdateHideRobotSetting()
  if GameSettingData.WorldBossHideRobot == nil then
    GameSettingData.WorldBossHideRobot = false
    GameUtils:SaveSettingData()
  end
  self:GetFlashObject():InvokeASCallback("_root", "setHideRobot", GameSettingData.WorldBossHideRobot)
end
function GameObjectWorldBoss:SetTargetWaitingTime(timevalue)
  self.TargetWaitingTime = os.time() + timevalue
end
function GameObjectWorldBoss:SetChallengeCDTime(timevalue)
  self.ChallengeCDTime = os.time() + timevalue
end
function GameObjectWorldBoss:SetEndTime(timevalue)
  self.EndTime = os.time() + timevalue
end
function GameObjectWorldBoss:SetBossName(BossName)
  GameObjectWorldBoss.BossName = BossName
  self:SetBossTitle()
end
function GameObjectWorldBoss:SetBossLv(lv)
  GameObjectWorldBoss.BossLv = lv
  self:SetBossTitle()
end
function GameObjectWorldBoss:SetBossTitle()
  local bossTitle = ""
  if GameObjectWorldBoss.BossName ~= nil and GameObjectWorldBoss.BossName ~= "" then
    bossTitle = bossTitle .. GameObjectWorldBoss.BossName
  end
  if GameObjectWorldBoss.BossLv ~= nil and GameObjectWorldBoss.BossLv ~= -1 then
    bossTitle = bossTitle .. "  " .. GameLoader:GetGameText("LC_MENU_Level") .. GameObjectWorldBoss.BossLv
  end
  self:GetFlashObject():InvokeASCallback("_root", "setBossName", bossTitle)
end
function GameObjectWorldBoss:UpdateChatMessage(htmlText)
  local flashobj = self:GetFlashObject()
  if flashobj then
    flashobj:InvokeASCallback("_root", "setChatContent", htmlText)
  end
end
function GameObjectWorldBoss:Update(dt)
  local flashobj = self:GetFlashObject()
  if flashobj then
    local ostime = os.time()
    if self.TargetWaitingTime then
      local lefttime = self.TargetWaitingTime - ostime
      if lefttime <= 0 then
        lefttime = 0
        self.TargetWaitingTime = nil
      end
      self:GetFlashObject():InvokeASCallback("_root", "setStartWaitingTime", lefttime)
    end
    if self.ChallengeCDTime then
      local lefttime = self.ChallengeCDTime - ostime
      if lefttime <= 0 then
        lefttime = 0
        self.ChallengeCDTime = nil
      end
      self:GetFlashObject():InvokeASCallback("_root", "setChallengeCDTime", lefttime)
    end
    if self.EndTime then
      local lefttime = self.EndTime - ostime
      if lefttime <= 0 then
        lefttime = 0
        self.EndTime = nil
      end
      self:GetFlashObject():InvokeASCallback("_root", "setEndTime", lefttime)
    end
    flashobj:InvokeASCallback("_root", "onFrameUpdate", dt)
    flashobj:Update(dt)
  end
end
function GameObjectWorldBoss.NetCallbackPowerup(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.add_world_boss_force_rate_req.Code then
    if content.code ~= 0 then
      local error_key = AlertDataList:GetTextIdFromCode(content.code)
      if error_key == "LC_ALERT_tech_not_enough_world_boss" then
        local GameObjectArcaneEnhance = LuaObjectManager:GetLuaObject("GameObjectArcaneEnhance")
        GameObjectArcaneEnhance:CheckIsNeedShowDialog(content.code)
      elseif error_key == "LC_ALERT_credit_not_enough_world_boss" then
        local GameVip = LuaObjectManager:GetLuaObject("GameVip")
        GameVip:CheckIsNeedShowVip(content.code)
      else
        GameUIGlobalScreen:ShowAlert("error", content.code, nil)
      end
    end
    return true
  end
  if msgtype == NetAPIList.add_world_boss_force_rate_ack.Code then
    local powerupinfo = GameStateWorldBoss:GetPowerupInfo()
    powerupinfo.upvalue = content.success
    powerupinfo.rate = content.rate
    powerupinfo.cost = content.cost
    GameObjectWorldBoss:UpdatePowerupInfo()
    return true
  end
  return false
end
function GameObjectWorldBoss.NetCallbackChallenge(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.challenge_world_boss_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.challenge_world_boss_ack.Code then
    if #content.report.rounds == 0 and GameStateManager:GetCurrentGameState().fightRoundData then
      content.report.rounds = GameStateManager:GetCurrentGameState().fightRoundData
    end
    GameStateBattlePlay.curBattleType = "worldBoss"
    GameStateBattlePlay:InitBattle(GameStateBattlePlay.MODE_PLAY, content.report, 53, 1)
    GameStateManager:SetCurrentGameState(GameStateBattlePlay)
    return true
  end
  return false
end
function GameObjectWorldBoss.NetCallbackCleanCD(msgtype, content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.clean_world_boss_cd_req.Code then
    if content.code ~= 0 then
      local GameVip = LuaObjectManager:GetLuaObject("GameVip")
      GameVip:CheckIsNeedShowVip(content.code)
    else
      GameObjectWorldBoss:SetChallengeCDTime(0)
    end
    return true
  end
  return false
end
if AutoUpdate.isAndroidDevice then
  function GameObjectWorldBoss.OnAndroidBack()
    GameObjectWorldBoss:GetFlashObject():InvokeASCallback("_root", "hideAll")
  end
end
function GameObjectWorldBoss.UpdateChatButtonStatus()
  DebugOut("UpdateChatButtonStatus")
  local chatCount = GameGlobalData:GetData("newChatTotalCount")
  DebugOut(chatCount)
  local flash_obj = GameObjectWorldBoss:GetFlashObject()
  if flash_obj == nil then
    return
  end
  if chatCount and chatCount > 0 then
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", true)
  else
    flash_obj:InvokeASCallback("_root", "lua2fs_ShowOrangeChatBtn", false)
  end
end
