local rest_170 = GameData.adventure.rest_170
rest_170[1] = {times = 1, credits = 10}
rest_170[2] = {times = 2, credits = 20}
rest_170[3] = {times = 3, credits = 30}
rest_170[4] = {times = 4, credits = 40}
rest_170[5] = {times = 5, credits = 60}
rest_170[6] = {times = 6, credits = 80}
rest_170[7] = {times = 7, credits = 100}
rest_170[8] = {times = 8, credits = 125}
rest_170[9] = {times = 9, credits = 150}
rest_170[10] = {times = 10, credits = 200}
