local GameUIWDDefence = LuaObjectManager:GetLuaObject("GameUIWDDefence")
local GameWaiting = LuaObjectManager:GetLuaObject("GameWaiting")
local GameUIGlobalScreen = LuaObjectManager:GetLuaObject("GameUIGlobalScreen")
local GameUIUserAlliance = LuaObjectManager:GetLuaObject("GameUIUserAlliance")
local GameTip = LuaObjectManager:GetLuaObject("GameTip")
local GameStateAlliance = GameStateManager.GameStateAlliance
GameUIWDDefence.mShareStoryChecked = {}
GameUIWDDefence.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER] = true
GameUIWDDefence.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK] = true
function GameUIWDDefence:OnInitGame()
  DebugWD("GameUIWDDefence:OnInitGame()")
end
function GameUIWDDefence:Init()
  GameGlobalData:RegisterDataChangeCallback("alliance_resource", self.RefreshResource)
  GameGlobalData:RegisterDataChangeCallback("resource", self.RefreshResource)
  self:RefreshResource()
  GameGlobalData:RegisterDataChangeCallback("alliance", self.RefreshAlliance)
  self:RefreshAlliance()
  self:SetAllianceName()
  self:SetUIInfo()
end
if AutoUpdate.isAndroidDevice then
  function GameUIWDDefence.OnAndroidBack()
    if GameUIWDDefence.IsShowDonateWindow then
      GameUIWDDefence:GetFlashObject():InvokeASCallback("_root", "hideDonateLeaderPanel")
    else
      GameUIWDDefence:MoveOut()
      GameUIWDDefence:OnFSCommand("onClose")
    end
  end
end
function GameUIWDDefence:OnAddToGameState()
  DebugWD("GameUIWDDefence:OnAddToGameState()")
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  GameUIWDDefence:CheckDownloadImage()
  self:Init()
  self:MoveIn()
end
function GameUIWDDefence:OnEraseFromGameState()
  self:UnloadFlashObject()
  GameUIUserAlliance:FetchActivities(false)
end
function GameUIWDDefence:CheckDownloadImage()
  if (ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == "" or ext.crc32.crc32("data2/LAZY_LOAD_domination_bg.png") == nil) and not AutoUpdateInBackground:IsHeroFileUpdatedToServer("data2/LAZY_LOAD_domination_bg.png") then
    self:GetFlashObject():ReplaceTexture("LAZY_LOAD_domination_bg.png", "territorial_map_bg.png")
  end
end
function GameUIWDDefence:MoveIn()
  DebugWD("GameUIWDDefence:MoveIn()")
  local flash_obj = self:GetFlashObject()
  local shareStoryEnabled = false
  local alliance = GameGlobalData:GetData("alliance")
  local canAppoint = alliance.position == GameGlobalData.MemberPositionType.chairman
  if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER) and canAppoint then
    shareStoryEnabled = true
  end
  flash_obj:InvokeASCallback("_root", "animationMoveIn", shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER])
end
function GameUIWDDefence:MoveOut()
  local flash_obj = self:GetFlashObject()
  flash_obj:InvokeASCallback("_root", "animationMoveOut")
end
function GameUIWDDefence:Update(dt)
  local flash_obj = self:GetFlashObject()
  flash_obj:Update(dt)
  flash_obj:InvokeASCallback("_root", "OnUpdate")
end
function GameUIWDDefence:OnFSCommand(cmd, arg)
  DebugWD("planet wd defence command: ", cmd, arg)
  if cmd == "BuyReleased" then
    self:RequestBuyItem()
  elseif cmd == "onClose" then
    GameUIUserAlliance:SetVisible(nil, true)
  elseif cmd == "onErase" then
    GameStateAlliance:EraseObject(GameUIWDDefence)
  elseif cmd == "onMoveInOver" then
    GameUIUserAlliance:SetVisible(nil, false)
  elseif cmd == "Dominate_released" then
    local flash_obj = self:GetFlashObject()
    local player_res = GameGlobalData:GetData("resource")
    local shareStoryEnabled = false
    if FacebookGraphStorySharer:IsStoryEnabled(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK) then
      shareStoryEnabled = true
    end
    flash_obj:InvokeASCallback("_root", "showDonateLeaderPanel", player_res.brick, shareStoryEnabled, self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK])
    self.IsShowDonateWindow = true
  elseif cmd == "donate_confirm_released" then
    if string.len(arg) ~= 0 then
      local brick = tonumber(arg)
      if brick ~= nil and brick > 0 and brick % 1 == 0 then
        local content = {
          brick = tonumber(arg)
        }
        NetMessageMgr:SendMsg(NetAPIList.alliance_defence_donate_req.Code, content, GameUIWDDefence.DonateCallback, true, nil)
      else
        GameTip:Show(GameLoader:GetGameText("LC_MENU_NEED_BRICK_CHAR"), 2000)
      end
    else
      GameTip:Show(GameLoader:GetGameText("LC_MENU_NEED_BRICK_CHAR"), 2000)
    end
  elseif cmd == "Appoint_released" then
    GameUIWDDefence.Members = nil
    self:RefreshAppointList()
    local flash_obj = self:GetFlashObject()
    flash_obj:InvokeASCallback("_root", "showDefenceLeaderPanel")
    if GameUIWDDefence.Members == nil then
      local data_alliance = GameGlobalData:GetData("alliance")
      local content = {
        alliance_id = data_alliance.id,
        index = 0,
        size = 100
      }
      NetMessageMgr:SendMsg(NetAPIList.alliance_members_req.Code, content, GameUIWDDefence.FetchMembersCallback, true, nil)
    end
  elseif cmd == "needUpdateAppointItem" then
    self:UpdateAppointItem(arg)
  elseif cmd == "appointItemReleased" then
    self:AppointLeader(arg)
  elseif cmd == "Reinforce_released" then
    NetMessageMgr:SendMsg(NetAPIList.alliance_defence_repair_req.Code, nil, GameUIWDDefence.ReinforceCallback, true, nil)
  elseif cmd == "closeDonatePanel" then
    self.IsShowDonateWindow = false
  elseif cmd == "shareSetDefencerChecked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER], FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER)
    end
  elseif cmd == "shareSendBrickChecked" then
    if self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK] then
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK] = false
    else
      self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK] = true
    end
    local flash_obj = self:GetFlashObject()
    if flash_obj then
      flash_obj:InvokeASCallback("_root", "setCheckBox", self.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK], FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK)
    end
  end
  GameTextEdit:fscommand(self:GetFlashObject(), cmd, arg)
end
function GameUIWDDefence:RefreshResource()
  if not GameUIWDDefence:GetFlashObject() then
    return
  end
  local alliance_res = GameGlobalData:GetData("alliance_resource")
  local player_res = GameGlobalData:GetData("resource")
  local flash_obj = GameUIWDDefence:GetFlashObject()
  if flash_obj and alliance_res then
    flash_obj:InvokeASCallback("_root", "setBrickNumber", player_res.brick, alliance_res.resource.brick)
  end
end
function GameUIWDDefence:RefreshAlliance()
  if not GameUIWDDefence:GetFlashObject() then
    return
  end
  local alliance = GameGlobalData:GetData("alliance")
  local flash_obj
  if self and self:GetFlashObject() then
    flash_obj = self:GetFlashObject()
  end
  if flash_obj and alliance then
    local canUpdate = alliance.position == GameGlobalData.MemberPositionType.chairman or alliance.position == GameGlobalData.MemberPositionType.vice_chairman or self:IAmDefenceLeader()
    local canAppoint = alliance.position == GameGlobalData.MemberPositionType.chairman
    flash_obj:InvokeASCallback("_root", "setUpdateButton", canUpdate)
    flash_obj:InvokeASCallback("_root", "setAppointButton", canAppoint)
    if GameUIWDDefence.DefenceInfo then
      DebugWD("setNextLvBrick")
      DebugWD(GameLoader:GetGameText("LC_MENU_BUTTON_UPGRADE"))
      flash_obj:InvokeASCallback("_root", "setNextLvBrick", GameUIWDDefence.DefenceInfo.brick, GameLoader:GetGameText("LC_MENU_BUTTON_UPGRADE"))
    end
  end
end
function GameUIWDDefence:SetAllianceName()
  if not GameUIWDDefence:GetFlashObject() then
    return
  end
  local flash_obj = self:GetFlashObject()
  if flash_obj and GameUIUserAlliance.alliance_info then
    flash_obj:InvokeASCallback("_root", "setAllianceName", GameUIUserAlliance.alliance_info.name)
  end
end
function GameUIWDDefence:SetUIInfo()
  if not GameUIWDDefence:GetFlashObject() then
    return
  end
  local flash_obj = GameUIWDDefence:GetFlashObject()
  DebugWD(flash_obj)
  DebugWD(GameUIWDDefence.DefenceInfo ~= nil)
  if flash_obj and GameUIWDDefence.DefenceInfo ~= nil then
    local info = GameUIWDDefence.DefenceInfo
    local alliance = GameGlobalData:GetData("alliance")
    local canUpdate = alliance.position == GameGlobalData.MemberPositionType.chairman or alliance.position == GameGlobalData.MemberPositionType.vice_chairman or self:IAmDefenceLeader()
    local canAppoint = alliance.position == GameGlobalData.MemberPositionType.chairman
    flash_obj:InvokeASCallback("_root", "setUIInfo", GameUtils:GetUserDisplayName(info.leader_name), info.brick, canUpdate, canAppoint, GameLoader:GetGameText("LC_MENU_BUTTON_UPGRADE"))
    flash_obj:InvokeASCallback("_root", "setLvAndHP", info.level, info.hp, info.hp_limit, info.point / 100)
  end
end
GameUIWDDefence.DefenceInfo = nil
function GameUIWDDefence.OnDefenceChangeHandler(content)
  DebugWD("GameUIWDDefence.OnDefenceChangeHandler")
  DebugWDTable(content)
  GameUIWDDefence.DefenceInfo = LuaUtils:table_rcopy(content.info)
  GameUIWDDefence:SetUIInfo()
  GameUIWDDefence.curLeader = GameUIWDDefence.DefenceInfo.leader_id
  local GameUIWDInBattle = LuaObjectManager:GetLuaObject("GameUIWDInBattle")
  if GameStateManager:GetCurrentGameState():IsObjectInState(GameUIWDInBattle) then
    GameUIWDInBattle:SetUIInfo()
  end
end
function GameUIWDDefence:IAmDefenceLeader()
  local data_userinfo = GameGlobalData:GetData("userinfo")
  if not data_userinfo or GameUIWDDefence.DefenceInfo == nil then
    return false
  end
  if GameUIWDDefence.DefenceInfo.leader_id == data_userinfo.player_id then
    return true
  else
    return false
  end
end
GameUIWDDefence.Members = nil
function GameUIWDDefence.FetchMembersCallback(msgtype, content)
  DebugWD("FetchMembersCallback")
  DebugWD(msgtype)
  DebugWDTable(content)
  if msgtype == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_members_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    end
    return true
  end
  if msgtype == NetAPIList.alliance_members_ack.Code then
    GameUIWDDefence.Members = LuaUtils:table_rcopy(content.members)
    local flash_obj = GameUIWDDefence:GetFlashObject()
    if flash_obj then
      GameUIWDDefence:RefreshAppointList()
    end
    return true
  end
  return false
end
function GameUIWDDefence:RefreshAppointList()
  local flash_obj = self:GetFlashObject()
  if flash_obj then
    flash_obj:InvokeASCallback("_root", "clearListItem")
    local count = 0
    if GameUIWDDefence.Members ~= nil then
      count = #GameUIWDDefence.Members
    end
    if count > 0 then
      flash_obj:InvokeASCallback("_root", "initListItem", count)
      for i = 1, count do
        flash_obj:InvokeASCallback("_root", "addListItem", i)
      end
    end
  end
end
function GameUIWDDefence:UpdateAppointItem(id)
  local itemKey = tonumber(id)
  local userInfo = GameUIWDDefence.Members[itemKey]
  local main_avatar = "male"
  local main_fleet_level = 0
  for j = 1, #userInfo.fleet_ids do
    if userInfo.fleet_ids[j].fleet_id == 1 then
      main_fleet_level = userInfo.fleet_ids[j].levelup
      break
    end
  end
  main_avatar = GameDataAccessHelper:GetFleetAvatar(userInfo.main_fleet_id, 0, userInfo.sex)
  local sex = userInfo.sex
  local name = GameUtils:GetUserDisplayName(userInfo.name)
  local lv = GameLoader:GetGameText("LC_MENU_Level") .. userInfo.level
  local force = GameUtils.numberConversion(userInfo.force)
  local appointText = GameLoader:GetGameText("LC_MENU_APPOINT_BUTTON")
  local forceText = GameLoader:GetGameText("LC_MENU_FRIEND_BATTLE")
  DebugWD("UpdateAppointItem")
  DebugWD(id)
  DebugWD(itemKey, name, main_avatar, lv, force, appointText, forceText)
  DebugWDTable(userInfo)
  self:GetFlashObject():InvokeASCallback("_root", "setItem", itemKey, name, main_avatar, lv, force, appointText, forceText)
end
GameUIWDDefence.tryToAppointUser = nil
function GameUIWDDefence:AppointLeader(arg)
  local itemKey = tonumber(arg)
  local userInfo = GameUIWDDefence.Members[itemKey]
  GameUIWDDefence.tryToAppointUser = LuaUtils:table_rcopy(userInfo)
  if not userInfo then
    return
  end
  local requestParam = {
    user_id = userInfo.id
  }
  NetMessageMgr:SendMsg(NetAPIList.alliance_set_defender_req.Code, requestParam, GameUIWDDefence.AppointCallback, true, nil)
end
function GameUIWDDefence.AppointCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_set_defender_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_APPOINT_SUCCESS_ALERT"))
      DebugOut("GameUIWDDefence.tryToAppointUser = ", GameUIWDDefence.tryToAppointUser)
      DebugOut("GameUIWDDefence.curLeader = ", GameUIWDDefence.curLeader)
      DebugWDTable(GameUIWDDefence.DefenceInfo)
      local flash_obj = GameUIWDDefence:GetFlashObject()
      if flash_obj and GameUIWDDefence.tryToAppointUser ~= nil then
        if GameUIWDDefence.DefenceInfo and GameUIWDDefence.tryToAppointUser.id ~= GameUIWDDefence.curLeader then
          do
            local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
            if GameUIWDDefence.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER] then
              local extraInfo = {}
              extraInfo.defencerName = GameUIWDDefence.DefenceInfo.leader_name or ""
              extraInfo.allianceName = GameUIUserAlliance.alliance_info.name or ""
              FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SET_DEFFENCE_LEADER, extraInfo)
              flash_obj:InvokeASCallback("_root", "setEnableStory", false)
            end
          end
        else
        end
        GameUIWDDefence.curLeader = GameUIWDDefence.tryToAppointUser.id
        flash_obj:InvokeASCallback("_root", "setDefenceLeader", GameUtils:GetUserDisplayName(GameUIWDDefence.tryToAppointUser.name))
      end
    end
    return true
  end
  return false
end
function GameUIWDDefence.DonateCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_defence_donate_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      local flash_obj = GameUIWDDefence:GetFlashObject()
      if flash_obj then
        local player_res = GameGlobalData:GetData("resource")
        flash_obj:InvokeASCallback("_root", "setDefaultBrick", player_res.brick)
      end
      local countryStr = ext.GetDeviceCountry and ext.GetDeviceCountry() or "unknow"
      if GameUIWDDefence.mShareStoryChecked[FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK] then
        local extraInfo = {}
        extraInfo.allianceName = GameUIUserAlliance.alliance_info.name or ""
        FacebookGraphStorySharer:ShareStory(FacebookGraphStorySharer.StoryType.STORY_TYPE_STARALLIANCE_SEND_BRICK, extraInfo)
      else
      end
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_DEFDONATION_SUCCESS_ALERT"))
    end
    return true
  end
  return false
end
function GameUIWDDefence.ReinforceCallback(msgType, content)
  if msgType == NetAPIList.common_ack.Code and content.api == NetAPIList.alliance_defence_repair_req.Code then
    if content.code ~= 0 then
      GameUIGlobalScreen:ShowAlert("error", content.code, nil)
    else
      GameUtils:ShowTips(GameLoader:GetGameText("LC_MENU_REINFORCE_SUCCESS_ALERT"))
    end
    return true
  end
  return false
end
