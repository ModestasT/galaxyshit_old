LuaUtils = {}
function LuaUtils:repr(val)
  if type(val) == "string" then
    return ...
  else
    return (...), val, val
  end
end
function LuaUtils:string_split(str, sep)
  local r = {}
  for i in string.gmatch(str, string.format("[^%s]+", sep)) do
    table.insert(r, i)
  end
  return r
end
function LuaUtils:string_startswith(str, prefix)
  return string.find(str, prefix) == 1
end
function LuaUtils:string_endswith(str, suffix)
  return ...
end
function LuaUtils:string_trim(s)
  local first = string.find(s, "%S")
  if not first then
    return ""
  end
  local last = string.find(string.reverse(s), "%S")
  return ...
end
function LuaUtils:table_merge(dest, src)
  for k, v in pairs(src) do
    dest[k] = v
  end
  return dest
end
function LuaUtils:table_imerge(dest, src)
  for i, v in ipairs(src) do
    table.insert(dest, v)
  end
end
function LuaUtils:array_equal(t, t2)
  for i, v in ipairs(t) do
    if v ~= t2[i] then
      return false
    end
  end
  return true
end
function LuaUtils:array_unique(array)
  local r = {}
  local s = {}
  for _, v in ipairs(array) do
    if not s[v] then
      s[v] = true
      table.insert(r, v)
    end
  end
  return r
end
function LuaUtils:array_new(len, val)
  local r = {}
  for i = 1, len do
    table.insert(r, val)
  end
  return r
end
function LuaUtils:array_find(t, val)
  for i, v in ipairs(t) do
    if val == v then
      return i
    end
  end
  return -1
end
function LuaUtils:array_find_range_if(array, first, last, predicate)
  for i = first, last do
    if predicate(array[i]) then
      return i, array[i]
    end
  end
  return 0
end
function LuaUtils:array_find_if(array, predicate)
  return ...
end
function LuaUtils:table_size(t)
  local r = 0
  for _, _ in pairs(t) do
    r = r + 1
  end
  return r
end
function LuaUtils:table_empty(t)
  return not next(t)
end
function LuaUtils:table_equal(t, t2)
  local n = 0
  for k, v in pairs(t) do
    if v ~= t2[k] then
      return false
    end
    n = n + 1
  end
  return n == self:table_size(t2)
end
function LuaUtils:table_copy(t)
  return ...
end
function LuaUtils:table_keys(t)
  local r = {}
  for k, _ in pairs(t) do
    table.insert(r, k)
  end
  return r
end
function LuaUtils:table_values(t)
  local r = {}
  for _, v in pairs(t) do
    table.insert(r, v)
  end
  return r
end
function LuaUtils:array_filter(arr, func)
  local r = {}
  for k, v in pairs(arr) do
    if func(k, v) then
      table.insert(r, v)
    end
  end
  return r
end
function LuaUtils:table_filter(t, func)
  local r = {}
  for k, v in pairs(t) do
    if func(k, v) then
      r[k] = v
    end
  end
  return r
end
function LuaUtils:table_map(t, func)
  local r = {}
  for k, v in pairs(t) do
    local nk, nv = func(k, v)
    r[nk] = nv
  end
  return r
end
function LuaUtils:array_partial_range(array, indexBegin, endIndex, predicate)
  local count = endIndex - indexBegin + 1
  assert(count > 0)
  if 1 == count then
    if predicate(array[indexBegin]) then
      return indexBegin + 1
    end
    return indexBegin
  end
  local indexLast = endIndex
  local i = indexBegin
  local j = indexLast
  while i < j do
    while i < j and not predicate(array[j]) do
      j = j - 1
    end
    while i < j and predicate(array[i]) do
      i = i + 1
    end
    if j > i then
      local temp = array[i]
      array[i] = array[j]
      array[j] = temp
    end
  end
  assert(i == j and "logic error if this assertion failed, should check this function")
  if predicate(array[i]) then
    return i + 1
  end
  return i
end
function LuaUtils:array_partial(array, predicate)
  return ...
end
function LuaUtils:array_find_range(arr, indexBegin, indexEnd, pre)
  for i = indexBegin, indexEnd do
    if pre(arr[i]) then
      return i
    end
  end
  return indexEnd + 1
end
function LuaUtils:array_copy_range(targetArr, targetIndexBegin, resourceArr, resourceArrBegin, resourceArrEnd, pre)
  if nil == pre then
    function pre(item)
      return true
    end
  end
  local targetIndex = targetIndexBegin
  for i = resourceArrBegin, resourceArrEnd do
    if pre(resourceArr[i]) then
      targetArr[targetIndex] = resourceArr[i]
      targetIndex = targetIndex + 1
    end
  end
end
function LuaUtils:stable_array_partial_range(arr, indexBegin, indexEnd, pre)
  local size = indexEnd - indexBegin + 1
  local arrForNotPre = {}
  for i = indexBegin, indexEnd do
    if not pre(arr[i]) then
      table.insert(arrForNotPre, arr[i])
      arr[i] = false
    end
  end
  local iCount = indexBegin
  while indexEnd >= iCount do
    local nextFalseIndex = self:array_find_range(arr, iCount, indexEnd, function(item)
      return false == item
    end)
    if nextFalseIndex == indexEnd + 1 then
      break
    end
    local nextUnFalseIndex = self:array_find_range(arr, nextFalseIndex, indexEnd, function(item)
      return false ~= item
    end)
    if nextUnFalseIndex == indexEnd + 1 then
      break
    end
    arr[nextFalseIndex] = arr[nextUnFalseIndex]
    arr[nextUnFalseIndex] = false
    iCount = nextFalseIndex + 1
  end
  local remainItemCount = size - #arrForNotPre
  self:array_copy_range(arr, indexBegin + remainItemCount, arrForNotPre, 1, #arrForNotPre)
  return remainItemCount + 1
end
function LuaUtils:array_shuffle(arr, indexBegin, indexEnd)
  local size = indexEnd - indexBegin + 1
  for i = indexBegin, indexEnd do
    local randomIndex = math.random(indexBegin, indexEnd)
    local temp = arr[randomIndex]
    arr[randomIndex] = arr[i]
    arr[i] = temp
  end
end
function LuaUtils:straight_insertion_sort(arr, indexBegin, indexEnd, fCompare)
  local arrCount = indexEnd - indexBegin + 1
  if arrCount <= 1 then
    return
  end
  local i, j, k
  i = indexBegin + 1
  while indexEnd >= i do
    j = indexBegin
    while true do
      if not (j <= i - 1) or fCompare(arr[i], arr[j]) then
        break
      end
      j = j + 1
    end
    if i ~= j then
      local temp = arr[i]
      for k = i, j + 1, -1 do
        arr[k] = arr[k - 1]
      end
      arr[j] = temp
    end
    i = i + 1
  end
end
LuaUtils.array_sort_max_recursion_deeps = 20
function LuaUtils:array_sort(arr, indexBegin, indexEnd, fCompare, deepNow)
  deepNow = deepNow or 1
  if deepNow > self.array_sort_max_recursion_deeps then
    return ...
  end
  local count = indexEnd - indexBegin + 1
  if count <= 32 then
    return ...
  end
  local referenceIndex = math.random(indexBegin, indexEnd)
  local reference = arr[referenceIndex]
  local function fForPartial(item)
    return ...
  end
  local mid = self:array_partial_range(arr, indexBegin, indexEnd, fForPartial)
  local nextDeep = deepNow + 1
  self:array_sort(arr, indexBegin, mid - 1, fCompare, nextDeep)
  return ...
end
function LuaUtils:table_requal(t, t2)
  local n = 0
  for k, v in pairs(t) do
    if type(v) == "table" then
      local v2 = t2[k]
      if type(v2) ~= "table" then
        return false
      end
      if not self:table_requal(v, v2) then
        return false
      end
    elseif v ~= t2[k] then
      return false
    end
    n = n + 1
  end
  return n == self:table_size(t2)
end
function LuaUtils:table_rcopy(t)
  local r = {}
  for k, v in pairs(t) do
    if type(v) == "table" then
      r[k] = self:table_rcopy(v)
    else
      r[k] = v
    end
  end
  return r
end
function LuaUtils:eval(str)
