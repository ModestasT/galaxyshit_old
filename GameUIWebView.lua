local GameUIWebView = LuaObjectManager:GetLuaObject("GameUIWebView")
function GameUIWebView:OnAddToGameState()
  if not self:GetFlashObject() then
    self:LoadFlashObject()
  end
  local flashObj = self:GetFlashObject()
  if flashObj then
    local webViewInfo = flashObj:InvokeASCallback("_root", "GetWebViewInfo")
    local param = LuaUtils:string_split(webViewInfo, "\001")
    Webview:Open(GameUIWebView.mWebUrl, param[3], param[4], param[5], param[6])
    GameUIWebView.mWebUrl = nil
  end
end
function GameUIWebView:OnEraseFromGameState()
  self:UnloadFlashObject()
end
function GameUIWebView:OnFSCommand(cmd, arg)
  if "close" == cmd then
    Webview:Destroy()
    GameUIWebView:Exit()
  end
end
function GameUIWebView:Exit()
  GameStateManager.GameStateWebview:EraseObject(self)
end
function GameUIWebView:CheckAndOpenWebView()
  if GameUIWebView.mWebUrl and GameStateManager:GetCurrentGameState() == GameStateManager.GameStateMainPlanet and not GameStateManager.GameStateWebview:IsObjectInState(self) then
    if AutoUpdate.isAndroidDevice then
      if Webview:IsSupport() then
        GameStateManager.GameStateWebview:AddObject(self)
      else
        ext.http.openURL(GameUIWebView.mWebUrl)
      end
    elseif Webview:IsSupport() then
      GameStateManager.GameStateWebview:AddObject(self)
    end
  end
end
function GameUIWebView.PlayerRecall(content)
  if content and content.url and string.len(content.url) > 0 then
    GameUIWebView.mWebUrl = content.url
    GameUIWebView:CheckAndOpenWebView()
  end
end
